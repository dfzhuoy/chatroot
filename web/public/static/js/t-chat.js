

/**
 * 上课
 * @return {[type]} [description]
 */
function beginClass(){
    var msg = {};
    msg.cmd = 'begin';
    msg.from = client_id;
    msg.content = '上课啦';
    msg.cfd = ta;
    console.log(msg);
    ws.send($.toJSON(msg));
}
/**
 * 下课
 * @return {[type]} [description]
 */
function endClass(){
    var msg = {};
    msg.cmd = 'end';
    msg.from = client_id;
    msg.content = '下课啦';
    msg.cfd = ta;
    console.log(msg);
    ws.send($.toJSON(msg));
}

