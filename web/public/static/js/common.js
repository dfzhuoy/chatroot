
function setCookie(name,value,time){     
    var exp  = new Date();
    if(time){
        exp.setTime(exp.getTime() + time*60*60*1000);
    }   
     else{
        exp.setTime(exp.getTime() + 4*60*60*1000); 
        }  
    if(time == 0){
        var ctime = ""
    }
    else{
        var ctime = ";expires=" + exp.toGMTString()
    }
    var domain = ';domain=.chat.jince.com'
    document.cookie = name + "="+ encodeURIComponent(value)+ domain + ctime;   
} 
function getCookie(name)
{
	var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");

	if (arr = document.cookie.match(reg))
		return decodeURIComponent(decodeURIComponent(arr[2]));
	else
		return null;
}
function center(obj) {
	var o = $('#' + obj);
	$("body").append('<div id="blackbg"></div>')
	o.show();
	var iWidth = o.width();
	var iHeight = o.height();
	var iLeft = ($(window).width() - iWidth) / 2;
	var iTop = ($(window).height() - iHeight) / 3;
	o.css({left: iLeft, top: iTop})
	$(".closebt").click(function () {
		o.fadeOut();
		$("#blackbg").fadeOut();
		$("#blackbg").remove();
	});
}
function removeCenter(time) {
	if (time) {
		setTimeout(function () {
			$(".checkbox").fadeOut();
			$("#blackbg").remove();
		}, time * 1000)
	}
	else {
		$(".checkbox").fadeOut();
		$("#blackbg").remove();
	}

}
function GetDateT(time_stamp) {
	var d;
	d = new Date();

	if (time_stamp) {
		d.setTime(time_stamp * 1000);
	}
	var h, i, s;
	h = d.getHours();
	i = d.getMinutes();
	s = d.getSeconds();

	h = (h < 10) ? '0' + h : h;
	i = (i < 10) ? '0' + i : i;
	s = (s < 10) ? '0' + s : s;
	return h + ":" + i + ":" + s;
}

function getRequest() {
	var url = location.search; // 获取url中"?"符后的字串
	var theRequest = new Object();
	if (url.indexOf("?") != -1) {
		var str = url.substr(1);

		strs = str.split("&");
		for (var i = 0; i < strs.length; i++) {
			var decodeParam = decodeURIComponent(strs[i]);
			var param = decodeParam.split("=");
			theRequest[param[0]] = param[1];
		}

	}
	return theRequest;
}
function request(paras) {
    var url = location.href;
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    var paraObj = {}
    for (i = 0; j = paraString[i]; i++) {
        paraObj[j.substring(0, j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1, j.length);
    }
    var returnValue = paraObj[paras.toLowerCase()];
    if (typeof(returnValue) == "undefined") {
        return "";
    } else {
        return decodeURIComponent(returnValue);
    }
}
function tabSwitch(){
	$(".tabul li:not(.noTab)").click(function(){
		$(".tabul li").removeClass("active");
		$(this).addClass("active");
		$(".tabbox").hide();
		$(".tabbox").eq($(this).index()).show()
    })
}
function loadScript(url, callback){

	var script = document.createElement("script")
	script.type = "text/javascript";
	if (script.readyState){ //IE
		script.onreadystatechange = function(){
			if (script.readyState == "loaded" || script.readyState == "complete"){
				script.onreadystatechange = null;
				callback();
			}
		};
	} 
	else { //Others: Firefox, Safari, Chrome, and Opera
		script.onload = function(){
			callback();
		};
	}
	script.src = url;
	document.body.appendChild(script);
} 
if (!console) {
    var console = {};
    console.log = function (str) {};
    console.dir = function (str) {};
}
function parseDate(time,format){
			var year=time.getFullYear(),month=time.getMonth()+1,date=time.getDate(),hours=time.getHours(),minutes=time.getMinutes(),seconds=time.getSeconds(),result;
			result=format.replace(/YYYY/gi,year)
				.replace(/YY/gi,addZero(year%100))
				.replace(/MM/g,addZero(month))
				.replace(/M/g,month)
				.replace(/DD/g,addZero(date))
				.replace(/D/g,date)
				.replace(/HH/gi,addZero(hours))
				.replace(/H/gi,hours)
				.replace(/mm/g,addZero(minutes))
				.replace(/m/g,minutes)
				.replace(/SS/gi,addZero(seconds))
				.replace(/S/gi,seconds);

			return result;
	
}
function addZero(num){
	if(parseInt(num)<10){
		num="0"+num;
	}
	return num;
}
function getTimeFromString(timeString){
			if(parseInt(timeString)==timeString*1){
				return new Date(timeString*1);
			}
			var date=timeString.split(" ");
			var dateArray=date[0].split("-");
			dateArray[1]=dateArray[1]-1;
			if(date[1]){
				var tempArray=date[1].split(":");
				tempArray[0]-=8;
				dateArray=dateArray.concat(tempArray);
			}
			return Date.UTC.apply(null,dateArray);
}
