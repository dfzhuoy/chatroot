
function listenCallEvent() {

	//使用原生WebSocket
	if (window.WebSocket || window.MozWebSocket)
	{
		ws = new WebSocket(webim.server);
	}
	//使用flash websocket
	else if (webim.flash_websocket)
	{
		WEB_SOCKET_SWF_LOCATION = "/static/flash-websocket/WebSocketMain.swf";
		$.getScript("/static/flash-websocket/swfobject.js", function () {
			$.getScript("/static/flash-websocket/web_socket.js", function () {
				ws = new WebSocket(webim.server);
			});
		});
	}
	//使用http xhr长轮循
	else
	{
		ws = new Comet(webim.server);
	}
	/**
	 * 连接建立时触发
	 */
	ws.onopen = function (e) {
		//发送登录信息
		msg = new Object();
		msg.cmd = 'login';
		msg.name = userinfo.user.user_nicename || "未设置";
		msg.avatar = userinfo.user.avatar || default_av
		msg.fd = userinfo.user.id;
		msg.uid = userinfo.user.id;
		ws.send($.toJSON(msg));
	};
	ws.onmessage = function (e) {
		var message = $.evalJSON(e.data);
		var cmd = message.cmd;
		console.log(message)
	}
	/**
	 * 连接关闭事件
	 */
	ws.onclose = function (e) {
        msg = new Object();
        msg.cmd = 'close';
        msg.name = userinfo.user.user_nicename;
        msg.room = room_id;
        msg.avatar = userinfo.user.avatar;
        ws.send($.toJSON(msg));
		setTimeout(function () {
			console.log("对不起，聊天服务器特殊原因中断，请再次刷新！")
		}, 1000)
		listenCallEvent()
	};

	/**
	 * 异常事件
	 */
	ws.onerror = function (e) {
		//alert("异常:" + e.data);
	};
}
var pageload = {
    api:"?g=Admin&m=public&a=is_login"
};
pageload.init=function(){
    getCallUserInfo(pageload.api);
}
/*喊单页获取用户信息*/
function getCallUserInfo(api){ 
    var url = api;
    $.getJSON(url,function(response){
        if(response){
         userinfo=  response;
         listenCallEvent();
        }    
    })
    
}
function checkCall(){
	var category=$("#category").val();
	var category_name = $("#category option:selected").html();
	var price_category = $("#price_category").val();
	var price_category_name = $("#price_category option:selected").html();
	var price = $("input[name='price']").val();
	var position=$("#position").val();
	var position_name = $("#position option:selected").html();
	var stop_profit_price = $("input[name='stop_profit_price']").val();
	var stop_loss_price = $("input[name='stop_loss_price']").val();
	var remark = $("textarea[name='remark']").val();
	var deal_variety=$("#deal_variety").val();
	var deal_variety_name = $("#deal_variety option:selected").html()
	if(price==""){
		alert("请输入价格");
		return;
	}
	else if(stop_profit_price==""){
		alert("请输入止盈价格");
		return;
	}
	else if(stop_loss_price==""){
		alert("请输入止损价格");
		return;
	}
	else if(remark==""){
		alert("请输入开仓备注");
		return;
	}
	var data = {
		category:category,
		price_category:price_category,
		price:price,
		position:position,
		stop_profit_price:stop_profit_price,
		stop_loss_price:stop_loss_price,
		deal_variety:deal_variety,
		remark:remark,
		category_name:category_name,
		deal_variety_name:deal_variety_name,
		position_name:position_name,
		price_category_name:price_category_name

	}
	sendCall(data)
}
function sendCall(data){
	var msg = {};
	msg.cmd = 'call';
	msg.from = userinfo.user.id;
	msg.category = data.category;
	msg.price_category = data.price_category;
	msg.price = data.price;
	msg.position = data.position;
	msg.stop_profit_price = data.stop_profit_price;
	msg.stop_loss_price = data.stop_loss_price;
	msg.deal_variety = data.deal_variety;
	msg.remark = data.remark;
	msg.category_name = data.category_name;
	msg.deal_variety_name = data.deal_variety_name
	msg.position_name = data.position_name;
	msg.price_category_name = data.price_category_name
	ws.send($.toJSON(msg));
	console.log(msg)
    alert("发送成功")
}
function checkCloseCall(){
	var id=$("#callid").val()
	var close_price_category = $("#close_price_category").val();
	var close_price = $("input[name='close_price']").val();
	var close_remark = $("textarea[name='close_remark']").val();
	if(close_price==""){
		alert("请输入平仓价格");
		return;
	}
	else if(close_remark==""){
		alert("请输入平仓备注");
		return;
	}
	var data={
		id:id,
		close_price_category:close_price_category,
		close_price:close_price,
		close_remark:close_remark

	}
	sendCloseCall(data)
}
function sendCloseCall(data){
	var msg = {};
	msg.cmd = 'call';
	msg.from = userinfo.user.id;
	msg.id = data.id;
	msg.if_close = 1;
	msg.close_price_category = data.close_price_category;
	msg.close_price = data.close_price;
	msg.close_remark = data.close_remark;
	ws.send($.toJSON(msg));
	console.log(msg)
    alert("平仓成功")
}
