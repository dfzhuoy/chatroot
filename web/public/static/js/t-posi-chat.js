

/**
 * 上课
 * @return {[type]} [description]
 */
function beginClass(s){
    
    $(".bclass").addClass("chatbt-dis").attr("disabled","disabled");
    $(".eclass").removeClass("chatbt-dis").removeAttr("disabled");
    if(s){
        var msg = {};
        msg.cmd = 'beginClass';
        msg.from = client_id;
        msg.content = '上课啦';
        msg.cfd = ta;
        msg.room = room_id;
        ws.send($.toJSON(msg));
        postChatStatus(2)
    }
    
}
/**
 * 下课
 * @return {[type]} [description]
 */
function endClass(s){
    
    $(".eclass").addClass("chatbt-dis").attr("disabled","disabled");
    $(".bclass").removeClass("chatbt-dis").removeAttr("disabled");
    if(s){
        var msg = {};
        msg.cmd = 'endClass';
        msg.from = client_id;
        msg.content = '下课啦';
        msg.cfd = ta;
        msg.room = room_id;
        ws.send($.toJSON(msg));
        postChatStatus(3)
    }
}
function beginSchool(s){
    
    $(".bschool").addClass("chatbt-dis").attr("disabled","disabled");
    $(".eschool").removeClass("chatbt-dis").removeAttr("disabled");
    if(s){
        var msg = {};
        msg.cmd = 'beginSchool';
        msg.from = client_id;
        msg.content = '上学啦';
        msg.cfd = ta;
        msg.room = room_id;
        ws.send($.toJSON(msg));
        postChatStatus(1)
    }
}
/**
 * 下课
 * @return {[type]} [description]
 */
function endSchool(s){

    $(".eschool").addClass("chatbt-dis").attr("disabled","disabled");
    $(".bschool").removeClass("chatbt-dis").removeAttr("disabled");
    if(s){
        var msg = {};
        msg.cmd = 'endSchool';
        msg.from = client_id;
        msg.content = '放学啦';
        msg.cfd = ta;
        msg.room = room_id;
        ws.send($.toJSON(msg));
        postChatStatus(4)
    }
}
function postChatStatus(type){
    var data = {"livestatus":type,"ta":ta,"room_id":room_id};
    $.getJSON(webconfig.apihost+"?c=api&a=changeroom&callback=?",data,function(response){

    });
}
function isTeacher(){
    $.getJSON(webconfig.apihost+"?c=api&a=isteacher&callback=?",function(response){
        if(response.code==1){
             
        }
        else{
            window.location.href="/?room="+user_room_id;
        }
    });
}

