
	Handlebars.registerHelper("compare",function(){
	    var operators=["+","-","*","/","=","==","===","!=","!==","&&","||","!","&","|","^","-","(",")",">",">=","<","<=","%"],operatorsLen=operators.length;
	    var args=[].splice.call(arguments,0,arguments.length);
	    var options=args.splice(-1,1)[0];
	    var fnString="",fn;
	    $.each(args,function(index,item){
	        var isOperator=false;
	        for(var i=0;i<operatorsLen;i++){
	            if(operators[i]==$.trim(item)){
	                isOperator=true;
	                break;
	            }
	        }
	        if((typeof item =="string")&&!isOperator){
	            fnString+="'"+item+"'";
	        }else{
	            fnString+=item;
	        }
	    })
	    try{
	        fn=new Function( "return "+fnString+";");
	    }catch(err){
	        throw new Error("compare 的参数不能构成函数");
	    }
	    if(options.fn){
	        if(!!fn()){
	            return options.fn(this);
	        }else{
	            return options.inverse(this);
	        }
	    }else{
	        return fn();
	    }
	});

	Handlebars.registerHelper("toFixed",function(number,point){
		return parseFloat(number).toFixed(point);
	});
	Handlebars.registerHelper("showTime",function(time,format,level){
		if(typeof level == "number"){
			time = time*level;
		}
		if(!time){
			return "";
		}
		if(typeof time == "number"){
			time=new Date(time);
		}
		if(!(time instanceof Date)){
			time=new Date(getTimeFromString(time));
		}
		if(!(time instanceof Date)){
			return "";
		}
		return parseDate(time,format);
	});
	Handlebars.registerHelper("parseXss",function(val){
		val = val.replace(/#(\d*)/g, '<img src="public/static/img/face/$1.gif" />');
		val = val.replace('&amp;', '&');
		return val;
	});

