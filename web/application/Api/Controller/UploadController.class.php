<?php

/**
 * 喊单接口
 */
namespace Api\Controller;
use Think\Controller;
use Think\Upload;

class UploadController extends Controller
{
    public function index(){
        $files = $_FILES;
        if(empty($files)){
            $this->ajaxreturn(['code'=>-1,'messgae'=>'没有上传文件'],'json');
        }
        $config = array(
            'maxSize'    =>    3145728,
            'rootPath'   =>    '/alidata/www/chat.xz286.com/web/data',
            'savePath'   =>    '/upload/img/',
            'saveName'   =>    array('uniqid',''),
            'exts'       =>    array('jpg', 'gif', 'png', 'jpeg'),
            'autoSub'    =>    true,
            'subName'    =>    array('date','Ymd'),
        );
        $upload = new Upload($config);// 实例化上传类
        // 上传文件
        $info   =   $upload->upload($files);
        if(!$info) {// 上传错误提示错误信息
            $this->ajaxreturn(['code'=>-1,'result'=>$upload->getError()],'json');
        }else{
            //print_r($info);
            // 上传成功
            $url= 'http://'.$_SERVER['HTTP_HOST'].'/data'.$info['name']['savepath'].$info['name']['savename'];
            $this->ajaxreturn(['code'=>1,'result'=>$url],'json');
        }
    }

    function test(){
        echo $str = <<<EOT
            <form action="/?s=api/upload/index" enctype="multipart/form-data" method="post" >
            <input type="file" name="name" />
            <input type="submit" value="提交" >
            </form>
EOT;
    }
}