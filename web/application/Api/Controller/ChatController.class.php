<?php

/**
 * 喊单接口
 */
namespace Api\Controller;
use Common\Model\CallModel;
use Common\Model\ChatModel;
use Think\Controller;
class ChatController extends Controller
{
    /**
     * 聊天历史
     */
    public function history(){
        //用户类型:3普通用户,2是老师,1是全部
        $user_type = I('request.user_type',3);
        $page = I('request.page',1);
        $limit_num = I('request.limit_num',20);
        $callModel = new ChatModel();
        $data = $callModel->historyList($user_type,$page,$limit_num);
        $this->ajaxreturn($data,'json');
    }

}