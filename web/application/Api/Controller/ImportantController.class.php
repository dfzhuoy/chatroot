<?php

/**
 * 重要经济数据接口
 */
namespace Api\Controller;
use Common\Model\ConfigModel;
use Common\Model\ImportantModel;
use Think\Controller;
class ImportantController extends Controller
{
    public function index(){
        $imptModel = new ImportantModel();
        $map['status'] = 1;
        $row = $imptModel->cache(true,10)->where($map)->order('id desc')->find();

        $home_row = (new ConfigModel())->getvalue('home_link_photo');
        $home_row = explode("|",$home_row[0]);
        $this->ajaxreturn(['imp'=>$row,'home'=>$home_row],'json');
    }
}
