<?php

/**
 * 重要经济数据接口
 */
namespace Api\Controller;
use Common\Model\ImportantModel;
use Common\Model\ZhanfaModel;
use Think\Controller;
class ZhanfaController extends Controller
{
    public function index(){
        $imptModel = new ZhanfaModel();
        $page = I('request.page',1);
        $limit_num = I('request.limit_num',15);

        $map['status'] = 1;
        $total = $imptModel->where($map)->count();

        $pageinfo = page_info($total,$page,$limit_num);

        $data = $imptModel->cache(true,10)->where($map)->page($page,$limit_num)->order('id desc')->select();
        $this->ajaxreturn(['list'=>$data,'pageinfo'=>$pageinfo],'json');
    }
}
