<?php

/**
 * 喊单接口
 */
namespace Api\Controller;
use Common\Model\CallModel;
use Think\Controller;
class CallController extends Controller
{

    /**
     * 最新喊单
     */
    public function now(){
        $callModel = new CallModel();
        $row = $callModel->now();
        $this->ajaxreturn($row,'json');
    }
    /**
     * 喊单历史
     */
    public function history(){
        $teacher_id = I('request.teacher_id');
        $page = I('request.page',1);
        $limit_num = I('request.limit_num',20);
        $callModel = new CallModel();
        $data = $callModel->historyList($teacher_id,'',$page,$limit_num);
        $this->ajaxreturn($data,'json');
    }

}