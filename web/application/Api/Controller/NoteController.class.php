<?php

/**
 * 纸条接口
 */
namespace Api\Controller;
use Common\Model\CallModel;
use Common\Model\NoteModel;
use Think\Controller;
class NoteController extends Controller
{
    /**
     * 纸条历史
     */
    public function history(){
        $page = I('request.page',1);
        $limit_num = I('request.limit_num',20);
        $callModel = new NoteModel();
        $user = $_SESSION['user'];
        $uid = $user['id'];
        $data = $callModel->historyList($uid,$page,$limit_num);
        $this->ajaxreturn($data,'json');
    }

}