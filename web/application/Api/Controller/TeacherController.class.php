<?php

/**
 * 纸条接口
 */
namespace Api\Controller;
use Common\Model\CallModel;
use Common\Model\NoteModel;
use Common\Model\UsersModel;
use Think\Controller;
class TeacherController extends Controller
{
    /**
     * 专家团队
     */
    public function index(){
        $map['user_type'] = 2;
        $map['user_status'] = 1;
        $data = (new UsersModel())->cache(true,10)->where($map)->field('id,avatar,user_nicename,signature,user_title,user_star')->select();
        foreach ($data as $key => &$value) {
            $value['avatar'] = sp_get_user_avatar_url($value['avatar']);
        }
        /*//判断老师状态,获取redis的数据
        $redis = new \Redis();
        $redis->connect('127.0.0.1', '6379');
        $redis->auth('*%FAU^J09mNU*xNU');
        foreach ($data as &$item) {
            $fd = $redis->hGet('online_client_uid',$item['id']);
            $item['live'] = $fd ? true:false; //老师状态
        }*/
        $this->ajaxreturn($data,'json');
    }

}