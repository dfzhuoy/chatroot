<?php

/**
 * 会员
 */
namespace User\Controller;
use Common\Controller\AdminbaseController;
class IndexadminController extends AdminbaseController {
    function index(){
        $map['user_type'] = 3;
        I('request.user_login') ? ($map['user_login'] = I('request.user_login')) : '';
        I('request.user_nicename') ? ($map['user_nicename'] = array('like','%'.I('request.user_nicename').'%')) : '';

    	$users_model=M("Users");
    	$count=$users_model->where($map)->count();
    	$page = $this->page($count, 20);
    	$lists = $users_model
        	->where($map)
        	->order("create_time DESC")
        	->limit($page->firstRow . ',' . $page->listRows)
        	->select();
    	$this->assign('lists', $lists);
        $this->assign('map', $map);
    	$this->assign("page", $page->show('Admin'));
    	
    	$this->display(":index");
    }
	function adduser(){
		$this->display(":adduser");
	}
    function ban(){
    	$id=intval($_GET['id']);
    	if ($id) {
    		$rst = M("Users")->where(array("id"=>$id,"user_type"=>3))->setField('user_status','0');
    		if ($rst) {
    			$this->success("会员拉黑成功！", U("indexadmin/index"));
    		} else {
    			$this->error('会员拉黑失败！');
    		}
    	} else {
    		$this->error('数据传入失败！');
    	}
    }
    
    function cancelban(){
    	$id=intval($_GET['id']);
    	if ($id) {
    		$rst = M("Users")->where(array("id"=>$id,"user_type"=>3))->setField('user_status','1');
    		if ($rst) {
    			$this->success("会员启用成功！", U("indexadmin/index"));
    		} else {
    			$this->error('会员启用失败！');
    		}
    	} else {
    		$this->error('数据传入失败！');
    	}
    }

	public function edit(){
		$users_model=M("Users");
		$id = I('request.id');
		if(empty($id)){
			$this->error('id'.L('COMMON_EMPTY'));
		}
		if(IS_POST){
			$data = I('request.');
			if ($users_model->create($data)){
				if ($users_model->where(['id'=>$id])->save()!==false) {
					$this->success(L('SAVE_SUCCESS'), U("indexadmin/index"));
				} else {
					$this->error(L('SAVE_FAILED'));
				}
			} else {
				$this->error($users_model->getError());
			}
		}
		$this->action_name  = __FUNCTION__;
		$this->row = $users_model->where(['id'=>$id])->find();
		$this->display(':adduser');
	}
}
