<?php
return array (
  'app' => 'Admin',
  'model' => 'Zbs',
  'action' => 'default',
  'data' => '',
  'type' => '1',
  'status' => '1',
  'name' => '直播室管理',
  'icon' => 'bullseye',
  'remark' => '',
  'listorder' => '0',
  'children' => 
  array (
    array (
      'app' => 'Admin',
      'model' => 'Important',
      'action' => 'index',
      'data' => '',
      'type' => '1',
      'status' => '1',
      'name' => '重要经济数据',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Call',
      'action' => 'index',
      'data' => '',
      'type' => '1',
      'status' => '1',
      'name' => '喊单管理',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'User',
      'action' => 'teacher',
      'data' => '',
      'type' => '1',
      'status' => '1',
      'name' => '老师管理',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Live',
      'action' => 'index',
      'data' => '',
      'type' => '1',
      'status' => '1',
      'name' => '聊天',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Zhanfa',
      'action' => 'index',
      'data' => '',
      'type' => '1',
      'status' => '1',
      'name' => '战法列表',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Word',
      'action' => 'index',
      'data' => '',
      'type' => '1',
      'status' => '1',
      'name' => '敏感词管理',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Call',
      'action' => 'add',
      'data' => '',
      'type' => '1',
      'status' => '0',
      'name' => '添加喊单',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Call',
      'action' => 'ping',
      'data' => '',
      'type' => '1',
      'status' => '0',
      'name' => '喊单平仓',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Word',
      'action' => 'add',
      'data' => '',
      'type' => '1',
      'status' => '0',
      'name' => '敏感词添加',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Word',
      'action' => 'edit',
      'data' => '',
      'type' => '1',
      'status' => '0',
      'name' => '敏感词编辑',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Word',
      'action' => 'del',
      'data' => '',
      'type' => '1',
      'status' => '0',
      'name' => '敏感词隐藏',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Zhanfa',
      'action' => 'add',
      'data' => '',
      'type' => '1',
      'status' => '0',
      'name' => '添加战法',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Zhanfa',
      'action' => 'del',
      'data' => '',
      'type' => '1',
      'status' => '0',
      'name' => '隐藏战法',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Chat',
      'action' => 'index',
      'data' => '',
      'type' => '1',
      'status' => '1',
      'name' => '消息管理',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Important',
      'action' => 'add',
      'data' => '',
      'type' => '1',
      'status' => '0',
      'name' => '重要经济数据添加',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Important',
      'action' => 'edit',
      'data' => '',
      'type' => '1',
      'status' => '0',
      'name' => '重要经济数据修改',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Important',
      'action' => 'del',
      'data' => '',
      'type' => '1',
      'status' => '0',
      'name' => '重要经济数据删除',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Chat',
      'action' => 'del',
      'data' => '',
      'type' => '1',
      'status' => '0',
      'name' => '消息删除',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'call',
      'action' => 'edit',
      'data' => '',
      'type' => '1',
      'status' => '0',
      'name' => '喊单编辑',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
    array (
      'app' => 'Admin',
      'model' => 'Call',
      'action' => 'del',
      'data' => '',
      'type' => '1',
      'status' => '0',
      'name' => '喊单删除',
      'icon' => '',
      'remark' => '',
      'listorder' => '0',
    ),
  ),
);