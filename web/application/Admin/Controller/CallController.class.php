<?php
namespace Admin\Controller;
use Common\Controller\AdminbaseController;
use Common\Model\CallModel;
use Common\Model\UsersModel;

class CallController extends AdminbaseController{
	function __construct() {
		parent::__construct();
		$this->model =  new CallModel();
	}

	/**
	 * 喊单
	 */
	public function index(){
		$map['status'] = 1;
		I('request.category') ? ($map['category'] = I('request.category')) : '';
		I('request.position') ? ($map['position'] = I('request.position')) : '';
		I('request.content') ? ($map['content'] = array('like','%'.I('request.content').'%')) : '';

		$count = $this->model->where($map)->count();
		$page = $this->page($count, 50);

		$data = $this->model->where($map)->order(array("id" => "desc"))->limit($page->firstRow . ',' . $page->listRows)->select();
        $category = $this->model->getCategory();
        $deal_variety = $this->model->getDealVariety();
        $position = $this->model->getPostion();
        $price_category = $this->model->getPriceCategory();
        foreach($data as $k=>&$v){
            $v['teacher_name'] = (new UsersModel())->getNiceName($v['teacher_id']);
            $v['category_name'] = $category[$v['category']]['name'];
            $v['deal_variety_name'] = $deal_variety[$v['deal_variety']]['name'];
            $v['position_name'] = $position[$v['position']]['name'];
            $v['price_category_name'] = $price_category[$v['price_category']]['name'];

        }
		$_GET = $map;
		$this->assign('map',I('request.'));
		$this->assign('data',$data);
		$this->assign("Page", $page->show('Admin'));
		$this->display();
	}
	/**
	 * 添加
	 */
	public function add(){
		if(IS_POST){
			$data = I('request.');
            $data['teacher_id'] = $_SESSION['ADMIN_ID'];
			if ($this->model->create($data)){
				if ($this->model->add()!==false) {
					$this->success(L('ADD_SUCCESS'), U("Call/index"));
				} else {
					$this->error(L('ADD_FAILED'));
				}
			} else {
				$this->error($this->model->getError());
			}
		}
        $this->assign('category',$this->model->getCategory());
        $this->assign('price_category',$this->model->getPriceCategory());
        $this->assign('position',$this->model->getPostion());
        $this->assign('deal_variety',$this->model->getDealVariety());

        $this->display();
	}

    /**
     * 平仓
     */
    public function ping(){
        $id = I('request.id');
        if(empty($id)){
            $this->error('id'.L('COMMON_EMPTY'));
        }
        if(IS_POST){
            $data = I('request.');
            $data['if_close'] = 1;
            $data['close_teacher_id'] = $_SESSION['ADMIN_ID'];
            if ($this->model->create($data)){
                if ($this->model->save()!==false) {
                    $this->success(L('ADD_SUCCESS'), U("Call/index"));
                } else {
                    $this->error(L('ADD_FAILED'));
                }
            } else {
                $this->error($this->model->getError());
            }
        }
        $row = $this->model->where(['id'=>$id])->find();
        $category = $this->model->getCategory();
        $deal_variety = $this->model->getDealVariety();
        $row['category_name'] = $category[$row['category']]['name'];
        $row['deal_variety_name'] = $deal_variety[$row['deal_variety']]['name'];

        $this->assign('row',$row);
        $this->assign('price_category',$this->model->getPriceCategory());
        $this->display('ping');
    }

	/**
	 * 编辑
	 */
	public function edit(){
		$id = I('request.id');
		if(empty($id)){
			$this->error('id'.L('COMMON_EMPTY'));
		}
		if(IS_POST){
			$data = I('request.');
			if ($this->model->create($data)){
				if ($this->model->save()!==false) {
					$this->success(L('ADD_SUCCESS'), U("Call/index"));
				} else {
					$this->error(L('ADD_FAILED'));
				}
			} else {
				$this->error($this->model->getError());
			}
		}
		$this->row = $this->model->where(['id'=>$id])->find();
		$this->display('add');
	}

	/**
	 * 删除
	 */
	public function del(){
		$id = I('request.id');
		if(empty($id)){
			$this->error('id'.L('COMMON_EMPTY'));
		}
		$map['id'] = $id;
		$row = $this->model->where($map)->find();
		if(empty($row)){
			$this->error('question'.L('COMMON_EMPTY'));
		}
		$row['status'] = -1;
		if ($this->model->where($map)->save($row)!==false) {
			$this->success(L('DEL_SUCCESS'), U("Call/index"));
		} else {
			$this->error(L('DEL_FAILED'));
		}
	}
}