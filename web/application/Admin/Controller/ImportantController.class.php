<?php
namespace Admin\Controller;
use Common\Controller\AdminbaseController;
use Common\Model\ImportantModel;

class ImportantController extends AdminbaseController{
	function __construct() {
		parent::__construct();
		$this->model =  new ImportantModel();
	}

	/**
	 * 试题
	 */
	public function index(){
		$map['status'] = 1;
		I('request.content') ? ($map['content'] = array('like','%'.I('request.content').'%')) : '';

		$count = $this->model->where($map)->count();
		$page = $this->page($count, 50);

		$data = $this->model->where($map)->order(array("id" => "desc"))->limit($page->firstRow . ',' . $page->listRows)->select();
		foreach($data as $k=>&$v){
		}
		$_GET = $map;
		$this->assign('map',I('request.'));
		$this->assign('data',$data);
		$this->assign("Page", $page->show('Admin'));
		$this->display();
	}
	/**
	 * 添加
	 */
	public function add(){
		if(IS_POST){
			$data = I('request.');
			if ($this->model->create($data)){
				if ($this->model->add()!==false) {
					$this->success(L('ADD_SUCCESS'), U("important/index"));
				} else {
					$this->error(L('ADD_FAILED'));
				}
			} else {
				$this->error($this->model->getError());
			}
		}
		$this->display();
	}

	/**
	 * 编辑
	 */
	public function edit(){
		$id = I('request.id');
		if(empty($id)){
			$this->error('id'.L('COMMON_EMPTY'));
		}
		if(IS_POST){
			$data = I('request.');
			if ($this->model->create($data)){
				if ($this->model->save()!==false) {
					$this->success(L('ADD_SUCCESS'), U("important/index"));
				} else {
					$this->error(L('ADD_FAILED'));
				}
			} else {
				$this->error($this->model->getError());
			}
		}
		$this->row = $this->model->where(['id'=>$id])->find();
		$this->display('add');
	}

	/**
	 * 删除
	 */
	public function del(){
		$id = I('request.id');
		if(empty($id)){
			$this->error('id'.L('COMMON_EMPTY'));
		}
		$map['id'] = $id;
		$row = $this->model->where($map)->find();
		if(empty($row)){
			$this->error('question'.L('COMMON_EMPTY'));
		}
		$row['status'] = -1;
		if ($this->model->where($map)->save($row)!==false) {
			$this->success(L('DEL_SUCCESS'), U("important/index"));
		} else {
			$this->error(L('DEL_FAILED'));
		}
	}
}