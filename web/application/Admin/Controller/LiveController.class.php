<?php
namespace Admin\Controller;
use Common\Controller\AdminbaseController;

class LiveController extends AdminbaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        $this->display();
    }
}
