<?php
namespace Admin\Controller;
use Common\Controller\AdminbaseController;
use Common\Model\ConfigModel;

class ConfigController extends AdminbaseController{
    public function __construct(){
        parent::__construct();
        $this->model = new ConfigModel();
    }

    /**
     * 配置管理
     */
    public function index(){
        $map['status'] = 1;
        I('request.key') ? ($map['key'] = I('request.key')) : '';

        I('request.info') ? ($map['info'] = array('like','%'.I('request.info').'%')) : '';
        I('request.value') ? ($map['value'] = array('like','%'.I('request.value').'%')) : '';

        if(I('request.be_time')){
            $map['create_time'] = ['EGT',I('request.be_time')];
        }
        if(I('request.end_time')){
            $map['create_time'] = ['ELT',I('request.end_time')];
        }
        if(I('request.be_time') && I('request.end_time')){
            $map['create_time'] = ['BETWEEN',array(I('request.be_time'),I('request.end_time'))];
        }

        $count = $this->model->where($map)->count();
        $page = $this->page($count, 20);

        $data = $this->model->where($map)->order(array("id" => "desc"))->limit($page->firstRow . ',' . $page->listRows)->select();
        /*foreach($data as $k=>&$v){
        }*/
        $this->assign('map',I('request.'));
        $this->assign('data',$data);
        $this->assign("Page", $page->show('Admin'));
        $this->display();
    }

    /**
     * 添加
     */
    public function add(){
        if(IS_POST){
            $data = I('request.');
            $data['creator'] = get_current_admin_id();
            if ($this->model->create($data)){
                if ($this->model->add()!==false) {
                    $this->success(L('ADD_SUCCESS'), U("config/index"));
                } else {
                    $this->error(L('ADD_FAILED'));
                }
            } else {
                $this->error($this->model->getError());
            }
        }
        $this->display();
    }

    /**
     * 编辑
     */
    public function editor(){
        $id = I('request.id');
        if(empty($id)){
            $this->error('id'.L('COMMON_EMPTY'));
        }
        if(IS_POST){
            $data = I('request.');
            $data['creator'] = get_current_admin_id();
            if ($this->model->create($data)){
                if ($this->model->save()!==false) {
                    $this->success(L('SAVE_SUCCESS'), U("config/index"));
                } else {
                    $this->error(L('SAVE_FAILED'));
                }
            } else {
                $this->error($this->model->getError());
            }
        }
        $this->row = $this->model->where(['id'=>$id])->find();
        $this->display('add');
    }

    /**
     * 删除
     */
    public function del(){
        $id = I('request.id');
        if(empty($id)){
            $this->error('id'.L('COMMON_EMPTY'));
        }
        $map['id'] = $id;
        $row = $this->model->where($map)->find();
        if(empty($row)){
            $this->error('question'.L('COMMON_EMPTY'));
        }
        $row['status'] = -1;
        if ($this->model->where($map)->save($row)!==false) {
            $this->success(L('DEL_SUCCESS'), U("config/index"));
        } else {
            $this->error(L('DEL_FAILED'));
        }
    }

}