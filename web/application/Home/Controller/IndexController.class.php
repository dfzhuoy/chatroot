<?php
    namespace Home\Controller;

    use Common\Controller\HomebaseController;

    class IndexController extends HomebaseController{
        public function __construct(){
            parent::__construct();
            $this->check_login();
        }
        public function index(){
            $this->display();
        }
        public function main(){
        	$this->display();
        }
        public function test(){
            phpinfo();
        }
    }


?>