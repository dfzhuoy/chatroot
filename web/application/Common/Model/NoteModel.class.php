<?php
namespace Common\Model;
use Common\Model\CommonModel;
class NoteModel extends CommonModel{


    public function __construct(){
        parent::__construct();

    }
	//自动验证
	protected $_validate = array(
		//array(验证字段,验证规则,错误提示,验证条件,附加规则,验证时间)
		array('message', 'require', '内容不能为空！', 1, 'reggex', 3),
    );
	
	protected function _before_write(&$data) {
		parent::_before_write($data);
	}

	/**
	 * @param $room_id
	 * @param $teacher_id
	 * @param int $page
	 * @param int $limit_num
	 * @return array
	 * 历史
	 */
	public function historyList($uid,$page=1,$limit_num=20){
		$pageinfo = [
			'page' => $page,
			'limit_num' => $limit_num,
		];
		if(empty($uid)){
			return false;
		}
		$params['uid|to'] = $uid;
		$params['status'] = 1;
		$pageinfo['total'] = $this->where($params)->count();

		$pageinfo = page_info($pageinfo['total'],$pageinfo['page'],$pageinfo['limit_num']);

		$callList = $this
			->cache(true,10)
			->where($params)
			->page($page, $pageinfo['limit_num'])
			->order('create_time desc')->select();

		foreach($callList as $k=>&$v){
			$v['data'] = $v['message'];
			$user = (new UsersModel())->getUserInfo($v['uid']);
			$v['user'] = [
				'uid' => $v['uid'],
				'avatar' => sp_get_user_avatar_url($user['head_portrait']),
				'name' => $user['user_nicename']?$user['user_nicename']:$user['user_login'],
			];
			$to_user = (new UsersModel())->getUserInfo($v['to']);
			$v['to_user'] = [
				'uid' => $v['to'],
				'avatar' => sp_get_user_avatar_url($user['head_portrait']),
				'name' => $to_user['user_nicename']?$to_user['user_nicename']:$to_user['user_login'],
			];
			if($v['note_uid']){
				$note_user = (new UsersModel())->getUserInfo($v['note_uid']);
				$v['note_user'] = [
					'uid' => $v['note_uid'],
					'avatar' => sp_get_user_avatar_url($user['head_portrait']),
					'name' => $note_user['user_nicename']?$note_user['user_nicename']:$note_user['user_login'],
				];
			}
		}

		return ['list' => $callList,'pageinfo' => $pageinfo];
	}

}