<?php
namespace Common\Model;
use Common\Model\CommonModel;
class CallModel extends CommonModel{


    public function __construct(){
        parent::__construct();
    }
	//自动验证
	protected $_validate = array(
		//array(验证字段,验证规则,错误提示,验证条件,附加规则,验证时间)
	);
	
	protected function _before_write(&$data) {
		parent::_before_write($data);
	}

	public function getCategory(){
		return $arr = [
			'1'=>['name'=>'买入建仓', 'value'=>'1'],
			'2'=>['name'=>'卖出建仓', 'value'=>'2'],
		];
	}

	public function getPriceCategory(){
		return $arr = [
			'1'=>['name'=>'市价', 'value'=>'1'],
			'2'=>['name'=>'指价', 'value'=>'2'],
		];
	}

	public function getPostion(){
		return $arr = [
			'100'=>['name'=>'满仓', 'value'=>'100'],
			'90'=>['name'=>'九层仓', 'value'=>'90'],
			'80'=>['name'=>'八层仓', 'value'=>'80'],
			'70'=>['name'=>'七层仓', 'value'=>'70'],
			'60'=>['name'=>'六层仓', 'value'=>'60'],
			'50'=>['name'=>'五层仓', 'value'=>'50'],
			'40'=>['name'=>'四层仓', 'value'=>'40'],
			'30'=>['name'=>'三层仓', 'value'=>'30'],
			'20'=>['name'=>'二层仓', 'value'=>'20'],
			'10'=>['name'=>'一层仓', 'value'=>'10'],
		];
	}

	public function getDealVariety(){
		$arr = [];
		$row = (new ConfigModel())->getvalue('call_deal_variety');
		if(!empty($row)){
			foreach ($row as $item) {
				list($value,$name) = explode('|',$item);
				$arr[$value] = ['name'=>$name,'value'=>$value];
			}
		}
		return $arr;
	}

	/**
	 * 当前喊单，只有一个
	 * @param int $room_id
	 * @return array
	 */
	public function now($room_id = false){
		//开仓
		$result_kai = $this->cache(true,10)->where([
			//'room_id'=>$room_id,
			'if_close'  => -1, //开仓
			'call_category' => ['NEQ', 2],
			'create_time' => ['BETWEEN',[getweekday(5).' 00:00:00',date('Y-m-d') . ' 23:59:59']]
		])->order('create_time desc')->find();
		//平仓
		$result_ping = $this->cache(true,10)->where([
			//'room_id'=>$room_id,
			'if_close'  => 1, //平仓
			'call_category' => ['NEQ', 2],
			'create_time' => ['BETWEEN',[getweekday(5).' 00:00:00',date('Y-m-d') . ' 23:59:59']]
		])->order('close_time desc')->find();

		if($result_ping['close_time'] > $result_kai['create_time']){
			$result = $result_ping;
		}else{
			$result = $result_kai;
		}

		$usermodel = new UsersModel();
		$teacher = $usermodel->getUserInfo($result['teacher_id']);

		if($result){
			$result['teacher'] = [
				'uid' => $result['teacher_id'],
				'avatar' => sp_get_user_avatar_url($teacher['head_portrait']),
				'name' => $teacher['user_nicename']?$teacher['user_nicename']:$teacher['user_login'],
			];
			$category = $this->getCategory();
			$deal_variety = $this->getDealVariety();
			$position = $this->getPostion();
			$price_category = $this->getPriceCategory();

			$result['category_name'] = $category[$result['category']]['name'];
			$result['deal_variety_name'] = $deal_variety[$result['deal_variety']]['name'];
			$result['position_name'] = $position[$result['position']]['name'];
			$result['price_category_name'] = $price_category[$result['price_category']]['name'];
			$result['close_price_category_name'] = $price_category[$result['close_price_category']]['name'];

		}


		return $result;
	}
	/**
	 * @param $room_id
	 * @param $teacher_id
	 * @param int $page
	 * @param int $limit_num
	 * @return array
	 * 喊单历史
	 */
	public function historyList($teacher_id= false,$room_id=false,$page=1,$limit_num=20){
		$pageinfo = [
			'page' => $page,
			'limit_num' => $limit_num,
		];
		if($teacher_id){
			$params['teacher_id'] = $teacher_id;
		}
		/*//获取上周五 8点到现在的数据
		//$params['create_time'] = ['EGT',date('Y-m-d',strtotime('-6 days')).' 00:00:00'];
		$params['create_time'] = ['EGT',getweekday(5).' 20:00:00'];*/
		$params['call_category'] = ['NEQ', 2];

		$pageinfo['total'] = $this->where($params)->count();

		$pageinfo = page_info($pageinfo['total'],$pageinfo['page'],$pageinfo['limit_num']);

		$callList = $this->where($params)
			->page($page, $pageinfo['limit_num'])
			->order('create_time desc')->select();
		$category = $this->getCategory();
		$deal_variety = $this->getDealVariety();
		$position = $this->getPostion();
		$price_category = $this->getPriceCategory();

		foreach($callList as $k=>&$v){
			$teacher = (new UsersModel())->getUserInfo($v['teacher_id']);
			$v['teacher'] = [
				'uid' => $v['teacher_id'],
				'avatar' => $teacher['head_portrait'],
				'name' => $teacher['user_nicename'],
			];
			$v['category_name'] = $category[$v['category']]['name'];
			$v['deal_variety_name'] = $deal_variety[$v['deal_variety']]['name'];
			$v['position_name'] = $position[$v['position']]['name'];
			$v['price_category_name'] = $price_category[$v['price_category']]['name'];
			$v['close_price_category_name'] = $price_category[$v['close_price_category']]['name'];
		}

		return ['list' => $callList,'pageinfo' => $pageinfo];
	}
}