<?php
namespace Common\Model;
use Common\Model\CommonModel;
class ImportantModel extends CommonModel{


    public function __construct(){
        parent::__construct();

    }
	//自动验证
	protected $_validate = array(
		//array(验证字段,验证规则,错误提示,验证条件,附加规则,验证时间)
		array('content', 'require', '经济数据内容不能为空！', 1, 'reggex', 3),
    );
	
	protected function _before_write(&$data) {
		parent::_before_write($data);
	}
}