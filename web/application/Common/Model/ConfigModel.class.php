<?php
namespace Common\Model;
use Common\Model\CommonModel;
class ConfigModel extends  CommonModel
{

    //自动验证
    protected $_validate = array(
        //array(验证字段,验证规则,错误提示,验证条件,附加规则,验证时间)
        array('key', 'require', 'key不能为空！', 1, 'regex', 3),
        array('value', 'require', '内容不能为空！', 1, 'regex', 3),

    );

    protected function _before_write(&$data)
    {
        parent::_before_write($data);
    }

    /**
     * @param $key
     * @return array|bool
     * 获取key对应的value值
     */
    public function getvalue($key){
        if(empty($key)){
            $this->error = 'key值不存在';
            return false;
        }
        $map['status'] = 1;
        $map['key'] = trim($key);
        $value_str = $this->cache(true,10)->where($map)->getField('value');
        $value = explode("\n",$value_str);
        return $value;
    }
}
