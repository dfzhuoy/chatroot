<?php
namespace Common\Model;
use Common\Model\CommonModel;
class ChatModel extends CommonModel{


    public function __construct(){
        parent::__construct();

    }
	//自动验证
	protected $_validate = array(
		//array(验证字段,验证规则,错误提示,验证条件,附加规则,验证时间)
		array('message', 'require', '内容不能为空！', 1, 'reggex', 3),
    );
	
	protected function _before_write(&$data) {
		parent::_before_write($data);
	}

	/**
	 * @param $room_id
	 * @param $teacher_id
	 * @param int $page
	 * @param int $limit_num
	 * @return array
	 * 历史
	 */
	public function historyList($user_type = 3,$page=1,$limit_num=20){
		$pageinfo = [
			'page' => $page,
			'limit_num' => $limit_num,
		];
		$params['status'] = 1;
		switch($user_type){
			case 2:
				$params['chat_users.user_type'] = 2;
				break;
			case 3:
				$params['chat_users.user_type'] = 3;
				break;
		}

		$pageinfo['total'] = $this->join('chat_users on chat_users.id = chat_chat.uid')->where($params)->count();

		$pageinfo = page_info($pageinfo['total'],$pageinfo['page'],$pageinfo['limit_num']);

		$callList = $this
			->cache(true,10)
			->join('chat_users on chat_users.id = chat_chat.uid')
			->where($params)
			->page($page, $pageinfo['limit_num'])
			->field('chat_chat.*')
			->order('chat_chat.create_time desc')->select();

		foreach($callList as $k=>&$v){
			$v['data'] = $v['message'];
			$user = (new UsersModel())->getUserInfo($v['uid']);
			$v['user'] = [
				'uid' => $v['uid'],
				'avatar' => sp_get_user_avatar_url($user['head_portrait']),
				'name' => $user['user_nicename']?$user['user_nicename']:$user['user_login'],
				'is_teacher'    => (new UsersModel())->isTeacher($v['uid']),
			];
			if($v['reply_uid']){
				$user = (new UsersModel())->getUserInfo($v['reply_uid']);
				$v['reply_user'] = [
					'uid' => $v['reply_uid'],
					'avatar' => sp_get_user_avatar_url($user['head_portrait']),
					'name' => $user['user_nicename']?$user['user_nicename']:$user['user_login'],
					'is_teacher'    => (new UsersModel())->isTeacher($v['reply_uid']),
				];
			}
		}

		return ['list' => $callList,'pageinfo' => $pageinfo];
	}
}