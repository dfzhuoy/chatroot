<?php

/**
 * @param $optionsArr
 * @param string $default_option
 * @param bool $need_html_controls
 * @return string
 * html选择器
 */
    function html_select($optionsArr ,$value,$name,$default_option = '',$need_html_controls = true,$id_name='',$multiple = false){
        if(empty($optionsArr) || empty($name)){
            return false;
        }
        $options_html = $default_option ? "<option value=''>{$default_option}</option>" : '';
        foreach ($optionsArr as $k=>$v) {
            $selected_html = '';
            if(is_array($value)){
                if(in_array($v['value'], $value)){
                    $selected_html = 'selected';
                }
            }else{
                if($v['value'] == $value){
                    $selected_html = 'selected';
                }
            }
            $options_html .= "<option value='{$v['value']}' {$selected_html}>{$v['name']}</option>";
        }

        if(empty($id_name)){
            $id_name = $name;
        }
        $multiple_html = '';
        if($multiple){
            $multiple_html = 'multiple="multiple"';
        }
        if($need_html_controls){
            $html = <<<EOT
            <div class="controls">
                    <select name="{$name}" id="{$id_name}" {$multiple_html}>
                        {$options_html}
                    </select>
                </div>
EOT;
        }else{
            $html = <<<EOT
                <select name="{$name}" id="{$id_name}" {$multiple_html}>
                    {$options_html}
                </select>
EOT;
        }

        return $html;

    }


/**
 * @param $uid
 * 显示用户
 */
    function showStudentName($uid,$str=''){
        $user = new \Common\Model\StudentModel();
        if($uid){
            echo '<div class="row text-center"><h3>'.$user->getName($uid).$str.'</h3></div>';
        }
    }


