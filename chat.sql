/*
Navicat MySQL Data Transfer

Source Server         : 【gd】阿里云
Source Server Version : 50616
Source Host           : rm-bp19ex01082a314sy.mysql.rds.aliyuncs.com:3306
Source Database       : chat

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2016-07-08 15:24:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for chat_ad
-- ----------------------------
DROP TABLE IF EXISTS `chat_ad`;
CREATE TABLE `chat_ad` (
  `ad_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '广告id',
  `ad_name` varchar(255) NOT NULL COMMENT '广告名称',
  `ad_content` text COMMENT '广告内容',
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1显示，0不显示',
  PRIMARY KEY (`ad_id`),
  KEY `ad_name` (`ad_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of chat_ad
-- ----------------------------

-- ----------------------------
-- Table structure for chat_asset
-- ----------------------------
DROP TABLE IF EXISTS `chat_asset`;
CREATE TABLE `chat_asset` (
  `aid` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户 id',
  `key` varchar(50) NOT NULL COMMENT '资源 key',
  `filename` varchar(50) DEFAULT NULL COMMENT '文件名',
  `filesize` int(11) DEFAULT NULL COMMENT '文件大小,单位Byte',
  `filepath` varchar(200) NOT NULL COMMENT '文件路径，相对于 upload 目录，可以为 url',
  `uploadtime` int(11) NOT NULL COMMENT '上传时间',
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1：可用，0：删除，不可用',
  `meta` text COMMENT '其它详细信息，JSON格式',
  `suffix` varchar(50) DEFAULT NULL COMMENT '文件后缀名，不包括点',
  `download_times` int(11) NOT NULL DEFAULT '0' COMMENT '下载次数',
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='资源表';

-- ----------------------------
-- Records of chat_asset
-- ----------------------------

-- ----------------------------
-- Table structure for chat_auth_access
-- ----------------------------
DROP TABLE IF EXISTS `chat_auth_access`;
CREATE TABLE `chat_auth_access` (
  `role_id` mediumint(8) unsigned NOT NULL COMMENT '角色',
  `rule_name` varchar(255) NOT NULL COMMENT '规则唯一英文标识,全小写',
  `type` varchar(30) DEFAULT NULL COMMENT '权限规则分类，请加应用前缀,如admin_',
  KEY `role_id` (`role_id`),
  KEY `rule_name` (`rule_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='权限授权表';

-- ----------------------------
-- Records of chat_auth_access
-- ----------------------------
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/menu/default', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/navcat/default1', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/nav/index', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/nav/listorders', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/nav/delete', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/nav/edit', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/nav/edit_post', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/nav/add', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/nav/add_post', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/navcat/index', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/navcat/delete', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/navcat/edit', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/navcat/edit_post', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/navcat/add', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/navcat/add_post', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/menu/index', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/menu/add', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/menu/add_post', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/menu/listorders', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/menu/export_menu', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/menu/edit', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/menu/edit_post', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/menu/delete', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/menu/lists', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/setting/default', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/setting/userdefault', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/user/userinfo', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/user/userinfo_post', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/setting/password', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/setting/password_post', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/user/avatar', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/zbs/default', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/important/index', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/call/index', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/live/index', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/zhanfa/index', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/word/index', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/call/add', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/call/ping', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/word/add', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/word/edit', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/word/del', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/zhanfa/add', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/zhanfa/del', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/chat/index', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/important/add', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/important/edit', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/important/del', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/chat/del', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/call/edit', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/call/del', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/config/index', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/config/add', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/config/editor', 'admin_url');
INSERT INTO `chat_auth_access` VALUES ('2', 'admin/config/del', 'admin_url');

-- ----------------------------
-- Table structure for chat_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `chat_auth_rule`;
CREATE TABLE `chat_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键',
  `module` varchar(20) NOT NULL COMMENT '规则所属module',
  `type` varchar(30) NOT NULL DEFAULT '1' COMMENT '权限规则分类，请加应用前缀,如admin_',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  `param` varchar(255) DEFAULT NULL COMMENT '额外url参数',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '规则中文描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效(0:无效,1:有效)',
  `condition` varchar(300) NOT NULL DEFAULT '' COMMENT '规则附加条件',
  PRIMARY KEY (`id`),
  KEY `module` (`module`,`status`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='权限规则表';

-- ----------------------------
-- Records of chat_auth_rule
-- ----------------------------
INSERT INTO `chat_auth_rule` VALUES ('1', 'Admin', 'admin_url', 'admin/content/default', null, '内容管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('2', 'Api', 'admin_url', 'api/guestbookadmin/index', null, '所有留言', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('3', 'Api', 'admin_url', 'api/guestbookadmin/delete', null, '删除网站留言', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('4', 'Comment', 'admin_url', 'comment/commentadmin/index', null, '评论管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('5', 'Comment', 'admin_url', 'comment/commentadmin/delete', null, '删除评论', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('6', 'Comment', 'admin_url', 'comment/commentadmin/check', null, '评论审核', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('7', 'Portal', 'admin_url', 'portal/adminpost/index', null, '文章管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('8', 'Portal', 'admin_url', 'portal/adminpost/listorders', null, '文章排序', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('9', 'Portal', 'admin_url', 'portal/adminpost/top', null, '文章置顶', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('10', 'Portal', 'admin_url', 'portal/adminpost/recommend', null, '文章推荐', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('11', 'Portal', 'admin_url', 'portal/adminpost/move', null, '批量移动', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('12', 'Portal', 'admin_url', 'portal/adminpost/check', null, '文章审核', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('13', 'Portal', 'admin_url', 'portal/adminpost/delete', null, '删除文章', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('14', 'Portal', 'admin_url', 'portal/adminpost/edit', null, '编辑文章', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('15', 'Portal', 'admin_url', 'portal/adminpost/edit_post', null, '提交编辑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('16', 'Portal', 'admin_url', 'portal/adminpost/add', null, '添加文章', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('17', 'Portal', 'admin_url', 'portal/adminpost/add_post', null, '提交添加', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('18', 'Portal', 'admin_url', 'portal/adminterm/index', null, '分类管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('19', 'Portal', 'admin_url', 'portal/adminterm/listorders', null, '文章分类排序', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('20', 'Portal', 'admin_url', 'portal/adminterm/delete', null, '删除分类', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('21', 'Portal', 'admin_url', 'portal/adminterm/edit', null, '编辑分类', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('22', 'Portal', 'admin_url', 'portal/adminterm/edit_post', null, '提交编辑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('23', 'Portal', 'admin_url', 'portal/adminterm/add', null, '添加分类', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('24', 'Portal', 'admin_url', 'portal/adminterm/add_post', null, '提交添加', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('25', 'Portal', 'admin_url', 'portal/adminpage/index', null, '页面管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('26', 'Portal', 'admin_url', 'portal/adminpage/listorders', null, '页面排序', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('27', 'Portal', 'admin_url', 'portal/adminpage/delete', null, '删除页面', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('28', 'Portal', 'admin_url', 'portal/adminpage/edit', null, '编辑页面', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('29', 'Portal', 'admin_url', 'portal/adminpage/edit_post', null, '提交编辑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('30', 'Portal', 'admin_url', 'portal/adminpage/add', null, '添加页面', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('31', 'Portal', 'admin_url', 'portal/adminpage/add_post', null, '提交添加', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('32', 'Admin', 'admin_url', 'admin/recycle/default', null, '回收站', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('33', 'Portal', 'admin_url', 'portal/adminpost/recyclebin', null, '文章回收', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('34', 'Portal', 'admin_url', 'portal/adminpost/restore', null, '文章还原', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('35', 'Portal', 'admin_url', 'portal/adminpost/clean', null, '彻底删除', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('36', 'Portal', 'admin_url', 'portal/adminpage/recyclebin', null, '页面回收', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('37', 'Portal', 'admin_url', 'portal/adminpage/clean', null, '彻底删除', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('38', 'Portal', 'admin_url', 'portal/adminpage/restore', null, '页面还原', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('39', 'Admin', 'admin_url', 'admin/extension/default', null, '扩展工具', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('40', 'Admin', 'admin_url', 'admin/backup/default', null, '备份管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('41', 'Admin', 'admin_url', 'admin/backup/restore', null, '数据还原', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('42', 'Admin', 'admin_url', 'admin/backup/index', null, '数据备份', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('43', 'Admin', 'admin_url', 'admin/backup/index_post', null, '提交数据备份', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('44', 'Admin', 'admin_url', 'admin/backup/download', null, '下载备份', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('45', 'Admin', 'admin_url', 'admin/backup/del_backup', null, '删除备份', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('46', 'Admin', 'admin_url', 'admin/backup/import', null, '数据备份导入', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('47', 'Admin', 'admin_url', 'admin/plugin/index', null, '插件管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('48', 'Admin', 'admin_url', 'admin/plugin/toggle', null, '插件启用切换', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('49', 'Admin', 'admin_url', 'admin/plugin/setting', null, '插件设置', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('50', 'Admin', 'admin_url', 'admin/plugin/setting_post', null, '插件设置提交', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('51', 'Admin', 'admin_url', 'admin/plugin/install', null, '插件安装', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('52', 'Admin', 'admin_url', 'admin/plugin/uninstall', null, '插件卸载', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('53', 'Admin', 'admin_url', 'admin/slide/default', null, '幻灯片', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('54', 'Admin', 'admin_url', 'admin/slide/index', null, '幻灯片管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('55', 'Admin', 'admin_url', 'admin/slide/listorders', null, '幻灯片排序', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('56', 'Admin', 'admin_url', 'admin/slide/toggle', null, '幻灯片显示切换', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('57', 'Admin', 'admin_url', 'admin/slide/delete', null, '删除幻灯片', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('58', 'Admin', 'admin_url', 'admin/slide/edit', null, '编辑幻灯片', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('59', 'Admin', 'admin_url', 'admin/slide/edit_post', null, '提交编辑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('60', 'Admin', 'admin_url', 'admin/slide/add', null, '添加幻灯片', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('61', 'Admin', 'admin_url', 'admin/slide/add_post', null, '提交添加', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('62', 'Admin', 'admin_url', 'admin/slidecat/index', null, '幻灯片分类', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('63', 'Admin', 'admin_url', 'admin/slidecat/delete', null, '删除分类', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('64', 'Admin', 'admin_url', 'admin/slidecat/edit', null, '编辑分类', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('65', 'Admin', 'admin_url', 'admin/slidecat/edit_post', null, '提交编辑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('66', 'Admin', 'admin_url', 'admin/slidecat/add', null, '添加分类', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('67', 'Admin', 'admin_url', 'admin/slidecat/add_post', null, '提交添加', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('68', 'Admin', 'admin_url', 'admin/ad/index', null, '网站广告', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('69', 'Admin', 'admin_url', 'admin/ad/toggle', null, '广告显示切换', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('70', 'Admin', 'admin_url', 'admin/ad/delete', null, '删除广告', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('71', 'Admin', 'admin_url', 'admin/ad/edit', null, '编辑广告', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('72', 'Admin', 'admin_url', 'admin/ad/edit_post', null, '提交编辑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('73', 'Admin', 'admin_url', 'admin/ad/add', null, '添加广告', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('74', 'Admin', 'admin_url', 'admin/ad/add_post', null, '提交添加', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('75', 'Admin', 'admin_url', 'admin/link/index', null, '友情链接', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('76', 'Admin', 'admin_url', 'admin/link/listorders', null, '友情链接排序', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('77', 'Admin', 'admin_url', 'admin/link/toggle', null, '友链显示切换', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('78', 'Admin', 'admin_url', 'admin/link/delete', null, '删除友情链接', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('79', 'Admin', 'admin_url', 'admin/link/edit', null, '编辑友情链接', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('80', 'Admin', 'admin_url', 'admin/link/edit_post', null, '提交编辑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('81', 'Admin', 'admin_url', 'admin/link/add', null, '添加友情链接', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('82', 'Admin', 'admin_url', 'admin/link/add_post', null, '提交添加', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('83', 'Api', 'admin_url', 'api/oauthadmin/setting', null, '第三方登陆', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('84', 'Api', 'admin_url', 'api/oauthadmin/setting_post', null, '提交设置', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('85', 'Admin', 'admin_url', 'admin/menu/default', null, '菜单管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('86', 'Admin', 'admin_url', 'admin/navcat/default1', null, '前台菜单', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('87', 'Admin', 'admin_url', 'admin/nav/index', null, '菜单管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('88', 'Admin', 'admin_url', 'admin/nav/listorders', null, '前台导航排序', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('89', 'Admin', 'admin_url', 'admin/nav/delete', null, '删除菜单', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('90', 'Admin', 'admin_url', 'admin/nav/edit', null, '编辑菜单', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('91', 'Admin', 'admin_url', 'admin/nav/edit_post', null, '提交编辑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('92', 'Admin', 'admin_url', 'admin/nav/add', null, '添加菜单', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('93', 'Admin', 'admin_url', 'admin/nav/add_post', null, '提交添加', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('94', 'Admin', 'admin_url', 'admin/navcat/index', null, '菜单分类', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('95', 'Admin', 'admin_url', 'admin/navcat/delete', null, '删除分类', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('96', 'Admin', 'admin_url', 'admin/navcat/edit', null, '编辑分类', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('97', 'Admin', 'admin_url', 'admin/navcat/edit_post', null, '提交编辑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('98', 'Admin', 'admin_url', 'admin/navcat/add', null, '添加分类', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('99', 'Admin', 'admin_url', 'admin/navcat/add_post', null, '提交添加', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('100', 'Admin', 'admin_url', 'admin/menu/index', null, '后台菜单', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('101', 'Admin', 'admin_url', 'admin/menu/add', null, '添加菜单', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('102', 'Admin', 'admin_url', 'admin/menu/add_post', null, '提交添加', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('103', 'Admin', 'admin_url', 'admin/menu/listorders', null, '后台菜单排序', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('104', 'Admin', 'admin_url', 'admin/menu/export_menu', null, '菜单备份', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('105', 'Admin', 'admin_url', 'admin/menu/edit', null, '编辑菜单', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('106', 'Admin', 'admin_url', 'admin/menu/edit_post', null, '提交编辑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('107', 'Admin', 'admin_url', 'admin/menu/delete', null, '删除菜单', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('108', 'Admin', 'admin_url', 'admin/menu/lists', null, '所有菜单', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('109', 'Admin', 'admin_url', 'admin/setting/default', null, '设置', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('110', 'Admin', 'admin_url', 'admin/setting/userdefault', null, '个人信息', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('111', 'Admin', 'admin_url', 'admin/user/userinfo', null, '修改信息', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('112', 'Admin', 'admin_url', 'admin/user/userinfo_post', null, '修改信息提交', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('113', 'Admin', 'admin_url', 'admin/setting/password', null, '修改密码', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('114', 'Admin', 'admin_url', 'admin/setting/password_post', null, '提交修改', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('115', 'Admin', 'admin_url', 'admin/setting/site', null, '网站信息', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('116', 'Admin', 'admin_url', 'admin/setting/site_post', null, '提交修改', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('117', 'Admin', 'admin_url', 'admin/route/index', null, '路由列表', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('118', 'Admin', 'admin_url', 'admin/route/add', null, '路由添加', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('119', 'Admin', 'admin_url', 'admin/route/add_post', null, '路由添加提交', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('120', 'Admin', 'admin_url', 'admin/route/edit', null, '路由编辑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('121', 'Admin', 'admin_url', 'admin/route/edit_post', null, '路由编辑提交', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('122', 'Admin', 'admin_url', 'admin/route/delete', null, '路由删除', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('123', 'Admin', 'admin_url', 'admin/route/ban', null, '路由禁止', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('124', 'Admin', 'admin_url', 'admin/route/open', null, '路由启用', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('125', 'Admin', 'admin_url', 'admin/route/listorders', null, '路由排序', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('126', 'Admin', 'admin_url', 'admin/mailer/default', null, '邮箱配置', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('127', 'Admin', 'admin_url', 'admin/mailer/index', null, 'SMTP配置', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('128', 'Admin', 'admin_url', 'admin/mailer/index_post', null, '提交配置', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('129', 'Admin', 'admin_url', 'admin/mailer/active', null, '邮件模板', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('130', 'Admin', 'admin_url', 'admin/mailer/active_post', null, '提交模板', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('131', 'Admin', 'admin_url', 'admin/setting/clearcache', null, '清除缓存', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('132', 'User', 'admin_url', 'user/indexadmin/default', null, '用户管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('133', 'User', 'admin_url', 'user/indexadmin/default1', null, '用户组', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('134', 'User', 'admin_url', 'user/indexadmin/index', null, '本站用户', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('135', 'User', 'admin_url', 'user/indexadmin/ban', null, '拉黑会员', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('136', 'User', 'admin_url', 'user/indexadmin/cancelban', null, '启用会员', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('137', 'User', 'admin_url', 'user/oauthadmin/index', null, '第三方用户', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('138', 'User', 'admin_url', 'user/oauthadmin/delete', null, '第三方用户解绑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('139', 'User', 'admin_url', 'user/indexadmin/default3', null, '管理组', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('140', 'Admin', 'admin_url', 'admin/rbac/index', null, '角色管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('141', 'Admin', 'admin_url', 'admin/rbac/member', null, '成员管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('142', 'Admin', 'admin_url', 'admin/rbac/authorize', null, '权限设置', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('143', 'Admin', 'admin_url', 'admin/rbac/authorize_post', null, '提交设置', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('144', 'Admin', 'admin_url', 'admin/rbac/roleedit', null, '编辑角色', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('145', 'Admin', 'admin_url', 'admin/rbac/roleedit_post', null, '提交编辑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('146', 'Admin', 'admin_url', 'admin/rbac/roledelete', null, '删除角色', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('147', 'Admin', 'admin_url', 'admin/rbac/roleadd', null, '添加角色', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('148', 'Admin', 'admin_url', 'admin/rbac/roleadd_post', null, '提交添加', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('149', 'Admin', 'admin_url', 'admin/user/index', null, '管理员', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('150', 'Admin', 'admin_url', 'admin/user/delete', null, '删除管理员', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('151', 'Admin', 'admin_url', 'admin/user/edit', null, '管理员编辑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('152', 'Admin', 'admin_url', 'admin/user/edit_post', null, '编辑提交', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('153', 'Admin', 'admin_url', 'admin/user/add', null, '管理员添加', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('154', 'Admin', 'admin_url', 'admin/user/add_post', null, '添加提交', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('155', 'Admin', 'admin_url', 'admin/plugin/update', null, '插件更新', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('156', 'Admin', 'admin_url', 'admin/storage/index', null, '文件存储', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('157', 'Admin', 'admin_url', 'admin/storage/setting_post', null, '文件存储设置提交', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('158', 'Admin', 'admin_url', 'admin/slide/ban', null, '禁用幻灯片', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('159', 'Admin', 'admin_url', 'admin/slide/cancelban', null, '启用幻灯片', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('160', 'Admin', 'admin_url', 'admin/user/ban', null, '禁用管理员', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('161', 'Admin', 'admin_url', 'admin/user/cancelban', null, '启用管理员', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('162', 'Admin', 'admin_url', 'admin/zbs/default', null, '直播室管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('163', 'Admin', 'admin_url', 'admin/important/index', null, '重要经济数据', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('164', 'Admin', 'admin_url', 'admin/call/index', null, '喊单管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('165', 'Admin', 'admin_url', 'admin/config/index', null, '配置管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('166', 'Admin', 'admin_url', 'admin/user/teacher', null, '老师管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('167', 'Admin', 'admin_url', 'admin/live/index', null, '聊天', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('168', 'Admin', 'admin_url', 'admin/zhanfa/index', null, '战法列表', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('169', 'Admin', 'admin_url', 'admin/word/index', null, '敏感词管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('170', 'Admin', 'admin_url', 'admin/call/add', null, '添加喊单', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('171', 'Admin', 'admin_url', 'admin/call/ping', null, '喊单平仓', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('172', 'Admin', 'admin_url', 'admin/word/add', null, '敏感词添加', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('173', 'Admin', 'admin_url', 'admin/word/edit', null, '敏感词编辑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('174', 'Admin', 'admin_url', 'admin/word/del', null, '敏感词隐藏', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('175', 'Admin', 'admin_url', 'admin/zhanfa/add', null, '添加战法', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('176', 'Admin', 'admin_url', 'admin/zhanfa/del', null, '隐藏战法', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('177', 'Admin', 'admin_url', 'admin/config/add', null, '添加配置', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('178', 'Admin', 'admin_url', 'admin/config/edit', null, '编辑配置', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('179', 'Admin', 'admin_url', 'admin/config/del', null, '删除配置', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('180', 'Admin', 'admin_url', 'admin/user/avatar', null, '设置头像', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('181', 'Admin', 'admin_url', 'admin/chat/index', null, '消息管理', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('182', 'Admin', 'admin_url', 'admin/important/add', null, '重要经济数据添加', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('183', 'Admin', 'admin_url', 'admin/important/edit', null, '重要经济数据修改', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('184', 'Admin', 'admin_url', 'admin/important/del', null, '重要经济数据删除', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('185', 'Admin', 'admin_url', 'admin/chat/del', null, '消息删除', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('186', 'Admin', 'admin_url', 'admin/call/edit', null, '喊单编辑', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('187', 'Admin', 'admin_url', 'admin/call/del', null, '喊单删除', '1', '');
INSERT INTO `chat_auth_rule` VALUES ('188', 'Admin', 'admin_url', 'admin/config/editor', null, '编辑配置', '1', '');

-- ----------------------------
-- Table structure for chat_call
-- ----------------------------
DROP TABLE IF EXISTS `chat_call`;
CREATE TABLE `chat_call` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` tinyint(1) NOT NULL DEFAULT '1' COMMENT '喊单类型 1=> 买入建仓  2=> 卖出建仓',
  `price_category` tinyint(1) NOT NULL DEFAULT '1' COMMENT '价格类型 1  => 市价 2 => 指价',
  `price` varchar(20) NOT NULL DEFAULT '0' COMMENT '价格',
  `position` int(5) NOT NULL DEFAULT '1' COMMENT '仓位 100 => 满仓  ， 60 => 六成仓位',
  `stop_profit_price` varchar(20) NOT NULL DEFAULT '0' COMMENT '止盈价格',
  `stop_loss_price` varchar(20) NOT NULL DEFAULT '0' COMMENT '止损价格',
  `deal_variety` tinyint(1) NOT NULL DEFAULT '1' COMMENT '交易品种：（1 => 大庆原油 2 => 天津原油）',
  `room_id` int(11) NOT NULL DEFAULT '0' COMMENT '直播室ID',
  `teacher_id` int(11) NOT NULL DEFAULT '0' COMMENT '老师ID',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` varchar(500) DEFAULT NULL,
  `if_close` tinyint(1) NOT NULL DEFAULT '-1' COMMENT '是否平仓 -1 未平仓  1 平仓',
  `close_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '平仓时间',
  `close_price_category` tinyint(1) unsigned DEFAULT '1' COMMENT '平仓价格类型 1  => 市价 2 => 指价',
  `close_price` varchar(20) DEFAULT '0' COMMENT '平仓价格',
  `close_remark` varchar(500) DEFAULT '' COMMENT '平仓备注',
  `close_teacher_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '平仓老师ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of chat_call
-- ----------------------------
INSERT INTO `chat_call` VALUES ('16', '1', '1', '330', '20', '336', '325', '1', '0', '10000000', '2016-06-22 11:23:50', '创新高，继续短多操作', '1', '2016-06-22 15:07:36', '1', '332.4', '上方压力，先平仓。稍后找机会再操作', '10000000');
INSERT INTO `chat_call` VALUES ('17', '2', '1', '330', '40', '322', '337', '1', '0', '10000000', '2016-06-22 22:30:22', 'EIA数据做空', '1', '2016-06-23 08:58:33', '1', '322', '达到止盈位，顺利止盈', '10000000');
INSERT INTO `chat_call` VALUES ('18', '1', '1', '325.2', '20', '319', '329', '1', '0', '10000001', '2016-06-23 15:14:04', '英国公投开后，避嫌情绪有所上升', '1', '2016-06-23 15:16:07', '1', '1', '错单重新填', '10000000');
INSERT INTO `chat_call` VALUES ('19', '2', '1', '325', '30', '318', '330', '1', '0', '10000001', '2016-06-23 15:19:03', '反弹到位，做空', '1', '2016-06-24 09:11:50', '1', '318', '顺利止盈，止盈离场', '10000000');
INSERT INTO `chat_call` VALUES ('20', '1', '1', '328.3', '30', '333', '325', '1', '0', '8', '2016-06-23 20:12:14', '最近退欧公投民调结果，留欧阵营占上风，市场避险情绪缓和。', '1', '2016-06-24 09:40:06', '1', '325', '脱欧占上风，先止损。', '10000000');
INSERT INTO `chat_call` VALUES ('21', '2', '1', '314.2', '30', '300', '319.5', '1', '0', '10000000', '2016-06-24 14:17:03', '原油出现小反弹，继续顺势做空，止损止盈带好，控制好仓位。', '1', '2016-06-27 08:46:17', '1', '310.2', '周末消息较多，建议空单先减仓获利大部分仓位，激进的可以留下小部分等止盈位', '10000000');
INSERT INTO `chat_call` VALUES ('22', '2', '1', '317.8', '40', '311.5', '322', '1', '0', '8', '2016-06-24 16:31:53', '空头格局，反弹受阻，顺势做空。', '1', '2016-06-24 21:43:15', '1', '316', '空头力量减弱，下方支撑较强，有盈利落袋为安。', '8');
INSERT INTO `chat_call` VALUES ('23', '2', '1', '316.6', '30', '310', '321', '1', '0', '8', '2016-06-27 15:39:54', '空头趋势，反弹受阻，顺势做空。', '1', '2016-06-27 20:12:12', '1', '310.5', '到达止盈位', '10000001');
INSERT INTO `chat_call` VALUES ('24', '2', '1', '309', '20', '305', '314', '1', '0', '10000001', '2016-06-27 22:06:52', '空头形势强劲，反弹无力，追空', '1', '2016-06-28 00:43:48', '1', '305', '到达止盈位', '10000001');
INSERT INTO `chat_call` VALUES ('25', '2', '1', '314.5', '40', '305', '319.9', '1', '0', '10000000', '2016-06-28 16:51:45', '反弹到压力位附近，布局空单，止损止盈带好。个人建议，仅供参考。', '1', '2016-06-29 09:14:00', '1', '319.9', '到达止损位，先离场', '10000103');
INSERT INTO `chat_call` VALUES ('26', '2', '1', '313.8', '20', '303', '319.2', '1', '0', '8', '2016-06-28 17:15:07', '小时图出现做空信号。', '1', '2016-06-28 22:45:09', '1', '312.5', '止盈先出', '10000001');
INSERT INTO `chat_call` VALUES ('27', '2', '1', '321.1', '30', '317.5', '324', '1', '0', '8', '2016-06-29 11:35:42', '日线图受中轨压制，上升走缓，预计有回调需要，短线做空。', '1', '2016-06-29 19:39:39', '1', '320.8', '多头形势，先离场', '10000001');
INSERT INTO `chat_call` VALUES ('28', '1', '1', '321', '20', '326', '318', '1', '0', '10000001', '2016-06-29 19:41:49', '多头形势，支撑位短多', '1', '2016-06-29 22:11:45', '1', '323.5', '获利先出，等待EIA数据公布', '10000001');
INSERT INTO `chat_call` VALUES ('29', '1', '1', '322', '40', '328', '317', '1', '0', '10000000', '2016-06-29 22:32:06', '数据利多，做多', '1', '2016-06-29 22:37:11', '1', '326.5', '行情不稳定，短多有利就走', '10000000');
INSERT INTO `chat_call` VALUES ('30', '2', '1', '327.8', '40', '320', '333.2', '1', '0', '10000000', '2016-06-30 15:15:43', '行情大幅回落，短线见顶。布局空单操作，止损止盈带好，个人建议仅供参考', '1', '2016-06-30 16:15:22', '1', '329.8', '行情走势非常诡异，空单先减仓。找准机会再布局。', '10000000');
INSERT INTO `chat_call` VALUES ('31', '2', '1', '328.8', '30', '322', '333.7', '1', '0', '8', '2016-06-30 16:41:40', '上方收330关键压力压制，上涨受阻，尝试短线空单，设好止盈止损。个人建议，仅供参考。', '1', '2016-06-30 20:24:28', '1', '326.5', '止盈先出', '10000001');
INSERT INTO `chat_call` VALUES ('32', '2', '1', '329.4', '20', '323', '334.4', '1', '0', '10000001', '2016-06-30 19:07:36', '压力位短线空单。个人建议，仅供参考。', '1', '2016-06-30 21:22:10', '1', '325.5', '获利先出', '10000001');
INSERT INTO `chat_call` VALUES ('33', '2', '1', '327', '20', '322.4', '332.4', '1', '0', '10000001', '2016-06-30 23:08:40', '反弹到位，短空。个人建议，仅供参考', '1', '2016-07-01 00:22:49', '1', '324.5', '获利先出', '10000001');
INSERT INTO `chat_call` VALUES ('34', '2', '1', '323.5', '40', '315', '328.9', '1', '0', '10000000', '2016-07-01 14:18:25', '昨天收阴，顺势做空。止损止盈带好。个人建议仅供参考。', '1', '2016-07-04 13:43:27', '1', '328.9', '行情短线还是偏强，空单先减仓。不破330之前，留下小部分仓位观察压力位是否会被突破', '10000000');
INSERT INTO `chat_call` VALUES ('35', '1', '1', '320.5', '20', '325.4', '316.4', '1', '0', '10000001', '2016-07-01 18:24:48', '支撑位，短线多单。', '1', '2016-07-01 21:54:00', '1', '323.5', '反弹都位，获利先出', '10000001');
INSERT INTO `chat_call` VALUES ('36', '2', '1', '321.5', '30', '310', '326.9', '1', '0', '8', '2016-07-01 20:58:27', '中期有见顶趋势，反弹无力，中线空单操作，止损止盈带好。个人建议，仅供参考。', '1', '2016-07-04 09:09:04', '1', '326.9', '止损离场', '10000001');
INSERT INTO `chat_call` VALUES ('37', '2', '1', '325.5', '20', '319.6', '330.4', '1', '0', '10000001', '2016-07-01 22:32:25', '反弹到位，短空（个人建议，仅供参考）', '1', '2016-07-04 22:13:42', '1', '327.6', '目前油价较为强势，离场观看', '10000001');
INSERT INTO `chat_call` VALUES ('38', '1', '1', '329', '30', '337', '324', '1', '0', '8', '2016-07-04 19:47:55', '油价反弹，短线看多。做单带好止盈止损。个人建议，仅供参考。', '1', '2016-07-05 09:24:22', '1', '324', '止损离场', '10000001');
INSERT INTO `chat_call` VALUES ('39', '1', '1', '324', '50', '330', '319', '1', '0', '10000000', '2016-07-05 12:05:21', '短线止跌，短多一把，有利润就走', '1', '2016-07-05 16:14:06', '1', '320', '原油是意外受到消息影响快速回落，多单先锁仓一半或者止损一半。', '10000000');
INSERT INTO `chat_call` VALUES ('40', '1', '1', '322.8', '20', '328', '317.4', '1', '0', '10000001', '2016-07-05 13:45:27', '短期支撑，轻仓短线多单，获利就跑（个人建议，仅供参考）', '1', '2016-07-05 22:19:30', '1', '317.4', '空头强势，318支撑被突破了，321上方的多单，超过两层仓，可以先锁仓一半。321下方的多单，可以先出来。', '8');
INSERT INTO `chat_call` VALUES ('41', '1', '1', '320.8', '30', '328', '317', '1', '0', '8', '2016-07-05 19:09:39', '下跌无力，短线博反弹，带好止盈止损（个人建议，仅供参考）', '1', '2016-07-05 22:18:32', '1', '317', '空头强势，318支撑被突破了，321上方的多单，超过两层仓，可以先锁仓一半。321下方的多单，可以先出来。', '8');
INSERT INTO `chat_call` VALUES ('42', '1', '1', '312.2', '30', '320', '307.2', '1', '0', '10000000', '2016-07-06 13:19:22', '止跌博取短多，个人建议仅供参考', '1', '2016-07-06 13:51:40', '1', '314.6', '行情开始反弹，但上方压力较大，先获利，回落再多', '10000000');
INSERT INTO `chat_call` VALUES ('43', '1', '1', '312.2', '30', '320', '307.2', '1', '0', '10000000', '2016-07-06 17:42:17', '短线站稳，继续短多，止损止盈带好。个人建议仅供参考。', '1', '2016-07-06 22:00:03', '1', '314.8', '短线反弹到位，获利先出！', '8');
INSERT INTO `chat_call` VALUES ('44', '1', '1', '320.8', '20', '325', '316', '1', '0', '10000001', '2016-07-07 11:37:23', '支撑位320附近，短线轻仓多单', '1', '2016-07-07 16:11:02', '1', '322', '短期压力位。获利先出', '10000001');
INSERT INTO `chat_call` VALUES ('45', '2', '1', '320.6', '30', '310', '325.9', '1', '0', '8', '2016-07-07 14:44:47', '日线级别看空，尝试布局中线空单，带好止盈止损。（个人建议，仅供参考）', '1', '2016-07-07 16:07:05', '1', '322', '短期压力位，获利先出', '10000001');
INSERT INTO `chat_call` VALUES ('46', '2', '1', '320.6', '30', '310', '325.9', '1', '0', '8', '2016-07-07 16:17:17', '日线级别看空，尝试布局中线空单，带好止盈止损。（个人建议，仅供参考）', '1', '2016-07-07 23:14:19', '1', '316.8', '下方支撑，获利先出。', '8');
INSERT INTO `chat_call` VALUES ('47', '2', '1', '323.5', '40', '315', '329', '1', '0', '10000000', '2016-07-07 17:33:11', '反弹到压力位，短空操作，止损止盈带好。个人建议仅供参考', '1', '2016-07-07 19:38:06', '1', '322.2', 'ADP数据马上公布，有利先走，稍后再做', '10000000');
INSERT INTO `chat_call` VALUES ('48', '2', '1', '322', '40', '312', '328', '1', '0', '10000000', '2016-07-07 23:00:28', '数据利空，做空。个人建议仅供参考。', '1', '2016-07-07 23:05:42', '1', '315', '下方到了短期支撑，先获利', '10000000');
INSERT INTO `chat_call` VALUES ('49', '1', '1', '310.4', '20', '316', '305.6', '1', '0', '10000001', '2016-07-07 23:49:47', '支撑位附近，短线多单', '1', '2016-07-08 01:47:53', '1', '305.6', '反弹无力', '8');
INSERT INTO `chat_call` VALUES ('50', '1', '1', '311.1', '20', '315', '308', '1', '0', '8', '2016-07-07 23:57:42', '短线博反弹，指引止损带好，个人建议，仅供参考。', '1', '2016-07-08 01:46:36', '1', '308', '反弹无力先出。', '8');
INSERT INTO `chat_call` VALUES ('51', '1', '1', '309.2', '40', '318', '304', '1', '0', '10000000', '2016-07-08 00:23:54', '到达支撑位，做多博反弹，个人建议仅供参考', '1', '2016-07-08 01:47:29', '1', '304', '反弹无力。', '8');
INSERT INTO `chat_call` VALUES ('52', '2', '1', '307.2', '40', '300', '312.6', '1', '0', '10000000', '2016-07-08 12:52:10', '反弹无力，继续顺势做空。止损止盈带好。个人建议仅供参考', '-1', '2016-07-08 12:52:10', '1', '0', '', '0');

-- ----------------------------
-- Table structure for chat_chat
-- ----------------------------
DROP TABLE IF EXISTS `chat_chat`;
CREATE TABLE `chat_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT 'text' COMMENT 'text文字，img图片，voice语音',
  `message` varchar(1000) DEFAULT NULL,
  `reply_uid` int(11) DEFAULT NULL,
  `reply_message` varchar(1000) DEFAULT NULL,
  `reply_time` timestamp NULL DEFAULT NULL COMMENT '回复内容创建时间',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1' COMMENT '状态：1，-1',
  `color` varchar(10) DEFAULT '#000000' COMMENT '文字颜色',
  `size` int(10) DEFAULT '14' COMMENT '文字大小',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=5218 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of chat_chat
-- ----------------------------
INSERT INTO `chat_chat` VALUES ('860', '7', 'text', '#9', null, null, '2016-06-22 10:04:25', '2016-06-22 10:04:25', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('861', '7', 'text', '大家好', null, null, '2016-06-22 10:05:01', '2016-06-22 10:05:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('862', '7', 'text', '哇哇哇哇', null, null, '2016-06-22 10:06:43', '2016-06-22 10:06:43', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('863', '10000000', 'text', '大家早上好', null, null, '2016-06-22 10:36:39', '2016-06-22 10:36:39', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('864', '10000000', 'text', '现在进行内测阶段', null, null, '2016-06-22 10:36:55', '2016-06-22 10:36:55', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('865', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160622/5769fabfa710d.jpg', null, null, '2016-06-22 10:41:03', '2016-06-22 10:41:03', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('866', '10000001', 'text', '各位朋友，大家好！', null, null, '2016-06-22 10:42:13', '2016-06-22 10:42:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('867', '10000001', 'text', '欢迎大家来到轩湛直播室', null, null, '2016-06-22 10:42:39', '2016-06-22 10:42:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('868', '10000103', 'text', '大家好', null, null, '2016-06-22 10:43:24', '2016-06-22 10:43:24', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('869', '10000103', 'text', '#9', null, null, '2016-06-22 10:44:11', '2016-06-22 10:44:11', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('870', '10000000', 'text', '你好', '7', '哇哇哇哇', '2016-06-22 10:44:54', '2016-06-22 10:44:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('871', '10000001', 'text', '#9', null, null, '2016-06-22 10:47:11', '2016-06-22 10:47:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('872', '10000103', 'text', '欢迎轩湛直播室的亲们', null, null, '2016-06-22 10:48:23', '2016-06-22 10:48:23', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('873', '10000015', 'text', '大家早上好', null, null, '2016-06-22 10:50:48', '2016-06-22 10:50:48', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('874', '10000103', 'text', '轩湛投资观察室', null, null, '2016-06-22 10:52:31', '2016-06-22 10:52:31', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('875', '10000001', 'text', '今日轩湛轩湛投资观察室开播了#9', null, null, '2016-06-22 10:54:21', '2016-06-22 10:54:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('876', '10000103', 'text', '大家早上好！传播投资智慧，见证财富增长，欢迎来到轩湛投资观察室！', null, null, '2016-06-22 11:05:12', '2016-06-22 11:05:12', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('877', '10000103', 'text', '#6#6', null, null, '2016-06-22 11:05:53', '2016-06-22 11:05:53', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('878', '10000103', 'text', '在西藏，再努力也烧不开一壶水，说明环境很重要；骑自行车，再努力也追不上宝马，说明平台很重要；一个人再有能力，也干不过一群人，说明团队很重要。#21', null, null, '2016-06-22 11:06:37', '2016-06-22 11:06:37', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('879', '10000103', 'text', '选择我们轩湛的精英分析师团队，让你的人生站上一个新高度，让你的财富稳步增长！', null, null, '2016-06-22 11:09:04', '2016-06-22 11:09:04', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('880', '10000084', 'text', '#1', null, null, '2016-06-22 11:10:57', '2016-06-22 11:10:57', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('881', '10000006', 'text', '#9', null, null, '2016-06-22 11:10:59', '2016-06-22 11:10:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('882', '10000084', 'text', '#1', null, null, '2016-06-22 11:11:02', '2016-06-22 11:11:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('883', '10000103', 'text', '下面就来熟悉一下我们分析师团队的各位精英！', null, null, '2016-06-22 11:11:11', '2016-06-22 11:11:11', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('884', '10000031', 'text', 'hao ', null, null, '2016-06-22 11:13:18', '2016-06-22 11:13:18', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('885', '10000084', 'text', '#9', null, null, '2016-06-22 11:13:22', '2016-06-22 11:13:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('886', '10000015', 'text', '#9', null, null, '2016-06-22 11:13:26', '2016-06-22 11:13:26', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('887', '10000103', 'text', '首先是我们的郑杰 郑老师 #14', null, null, '2016-06-22 11:14:11', '2016-06-22 11:14:11', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('888', '4', 'text', '#9', null, null, '2016-06-22 11:14:58', '2016-06-22 11:14:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('889', '10000103', 'text', '从事金融行业6年，曾担任国内一大型投资机构分析师和培训师。在期货，外汇和大宗商品市场均有较强的实战经验。对均线系统，K线形态，数据建模，宏观分析等均有较深的理解和运用。擅长中线行情分析布局和宏观分析，交易秉承“耐心等待，大胆出击”的风格，坚持顺应了趋势就大胆加仓，错误了就坚决离场的交易理念。曾经在进入金融行业前，利用自身所学理论成功预测金银2013年将在近十年春节首次出现下跌。', null, null, '2016-06-22 11:15:04', '2016-06-22 11:15:04', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('890', '10000088', 'text', '1', null, null, '2016-06-22 11:15:55', '2016-06-22 11:15:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('891', '10000030', 'text', '#6 辛苦了#9', null, null, '2016-06-22 11:16:17', '2016-06-22 11:16:17', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('892', '10000026', 'text', '#9', null, null, '2016-06-22 11:16:23', '2016-06-22 11:16:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('893', '10000008', 'text', '#20', null, null, '2016-06-22 11:16:54', '2016-06-22 11:16:54', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('894', '10000109', 'text', '#9#17', null, null, '2016-06-22 11:17:02', '2016-06-22 11:17:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('895', '10000056', 'text', '#9', null, null, '2016-06-22 11:17:11', '2016-06-22 11:17:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('896', '10000109', 'text', '总算进来了！', null, null, '2016-06-22 11:17:18', '2016-06-22 11:17:18', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('897', '10000027', 'text', '#9', null, null, '2016-06-22 11:17:39', '2016-06-22 11:17:39', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('898', '10000103', 'text', '接着是我们的吴勇坚 吴老师  ', null, null, '2016-06-22 11:21:07', '2016-06-22 11:21:07', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('899', '10000032', 'text', '#14', null, null, '2016-06-22 11:21:33', '2016-06-22 11:21:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('900', '10000103', 'text', '从2010年进入国际金融市场，至今5年有余，先后曾在多家企业担任职业操盘，在股市、贵金属，原油及贵金属市场拥有丰富的实战操作经验和分析能力2013-2015年间，平均月盈率达到40%。 ', null, null, '2016-06-22 11:21:33', '2016-06-22 11:21:33', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('901', '10000079', 'text', '没有直播间吗  ', null, null, '2016-06-22 11:21:35', '2016-06-22 11:21:35', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('902', '10000002', 'text', '#20', null, null, '2016-06-22 11:21:36', '2016-06-22 11:21:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('903', '10000103', 'text', '擅长江恩理论，波浪理论、趋势线和通道以及各种技术指标应用进行短期投资。 其交易理念为，从基本面需求大方向，从技术面寻求入场水平，控制仓位耐心等待机会顺势而为', null, null, '2016-06-22 11:21:51', '2016-06-22 11:21:51', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('904', '10000032', 'text', '#14', null, null, '2016-06-22 11:21:58', '2016-06-22 11:21:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('905', '10000035', 'text', '#9', null, null, '2016-06-22 11:22:08', '2016-06-22 11:22:08', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('906', '10000096', 'text', '#14#14', null, null, '2016-06-22 11:22:16', '2016-06-22 11:22:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('907', '10000032', 'text', '#21', null, null, '2016-06-22 11:22:59', '2016-06-22 11:22:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('908', '10000079', 'text', '搞个直播间  让郑老师在里面现场直播  #4', null, null, '2016-06-22 11:24:23', '2016-06-22 11:24:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('909', '10000103', 'text', '\n接下来下一位老师是伍尤顺 伍老师', null, null, '2016-06-22 11:25:03', '2016-06-22 11:25:03', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('910', '10000006', 'text', '老师好#14', null, null, '2016-06-22 11:25:22', '2016-06-22 11:25:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('911', '10000103', 'text', '2010年至今5年有余工作年限，具有多年股市、汇市，期货，原油及贵金属的交易经验。2009年进入国际金融市场，2010年入驻国内白银市场， 2012年以月收益800%获称“盈利王”的称号，曾创造原油连续11周盈利神话！ ', null, null, '2016-06-22 11:25:29', '2016-06-22 11:25:29', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('912', '10000103', 'text', '对经济数据、K线理论、均线理论、指标理论、形态理论、波浪理论均有独特的研究造诣，不仅具备深厚的技术分析功底，而且擅用逆向思维来把握市场群体心理反应，能够准确的把握市场动向，在变幻莫测、扑朔迷离的交易市场中游刃有余。', null, null, '2016-06-22 11:26:04', '2016-06-22 11:26:04', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('913', '10000103', 'text', '欢迎以上三位老师#14#14', null, null, '2016-06-22 11:27:06', '2016-06-22 11:27:06', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('914', '10000084', 'text', '#9老师好', null, null, '2016-06-22 11:29:01', '2016-06-22 11:29:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('915', '10000103', 'text', '最后自我介绍一下，我是直播室小秘书#6#6大家可以叫我小罗老师', null, null, '2016-06-22 11:29:56', '2016-06-22 11:29:56', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('916', '10000103', 'text', '本科金融专业毕业，2014年进入金融市场，两年现货市场的从业和讲师经验，擅长道氏理论，拥有丰富的客户理财经验。', null, null, '2016-06-22 11:30:34', '2016-06-22 11:30:34', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('917', '10000028', 'text', '#1', null, null, '2016-06-22 11:31:02', '2016-06-22 11:31:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('918', '10000121', 'text', '#14#14#14', null, null, '2016-06-22 11:32:21', '2016-06-22 11:32:21', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('919', '10000103', 'text', '好啦，介绍完毕#1下面交给我们的郑杰老师，分析今天的行情。', null, null, '2016-06-22 11:32:37', '2016-06-22 11:32:37', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('920', '10000000', 'text', '大家早上好！朋友们好久不见！谢谢我们的主持人小罗老师#4', null, null, '2016-06-22 11:33:30', '2016-06-22 11:33:30', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('921', '10000013', 'text', '郑老师好', null, null, '2016-06-22 11:34:46', '2016-06-22 11:34:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('922', '10000000', 'text', '来到轩湛投资观察室不仅能得到实时的资讯，技术的分析，精彩的课堂，大家还能够在这里畅所欲言，畅谈人生！', null, null, '2016-06-22 11:35:52', '2016-06-22 11:35:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('923', '10000013', 'text', '#1', null, null, '2016-06-22 11:35:58', '2016-06-22 11:35:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('924', '10000000', 'text', '所以，大家有什么问题，尽情想我提问吧，我都会满足大家的需求#4', null, null, '2016-06-22 11:36:49', '2016-06-22 11:36:49', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('925', '10000102', 'text', '#9', null, null, '2016-06-22 11:37:03', '2016-06-22 11:37:03', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('926', '10000000', 'text', '好了，下面就开始昨天行情的回顾。', null, null, '2016-06-22 11:37:25', '2016-06-22 11:37:25', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('927', '10000037', 'text', '#4', null, null, '2016-06-22 11:40:15', '2016-06-22 11:40:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('928', '10000000', 'text', '昨天早盘，受到英国留欧希望大增的影响，原油大幅走弱，一度下跌超过2%.但美联储主席耶伦在货币政策报告听证会上表示经济前景依然具有相当大的不确定性。言语偏于鸽派，使得油价下降至321支撑位后反弹至325位短期压力位。', null, null, '2016-06-22 11:40:39', '2016-06-22 11:40:39', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('929', '10000000', 'text', '凌晨4:30公布了最新API库存超预期大降522.4万桶，使得油价有快速反弹，冲破压力位326，上探至330压力位处。', null, null, '2016-06-22 11:41:05', '2016-06-22 11:41:05', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('930', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a08f2e0bf5.jpg', null, null, '2016-06-22 11:41:38', '2016-06-22 11:41:38', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('931', '10000000', 'text', '技术面上，目前原油陷入了高位震荡，短期走势依然强势，上方面临前期的压力340一带', null, null, '2016-06-22 11:44:33', '2016-06-22 11:44:33', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('932', '10000000', 'text', '4小时图看，均线系统呈多头向上走势，5日均线向上贯穿10日均线形成金叉，K线价格沿5日均线向上运行，MACD指标在零轴下方金叉向上运行，红色动能柱放量，KDJ指标向上发散运行', null, null, '2016-06-22 11:44:59', '2016-06-22 11:44:59', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('933', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a0a0257ef1.jpg', null, null, '2016-06-22 11:46:10', '2016-06-22 11:46:10', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('934', '10000000', 'text', '而跟原油关联非常大的美元，我们也来看看它的走势情况', null, null, '2016-06-22 11:49:13', '2016-06-22 11:49:13', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('935', '10000000', 'text', '美元技术面方面，目前美元走在下降通道当中。日线图中，布林带开口呈向下走势，5日均线下穿布林中轨和10日均线，综合来看，短期内预计美元将继续走低。', null, null, '2016-06-22 11:50:57', '2016-06-22 11:50:57', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('936', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a0b3a60c3c.jpg', null, null, '2016-06-22 11:51:22', '2016-06-22 11:51:22', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('937', '10000000', 'text', '日内操作上，倾向短线偏多，多单为主，空单为辅。上方压力区：332~333，下方支撑：325~326。', null, null, '2016-06-22 11:52:52', '2016-06-22 11:52:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('938', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a0bdda4b07.jpg', null, null, '2016-06-22 11:54:05', '2016-06-22 11:54:05', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('939', '10000000', 'text', '而为了庆祝我们的轩湛投资观察室隆重展开，市场今晚10点半也迎来了一个超级数据：EIA原油库存数据。真是给我们送来一份大礼啊#4', null, null, '2016-06-22 11:55:35', '2016-06-22 11:55:35', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('940', '10000000', 'text', '由于昨晚凌晨API的数据大幅利多，所以目前市场也会认为EIA也会像API那样利多。', null, null, '2016-06-22 11:56:36', '2016-06-22 11:56:36', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('941', '10000005', 'text', '请老师多多关照#9', null, null, '2016-06-22 11:56:38', '2016-06-22 11:56:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('942', '10000000', 'text', '而之前美国国内原油产量削减2.9万桶/日。美国EIA原油库存连续四周录得下滑，且此次美国EIA汽油库存降幅刷新八周以来最大降幅水平。', null, null, '2016-06-22 11:56:59', '2016-06-22 11:56:59', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('943', '10000000', 'text', '所以今晚的EIA很有可能也是利多行情。', null, null, '2016-06-22 11:57:15', '2016-06-22 11:57:15', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('944', '10000092', 'text', '#9', null, null, '2016-06-22 11:58:39', '2016-06-22 11:58:39', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('945', '10000079', 'text', '#9', null, null, '2016-06-22 11:59:24', '2016-06-22 11:59:24', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('946', '10000000', 'text', '以上就是行情回顾和技术分析。', null, null, '2016-06-22 11:59:27', '2016-06-22 11:59:27', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('947', '10000000', 'text', '大家也看到了，今晚又迎来了一周一度的EIA之夜，又是我们把握机会大赚特赚的机会，不要犹豫了，今晚准时十点半，我会在轩湛投资观察室等着你的到来！#15#15', null, null, '2016-06-22 12:01:09', '2016-06-22 12:01:09', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('948', '10000099', 'text', '', null, null, '2016-06-22 12:01:32', '2016-06-22 12:01:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('949', '10000000', 'text', '好了，大家现在可以先去吃饭，稍后一点将会给大家继续带来直播。', null, null, '2016-06-22 12:01:56', '2016-06-22 12:01:56', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('950', '10000099', 'text', '', null, null, '2016-06-22 12:03:33', '2016-06-22 12:03:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('951', '10000072', 'text', '#1', null, null, '2016-06-22 12:05:06', '2016-06-22 12:05:06', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('952', '10000085', 'text', '老师好#9', null, null, '2016-06-22 12:42:11', '2016-06-22 12:42:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('953', '10000089', 'text', '#14', null, null, '2016-06-22 12:45:40', '2016-06-22 12:45:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('954', '10000061', 'text', '.', null, null, '2016-06-22 12:49:39', '2016-06-22 12:49:39', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('955', '10000063', 'text', '#4', null, null, '2016-06-22 12:49:54', '2016-06-22 12:49:54', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('956', '10000034', 'text', '#4', null, null, '2016-06-22 12:57:47', '2016-06-22 12:57:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('957', '10000077', 'text', '#9', null, null, '2016-06-22 12:58:29', '2016-06-22 12:58:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('958', '10000064', 'text', '#9', null, null, '2016-06-22 12:59:32', '2016-06-22 12:59:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('959', '10000000', 'text', '大家中午好！', null, null, '2016-06-22 13:00:33', '2016-06-22 13:00:33', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('960', '10000000', 'text', '大家中午好！', null, null, '2016-06-22 13:01:01', '2016-06-22 13:01:01', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('961', '10000063', 'text', '#14坐等老师直播', null, null, '2016-06-22 13:01:17', '2016-06-22 13:01:17', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('962', '10000061', 'text', 'Hi', null, null, '2016-06-22 13:01:25', '2016-06-22 13:01:25', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('963', '10000092', 'text', '#14', null, null, '2016-06-22 13:01:32', '2016-06-22 13:01:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('964', '10000000', 'text', '酒足饭饱之后，我们继续开始下午盘的讲解#4', null, null, '2016-06-22 13:01:36', '2016-06-22 13:01:36', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('965', '10000008', 'text', '#14', null, null, '2016-06-22 13:02:38', '2016-06-22 13:02:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('966', '10000000', 'text', '早上的原油高开以后虽然出现小幅的回落，但是依然重新回升并创出新高！', null, null, '2016-06-22 13:03:19', '2016-06-22 13:03:19', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('967', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a1ca2be5dd.jpg', null, null, '2016-06-22 13:05:38', '2016-06-22 13:05:38', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('968', '10000093', 'text', '#9', null, null, '2016-06-22 13:06:20', '2016-06-22 13:06:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('969', '10000000', 'text', '我们日内的思路依然是逢低做多为主！刚才按照我的操作建议在330做了短多的朋友，关注下午能否拉升到335上方', null, null, '2016-06-22 13:06:41', '2016-06-22 13:06:41', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('970', '10000000', 'text', '下面就让我们的吴老师来给大家简单讲解一下早上A股的走势情况，大家欢迎！#14', null, null, '2016-06-22 13:07:44', '2016-06-22 13:07:44', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('971', '10000037', 'text', '', '10000013', '#1', '2016-06-22 13:07:53', '2016-06-22 13:07:53', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('972', '10000037', 'text', '#9', null, null, '2016-06-22 13:08:00', '2016-06-22 13:08:00', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('973', '10000093', 'text', '#14', null, null, '2016-06-22 13:08:02', '2016-06-22 13:08:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('974', '10000102', 'text', '#14', null, null, '2016-06-22 13:08:03', '2016-06-22 13:08:03', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('975', '10000090', 'text', '#14', null, null, '2016-06-22 13:08:40', '2016-06-22 13:08:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('976', '10000084', 'text', '#14', null, null, '2016-06-22 13:08:48', '2016-06-22 13:08:48', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('977', '10000001', 'text', '大家下午好！#9', null, null, '2016-06-22 13:08:50', '2016-06-22 13:08:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('978', '10000037', 'text', '#14', null, null, '2016-06-22 13:09:40', '2016-06-22 13:09:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('979', '10000001', 'text', '今日A股开盘后继续上探压力位2900', null, null, '2016-06-22 13:10:47', '2016-06-22 13:10:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('980', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a1e44f30d5.png', null, null, '2016-06-22 13:12:37', '2016-06-22 13:12:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('981', '10000013', 'text', '#9', null, null, '2016-06-22 13:13:45', '2016-06-22 13:13:45', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('982', '10000001', 'text', '今日盘面上，仪器仪表、煤炭、深港通、次新股、OLED等板块涨幅居前', null, null, '2016-06-22 13:15:33', '2016-06-22 13:15:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('983', '10000001', 'text', '技术面看，大盘日线MACD走平且波动极小，KDJ低位钝化的迹象再次显现，预计今日沪指将收带星小阳线。', null, null, '2016-06-22 13:22:31', '2016-06-22 13:22:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('984', '10000013', 'text', '郑老师现在可以做空吗？', null, null, '2016-06-22 13:22:35', '2016-06-22 13:22:35', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('985', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a20b1e311d.png', null, null, '2016-06-22 13:22:57', '2016-06-22 13:22:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('986', '10000080', 'text', '#9', null, null, '2016-06-22 13:23:16', '2016-06-22 13:23:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('987', '7', 'text', '#20', null, null, '2016-06-22 13:24:27', '2016-06-22 13:24:27', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('988', '10000000', 'text', '目前原油来到高位压力区，暂时没有明显回落转弱的迹象。如果激进的想操作，建议带好止损334.下方看325', '10000013', '郑老师现在可以做空吗？', '2016-06-22 13:24:28', '2016-06-22 13:24:28', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('989', '10000088', 'text', '我在330位置做了3手空', null, null, '2016-06-22 13:24:49', '2016-06-22 13:24:49', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('990', '10000088', 'text', '现在怎么看？', null, null, '2016-06-22 13:24:55', '2016-06-22 13:24:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('991', '10000000', 'text', '带好止损334，止盈先看325', '10000088', '我在330位置做了3手空', '2016-06-22 13:26:36', '2016-06-22 13:26:36', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('992', '10000102', 'text', '耶伦昨晚的讲话，偏利多还是利空？', null, null, '2016-06-22 13:26:40', '2016-06-22 13:26:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('993', '10000001', 'text', '预测大盘接下来的走势市场依旧是存量博弈下的区间震荡走势，大涨大跌的概率都不高', null, null, '2016-06-22 13:29:32', '2016-06-22 13:29:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('994', '10000088', 'text', 'ok', null, null, '2016-06-22 13:30:09', '2016-06-22 13:30:09', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('995', '10000092', 'text', '老师，为什么今天石油高开了这么多个点', '10000008', '#14', '2016-06-22 13:30:27', '2016-06-22 13:30:27', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('996', '10000001', 'text', '所以建议持有股票的朋友，趁着大盘现在反弹到压力位，可以适当减仓出逃', null, null, '2016-06-22 13:31:08', '2016-06-22 13:31:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('997', '10000102', 'text', '英国退欧对股市，有什么影响？', null, null, '2016-06-22 13:31:26', '2016-06-22 13:31:26', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('998', '10000096', 'text', '', '10000092', '老师，为什么今天石油高开了这么多个点', '2016-06-22 13:32:20', '2016-06-22 13:32:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('999', '10000000', 'text', '昨晚凌晨的API库存数据大幅利多，导致了油价高开', '10000092', '老师，为什么今天石油高开了这么多个点', '2016-06-22 13:33:31', '2016-06-22 13:33:31', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1000', '10000000', 'text', '耶伦昨晚的讲话稍稍偏多', '10000102', '耶伦昨晚的讲话，偏利多还是利空？', '2016-06-22 13:33:59', '2016-06-22 13:33:59', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1001', '10000000', 'text', '英国退欧会引起外围环境的恶化，利空股市', '10000102', '英国退欧对股市，有什么影响？', '2016-06-22 13:34:20', '2016-06-22 13:34:20', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1002', '10000003', 'text', '老师好', null, null, '2016-06-22 13:34:22', '2016-06-22 13:34:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1003', '10000001', 'text', '想短线买入股票的朋友，可以先等待英国公投结果，预计周五行情可能出现变动，此前更多倾向于等待观望', null, null, '2016-06-22 13:35:14', '2016-06-22 13:35:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1004', '10000102', 'text', '好的，谢谢老师', null, null, '2016-06-22 13:35:17', '2016-06-22 13:35:17', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1005', '10000102', 'text', '那已经持有股票的，要不要出？', null, null, '2016-06-22 13:39:30', '2016-06-22 13:39:30', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1006', '10000001', 'text', '好，接下来我们关注一下今周的风险事件“英国退欧公投”', null, null, '2016-06-22 13:40:20', '2016-06-22 13:40:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1007', '10000003', 'text', '#17', null, null, '2016-06-22 13:40:23', '2016-06-22 13:40:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1008', '10000001', 'text', '最近英国退欧可以说是沸沸扬扬啊', null, null, '2016-06-22 13:41:30', '2016-06-22 13:41:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1009', '10000048', 'text', '#15', null, null, '2016-06-22 13:42:33', '2016-06-22 13:42:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1010', '10000053', 'text', '老师，现在这个点位可以做单吗？', null, null, '2016-06-22 13:43:55', '2016-06-22 13:43:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1011', '10000000', 'text', '可以顺势做点短多，有盈利就可以走', '10000053', '老师，现在这个点位可以做单吗？', '2016-06-22 13:44:36', '2016-06-22 13:44:36', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1012', '10000000', 'text', '趁反弹到2900上方减仓', '10000102', '那已经持有股票的，要不要出？', '2016-06-22 13:44:52', '2016-06-22 13:44:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1013', '10000001', 'text', '欧盟是世界上最有吸引力的发达国家联盟组织，加入欧盟都是中东欧国家梦寐以求的目标啊。那为什么英国人在喊退出欧盟呢，我想大家都很困惑吧', null, null, '2016-06-22 13:45:18', '2016-06-22 13:45:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1014', '10000056', 'text', '#9', '10000102', '耶伦昨晚的讲话，偏利多还是利空？', '2016-06-22 13:45:48', '2016-06-22 13:45:48', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1015', '10000087', 'text', '#9', null, null, '2016-06-22 13:48:54', '2016-06-22 13:48:54', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1016', '10000051', 'text', '#1', null, null, '2016-06-22 13:50:29', '2016-06-22 13:50:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1017', '10000001', 'text', '从现实上看：现在的欧盟经济形势仍旧疲软，而且欧盟的政策很大程度妨碍了英国主权，损害了英国的经济利益，所以英国退欧是为了完全收回英国主权，保护英国利益', null, null, '2016-06-22 13:50:33', '2016-06-22 13:50:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1018', '10000084', 'text', '#9', null, null, '2016-06-22 13:53:43', '2016-06-22 13:53:43', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1019', '10000001', 'text', '从传统上看：英国一直奉行“光荣孤立”政策，同时警惕和遏制任何有望统一欧洲的大陆强权，而且孤立主义是英国成就近代辉煌的重要经验', null, null, '2016-06-22 13:53:45', '2016-06-22 13:53:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1020', '10000001', 'text', '好，现在我们知道英国为什么退欧。那么英国退欧会带来什么影响呢？', null, null, '2016-06-22 13:56:21', '2016-06-22 13:56:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1021', '10000017', 'text', '#1', null, null, '2016-06-22 14:02:40', '2016-06-22 14:02:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1022', '10000001', 'text', '欧盟有28个成员国，目前，以GDP总量计算，英国在欧盟是仅次于德国之后的第二大经济体，所以英国一旦在公投中选择退欧，欧盟区年化GDP增速大幅降低', null, null, '2016-06-22 14:03:28', '2016-06-22 14:03:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1023', '10000086', 'text', '明天下午两点公布退欧公投的结果！有两倍行情？？', null, null, '2016-06-22 14:03:53', '2016-06-22 14:03:53', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1024', '10000001', 'text', '导致欧元大幅走低，股市走低，油价走低，将引起市场激烈反应！', null, null, '2016-06-22 14:04:52', '2016-06-22 14:04:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1025', '10000006', 'text', '是2点吗？', null, null, '2016-06-22 14:05:00', '2016-06-22 14:05:00', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1026', '10000088', 'text', '1', null, null, '2016-06-22 14:05:14', '2016-06-22 14:05:14', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1027', '10000084', 'text', '#9', null, null, '2016-06-22 14:11:14', '2016-06-22 14:11:14', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1028', '10000001', 'text', '好，我们看看英国退欧在市场的热度有多高', null, null, '2016-06-22 14:17:01', '2016-06-22 14:17:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1029', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a2da29ed44.jpeg', null, null, '2016-06-22 14:18:10', '2016-06-22 14:18:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1030', '10000001', 'text', '根据过去两年的风险事件来看，算上杠杆，是有的', '10000086', '明天下午两点公布退欧公投的结果！有两倍行情？？', '2016-06-22 14:27:24', '2016-06-22 14:27:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1031', '10000003', 'text', '跟着老师做的多，涨了#14', null, null, '2016-06-22 14:27:35', '2016-06-22 14:27:35', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1032', '10000102', 'text', '我也赚了，做了10手#9', null, null, '2016-06-22 14:28:11', '2016-06-22 14:28:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1033', '10000052', 'text', '#20', null, null, '2016-06-22 14:28:22', '2016-06-22 14:28:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1034', '10000006', 'text', '你们都买了吗', null, null, '2016-06-22 14:28:40', '2016-06-22 14:28:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1035', '10000003', 'text', '#17', null, null, '2016-06-22 14:28:51', '2016-06-22 14:28:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1036', '10000001', 'text', '央视也来了！！！', null, null, '2016-06-22 14:29:20', '2016-06-22 14:29:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1037', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a304b3a05b.png', null, null, '2016-06-22 14:29:31', '2016-06-22 14:29:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1038', '10000102', 'text', '当然，看到那个点位，就做进去了', null, null, '2016-06-22 14:30:16', '2016-06-22 14:30:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1039', '10000001', 'text', '被称为打垮了英格兰银行的人，怎么看待英国退欧', null, null, '2016-06-22 14:32:45', '2016-06-22 14:32:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1040', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a3114aca02.jpeg', null, null, '2016-06-22 14:32:52', '2016-06-22 14:32:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1041', '10000084', 'text', '#9我也做进去了', null, null, '2016-06-22 14:33:01', '2016-06-22 14:33:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1042', '10000084', 'text', '#20', null, null, '2016-06-22 14:33:06', '2016-06-22 14:33:06', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1043', '10000001', 'text', '所以英国脱欧真的是可以称得上是近20年来最大的金融政治事件之一！！！', null, null, '2016-06-22 14:35:08', '2016-06-22 14:35:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1044', '10000001', 'text', '全球瞩目，所以行情绝对不小！！！！', null, null, '2016-06-22 14:36:46', '2016-06-22 14:36:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1045', '10000001', 'text', '最近英国民调显示留欧支持率45% 退欧支持率44%', null, null, '2016-06-22 14:40:07', '2016-06-22 14:40:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1046', '10000001', 'text', '我们知道，从大众人民的角度看：欧盟是影响力最高的发达国家联盟，英国肯定不会退欧啦，但是历史告诉你', null, null, '2016-06-22 14:44:44', '2016-06-22 14:44:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1047', '10000001', 'text', '曾在1990年，英国就加入过欧洲汇率体系，但是到了92年，英国宣布退出欧洲汇率体系', null, null, '2016-06-22 14:45:28', '2016-06-22 14:45:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1048', '10000004', 'text', '#20', null, null, '2016-06-22 14:51:51', '2016-06-22 14:51:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1049', '10000001', 'text', '所以，就是因为这种不确定性，市场的波动才会这么大', null, null, '2016-06-22 14:53:18', '2016-06-22 14:53:18', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1050', '8', 'text', '大家好！我是你们的晚盘分析老师，将在晚间为大家实时解盘。', null, null, '2016-06-22 14:54:13', '2016-06-22 14:54:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1051', '8', 'text', '我们晚上不见不散！#4', null, null, '2016-06-22 14:54:48', '2016-06-22 14:54:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1052', '8', 'text', '现在直播室交还给吴老师。', null, null, '2016-06-22 14:55:33', '2016-06-22 14:55:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1053', '10000102', 'text', '322了，欧盘时间还会涨吗？', null, null, '2016-06-22 14:55:50', '2016-06-22 14:55:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1054', '10000001', 'text', '好，谢谢伍老师#9', null, null, '2016-06-22 14:56:53', '2016-06-22 14:56:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1055', '10000001', 'text', '我们回到英国退欧事件', null, null, '2016-06-22 14:57:55', '2016-06-22 14:57:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1056', '10000001', 'text', '现在油价在上探上方压力位334，如果是重仓多单，可以先获利减仓。', '10000102', '322了，欧盘时间还会涨吗？', '2016-06-22 14:59:49', '2016-06-22 14:59:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1057', '10000102', 'text', '好的', null, null, '2016-06-22 15:00:26', '2016-06-22 15:00:26', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1058', '10000102', 'text', '#9', null, null, '2016-06-22 15:00:30', '2016-06-22 15:00:30', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1059', '10000001', 'text', '那么英国退欧事件结果怎么影响油价呢？', null, null, '2016-06-22 15:02:30', '2016-06-22 15:02:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1060', '10000001', 'text', '英国公投结果是脱欧，则利空原油；留欧，则利多原油。', null, null, '2016-06-22 15:03:21', '2016-06-22 15:03:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1061', '10000001', 'text', '历史上，这种黑天鹅事件也是经常存在。下面我们来看看这些金融政治事件对原油当晚的影响', null, null, '2016-06-22 15:04:49', '2016-06-22 15:04:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1062', '10000001', 'text', '瑞士央行在2015年1月15日意外宣布取消欧元兑瑞郎1:1.20保底汇率', null, null, '2016-06-22 15:05:27', '2016-06-22 15:05:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1063', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a38cf28e8b.jpeg', null, null, '2016-06-22 15:05:51', '2016-06-22 15:05:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1064', '10000001', 'text', '当天原油直接从315跌到283，跌了32块钱，跌幅达到10%，算上杠杆直接是3倍的收益空间！', null, null, '2016-06-22 15:06:41', '2016-06-22 15:06:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1065', '10000001', 'text', '2015年8月24日中国央行出面干预人民币，让人民币直接从6.1贬值到6.5', null, null, '2016-06-22 15:07:50', '2016-06-22 15:07:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1066', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a396382d39.jpeg', null, null, '2016-06-22 15:08:19', '2016-06-22 15:08:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1067', '10000001', 'text', '原油当日从355跌至340，跌幅超过5%！收益空间达到160%以上', null, null, '2016-06-22 15:09:01', '2016-06-22 15:09:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1068', '10000001', 'text', '所以大家看到，这些无法让市场有确切预期的风险事件对于投资者来说，绝对是一个大赚特赚的好机会！', null, null, '2016-06-22 15:09:59', '2016-06-22 15:09:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1069', '10000000', 'text', '今天330的多单可以先出来，等下午欧市开了我们再找机会做', null, null, '2016-06-22 15:10:30', '2016-06-22 15:10:30', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1070', '10000000', 'text', '330的多单我们先平仓获利出来，吃个餐前菜。等欧洲开市我们再找机会操作', null, null, '2016-06-22 15:11:20', '2016-06-22 15:11:20', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1071', '10000001', 'text', '所以被深套的朋友，借助退欧公投和杠杆很有可能一天就能帮你解套你套了一两年的股市资金了', null, null, '2016-06-22 15:11:31', '2016-06-22 15:11:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1072', '10000001', 'text', '好，现在股市收盘了，我们关注一下', null, null, '2016-06-22 15:15:59', '2016-06-22 15:15:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1073', '10000001', 'text', '今日大盘上涨26.99点，截至收盘，沪指报2905.55点，，涨幅0.94%，成交1563亿元', null, null, '2016-06-22 15:18:12', '2016-06-22 15:18:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1074', '10000001', 'text', '继续盘旋在压力位未能冲破', null, null, '2016-06-22 15:19:51', '2016-06-22 15:19:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1075', '10000088', 'text', '厉害啊', null, null, '2016-06-22 15:20:53', '2016-06-22 15:20:53', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1076', '10000001', 'text', '后市展望：从近期的行情来看，券商板块整体保持了较为温和走高的态势，有助于封杀大盘的下跌空间', null, null, '2016-06-22 15:28:32', '2016-06-22 15:28:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1077', '10000001', 'text', '目前大盘处于相对平静期，既无消息面的刺激，也无资金面的冲击，仍是存量资金的博弈，所以倾向于等待观望，等待英国公投结果。', null, null, '2016-06-22 15:30:07', '2016-06-22 15:30:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1078', '10000102', 'text', '大盘明天还会在2900以上再冲一波吗？', null, null, '2016-06-22 15:31:23', '2016-06-22 15:31:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1079', '10000003', 'text', '#4卖的时候正好322#14，  ', null, null, '2016-06-22 15:31:46', '2016-06-22 15:31:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1080', '10000000', 'text', '恭喜恭喜#14', '10000003', '#4卖的时候正好322#14，  ', '2016-06-22 15:33:11', '2016-06-22 15:33:11', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1081', '10000102', 'text', '郑老师，332还会向上冲吗？止盈止损要多少？', null, null, '2016-06-22 15:33:38', '2016-06-22 15:33:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1082', '10000006', 'text', '虽然赚的不多，但是第一次做单就有钱赚的感觉真好#1', null, null, '2016-06-22 15:33:52', '2016-06-22 15:33:52', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1083', '10000001', 'text', '最近没有重大利好消息面，只有重大的风险事件英国退欧，所以明天大盘想冲破压力位非常困难，预测明天会小幅回调下降', '10000102', '大盘明天还会在2900以上再冲一波吗？', '2016-06-22 15:34:55', '2016-06-22 15:34:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1084', '10000102', 'text', '如果卖，什么时候卖比较好?吴老师', null, null, '2016-06-22 15:36:22', '2016-06-22 15:36:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1085', '10000000', 'text', '上面的333~334区域的压力较大，要继续往上看看今晚的EIA是否给力了', '10000102', '郑老师，332还会向上冲吗？止盈止损要多少？', '2016-06-22 15:36:52', '2016-06-22 15:36:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1086', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a40adb517f.png', null, null, '2016-06-22 15:39:25', '2016-06-22 15:39:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1087', '10000001', 'text', '最好趁着现在大盘反弹到压力位把持有的股票平仓出逃，不然明天14:00英国就开始正式公投了，很有可能大大的利空股市！', '10000102', '如果卖，什么时候卖比较好?吴老师', '2016-06-22 15:43:16', '2016-06-22 15:43:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1088', '10000102', 'text', '好的，老师#9', null, null, '2016-06-22 15:50:19', '2016-06-22 15:50:19', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1089', '10000001', 'text', '好，下面交给我们的小罗老师，讲一下交易理念#9', null, null, '2016-06-22 16:03:10', '2016-06-22 16:03:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1090', '10000004', 'text', '明天股市会大跌吗？', null, null, '2016-06-22 16:06:16', '2016-06-22 16:06:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1091', '10000103', 'text', '投资过程中，要注意自己的交易习惯噢', null, null, '2016-06-22 16:08:57', '2016-06-22 16:08:57', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1092', '10000103', 'text', '人都是缺乏安全感的，总是怕自己做错。而一旦被错误的交易习惯主导投资，那么你永远都会错过一次又一次的牛市。', null, null, '2016-06-22 16:09:32', '2016-06-22 16:09:32', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1093', '10000001', 'text', '这个要看英国公投的数据怎么样，如果第一组公投数据偏向退欧阵营的，股市大跌的概率很高', '10000004', '明天股市会大跌吗？', '2016-06-22 16:16:15', '2016-06-22 16:16:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1094', '10000103', 'text', '很多投资者喜欢交易超短线的行情，但可能会缺乏严谨的交易计划', null, null, '2016-06-22 16:18:39', '2016-06-22 16:18:39', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1095', '10000013', 'text', '郑老师现在可以做空了吗？', '10000092', '老师，为什么今天石油高开了这么多个点', '2016-06-22 16:19:41', '2016-06-22 16:19:41', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1096', '10000103', 'text', '导致行情一出现反方向就慌张，马上平仓再反向建仓，但建仓后行情很多时候又回来了，导致两个方向交易都出错。', null, null, '2016-06-22 16:23:31', '2016-06-22 16:23:31', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1097', '10000000', 'text', '由于昨晚凌晨的API数据大幅利多，所以今天原油高开', null, null, '2016-06-22 16:29:06', '2016-06-22 16:29:06', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1098', '10000000', 'text', '目前原油还没有明显转弱的信号，想做空的建议先耐心等待', '10000013', '郑老师现在可以做空了吗？', '2016-06-22 16:29:34', '2016-06-22 16:29:34', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1099', '10000102', 'text', '好的，我现在多单还拿着', null, null, '2016-06-22 16:29:55', '2016-06-22 16:29:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1100', '10000103', 'text', '由于缺乏在交易前制定计划，所以交易中很容易被个人的情绪所控制。', null, null, '2016-06-22 16:35:37', '2016-06-22 16:35:37', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1101', '10000103', 'text', '恐惧心态会让自己判断对的单子被震荡行情早早震出场', null, null, '2016-06-22 16:36:54', '2016-06-22 16:36:54', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1102', '10000103', 'text', '而当市场已经出现反转的时候，贪念也会让自己不愿意离开这个市场，从而导致利润甚至本金消失。', null, null, '2016-06-22 16:37:36', '2016-06-22 16:37:36', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1103', '10000103', 'text', ' 所以要适时控制好情绪噢', null, null, '2016-06-22 16:42:38', '2016-06-22 16:42:38', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1104', '10000103', 'text', '同时交易前制定好计划也是很重要的呢', null, null, '2016-06-22 16:44:45', '2016-06-22 16:44:45', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1105', '10000103', 'text', '另外很多投资者在建仓以后，都会找支持自己单子的因素而忽视对自己单子不利的因素。', null, null, '2016-06-22 16:47:08', '2016-06-22 16:47:08', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1106', '10000103', 'text', '例如，建仓了多单就只会相信利多的基本面，而忽视利空的基本面', null, null, '2016-06-22 16:48:37', '2016-06-22 16:48:37', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1107', '10000103', 'text', '这其实是一个逃避式自我麻痹的心态', null, null, '2016-06-22 16:50:00', '2016-06-22 16:50:00', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1108', '10000103', 'text', '很大可能会导致投资者盲目恶性的加仓行为', null, null, '2016-06-22 16:51:59', '2016-06-22 16:51:59', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1109', '10000102', 'text', '说得好，之前我就是自己瞎操作，亏了好多#16', null, null, '2016-06-22 16:52:09', '2016-06-22 16:52:09', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1110', '10000103', 'text', ' 所以勿把市场看成是一个赌场，总想短时间内就让财富暴涨，没有正确的理财投资观念', null, null, '2016-06-22 16:57:12', '2016-06-22 16:57:12', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1111', '10000103', 'text', '操作上要做到计划你的交易，交易你的计划。', null, null, '2016-06-22 16:58:53', '2016-06-22 16:58:53', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1112', '10000103', 'text', '只有做好了交易的计划并严格执行，才能杜绝这种赌徒心态。', null, null, '2016-06-22 17:00:35', '2016-06-22 17:00:35', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1113', '10000103', 'text', '而投资是一个周期性的过程，更应该看重的是长期稳定的盈利。', null, null, '2016-06-22 17:01:29', '2016-06-22 17:01:29', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1114', '10000103', 'text', '切勿有“交易当中总想买在最低点，卖在最高点”这种心态，很容易导致入市前迷惘，入市后惊慌', null, null, '2016-06-22 17:05:57', '2016-06-22 17:05:57', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1115', '10000103', 'text', '我们专业的投资机构就能根据你的情况帮你制定长期稳定盈利的交易计划。', null, null, '2016-06-22 17:08:48', '2016-06-22 17:08:48', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1116', '10000103', 'text', '好了，接下来把直播室交还给吴老师#9', null, null, '2016-06-22 17:51:11', '2016-06-22 17:51:11', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1117', '10000001', 'text', '好，谢谢小罗老师#9', null, null, '2016-06-22 17:51:59', '2016-06-22 17:51:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1118', '10000001', 'text', '目前油价盘整后继续上探上方压力区334-335', null, null, '2016-06-22 17:57:17', '2016-06-22 17:57:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1119', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a610b625f3.png', null, null, '2016-06-22 17:57:31', '2016-06-22 17:57:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1120', '10000001', 'text', '好了，接下来，交给伍老师为我们继续讲解行情#9#14#14', null, null, '2016-06-22 18:03:47', '2016-06-22 18:03:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1121', '8', 'text', '大家下午好！#9', null, null, '2016-06-22 18:05:09', '2016-06-22 18:05:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1122', '8', 'text', '接下来由我继续为大家分析晚间行情。', null, null, '2016-06-22 18:06:08', '2016-06-22 18:06:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1123', '8', 'text', '目前，今日原油价格涨幅已达近1.9%左右。', null, null, '2016-06-22 18:15:00', '2016-06-22 18:15:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1124', '8', 'text', '今日亚洲时段，油价曾有过短时下降，不过很快便重振旗鼓。', null, null, '2016-06-22 18:15:53', '2016-06-22 18:15:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1125', '10000102', 'text', '还会继续涨', null, null, '2016-06-22 18:18:55', '2016-06-22 18:18:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1126', '10000102', 'text', '吗？', null, null, '2016-06-22 18:18:58', '2016-06-22 18:18:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1127', '8', 'text', '从盘面来看，上方短期压力在333一线，现在价格交投于压力位下方附近。', null, null, '2016-06-22 18:22:19', '2016-06-22 18:22:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1128', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a68c3b3712.png', null, null, '2016-06-22 18:30:27', '2016-06-22 18:30:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1129', '8', 'text', '下方关注330支撑位置。', null, null, '2016-06-22 18:31:11', '2016-06-22 18:31:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1130', '8', 'text', '从盘面上我们可以看到，日内多头明显占优，目前多头力量虽有所放缓，但还是有进一步上涨可能，关键看晚上EIA数据是否延续库存优势。', '10000102', '还会继续涨', '2016-06-22 18:39:32', '2016-06-22 18:39:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1131', '8', 'text', '大家是否记得今晚的重点数据呢？', null, null, '2016-06-22 18:48:41', '2016-06-22 18:48:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1132', '8', 'text', '美国至6月17日当周EIA原油库存将在晚上22:30的时候公布', null, null, '2016-06-22 18:50:10', '2016-06-22 18:50:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1133', '8', 'text', '这是我们原油投资者的盛宴，可千万不能忘记啊#4', null, null, '2016-06-22 18:51:56', '2016-06-22 18:51:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1134', '10000037', 'text', '期待今晚', null, null, '2016-06-22 19:02:08', '2016-06-22 19:02:08', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1135', '10000037', 'text', '#6', null, null, '2016-06-22 19:02:20', '2016-06-22 19:02:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1136', '10000084', 'text', '#20', null, null, '2016-06-22 19:04:18', '2016-06-22 19:04:18', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1137', '8', 'text', '在晚上的EIA数据公布之前，还有美联储主席耶伦在众议院金融服务委员会作半年度证词，也可是适当给予关注。', null, null, '2016-06-22 19:06:14', '2016-06-22 19:06:14', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1138', '8', 'text', '昨晚耶伦在美国参议院金融委员会半年一度的货币政策报告听证会上证词并无新意，市场大失所望，美元下挫，原油受到支撑而上涨。预计今晚的证词也不会有很大改变，对原油影响有限。', null, null, '2016-06-22 19:14:26', '2016-06-22 19:14:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1139', '8', 'text', '我们的关注的重点还是放在EIA原油库存上。', null, null, '2016-06-22 19:16:10', '2016-06-22 19:16:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1140', '8', 'text', '受上方压力，现原油价格出现小幅下挫。', null, null, '2016-06-22 19:28:03', '2016-06-22 19:28:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1141', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a777b7c8e5.png', null, null, '2016-06-22 19:33:15', '2016-06-22 19:33:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1142', '8', 'text', '下方支撑在黄金分割线的38.2%', null, null, '2016-06-22 19:35:02', '2016-06-22 19:35:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1143', '10000102', 'text', '开始下跌了，什么原因？', null, null, '2016-06-22 19:35:11', '2016-06-22 19:35:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1144', '8', 'text', '受消息面影响，价格出现短线下挫。', '10000102', '开始下跌了，什么原因？', '2016-06-22 19:38:47', '2016-06-22 19:38:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1145', '8', 'text', '全球最大的独立原油交易商预计美国原油产出不再下滑。', null, null, '2016-06-22 19:38:53', '2016-06-22 19:38:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1146', '8', 'text', '尼日利亚、委内瑞拉供应中断及美国页岩油产出下滑有助于稳定油市。', null, null, '2016-06-22 19:40:07', '2016-06-22 19:40:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1147', '8', 'text', '短期供应忧虑，市场有所震荡。', null, null, '2016-06-22 19:42:43', '2016-06-22 19:42:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1148', '8', 'text', '目前，从盘面上看价格下跌力度有点大，对多头不太有利。', null, null, '2016-06-22 19:44:47', '2016-06-22 19:44:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1149', '10000013', 'text', '可以做空吗现在', null, null, '2016-06-22 19:52:33', '2016-06-22 19:52:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1150', '8', 'text', '短周期看价格在支撑位上方，现在做空有点激进，建议等趋势再明朗的时候操作。', '10000013', '可以做空吗现在', '2016-06-22 19:59:37', '2016-06-22 19:59:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1151', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a7dd358bb8.png', null, null, '2016-06-22 20:00:19', '2016-06-22 20:00:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1152', '8', 'text', '我们做交易的时候，心态是很重要的。', null, null, '2016-06-22 20:08:58', '2016-06-22 20:08:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1153', '8', 'text', '不要仅仅因为失去耐心而出市，也不要因为迫不及待而入市。', null, null, '2016-06-22 20:09:22', '2016-06-22 20:09:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1154', '8', 'text', '平心静气是交易必须的心态。', null, null, '2016-06-22 20:10:09', '2016-06-22 20:10:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1155', '10000006', 'text', '今晚有数据是吗', null, null, '2016-06-22 20:11:07', '2016-06-22 20:11:07', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1156', '8', 'text', '大家在交易过程中，一定要慢慢锻炼自己的心态。这样我们才能在这个市场做一个常胜者！', null, null, '2016-06-22 20:11:54', '2016-06-22 20:11:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1157', '8', 'text', '是的。晚上22:30有美国至6月17日当周EIA原油库存数据。', '10000006', '今晚有数据是吗', '2016-06-22 20:12:30', '2016-06-22 20:12:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1158', '8', 'text', '经过刚才的小幅下挫，现在价格在330支撑位附近盘整。', null, null, '2016-06-22 20:17:35', '2016-06-22 20:17:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1159', '10000000', 'text', '大家晚上好！', null, null, '2016-06-22 20:22:00', '2016-06-22 20:22:00', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1160', '10000000', 'text', '刚才全球最大的独立原油交易商预计美国原油产出不再下滑，导致油价小幅下挫', '10000102', '开始下跌了，什么原因？', '2016-06-22 20:23:52', '2016-06-22 20:23:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1161', '10000000', 'text', '如果做空，止损带好在334，止盈看325', '10000013', '可以做空吗现在', '2016-06-22 20:25:08', '2016-06-22 20:25:08', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1162', '10000000', 'text', '是的', '10000006', '今晚有数据是吗', '2016-06-22 20:25:13', '2016-06-22 20:25:13', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1163', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a843031975.jpg', null, null, '2016-06-22 20:27:28', '2016-06-22 20:27:28', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1164', '10000000', 'text', '今晚10点半重点关注EIA的原油库存！', null, null, '2016-06-22 20:27:47', '2016-06-22 20:27:47', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1165', '10000000', 'text', '这是原油市场上的数据之王！', null, null, '2016-06-22 20:28:02', '2016-06-22 20:28:02', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1166', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a84f49b066.jpg', null, null, '2016-06-22 20:30:44', '2016-06-22 20:30:44', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1167', '10000000', 'text', '昨晚的API大幅利多，现在市场充斥着利多的看法', null, null, '2016-06-22 20:31:37', '2016-06-22 20:31:37', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1168', '10000000', 'text', '所以，今晚的数据按照目前的市场预期，应该以-300作为分水岭，-300以上才是利多，-300以内估计都是利空为主', null, null, '2016-06-22 20:31:56', '2016-06-22 20:31:56', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1169', '10000000', 'text', '所以本周，从走势上看，很有可能就会打破短期高位震荡的格局', null, null, '2016-06-22 20:42:12', '2016-06-22 20:42:12', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1170', '10000000', 'text', '市场在横盘等数据', null, null, '2016-06-22 20:47:14', '2016-06-22 20:47:14', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1171', '10000000', 'text', '下面我们来谈谈，很多朋友在交易当中遇到的那些心态。', null, null, '2016-06-22 20:48:00', '2016-06-22 20:48:00', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1172', '10000000', 'text', '很多朋友一定有这样一个经历：同样的市场行情，很多时候真金白银交易的时候总是会亏损，而当使用模拟仓交易，做单了以后一段时间不管，过后发现反而盈利了。', null, null, '2016-06-22 20:48:58', '2016-06-22 20:48:58', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1173', '10000000', 'text', '同样的行情，反而会出现不一样的结果，原因就是心态的偏差。', null, null, '2016-06-22 20:49:04', '2016-06-22 20:49:04', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1174', '10000000', 'text', '那么对于投资者，特别是没有严格训练的投资者而言，投资过程中都会出现哪些心态呢？', null, null, '2016-06-22 20:56:12', '2016-06-22 20:56:12', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1175', '10000000', 'text', '1. 跟风。', null, null, '2016-06-22 20:57:24', '2016-06-22 20:57:24', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1176', '10000102', 'text', '#2', null, null, '2016-06-22 20:57:32', '2016-06-22 20:57:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1177', '10000000', 'text', ' 在投资市场上，大部分散户都缺乏安全感，在做每一个决策之前，都希望身边有榜样或者一个氛围存在。', null, null, '2016-06-22 20:57:40', '2016-06-22 20:57:40', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1178', '10000000', 'text', '但根据二八定律，投资市场要赚钱永远靠的都是眼光和专业，比如当街上的大妈都知道能赚钱的话，那么你再跟风，就离亏损不远了。', null, null, '2016-06-22 20:57:52', '2016-06-22 20:57:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1179', '10000000', 'text', '2. 盲目。', null, null, '2016-06-22 20:59:24', '2016-06-22 20:59:24', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1180', '10000000', 'text', '在看到别人赚钱就跟进，没有彻底了解投资产品，风险控制，收益空间，市场状况等。同时也没有选择好的专业的投资咨询机构，很多甚至不愿意相信专业的投资机构。', null, null, '2016-06-22 21:00:14', '2016-06-22 21:00:14', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1181', '10000000', 'text', '比如很多朋友去炒股，但是连一只股票的财务状况，三大表（资产负债表，利润表，现金流量表）都完全不懂的情况下就真金白银的操作，这个风险是非常大的。', null, null, '2016-06-22 21:01:23', '2016-06-22 21:01:23', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1182', '10000000', 'text', '而在英美等发达国家，超过66%的散户投资都是交给专业的服务机构进行咨询或者操作，不少的客户都是通过专业机构的指导获得每年稳定的收益。', null, null, '2016-06-22 21:01:43', '2016-06-22 21:01:43', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1183', '10000000', 'text', '3. 凭主观感觉。', null, null, '2016-06-22 21:04:00', '2016-06-22 21:04:00', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1184', '10000000', 'text', '散户在操作的时候，做什么方向，选什么股，下多少仓位全凭个人的喜好和感觉，绝大部分人都没有给自己制定一个详细周全的交易计划。', null, null, '2016-06-22 21:04:10', '2016-06-22 21:04:10', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1185', '10000000', 'text', '其实交易的真谛并不是市场的方向，而是仓位的控制。仓位的合理控制才能让你在投资市场上稳定盈利。', null, null, '2016-06-22 21:04:36', '2016-06-22 21:04:36', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1186', '10000000', 'text', '仓位控制的公式：\n每次操作仓位=真实杠杆X账户余额/平台杠杆', null, null, '2016-06-22 21:07:21', '2016-06-22 21:07:21', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1187', '10000000', 'text', '（真实杠杆就是你每次交易盈利和亏损的速度和没有杠杆账户的盈利亏损速度比例，华尔街的交易员一般使用8倍到10倍的真实杠杠）', null, null, '2016-06-22 21:08:16', '2016-06-22 21:08:16', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1188', '10000000', 'text', '4. 赌徒心态。', null, null, '2016-06-22 21:12:46', '2016-06-22 21:12:46', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1189', '10000000', 'text', '把市场看成是一个赌场，总想短时间内就让财富暴涨，没有正确的理财投资观念。交易当中总想买在最低点，卖在最高点。这种心态导致了入市前迷惘，入市后惊慌。', null, null, '2016-06-22 21:12:57', '2016-06-22 21:12:57', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1190', '10000000', 'text', '操作上要做到计划你的交易，交易你的计划。只有做好了交易的计划并严格执行，才能杜绝这种赌徒心态。而投资是一个周期性的过程，更应该看重的是长期稳定的盈利。我们专业的投资机构就能根据你的情况帮你制定长期稳定盈利的交易计划。', null, null, '2016-06-22 21:13:08', '2016-06-22 21:13:08', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1191', '10000000', 'text', '5. 贪念和恐惧心态。', null, null, '2016-06-22 21:18:30', '2016-06-22 21:18:30', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1192', '10000000', 'text', '由于缺乏在交易前制定计划，所以交易中很容易被个人的情绪所控制。恐惧心态会让自己判断对的单子被震荡行情早早震出场；', null, null, '2016-06-22 21:18:43', '2016-06-22 21:18:43', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1193', '10000000', 'text', '而当市场已经出现反转的时候，贪念也会让自己不愿意离开这个市场，从而导致利润甚至本金消失。', null, null, '2016-06-22 21:18:53', '2016-06-22 21:18:53', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1194', '10000000', 'text', '6.逃避心态', null, null, '2016-06-22 21:21:56', '2016-06-22 21:21:56', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1195', '10000000', 'text', '很多投资者在建仓以后，都会找支持自己单子的因素而忽视对自己单子不利的因素。例如建仓了多单就只会相信利多的基本面而忽视利空的基本面。这其实是一个逃避式的自我麻痹的心态。会导致投资盲目恶性的加仓行为。', null, null, '2016-06-22 21:22:13', '2016-06-22 21:22:13', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1196', '10000037', 'text', '期待', null, null, '2016-06-22 21:25:34', '2016-06-22 21:25:34', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1197', '10000000', 'text', '好了，这就是投资者们普遍存在的心态', null, null, '2016-06-22 21:32:04', '2016-06-22 21:32:04', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1198', '10000000', 'text', '而我们轩湛——专业的投资机构就能帮大家量身订造详细的交易计划，让大家的交易不被个人情绪所控制，从而实现稳定盈利！', null, null, '2016-06-22 21:33:12', '2016-06-22 21:33:12', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1199', '10000000', 'text', '好了，下面就是重点关注今晚10点半的EIA了。我先写数据前的交易计划，把时间交给伍老师，我稍后上来#15', null, null, '2016-06-22 21:40:05', '2016-06-22 21:40:05', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1200', '10000203', 'text', '#9', null, null, '2016-06-22 21:41:20', '2016-06-22 21:41:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1201', '8', 'text', '好的！郑老师辛苦了。#9', null, null, '2016-06-22 21:41:44', '2016-06-22 21:41:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1202', '10000037', 'text', '#20', null, null, '2016-06-22 21:43:17', '2016-06-22 21:43:17', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1203', '10000074', 'text', '#9', null, null, '2016-06-22 21:43:21', '2016-06-22 21:43:21', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1204', '8', 'text', '现在距离EIA公布还有大概50分钟，市场在数据公布前交投比价谨慎，现在价格一直在330处盘整。', null, null, '2016-06-22 21:43:54', '2016-06-22 21:43:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1205', '8', 'text', '趁着数据公布还有段时间，我给大家简单分析下原油库存背后的投资规律。', null, null, '2016-06-22 21:47:32', '2016-06-22 21:47:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1206', '10000100', 'text', '#9', null, null, '2016-06-22 21:47:41', '2016-06-22 21:47:41', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1207', '8', 'text', '今天凌晨04:30，美国石油协会(API)公布了上周美国原油库存，数据显示，至6月17日当周API原油库存大减522.4万桶，降幅远超预期的减少190万桶，前值为增加115.8万桶；库欣地区原油库存减少131.1万桶。数据公布后，油价短线上涨。', null, null, '2016-06-22 21:48:23', '2016-06-22 21:48:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1208', '8', 'text', '每周公布一次的API原油库存和EIA原油库存是原油投资离不开的两个重要数据。', null, null, '2016-06-22 21:49:01', '2016-06-22 21:49:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1209', '10000064', 'text', '老师  今晚EIA预计是利空还是利多', null, null, '2016-06-22 21:49:46', '2016-06-22 21:49:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1210', '8', 'text', '其中EIA原油库存更是与油价直接挂钩，对原油投资者来说是一场盛宴。', null, null, '2016-06-22 21:52:11', '2016-06-22 21:52:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1211', '8', 'text', 'EIA原油库存数据是由美国能源信息署(EIA)每周三定期发布，其公布时间为每周三晚间22:30。', null, null, '2016-06-22 21:52:46', '2016-06-22 21:52:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1212', '8', 'text', '该数据测量了每周美国公司的商业原油库存的变化，若库存水平低于预期，则理论利多原油；若库存水平高于预期，则理论利空原油。', null, null, '2016-06-22 21:53:16', '2016-06-22 21:53:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1213', '8', 'text', '有调查总结近三个月来API和EIA同向概率高达76.4%。', null, null, '2016-06-22 21:56:41', '2016-06-22 21:56:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1214', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a99813c111.png', null, null, '2016-06-22 21:58:25', '2016-06-22 21:58:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1215', '8', 'text', '市场预期是利多的。', '10000064', '老师  今晚EIA预计是利空还是利多', '2016-06-22 21:59:35', '2016-06-22 21:59:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1216', '8', 'text', '在统计范围内的55组有效数据中（本周EIA尚未公布），仅有13次API和EIA公布的原油库存录得反向（一个为正值，一个为负值），即当凌晨公布的API录得增加时，当晚公布的EIA录得减少。', null, null, '2016-06-22 22:01:06', '2016-06-22 22:01:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1217', '8', 'text', '换句话说，在统计范围内，API和EIA同向概率为42/55，约为76.4%。那么当API数据率先出炉，随之而来的EIA数据有76.4%的机率与API数据同向。', null, null, '2016-06-22 22:02:26', '2016-06-22 22:02:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1218', '10000000', 'text', '现在的EIA预期值和前值比是利多', '10000064', '老师  今晚EIA预计是利空还是利多', '2016-06-22 22:04:23', '2016-06-22 22:04:23', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1219', '8', 'text', '今天凌晨公布的API库存录得减少522.4万桶，按此估算，EIA库存减少的概率相当大。', null, null, '2016-06-22 22:05:50', '2016-06-22 22:05:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1220', '8', 'text', '下面我们看一看数据公布后的涨跌幅统计。', null, null, '2016-06-22 22:12:52', '2016-06-22 22:12:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1221', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576a9cf27732a.png', null, null, '2016-06-22 22:13:06', '2016-06-22 22:13:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1222', '10000000', 'text', '从今年来看，除了第一个月，二月以来EIA的库存数据都是震荡下降的。加上上个月开始美国库欣地区的库存也出现下滑，所以整体美国的原油库存从最高位开始回落', null, null, '2016-06-22 22:13:07', '2016-06-22 22:13:07', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1223', '10000000', 'text', '但近期的原油库存走势来看，原油库存降速有减弱，库存有回升的迹象。这都是因为受到美国第二季度经济下滑所影响。', null, null, '2016-06-22 22:14:14', '2016-06-22 22:14:14', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1224', '10000035', 'text', '#9', null, null, '2016-06-22 22:14:38', '2016-06-22 22:14:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1225', '10000000', 'text', '所以，今晚给出的建议是除非数据大幅利多，否则我们都是以短空操作为主', null, null, '2016-06-22 22:14:42', '2016-06-22 22:14:42', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1226', '10000000', 'text', '1. 今晚数据在-300以上（例如-400），那么就在数据出来后做短多。有2~3块的利润空间就离场。\n\n2. 数据在-167~-300区间，那么就拉升后做一个短空，入场点在332.5上方比较稳健。止损337，止盈327\n\n3. 数据在-167以下，因为出乎了市场利多的预期，所以可以大胆追空操作。', null, null, '2016-06-22 22:14:53', '2016-06-22 22:14:53', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1227', '10000000', 'text', '1. 今晚数据在-300以上（例如-400），那么就在数据出来后做短多。有2~3块的利润空间就离场。', null, null, '2016-06-22 22:15:04', '2016-06-22 22:15:04', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1228', '10000000', 'text', '2. 数据在-167~-300区间，那么就拉升后做一个短空，入场点在332.5上方比较稳健。止损337，止盈327', null, null, '2016-06-22 22:15:13', '2016-06-22 22:15:13', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1229', '10000000', 'text', '3. 数据在-167以下，因为出乎了市场利多的预期，所以可以大胆追空操作。', null, null, '2016-06-22 22:15:15', '2016-06-22 22:15:15', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1230', '10000084', 'text', '#9', null, null, '2016-06-22 22:22:40', '2016-06-22 22:22:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1231', '10000084', 'text', '#20', null, null, '2016-06-22 22:24:20', '2016-06-22 22:24:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1232', '8', 'text', 'EIA利空原油', null, null, '2016-06-22 22:30:18', '2016-06-22 22:30:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1233', '10000000', 'text', '数据利空，可以追空操作', null, null, '2016-06-22 22:30:48', '2016-06-22 22:30:48', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1234', '10000000', 'text', '数据利空，可以追空操作', null, null, '2016-06-22 22:30:57', '2016-06-22 22:30:57', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1235', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160622/576aa12da5225.png', null, null, '2016-06-22 22:31:09', '2016-06-22 22:31:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1236', '10000006', 'text', '#3', null, null, '2016-06-22 22:33:41', '2016-06-22 22:33:41', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1237', '10000006', 'text', '还可以空吗', null, null, '2016-06-22 22:34:23', '2016-06-22 22:34:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1238', '10000000', 'text', '做了空单的朋友请耐心等待，没做空的还可以做空操作', null, null, '2016-06-22 22:34:47', '2016-06-22 22:34:47', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1239', '10000000', 'text', '按照建议带好止损止盈', null, null, '2016-06-22 22:34:59', '2016-06-22 22:34:59', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1240', '10000006', 'text', '好的，郑老师', null, null, '2016-06-22 22:36:16', '2016-06-22 22:36:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1241', '10000006', 'text', '赚钱了#20', null, null, '2016-06-22 22:39:24', '2016-06-22 22:39:24', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1242', '10000000', 'text', '今天是直播室的公测日，所以今晚的直播时间到晚上11点就结束。但是从明天开始，我们直播室的时间将从9点半到凌晨的1点半，全天16个小时的直播服务，陪伴大家的捞金之旅。敬请关注#4', null, null, '2016-06-22 22:39:27', '2016-06-22 22:39:27', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1243', '10000085', 'text', '#20', null, null, '2016-06-22 22:40:45', '2016-06-22 22:40:45', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1244', '10000084', 'text', '#14', null, null, '2016-06-22 22:40:56', '2016-06-22 22:40:56', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1245', '10000000', 'text', '下面把时间交回给伍老师', null, null, '2016-06-22 22:41:52', '2016-06-22 22:41:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1246', '10000084', 'text', '#20', null, null, '2016-06-22 22:42:38', '2016-06-22 22:42:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1247', '8', 'text', '好的，接下来我们还是继续关注EIA后续行情走势。', null, null, '2016-06-22 22:43:00', '2016-06-22 22:43:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1248', '8', 'text', '数据公布后，原油短线下挫1%。', null, null, '2016-06-22 22:44:02', '2016-06-22 22:44:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1249', '10000003', 'text', '太棒了#20', null, null, '2016-06-22 22:44:49', '2016-06-22 22:44:49', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1250', '8', 'text', '美国EIA原油库存连续五周录得下滑，和昨晚API数据相左，出乎市场预料。', null, null, '2016-06-22 22:45:11', '2016-06-22 22:45:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1251', '10000080', 'text', '#17', null, null, '2016-06-22 22:45:23', '2016-06-22 22:45:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1252', '10000080', 'text', '#21', null, null, '2016-06-22 22:45:30', '2016-06-22 22:45:30', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1253', '8', 'text', '目前，基本回吐日内涨幅，现报327.5.', null, null, '2016-06-22 22:47:08', '2016-06-22 22:47:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1254', '8', 'text', 'EIA数据显示当周精炼油库存录得增加15.1万桶，预期为增加25.7万桶，前值为增加78.6万桶，连续三周录得增加。', null, null, '2016-06-22 22:49:00', '2016-06-22 22:49:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1255', '8', 'text', '当周俄克拉荷马州库欣地区原油库存录得减少128万桶，前值为增加90.4万桶。为油价提供了一些支撑。', null, null, '2016-06-22 22:50:34', '2016-06-22 22:50:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1256', '8', 'text', 'EIA显示上周美国汽油日需求触及纪录高位982万桶。同样对油价起支撑作用。', null, null, '2016-06-22 22:51:26', '2016-06-22 22:51:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1257', '8', 'text', 'API原油库存数据虽利好油价，EIA报告也显示美国上周原油产量已降至2014年9月来低点；但EIA原油库存、库欣原油库存降幅不及预期，且汽油和精炼油都录得增长，能源情报机构也报道称欧盟地区原油库存录得3年来最高水平，多种利空因素使得油价在EIA数据公布后回吐早间涨幅。', null, null, '2016-06-22 22:55:43', '2016-06-22 22:55:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1258', '8', 'text', '大家晚上好', null, null, '2016-06-22 22:57:26', '2016-06-22 22:57:26', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1259', '8', 'text', '晚上好', null, null, '2016-06-22 23:00:09', '2016-06-22 23:00:09', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1260', '8', 'text', '今晚没什么行情，大家早点睡', null, null, '2016-06-22 23:03:47', '2016-06-22 23:03:47', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1261', '8', 'text', '刚才是技术人员在测试，今天的直播就到这里结束了，大家晚安！明天再见~#9', null, null, '2016-06-22 23:06:52', '2016-06-22 23:06:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1262', '5', 'text', '#1', null, null, '2016-06-23 08:57:55', '2016-06-23 08:57:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1263', '10000006', 'text', '郑老师，昨晚的空单好牛啊#20', null, null, '2016-06-23 09:01:01', '2016-06-23 09:01:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1264', '10000001', 'text', '大家早上好！#9', null, null, '2016-06-23 09:30:13', '2016-06-23 09:30:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1265', '10000001', 'text', '人生的磨难是很多的，所以我们不可对于每一件轻微的伤害都过于敏感。在生活磨难面前，精神上的坚强和无动于衷是我们抵抗罪恶和人生意外的最好武器。 —— 洛克', null, null, '2016-06-23 09:34:38', '2016-06-23 09:34:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1266', '10000001', 'text', '好了，接下来回顾一下昨天行情', null, null, '2016-06-23 09:38:34', '2016-06-23 09:38:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1267', '10000001', 'text', '昨晚在EIA数据公布之前，油价一直在短期支撑位330附近盘旋', null, null, '2016-06-23 09:39:36', '2016-06-23 09:39:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1268', '10000001', 'text', 'EIA数据公布6月17日当周原油库存录得减少91.7万桶，数据公布后，虽然库存连续五周下降，但低于预期和汽油和精炼油库存双双增加，而且与周三凌晨API录得的-522万桶相差太远。油价急速下跌', null, null, '2016-06-23 09:40:27', '2016-06-23 09:40:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1269', '10000001', 'text', '直接跌破短期支撑位325，下挫到了下方支撑位320位置', null, null, '2016-06-23 09:40:51', '2016-06-23 09:40:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1270', '10000001', 'text', '但在凌晨美联储主席表示美国的通胀指标预期下降和机构公布的数据显示英国留欧的概率提升，油价在支撑位320附近快速反弹至325上方', null, null, '2016-06-23 09:41:06', '2016-06-23 09:41:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1271', '10000102', 'text', '对啊，我没敢拿那么久，就平了#2', null, null, '2016-06-23 09:43:15', '2016-06-23 09:43:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1272', '10000013', 'text', '可以做多吗现在？', null, null, '2016-06-23 09:44:09', '2016-06-23 09:44:09', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1273', '10000102', 'text', '对啊，今天什么时候开始公投？', null, null, '2016-06-23 09:45:50', '2016-06-23 09:45:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1274', '10000001', 'text', '哈哈，昨晚我们郑老师的单子盈利率达到了80%，顺利的止盈离场了#14', null, null, '2016-06-23 09:45:50', '2016-06-23 09:45:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1275', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b3f7045b24.jpeg', null, null, '2016-06-23 09:46:24', '2016-06-23 09:46:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1276', '10000001', 'text', '可以先等待，早上的行情比较清淡，盈利空间比较小！', '10000013', '可以做多吗现在？', '2016-06-23 09:48:00', '2016-06-23 09:48:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1277', '10000001', 'text', '北京时间14:00开始正式投票！', '10000102', '对啊，今天什么时候开始公投？', '2016-06-23 09:48:35', '2016-06-23 09:48:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1278', '10000004', 'text', '#9', null, null, '2016-06-23 09:51:05', '2016-06-23 09:51:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1279', '10000001', 'text', '好了，接下来有请我们的郑老师#14', null, null, '2016-06-23 09:58:50', '2016-06-23 09:58:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1280', '10000000', 'text', '大家早上好！#4', null, null, '2016-06-23 09:59:33', '2016-06-23 09:59:33', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1281', '10000006', 'text', '去不小心睡着了，醒来单子就没了#2', null, null, '2016-06-23 10:01:50', '2016-06-23 10:01:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1282', '10000000', 'text', '昨晚的EIA之夜群情汹涌，早盘吃完前菜以后晚上我们的大餐同样非常丰富。两单合计104.9%，直接翻倍！大家把握住了吗？#4', null, null, '2016-06-23 10:02:32', '2016-06-23 10:02:32', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1283', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b434a9a542.jpg', null, null, '2016-06-23 10:02:50', '2016-06-23 10:02:50', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1284', '10000000', 'text', '有我们轩湛投资观察室的精英团队为大家做专业的指导，大家的财富就会稳步增长！', null, null, '2016-06-23 10:06:57', '2016-06-23 10:06:57', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1285', '10000000', 'text', '好了，回顾了昨晚的走势，下面就来看看今天行情的焦点。', null, null, '2016-06-23 10:10:14', '2016-06-23 10:10:14', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1286', '10000000', 'text', '举世瞩目的英国退欧公投将在今天下午14:00进行第一轮投票。', null, null, '2016-06-23 10:11:06', '2016-06-23 10:11:06', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1287', '10000000', 'text', '那么下面我们看看退欧与否对油价的可能性影响', null, null, '2016-06-23 10:13:41', '2016-06-23 10:13:41', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1288', '10000000', 'text', '若英国脱欧势必立即引发市场波动，这可能会进一步打击投资者对于大宗商品等风险资产的情绪。英国脱欧将使美元走强，而强势美元同样也会使油价受挫。此外，长期看来，英国经济可能会冲击欧洲经济，从而额削弱这一地区的石油需求。', null, null, '2016-06-23 10:13:56', '2016-06-23 10:13:56', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1289', '10000000', 'text', '如果不退欧，则对稳定风险情绪有很大的作用，油价也会稳步上涨', null, null, '2016-06-23 10:15:52', '2016-06-23 10:15:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1290', '10000000', 'text', '技术上，原油目前有筑顶的迹象，跌破今年的上涨趋势线以后，原油后市只要跌破318，那么就会彻底转弱。', null, null, '2016-06-23 10:19:01', '2016-06-23 10:19:01', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1291', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b4741c23aa.jpg', null, null, '2016-06-23 10:19:45', '2016-06-23 10:19:45', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1292', '10000000', 'text', '错过了上半年牛市的你，还在犹豫要错过下半年的熊市做空翻倍的机会吗？#4', null, null, '2016-06-23 10:24:04', '2016-06-23 10:24:04', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1293', '10000000', 'text', '退欧公投，将是原油后市行情爆发的引爆点！', null, null, '2016-06-23 10:24:24', '2016-06-23 10:24:24', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1294', '10000109', 'text', '#9', null, null, '2016-06-23 10:30:07', '2016-06-23 10:30:07', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1295', '10000000', 'text', '那么这两天的行情，大家一定要重点关注。我也会在直播室给大家带来公投的全过程。#4', null, null, '2016-06-23 10:31:18', '2016-06-23 10:31:18', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1296', '10000102', 'text', '今天下午两点开始，明天下午两点出结果，是吗？', null, null, '2016-06-23 10:32:40', '2016-06-23 10:32:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1297', '10000109', 'text', '今天下午行情会有多大的波动啊老师！我怕我下午没有时间！能不能给我提前来个电话！', null, null, '2016-06-23 10:37:01', '2016-06-23 10:37:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1298', '10000001', 'text', '今日14:00-明日5:00进行投票；明日7:00-14:00将陆续公布各个选区计票结果', '10000102', '今天下午两点开始，明天下午两点出结果，是吗？', '2016-06-23 10:37:20', '2016-06-23 10:37:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1299', '10000000', 'text', '近两年发生的政治金融事件都会至少带给原油5%以上的波动，2015年初的瑞士法郎脱钩事件更是让原油下跌了10%！', '10000109', '今天下午行情会有多大的波动啊老师！我怕我下午没有时间！能不能给我提前来个电话！', '2016-06-23 10:44:09', '2016-06-23 10:44:09', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1300', '10000000', 'text', '如果你想参与，可以电话咨询一下你的投顾#15', '10000109', '今天下午行情会有多大的波动啊老师！我怕我下午没有时间！能不能给我提前来个电话！', '2016-06-23 10:44:44', '2016-06-23 10:44:44', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1301', '10000000', 'text', '早盘的原油还没有启动，那么下面我们来看看股市的情况', null, null, '2016-06-23 10:58:37', '2016-06-23 10:58:37', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1302', '10000000', 'text', '早盘的原油还没有启动，那么下面我们来看看股市的情况', null, null, '2016-06-23 10:58:45', '2016-06-23 10:58:45', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1303', '10000013', 'text', '郑老师现在可以做多吗？？？？', null, null, '2016-06-23 11:01:36', '2016-06-23 11:01:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1304', '10000013', 'text', '可以吗？？？？', null, null, '2016-06-23 11:01:45', '2016-06-23 11:01:45', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1305', '10000000', 'text', '早盘大盘依然维持一个震荡的格局，成交量依然较为低迷。有色，券商等板块跌幅居前。创业板和中小板也是表现平淡', null, null, '2016-06-23 11:01:49', '2016-06-23 11:01:49', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1306', '10000000', 'text', '现在的走势经过昨晚的拉升还是较为强势的，如果做多，止损322一定要带好。上看330', '10000013', '郑老师现在可以做多吗？？？？', '2016-06-23 11:03:04', '2016-06-23 11:03:04', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1307', '10000000', 'text', '早盘大盘依然维持一个震荡的格局，成交量依然较为低迷。有色，券商等板块跌幅居前。创业板和中小板也是表现平淡', null, null, '2016-06-23 11:03:17', '2016-06-23 11:03:17', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1308', '10000000', 'text', '现在的走势经过昨晚的拉升还是较为强势的，如果做多，止损322一定要带好。上看330', '10000013', '郑老师现在可以做多吗？？？？', '2016-06-23 11:03:23', '2016-06-23 11:03:23', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1309', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b521108796.jpg', null, null, '2016-06-23 11:05:53', '2016-06-23 11:05:53', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1310', '10000000', 'text', '大盘短期的走势还是较为强势的，走出了一个多头持续的形态，上方压力是2940附近', null, null, '2016-06-23 11:06:43', '2016-06-23 11:06:43', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1311', '10000000', 'text', '板块方面，资金南下抢筹港股创一年多新高，深港通概念股遭爆炒。深港通到底什么时候宣布开通？对于这个问题市场存在各种猜测但没有定论。不过内地资金还是用积极南下布局港股，6月22日内地资金通过港股通使用了33.32亿元人民币的额度，这个“扫货”力度创下了一年多以来的新高。', null, null, '2016-06-23 11:14:25', '2016-06-23 11:14:25', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1312', '10000000', 'text', '但消息面催动的上涨可持续性存疑，投资者须谨慎操作，可以适当关注新能源、券商以及半导体集成电路板块的投资机会，中线建议关注国企供给侧改革、白酒、零售等基本面向好的投资机会。', null, null, '2016-06-23 11:23:44', '2016-06-23 11:23:44', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1313', '10000000', 'text', '个股推荐关注：1. 京运通。战略性布局售电，期待模式创新：公司公告拟出资3024万元参与投资设立北京融和晟源售电公司，持股15%。售电公司或成为第一家全国性售电公司。', null, null, '2016-06-23 11:31:27', '2016-06-23 11:31:27', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1314', '10000000', 'text', '个股推荐关注：1. 京运通。战略性布局售电，期待模式创新：公司公告拟出资3024万元参与投资设立北京融和晟源售电公司，持股15%。售电公司或成为第一家全国性售电公司。', null, null, '2016-06-23 11:31:35', '2016-06-23 11:31:35', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1315', '10000000', 'text', '2. 通鼎互联。收购百卓网络、微能科技，对价14.8亿元，募集配套资金9.72亿元：2016年6月8日，公司发布定增预案，拟发行股份收购百卓网络100%股权，交易对价10亿，收购微能科技100%股份，交易对价4.8亿元。发行股份购买资产发行价格14.05元。同时拟向不超过10名符合条件的特定对象发行股份募集配套资金，配套资金预估不超过9.72亿元，募集配套资金发行底价为13.3元。', null, null, '2016-06-23 11:32:29', '2016-06-23 11:32:29', '1', '#e71516', '18');
INSERT INTO `chat_chat` VALUES ('1316', '10000000', 'text', '3. 恩华药业。高管增持股份，彰显发展信心：公司总经理孙家权于2016年6月21日以自有资金在二级市场上以19.18元/股的价格买入公司股份20.85万股，占公司股份的0.033%，买入金额399.9万元。孙家权此前通过持有恩华投资1.48%的股权间接持有公司股份，本次买入后直接与通过恩华投资间接合计持有公司0.55%的股份，彰显了高管对公司未来发展的信心。', null, null, '2016-06-23 11:33:30', '2016-06-23 11:33:30', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1317', '10000000', 'text', '早盘已经收盘，大盘跌0.83%，失守2900', null, null, '2016-06-23 11:40:27', '2016-06-23 11:40:27', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1318', '10000102', 'text', '现在做空可以吗？', null, null, '2016-06-23 11:41:49', '2016-06-23 11:41:49', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1319', '10000000', 'text', '今天有退欧公投，建议等反弹到330附近就进场操作。带好止损止盈', '10000102', '现在做空可以吗？', '2016-06-23 11:43:41', '2016-06-23 11:43:41', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1320', '10000102', 'text', '好的，老师#9', null, null, '2016-06-23 11:44:17', '2016-06-23 11:44:17', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1321', '10000102', 'text', '330空，止盈止损怎么设', null, null, '2016-06-23 11:45:47', '2016-06-23 11:45:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1322', '10000000', 'text', '原油方面早盘小幅震荡回落，短期可能维持区间319~329波动', null, null, '2016-06-23 11:47:38', '2016-06-23 11:47:38', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1323', '10000000', 'text', '止损334，止盈324', '10000102', '330空，止盈止损怎么设', '2016-06-23 11:47:54', '2016-06-23 11:47:54', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1324', '10000102', 'text', '谢谢，老师#14', null, null, '2016-06-23 11:48:20', '2016-06-23 11:48:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1325', '10000000', 'text', '好了，行情暂时比较清淡，大家可以先去吃饭，稍后一点我们再来重点关注下午的公投#4', null, null, '2016-06-23 11:53:21', '2016-06-23 11:53:21', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1326', '10000000', 'text', '大家下午好！欢迎来到英国退欧公投专场！#9#14', null, null, '2016-06-23 13:03:20', '2016-06-23 13:03:20', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1327', '10000013', 'text', '郑老师好#14#9', null, null, '2016-06-23 13:03:41', '2016-06-23 13:03:41', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1328', '10000102', 'text', '来咯！#6', null, null, '2016-06-23 13:04:26', '2016-06-23 13:04:26', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1329', '10000037', 'text', '#14', null, null, '2016-06-23 13:04:32', '2016-06-23 13:04:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1330', '10000079', 'text', '#14', null, null, '2016-06-23 13:04:54', '2016-06-23 13:04:54', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1331', '10000078', 'text', '#9', null, null, '2016-06-23 13:05:14', '2016-06-23 13:05:14', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1332', '10000006', 'text', '#14', null, null, '2016-06-23 13:06:16', '2016-06-23 13:06:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1333', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b6e58c774b.jpg', null, null, '2016-06-23 13:06:32', '2016-06-23 13:06:32', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1334', '10000000', 'text', '【退欧公投专场】由于中国比英国当地时间快了7个小时，所以现在英国是早上的6点10分左右。', null, null, '2016-06-23 13:10:31', '2016-06-23 13:10:31', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1335', '10000000', 'text', '【退欧公投专场】而民众投票时间将在北京时间下午14:00，也就是英国当地时间早上7点开放投票。', null, null, '2016-06-23 13:11:54', '2016-06-23 13:11:54', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1336', '10000000', 'text', '【退欧公投专场】下面先看看英国各大政要对公投的态度：', null, null, '2016-06-23 13:18:01', '2016-06-23 13:18:01', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1337', '10000000', 'text', '【退欧公投专场】英国女王暗示支持脱欧是误传。英国独立报报道称，太阳报曾于3月份以“女王支持脱欧”作大字标题出版刊物，英国新闻监管机构“独立媒体标准组织”上月裁定，太阳报上述举动是“严重误导”并违反了出版规定；白金汉宫亦表示女王始终坚持“政治中立”。	', null, null, '2016-06-23 13:18:54', '2016-06-23 13:18:54', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1338', '5', 'text', '#9', null, null, '2016-06-23 13:20:51', '2016-06-23 13:20:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1339', '10000000', 'text', '【退欧公投专场】英国退欧公投已经只剩下很短的时间了。而英国首相卡梅伦还在做最后一分钟努力，在唐宁街首相府外发表电视直播演说，希望说服英国人选择留在欧盟。卡梅伦说：“英国人不退缩。”由于较年长者被视为较为疑欧，也较可能参加公投，卡梅伦直接对这些人喊话说：“投票亭里只会有你一个人。只有你，能做出影响你的未来、你孩子的未来、你孙子的未来的决定。”', null, null, '2016-06-23 13:22:56', '2016-06-23 13:22:56', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1340', '10000000', 'text', '【退欧公投专场】卡梅伦也重申，脱欧将危害到英国经济和国家安全，同时导致就业机会减少和物价上涨。最新公布的民调显示，留欧支持率的领先幅度，缩小到只剩下一个百分点。评论员说，卡梅伦匆促发表电视演说，显示留欧阵营非常忧心公投结果。支持脱欧的英国保险业大亨班克斯在推特上说：“卡梅伦慌了。情况已不受他的控制。”', null, null, '2016-06-23 13:26:19', '2016-06-23 13:26:19', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1341', '10000000', 'text', '【退欧公投专场】卡梅伦也重申，脱欧将危害到英国经济和国家安全，同时导致就业机会减少和物价上涨。最新公布的民调显示，留欧支持率的领先幅度，缩小到只剩下一个百分点。评论员说，卡梅伦匆促发表电视演说，显示留欧阵营非常忧心公投结果。支持脱欧的英国保险业大亨班克斯在推特上说：“卡梅伦慌了。情况已不受他的控制。”', null, null, '2016-06-23 13:26:27', '2016-06-23 13:26:27', '1', '#e71516', '18');
INSERT INTO `chat_chat` VALUES ('1342', '10000000', 'text', '【退欧公投专场】我们的吴老师也来了，下面把时间交给吴老师讲讲目前的盘面情况！#14', null, null, '2016-06-23 13:31:44', '2016-06-23 13:31:44', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1343', '10000079', 'text', '#9', null, null, '2016-06-23 13:33:27', '2016-06-23 13:33:27', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1344', '10000001', 'text', '大家下午好！#9', null, null, '2016-06-23 13:33:30', '2016-06-23 13:33:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1345', '10000102', 'text', '那是不是不退欧的几率更大?', null, null, '2016-06-23 13:33:46', '2016-06-23 13:33:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1346', '10000001', 'text', '【退欧公投专场】目前油价涨幅回调盘整，为公投蓄势而发！', null, null, '2016-06-23 13:38:38', '2016-06-23 13:38:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1347', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b75ef59b6f.png', null, null, '2016-06-23 13:38:55', '2016-06-23 13:38:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1348', '10000013', 'text', '老师我的空单在326买的现在可以出吗？', '10000079', '#9', '2016-06-23 13:40:36', '2016-06-23 13:40:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1349', '10000001', 'text', '【退欧公投专场】昨晚公布两项民调数据均显示脱欧支持率暂时领先，空头蠢蠢欲动！', null, null, '2016-06-23 13:43:24', '2016-06-23 13:43:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1350', '10000001', 'text', '可以，我的看法建议留着观察，公投还没开始，油价还不确定会怎么走呢，但一定设好止损止盈喔', '10000013', '老师我的空单在326买的现在可以出吗？', '2016-06-23 13:46:52', '2016-06-23 13:46:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1351', '10000013', 'text', '知道了，谢谢老师#9', null, null, '2016-06-23 13:48:07', '2016-06-23 13:48:07', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1352', '10000001', 'text', '目前退欧阵营和留欧阵营势均力敌！', '10000102', '那是不是不退欧的几率更大?', '2016-06-23 13:48:11', '2016-06-23 13:48:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1353', '10000104', 'text', '这个走势是破位了吧', null, null, '2016-06-23 13:49:56', '2016-06-23 13:49:56', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1354', '10000100', 'text', '现在买多，买空', null, null, '2016-06-23 13:50:16', '2016-06-23 13:50:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1355', '10000104', 'text', '？', null, null, '2016-06-23 13:50:31', '2016-06-23 13:50:31', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1356', '10000000', 'text', '目前还在区间319~329震荡。', '10000104', '这个走势是破位了吧', '2016-06-23 13:52:25', '2016-06-23 13:52:25', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1357', '10000000', 'text', '从结构来看是震荡偏多', '10000100', '现在买多，买空', '2016-06-23 13:52:43', '2016-06-23 13:52:43', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1358', '10000000', 'text', '【退欧公投专场】投票开放时间马上开始了！', null, null, '2016-06-23 13:57:42', '2016-06-23 13:57:42', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1359', '10000000', 'text', '【退欧公投专场】下面就让我们来连线我们的英国特派分析师小罗老师！看看她从英国曼切斯特发回的图文报道。#14', null, null, '2016-06-23 14:01:57', '2016-06-23 14:01:57', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1360', '10000102', 'text', '#3', null, null, '2016-06-23 14:03:23', '2016-06-23 14:03:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1361', '10000103', 'text', '【退欧公投专场】大家好，现在就由我来公投现场直播！', null, null, '2016-06-23 14:03:28', '2016-06-23 14:03:28', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1362', '10000103', 'text', '【退欧公投专场】现在是英国当地时间早上7点，由于明天最终的公投结果会在英国的曼彻斯特市政厅公布，所以我现在在离曼彻斯特市政厅一公里外的Mount大街.', null, null, '2016-06-23 14:07:15', '2016-06-23 14:07:15', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1363', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b7dd313604.jpeg', null, null, '2016-06-23 14:12:35', '2016-06-23 14:12:35', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1364', '10000103', 'text', '【退欧公投专场】现在时间还比较早，街上的人还不是很多，但是也能够感受到退欧公投的紧绷感', null, null, '2016-06-23 14:13:55', '2016-06-23 14:13:55', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1365', '10000103', 'text', '【退欧公投专场】现在我继续往前走，先来看看曼彻斯特的著名的景点：市政厅', null, null, '2016-06-23 14:18:56', '2016-06-23 14:18:56', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1366', '10000001', 'text', '【退欧公投专场】英国国内约40000个投票点已正式开启，投票共计将持续15个小时！', '10000104', '这个走势是破位了吧', '2016-06-23 14:23:38', '2016-06-23 14:23:38', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1367', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b808aac76b.jpeg', null, null, '2016-06-23 14:24:10', '2016-06-23 14:24:10', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1368', '10000001', 'text', '【退欧公投专场】英国国内约40000个投票点已正式开启，投票共计将持续15个小时', null, null, '2016-06-23 14:24:56', '2016-06-23 14:24:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1369', '10000104', 'text', '这次退欧事件让我赚钱了我就去趟欧洲', null, null, '2016-06-23 14:27:32', '2016-06-23 14:27:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1370', '10000104', 'text', '他们也挺不容易的，支持下他们的消费', null, null, '2016-06-23 14:28:03', '2016-06-23 14:28:03', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1371', '10000104', 'text', '#4', null, null, '2016-06-23 14:28:12', '2016-06-23 14:28:12', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1372', '10000103', 'text', '【退欧公投专场】曼彻斯特外观非常雄伟！我打算进去看看里面的装饰#3', null, null, '2016-06-23 14:28:42', '2016-06-23 14:28:42', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1373', '10000104', 'text', '看来要大跌了', null, null, '2016-06-23 14:29:34', '2016-06-23 14:29:34', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1374', '10000001', 'text', '【退欧公投专场】公投开始后，英镑短线下挫60余点，油价下跌17余点', null, null, '2016-06-23 14:29:39', '2016-06-23 14:29:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1375', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b8207d42b3.png', null, null, '2016-06-23 14:30:31', '2016-06-23 14:30:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1376', '10000001', 'text', '如你所愿！#9', '10000104', '这次退欧事件让我赚钱了我就去趟欧洲', '2016-06-23 14:30:50', '2016-06-23 14:30:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1377', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b826f4b072.jpeg', null, null, '2016-06-23 14:32:15', '2016-06-23 14:32:15', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1378', '10000103', 'text', '【退欧公投专场】由于市政厅钟楼的办公区还没到点开放，我就先去到廊区先去看下十九世纪的雕像', null, null, '2016-06-23 14:33:22', '2016-06-23 14:33:22', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1379', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b82f1b3f70.jpeg', null, null, '2016-06-23 14:34:25', '2016-06-23 14:34:25', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1380', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b835f6e22d.jpeg', null, null, '2016-06-23 14:36:15', '2016-06-23 14:36:15', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1381', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b83a982924.jpeg', null, null, '2016-06-23 14:37:29', '2016-06-23 14:37:29', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1382', '10000103', 'text', '【退欧公投专场】市政厅的雕像真是栩栩如生，复古风格的装饰也体现了英国人独有的一面。', null, null, '2016-06-23 14:37:52', '2016-06-23 14:37:52', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1383', '10000102', 'text', 'The Queen\'s Gallery', null, null, '2016-06-23 14:40:24', '2016-06-23 14:40:24', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1384', '10000103', 'text', '【退欧公投专场】从市政厅出来，街头上的人越来越多了。', null, null, '2016-06-23 14:43:01', '2016-06-23 14:43:01', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1385', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b859caca47.jpeg', null, null, '2016-06-23 14:45:48', '2016-06-23 14:45:48', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1386', '10000103', 'text', '【退欧公投专场】有不少人在排队，问了一下，是在换美元。原来英国当地人都担心一旦脱欧，英镑会大幅贬值。所以先换美元以防后患。', null, null, '2016-06-23 14:46:33', '2016-06-23 14:46:33', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1387', '10000103', 'text', '【退欧公投专场】曼彻斯特目前的天空有点昏沉，温度也不是很高，估计也就二十一二度，感觉有点凉。但是并没有影响民众投票的热情。', null, null, '2016-06-23 14:54:55', '2016-06-23 14:54:55', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1388', '10000001', 'text', '【退欧公投专场】公投开始后，避险情绪上涨，油价继续小幅下跌', null, null, '2016-06-23 14:55:00', '2016-06-23 14:55:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1389', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b87dd284c3.png', null, null, '2016-06-23 14:55:25', '2016-06-23 14:55:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1390', '10000032', 'text', '#8', null, null, '2016-06-23 14:55:47', '2016-06-23 14:55:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1391', '10000001', 'text', '【退欧公投专场】下方支撑322-321，如果下破下方支撑位，油价将会继续下跌', null, null, '2016-06-23 14:59:35', '2016-06-23 14:59:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1392', '10000102', 'text', '还会跌吗？', null, null, '2016-06-23 15:01:17', '2016-06-23 15:01:17', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1393', '10000102', 'text', '现在进场可以吗？', null, null, '2016-06-23 15:01:26', '2016-06-23 15:01:26', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1394', '10000001', 'text', '可以，但一定要设好止盈止损喔', '10000102', '现在进场可以吗？', '2016-06-23 15:05:17', '2016-06-23 15:05:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1395', '10000103', 'text', '【退欧公投专场】因为现在公投正有条不紊得进行中，时间长达十几个小时，那么，我就先从曼彻斯特坐车去伦敦，看看伦敦当地的情况，如果车上有空，就给大家介绍一下英国的当地人文风情和美食', null, null, '2016-06-23 15:06:37', '2016-06-23 15:06:37', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1396', '10000103', 'text', '#6#4', null, null, '2016-06-23 15:07:00', '2016-06-23 15:07:00', '-1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1397', '10000008', 'text', '小秘书 老师，你们辛苦了#9', null, null, '2016-06-23 15:13:55', '2016-06-23 15:13:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1398', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b8c6ae1d15.png', null, null, '2016-06-23 15:14:50', '2016-06-23 15:14:50', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1399', '10000001', 'text', '现在反弹，有建仓空单的机会，现在可以尝试做一下空单', null, null, '2016-06-23 15:18:04', '2016-06-23 15:18:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('1400', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b8da1f0413.png', null, null, '2016-06-23 15:20:02', '2016-06-23 15:20:02', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1401', '10000005', 'text', '小秘书是谁，漂亮不#4', null, null, '2016-06-23 15:29:25', '2016-06-23 15:29:25', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1402', '10000103', 'text', '【退欧公投专场】经过坐车，我来到了曼彻斯特的Piccadilly火车站。这里的建筑不仅现代化之余，也透着浓浓的复古气息。同时也不得不可惜英国工业没落。', null, null, '2016-06-23 15:33:57', '2016-06-23 15:33:57', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1403', '10000103', 'text', '【退欧公投专场】经过坐车，我来到了曼彻斯特的Piccadilly火车站。这里的建筑不仅现代化之余，也透着浓浓的复古气息。同时也不得不可惜英国工业没落。', null, null, '2016-06-23 15:34:08', '2016-06-23 15:34:08', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1404', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b91285efc1.jpeg', null, null, '2016-06-23 15:35:04', '2016-06-23 15:35:04', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1405', '10000103', 'text', '【退欧公投专场】走进火车站里面，各种设施一应俱全，已经达到了国内的飞机场的配置级别#4', null, null, '2016-06-23 15:39:22', '2016-06-23 15:39:22', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1406', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b923e5a5a0.jpeg', null, null, '2016-06-23 15:39:42', '2016-06-23 15:39:42', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1407', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b92a001598.jpeg', null, null, '2016-06-23 15:41:20', '2016-06-23 15:41:20', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1408', '10000103', 'text', '【退欧公投专场】离开曼彻斯特之前，来个纪念照吧#5', null, null, '2016-06-23 15:45:15', '2016-06-23 15:45:15', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1409', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576b93981fcf4.jpeg', null, null, '2016-06-23 15:45:28', '2016-06-23 15:45:28', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1410', '10000103', 'text', '【退欧公投专场】帮我拍照的英国人还是很热情滴#4', null, null, '2016-06-23 15:47:17', '2016-06-23 15:47:17', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1411', '10000013', 'text', '小秘书你穿这么多不热吗', null, null, '2016-06-23 15:48:36', '2016-06-23 15:48:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1412', '10000103', 'text', '英国现在天气还是二十度以下呢#15', '10000013', '小秘书你穿这么多不热吗', '2016-06-23 15:50:38', '2016-06-23 15:50:38', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1413', '10000103', 'text', '【退欧公投专场】火车到了，伦敦我来了！ #3', null, null, '2016-06-23 15:51:37', '2016-06-23 15:51:37', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1414', '10000032', 'text', '#7', null, null, '2016-06-23 16:01:29', '2016-06-23 16:01:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1415', '10000103', 'text', '【退欧公投专场】大家知道英国人饮食的特点有哪些吗', null, null, '2016-06-23 16:09:59', '2016-06-23 16:09:59', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1416', '10000103', 'text', '【退欧公投专场】话说英国家庭一天通常是四餐：早餐、午餐、午茶点和晚餐。英国人早晨起床前有吃“被窝茶”的习惯，然后传统的英式早餐有煎培根、香肠和煎土司。', null, null, '2016-06-23 16:18:21', '2016-06-23 16:18:21', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1417', '10000103', 'text', '【退欧公投专场】英国人的中餐从简，通常都是三明治，很快就解决', null, null, '2016-06-23 16:22:03', '2016-06-23 16:22:03', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1418', '10000103', 'text', '　【退欧公投专场】一天当中最丰盛的是晚餐，也称正餐，常见的主菜是烤炙肉类浇肉汁，以及牛排、火腿、鱼类等，还有土豆泥、蔬菜沙拉等，一般还要喝啤酒或葡萄酒', null, null, '2016-06-23 16:28:19', '2016-06-23 16:28:19', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1419', '10000103', 'text', '　【退欧公投专场】英国人对饮茶情有独钟，并乐此不疲。喝茶在英国人看来是一种乐趣，为西方各国之冠。', null, null, '2016-06-23 16:30:55', '2016-06-23 16:30:55', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1420', '10000053', 'text', '现在可以做单了没有？', null, null, '2016-06-23 16:49:33', '2016-06-23 16:49:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1421', '10000000', 'text', '吴老师已经在左边给出了操作建议啦#9', '10000053', '现在可以做单了没有？', '2016-06-23 16:57:52', '2016-06-23 16:57:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1422', '10000103', 'text', '【退欧公投专场】总而言之，英国是一个神奇的地方，空气中弥漫着慵懒和激情，仿佛是古典与现代的碰撞。', null, null, '2016-06-23 16:59:03', '2016-06-23 16:59:03', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1423', '10000000', 'text', '【退欧公投专场】吴老师的操作建议现在大家还是可以参考的：325做空，止损330，止盈第一目标先看318', null, null, '2016-06-23 17:00:00', '2016-06-23 17:00:00', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1424', '10000103', 'text', '【退欧公投专场】我先休息下#1郑老师来继续#5', null, null, '2016-06-23 17:01:12', '2016-06-23 17:01:12', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1425', '10000103', 'text', '【退欧公投专场】提到英国的食物，最被人们津津乐道的可能就是炸鱼和薯条，是最普遍的街头小吃，非常受英国人欢迎。炸鱼是将去骨的鱼切成片后裹上湿面炸制而成，外焦里嫩。吃的时候还会配上不同口味的调味酱。尽管褒贬不一，但既然稍后来到了它的故乡，不试试当然会留下遗憾。', null, null, '2016-06-23 17:10:47', '2016-06-23 17:10:47', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1426', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576ba7bcf2f50.jpeg', null, null, '2016-06-23 17:11:25', '2016-06-23 17:11:25', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1427', '10000103', 'text', '【退欧公投专场】让我在火车上也忍不住点了一份炸鱼和薯条#4', null, null, '2016-06-23 17:12:16', '2016-06-23 17:12:16', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1428', '10000103', 'text', '【退欧公投专场】大家是不是也嘴馋啦#5', null, null, '2016-06-23 17:12:57', '2016-06-23 17:12:57', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1429', '10000103', 'text', '【退欧公投专场】窗外开始下雨了\n', null, null, '2016-06-23 17:16:35', '2016-06-23 17:16:35', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1430', '10000103', 'text', '【退欧公投专场】不知道大雨是否会影响公投的进程。不过我的心情却好极了#5', null, null, '2016-06-23 17:17:55', '2016-06-23 17:17:55', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1431', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576ba9c8a3511.jpeg', null, null, '2016-06-23 17:20:08', '2016-06-23 17:20:08', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1432', '10000000', 'text', '【退欧公投专场】英国博彩公司必发最新赌注显示，英国留欧的几率为78%。导致原油出现小幅反弹', null, null, '2016-06-23 17:21:46', '2016-06-23 17:21:46', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1433', '10000103', 'text', '【退欧公投专场】下面继续为大家送上好吃的牛排#4', null, null, '2016-06-23 17:21:57', '2016-06-23 17:21:57', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1434', '10000103', 'text', '【退欧公投专场】牛排是西餐当中的经典菜式，听说英国人对牛排的喜好和痴迷可以追溯到两百多年前。英国最有名的牛排当数英国最古老的肉牛品种之一的安格斯牛。#3', null, null, '2016-06-23 17:23:05', '2016-06-23 17:23:05', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1435', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576baaa7e5a0b.jpeg', null, null, '2016-06-23 17:23:51', '2016-06-23 17:23:51', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1436', '10000103', 'text', '【退欧公投专场】鳗鱼冻是东伦敦的一种传统小吃，做法是把新鲜鳗鱼加上醋和香料水煮之后，放置冷却会自然形成凝胶状。鳗鱼冻看起来样子好像不怎么样，但伦敦人却很喜欢它。听说鳗鱼冻的确有种不一样的风味，下火车后一定去试', null, null, '2016-06-23 17:28:13', '2016-06-23 17:28:13', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1437', '10000103', 'text', '#17', null, null, '2016-06-23 17:28:29', '2016-06-23 17:28:29', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1438', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576babd850ac8.jpeg', null, null, '2016-06-23 17:28:56', '2016-06-23 17:28:56', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1439', '10000000', 'text', '【退欧公投专场】英国首相卡梅伦已经投票。', null, null, '2016-06-23 17:35:15', '2016-06-23 17:35:15', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1440', '10000000', 'text', '【退欧公投专场】苏格兰首席部长兼民族党党魁斯图金已投票，她昨日曾表示若英国果真退欧，苏格兰将寻求独立然后重新加入欧盟，并支持卡梅伦呼吁选民留欧。', null, null, '2016-06-23 17:37:37', '2016-06-23 17:37:37', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1441', '10000103', 'text', '【退欧公投专场】坐了两个小时的火车，终于到了伦敦了#4', null, null, '2016-06-23 17:49:10', '2016-06-23 17:49:10', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1442', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576bb0cdb5ebd.jpeg', null, null, '2016-06-23 17:50:05', '2016-06-23 17:50:05', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1443', '10000000', 'text', '【退欧公投专场】英国博彩公司立博最新赌注中，55%押注英国脱欧。', null, null, '2016-06-23 17:51:49', '2016-06-23 17:51:49', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1444', '10000103', 'text', '【退欧公投专场】伦敦的雨还是很大，我先到酒店躲一下。把时间交回给直播室的其他老师，这边有什么突发情况我会第一时间告诉大家#6#6', null, null, '2016-06-23 18:00:31', '2016-06-23 18:00:31', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1445', '10000000', 'text', '【退欧公投专场】下面把时间交给伍老师', null, null, '2016-06-23 18:01:24', '2016-06-23 18:01:24', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1446', '8', 'text', '【退欧公投专场】好的，今天各位老师都辛苦了，特别是我们的小秘书同学#9#9', null, null, '2016-06-23 18:02:07', '2016-06-23 18:02:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1447', '8', 'text', '【退欧公投专场】为了给大家带来更好的直播体验，去到了公投现场冒雨直播。#14#14', null, null, '2016-06-23 18:03:27', '2016-06-23 18:03:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1448', '10000103', 'text', '【退欧公投专场】#1今晚就辛苦伍老师给大家直播啦', null, null, '2016-06-23 18:04:25', '2016-06-23 18:04:25', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1449', '10000205', 'text', '老师，怎么行情反弹了', null, null, '2016-06-23 18:05:13', '2016-06-23 18:05:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1450', '8', 'text', '【退欧公投专场】退欧公投从下午2:00到现在已经进行了4个小时了。', null, null, '2016-06-23 18:06:09', '2016-06-23 18:06:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1451', '8', 'text', '【退欧公投专场】最新民调中，留欧支持率从47%增加至52%，脱欧从53%减少至48%。', null, null, '2016-06-23 18:08:11', '2016-06-23 18:08:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1452', '8', 'text', '公投最终结果没有出来，市场避险情绪有所反复引起油价波动这都是正常的。', '10000205', '老师，怎么行情反弹了', '2016-06-23 18:11:46', '2016-06-23 18:11:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1453', '8', 'text', '【退欧公投专场】英国博彩公司立博最新赌注中，55%押注英国脱欧。', null, null, '2016-06-23 18:13:21', '2016-06-23 18:13:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1454', '8', 'text', '【退欧公投专场】最终结果还是具有很大不确定性。', null, null, '2016-06-23 18:14:23', '2016-06-23 18:14:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1455', '8', 'text', '【退欧公投专场】亚洲将是英国脱欧公投后第一个开始交易的市场，预计市场的波动将会相当之剧烈。', null, null, '2016-06-23 18:17:01', '2016-06-23 18:17:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1456', '8', 'text', '【退欧公投专场】大家手上有单的，一定要提前做好风险控制。', null, null, '2016-06-23 18:19:24', '2016-06-23 18:19:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1457', '8', 'text', '【退欧公投专场】目前，留欧阵营是占上风的。', null, null, '2016-06-23 18:28:21', '2016-06-23 18:28:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1458', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576bbb5bc225b.png', null, null, '2016-06-23 18:35:07', '2016-06-23 18:35:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1459', '10000102', 'text', '空单还能拿着吗？', null, null, '2016-06-23 18:37:31', '2016-06-23 18:37:31', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1460', '8', 'text', '【退欧公投专场】目前，留欧阵营占据上风，避险情绪有所下降，原油得以支撑，出现小幅反弹。', null, null, '2016-06-23 18:39:57', '2016-06-23 18:39:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1461', '10000000', 'text', '【退欧公投专场】由于公投的不确定性太大，所以建议手上持有单子的朋友，不管多空，都要带好止损止盈！', null, null, '2016-06-23 18:40:07', '2016-06-23 18:40:07', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1462', '10000000', 'text', '【退欧公投专场】持有空单的朋友，按照吴老师给出的建议做好止损止盈就行。如果不愿意止损，可以在止损位开一个多单代替', null, null, '2016-06-23 18:41:55', '2016-06-23 18:41:55', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1463', '8', 'text', '公投最终结果未出来之前，空单都有机会，但是一定记得带好止损。', '10000102', '空单还能拿着吗？', '2016-06-23 18:42:40', '2016-06-23 18:42:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1464', '10000013', 'text', '老师现在可以加仓空单吗', null, null, '2016-06-23 18:49:36', '2016-06-23 18:49:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1465', '8', 'text', '【退欧公投专场】欧元涨幅超1%', null, null, '2016-06-23 18:56:23', '2016-06-23 18:56:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1466', '8', 'text', '【退欧公投专场】民调显示，55%的投票者支持英国留欧，45%支持退欧。', null, null, '2016-06-23 19:17:42', '2016-06-23 19:17:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1467', '8', 'text', '【退欧公投专场】今晚8:30美国至6月18日当周初请失业金人数 大家适当给与关注。', null, null, '2016-06-23 19:21:15', '2016-06-23 19:21:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1468', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576bc6522f7e1.png', null, null, '2016-06-23 19:21:54', '2016-06-23 19:21:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1469', '8', 'text', '【退欧公投专场】市场预测值略小与前值', null, null, '2016-06-23 19:23:44', '2016-06-23 19:23:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1470', '8', 'text', '【退欧公投专场】若公布值大于预期值，则利空美元，利多原油。', null, null, '2016-06-23 19:25:27', '2016-06-23 19:25:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1471', '8', 'text', '【退欧公投专场】目前，从盘面来看短期阻力支撑在333-320区域。', null, null, '2016-06-23 19:44:05', '2016-06-23 19:44:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1472', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576bcb969b36e.png', null, null, '2016-06-23 19:44:22', '2016-06-23 19:44:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1473', '10000003', 'text', '工作忙了一天...上来找找赚钱机会#4', null, null, '2016-06-23 20:10:38', '2016-06-23 20:10:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1474', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576bd3982434c.png', null, null, '2016-06-23 20:18:32', '2016-06-23 20:18:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1475', '8', 'text', '【退欧公投专场】部分支持脱欧的英国民众称，投票站提供铅笔填写选票是留欧派阴谋，以便点票时篡改结果，但英国政府官方推特表示不强制使用投票站提供笔，民众可以自带笔填写选票。', null, null, '2016-06-23 20:19:08', '2016-06-23 20:19:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1476', '10000102', 'text', '我也在这儿等着呢！想晚上赚一笔', null, null, '2016-06-23 20:21:57', '2016-06-23 20:21:57', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1477', '8', 'text', '【退欧公投专场】还有十分钟公布美国至6月18日当周初请失业金人数，大家可以关注一下。', null, null, '2016-06-23 20:22:17', '2016-06-23 20:22:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1478', '8', 'text', '哈哈，这次20年一遇的行情，会让你赚到尽兴的。', '10000102', '我也在这儿等着呢！想晚上赚一笔', '2016-06-23 20:24:08', '2016-06-23 20:24:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1479', '10000109', 'text', '老师马上就要有数据了现在做单吗？', null, null, '2016-06-23 20:24:54', '2016-06-23 20:24:54', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1480', '8', 'text', '数据利空，可以轻仓做个短空，记得带好止损。', '10000109', '老师马上就要有数据了现在做单吗？', '2016-06-23 20:32:27', '2016-06-23 20:32:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1481', '10000102', 'text', '空单，止盈止损怎么设', null, null, '2016-06-23 20:38:11', '2016-06-23 20:38:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1482', '10000197', 'text', '能不能把手续费降低点，签个止盈止损协议分成。相信大多数人愿意。', null, null, '2016-06-23 20:39:04', '2016-06-23 20:39:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1483', '8', 'text', '【退欧公投专场】美国当周初请人数创下近43年来的最低水平，而四周均值也录得下滑趋势。', null, null, '2016-06-23 20:40:05', '2016-06-23 20:40:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1484', '8', 'text', '【退欧公投专场】这已经是连续68周处于30万重要关口下方，持续时期为1973年来最长。', null, null, '2016-06-23 20:40:43', '2016-06-23 20:40:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1485', '8', 'text', '【退欧公投专场】这意味着美国劳动力市场依旧具有弹性，就业增长强劲。', null, null, '2016-06-23 20:41:23', '2016-06-23 20:41:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1486', '8', 'text', '【退欧公投专场】这或许会为美联储加息提供更多理由，对原油来说是有一定打压的。', null, null, '2016-06-23 20:43:01', '2016-06-23 20:43:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1487', '8', 'text', '手续费是交易所规定的，而且相比股票的手续费其实是差不多的，而且法律是不允许和客户作分成协议的。#15', '10000197', '能不能把手续费降低点，签个止盈止损协议分成。相信大多数人愿意。', '2016-06-23 20:46:09', '2016-06-23 20:46:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1488', '10000102', 'text', '止盈止损点', null, null, '2016-06-23 20:48:42', '2016-06-23 20:48:42', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1489', '8', 'text', '设止盈止损看支撑压力水平，目前，短期支撑在321-322区域，压力在332-333区域。', '10000102', '空单，止盈止损怎么设', '2016-06-23 20:50:30', '2016-06-23 20:50:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1490', '10000197', 'text', '这样其实更能激发老师和学员的热情', null, null, '2016-06-23 20:54:15', '2016-06-23 20:54:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1491', '8', 'text', '【退欧公投专场】大家手上持有空单的，按照白天吴老师的喊单点位，设好止盈止损。', null, null, '2016-06-23 20:54:21', '2016-06-23 20:54:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1492', '8', 'text', '这其实也是法律保护我们投资者的所规定的。#17', '10000197', '这样其实更能激发老师和学员的热情', '2016-06-23 20:58:07', '2016-06-23 20:58:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1493', '10000197', 'text', '找俩人，找个借口收俩钱叫黑社会收保护费，', null, null, '2016-06-23 21:01:02', '2016-06-23 21:01:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1494', '10000003', 'text', '等待机会中#6昨天连赚两拨#4#4', null, null, '2016-06-23 21:02:26', '2016-06-23 21:02:26', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1495', '10000197', 'text', '找一群人，立个项目收钱叫龟腚', null, null, '2016-06-23 21:02:33', '2016-06-23 21:02:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1496', '8', 'text', '还挺幽默。#4', '10000197', '找一群人，立个项目收钱叫龟腚', '2016-06-23 21:05:40', '2016-06-23 21:05:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1497', '8', 'text', '【退欧公投专场】从接近监管的消息人士处获悉，微信支付因接入大量没有支付牌照或收单资质的“二清机构”。', null, null, '2016-06-23 21:06:31', '2016-06-23 21:06:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1498', '8', 'text', '【退欧公投专场】微信支付都出问题了。#4', null, null, '2016-06-23 21:08:01', '2016-06-23 21:08:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1499', '8', 'text', '【退欧公投专场】目前结果来说，还是留欧阵营占上风。', null, null, '2016-06-23 21:14:51', '2016-06-23 21:14:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1500', '8', 'text', '【退欧公投专场】看来英国人真的是特别爱喝酒啊！', null, null, '2016-06-23 21:17:42', '2016-06-23 21:17:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1501', '8', 'text', '【退欧公投专场】英国选举改革协会都在社交媒体发布“温馨提示”，不建议选民喝醉时前往公投。#4这是一个爱喝酒的民族！', null, null, '2016-06-23 21:18:45', '2016-06-23 21:18:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1502', '8', 'text', '【退欧公投专场】有意思的是，选举改革协会表示，只要记得自己名字和地址也可以参加公投投票。', null, null, '2016-06-23 21:19:46', '2016-06-23 21:19:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1503', '8', 'text', '【退欧公投专场】英国博彩公司必发的最新博彩赔率显示，英国公投结果为“留在欧盟”的概率进一步走高，升至87%。', null, null, '2016-06-23 21:28:17', '2016-06-23 21:28:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1504', '8', 'text', '【退欧公投专场】如今市场主线都在英国退欧公投时间，连之前的初请数据出来市场反应都显得比较平淡。', null, null, '2016-06-23 21:31:08', '2016-06-23 21:31:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1505', '8', 'text', '【退欧公投专场】依据《石油价格管理办法》规定，“当调价幅度低于每吨50元时，不作调整，纳入下次调价时累加或冲抵”。', null, null, '2016-06-23 21:38:58', '2016-06-23 21:38:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1506', '8', 'text', '【退欧公投专场】这意味着，国内成品油调价时隔两个多月后将再度迎来搁浅。', null, null, '2016-06-23 21:39:59', '2016-06-23 21:39:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1507', '8', 'text', '【退欧公投专场】如此，2016年上半年的油价调整将呈现“四涨一跌七搁浅”的格局。', null, null, '2016-06-23 21:40:50', '2016-06-23 21:40:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1508', '8', 'text', '【退欧公投专场】21:45将公布美国6月Markit制造业PMI初值。', null, null, '2016-06-23 21:44:36', '2016-06-23 21:44:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1509', '8', 'text', '【退欧公投专场】不过退欧公投在即，估计此项数据依然难以受到市场很大关注。', null, null, '2016-06-23 21:45:07', '2016-06-23 21:45:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1510', '8', 'text', '【退欧公投专场】美国6月Markit制造业PMI初值数据利空。', null, null, '2016-06-23 21:45:48', '2016-06-23 21:45:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1511', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576be82cd4a33.png', null, null, '2016-06-23 21:46:20', '2016-06-23 21:46:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1512', '8', 'text', '【退欧公投专场】市场依然交投平淡。', null, null, '2016-06-23 21:52:17', '2016-06-23 21:52:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1513', '8', 'text', '【退欧公投专场】有经济学家表示，因出口销售表现强劲，美国6月制造业整体有所改善。', null, null, '2016-06-23 21:53:32', '2016-06-23 21:53:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1514', '8', 'text', '【退欧公投专场】不过，这些改善还不足以令制造业摆脱拖累美国经济增长的角色。', null, null, '2016-06-23 21:53:58', '2016-06-23 21:53:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1515', '10000006', 'text', '明天的行情会不会很吓人啊', null, null, '2016-06-23 22:02:11', '2016-06-23 22:02:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1516', '8', 'text', '【退欧公投专场】由于公投第一组投票结果预计要在明天早上07:00知晓，市场都在等结果，交投谨慎，晚间盘面波动比较小。', null, null, '2016-06-23 22:05:56', '2016-06-23 22:05:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1517', '8', 'text', '【退欧公投专场】美国5月谘商会领先指标月率结果出来了，利多原油。', null, null, '2016-06-23 22:07:10', '2016-06-23 22:07:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1518', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576bed32069d8.png', null, null, '2016-06-23 22:07:46', '2016-06-23 22:07:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1519', '8', 'text', '【退欧公投专场】盘面还是没有什么动静。', null, null, '2016-06-23 22:11:10', '2016-06-23 22:11:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1520', '8', 'text', '【退欧公投专场】今天美股走的不错，市场还是比较轻松。', null, null, '2016-06-23 22:12:21', '2016-06-23 22:12:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1521', '8', 'text', '【退欧公投专场】大家也都放轻松点，今天只是预演，明天才是战斗号角吹响的时候。#4', null, null, '2016-06-23 22:14:03', '2016-06-23 22:14:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1522', '8', 'text', '【退欧公投专场】今天英国“脱欧”公投是在大雨中进行的，一个公投被操纵了的说法却不胫而走。', null, null, '2016-06-23 22:25:27', '2016-06-23 22:25:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1523', '8', 'text', '【退欧公投专场】根据民调显示，总体受访者中约三成认为已经被操纵，而“脱欧”支持者中近半数相信被操纵。', null, null, '2016-06-23 22:25:56', '2016-06-23 22:25:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1524', '8', 'text', '【退欧公投专场】由于“脱欧”支持率较为落后，这可能是“受害者心理”作祟。#5', null, null, '2016-06-23 22:26:38', '2016-06-23 22:26:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1525', '8', 'text', '【退欧公投专场】另外还有人揣测，英国两大广播公司BBC和ITN会蓄意隐瞒出口民调的结果，以便给操作投票结果留出空间。', null, null, '2016-06-23 22:27:54', '2016-06-23 22:27:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1526', '8', 'text', '【退欧公投专场】不过，无论是否被操纵，“脱欧公投”的结果基本就是一锤定音。', null, null, '2016-06-23 22:28:45', '2016-06-23 22:28:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1527', '8', 'text', '【退欧公投专场】随着公投进行，今天英镑已经上蹿下跳的巨幅波动，可能会出现巨大的无风险套利机会。', null, null, '2016-06-23 22:29:07', '2016-06-23 22:29:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1528', '8', 'text', '【退欧公投专场】据BBC报道，本次公投预计将有 46,499,537人参加，创历史最高，此次退欧公投只是英国历史上第三次全国性的公投。', null, null, '2016-06-23 22:35:32', '2016-06-23 22:35:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1529', '8', 'text', '【退欧公投专场】英国选民可选两种答案：1是“继续当欧盟成员”，2是“离开欧盟”。', null, null, '2016-06-23 22:36:38', '2016-06-23 22:36:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1530', '8', 'text', '【退欧公投专场】英国各大媒体几乎“一边倒”的支持留在欧盟。', null, null, '2016-06-23 22:37:36', '2016-06-23 22:37:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1531', '8', 'text', '【退欧公投专场】英国民意调查的结果显示：许多年纪较长的英格兰蓝领选民主张退出欧盟，而年轻一些受过良好教育的大不列颠选民、大多数的苏格兰和北爱尔兰人则青睐留下。', null, null, '2016-06-23 22:40:08', '2016-06-23 22:40:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1532', '8', 'text', '【退欧公投专场】今天英国多地下起小雨，此前有分析称天气原因或影响投票结果，因为温和的留欧派或呆在家中，懒得出门。#7', null, null, '2016-06-23 22:41:59', '2016-06-23 22:41:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1533', '8', 'text', '【退欧公投专场】只能说有钱任性。#4', null, null, '2016-06-23 22:43:02', '2016-06-23 22:43:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1534', '8', 'text', '【退欧公投专场】好，闲话少说，咱们继续讲。', null, null, '2016-06-23 22:43:46', '2016-06-23 22:43:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1535', '8', 'text', '【退欧公投专场】大概下午3点的时候，美国共和党总统候选人特朗普抵达英国，称倾向于英国脱离欧盟。', null, null, '2016-06-23 22:45:15', '2016-06-23 22:45:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1536', '8', 'text', '【退欧公投专场】脱欧公投开始后，欧洲股市开盘普遍走高，欧洲投资者希望英国能够留在欧盟。', null, null, '2016-06-23 22:47:17', '2016-06-23 22:47:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1537', '8', 'text', '【退欧公投专场】英国首相卡梅伦抵达票站参与脱欧公投，卡梅伦此前在多个场合表示自己持留欧意愿。', null, null, '2016-06-23 22:48:44', '2016-06-23 22:48:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1538', '8', 'text', '【退欧公投专场】好了，咱们继续回到退欧公投事件。', null, null, '2016-06-23 23:03:56', '2016-06-23 23:03:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1539', '8', 'text', '【退欧公投专场】下午4点左右，英国最大的反对党工党主席杰里米·科尔宾在推特宣布，他投下了留欧一票。', null, null, '2016-06-23 23:04:58', '2016-06-23 23:04:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1540', '8', 'text', '【退欧公投专场】但是，公投结果的不确定性实在太大，为了控制货币风险，很多英国公民焦急的将手里的英镑兑换成美元和欧元。', null, null, '2016-06-23 23:07:30', '2016-06-23 23:07:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1541', '8', 'text', '【退欧公投专场】下午5点左右，苏格兰首席部长斯特金出现在所属票区现场，她呼吁英国留在欧盟。', null, null, '2016-06-23 23:08:53', '2016-06-23 23:08:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1542', '8', 'text', '【退欧公投专场】18:00 IPSOS-MORI民调显示，公投前有52%的投票者支持英国留欧，48%支持脱欧。至此，留欧阵营占据上风。', null, null, '2016-06-23 23:10:31', '2016-06-23 23:10:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1543', '8', 'text', '【退欧公投专场】19：00左右，Populus民调公布，55%的投票者支持英国留欧，45%支持退欧。', null, null, '2016-06-23 23:12:04', '2016-06-23 23:12:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1544', '8', 'text', '【退欧公投专场】虽然该调查平台是网络投票，单亦可作为参考。', null, null, '2016-06-23 23:13:09', '2016-06-23 23:13:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1545', '8', 'text', '【退欧公投专场】20:30左右，最新的一份民调显示留欧派领先。', null, null, '2016-06-23 23:14:44', '2016-06-23 23:14:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1546', '8', 'text', '【退欧公投专场】综合上面的消息来看，明天的公投结果是留欧的可能性比较大。', null, null, '2016-06-23 23:18:03', '2016-06-23 23:18:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1547', '8', 'text', '【退欧公投专场】毕竟民心所向。', null, null, '2016-06-23 23:18:53', '2016-06-23 23:18:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1548', '8', 'text', '【退欧公投专场】根据最新的数据，至少已有50人受伤，不过有说法称其中25人或事因胡椒喷雾而受伤。', null, null, '2016-06-23 23:25:36', '2016-06-23 23:25:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1549', '8', 'text', '【退欧公投专场】最新消息显示，德国警方已经击毙了枪手，目前不清楚作案动机。 ', null, null, '2016-06-23 23:26:34', '2016-06-23 23:26:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1550', '8', 'text', '【退欧公投专场】是不是恐怖袭击，目前还不太清楚。', null, null, '2016-06-23 23:27:24', '2016-06-23 23:27:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1551', '8', 'text', '【退欧公投专场】虽然作案动机尚不明确，但是德国警方反复强调，这或许是ISIS组织策划的对德国的恐怖袭击。', null, null, '2016-06-23 23:30:50', '2016-06-23 23:30:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1552', '8', 'text', '【退欧公投专场】目前，美元指数短线下挫，原油受到支撑，出现小幅反弹。', null, null, '2016-06-23 23:41:20', '2016-06-23 23:41:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1553', '8', 'text', '【退欧公投专场】欧洲股市今天走势都非常不错。', null, null, '2016-06-23 23:52:31', '2016-06-23 23:52:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1554', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160623/576c05edc5c28.png', null, null, '2016-06-23 23:53:17', '2016-06-23 23:53:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1555', '8', 'text', '【退欧公投专场】明天公投结果出炉，不过欧股今晚走势或已经告诉我们答案了，全球市场短期方向将逐步明朗。', null, null, '2016-06-23 23:54:28', '2016-06-23 23:54:28', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1556', '8', 'text', '【退欧公投专场】退欧公投从今天周四下午两点开始，截止六点欧股走势依然继续维持上升，暗示留欧结局已定！今天收盘涨跌对明日影响会很大！ ', null, null, '2016-06-23 23:55:21', '2016-06-23 23:55:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1557', '8', 'text', '【退欧公投专场】退欧对全球市场来说都是一次巨大的风险事件，对原油市场也一样，短期将主导市场。但是从长期来说，供需才是引领长期走势的关键。', null, null, '2016-06-24 00:05:40', '2016-06-24 00:05:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1558', '8', 'text', '【退欧公投专场】下面，我们来看看市场长期的一个走势。', null, null, '2016-06-24 00:07:56', '2016-06-24 00:07:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1559', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576c0bc591a54.png', null, null, '2016-06-24 00:18:13', '2016-06-24 00:18:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1560', '8', 'text', '【退欧公投专场】日线级别来看，上方关键压力位置在380左右，下方关键支撑在270左右。', null, null, '2016-06-24 00:21:42', '2016-06-24 00:21:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1561', '8', 'text', '【退欧公投专场】也就是说，退欧事件过后，靴子落地。市场将重新由供需所引导，技术面可关注上面提到的关键支撑压力位。', null, null, '2016-06-24 00:31:53', '2016-06-24 00:31:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1562', '8', 'text', '【退欧公投专场】晚间的德国枪击事件，德国安全部队消息人士称，电影院枪手似乎与恐怖组织无关。', null, null, '2016-06-24 00:33:27', '2016-06-24 00:33:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1563', '8', 'text', '【退欧公投专场】欧盟公报草案：如果英国选择留在欧盟，那么下周举行的欧盟领导人会议将讨论欧洲难民危机和安全政策。', null, null, '2016-06-24 00:34:40', '2016-06-24 00:34:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1564', '8', 'text', '【退欧公投专场】这是否暗示英国留在欧盟已经成为定局了呢？', null, null, '2016-06-24 00:36:33', '2016-06-24 00:36:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1565', '8', 'text', '【退欧公投专场】可能性非常大。', null, null, '2016-06-24 00:38:06', '2016-06-24 00:38:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1566', '8', 'text', '【退欧公投专场】英国石油公司CEO表示，今年原油市场将继续恢复平衡。', null, null, '2016-06-24 01:09:37', '2016-06-24 01:09:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1567', '8', 'text', '【退欧公投专场】他称原油价格为100美元/桶的日子不会很快回来。', null, null, '2016-06-24 01:10:30', '2016-06-24 01:10:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1568', '8', 'text', '【退欧公投专场】退欧公投在今天北京事件凌晨5：00结束，并将在下午14:00公布公投结果。', null, null, '2016-06-24 01:24:41', '2016-06-24 01:24:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1569', '8', 'text', '【退欧公投专场】鉴于公投期间全球市场的高度不确定性，在这里还是要提醒大家注意做好风险控制，严格执行止盈止损的交易纪律。', null, null, '2016-06-24 01:27:10', '2016-06-24 01:27:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1570', '10000000', 'text', '大家早上好！', null, null, '2016-06-24 09:08:46', '2016-06-24 09:08:46', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1571', '10000000', 'text', '大家早上好！', null, null, '2016-06-24 09:08:54', '2016-06-24 09:08:54', '1', '#e71516', '18');
INSERT INTO `chat_chat` VALUES ('1572', '5', 'text', '#16暴跌了', null, null, '2016-06-24 09:09:01', '2016-06-24 09:09:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1573', '5', 'text', '太疯狂了', null, null, '2016-06-24 09:09:05', '2016-06-24 09:09:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1574', '10000000', 'text', '行情在早盘出现大跌！', null, null, '2016-06-24 09:09:30', '2016-06-24 09:09:30', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1575', '10000000', 'text', '行情在早盘出现大跌！', null, null, '2016-06-24 09:09:38', '2016-06-24 09:09:38', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1576', '10000013', 'text', '为什么跌那么厉害啊老师', null, null, '2016-06-24 09:10:49', '2016-06-24 09:10:49', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1577', '10000005', 'text', '这是不是就能做多了', null, null, '2016-06-24 09:10:54', '2016-06-24 09:10:54', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1578', '10000102', 'text', '行情疯了吗？', null, null, '2016-06-24 09:11:52', '2016-06-24 09:11:52', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1579', '10000000', 'text', '【退欧公投专场】6月24日亚市早盘，英国退欧投票结束，各选区陆续公布结果。从目前计票来看，退欧阵营暂时领先，市场避险情绪升温', null, null, '2016-06-24 09:12:34', '2016-06-24 09:12:34', '1', '#e71516', '18');
INSERT INTO `chat_chat` VALUES ('1580', '10000103', 'text', '早上好亲们#9#9', null, null, '2016-06-24 09:13:15', '2016-06-24 09:13:15', '-1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1581', '10000103', 'text', '#9#9大家好', null, null, '2016-06-24 09:15:25', '2016-06-24 09:15:25', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1582', '10000103', 'text', '【退欧公投专场】现在是英国时间凌晨两点十五分', null, null, '2016-06-24 09:15:58', '2016-06-24 09:15:58', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1583', '10000000', 'text', '【退欧公投专场】目前来看，在英国公投结果出来之前，建议大家还是谨慎操作。', null, null, '2016-06-24 09:16:13', '2016-06-24 09:16:13', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1584', '10000103', 'text', '【退欧公投专场】在昨天下午，现场投票的选民非常多！退欧和留欧的支持者，都热情高涨。下面给大家看下昨天英国投票的现场情况', null, null, '2016-06-24 09:18:10', '2016-06-24 09:18:10', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1585', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576c8aa8e4114.jpeg', null, null, '2016-06-24 09:19:36', '2016-06-24 09:19:36', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1586', '10000006', 'text', '不可思议', null, null, '2016-06-24 09:20:43', '2016-06-24 09:20:43', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1587', '10000000', 'text', '【退欧公投专场】目前382个计票区中统计出43个的结果，留欧支持率为47.1%，脱欧支持率52.9%！', null, null, '2016-06-24 09:20:53', '2016-06-24 09:20:53', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1588', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576c8afdce780.jpeg', null, null, '2016-06-24 09:21:01', '2016-06-24 09:21:01', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1589', '10000000', 'text', '【退欧公投专场】牛津计票区投票70%支持留欧，30%支持脱欧。', null, null, '2016-06-24 09:25:32', '2016-06-24 09:25:32', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1590', '10000000', 'text', '【退欧公投专场】格拉斯哥计票区67%支持留欧，33%支持退欧', null, null, '2016-06-24 09:25:59', '2016-06-24 09:25:59', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1591', '10000000', 'text', '【退欧公投专场】北爱尔兰计票区56%支持留欧，44%支持脱欧，北爱尔兰是最大单独计票区。', null, null, '2016-06-24 09:26:16', '2016-06-24 09:26:16', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1592', '10000103', 'text', '【退欧公投专场】现在虽然是凌晨，但关乎到国家命运的一晚，很多民众还是留在伦敦广场上，等待投票的结果。', null, null, '2016-06-24 09:27:42', '2016-06-24 09:27:42', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1593', '10000000', 'text', '【退欧公投专场】行情来的非常迅猛，还没有完全公布结果，一个早盘原油已经跌了超过4%！', null, null, '2016-06-24 09:30:46', '2016-06-24 09:30:46', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1594', '10000000', 'text', '而我们的吴老师昨天仿佛也是如有神助，直接给出了空单并且顺利盈利！', null, null, '2016-06-24 09:35:53', '2016-06-24 09:35:53', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1595', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576c8e7fd12bf.jpg', null, null, '2016-06-24 09:35:59', '2016-06-24 09:35:59', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1596', '10000008', 'text', '这原油是要逆天啊', null, null, '2016-06-24 09:36:57', '2016-06-24 09:36:57', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1597', '10000003', 'text', '327空单止盈了。。好强大啊！！！快激动疯了。。。#20   ', null, null, '2016-06-24 09:38:58', '2016-06-24 09:38:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1598', '10000109', 'text', '这要是再股市就要跳楼了。。。。今天大盘会怎么样啊！老师我股票要不要跑！', null, null, '2016-06-24 09:39:30', '2016-06-24 09:39:30', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1599', '10000109', 'text', '#9', null, null, '2016-06-24 09:39:33', '2016-06-24 09:39:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1600', '10000004', 'text', '老师 我没跟上你的单子  还有机会吗？#2', null, null, '2016-06-24 09:39:49', '2016-06-24 09:39:49', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1601', '10000000', 'text', '[|@结点 老师 我没跟上你的单子  还有机会吗？#2|公投结果公布后，行情就会明朗，明朗后就可以跟着我们的建议操作了', '10000004', '老师 我没跟上你的单子  还有机会吗？#2', '2016-06-24 09:41:51', '2016-06-24 09:41:51', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1602', '10000000', 'text', '公投结果公布后，行情就会明朗，明朗后就可以跟着我们的建议操作了', '10000004', '老师 我没跟上你的单子  还有机会吗？#2', '2016-06-24 09:42:07', '2016-06-24 09:42:07', '1', '#e71516', '18');
INSERT INTO `chat_chat` VALUES ('1603', '10000004', 'text', '好的 谢谢#9', null, null, '2016-06-24 09:42:16', '2016-06-24 09:42:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1604', '10000103', 'text', '【退欧公投专场】这是伦敦最大的Trafalgar广场，白天很多民众在这里等待国家的命运揭晓的那一刻', null, null, '2016-06-24 09:42:30', '2016-06-24 09:42:30', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1605', '10000000', 'text', '如果下午14:00公布结果是离开欧盟，中国的股市是会有短暂的下行。建议减仓', '10000109', '这要是再股市就要跳楼了。。。。今天大盘会怎么样啊！老师我股票要不要跑！', '2016-06-24 09:42:53', '2016-06-24 09:42:53', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1606', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576c9023812f1.jpeg', null, null, '2016-06-24 09:42:59', '2016-06-24 09:42:59', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1607', '10000000', 'text', '恭喜恭喜！#14', '10000003', '327空单止盈了。。好强大啊！！！快激动疯了。。。#20   ', '2016-06-24 09:43:13', '2016-06-24 09:43:13', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1608', '10000000', 'text', '【退欧公投专场】目前382个计票区中已统计出72个的结果，留欧支持率为50.8%，脱欧为49.2%。', null, null, '2016-06-24 09:46:08', '2016-06-24 09:46:08', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1609', '10000006', 'text', '老师神啊，昨天这个空单就差0.2差点止损了', null, null, '2016-06-24 09:46:25', '2016-06-24 09:46:25', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1610', '10000000', 'text', '是的，吴老师很厉害！#14', '10000006', '老师神啊，昨天这个空单就差0.2差点止损了', '2016-06-24 09:46:57', '2016-06-24 09:46:57', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1611', '10000006', 'text', '老师计算的太精准了，这一把我股票亏的都赚回来啦#20', null, null, '2016-06-24 09:49:42', '2016-06-24 09:49:42', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1612', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576c92d918dce.jpg', null, null, '2016-06-24 09:54:33', '2016-06-24 09:54:33', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1613', '10000012', 'text', '#14', null, null, '2016-06-24 09:56:40', '2016-06-24 09:56:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1614', '10000102', 'text', '今天是不是比较适合做波段？', null, null, '2016-06-24 09:57:42', '2016-06-24 09:57:42', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1615', '10000000', 'text', '【退欧公投专场】原油行情群情汹涌，朋友们，你们还在死心塌地的做那个毫无希望的股票市场吗？？', null, null, '2016-06-24 09:58:24', '2016-06-24 09:58:24', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1616', '10000000', 'text', '今天等结果出来以后顺势操作是最安全的', '10000102', '今天是不是比较适合做波段？', '2016-06-24 09:58:42', '2016-06-24 09:58:42', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1617', '10000000', 'text', '【退欧公投专场】382个计票区已统计105个，留欧支持3776470(49.9%)，脱欧3788990(50.1%)', null, null, '2016-06-24 10:01:55', '2016-06-24 10:01:55', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1618', '10000006', 'text', '好深的事业线', null, null, '2016-06-24 10:08:26', '2016-06-24 10:08:26', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1619', '10000000', 'text', '【退欧公投专场】威斯敏斯特计票区69%支持留欧，31%支持脱欧。', null, null, '2016-06-24 10:08:46', '2016-06-24 10:08:46', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1620', '10000102', 'text', '感觉现在的行情好危险#2', null, null, '2016-06-24 10:09:42', '2016-06-24 10:09:42', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1621', '10000121', 'text', '老师，现在怎么操作好一些？', null, null, '2016-06-24 10:13:19', '2016-06-24 10:13:19', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1622', '10000006', 'text', '老师，您认为英国会脱里欧盟吗？', null, null, '2016-06-24 10:13:50', '2016-06-24 10:13:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1623', '10000000', 'text', '只有等结果出来后才知道哦', '10000006', '老师，您认为英国会脱里欧盟吗？', '2016-06-24 10:14:43', '2016-06-24 10:14:43', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1624', '10000000', 'text', '现在还在统计选票阶段，如果要做，建议短线快进快出比较好。在区间318~328高抛低吸最好', '10000121', '老师，现在怎么操作好一些？', '2016-06-24 10:15:47', '2016-06-24 10:15:47', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1625', '10000000', 'text', '【退欧公投专场】留欧阵营与脱欧阵营势均力敌，全球金融市场瞬息万变，黄金白银原油等资产价格几乎跟随着阵营的支持率而上下波动。行情飘忽不定，敬请控制仓位风险。', null, null, '2016-06-24 10:16:35', '2016-06-24 10:16:35', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1626', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576c9b96e2da3.jpg', null, null, '2016-06-24 10:31:50', '2016-06-24 10:31:50', '1', '#e71516', '18');
INSERT INTO `chat_chat` VALUES ('1627', '10000000', 'text', '行情又来到了区间底部，激进的朋友可以现价318短多一把，止损314，止盈先看324', null, null, '2016-06-24 10:32:50', '2016-06-24 10:32:50', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1628', '10000000', 'text', '【退欧公投专场】382个计票区已统计148个，留欧支持5375420(49.3%)，脱欧5520142(50.7%)', null, null, '2016-06-24 10:33:50', '2016-06-24 10:33:50', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1629', '10000102', 'text', '好嘞！#4', null, null, '2016-06-24 10:34:49', '2016-06-24 10:34:49', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1630', '10000000', 'text', '【退欧公投专场】HART计票区52%支持留欧，48%支持脱欧', null, null, '2016-06-24 10:42:07', '2016-06-24 10:42:07', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1631', '10000000', 'text', '【退欧公投专场】HART计票区52%支持留欧，48%支持脱欧', null, null, '2016-06-24 10:42:14', '2016-06-24 10:42:14', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1632', '10000000', 'text', '【退欧公投专场】382个计票区已统计199个，留欧支持7592500(48.3%)，脱欧8119504(51.7%)', null, null, '2016-06-24 10:44:33', '2016-06-24 10:44:33', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1633', '10000000', 'text', '【退欧公投专场】382个计票区已统计199个，留欧支持7592500(48.3%)，脱欧8119504(51.7%)', null, null, '2016-06-24 10:44:41', '2016-06-24 10:44:41', '1', '#e71516', '18');
INSERT INTO `chat_chat` VALUES ('1634', '10000000', 'text', '【退欧公投专场】英国退欧风险越来越大，亚洲早盘各国股市不断下跌！', null, null, '2016-06-24 10:50:19', '2016-06-24 10:50:19', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1635', '10000003', 'text', '#9', null, null, '2016-06-24 10:51:39', '2016-06-24 10:51:39', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1636', '10000000', 'text', '【退欧公投专场】下面即将准备公布曼彻斯特的票数，曼彻斯特此前被预计是强烈支持欧盟的地区。', null, null, '2016-06-24 10:55:10', '2016-06-24 10:55:10', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1637', '10000000', 'text', '【退欧公投专场】曼彻斯特是票数最多的区域之一', null, null, '2016-06-24 10:55:56', '2016-06-24 10:55:56', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1638', '10000000', 'text', '【退欧公投专场】英镑兑美元跌9%至1.3532。', null, null, '2016-06-24 10:58:16', '2016-06-24 10:58:16', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1639', '10000000', 'text', '【退欧公投专场】对于一个国家的货币而言，货币跌近10%是非常可怕的，特别是发达国家！', null, null, '2016-06-24 10:59:40', '2016-06-24 10:59:40', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1640', '10000000', 'text', '如果持有多单的朋友，建议破了312一定要锁仓一半或者止损减仓一半仓位', null, null, '2016-06-24 11:03:10', '2016-06-24 11:03:10', '-1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1641', '10000000', 'text', '【退欧公投专场】如果持有多单的朋友，建议破了312一定要锁仓一半或者止损减仓一半仓位', null, null, '2016-06-24 11:03:18', '2016-06-24 11:03:18', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1642', '10000000', 'text', '【退欧公投专场】382个计票区已统计245个，留欧支持9587370(48.9%)，脱欧10002965(51.1%)！', null, null, '2016-06-24 11:09:20', '2016-06-24 11:09:20', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1643', '10000000', 'text', '【退欧公投专场】脱欧选票已经占据上风，多头建议做好风险控制！', null, null, '2016-06-24 11:10:57', '2016-06-24 11:10:57', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1644', '10000000', 'text', '【退欧公投专场】切斯菲尔特计票区40%支持留欧，60%支持脱欧', null, null, '2016-06-24 11:13:36', '2016-06-24 11:13:36', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1645', '10000000', 'text', '【退欧公投专场】“脱欧派”领跑近50万票 英镑跌幅向10%迈进！', null, null, '2016-06-24 11:15:08', '2016-06-24 11:15:08', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1646', '10000000', 'text', '【退欧公投专场】原油在早盘跌幅也已经接近6%！', null, null, '2016-06-24 11:15:33', '2016-06-24 11:15:33', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1647', '10000013', 'text', '老师，我316的多单要怎么办？', null, null, '2016-06-24 11:15:56', '2016-06-24 11:15:56', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1648', '10000000', 'text', '【退欧公投专场】上证指数受到影响，也跌幅达到1.5%', null, null, '2016-06-24 11:16:03', '2016-06-24 11:16:03', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1649', '10000000', 'text', '开一些空单先锁仓，或者如果仓位不重的话可以止损出来，等结果出来后再做', '10000013', '老师，我316的多单要怎么办？', '2016-06-24 11:16:46', '2016-06-24 11:16:46', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1650', '10000000', 'text', '【退欧公投专场】英国独立党支持脱欧的领袖法拉奇：如果英国脱欧成功，英国首相卡梅伦应该立刻辞职。', null, null, '2016-06-24 11:20:27', '2016-06-24 11:20:27', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1651', '10000000', 'text', '【退欧公投专场】据ITV NEWS分析：英国脱欧阵营取胜的概率为85%。', null, null, '2016-06-24 11:21:44', '2016-06-24 11:21:44', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1652', '10000000', 'text', '【退欧公投专场】英国已经超过80%的概率脱欧。', null, null, '2016-06-24 11:22:14', '2016-06-24 11:22:14', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1653', '10000000', 'text', '【退欧公投专场】有空单的朋友可以继续拿一拿', null, null, '2016-06-24 11:22:28', '2016-06-24 11:22:28', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1654', '10000000', 'text', '【退欧公投专场】据美国联邦基金利率期货调查显示，至少到2018年初，几乎看不到美联储加息的可能性。', null, null, '2016-06-24 11:27:12', '2016-06-24 11:27:12', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1655', '10000000', 'text', '【退欧公投专场】382个计票区已统计274个，留欧10684775(48.7%)，脱欧11274488(51.3%)。', null, null, '2016-06-24 11:29:49', '2016-06-24 11:29:49', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1656', '10000000', 'text', '【退欧公投专场】美元指数上涨3%，料将录得1978年以来最大单日涨幅。', null, null, '2016-06-24 11:30:01', '2016-06-24 11:30:01', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1657', '10000000', 'text', '【退欧公投专场】据ITV新闻：脱欧阵营已经赢得此次公投。', null, null, '2016-06-24 11:36:52', '2016-06-24 11:36:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1658', '10000000', 'text', '【退欧公投专场】小道消息已经表示英国脱欧了', null, null, '2016-06-24 11:37:10', '2016-06-24 11:37:10', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1659', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576cab9a3dd42.jpeg', null, null, '2016-06-24 11:40:10', '2016-06-24 11:40:10', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1660', '10000103', 'text', '【退欧公投专场】民众都在等待最终的结果', null, null, '2016-06-24 11:40:25', '2016-06-24 11:40:25', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1661', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576cabc631d1d.jpeg', null, null, '2016-06-24 11:40:54', '2016-06-24 11:40:54', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1662', '10000102', 'text', '我的多单也拿着呢', null, null, '2016-06-24 11:43:05', '2016-06-24 11:43:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1663', '10000000', 'text', '【退欧公投专场】据天空新闻：预计英国脱欧已成定局。', null, null, '2016-06-24 11:43:55', '2016-06-24 11:43:55', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1664', '10000000', 'text', '【退欧公投专场】据BBC：预计在公投中英国投票支持脱欧。', null, null, '2016-06-24 11:44:05', '2016-06-24 11:44:05', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1665', '10000000', 'text', '【退欧公投专场】两大权威媒体都已经表态，英国脱欧基本成定局。', null, null, '2016-06-24 11:44:34', '2016-06-24 11:44:34', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1666', '10000000', 'text', '【退欧公投专场】原油已经暴跌达到6%！', null, null, '2016-06-24 11:45:27', '2016-06-24 11:45:27', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1667', '10000000', 'text', '建议一定要做风险控制。开一些空单锁仓或者减仓多单', '10000102', '我的多单也拿着呢', '2016-06-24 11:45:53', '2016-06-24 11:45:53', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1668', '10000000', 'text', '【退欧公投专场】原油的空头格局基本形成', null, null, '2016-06-24 11:46:15', '2016-06-24 11:46:15', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1669', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576cad4220f0e.jpg', null, null, '2016-06-24 11:47:14', '2016-06-24 11:47:14', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1670', '10000000', 'text', '【退欧公投专场】382个计票区已统计315个，留欧12635440(48.3%)，脱欧13524404(51.7%)。', null, null, '2016-06-24 11:50:53', '2016-06-24 11:50:53', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1671', '10000000', 'text', '【退欧公投专场】英国脱欧已成定局，导致黄金白银美元等避险资产全线大涨，原油等商品全线大跌', null, null, '2016-06-24 11:54:38', '2016-06-24 11:54:38', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1672', '10000000', 'text', '【退欧公投专场】英国脱欧已成定局，导致黄金白银美元等避险资产全线大涨，原油等商品全线大跌', null, null, '2016-06-24 11:54:46', '2016-06-24 11:54:46', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1673', '10000000', 'text', '【退欧公投专场】到了午饭时间，大家先出去吃饭，稍后就公布结果', null, null, '2016-06-24 11:59:57', '2016-06-24 11:59:57', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1674', '10000000', 'text', '【退欧公投专场】大家中午好！', null, null, '2016-06-24 13:11:03', '2016-06-24 13:11:03', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1675', '10000000', 'text', '【退欧公投专场】382个计票区已统计374个，支持脱欧人数已过半，其中支持留欧15692093人(48.2%)，脱欧16835512(51.8%)人。', null, null, '2016-06-24 13:13:00', '2016-06-24 13:13:00', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1676', '10000000', 'text', '【退欧公投专场】英国成功脱欧', null, null, '2016-06-24 13:13:10', '2016-06-24 13:13:10', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1677', '10000000', 'text', '【退欧公投专场】后市对原油将是一大利空因素！', null, null, '2016-06-24 13:13:24', '2016-06-24 13:13:24', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1678', '10000102', 'text', '空单吗？', null, null, '2016-06-24 13:14:02', '2016-06-24 13:14:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1679', '10000006', 'text', '#13', null, null, '2016-06-24 13:14:14', '2016-06-24 13:14:14', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1680', '10000006', 'text', '不是还没到时间吗', null, null, '2016-06-24 13:14:29', '2016-06-24 13:14:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1681', '10000006', 'text', '老师，不是2点才最终出结果吗？', null, null, '2016-06-24 13:15:13', '2016-06-24 13:15:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1682', '10000004', 'text', '好紧张哦！现在可以追空吗？老师#13', null, null, '2016-06-24 13:15:53', '2016-06-24 13:15:53', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1683', '10000000', 'text', '【退欧公投专场】现在还有大概十个区没有计算完票数，但是由于退欧的优势太明显，超过了100万，所以基本没有悬念了', null, null, '2016-06-24 13:17:18', '2016-06-24 13:17:18', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1684', '10000000', 'text', '反弹到314~315区域继续做空', '10000004', '好紧张哦！现在可以追空吗？老师#13', '2016-06-24 13:17:44', '2016-06-24 13:17:44', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1685', '10000000', 'text', '【退欧公投专场】14点首相卡梅伦将公布最终结果', null, null, '2016-06-24 13:18:12', '2016-06-24 13:18:12', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1686', '10000000', 'text', '【退欧公投专场】A股也受到脱欧的影响，最大跌幅达到3%', null, null, '2016-06-24 13:19:45', '2016-06-24 13:19:45', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1687', '10000000', 'text', '【退欧公投专场】同样是下跌，原油做空大赚，股市却令人沮丧。。。', null, null, '2016-06-24 13:20:19', '2016-06-24 13:20:19', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1688', '10000102', 'text', '现在的操作意见是怎样的', null, null, '2016-06-24 13:22:42', '2016-06-24 13:22:42', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1689', '10000000', 'text', '等原油反弹到314~315区域继续做空', '10000102', '现在的操作意见是怎样的', '2016-06-24 13:23:27', '2016-06-24 13:23:27', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1690', '10000102', 'text', '好的', null, null, '2016-06-24 13:25:22', '2016-06-24 13:25:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1691', '1', 'text', '现在能操作么？', null, null, '2016-06-24 13:32:38', '2016-06-24 13:32:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1692', '10000103', 'text', '【退欧公投专场】现在退欧基本尘埃落定，街上民众神情有喜有忧。很多人还在等待一会首相卡梅伦的官方公布结果。', null, null, '2016-06-24 13:33:07', '2016-06-24 13:33:07', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1693', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576cc63328713.jpeg', null, null, '2016-06-24 13:33:39', '2016-06-24 13:33:39', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1694', '10000000', 'text', '由于早盘跌幅过大，现在先等反弹到314~315区域再做空比较安全', '1', '现在能操作么？', '2016-06-24 13:34:05', '2016-06-24 13:34:05', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1695', '10000000', 'text', '【退欧公投专场】手中有318上方的多单的，建议现在先锁仓。有318下方多单的，建议看看能否反弹到314~315区域。', null, null, '2016-06-24 13:37:02', '2016-06-24 13:37:02', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1696', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576cc83e12b7a.jpeg', null, null, '2016-06-24 13:42:22', '2016-06-24 13:42:22', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1697', '10000103', 'text', '	\n【退欧公投专场】广场上已经群雄汹涌了', null, null, '2016-06-24 13:42:31', '2016-06-24 13:42:31', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('1698', '10000006', 'text', '老师。那是着火吗', null, null, '2016-06-24 13:52:06', '2016-06-24 13:52:06', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1699', '10000230', 'text', ' ', null, null, '2016-06-24 13:52:37', '2016-06-24 13:52:37', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1700', '10000007', 'text', '老师，现在可以操作吗', '10000006', '老师。那是着火吗', '2016-06-24 13:53:33', '2016-06-24 13:53:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1701', '7', 'text', '老师接下来怎么看？', null, null, '2016-06-24 14:02:45', '2016-06-24 14:02:45', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1702', '10000000', 'text', '【退欧公投专场】据BBC计票统计，目前382个计票区已统计381个，英国脱欧公投已尘埃落定，其中支持留欧16000701人(48.2%)，脱欧17228077(51.8%)人。', null, null, '2016-06-24 14:02:46', '2016-06-24 14:02:46', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1703', '10000000', 'text', '【退欧公投专场】据BBC计票统计，目前382个计票区已统计381个，英国脱欧公投已尘埃落定，其中支持留欧16000701人(48.2%)，脱欧17228077(51.8%)人。', null, null, '2016-06-24 14:02:53', '2016-06-24 14:02:53', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1704', '10000102', 'text', '空单？', null, null, '2016-06-24 14:03:40', '2016-06-24 14:03:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1705', '10000000', 'text', '现在可以建一点仓位的空单。', '7', '老师接下来怎么看？', '2016-06-24 14:03:54', '2016-06-24 14:03:54', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('1706', '10000000', 'text', '止损320，止盈305', '7', '老师接下来怎么看？', '2016-06-24 14:04:26', '2016-06-24 14:04:26', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1707', '10000000', 'text', '现在可以建一点仓位的空单。', '10000102', '空单？', '2016-06-24 14:04:35', '2016-06-24 14:04:35', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1708', '10000000', 'text', '【英国成功退欧】据BBC计票统计，382个计票区已全部统计完毕，其中支持留欧16141241人(48.1%)，脱欧17410742(51.9%)人。', null, null, '2016-06-24 14:04:59', '2016-06-24 14:04:59', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1709', '10000000', 'text', '英国正式宣布退出欧盟！', '10000013', '为什么跌那么厉害啊老师', '2016-06-24 14:06:49', '2016-06-24 14:06:49', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1710', '10000000', 'text', '空头趋势已经很明显，反弹后继续顺势做空！', '10000005', '这是不是就能做多了', '2016-06-24 14:07:09', '2016-06-24 14:07:09', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1711', '10000092', 'text', '老师，为什么股票还长了', null, null, '2016-06-24 14:07:15', '2016-06-24 14:07:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1712', '10000000', 'text', '脱欧对A股的影响已经影响完了', '10000092', '老师，为什么股票还长了', '2016-06-24 14:07:50', '2016-06-24 14:07:50', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1713', '10000109', 'text', '股票周一会出现大跌什么的吗？要不要现在减仓啊！', null, null, '2016-06-24 14:10:59', '2016-06-24 14:10:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1714', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576ccf35d4e63.jpg', null, null, '2016-06-24 14:12:05', '2016-06-24 14:12:05', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1715', '10000000', 'text', 'A股现在已经到阶段性底部，但是要大涨也是难，只能够在低位徘徊', '10000109', '股票周一会出现大跌什么的吗？要不要现在减仓啊！', '2016-06-24 14:12:38', '2016-06-24 14:12:38', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1716', '10000037', 'text', '油价怎么跌不下去呢', null, null, '2016-06-24 14:13:33', '2016-06-24 14:13:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1717', '10000037', 'text', '止损位317.2', null, null, '2016-06-24 14:13:44', '2016-06-24 14:13:44', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1718', '10000000', 'text', '【英国成功退欧】从技术上看，原油短期顶部已经完成，我们后市利用反弹就可以逢高做空操作', null, null, '2016-06-24 14:13:46', '2016-06-24 14:13:46', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1719', '10000003', 'text', '老师昨天推的中国汽研也涨了#20顺利出货。', null, null, '2016-06-24 14:13:57', '2016-06-24 14:13:57', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1720', '10000000', 'text', '别急，早盘已经跌了6,5%了，因为这是世纪豪赌，所以空头会有一些盈利单结利离场，导致原油出现小幅反弹', '10000037', '油价怎么跌不下去呢', '2016-06-24 14:14:39', '2016-06-24 14:14:39', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1721', '10000003', 'text', '但是还是没有油赚得多#1', null, null, '2016-06-24 14:15:16', '2016-06-24 14:15:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1722', '10000003', 'text', '以后多多给建议啊#9', null, null, '2016-06-24 14:16:11', '2016-06-24 14:16:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1723', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576cd097afc6c.jpg', null, null, '2016-06-24 14:17:59', '2016-06-24 14:17:59', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1724', '10000006', 'text', '#20', null, null, '2016-06-24 14:18:10', '2016-06-24 14:18:10', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1725', '10000102', 'text', '#20', null, null, '2016-06-24 14:23:21', '2016-06-24 14:23:21', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1726', '10000000', 'text', '【退欧公投专场】受到空头获利了结的原因，原油出现小幅反弹', null, null, '2016-06-24 14:27:18', '2016-06-24 14:27:18', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1727', '10000102', 'text', '还拿空单吗？', null, null, '2016-06-24 14:27:42', '2016-06-24 14:27:42', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1728', '10000000', 'text', '别急，控制好仓位在2成左右就好。原油还在筑顶', '10000102', '还拿空单吗？', '2016-06-24 14:28:42', '2016-06-24 14:28:42', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1729', '10000037', 'text', '还要买空吗', null, null, '2016-06-24 14:31:58', '2016-06-24 14:31:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1730', '10000000', 'text', '已经有空的先持有', '10000037', '还要买空吗', '2016-06-24 14:32:13', '2016-06-24 14:32:13', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1731', '10000037', 'text', '止损掉了，还要建吗', null, null, '2016-06-24 14:32:31', '2016-06-24 14:32:31', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1732', '10000000', 'text', '所有的操作建议都是按照卖价！所以点差要自己加的', '10000037', '止损掉了，还要建吗', '2016-06-24 14:33:32', '2016-06-24 14:33:32', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1733', '10000000', 'text', '【温馨提示】直播室所有的价格分析和操作建议止损止盈都是按照卖价来进行，操作的时候点差需要自己加！', null, null, '2016-06-24 14:34:16', '2016-06-24 14:34:16', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1734', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576cd4870e150.jpg', null, null, '2016-06-24 14:34:47', '2016-06-24 14:34:47', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1735', '10000000', 'text', '建议先等一下，等走稳', '10000037', '止损掉了，还要建吗', '2016-06-24 14:35:01', '2016-06-24 14:35:01', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1736', '10000000', 'text', '【退欧公投专场】这种短暂的大跌，市场出现小幅修正是正常的', null, null, '2016-06-24 14:36:57', '2016-06-24 14:36:57', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1737', '10000000', 'text', '【退欧公投专场】为赢得上一届大选，早在2013年时英国首相卡梅伦就公开承诺让英国人民有机会选择留在或退出欧盟；卡梅伦所在的保守党本身倾向留在欧盟，但显然误判英国民众对欧盟的了解程度，今日的公投结果虽不会让英国立即退出欧盟，但英国将启动退出流程已成事实，观察人士质疑“当一个国家超过一半选民在一个很重要的政治议题上与首相唱反调，这个人怎能继续留任首相？”', null, null, '2016-06-24 14:37:36', '2016-06-24 14:37:36', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1738', '10000000', 'text', '【退欧公投专场】外媒也预计卡梅伦可能会在今日晚些时候发表讲话并宣布离职。', null, null, '2016-06-24 14:37:51', '2016-06-24 14:37:51', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1739', '10000000', 'text', '【英国退欧引爆导火索：荷兰、法国也坐不住了】退欧派胜出此次英国公投。荷兰极右翼自由党领袖呼吁荷兰退欧公投，“荷兰人民也应该有这个机会，越快越好！”法国极右翼党派国民阵线主席亲属呼吁“是时候在法国引入民主机制，法国也应有权利选择。”丹麦红绿联盟也呼吁退欧公投。', null, null, '2016-06-24 14:41:59', '2016-06-24 14:41:59', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1740', '10000000', 'text', '【公投意味着英国马上退欧？远远没有那么简单】英国退欧公投结果出炉，尽管退欧派获胜，从理论上看，随后的退出过程似乎相当简单，但实际情况却可能极为复杂。瑞信此前报告称，政治和经济动荡很有可能引发第二次公投，英国最终仍然留在欧盟也不是没有可能。', null, null, '2016-06-24 14:43:09', '2016-06-24 14:43:09', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1741', '10000227', 'text', '老师你好', null, null, '2016-06-24 14:45:30', '2016-06-24 14:45:30', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1742', '10000000', 'text', '【退欧公投专场】市场暂时开始稳定下来。关注15:30分欧洲股市开盘对原油的影响', null, null, '2016-06-24 14:45:50', '2016-06-24 14:45:50', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1743', '10000000', 'text', '你好！', '10000227', '老师你好', '2016-06-24 14:45:55', '2016-06-24 14:45:55', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1744', '10000109', 'text', '老师今天晚上有什么大的数据吗？我空单现在要不要止盈出来！', '10000227', '老师你好', '2016-06-24 14:57:01', '2016-06-24 14:57:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1745', '10000109', 'text', '#1', null, null, '2016-06-24 14:57:37', '2016-06-24 14:57:37', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1746', '10000102', 'text', '行情大跌了', null, null, '2016-06-24 15:00:29', '2016-06-24 15:00:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1747', '10000003', 'text', '刚在315.4有搞进一个空单了', null, null, '2016-06-24 15:02:52', '2016-06-24 15:02:52', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1748', '10000003', 'text', '500桶的', null, null, '2016-06-24 15:03:52', '2016-06-24 15:03:52', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1749', '10000003', 'text', '现在在赚了900了。。要卖了吗', null, null, '2016-06-24 15:04:12', '2016-06-24 15:04:12', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1750', '10000000', 'text', '【退欧公投专场】英国脱欧消息拖累欧洲主要股市开盘狂泻千里。', null, null, '2016-06-24 15:06:54', '2016-06-24 15:06:54', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1751', '10000000', 'text', '控制好仓位即可', '10000003', '现在在赚了900了。。要卖了吗', '2016-06-24 15:07:48', '2016-06-24 15:07:48', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1752', '10000000', 'text', '今晚还有消费者信心指数', '10000109', '老师今天晚上有什么大的数据吗？我空单现在要不要止盈出来！', '2016-06-24 15:08:41', '2016-06-24 15:08:41', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1753', '10000000', 'text', '控制好仓位继续持有', '10000003', '现在在赚了900了。。要卖了吗', '2016-06-24 15:09:22', '2016-06-24 15:09:22', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1754', '10000000', 'text', '【退欧公投专场】欧洲银行股遭受重创', null, null, '2016-06-24 15:15:29', '2016-06-24 15:15:29', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1755', '10000000', 'text', '【汇市血雨腥风：离岸人民币盘中大跌逾600点】受英国退欧公投影响，人民币跟随全球金融市场受惊大跌。离岸人民币兑美元汇率盘中大跌逾600点，至五个半月新低。在岸人民币早盘跌至2011年1月以来新低。', null, null, '2016-06-24 15:16:18', '2016-06-24 15:16:18', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1756', '10000000', 'text', '【卡梅伦发布会】英国首相卡梅伦正在召开新闻发布会', null, null, '2016-06-24 15:23:28', '2016-06-24 15:23:28', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1757', '10000000', 'text', '【卡梅伦发布会】英国首相卡梅伦：必须准备与欧盟进行谈判。', null, null, '2016-06-24 15:23:52', '2016-06-24 15:23:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1758', '10000000', 'text', '【卡梅伦发布会】英国首相卡梅伦：希望市场和投资者放心，英国经济强劲。', null, null, '2016-06-24 15:24:08', '2016-06-24 15:24:08', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1759', '10000000', 'text', '【卡梅伦发布会】英国首相卡梅伦：对于居住在英国的欧盟公民不会立即面临变化。', null, null, '2016-06-24 15:24:27', '2016-06-24 15:24:27', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1760', '10000000', 'text', '【卡梅伦发布会】英国首相卡梅伦：英国需要全新的领导。', null, null, '2016-06-24 15:24:40', '2016-06-24 15:24:40', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1761', '10000000', 'text', '【卡梅伦发布会】英国首相卡梅伦：旨在10月选出新首相。', null, null, '2016-06-24 15:24:52', '2016-06-24 15:24:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1762', '10000000', 'text', '【卡梅伦发布会】英国首相卡梅伦：本人将在未来三个月内继续担任首相。', null, null, '2016-06-24 15:25:17', '2016-06-24 15:25:17', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1763', '10000000', 'text', ' 【卡梅伦发布会】黑色星期五对于油价来说无疑是噩梦，盘中一度下跌逾6%。虽然，对于油价来说今日跌势只是受惊，但基本面依然不容乐观，只怕英国脱欧打开了暴跌的潘多拉盒子。', null, null, '2016-06-24 15:26:15', '2016-06-24 15:26:15', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1764', '10000000', 'text', '【卡梅伦发布会】英国首相卡梅伦：与欧盟的谈判需要在新首相的带领下开始。', null, null, '2016-06-24 15:27:46', '2016-06-24 15:27:46', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1765', '10000000', 'text', '刚才没有做空的朋友依然可以做空操作，带好止损止盈', null, null, '2016-06-24 15:29:10', '2016-06-24 15:29:10', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1766', '10000000', 'text', '下面来看看今天的股市状况', null, null, '2016-06-24 15:38:38', '2016-06-24 15:38:38', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1767', '10000000', 'text', '周五早盘上证指数受到英国留欧、脱欧计票结果反复不定，风险加大的影响，出现大幅震荡，大盘在冲高触及到2900点压力位后，开始一路下行，大盘再度跳水。午后开盘沪指快速下挫，最低触及2807.60点，随后股指迅速反弹，创业板率先收复并翻红。截至收盘，沪指跌1.30%，报2854.29点，成交2149.13亿元。', null, null, '2016-06-24 15:38:53', '2016-06-24 15:38:53', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1768', '10000000', 'text', '盘面上看，黄金、采掘服务、杭州亚运会等板块涨上涨，养殖业、军工、网络安全、超导等板块跌幅居前。', null, null, '2016-06-24 15:39:04', '2016-06-24 15:39:04', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1769', '10000000', 'text', '消息面上，英国脱欧“黑天鹅”肆虐全球市场，英镑、欧元惨遭抛售，相对美元分别大幅贬值11%和4%，避险品种黄金、日元均上涨5%左右。同时，日经225指数期货在大阪一度跌8.1%触发熔断，香港恒生指数也大跌逾4%失守20000点大关。然而，A股市场却较为抗跌，呈现触底回升的态势，较外围市场来说，表现较为亮丽。', null, null, '2016-06-24 15:39:17', '2016-06-24 15:39:17', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1770', '10000053', 'text', '今天会不会跌到311以下啊？', null, null, '2016-06-24 15:39:41', '2016-06-24 15:39:41', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1771', '10000000', 'text', '每当重大事件来临时，正是投资者介入的时机，同时，投资者应控制好风险，并采用多元化的投资方式来提高收益，分散风险。譬如原油等大宗商品，T+O制度随买随卖及时锁定利润，双向做单机制既能做多也能做空顺势而为，让风险最小化，收益最大化。', null, null, '2016-06-24 15:40:53', '2016-06-24 15:40:53', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1772', '10000006', 'text', '还有下跌的可能吗', null, null, '2016-06-24 15:42:09', '2016-06-24 15:42:09', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1773', '10000000', 'text', '	\n这两天，直播室已经带给客户170%的利润了，相当于17个涨停板。同样是跌，原油就是有这样的魅力！#4', null, null, '2016-06-24 15:43:36', '2016-06-24 15:43:36', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1774', '10000000', 'text', '趋势已经是向下，我们找准机会做空，控制好仓位', '10000006', '还有下跌的可能吗', '2016-06-24 15:43:56', '2016-06-24 15:43:56', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1775', '10000000', 'text', '美元的跳水也是原油出现小幅下跌的原因', null, null, '2016-06-24 15:50:05', '2016-06-24 15:50:05', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1776', '8', 'text', '大家下午好！#9', null, null, '2016-06-24 15:56:12', '2016-06-24 15:56:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1777', '10000000', 'text', '好了，吴老师也来了，看看他对目前盘面有什么看法#4', null, null, '2016-06-24 15:57:08', '2016-06-24 15:57:08', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1778', '8', 'text', '今天真是一个令人激动的日子！', null, null, '2016-06-24 15:58:19', '2016-06-24 15:58:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1779', '8', 'text', '英国历尽艰辛终于脱欧想成功了！#4', null, null, '2016-06-24 15:59:30', '2016-06-24 15:59:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1780', '8', 'text', '这是英国给世界人民发福利呀，大家都赚爽了吗#17', null, null, '2016-06-24 16:01:06', '2016-06-24 16:01:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1781', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576ce98926ce9.png', null, null, '2016-06-24 16:04:25', '2016-06-24 16:04:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1782', '10000006', 'text', '我胆小，只赚到一波#12', null, null, '2016-06-24 16:10:45', '2016-06-24 16:10:45', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1783', '8', 'text', '目前，从盘面来看，行情处于一个空头回补状态。', null, null, '2016-06-24 16:13:40', '2016-06-24 16:13:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1784', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576cec1e19ee9.png', null, null, '2016-06-24 16:15:26', '2016-06-24 16:15:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1785', '10000102', 'text', '最高回补到哪个点位？', null, null, '2016-06-24 16:16:39', '2016-06-24 16:16:39', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1786', '8', 'text', '我们可以看到上方短期压力在320附近。', null, null, '2016-06-24 16:16:39', '2016-06-24 16:16:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1787', '8', 'text', '从盘面来看，在320附近。', '10000102', '最高回补到哪个点位？', '2016-06-24 16:17:34', '2016-06-24 16:17:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1788', '8', 'text', '320一线是之前的一个比较强的支撑，脱欧令行情成功下破，那么，这个位置就转化成短期的一个比较关键的压力位了。', null, null, '2016-06-24 16:21:26', '2016-06-24 16:21:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1789', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576cf033ec367.png', null, null, '2016-06-24 16:32:52', '2016-06-24 16:32:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1790', '8', 'text', '空头格局，反弹受阻，顺势做空。', null, null, '2016-06-24 16:33:06', '2016-06-24 16:33:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1791', '8', 'text', '有做单的朋友记得带好止盈止损。#9', null, null, '2016-06-24 16:35:52', '2016-06-24 16:35:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1792', '8', 'text', '在岸人民币兑美元今天16:30收盘报6.6148，较上一交易日下跌369点。', null, null, '2016-06-24 16:37:24', '2016-06-24 16:37:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1793', '8', 'text', '脱欧令货币风险大增', null, null, '2016-06-24 16:38:07', '2016-06-24 16:38:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1794', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576cf235166f2.png', null, null, '2016-06-24 16:41:25', '2016-06-24 16:41:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1795', '8', 'text', '美元指数今天暴涨350多点', null, null, '2016-06-24 16:42:01', '2016-06-24 16:42:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1796', '8', 'text', '黑天鹅如期降临，金融市场就像疯狂过山车一样。 ', null, null, '2016-06-24 16:50:08', '2016-06-24 16:50:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1797', '8', 'text', '英国成为史上第一个脱离欧盟的国家。', null, null, '2016-06-24 16:51:09', '2016-06-24 16:51:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1798', '8', 'text', '英国脱欧公投令油价一夜回到解放前。#4', null, null, '2016-06-24 16:54:26', '2016-06-24 16:54:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1799', '8', 'text', '目前美油、布油跌幅均逾6%', null, null, '2016-06-24 16:55:05', '2016-06-24 16:55:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1800', '8', 'text', '此时，沙特传出一个比EIA更能影响油价的消息。', null, null, '2016-06-24 17:00:13', '2016-06-24 17:00:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1801', '8', 'text', '原油供应向来最充沛的沙特发出了一个至今为止最强烈的信号。', null, null, '2016-06-24 17:01:27', '2016-06-24 17:01:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1802', '8', 'text', '2014年以来一直在虐待油价的供应过剩局面即将结束。', null, null, '2016-06-24 17:01:46', '2016-06-24 17:01:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1803', '10000006', 'text', '赶紧跌吧#5', null, null, '2016-06-24 17:03:10', '2016-06-24 17:03:10', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1804', '8', 'text', '据原油市场数据供应机构统计，尽管沙特的产量仍接近纪录高位，但它的原油库存已经连续六个月下降。', null, null, '2016-06-24 17:03:56', '2016-06-24 17:03:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1805', '8', 'text', '已经开始了#4', '10000006', '赶紧跌吧#5', '2016-06-24 17:05:02', '2016-06-24 17:05:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1806', '10000006', 'text', '老师，你这个空单要出的时候记得通知我啊，我317.2进去的', null, null, '2016-06-24 17:07:31', '2016-06-24 17:07:31', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1807', '8', 'text', '有跟我策略做了空单的朋友，按计划执行就好，没做的也不用着急，耐心等待机会，控制好仓位。', null, null, '2016-06-24 17:08:10', '2016-06-24 17:08:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1808', '8', 'text', '好的，你自己也记得设好止盈止损。', '10000006', '老师，你这个空单要出的时候记得通知我啊，我317.2进去的', '2016-06-24 17:08:51', '2016-06-24 17:08:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1809', '8', 'text', '约秘书长斯托尔滕伯格：尽管英国脱欧，但其在北约的地位仍然稳固不变。', null, null, '2016-06-24 17:17:41', '2016-06-24 17:17:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1810', '8', 'text', '惠誉表示脱欧将对英国主权债信评级产生一定的冲击，将在近期对该国主权评级进行评估。', null, null, '2016-06-24 17:18:40', '2016-06-24 17:18:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1811', '8', 'text', '随着部分金融机构业务迁往欧盟，英国作为全球主要金融中心的地位被削弱。', null, null, '2016-06-24 17:18:58', '2016-06-24 17:18:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1812', '8', 'text', '英国五年期信用违约互换飙升23个基点至56个基点，创下近四年最高。', null, null, '2016-06-24 17:21:45', '2016-06-24 17:21:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1813', '8', 'text', '虽然脱欧结果已出，但后续具体怎么脱离，还有很长一段拉锯战。根据《里斯本条约》第50条规定，有退欧意向的成员国需要与欧盟进行为期两年的谈判，而如果两年内双方没有达成协议，英国将需要在除贸易之外的几乎所有领域和欧盟重新一一协商，除非其他全部成员国同意延长谈判期限。', null, null, '2016-06-24 17:31:57', '2016-06-24 17:31:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1814', '8', 'text', '“脱欧”引发全球投资者风险偏好下滑，整体利好美元、金银、日元等避险资产，利空股票、新兴市场货币、原油、工业金属等风险资产，由于脱欧将使得英国和欧盟两败俱伤，同时利空英镑和欧元。——以上是脱欧对全球大类资产的影响逻辑，为大家再梳理了一遍。', null, null, '2016-06-24 17:35:50', '2016-06-24 17:35:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1815', '8', 'text', '在公投结果出来后的欧市早盘，除了欧洲股市普遍大幅下挫，其他市场表现相对平静，我们先养精蓄锐，晚上静待美盘开市后市场的变化，以及交易的机会。', null, null, '2016-06-24 17:39:39', '2016-06-24 17:39:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1816', '8', 'text', '欧洲央行已准备好在必要时以欧元或其它货币提供额外的流动性。', null, null, '2016-06-24 17:40:47', '2016-06-24 17:40:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1817', '8', 'text', '国际能源署表示英国脱欧对全球原油供应和需求的基本面没有影响。', null, null, '2016-06-24 17:41:23', '2016-06-24 17:41:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1818', '8', 'text', '俄罗斯能源部长：英国脱欧或会导致近期油市剧烈波动。', null, null, '2016-06-24 17:52:16', '2016-06-24 17:52:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1819', '8', 'text', '俄罗斯能源部长诺瓦克：受其他因素的共同作用，油价可能将大跌。', null, null, '2016-06-24 17:53:08', '2016-06-24 17:53:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1820', '8', 'text', '油价大跌，我们就有做空的机会。', null, null, '2016-06-24 17:53:49', '2016-06-24 17:53:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1821', '8', 'text', '俄罗斯能源部消息称没有必要因为英国脱欧而使大型产油商有进一步的行动。', null, null, '2016-06-24 18:04:41', '2016-06-24 18:04:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1822', '8', 'text', '说明英国脱欧对原油基本面没有很大影响。', null, null, '2016-06-24 18:05:38', '2016-06-24 18:05:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1823', '10000000', 'text', '请随时关注直播室左边的操作建议哦', '10000006', '老师，你这个空单要出的时候记得通知我啊，我317.2进去的', '2016-06-24 18:08:23', '2016-06-24 18:08:23', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1824', '10000102', 'text', '可以落到309以下吗？', null, null, '2016-06-24 18:10:44', '2016-06-24 18:10:44', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1825', '10000000', 'text', '空单的第一目标就是先看到今天的低点308~309附近', '10000102', '可以落到309以下吗？', '2016-06-24 18:13:04', '2016-06-24 18:13:04', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1826', '10000000', 'text', '欧盟联合声明：对英国的决定感到遗憾但表示尊重；27个成员国组成的欧盟将继续下去。', null, null, '2016-06-24 18:14:26', '2016-06-24 18:14:26', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1827', '10000000', 'text', '欧盟联合声明：欧盟准备好与英国迅速展开谈判。', null, null, '2016-06-24 18:14:34', '2016-06-24 18:14:34', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1828', '10000000', 'text', '欧盟联合声明：欧盟2月起与英国达成的协议将不会再进行谈判。', null, null, '2016-06-24 18:14:40', '2016-06-24 18:14:40', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1829', '10000102', 'text', '#2我在那里建的空单', null, null, '2016-06-24 18:14:43', '2016-06-24 18:14:43', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1830', '10000000', 'text', '欧盟联合声明：英国必须尽快启动脱欧进程。', null, null, '2016-06-24 18:14:48', '2016-06-24 18:14:48', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1831', '10000000', 'text', '合理控制好仓位即可，原油短期已经偏弱', '10000102', '#2我在那里建的空单', '2016-06-24 18:16:19', '2016-06-24 18:16:19', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1832', '10000000', 'text', '英国脱欧虽然告一段落，但后续的问题可能是爆炸性的。除了荷兰，丹麦，法国等国国内的党羽都提出要公投外，英国本身的苏格兰也计划发表独立言论。因为近几百年来，苏格兰就一直都脱离英国独立的想法。', null, null, '2016-06-24 18:30:51', '2016-06-24 18:30:51', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1833', '10000000', 'text', '一旦欧洲再出闹剧，那么后市原油将会继续演绎疯狂！', null, null, '2016-06-24 18:31:16', '2016-06-24 18:31:16', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1834', '10000000', 'text', '大家还犹豫什么？还没感受到我们原油比股市魅力大的地方吗？#4', null, null, '2016-06-24 18:31:55', '2016-06-24 18:31:55', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1835', '10000000', 'text', '我们的伍老师也是独具慧眼，刚才果断反弹到位后及时给出做空的操作建议。', null, null, '2016-06-24 18:33:06', '2016-06-24 18:33:06', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1836', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576d0c8c2d15e.jpg', null, null, '2016-06-24 18:33:48', '2016-06-24 18:33:48', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1837', '10000006', 'text', '', '10000102', '#2我在那里建的空单', '2016-06-24 18:54:25', '2016-06-24 18:54:25', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1838', '10000000', 'text', '下面把时间交回给伍老师。#4', null, null, '2016-06-24 18:56:10', '2016-06-24 18:56:10', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1839', '10000006', 'text', '', '10000102', '#2我在那里建的空单', '2016-06-24 18:57:41', '2016-06-24 18:57:41', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1840', '10000006', 'text', '#2', null, null, '2016-06-24 18:58:15', '2016-06-24 18:58:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1841', '10000006', 'text', '怎么买那么低去了', null, null, '2016-06-24 18:58:29', '2016-06-24 18:58:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1842', '8', 'text', '朋友们晚上好！我回来了。', null, null, '2016-06-24 19:00:33', '2016-06-24 19:00:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1843', '10000000', 'text', '轩湛投资观察室欢迎大家踊跃发言和提问？不怕你讲错，因为我们有老师能够解决你的交易问题；就怕你不讲，这只会错上加错！#15', null, null, '2016-06-24 19:01:43', '2016-06-24 19:01:43', '-1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1844', '10000000', 'text', '轩湛投资观察室欢迎大家踊跃发言和提问！不怕你讲错，因为我们有老师能够解决你的交易问题；就怕你不讲，这只会错上加错！', null, null, '2016-06-24 19:01:54', '2016-06-24 19:01:54', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1845', '10000100', 'text', '老师，今晚行情怎么看', null, null, '2016-06-24 19:02:59', '2016-06-24 19:02:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1846', '8', 'text', '按照目前是走势，下去的可能性很大。', '10000102', '可以落到309以下吗？', '2016-06-24 19:03:10', '2016-06-24 19:03:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1847', '10000000', 'text', '今晚的行情还是以震荡偏弱为主，但由于今天已经跌幅达到6.5%，所以可能晚间维持弱势震荡。但后市继续回落的空间非常大。希望能好好把握', '10000100', '老师，今晚行情怎么看', '2016-06-24 19:04:19', '2016-06-24 19:04:19', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1848', '10000102', 'text', '伍老师，309空，在哪儿止盈？', null, null, '2016-06-24 19:06:44', '2016-06-24 19:06:44', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1849', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576d1594781e8.png', null, null, '2016-06-24 19:12:20', '2016-06-24 19:12:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1850', '8', 'text', '穆迪表示，伴随英国-欧盟未来贸易关系谈判而来的“长久不确定性”，不利于英国和英国其他债券发行人的信用评级。', null, null, '2016-06-24 19:19:49', '2016-06-24 19:19:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1851', '10000053', 'text', '什么时候还会涨上去？我314.7建了多单', null, null, '2016-06-24 19:33:20', '2016-06-24 19:33:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1852', '8', 'text', '目前，盘面比较平静。大家有什么问题，都可以提出来。', null, null, '2016-06-24 19:33:30', '2016-06-24 19:33:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1853', '10000160', 'text', '#20有人说今天是无脑空单，你还多啊', null, null, '2016-06-24 19:38:59', '2016-06-24 19:38:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1854', '8', 'text', '摩根大通预期欧股在未来几天将屡创新低。', null, null, '2016-06-24 19:48:52', '2016-06-24 19:48:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1855', '8', 'text', '英国退欧对欧洲经济将造成很大破坏。', null, null, '2016-06-24 19:49:58', '2016-06-24 19:49:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1856', '10000102', 'text', '啥时候回跌', null, null, '2016-06-24 19:57:41', '2016-06-24 19:57:41', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1857', '10000102', 'text', '美盘时间段，会不会跌？', null, null, '2016-06-24 19:58:05', '2016-06-24 19:58:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1858', '10000102', 'text', '为啥又涨了？', null, null, '2016-06-24 20:01:12', '2016-06-24 20:01:12', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1859', '10000006', 'text', '这些314买涨的赚钱了', null, null, '2016-06-24 20:03:38', '2016-06-24 20:03:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1860', '8', 'text', '部分空头获利了结。', '10000102', '为啥又涨了？', '2016-06-24 20:05:18', '2016-06-24 20:05:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1861', '8', 'text', '短线单关键在于找准支撑压力。', '10000006', '这些314买涨的赚钱了', '2016-06-24 20:06:36', '2016-06-24 20:06:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1862', '8', 'text', '目前空头格局比较明朗，做多单的朋友建议是以短线为主，有利润要找机会落袋为安。', null, null, '2016-06-24 20:10:15', '2016-06-24 20:10:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1863', '8', 'text', '行情短线出现冲高回落，说明上方压力还是很强。', null, null, '2016-06-24 20:12:48', '2016-06-24 20:12:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1864', '8', 'text', '价格又下来了，多单朋友要注意控制风险。', null, null, '2016-06-24 20:16:35', '2016-06-24 20:16:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1865', '8', 'text', '北京时间20:30将公布：美国5月耐用品订单月率。', null, null, '2016-06-24 20:27:08', '2016-06-24 20:27:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1866', '10000013', 'text', '老师，我317的多单怎么办？', '10000102', '可以落到309以下吗？', '2016-06-24 20:28:22', '2016-06-24 20:28:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1867', '8', 'text', '实际值大于预期值，利好美元，利空原油。', null, null, '2016-06-24 20:28:38', '2016-06-24 20:28:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1868', '10000057', 'text', '老师棒棒的#14', null, null, '2016-06-24 20:35:32', '2016-06-24 20:35:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1869', '10000102', 'text', '#13还不跌', null, null, '2016-06-24 20:39:20', '2016-06-24 20:39:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1870', '8', 'text', '不要着急，交易最忌心浮气躁，耐心是成功交易的基础。', '10000102', '#13还不跌', '2016-06-24 20:41:36', '2016-06-24 20:41:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1871', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576d2ae64f25d.png', null, null, '2016-06-24 20:43:18', '2016-06-24 20:43:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1872', '8', 'text', '看来这项数据影响力有限。', null, null, '2016-06-24 20:44:09', '2016-06-24 20:44:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1873', '8', 'text', '【华尔街日报评美国5月耐用品订单月率】美国5月耐用品订单月率不及预期且有所下滑，飞机、工业设备和其他耐用品的新订单相比上月大幅减少，尤其是军用飞机方面的订单下滑34.1%，这拖累了耐用品订单的整体表现；此次数据疲软意味着本春季美国在新设备方面的商业投资表现不佳。', null, null, '2016-06-24 20:44:28', '2016-06-24 20:44:28', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1874', '8', 'text', '美国联邦基金利率市场：美联储今年加息几率接近0，12月加息几率仅有10%。', null, null, '2016-06-24 20:45:39', '2016-06-24 20:45:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1875', '8', 'text', '美联储要是今年不加息，对原油多头来说确是一个好消息。', null, null, '2016-06-24 20:47:39', '2016-06-24 20:47:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1876', '8', 'text', '“美联储通讯社”Hilsenrath：英国脱欧意味着美国或推迟加息。', null, null, '2016-06-24 20:48:22', '2016-06-24 20:48:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1877', '10000194', 'text', '耐心等待，才收获大鱼#4', null, null, '2016-06-24 20:48:43', '2016-06-24 20:48:43', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1878', '8', 'text', '“美联储通讯社”Hilsenrath：7月加息现在看起来是不可能的了。', null, null, '2016-06-24 20:48:43', '2016-06-24 20:48:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1879', '10000187', 'text', '#20', '10000194', '耐心等待，才收获大鱼#4', '2016-06-24 20:49:59', '2016-06-24 20:49:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1880', '8', 'text', '朋友你已经领悟到交易的真谛了#14', '10000194', '耐心等待，才收获大鱼#4', '2016-06-24 20:51:18', '2016-06-24 20:51:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1881', '8', 'text', '据BBC：摩根士丹利据称开始将2000名伦敦员工迁往都柏林和法兰克福。', null, null, '2016-06-24 21:04:00', '2016-06-24 21:04:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1882', '8', 'text', '伦敦世界金融中心地位受到影响', null, null, '2016-06-24 21:04:30', '2016-06-24 21:04:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1883', '10000194', 'text', '老师311。5就出了吗#3', null, null, '2016-06-24 21:05:53', '2016-06-24 21:05:53', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1884', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576d32a879464.png', null, null, '2016-06-24 21:16:24', '2016-06-24 21:16:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1885', '8', 'text', '今晚还有两项数据没有出来，大家可以关注一下。', null, null, '2016-06-24 21:17:19', '2016-06-24 21:17:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1886', '8', 'text', '特别是后面一个，对原油有比较重要的影响。', null, null, '2016-06-24 21:18:48', '2016-06-24 21:18:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1887', '10000006', 'text', '老师，我317.2的空单现在能提前出来吗？', null, null, '2016-06-24 21:24:44', '2016-06-24 21:24:44', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1888', '10000194', 'text', '买得真高#14', '10000006', '老师，我317.2的空单现在能提前出来吗？', '2016-06-24 21:27:20', '2016-06-24 21:27:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1889', '8', 'text', '目前盘面呈震荡收窄三角形走势，下方受312一线支撑。', null, null, '2016-06-24 21:30:46', '2016-06-24 21:30:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1890', '10000006', 'text', '老师建议的317.8做空啊，我买的低了一点', null, null, '2016-06-24 21:31:42', '2016-06-24 21:31:42', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1891', '10000006', 'text', '#17', null, null, '2016-06-24 21:31:50', '2016-06-24 21:31:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1892', '10000194', 'text', '破312线会继续(┬＿┬)↘ 跌吗', null, null, '2016-06-24 21:32:16', '2016-06-24 21:32:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1893', '8', 'text', '以后要手快呀#4', '10000006', '#17', '2016-06-24 21:32:30', '2016-06-24 21:32:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1894', '10000006', 'text', '我刚才313已经抛了，因为我有聚会待会没法看盘', null, null, '2016-06-24 21:32:35', '2016-06-24 21:32:35', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1895', '10000006', 'text', '谢谢老师，么么哒#9', null, null, '2016-06-24 21:32:50', '2016-06-24 21:32:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1896', '10000006', 'text', '这周跟老师做单感觉好轻松', null, null, '2016-06-24 21:33:07', '2016-06-24 21:33:07', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1897', '10000194', 'text', '看好就收，我们还没开始盈利', '10000006', '我刚才313已经抛了，因为我有聚会待会没法看盘', '2016-06-24 21:33:13', '2016-06-24 21:33:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1898', '8', 'text', '可以的，落袋为安嘛。', '10000006', '我刚才313已经抛了，因为我有聚会待会没法看盘', '2016-06-24 21:33:16', '2016-06-24 21:33:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1899', '8', 'text', '不着急，80%的空间都是在20%的时间走完，横盘后才是大收益。', '10000194', '看好就收，我们还没开始盈利', '2016-06-24 21:36:01', '2016-06-24 21:36:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1900', '8', 'text', '行情出现短线上冲', null, null, '2016-06-24 21:46:08', '2016-06-24 21:46:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1901', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576d39c87109b.png', null, null, '2016-06-24 21:46:48', '2016-06-24 21:46:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1902', '8', 'text', '空单获利先出吧。', null, null, '2016-06-24 21:47:25', '2016-06-24 21:47:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1903', '8', 'text', '不想平仓的也建议设好保护性止损。', null, null, '2016-06-24 21:48:19', '2016-06-24 21:48:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1904', '8', 'text', '道指开盘一分钟迅速下泻400余点。', null, null, '2016-06-24 21:50:28', '2016-06-24 21:50:28', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1905', '8', 'text', '美银美林：将美联储加息时间预期延迟至12月。', null, null, '2016-06-24 21:50:38', '2016-06-24 21:50:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1906', '8', 'text', '北京时间22:00将公布：美国6月密歇根大学消费者信心指数终值。', null, null, '2016-06-24 21:50:57', '2016-06-24 21:50:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1907', '10000194', 'text', '#7我还差三块钱才开始盈利。#4#4#4我的心在等待永远在等待', null, null, '2016-06-24 21:56:13', '2016-06-24 21:56:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1908', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160624/576d3d90656f5.png', null, null, '2016-06-24 22:02:56', '2016-06-24 22:02:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1909', '8', 'text', '数据利多。', null, null, '2016-06-24 22:03:04', '2016-06-24 22:03:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1910', '10000194', 'text', '#10#10', null, null, '2016-06-24 22:03:59', '2016-06-24 22:03:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1911', '8', 'text', '这项数据影响不大', null, null, '2016-06-24 22:04:22', '2016-06-24 22:04:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1912', '10000194', 'text', '吓死宝宝#19', null, null, '2016-06-24 22:06:02', '2016-06-24 22:06:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1913', '8', 'text', '这是美国消费者信息指数，反应美国经济状况，对原油影响有限。', '10000194', '#10#10', '2016-06-24 22:08:05', '2016-06-24 22:08:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1914', '8', 'text', '淡定#9', '10000194', '吓死宝宝#19', '2016-06-24 22:10:11', '2016-06-24 22:10:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1915', '10000194', 'text', '老师在，都放心#6', null, null, '2016-06-24 22:13:27', '2016-06-24 22:13:27', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1916', '8', 'text', '亚特兰大联储GDPNOW模型：将第二季度GDP增长预期由2.8%下调至2.6%。', null, null, '2016-06-24 22:37:36', '2016-06-24 22:37:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1917', '8', 'text', '英国投资机构景顺基金欧洲股票主管Taylor：把英国脱欧视作欧盟彻底分裂的导火索是错误的；这仅仅是一些政客持有反欧盟立场，并不意味着一定会出现一系列的类似后续公投。', null, null, '2016-06-24 22:53:00', '2016-06-24 22:53:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1918', '8', 'text', '中国央行行长周小川：中国央行将维持物价稳定放在高度优先的地位。', null, null, '2016-06-24 23:12:26', '2016-06-24 23:12:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1919', '8', 'text', '中国央行行长周小川：将根据今后的情况动态调整货币政策。', null, null, '2016-06-24 23:12:43', '2016-06-24 23:12:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1920', '10000106', 'text', '等待多么煎熬利空还好,利多又伤财又劳心', null, null, '2016-06-24 23:19:54', '2016-06-24 23:19:54', '-1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1921', '10000106', 'text', '312没看,要是盯盘亏一二千平仓睡个好觉多好,现在还不知会不会到312', null, null, '2016-06-24 23:23:17', '2016-06-24 23:23:17', '-1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1922', '8', 'text', '随着英国“脱欧”公投结果确定，美国金融大鳄索罗斯有望再次从英国金融市场的动荡中大赚一笔，重写他在1992年打赢英镑狙击战的神话。', null, null, '2016-06-24 23:34:10', '2016-06-24 23:34:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1923', '8', 'text', '他上周在英国《卫报》撰文警告称，如果英国选择退出欧盟，将在全球金融市场触发“黑色星期五”，并预言英镑可能下跌15-20%', null, null, '2016-06-24 23:36:02', '2016-06-24 23:36:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1924', '8', 'text', '1992年，索罗斯曾抓住时机大举做空英镑，在这场世纪豪赌中，索罗斯和旗下量子基金打败了英国央行，赚取了逾十亿美元的暴利，并迫使英国退出欧洲汇率机制。', null, null, '2016-06-24 23:37:13', '2016-06-24 23:37:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1925', '8', 'text', '他的投资招牌绝技是——永远不要靠市场走高获利。', null, null, '2016-06-24 23:37:59', '2016-06-24 23:37:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1926', '8', 'text', '上世纪70年代，全球股市大多低迷，市场中性策略帮助索罗斯逆市狂赚，十年里回报率超过4000%。', null, null, '2016-06-24 23:39:08', '2016-06-24 23:39:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1927', '8', 'text', '过去四十年，索罗斯旗下的量子基金获利高达396亿美元。', null, null, '2016-06-24 23:39:52', '2016-06-24 23:39:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1928', '8', 'text', '说他是投资世界的神确实不为过。', null, null, '2016-06-24 23:42:49', '2016-06-24 23:42:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1929', '8', 'text', '市场交投平淡，一直在日内均线附近波动。', null, null, '2016-06-24 23:51:53', '2016-06-24 23:51:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1930', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160625/576d5be01b693.png', null, null, '2016-06-25 00:12:16', '2016-06-25 00:12:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1931', '8', 'text', '上周五美国油服贝克休斯公布的数据显示，截至6月17日当周，美国原油钻井平台数量大增9口，增至337口。', null, null, '2016-06-25 00:15:02', '2016-06-25 00:15:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1932', '8', 'text', '美国原油钻井数量已经连续3周上升。', null, null, '2016-06-25 00:16:48', '2016-06-25 00:16:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1933', '8', 'text', '另据彭博社的研究报告显示，虽然美国石油钻井出现增加的“迹象”，但是若要美国钻井机数量大幅增加，或需国际原油价格升至55美元/桶上方。', null, null, '2016-06-25 00:22:16', '2016-06-25 00:22:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1934', '8', 'text', '若公布数据大于337，原油价格将进一步承压，那么下周美国的原油库存增加的可能性也会大增。\n', null, null, '2016-06-25 00:25:02', '2016-06-25 00:25:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1935', '8', 'text', '美国石油钻井数据利多。', null, null, '2016-06-25 01:04:01', '2016-06-25 01:04:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1936', '8', 'text', '与预期值相差不大，影响有限。', null, null, '2016-06-25 01:05:39', '2016-06-25 01:05:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1937', '8', 'text', '盘面上交投也比较平淡，市场并没有过度反应。', null, null, '2016-06-25 01:12:53', '2016-06-25 01:12:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1938', '8', 'text', '明天就是周末了', null, null, '2016-06-25 01:14:05', '2016-06-25 01:14:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1939', '8', 'text', '今天英国退欧结果刚出来，周末各个央行可能都会有动作。', null, null, '2016-06-25 01:14:44', '2016-06-25 01:14:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1940', '8', 'text', '所以持仓过周末的朋友要注意控制风险。', null, null, '2016-06-25 01:15:34', '2016-06-25 01:15:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1941', '8', 'text', '今天如果没有理想的点位，不要急于入场了，等待下周的机会。', null, null, '2016-06-25 01:16:12', '2016-06-25 01:16:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1942', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160625/576d6b9716922.png', null, null, '2016-06-25 01:19:19', '2016-06-25 01:19:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1943', '10000194', 'text', '可以持仓过周末吗', null, null, '2016-06-25 01:20:50', '2016-06-25 01:20:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1944', '8', 'text', '持仓过周末是可以的，但是要记得带好止盈止损，控制风险。', '10000194', '可以持仓过周末吗', '2016-06-25 01:23:10', '2016-06-25 01:23:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1945', '8', 'text', '行情走到现在，其实也还处在一个宽幅震荡的区间里，脱欧事件加速了行情的走势，这或可在下周打破这种格局，从盘面可以看到，上方关键压力位置在330附近，下方支撑在300附近，', null, null, '2016-06-25 01:25:57', '2016-06-25 01:25:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1946', '8', 'text', '行情一旦突破，或又会重启单边上涨或下跌趋势，空间非常可观。', null, null, '2016-06-25 01:27:54', '2016-06-25 01:27:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1947', '10000194', 'text', '#2哪方面的几率会大一些', null, null, '2016-06-25 01:32:28', '2016-06-25 01:32:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1948', '8', 'text', '好了，今天的直播就好这里了，我们养精蓄锐，下周再战！祝大家周末愉快！#9', null, null, '2016-06-25 01:39:31', '2016-06-25 01:39:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1949', '10000000', 'text', '大家早上好！欢迎来到轩湛投资观察室！#4', null, null, '2016-06-27 09:26:50', '2016-06-27 09:26:50', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1950', '10000187', 'text', '老师早上好，今天会怎么走呢？', null, null, '2016-06-27 09:35:27', '2016-06-27 09:35:27', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1951', '10000000', 'text', '在晨光中慢慢醒来，听听鸟语，闻闻花香，让心情更加舒畅，排却烦恼忧伤，接纳快乐芬芳，让每一天都充满阳光，让每一秒都心情飞扬！#4#9', null, null, '2016-06-27 09:37:26', '2016-06-27 09:37:26', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1952', '10000000', 'text', '好了，下面就给大家回顾和分析一下目前的走势，请大家稍安勿躁', null, null, '2016-06-27 09:39:20', '2016-06-27 09:39:20', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1953', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5770848b88770.jpg', null, null, '2016-06-27 09:42:35', '2016-06-27 09:42:35', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1954', '10000000', 'text', '这是原油的日线图，大家可以看到，经过上周的回落后，原油短期的顶部已经成型。同时价格已经跌破均线组，MACD等指标也出现死叉回落。短期下行格局明显。', null, null, '2016-06-27 09:45:56', '2016-06-27 09:45:56', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1955', '10000000', 'text', '所以，日内我们依然维持逢高做空的思路，找准强压力附近布局空单。上方压力在314.5一带，下方短期支撑在306~307.', null, null, '2016-06-27 09:47:45', '2016-06-27 09:47:45', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1956', '10000000', 'text', '温馨提示一下：英国退欧以后，将会引发持续性的政治和经济问题，例如目前英国有民众提议第二次公投，苏格兰也在酝酿脱英公投等，所以后续的行情依然具备非常多的机会，希望大家能够把握住了#15#9', null, null, '2016-06-27 09:50:02', '2016-06-27 09:50:02', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1957', '10000000', 'text', '而上周公投日，直播室也给出了两单空头的操作机会，共盈利61%以上。而上周直播室开播三天以来，则给大家带来了2倍以上的利润！', null, null, '2016-06-27 09:51:46', '2016-06-27 09:51:46', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1958', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160627/577086d20b09d.jpg', null, null, '2016-06-27 09:52:18', '2016-06-27 09:52:18', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1959', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160627/577087089063b.jpg', null, null, '2016-06-27 09:53:12', '2016-06-27 09:53:12', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1960', '10000000', 'text', '所以，轩湛投资观察室优秀的分析师团队将为你的投资保驾护航！#4', null, null, '2016-06-27 09:57:31', '2016-06-27 09:57:31', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1961', '10000000', 'text', '好了，下面先回到盘面。', null, null, '2016-06-27 10:00:33', '2016-06-27 10:00:33', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1962', '10000000', 'text', '受到今天A股高开的影响，原油早盘探底后出现反弹', null, null, '2016-06-27 10:04:31', '2016-06-27 10:04:31', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1963', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160627/57708a3d3ce3c.jpg', null, null, '2016-06-27 10:06:53', '2016-06-27 10:06:53', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1964', '10000000', 'text', '下面我们来看看今天A股的走势情况', null, null, '2016-06-27 10:07:09', '2016-06-27 10:07:09', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1965', '10000000', 'text', '早盘受到工业利润数据回升的影响，A股高开。目前勉强守住2780~2800区域的支撑区。', null, null, '2016-06-27 10:07:53', '2016-06-27 10:07:53', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1966', '10000000', 'text', '技术上，目前A股依然在底部震荡，短期上行动力依然不足。', null, null, '2016-06-27 10:08:20', '2016-06-27 10:08:20', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1967', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160627/57708b07ce1f8.jpg', null, null, '2016-06-27 10:10:15', '2016-06-27 10:10:15', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1968', '10000000', 'text', '板块方面，钢铁、煤炭、军工板块等一路冲高，随后在高位震荡。', null, null, '2016-06-27 10:22:10', '2016-06-27 10:22:10', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1969', '10000000', 'text', '消息面上，27日上午，李克强将出席2016夏季达沃斯论坛开幕式并发表特别致辞。而脱欧后全球局势复杂军工或具避险价值；宝钢集团与武钢集团重组央企整合进入加速阶段。', null, null, '2016-06-27 10:23:27', '2016-06-27 10:23:27', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1970', '10000000', 'text', '而操作方面，建议多些耐心，稳坐钓鱼台。这句话有两个意思。多些耐心指投资者不用急着重仓满仓进场，不妨多观察一段时间，等待市场指数再一次出现好的买点。稳坐钓鱼台指投资者在下跌时也不用过于恐慌， A股已经经历三轮大的调整，目前点位从高点下来接近腰斩，部分股票已经逐步显现投资价值，可以考虑撒网下钩。', null, null, '2016-06-27 10:24:33', '2016-06-27 10:24:33', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1971', '10000000', 'text', '个股推荐方面，建议关注怡球资源(601388)，康跃科技(300391)，狮头股份(600539)', null, null, '2016-06-27 10:29:56', '2016-06-27 10:29:56', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1972', '10000000', 'text', '怡球资源(601388)6月26日晚间公告，公司董事会审议通过了《关于公司2015年末未分配利润送红股及资本公积转增股本方案的议案》，拟以截至2015年12月31日公司股本总数5.33亿股为基数，以资本公积向全体股东每10股转增19股；同时，以2015年末公司可供分配利润向全体股东每10股送红股9股。', null, null, '2016-06-27 10:30:16', '2016-06-27 10:30:16', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1973', '10000000', 'text', '康跃科技(300391)6月24日晚间公告，公司拟通过发行股份和支付现金相结合的方式，对价9亿元，购买羿珩科技100%股份，发行价格为16.30 元/股。同时，公司拟向包括公司控股股东康跃投资在内的不超过5名特定对象发行股份募集配套资金4.168亿元，用于支付购买标的资产的现金对价，以及中介机构费用等相关交易税费。', null, null, '2016-06-27 10:30:33', '2016-06-27 10:30:33', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1974', '10000000', 'text', '狮头股份(600539)6月24日晚间披露重组预案，公司拟将水泥主业相关的业务、资产和负债，包括所持有的狮头中联51%的股权，转让给控股股东狮头集团。同时，公司拟以支付现金方式向上海纳克和潞安煤基油购买二者合计持有的山西潞安纳克碳一化工有限公司(以下简称“潞安纳克”)100%股权。公司股票继续停牌。', null, null, '2016-06-27 10:30:48', '2016-06-27 10:30:48', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1975', '10000000', 'text', '以上就是今天的A股早盘情况', null, null, '2016-06-27 10:32:47', '2016-06-27 10:32:47', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1976', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5770937ad4fea.jpg', null, null, '2016-06-27 10:46:18', '2016-06-27 10:46:18', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1977', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5770939cc86fc.jpg', null, null, '2016-06-27 10:46:52', '2016-06-27 10:46:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1978', '10000000', 'text', '今天需要关注美国的两个数据', null, null, '2016-06-27 10:47:02', '2016-06-27 10:47:02', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1979', '10000000', 'text', '原油继续反弹，关注是否有操作的机会', null, null, '2016-06-27 11:10:30', '2016-06-27 11:10:30', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1980', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160627/577099a6991bd.jpg', null, null, '2016-06-27 11:12:38', '2016-06-27 11:12:38', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1981', '10000000', 'text', '由于今天早上的下跌没有能创出新低，所以原油短期可能会先震荡', null, null, '2016-06-27 11:20:55', '2016-06-27 11:20:55', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1982', '10000000', 'text', '目前原油反弹到314附近的压力，激进的可以先轻仓做空，2成仓位以内，止损318，止盈先看309.稳健的先等走稳', null, null, '2016-06-27 11:30:55', '2016-06-27 11:30:55', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1983', '10000000', 'text', '目前原油反弹到314附近的压力，激进的可以先轻仓做空，2成仓位以内，止损318，止盈先看309.稳健的先等走稳', null, null, '2016-06-27 11:33:25', '2016-06-27 11:33:25', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1984', '10000000', 'text', '英国退欧事件才刚落幕，二次公投的提议还有苏格兰独立公投又准备来了', null, null, '2016-06-27 11:44:48', '2016-06-27 11:44:48', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1985', '10000013', 'text', '老师，我现在手上有空单，哪个点位可以平呢？', null, null, '2016-06-27 11:49:05', '2016-06-27 11:49:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('1986', '10000000', 'text', '现在还是中期偏空，但短期可能还要在区间305~315区域震荡，因为之前跌幅太大了。所以建议如果有空单，可以在310下方先获利', '10000013', '老师，我现在手上有空单，哪个点位可以平呢？', '2016-06-27 11:52:38', '2016-06-27 11:52:38', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1987', '10000000', 'text', '好了，大家可以先去吃饭，稍后一点钟我们继续下午的行情直播', null, null, '2016-06-27 12:01:13', '2016-06-27 12:01:13', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('1988', '8', 'text', '朋友们下午好，#9下午将由我继续给大家带来原油直播。', null, null, '2016-06-27 12:59:32', '2016-06-27 12:59:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1989', '8', 'text', '扣人心弦的英国公投出乎意料地以“脱欧”成功而结束了。', null, null, '2016-06-27 13:05:26', '2016-06-27 13:05:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1990', '8', 'text', '全球市场也随着民调、投票结果的变化而经历了过山车般的刺激行情。', null, null, '2016-06-27 13:06:02', '2016-06-27 13:06:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1991', '8', 'text', '对于一个立志从事投资的人来说，尤其是宏观交易者，这两天算是难得一见的盛况。', null, null, '2016-06-27 13:09:11', '2016-06-27 13:09:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1992', '8', 'text', '虽然英国“脱欧”公投结果已尘埃落定，但这几天的变化足以让人学到很多。', null, null, '2016-06-27 13:11:15', '2016-06-27 13:11:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1993', '8', 'text', '英国“脱欧”后，英国未来将不再适用欧盟的一系列经济贸易等政策。同时，如果英国“脱欧”引发英国内部苏格兰、威尔士要脱离英国，很可能再度引发英国震动。', null, null, '2016-06-27 13:17:26', '2016-06-27 13:17:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1994', '8', 'text', '此外，英国“脱欧”是全球民粹主义和社会思潮保守的先行信号，因此也不排除欧洲部分国家可能考虑脱欧。', null, null, '2016-06-27 13:19:05', '2016-06-27 13:19:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1995', '8', 'text', '而英国“脱欧”所带来的这些后续事件将进一步影响原油市场，并为原油投资者创造机会。', null, null, '2016-06-27 13:25:40', '2016-06-27 13:25:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1996', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5770ba3f0baa5.png', null, null, '2016-06-27 13:31:43', '2016-06-27 13:31:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1997', '8', 'text', '原油早盘以跳空走低开盘，目前，价格出现反弹已补回了跳空的缺口。', null, null, '2016-06-27 13:34:52', '2016-06-27 13:34:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1998', '8', 'text', '目前来看，现在是处于一个空头回补的行情，入市机会或可以关注上方短期压力314-315区域，一旦价格出现受阻打压，可顺势轻仓做空单操作。', null, null, '2016-06-27 13:44:37', '2016-06-27 13:44:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('1999', '8', 'text', '周一人民币中间价大幅下调，跌破6.63水平至近六年来新低，离岸人民币和在岸人民币汇率均下挫。', null, null, '2016-06-27 13:49:07', '2016-06-27 13:49:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2000', '8', 'text', '英国“脱欧”，人们币也是受到了一定程度的冲击。', null, null, '2016-06-27 13:52:34', '2016-06-27 13:52:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2001', '8', 'text', '【苏格兰人不赞成二次独立公投】6月25日进行的一项民调显示，44.7%的人认为苏格兰不应举行第二次独立公投，41.9%的人则希望进行；不过民调还显示若立即举行公投，苏格兰人将支持退出英国；英国公投决定脱欧后，支持留欧的苏格兰举行第二次独立公投可能性进一步提高。', null, null, '2016-06-27 13:53:07', '2016-06-27 13:53:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2002', '8', 'text', '迄今为止已有超过355万人签名，但媒体认为“二次公投”可能性为零，且英国议会表示“二次公投”请愿存在造假。周一英镑延续跌势再跌2%。', null, null, '2016-06-27 14:00:52', '2016-06-27 14:00:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2003', '8', 'text', ' 高盛26日发布的研报指出，未来十八个月由于变数的增加和贸易的恶化，英国国内生产总值将遭受2.75个百分点的损失。其将英国今明两年GDP增速预期分别从2%和1.8%下调至1.5%和0.2%，并预计英国央行将把利率调降25个基点至0.25%。', null, null, '2016-06-27 14:06:35', '2016-06-27 14:06:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2004', '10000102', 'text', '突破314-315重压力，会不会就要向上走了，现在做多来得及吗？', null, null, '2016-06-27 14:07:52', '2016-06-27 14:07:52', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2005', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5770c3bf067c6.png', null, null, '2016-06-27 14:12:15', '2016-06-27 14:12:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2006', '8', 'text', '目前，反弹力度较为强劲，但是鉴于依然处空头趋势，稳健操作，暂不考虑多单，耐心等待机会。', null, null, '2016-06-27 14:14:48', '2016-06-27 14:14:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2007', '10000102', 'text', '好的，老师', null, null, '2016-06-27 14:15:18', '2016-06-27 14:15:18', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2008', '8', 'text', '现在多单，位置稍高，建议耐心等待下次机会。', '10000102', '突破314-315重压力，会不会就要向上走了，现在做多来得及吗？', '2016-06-27 14:16:50', '2016-06-27 14:16:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2009', '8', 'text', '对的，不要想去抓住每个波段，这种心态是很危险的。#9', '10000102', '好的，老师', '2016-06-27 14:19:52', '2016-06-27 14:19:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2010', '8', 'text', '尼日利亚政府：两天前产油量为180-190万桶/日，预期7月底能达到220万桶/日。', null, null, '2016-06-27 14:22:22', '2016-06-27 14:22:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2011', '8', 'text', '尼日利亚供应中断已经在逐渐恢复。', null, null, '2016-06-27 14:23:43', '2016-06-27 14:23:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2012', '10000102', 'text', '恢复生产那不就是利多？', null, null, '2016-06-27 14:31:15', '2016-06-27 14:31:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2013', '8', 'text', '恢复生产，供应量就增加了，就供需基本面来说是利空原油的。', '10000102', '恢复生产那不就是利多？', '2016-06-27 14:35:01', '2016-06-27 14:35:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2014', '8', 'text', '大神索罗斯在这次的“脱欧”事件中又大赚了一笔。#20', null, null, '2016-06-27 14:40:07', '2016-06-27 14:40:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2015', '8', 'text', '而索罗斯发言人说索罗斯在英国脱欧前曾做多英镑，没有针对英镑进行投机。索罗斯的盈利从其他投资而来。', null, null, '2016-06-27 14:42:24', '2016-06-27 14:42:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2016', '8', 'text', '英国财政大臣奥斯本发表讲话：目前英国拥有世界上最强大的发达经济体。今年预算赤字预期将低于3%。', null, null, '2016-06-27 14:49:08', '2016-06-27 14:49:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2017', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5770cf66da365.png', null, null, '2016-06-27 15:01:58', '2016-06-27 15:01:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2018', '8', 'text', '欧洲那边这时候差不多也开盘了，目前，价格依然处于反弹阶段，若欧盘持续上冲，上方关注压力319-320区域。', null, null, '2016-06-27 15:06:46', '2016-06-27 15:06:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2019', '8', 'text', '股票刚刚收盘了，下面咱们也简单讲一下。', null, null, '2016-06-27 15:11:25', '2016-06-27 15:11:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2020', '8', 'text', ' 沪指早盘低开，但随后在煤炭、钢铁板块的带领下一路走高，短暂震荡后强势上扬，午后，计算机板块全面爆发，沪指继续走强，逼近2900点整数关口，深成指涨逾2%，创业板涨逾3%，两市成交逾5700亿元，与前一交易日大致持平。', null, null, '2016-06-27 15:12:43', '2016-06-27 15:12:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2021', '8', 'text', '截止收盘，沪指报2895.52点，涨幅1.44%，成交1899亿元；深成指报10377.57点，涨幅2.27%，成交3932亿元；创业板指报2191.81点，涨幅3.03%，成交1386亿元。', null, null, '2016-06-27 15:15:01', '2016-06-27 15:15:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2022', '8', 'text', '板块方面：计算机板块飙涨，个股迎来涨停潮。', null, null, '2016-06-27 15:19:27', '2016-06-27 15:19:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2023', '8', 'text', '国防军工板块强势。', null, null, '2016-06-27 15:19:35', '2016-06-27 15:19:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2024', '8', 'text', '次新股板块大涨，17只个股涨停。', null, null, '2016-06-27 15:20:25', '2016-06-27 15:20:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2025', '8', 'text', '节能照明板块活跃，6股涨停。', null, null, '2016-06-27 15:20:53', '2016-06-27 15:20:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2026', '8', 'text', '中国主要股指短期内跌幅或在中等个位数，之后将反弹或企稳。', null, null, '2016-06-27 15:21:32', '2016-06-27 15:21:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2027', '8', 'text', '看好信息技术、可选消费品及医疗保健，回避原材料及能源等周期股。', null, null, '2016-06-27 15:22:33', '2016-06-27 15:22:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2028', '8', 'text', '英国脱欧对A股主要是心理层面影响，过去两个多月国内外多个利空因素逐个落地，市场通过下跌震荡逐渐消化。', null, null, '2016-06-27 15:23:58', '2016-06-27 15:23:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2029', '8', 'text', '潜在利多如创新和改革不断推进，当下投资者情绪略偏谨慎，市场多空对比的天平更利于多方。', null, null, '2016-06-27 15:24:27', '2016-06-27 15:24:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2030', '8', 'text', '存量博弈震荡市里业绩为王，中期继续以食品饮料、家电等稳定增长为底仓，智能制造等高成长根据市场波动适当择时，短期关注市场预期低、有潜在催化的券商、国企改革（军工）。', null, null, '2016-06-27 15:25:30', '2016-06-27 15:25:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2031', '8', 'text', '回到原油。', null, null, '2016-06-27 15:37:49', '2016-06-27 15:37:49', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2032', '8', 'text', '反弹稍受阻力压制。', null, null, '2016-06-27 15:38:04', '2016-06-27 15:38:04', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2033', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5770d88695bad.png', null, null, '2016-06-27 15:40:54', '2016-06-27 15:40:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2034', '8', 'text', '空头趋势，反弹受阻，顺势做空。', null, null, '2016-06-27 15:41:35', '2016-06-27 15:41:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2035', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5770d9e7135ea.png', null, null, '2016-06-27 15:46:47', '2016-06-27 15:46:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2036', '8', 'text', '晚间可能有欧洲央行举行的中央银行论坛，会议上美联储主席耶伦等人将发表讲话。', null, null, '2016-06-27 15:48:50', '2016-06-27 15:48:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2037', '8', 'text', '大家有时间可以关注一下，对金融市场都是有影响的。', null, null, '2016-06-27 15:49:21', '2016-06-27 15:49:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2038', '8', 'text', '瑞银预期英国央行今年8月和11月分别降息25个基点。', null, null, '2016-06-27 15:55:26', '2016-06-27 15:55:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2039', '8', 'text', '瑞银预期美联储将推迟加息至今年12月。', null, null, '2016-06-27 15:55:42', '2016-06-27 15:55:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2040', '8', 'text', '英国下院领袖葛雷林：预期英国会在2020选举前脱离欧盟。', null, null, '2016-06-27 16:06:20', '2016-06-27 16:06:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2041', '8', 'text', '也就是说，英国离真正脱离欧盟还有比较长的一段时间要走。', null, null, '2016-06-27 16:08:07', '2016-06-27 16:08:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2042', '8', 'text', '脱欧让英国富人很受伤，资产净值一夜蒸发55亿美元。', null, null, '2016-06-27 16:12:25', '2016-06-27 16:12:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2043', '8', 'text', '英国上周五投票支持脱欧之举震惊市场。随着欧洲市场迈向2008年以来最大降幅，英镑跳水至30多年最低水平', null, null, '2016-06-27 16:12:44', '2016-06-27 16:12:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2044', '8', 'text', '彭博指数显示，截至上周五，英国15大富豪的资产净值总共蒸发了55亿美元，资产净值百分比降幅最大的达到19%。', null, null, '2016-06-27 16:13:00', '2016-06-27 16:13:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2045', '8', 'text', '法巴银行：将三季度英镑/美元汇率预测从1.58美元调低至1.35美元。', null, null, '2016-06-27 16:13:51', '2016-06-27 16:13:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2046', '8', 'text', '法巴银行：预计英国央行到8月份会降息50个基点，到11月份会宣布量化宽松。', null, null, '2016-06-27 16:14:24', '2016-06-27 16:14:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2047', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5770e1282c04f.png', null, null, '2016-06-27 16:17:44', '2016-06-27 16:17:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2048', '8', 'text', '美元方面，从日线级别看，上方是有受到下跌趋势线压制的，如果不能上破成功，将重新回到下跌通道之中。', null, null, '2016-06-27 16:20:08', '2016-06-27 16:20:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2049', '8', 'text', '30年期英国国债延续涨势，收益率跌至纪录新低1.879%。', null, null, '2016-06-27 16:25:02', '2016-06-27 16:25:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2050', '8', 'text', '受脱欧事件影响，5月全球黄金ETP吸引41亿美元避险资金流入。', null, null, '2016-06-27 16:34:35', '2016-06-27 16:34:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2051', '8', 'text', '苏格兰皇家银行在伦敦停牌，此前股价跌14.2%。', null, null, '2016-06-27 16:34:50', '2016-06-27 16:34:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2052', '8', 'text', '10年期英国国债收益率有史以来首次跌破1%。', null, null, '2016-06-27 16:35:03', '2016-06-27 16:35:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2053', '8', 'text', '穆迪：随着英国脱欧影响显现，未来几个月黄金和汇市可能会有波动。', null, null, '2016-06-27 16:41:05', '2016-06-27 16:41:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2054', '8', 'text', '【马达加斯加国庆发生爆炸】据欧洲时报，马达加斯加首都纳塔塔利佛26庆祝国庆独立日时，市体育场一枚手榴弹爆炸导致2人死亡50人受伤，爆炸发生时正举办免费演唱会，庆祝该国推翻法国殖民统治获得独立。目前尚不清楚手榴弹的来源及爆炸的性质。', null, null, '2016-06-27 16:44:05', '2016-06-27 16:44:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2055', '8', 'text', '刚刚有跟单的朋友，现在有盈利了。', null, null, '2016-06-27 16:52:25', '2016-06-27 16:52:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2056', '8', 'text', '稳健的可以在建仓点位设置一个移动止损控制风险。', null, null, '2016-06-27 16:54:11', '2016-06-27 16:54:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2057', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5770ebaece21d.png', null, null, '2016-06-27 17:02:38', '2016-06-27 17:02:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2058', '8', 'text', '目前，短周期来看顶部较为明显。', null, null, '2016-06-27 17:03:55', '2016-06-27 17:03:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2059', '8', 'text', '离岸人民币兑美元继续下跌，日内跌逾400点，现报6.6692，刷新1月中来低位。', null, null, '2016-06-27 17:20:53', '2016-06-27 17:20:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2060', '8', 'text', '伊朗油长赞加内：伊朗将在今年夏季就新的石油和天然气合约进行第一阶段的招标。\n', null, null, '2016-06-27 17:22:22', '2016-06-27 17:22:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2061', '8', 'text', '伊朗油长赞加内：将对10-15个油田进行招标。', null, null, '2016-06-27 17:22:38', '2016-06-27 17:22:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2062', '8', 'text', '随着夏季旅游及驾驶季节的来临，不断上升的需求将使油价在未来几个月保持稳定。但到了秋季这种稳定将被打破。', null, null, '2016-06-27 17:32:43', '2016-06-27 17:32:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2063', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5770f420214ad.png', null, null, '2016-06-27 17:38:40', '2016-06-27 17:38:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2064', '8', 'text', '日内均线有拐头趋势，多头反攻无力。', null, null, '2016-06-27 17:39:42', '2016-06-27 17:39:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2065', '8', 'text', '炼油厂的开工率较低，低油价导致大量的汽油过剩。', null, null, '2016-06-27 17:42:23', '2016-06-27 17:42:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2066', '8', 'text', '即使宏观经济非常稳定，汽油的需求也可能下滑4%-5%。', null, null, '2016-06-27 17:42:49', '2016-06-27 17:42:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2067', '8', 'text', '到了秋季，原油市场竞争将愈发复杂化，在尼日利亚和库尔德斯坦等地区恢复局势稳定，原油产量上升后尤其如此。', null, null, '2016-06-27 17:48:56', '2016-06-27 17:48:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2068', '8', 'text', '英镑兑美元继上周五后再度跌破1.33关口，日内跌幅近3%。', null, null, '2016-06-27 17:50:47', '2016-06-27 17:50:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2069', '10000001', 'text', '大家下午好！#9\n', null, null, '2016-06-27 17:51:16', '2016-06-27 17:51:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2070', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5770f9574c814.png', null, null, '2016-06-27 18:00:55', '2016-06-27 18:00:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2071', '8', 'text', '4小时级别看，原油中期下跌趋势已有苗头。', null, null, '2016-06-27 18:03:04', '2016-06-27 18:03:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2072', '8', 'text', '好了，下面晚间的直播就交给吴老师了。#9', null, null, '2016-06-27 18:04:53', '2016-06-27 18:04:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2073', '10000001', 'text', '好，感谢我们的伍老师，辛苦了#9', null, null, '2016-06-27 18:05:52', '2016-06-27 18:05:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2074', '10000001', 'text', '好，接下来，我们一起去看看目前的油价的走势', null, null, '2016-06-27 18:09:22', '2016-06-27 18:09:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2075', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5770fb5fc6ab8.png', null, null, '2016-06-27 18:09:35', '2016-06-27 18:09:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2076', '10000001', 'text', '从上图看出，今日下午油价回调到压力位315-316附近受阻后，小幅下跌', null, null, '2016-06-27 18:11:56', '2016-06-27 18:11:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2077', '10000001', 'text', '在上周五英国公投退欧后，今日欧洲股市继续下跌。而且10年期英国国债收益率有史以来首次跌破1%', null, null, '2016-06-27 18:20:45', '2016-06-27 18:20:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2078', '10000001', 'text', '所以目前市场的避险情绪还是非常浓烈的', null, null, '2016-06-27 18:21:18', '2016-06-27 18:21:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2079', '10000006', 'text', '#9', null, null, '2016-06-27 18:21:56', '2016-06-27 18:21:56', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2080', '10000001', 'text', '所以油价将会继续受到承压的', null, null, '2016-06-27 18:23:15', '2016-06-27 18:23:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2081', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5770ff315048d.png', null, null, '2016-06-27 18:25:53', '2016-06-27 18:25:53', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2082', '10000001', 'text', '从上图看出，目前油价短期支撑位在316-315附近的', null, null, '2016-06-27 18:26:33', '2016-06-27 18:26:33', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2083', '5', 'text', '老师，今晚怎么走？', null, null, '2016-06-27 18:30:01', '2016-06-27 18:30:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2084', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5771006111555.png', null, null, '2016-06-27 18:30:57', '2016-06-27 18:30:57', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2085', '10000001', 'text', '从上图看出，目前短期的支撑位在305-306附近的', null, null, '2016-06-27 18:31:20', '2016-06-27 18:31:20', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2086', '10000001', 'text', '从上图看出，目前短期的支撑位在305-306附近的', null, null, '2016-06-27 18:31:42', '2016-06-27 18:31:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2087', '10000001', 'text', '目前油价处于空头形势，预测今晚油价将会震荡下跌至306支撑位附近的', '5', '老师，今晚怎么走？', '2016-06-27 18:33:19', '2016-06-27 18:33:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2088', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/577105222d3e8.png', null, null, '2016-06-27 18:51:14', '2016-06-27 18:51:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2089', '10000001', 'text', '预测今晚油价走势可能是下跌至310附近，震荡回调后，再下跌到306附近', null, null, '2016-06-27 18:53:25', '2016-06-27 18:53:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2090', '10000001', 'text', '欧洲央行：取消了计划中中央银行论坛的美联储主席耶伦讲话。', null, null, '2016-06-27 18:57:50', '2016-06-27 18:57:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2091', '10000001', 'text', '看来欧洲央行也很担心耶伦的讲话会引起市场的大幅波动！', null, null, '2016-06-27 19:03:41', '2016-06-27 19:03:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2092', '10000001', 'text', '因为耶伦曾表示英国离开欧盟将对美国经济前景产生更大的负面影响', null, null, '2016-06-27 19:04:04', '2016-06-27 19:04:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2093', '10000001', 'text', '今晚数据方面关注：', null, null, '2016-06-27 19:23:13', '2016-06-27 19:23:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2094', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/57710ca820548.png', null, null, '2016-06-27 19:23:20', '2016-06-27 19:23:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2095', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/57710cabb6dbd.png', null, null, '2016-06-27 19:23:23', '2016-06-27 19:23:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2096', '10000001', 'text', '21:45 美国6月Markit服务业PMI初值。22:30 美国6月达拉斯联储商业活动指数', null, null, '2016-06-27 19:23:37', '2016-06-27 19:23:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2097', '10000001', 'text', '如果公布的数据>预期=利好美元，打压油价', null, null, '2016-06-27 19:24:13', '2016-06-27 19:24:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2098', '10000001', 'text', '今晚重点关注：欧洲央行行长和英国央行行长等人的讲话，耶伦的讲话已被取消', null, null, '2016-06-27 19:32:08', '2016-06-27 19:32:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2099', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/57710ec001ee6.png', null, null, '2016-06-27 19:32:16', '2016-06-27 19:32:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2100', '10000001', 'text', '上周五英国退欧公投脱欧后，今晚这些人的讲话将会影响欧元和英镑的未来走势，从而影响油价的走势', null, null, '2016-06-27 19:33:45', '2016-06-27 19:33:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2101', '10000001', 'text', '所以今晚油价波幅可能蛮大的喔，大家赚钱的时机又到了！哈哈#14', null, null, '2016-06-27 19:37:55', '2016-06-27 19:37:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2102', '10000194', 'text', '#14#14#14', null, null, '2016-06-27 19:40:55', '2016-06-27 19:40:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2103', '10000006', 'text', '今晚还有机会吗', null, null, '2016-06-27 19:57:33', '2016-06-27 19:57:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2104', '10000006', 'text', '老是', null, null, '2016-06-27 19:57:36', '2016-06-27 19:57:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2105', '10000006', 'text', '老师#7', null, null, '2016-06-27 19:57:44', '2016-06-27 19:57:44', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2106', '10000001', 'text', '英国首相发言人表示不太会举行第二轮公投，未来几周将继续保持与欧洲伙伴国的联系，看来希望改变英国脱欧的英国人有所失望了', null, null, '2016-06-27 20:01:30', '2016-06-27 20:01:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2107', '10000001', 'text', '有的，伊朗增产，尼日利亚产量恢复，市场避险情绪浓烈，预测今晚将下跌至306支撑位附近', '10000006', '今晚还有机会吗', '2016-06-27 20:05:34', '2016-06-27 20:05:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2108', '10000001', 'text', '受到英国首相发言人讲话的影响，市场避险情绪浓烈，油价继续下跌！', null, null, '2016-06-27 20:07:17', '2016-06-27 20:07:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2109', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5771171bbda29.png', null, null, '2016-06-27 20:07:55', '2016-06-27 20:07:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2110', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/577118620142b.png', null, null, '2016-06-27 20:13:22', '2016-06-27 20:13:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2111', '10000001', 'text', '今日我们伍老师的空单已经顺利止盈了！#14', null, null, '2016-06-27 20:13:52', '2016-06-27 20:13:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2112', '10000001', 'text', '英国首相发言人：卡梅伦设立部门就英国脱欧事宜上开展工作', null, null, '2016-06-27 20:23:05', '2016-06-27 20:23:05', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2113', '10000001', 'text', '所以英国第二次公投请愿非常渺茫啊', null, null, '2016-06-27 20:24:35', '2016-06-27 20:24:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2114', '10000001', 'text', '欧元区长期通胀预期市场指标继续下跌，刷新纪录低位的1.28%！', null, null, '2016-06-27 20:34:18', '2016-06-27 20:34:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2115', '10000001', 'text', '油价响声继续下跌至309附近', null, null, '2016-06-27 20:34:54', '2016-06-27 20:34:54', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2116', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/57711da2c5535.png', null, null, '2016-06-27 20:35:46', '2016-06-27 20:35:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2117', '10000001', 'text', '前英国央行行长金恩：英国脱欧后，英国央行可能将利率长期保持在更低的水平。', null, null, '2016-06-27 20:45:45', '2016-06-27 20:45:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2118', '10000001', 'text', '美国洲际交易所：截止至6月21日当周，布伦特原油投机者的净多头头寸减少9153手至362765手。', null, null, '2016-06-27 20:59:44', '2016-06-27 20:59:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2119', '10000001', 'text', '据路透报道：可能周六开始将有至少600名挪威石油工人进行罢工', null, null, '2016-06-27 21:06:32', '2016-06-27 21:06:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2120', '10000102', 'text', '老师，现价做多可以吗？', null, null, '2016-06-27 21:12:49', '2016-06-27 21:12:49', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2121', '10000001', 'text', '所以尼日利亚恢复产量将会受阻，为油价提供支撑', null, null, '2016-06-27 21:16:04', '2016-06-27 21:16:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2122', '10000001', 'text', '可以，带好止损止盈哦#9', '10000102', '老师，现价做多可以吗？', '2016-06-27 21:20:30', '2016-06-27 21:20:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2123', '10000001', 'text', '目标看312', '10000102', '老师，现价做多可以吗？', '2016-06-27 21:24:25', '2016-06-27 21:24:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2124', '10000017', 'text', '现在什么单啊', null, null, '2016-06-27 21:24:39', '2016-06-27 21:24:39', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2125', '10000001', 'text', '现在空单为主，可以等待反弹到311-312附近建仓空单', '10000017', '现在什么单啊', '2016-06-27 21:26:43', '2016-06-27 21:26:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2126', '10000001', 'text', '前美联储主席格林斯潘：预计苏格兰将脱离英国，北爱尔兰很可能也是如此', null, null, '2016-06-27 21:28:56', '2016-06-27 21:28:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2127', '10000001', 'text', '如果英格兰真的举行脱离公投，那么市场风险情绪将会继续增强，提振美元，打压油价', null, null, '2016-06-27 21:30:21', '2016-06-27 21:30:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2128', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/57712d762ef3b.png', null, null, '2016-06-27 21:43:18', '2016-06-27 21:43:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2129', '10000001', 'text', '目前油价下跌后，小幅反弹盘整', null, null, '2016-06-27 21:43:46', '2016-06-27 21:43:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2130', '10000001', 'text', '美国6月Markit服务业PMI初值，前值：51.3 预期：52 实际：51.3', null, null, '2016-06-27 21:46:28', '2016-06-27 21:46:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2131', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/57712e3e9b89f.png', null, null, '2016-06-27 21:46:38', '2016-06-27 21:46:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2132', '10000001', 'text', '数据利空美元，从而提振油价', null, null, '2016-06-27 21:47:22', '2016-06-27 21:47:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2133', '10000001', 'text', '原本今晚耶伦、卡尼将与欧央行行长德拉基碰面并发表讲话，但耶伦行程被取消。此番变动有可能暗示央行将采取行动干预市场，稳定欧元和英镑的波动！', null, null, '2016-06-27 21:52:31', '2016-06-27 21:52:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2134', '10000001', 'text', 'Markit首席经济学家威廉姆斯：PMI数据暗示美国二季度潜在经济增速或仅为1%', null, null, '2016-06-27 21:54:57', '2016-06-27 21:54:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2135', '10000001', 'text', '所以美元未来的走势有可能将会减弱', null, null, '2016-06-27 21:55:37', '2016-06-27 21:55:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2136', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/57713354919fd.png', null, null, '2016-06-27 22:08:20', '2016-06-27 22:08:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2137', '10000001', 'text', '空头形势强劲，油价反弹无力，追空', null, null, '2016-06-27 22:08:52', '2016-06-27 22:08:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2138', '10000194', 'text', '刚建的多单#13', null, null, '2016-06-27 22:12:37', '2016-06-27 22:12:37', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2139', '10000001', 'text', '可以关注下方316短期支撑，若没有跌破，油价还是有反弹空间的#9', '10000194', '刚建的多单#13', '2016-06-27 22:17:22', '2016-06-27 22:17:22', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2140', '10000001', 'text', '可以关注下方306短期支撑位，若没有跌破还是有反弹空间的#9', '10000194', '刚建的多单#13', '2016-06-27 22:18:20', '2016-06-27 22:18:20', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2141', '10000001', 'text', '可以关注下方306短期支撑位，若没有跌破还是有反弹空间的#9', '10000194', '刚建的多单#13', '2016-06-27 22:18:46', '2016-06-27 22:18:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2142', '10000194', 'text', '弹弹弹，弹走306#5', null, null, '2016-06-27 22:19:13', '2016-06-27 22:19:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2143', '10000001', 'text', '北京时间22:30将公布：美国6月达拉斯联储商业活动指数', null, null, '2016-06-27 22:21:09', '2016-06-27 22:21:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2144', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/577136730c009.png', null, null, '2016-06-27 22:21:39', '2016-06-27 22:21:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2145', '10000194', 'text', '#2#2#2', null, null, '2016-06-27 22:24:47', '2016-06-27 22:24:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2146', '7', 'text', '#20', null, null, '2016-06-27 22:25:26', '2016-06-27 22:25:26', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2147', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/577138ac34166.png', null, null, '2016-06-27 22:31:08', '2016-06-27 22:31:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2148', '10000001', 'text', '数据利空美元，从而提振油价', null, null, '2016-06-27 22:31:52', '2016-06-27 22:31:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2149', '10000194', 'text', '#14#14#14#9', null, null, '2016-06-27 22:34:57', '2016-06-27 22:34:57', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2150', '10000001', 'text', '英国首相卡梅伦正在发表讲话，将可能引发英镑和油价的波动', null, null, '2016-06-27 22:38:38', '2016-06-27 22:38:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2151', '10000001', 'text', '英国首相卡梅伦：本人和内阁成员都很清楚，必须接受脱欧公投结果', null, null, '2016-06-27 22:41:06', '2016-06-27 22:41:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2152', '10000194', 'text', '老师你说话就让我感觉是坐过山车#7', null, null, '2016-06-27 22:43:39', '2016-06-27 22:43:39', '-1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2153', '10000001', 'text', '目前英国退欧后对市场的影响还没消退，所以这些重要领导人讲话将会盖过数据对市场的影响的#9', '10000194', '老师你说话就让我感觉是坐过山车#7', '2016-06-27 22:48:30', '2016-06-27 22:48:30', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2154', '10000194', 'text', '#2#2#2嗯', null, null, '2016-06-27 22:51:28', '2016-06-27 22:51:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2155', '10000001', 'text', '英国首相卡梅伦：不论以后英国与欧盟的关系是何种性质，都希望能够继续保持现有的在安全方面的安排。', null, null, '2016-06-27 22:53:14', '2016-06-27 22:53:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2156', '10000001', 'text', '暗示英国脱欧后可能危及国家安全', null, null, '2016-06-27 22:59:16', '2016-06-27 22:59:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2157', '10000194', 'text', '这是肯定的了#2', null, null, '2016-06-27 23:00:00', '2016-06-27 23:00:00', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2158', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160627/5771402736c96.png', null, null, '2016-06-27 23:03:03', '2016-06-27 23:03:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2159', '10000194', 'text', '#11#11#11都拉不起来', null, null, '2016-06-27 23:03:05', '2016-06-27 23:03:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2160', '10000001', 'text', '目前油价跌至近两周低位', null, null, '2016-06-27 23:03:24', '2016-06-27 23:03:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2161', '10000170', 'text', '你是冒天下之大不韪哦', null, null, '2016-06-27 23:04:32', '2016-06-27 23:04:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2162', '10000194', 'text', '#17#17#17', '10000170', '你是冒天下之大不韪哦', '2016-06-27 23:06:08', '2016-06-27 23:06:08', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2163', '10000001', 'text', '英国首相卡梅伦：如何以最佳方式进入单一市场是下届政府将要做出的最重要决定之一', null, null, '2016-06-27 23:09:30', '2016-06-27 23:09:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2164', '10000001', 'text', '暗示英国央行将进行降息', null, null, '2016-06-27 23:10:01', '2016-06-27 23:10:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2165', '10000194', 'text', '降息是涨么#8', null, null, '2016-06-27 23:13:27', '2016-06-27 23:13:27', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2166', '10000170', 'text', '你懂得', '10000194', '降息是涨么#8', '2016-06-27 23:14:46', '2016-06-27 23:14:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2167', '10000194', 'text', '#10不懂', '10000170', '你懂得', '2016-06-27 23:15:50', '2016-06-27 23:15:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2168', '10000170', 'text', '886', null, null, '2016-06-27 23:16:03', '2016-06-27 23:16:03', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2169', '10000194', 'text', '#21#21你睡吧我守夜#4', '10000170', '886', '2016-06-27 23:19:32', '2016-06-27 23:19:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2170', '10000001', 'text', '从长远角度看，降息有利于推动通货膨胀提高GDP增速，稳定英国退欧后面临的经济下行压力。从而降低英国退欧后引发市场的波动，对油价下跌提供一定的支撑#9', '10000194', '降息是涨么#8', '2016-06-27 23:20:41', '2016-06-27 23:20:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2171', '10000194', 'text', '#2明白了', null, null, '2016-06-27 23:25:00', '2016-06-27 23:25:00', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2172', '10000001', 'text', '英国首相卡梅伦：内阁同意设立一个新的工作组，专注于与欧盟进行谈判', null, null, '2016-06-27 23:25:10', '2016-06-27 23:25:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2173', '10000194', 'text', '老师可怕的306来了#10', null, null, '2016-06-27 23:25:19', '2016-06-27 23:25:19', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2174', '10000001', 'text', '暗示英国将很快完成脱离欧盟的谈判', null, null, '2016-06-27 23:26:01', '2016-06-27 23:26:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2175', '10000001', 'text', '跌破306，建议多单可以先锁仓一半', '10000194', '老师可怕的306来了#10', '2016-06-27 23:27:03', '2016-06-27 23:27:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2176', '10000194', 'text', '#16嗯', null, null, '2016-06-27 23:27:52', '2016-06-27 23:27:52', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2177', '10000001', 'text', '据外媒报道：加拿大热电联产电力发电量已经接近5月山火发生前的水平', null, null, '2016-06-27 23:45:08', '2016-06-27 23:45:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2178', '10000001', 'text', '或暗示加拿大原油产出得到恢复', null, null, '2016-06-27 23:46:35', '2016-06-27 23:46:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2179', '10000001', 'text', '加拿大皇家银行：预计美联储下一次加息最早在2017年年中，此前预期为今年12月', null, null, '2016-06-28 00:09:52', '2016-06-28 00:09:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2180', '10000001', 'text', '英国首相卡梅伦：将尽一切努力说服法国当局维持英国与法国的边境管制不变', null, null, '2016-06-28 00:21:30', '2016-06-28 00:21:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2181', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/5771534b7d08a.png', null, null, '2016-06-28 00:24:43', '2016-06-28 00:24:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2182', '10000001', 'text', '目前油价继续震荡下跌', null, null, '2016-06-28 00:25:03', '2016-06-28 00:25:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2183', '10000001', 'text', '今日油价开盘后，目前跌幅已经超过2%', null, null, '2016-06-28 00:26:28', '2016-06-28 00:26:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2184', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57715807a9c39.png', null, null, '2016-06-28 00:44:55', '2016-06-28 00:44:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2185', '10000001', 'text', '今晚的空单顺利止盈平仓了#14', null, null, '2016-06-28 00:45:38', '2016-06-28 00:45:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2186', '10000001', 'text', '美国国务卿克里：美国希望英国在北约、G7和联合国安理会继续起到强有力的领导作用', null, null, '2016-06-28 01:19:28', '2016-06-28 01:19:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2187', '10000001', 'text', '北京时间01:30 欧洲央行行长将在欧洲央行的中央银行论坛就“国际货币和金融体系的未来”发表讲话', null, null, '2016-06-28 01:22:34', '2016-06-28 01:22:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2188', '10000001', 'text', '将有可能继续引起油价的波动！', null, null, '2016-06-28 01:23:27', '2016-06-28 01:23:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2189', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57716173c5a23.png', null, null, '2016-06-28 01:25:07', '2016-06-28 01:25:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2190', '10000001', 'text', '目前油价继续震荡下跌', null, null, '2016-06-28 01:25:31', '2016-06-28 01:25:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2191', '10000001', 'text', '下方支撑位310', null, null, '2016-06-28 01:31:56', '2016-06-28 01:31:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2192', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/5771647a7e165.png', null, null, '2016-06-28 01:38:02', '2016-06-28 01:38:02', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2193', '10000001', 'text', '预测明天油价的走势将反弹至307附近后震荡下跌', null, null, '2016-06-28 01:38:55', '2016-06-28 01:38:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2194', '10000001', 'text', '好了，今晚的直播就到这里了，感谢大家的收看#9', null, null, '2016-06-28 01:39:25', '2016-06-28 01:39:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2195', '10000001', 'text', '大家晚安#9', null, null, '2016-06-28 01:39:42', '2016-06-28 01:39:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2196', '8', 'text', '朋友们早上好！#4', null, null, '2016-06-28 09:25:46', '2016-06-28 09:25:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2197', '8', 'text', '清新的空气，快乐的气息，透过空气射入你的灵魂里，将阳光呼吸，将幸福抱起，泡一杯甜蜜的咖啡，品尝幸福的意义，接受祝福的信息，祝你早安温馨无比!', null, null, '2016-06-28 09:35:53', '2016-06-28 09:35:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2198', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160628/5771d4b77a810.jpeg', null, null, '2016-06-28 09:36:55', '2016-06-28 09:36:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2199', '8', 'text', '昨天的单子顺利止盈，大家都跟紧了吗？#4', null, null, '2016-06-28 09:38:01', '2016-06-28 09:38:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2200', '8', 'text', '虽然脱欧已经成为定局，但是依然受到市场强烈关注。', null, null, '2016-06-28 09:44:02', '2016-06-28 09:44:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2201', '8', 'text', '一个呼吁英国议会推翻上周脱离欧盟(EU)决定的请愿活动，迄今已征得逾375万个签名，而且签名人数还在增加。', null, null, '2016-06-28 09:44:36', '2016-06-28 09:44:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2202', '8', 'text', '脱欧派将发现，解除英国与欧盟关系的过程将是代价巨大而极具破坏力的。比起鲍里斯•约翰逊和迈克尔•戈夫等脱欧派人士在虚张声势的声明中设想的情形，上述过程的难度不论从政治上、法律上还是宪法上来说都要大得多。', null, null, '2016-06-28 09:46:04', '2016-06-28 09:46:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2203', '8', 'text', '昨天看到一个新闻，居然有人说自己投出脱欧票是因为他觉得英国不可能脱离欧盟，所以只是为了好玩而选着了投脱欧一票。', null, null, '2016-06-28 09:47:26', '2016-06-28 09:47:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2204', '8', 'text', '首先，还是回顾一下昨天的行情。', null, null, '2016-06-28 09:54:51', '2016-06-28 09:54:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2205', '8', 'text', '原油指数昨天收长上下影线十字星线。', null, null, '2016-06-28 09:56:44', '2016-06-28 09:56:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2206', '8', 'text', '最低去到303.5，下跌了7.0，跌幅达2.25%。', null, null, '2016-06-28 09:58:22', '2016-06-28 09:58:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2207', '8', 'text', '自上周四收盘以来，美油累积下跌近8%，录得2月初以来最大两日跌幅，并创6月16日以来收盘新低。', null, null, '2016-06-28 09:59:47', '2016-06-28 09:59:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2208', '8', 'text', '因英国公投决定退欧后，投资者逃向美元、美国公债和黄金等资产避险，从而导致全球风险资产重挫。\n', null, null, '2016-06-28 10:07:55', '2016-06-28 10:07:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2209', '8', 'text', '从技术图表上看，短期内可能有一些多头需要结清头寸，随着全球波动性继续攀升，多头结清头寸的倾向会越发强烈。', null, null, '2016-06-28 10:15:55', '2016-06-28 10:15:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2210', '8', 'text', '尽管多家银行认为油市基本面仍然强劲，但有迹象显示，市场对基本面的看法可能正在发生改变。', null, null, '2016-06-28 10:17:13', '2016-06-28 10:17:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2211', '10000102', 'text', '老师，我多单还拿着', null, null, '2016-06-28 10:20:05', '2016-06-28 10:20:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2212', '8', 'text', '下面来看看股票大盘吧。', null, null, '2016-06-28 10:28:17', '2016-06-28 10:28:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2213', '8', 'text', '什么位置的多单呢？目前价格处触底反弹走势，多单可以先拿着没事。', '10000102', '老师，我多单还拿着', '2016-06-28 10:30:01', '2016-06-28 10:30:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2214', '8', 'text', '沪指早盘小幅低开后，一路走高，一度逼近翻红，随后冲高回落。', null, null, '2016-06-28 10:31:44', '2016-06-28 10:31:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2215', '10000102', 'text', '309-311附近的多单，分批建仓的', null, null, '2016-06-28 10:32:16', '2016-06-28 10:32:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2216', '8', 'text', '盘面上，基因测序板块领涨，生物制药、交运设备、网络安全等板块涨幅居前。', null, null, '2016-06-28 10:33:10', '2016-06-28 10:33:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2217', '8', 'text', '点位具目前很近，可以先拿者，等反弹到高位再出手，下方支撑比较强，短时间下破的可能性很小。', '10000102', '309-311附近的多单，分批建仓的', '2016-06-28 10:36:05', '2016-06-28 10:36:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2218', '8', 'text', '消息面上，中国央行发布2016金融稳定报告称，2016年继续实施稳健的货币政策、完善QFII、RQFII制度，逐步放宽投资额度，推动A股纳入国际指数。', null, null, '2016-06-28 10:39:01', '2016-06-28 10:39:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2219', '8', 'text', '首例人体“基因剪刀”临床试验获绿灯 概念股站上风口', null, null, '2016-06-28 10:43:36', '2016-06-28 10:43:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2220', '8', 'text', '百度无人车计划三年商用 辅助驾驶迎千亿市场', null, null, '2016-06-28 10:44:01', '2016-06-28 10:44:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2221', '8', 'text', '物联网成巨头必争之地 万亿市场面临爆发', null, null, '2016-06-28 10:45:10', '2016-06-28 10:45:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2222', '8', 'text', '次新股涨停潮再现，上海沪工等12股涨停。', null, null, '2016-06-28 10:51:55', '2016-06-28 10:51:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2223', '8', 'text', '下面回到原油。', null, null, '2016-06-28 11:00:07', '2016-06-28 11:00:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2224', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160628/5771e932662ca.png', null, null, '2016-06-28 11:04:18', '2016-06-28 11:04:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2225', '8', 'text', '从盘面来看，目前依然是空头趋势，但是鉴于日线图300附近强力支撑，预计今天震荡走势的可能性比较大，关注上方压力314-315，下方支撑303-304，日内操作建议高抛低吸思路即可。', null, null, '2016-06-28 11:07:08', '2016-06-28 11:07:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2226', '10000102', 'text', '多单现在还能拿吗？', null, null, '2016-06-28 11:07:11', '2016-06-28 11:07:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2227', '8', 'text', '反弹受到1小时20均线压制，稳健操作的话，有盈利的单子建议先离场，后续再找机会入市。', '10000102', '多单现在还能拿吗？', '2016-06-28 11:10:18', '2016-06-28 11:10:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2228', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160628/5771ed3ac6614.png', null, null, '2016-06-28 11:21:30', '2016-06-28 11:21:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2229', '8', 'text', '分时图看，均线上升势头走缓，价格有震荡回落之势。', null, null, '2016-06-28 11:23:02', '2016-06-28 11:23:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2230', '8', 'text', '昨日英国财政大臣奥斯本两点就英国脱欧等相关问题发表讲话以及晚间欧洲央行举行中央银行论坛，会议演讲期间正面言论对英国脱欧恐慌情绪有一定平抚作用。其直接反映在早间原油一类的风险资产盘面，使得风险资产都有小幅反弹拉升。', null, null, '2016-06-28 11:29:51', '2016-06-28 11:29:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2231', '8', 'text', '当前市场关注点在英国脱欧的持续影响上面，高盛表示，即使在脱欧后英国经济增长下降2%，英国原油需求也仅会减少约1%或1.6万桶/日，这只占到全球原油需求总量的0.016%。', null, null, '2016-06-28 11:37:35', '2016-06-28 11:37:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2232', '8', 'text', '虽然英国原油需求在全球范围占比很小，但投资者担心英国脱欧将会引发欧盟一系列连锁反应，恐令油价遭受重创。', null, null, '2016-06-28 11:40:21', '2016-06-28 11:40:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2233', '8', 'text', '美元日内再度走高近1%，维持在上周五触及的三个月高位附近，令原油等以美元计价的大宗商品价格承压。', null, null, '2016-06-28 11:43:36', '2016-06-28 11:43:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2234', '8', 'text', '截止午盘，沪指报2894.50点，跌幅0.04%，成交1090亿元；深成指报10392.23点，涨幅0.14%，成交2534亿元；创业板指报2200.46点，涨幅0.39%，成交925.7亿元。', null, null, '2016-06-28 11:49:22', '2016-06-28 11:49:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2235', '8', 'text', '医疗股表现强势，网络安全再度涨幅居前，沪指在临近午盘再度发力上攻，逼近翻红。两市半日成交超3600亿元，量能有所放大。', null, null, '2016-06-28 11:49:56', '2016-06-28 11:49:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2236', '8', 'text', '李克强：资本市场要防止出现井喷式或断崖式变化', null, null, '2016-06-28 11:54:46', '2016-06-28 11:54:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2237', '8', 'text', '英国脱欧对全球资本市场短期影响较大，欧洲股市连续走低，市场避险情绪仍未消散，近期A股走势较为坚挺，但独善其身较难。大盘总体上攻动能不济，仍有回落可能。', null, null, '2016-06-28 11:58:45', '2016-06-28 11:58:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2238', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160628/5771f6c00c4a7.png', null, null, '2016-06-28 12:02:08', '2016-06-28 12:02:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2239', '8', 'text', '原油1小时图，MACD指标出现背离，快慢线金叉运行，短期或还有继续反弹需要，上方压力在黄金分割线38.2%处315附近。', null, null, '2016-06-28 12:05:32', '2016-06-28 12:05:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2240', '8', 'text', '好了，到了午餐时间了，大家美餐一顿，下午继续。#9', null, null, '2016-06-28 12:06:29', '2016-06-28 12:06:29', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2241', '10000170', 'text', '你是先知先觉哦#5，提前布局多单', '10000102', '309-311附近的多单，分批建仓的', '2016-06-28 12:22:42', '2016-06-28 12:22:42', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2242', '10000170', 'text', '老师我有通化东宝，1元盈利，可以继续持有么', null, null, '2016-06-28 12:29:15', '2016-06-28 12:29:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2243', '10000000', 'text', '大家中午好！', null, null, '2016-06-28 13:03:36', '2016-06-28 13:03:36', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2244', '10000000', 'text', '这个世界从不缺乏有才能的人，但缺少有精神的人。生活是没有路标的单程轮回，固守心的航向，需要非凡的勇气。以微笑驾驭人生之舟，轩湛带你收获一路精彩!#9', null, null, '2016-06-28 13:09:55', '2016-06-28 13:09:55', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2245', '10000000', 'text', '这股21上方压力较大，可以先获利', '10000170', '老师我有通化东宝，1元盈利，可以继续持有么', '2016-06-28 13:13:40', '2016-06-28 13:13:40', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2246', '10000000', 'text', '下面来到数据分析栏目，看看今天有什么重大数据值得我们投资者关注。', null, null, '2016-06-28 13:14:24', '2016-06-28 13:14:24', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2247', '10000000', 'text', '首先是今晚8点半的重头戏：美国GDP和核心通胀CPI数据', null, null, '2016-06-28 13:16:13', '2016-06-28 13:16:13', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2248', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57720828152d3.jpg', null, null, '2016-06-28 13:16:24', '2016-06-28 13:16:24', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2249', '8', 'text', 'GDP是指在一定时期内(一个季度或一年)，一个国家或地区的经济中所生产出的全部最终产品和劳务的价值，常被公认为衡量国家经济状况的最佳指标。它不但可反映一个国家的经济表现，更可以反映一国的国力与财富。', null, null, '2016-06-28 13:22:24', '2016-06-28 13:22:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2250', '10000000', 'text', '通胀就是我们平常非常关注的物价水平', null, null, '2016-06-28 13:24:21', '2016-06-28 13:24:21', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2251', '10000000', 'text', '那么很明显，这两个数据是任何一个国家都非常重视的数据，在数据公布的时候就会引起较大的波动', null, null, '2016-06-28 13:26:14', '2016-06-28 13:26:14', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2252', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57720e57abc48.jpg', null, null, '2016-06-28 13:42:47', '2016-06-28 13:42:47', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2253', '10000000', 'text', '大家可以看到，每次GDP和通胀数据公布的时候，都可以给大家带来至少一倍的收益空间，这是其他市场所没有的魅力！只要你有勇气，轩湛的精英分析团队将陪伴你在大数据之夜的捞金之旅！#4', null, null, '2016-06-28 13:46:07', '2016-06-28 13:46:07', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2254', '10000000', 'text', '除了GDP和CPI以外，今晚还有一道大餐准备给投资者朋友们，就是凌晨4点半的API原油库存数据', null, null, '2016-06-28 13:48:57', '2016-06-28 13:48:57', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2255', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57720fd226376.jpg', null, null, '2016-06-28 13:49:06', '2016-06-28 13:49:06', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2256', '10000000', 'text', '做过原油投资的朋友都知道，API总是令第二天的开盘出现大行情的。上周API数据意外大幅利多，导致行情出现近3%的上涨。', null, null, '2016-06-28 13:55:11', '2016-06-28 13:55:11', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2257', '10000013', 'text', '老师，现在可以做空吗？', null, null, '2016-06-28 13:59:32', '2016-06-28 13:59:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2258', '10000000', 'text', '所以，今晚大家要准备好自己的子弹，把握住月底这种每月一次的大波动大机会#4', null, null, '2016-06-28 14:00:27', '2016-06-28 14:00:27', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2259', '10000000', 'text', '反弹到314~315区域再空比较好', '10000013', '老师，现在可以做空吗？', '2016-06-28 14:02:11', '2016-06-28 14:02:11', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2260', '10000102', 'text', '', '10000170', '你是先知先觉哦#5，提前布局多单', '2016-06-28 14:03:01', '2016-06-28 14:03:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2261', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160628/5772147fb1d69.jpg', null, null, '2016-06-28 14:09:03', '2016-06-28 14:09:03', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2262', '10000000', 'text', '这是这几个月以来API的原油库存数据，大家可以看到，每隔一周数据就上涨，下一周就回落，如果按照这个规律，那么今晚的API很有可能是利空的', null, null, '2016-06-28 14:10:20', '2016-06-28 14:10:20', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2263', '10000000', 'text', '如果大家有兴趣，可以拿点仓位尝试一下今晚的数据是否符合这个规律', null, null, '2016-06-28 14:10:55', '2016-06-28 14:10:55', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2264', '10000102', 'text', '好像真的是这个规律#20', null, null, '2016-06-28 14:14:10', '2016-06-28 14:14:10', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2265', '10000000', 'text', '做交易，就得擅于发现市场的规律所在#4', null, null, '2016-06-28 14:16:33', '2016-06-28 14:16:33', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2266', '10000000', 'text', '好了，下面把时间交回给伍老师', null, null, '2016-06-28 14:25:22', '2016-06-28 14:25:22', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2267', '8', 'text', '好的，谢谢郑老师带来的数据解读。', null, null, '2016-06-28 14:26:20', '2016-06-28 14:26:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2268', '8', 'text', '下面我们回到原油盘面', null, null, '2016-06-28 14:27:14', '2016-06-28 14:27:14', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2269', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160628/577219ac16366.png', null, null, '2016-06-28 14:31:08', '2016-06-28 14:31:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2270', '8', 'text', '技术分析的魅力就在于此，尽最大可能找到市场在各个周期所呈现出的规律。#4', '10000102', '好像真的是这个规律#20', '2016-06-28 14:37:17', '2016-06-28 14:37:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2271', '8', 'text', '回到分析，30分钟级别，我们可以看到，315附近有一个短期压力平台，目前，反弹力量受上方压制已有所减缓，附图MACD红色动能柱缩量，KD指标高位死叉。', null, null, '2016-06-28 14:43:18', '2016-06-28 14:43:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2272', '8', 'text', '手中持有多单的朋友，注意关注上方压力位，若出现回落，就可以及时获利了结，同时寻找机会布局空单。', null, null, '2016-06-28 14:53:59', '2016-06-28 14:53:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2273', '8', 'text', '据今日俄罗斯：美国密西西比州的帕斯卡古拉天然气站发生爆炸。', null, null, '2016-06-28 14:54:56', '2016-06-28 14:54:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2274', '8', 'text', '虽因西欧最大产油国挪威和阿根廷的石油工人罢工威胁推高油价，但英国脱欧引发的风险资产抛售仍对油价构成压力。', null, null, '2016-06-28 15:04:24', '2016-06-28 15:04:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2275', '10000170', 'text', '老师华胜天成是物联网概念股，可以进不', '10000194', '#17#17#17', '2016-06-28 15:13:58', '2016-06-28 15:13:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2276', '8', 'text', '据路透报道，如果周五期限之前仍无法达成新的薪酬协议，挪威七大油气田的大约755名工人可能从本周六起开始罢工，预计将影响该北海最大产油国的产出。', null, null, '2016-06-28 15:17:51', '2016-06-28 15:17:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2277', '8', 'text', '国家调解员将在6月30日和7月1日组织最后一轮强制性会谈，如果会谈失败，罢工将在第二天上演。', null, null, '2016-06-28 15:18:03', '2016-06-28 15:18:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2278', '10000102', 'text', '老师，现在做空，可以吗？', null, null, '2016-06-28 15:22:15', '2016-06-28 15:22:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2279', '8', 'text', '资金呈现逐渐流入增强之势，且属近期热门，短期该股进入多头行情中，股价短线上涨概率较大。\n', '10000170', '老师华胜天成是物联网概念股，可以进不', '2016-06-28 15:22:34', '2016-06-28 15:22:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2280', '8', 'text', '可以，但是注意带好止盈止损。#9', '10000102', '老师，现在做空，可以吗？', '2016-06-28 15:26:01', '2016-06-28 15:26:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2281', '10000037', 'text', '股价？', null, null, '2016-06-28 15:27:00', '2016-06-28 15:27:00', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2282', '10000037', 'text', '#3期待今晚的数据', null, null, '2016-06-28 15:27:37', '2016-06-28 15:27:37', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2283', '8', 'text', '上面叫荷的朋友问到股票。', '10000037', '股价？', '2016-06-28 15:27:51', '2016-06-28 15:27:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2284', '8', 'text', '今晚有几个重量级数据，市场波动不会小，注意关注。#4', '10000037', '#3期待今晚的数据', '2016-06-28 15:29:21', '2016-06-28 15:29:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2285', '10000102', 'text', '今天到目前为止有操作建议吗？', null, null, '2016-06-28 15:34:31', '2016-06-28 15:34:31', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2286', '8', 'text', '早上的时候就说了，从盘面来看，目前依然是空头趋势，但是鉴于日线图300附近强力支撑，预计今天震荡走势的可能性比较大，关注上方压力314-315，下方支撑303-304，日内操作建议高抛低吸思路即可。', '10000102', '今天到目前为止有操作建议吗？', '2016-06-28 15:40:43', '2016-06-28 15:40:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2287', '8', 'text', '阿根廷大部分地区的石油工人周一也启动了48小时罢工行动。', null, null, '2016-06-28 15:42:01', '2016-06-28 15:42:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2288', '8', 'text', '油气工人工会的秘书长阿维拉表示，如果石油生产和服务企业周二结束之前不同意加薪至少30%，周三将继续进行罢工；如果周三还没有答复，那罢工将“无限期”进行。', null, null, '2016-06-28 15:42:39', '2016-06-28 15:42:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2289', '10000037', 'text', '#21#21今晚通宵赚钱看欧洲杯', null, null, '2016-06-28 15:48:08', '2016-06-28 15:48:08', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2290', '8', 'text', '哈哈，还是你懂得享受啊，娱乐赚钱两不误。#4', '10000037', '#21#21今晚通宵赚钱看欧洲杯', '2016-06-28 15:51:25', '2016-06-28 15:51:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2291', '8', 'text', '今天股市已经收盘，下面我们也一起来看看。', null, null, '2016-06-28 15:51:44', '2016-06-28 15:51:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2292', '8', 'text', '沪指早盘低开后，一度维持在低位震荡盘整，两次冲高，都未能有所突破', null, null, '2016-06-28 15:52:12', '2016-06-28 15:52:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2293', '8', 'text', '临近午盘，医药股全面爆发，国产软件强势，带动沪指小幅走高，逼近翻红。', null, null, '2016-06-28 15:52:26', '2016-06-28 15:52:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2294', '8', 'text', '午后开盘，沪指继续低位震荡，临近尾盘，券商股拉升，传媒股大涨，沪指持续拉升翻红，重新站上2900点，创业板涨逾1%。两市成交超6600亿元，量能较昨日放大。', null, null, '2016-06-28 15:52:53', '2016-06-28 15:52:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2295', '8', 'text', '收盘沪指报2912.56点，涨幅0.58%，成交2111亿元，深成指报10463.45点，涨幅0.83%，成交4539亿元，创业板指报2216.71点，涨幅1.14%，成交1582亿元。', null, null, '2016-06-28 15:57:59', '2016-06-28 15:57:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2296', '8', 'text', '英国退欧公投结束后，全球避险情绪余音未了，稳定成为主基调。市场仍受制于多因素影响，在外围市场连续走低下，作为和全球市场联系紧密的A股难以独善其身。', null, null, '2016-06-28 15:59:19', '2016-06-28 15:59:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2297', '8', 'text', '逢高减仓观望仍是主策略，具体操作上，逢急跌可低吸近期走低的电池股、中报业绩预期良好的标的以及深港通利好的稀缺性的白酒、医药等个股。', null, null, '2016-06-28 15:59:59', '2016-06-28 15:59:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2298', '8', 'text', '外围事件冲击相继落地，A股重新回到自己的运行节奏之中，投资者应当把关注点放在内部因素的微妙变化上。', null, null, '2016-06-28 16:02:00', '2016-06-28 16:02:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2299', '8', 'text', 'A股“七翻身”行情能否成行需看“三方脸色”。第一是成交量能否持续放大。第二是领涨龙头能否出现。第三是经济数据与政策能否相互验证。', null, null, '2016-06-28 16:03:24', '2016-06-28 16:03:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2300', '8', 'text', '下面我们看看美元指数。', null, null, '2016-06-28 16:17:01', '2016-06-28 16:17:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2301', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57723284af5cc.png', null, null, '2016-06-28 16:17:08', '2016-06-28 16:17:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2302', '8', 'text', '美元指数受退欧事件影响，上周出现暴涨，目前处短期回调走势之中。', null, null, '2016-06-28 16:18:48', '2016-06-28 16:18:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2303', '8', 'text', '从盘面我们可以看到，下方短期支撑位于黄金分割线23.6%的位置95.8。', null, null, '2016-06-28 16:20:49', '2016-06-28 16:20:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2304', '8', 'text', '若美指在支撑位止跌继续走上升的话，原油价格将继续承压。', null, null, '2016-06-28 16:23:17', '2016-06-28 16:23:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2305', '10000092', 'text', '老师，今天没有操作建议吗', null, null, '2016-06-28 16:42:27', '2016-06-28 16:42:27', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2306', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160628/5772390fd85d5.png', null, null, '2016-06-28 16:45:03', '2016-06-28 16:45:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2307', '8', 'text', '欧洲央行行长德拉基：货币政策无可避免地产生不稳定的溢出效应。\n', null, null, '2016-06-28 16:45:49', '2016-06-28 16:45:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2308', '8', 'text', '欧洲央行行长德拉基：各大央行不应该放弃其通胀目标。', null, null, '2016-06-28 16:46:04', '2016-06-28 16:46:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2309', '8', 'text', '有的，稍后会有操作建议给出。', '10000092', '老师，今天没有操作建议吗', '2016-06-28 16:46:54', '2016-06-28 16:46:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2310', '8', 'text', '欧洲央行行长德拉基：新兴市场放缓引发能源以及与大宗商品需求下降，进而压制全球通胀。', null, null, '2016-06-28 16:47:20', '2016-06-28 16:47:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2311', '8', 'text', '欧洲央行行长德拉基：几大央行的政策分歧可能对未来政策意图带来不确定性，从而加大汇率波动和风险外溢。', null, null, '2016-06-28 16:47:56', '2016-06-28 16:47:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2312', '8', 'text', '从盘面来看，德拉基的讲话并没有给原油市场带来提振。', null, null, '2016-06-28 16:49:20', '2016-06-28 16:49:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2313', '10000000', 'text', '行情反弹到压力位315区域，我们进场布局空单，止损止盈带好，个人建议仅供参考。', null, null, '2016-06-28 16:52:46', '2016-06-28 16:52:46', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2314', '10000000', 'text', '现在可以参考左侧的操作建议', '10000092', '老师，今天没有操作建议吗', '2016-06-28 16:54:13', '2016-06-28 16:54:13', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2315', '8', 'text', '郑老师已经给出操作建议，大家可以参考#9', null, null, '2016-06-28 17:00:17', '2016-06-28 17:00:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2316', '8', 'text', '伦敦市长萨迪克·汗讲话称在英国脱欧后，希望伦敦能获得更大自主权。', null, null, '2016-06-28 17:11:52', '2016-06-28 17:11:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2317', '8', 'text', '伦敦市场是之前脱欧派的主要力量。', null, null, '2016-06-28 17:12:49', '2016-06-28 17:12:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2318', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160628/5772404e5c058.png', null, null, '2016-06-28 17:15:58', '2016-06-28 17:15:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2319', '8', 'text', '1小时图已出现做空信号，控制好仓位，做空操作。注意设好止盈止损。#9', null, null, '2016-06-28 17:18:34', '2016-06-28 17:18:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2320', '8', 'text', '操作建议仅供参考，大家可根据自身个性特点，选择适合自己的操作风格。#17', null, null, '2016-06-28 17:21:43', '2016-06-28 17:21:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2321', '8', 'text', '很多投资者常常会感叹：天天看新闻，了解和把握宏观政策走势，了解不同现货的发展趋势;技术分析也学得不错，如K线、K线形态、趋势线、黄金分割线、技术指标等。', null, null, '2016-06-28 17:28:22', '2016-06-28 17:28:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2322', '8', 'text', '因为它是投资者生存的基础，但要想在现货市场成为赢家，还要具有正确的投资心态。', null, null, '2016-06-28 17:29:53', '2016-06-28 17:29:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2323', '8', 'text', '现货市场中的心态：心态就是人的心理状态，是指对事物发展的反应和理解表现出不同的思想状态和观点。一个人心平气和、不偏不倚地去看待一件事情，和带有浓厚的感情去看待一件事情，其结果有天壤之别，甚至得出的结论有可能完全相悖。', null, null, '2016-06-28 17:33:09', '2016-06-28 17:33:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2324', '8', 'text', '在市场中，心态的重要性，是被大多数成功交易者所认可的。对交易有非常重要的影响。下面我举两个例子', null, null, '2016-06-28 17:41:56', '2016-06-28 17:41:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2325', '8', 'text', '例如，当价格快速上涨时，明显已经出现了受阻征兆，但害怕价格继续上涨，少了利润，所以持仓不动，有的甚至为了获得暴利，在这里不减仓反而加仓，最终价格回落，一切变成过眼云烟。', null, null, '2016-06-28 17:48:48', '2016-06-28 17:48:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2326', '8', 'text', '又如，当价格已破位下跌，明明知道它可能会继续下跌，但投资者难以接受现实，幻想着反弹或反转，结果错过最佳卖出时机。', null, null, '2016-06-28 17:51:13', '2016-06-28 17:51:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2327', '10000001', 'text', '大家下午好！#9', null, null, '2016-06-28 17:52:40', '2016-06-28 17:52:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2328', '8', 'text', '总之，身处风云变幻的现货市场，投资者要想在保全自己的情况下获得利润，除了要具备最基本的常识和技术外，还要摆正自己的心态，做到游刃有余。', null, null, '2016-06-28 17:56:07', '2016-06-28 17:56:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2329', '8', 'text', '良好的心态：投资者要明白，只要你进入市场，就会受到价格波动的影响，从而被市场玩弄于鼓掌之间，要想避开市场的波动影响，就必须要与市场保持一定距离，从而可以更清楚地看待市场的变化，保持良好的心态，努力做到无论市场出现任何波动，都能气定神闲、镇定自若。', null, null, '2016-06-28 17:58:27', '2016-06-28 17:58:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2330', '8', 'text', '对于投资者来说，良好的心态就像一粒鲜花的种子，他在悄声无息中发芽、开花，终会有一天它会把扑鼻的芳香献给你。', null, null, '2016-06-28 18:00:22', '2016-06-28 18:00:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2331', '8', 'text', '心态今天先讲到这里，晚间的行间分析交给我们的吴老师了！#14', null, null, '2016-06-28 18:03:12', '2016-06-28 18:03:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2332', '8', 'text', '下面请出吴老师#9', null, null, '2016-06-28 18:03:51', '2016-06-28 18:03:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2333', '10000001', 'text', '好，谢谢伍老师，辛苦了#9', null, null, '2016-06-28 18:04:03', '2016-06-28 18:04:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2334', '10000001', 'text', '重要的不是你的判断是错还是对，而是在你正确的时候要最大限度地发挥出你的力量来!', null, null, '2016-06-28 18:05:24', '2016-06-28 18:05:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2335', '10000001', 'text', '我是轩湛投资观察室的吴勇坚，接下来我来为大家做直播，大家有什么疑问或者疑惑，可以在互动区跟我交流喔#9', null, null, '2016-06-28 18:05:48', '2016-06-28 18:05:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2336', '10000001', 'text', '好了，接下来我们一起看看油价目前的走势', null, null, '2016-06-28 18:07:19', '2016-06-28 18:07:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2337', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57724cdff2e65.png', null, null, '2016-06-28 18:09:36', '2016-06-28 18:09:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2338', '10000194', 'text', '老师可坐空单吗', null, null, '2016-06-28 18:09:36', '2016-06-28 18:09:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2339', '10000102', 'text', '怎么突然拉涨那么多？', null, null, '2016-06-28 18:11:01', '2016-06-28 18:11:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2340', '10000001', 'text', '从图上可以看出，目前油价已经反弹到压力位了', null, null, '2016-06-28 18:11:24', '2016-06-28 18:11:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2341', '10000001', 'text', '可以，目前空头格局还没打破，带好止损止盈#9', '10000194', '老师可坐空单吗', '2016-06-28 18:12:22', '2016-06-28 18:12:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2342', '10000194', 'text', '#9#9#9', null, null, '2016-06-28 18:12:43', '2016-06-28 18:12:43', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2343', '10000001', 'text', '尼日利亚产量下降，伊朗原油出口量减少，西欧最大产油国工人罢工，推动油价反弹', '10000102', '怎么突然拉涨那么多？', '2016-06-28 18:15:17', '2016-06-28 18:15:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2344', '10000102', 'text', '316压力，现在还会向上涨吗？', null, null, '2016-06-28 18:17:02', '2016-06-28 18:17:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2345', '10000001', 'text', '目前走到了压力位，会有短期的下降回调盘整', '10000102', '316压力，现在还会向上涨吗？', '2016-06-28 18:18:30', '2016-06-28 18:18:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2346', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57724fd878674.png', null, null, '2016-06-28 18:22:16', '2016-06-28 18:22:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2347', '10000001', 'text', '从上图看出，油价从今日开盘已经涨幅超过2.5%了', null, null, '2016-06-28 18:23:50', '2016-06-28 18:23:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2348', '10000001', 'text', '已经回吐了昨日的跌幅', null, null, '2016-06-28 18:24:33', '2016-06-28 18:24:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2349', '10000001', 'text', '今日油价反弹主要是因为亚洲和美国强劲的夏季原油需求及今年来的供应紧张对油价构成支撑', null, null, '2016-06-28 18:25:45', '2016-06-28 18:25:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2350', '10000001', 'text', '外挪威数个油气公司石油工人或罢工可能导致西欧最大的产油国减产，也利好油价', null, null, '2016-06-28 18:26:09', '2016-06-28 18:26:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2351', '10000001', 'text', '虽然英国公投为退出欧盟的结果近几个交易日引发全球股市和汇市大震荡，但对油价的拖累影响却有限', null, null, '2016-06-28 18:28:18', '2016-06-28 18:28:18', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2352', '10000135', 'text', '', '10000194', '老师可坐空单吗', '2016-06-28 18:29:10', '2016-06-28 18:29:10', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2353', '10000080', 'text', '#1', null, null, '2016-06-28 18:29:51', '2016-06-28 18:29:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2354', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/577252a064580.png', null, null, '2016-06-28 18:34:08', '2016-06-28 18:34:08', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2355', '10000194', 'text', '听老师的都是好孩子#6', '10000080', '#1', '2016-06-28 18:35:12', '2016-06-28 18:35:12', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2356', '10000187', 'text', '#14#14', null, null, '2016-06-28 18:36:05', '2016-06-28 18:36:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2357', '10000001', 'text', '目前油价，上方压力位：316 下方短期支撑：310', null, null, '2016-06-28 18:36:20', '2016-06-28 18:36:20', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2358', '10000001', 'text', ' 目前油价，上方压力位：316 下方短期支撑：310', null, null, '2016-06-28 18:36:37', '2016-06-28 18:36:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2359', '10000194', 'text', '#14#14#14', '10000187', '#14#14', '2016-06-28 18:37:04', '2016-06-28 18:37:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2360', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/5772551856e8e.png', null, null, '2016-06-28 18:44:40', '2016-06-28 18:44:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2361', '10000001', 'text', '日线级别，压力位：320 支撑位300', null, null, '2016-06-28 18:47:11', '2016-06-28 18:47:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2362', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57725730907e3.png', null, null, '2016-06-28 18:53:36', '2016-06-28 18:53:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2363', '10000001', 'text', '从上图看，技术面上，预测今晚油价的走势下跌至310附近后反弹', null, null, '2016-06-28 18:56:07', '2016-06-28 18:56:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2364', '10000102', 'text', '压力位317？', null, null, '2016-06-28 18:56:12', '2016-06-28 18:56:12', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2365', '10000001', 'text', '目前压力位在316-315区间，上方较强压力位320', '10000102', '压力位317？', '2016-06-28 18:58:07', '2016-06-28 18:58:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2366', '10000001', 'text', '挪威工会联合会：警告称可能有500名挪威国家石油公司工人罢工', null, null, '2016-06-28 19:08:06', '2016-06-28 19:08:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2367', '10000001', 'text', '如果雇主和工会不能在本周五的截止日期内达成新的薪资协议，挪威国家石油公司工人罢工才会罢工', null, null, '2016-06-28 19:10:26', '2016-06-28 19:10:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2368', '10000001', 'text', '所以罢工这个消息面引发油价的涨势及已经走完了', null, null, '2016-06-28 19:11:17', '2016-06-28 19:11:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2369', '10000001', 'text', '今晚关注的数据：', null, null, '2016-06-28 19:19:10', '2016-06-28 19:19:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2370', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57725d620e79d.png', null, null, '2016-06-28 19:20:02', '2016-06-28 19:20:02', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2371', '10000001', 'text', '北京时间20:30 美国第一季度实际GDP年化季率终值', null, null, '2016-06-28 19:20:42', '2016-06-28 19:20:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2372', '10000013', 'text', '老师今天会不会单边上涨阿/', null, null, '2016-06-28 19:23:48', '2016-06-28 19:23:48', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2373', '10000001', 'text', '单边上涨的可能能性很少，目前为止利多消息面的行情已经走完了', '10000013', '老师今天会不会单边上涨阿/', '2016-06-28 19:26:08', '2016-06-28 19:26:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2374', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57725fa421d44.png', null, null, '2016-06-28 19:29:40', '2016-06-28 19:29:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2375', '10000001', 'text', '从上图看出，美国去年加息后，美国实际GDP年化季率在下降', null, null, '2016-06-28 19:31:33', '2016-06-28 19:31:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2376', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/5772611f99772.png', null, null, '2016-06-28 19:35:59', '2016-06-28 19:35:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2377', '10000001', 'text', '20:30 美国第一季度核心PCE物价指数年化季率终值', null, null, '2016-06-28 19:36:24', '2016-06-28 19:36:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2378', '10000017', 'text', '130%', null, null, '2016-06-28 19:36:40', '2016-06-28 19:36:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2379', '10000001', 'text', '美联储加息最喜欢参考的核心个人消费支出(PCE)物价指数', null, null, '2016-06-28 19:37:48', '2016-06-28 19:37:48', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2380', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/5772631ac22ca.png', null, null, '2016-06-28 19:44:26', '2016-06-28 19:44:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2381', '10000001', 'text', '次日凌晨04:30 美国至6月24日当周API原油库存', null, null, '2016-06-28 19:47:23', '2016-06-28 19:47:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2382', '10000001', 'text', '这个数据每周都引起油价较大的波幅', null, null, '2016-06-28 19:47:44', '2016-06-28 19:47:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2383', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57726400bafca.png', null, null, '2016-06-28 19:48:16', '2016-06-28 19:48:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2384', '10000001', 'text', '能源情报监测机构最新数据显示，截至6月24日一周美国主要原油交割地库欣地区库存下滑逾130万桶', null, null, '2016-06-28 19:49:34', '2016-06-28 19:49:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2385', '10000001', 'text', '加上美国夏季原油需求强劲', null, null, '2016-06-28 19:50:19', '2016-06-28 19:50:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2386', '10000001', 'text', '预测今晚凌晨4:30 API库存将会继续录得一个负值', null, null, '2016-06-28 19:51:26', '2016-06-28 19:51:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2387', '10000037', 'text', '现在还会跌吗', null, null, '2016-06-28 19:52:55', '2016-06-28 19:52:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2388', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57726537363b3.png', null, null, '2016-06-28 19:53:27', '2016-06-28 19:53:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2389', '10000001', 'text', '目前油价在压力位盘整，未能冲破，所以目前还是看空的', '10000037', '现在还会跌吗', '2016-06-28 19:55:52', '2016-06-28 19:55:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2390', '10000170', 'text', '几家欢乐几家愁啊', null, null, '2016-06-28 20:06:43', '2016-06-28 20:06:43', '-1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2391', '10000001', 'text', '沙特阿美CEO：沙特阿美目前原油产出为1200万桶/日', null, null, '2016-06-28 20:07:15', '2016-06-28 20:07:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2392', '10000102', 'text', '这个消息利多还是利空', null, null, '2016-06-28 20:11:47', '2016-06-28 20:11:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2393', '10000001', 'text', '5月据沙特上报的日产量是1027万/桶', null, null, '2016-06-28 20:12:26', '2016-06-28 20:12:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2394', '10000102', 'text', '利空？', null, null, '2016-06-28 20:12:43', '2016-06-28 20:12:43', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2395', '10000001', 'text', '利空原油，供应增产', '10000102', '这个消息利多还是利空', '2016-06-28 20:13:21', '2016-06-28 20:13:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2396', '10000232', 'text', '在316时我想补仓的#7', null, null, '2016-06-28 20:17:04', '2016-06-28 20:17:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2397', '10000001', 'text', '北京时间20:30将公布：美国第一季度实际GDP年化季率终值、美国第一季度核心PCE物价指数年化季率终值', null, null, '2016-06-28 20:20:37', '2016-06-28 20:20:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2398', '10000232', 'text', '老师和各位同学，我是初学者，请多关照和指点#9', null, null, '2016-06-28 20:21:29', '2016-06-28 20:21:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2399', '10000232', 'text', '老师，公布出来后，我们该做什么？', null, null, '2016-06-28 20:23:20', '2016-06-28 20:23:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2400', '10000001', 'text', '观察数据利空还是利多，然后在压力位附近布局空单还是多单', '10000232', '老师，公布出来后，我们该做什么？', '2016-06-28 20:28:28', '2016-06-28 20:28:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2401', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57726df564e45.png', null, null, '2016-06-28 20:30:45', '2016-06-28 20:30:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2402', '10000001', 'text', '美国第一季度实际GDP年化季率终值数据 利空油价', null, null, '2016-06-28 20:31:41', '2016-06-28 20:31:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2403', '10000001', 'text', '美国第一季度核心PCE物价指数年化季率终值数据 利空美元 利多油价', null, null, '2016-06-28 20:32:18', '2016-06-28 20:32:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2404', '10000074', 'text', '空单还是多单', null, null, '2016-06-28 20:33:15', '2016-06-28 20:33:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2405', '10000187', 'text', '一个利多，一个利空，怎么走？', '10000194', '#14#14#14', '2016-06-28 20:33:39', '2016-06-28 20:33:39', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2406', '10000001', 'text', '这些数据影响油价不会一瞬间的，要有耐心，目前油价在压力位，可以轻仓布局空单的#9', '10000074', '空单还是多单', '2016-06-28 20:35:27', '2016-06-28 20:35:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2407', '10000001', 'text', '目前压力位未能冲破，个人看法倾向于布局空单', '10000187', '一个利多，一个利空，怎么走？', '2016-06-28 20:36:39', '2016-06-28 20:36:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2408', '10000194', 'text', '空#3加单', null, null, '2016-06-28 20:37:06', '2016-06-28 20:37:06', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2409', '10000187', 'text', '好的#21', null, null, '2016-06-28 20:38:26', '2016-06-28 20:38:26', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2410', '10000170', 'text', '老师，上方压力位是多少？', null, null, '2016-06-28 20:43:20', '2016-06-28 20:43:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2411', '10000102', 'text', '317会破吗？', null, null, '2016-06-28 20:44:00', '2016-06-28 20:44:00', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2412', '10000001', 'text', '上方日线级别压力位320', '10000170', '老师，上方压力位是多少？', '2016-06-28 20:44:42', '2016-06-28 20:44:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2413', '10000232', 'text', '老师，在什么时候平仓？', null, null, '2016-06-28 20:45:46', '2016-06-28 20:45:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2414', '10000001', 'text', '目前走势冲破320压力位可能性较低，317较弱，容易上破', '10000102', '317会破吗？', '2016-06-28 20:47:19', '2016-06-28 20:47:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2415', '10000001', 'text', '目前现价的空单止损221，止盈210', '10000232', '老师，在什么时候平仓？', '2016-06-28 20:48:00', '2016-06-28 20:48:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2416', '10000232', 'text', '止损321，止盈310吗？', null, null, '2016-06-28 20:49:36', '2016-06-28 20:49:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2417', '10000001', 'text', '是的，目前现价的空单', '10000232', '止损321，止盈310吗？', '2016-06-28 20:50:23', '2016-06-28 20:50:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2418', '10000232', 'text', '那今晚会否出现这二个操盘点？', null, null, '2016-06-28 20:52:29', '2016-06-28 20:52:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2419', '10000001', 'text', '会的，因为今晚凌晨4:30有API原油库存的重磅数据，将会继续影响油价', '10000232', '那今晚会否出现这二个操盘点？', '2016-06-28 20:54:17', '2016-06-28 20:54:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2420', '10000232', 'text', '今晚API原油库存的数据，如果量大是利空？反之则利多？', null, null, '2016-06-28 20:59:16', '2016-06-28 20:59:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2421', '10000001', 'text', '没错#20', '10000232', '今晚API原油库存的数据，如果量大是利空？反之则利多？', '2016-06-28 21:00:20', '2016-06-28 21:00:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2422', '10000232', 'text', '老师，这次美国GDP公布，会给我们带来多大盈利空间', null, null, '2016-06-28 21:04:04', '2016-06-28 21:04:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2423', '10000001', 'text', '根据以往公布后元原油的波动是带来的平均收益的空间是128%', '10000232', '老师，这次美国GDP公布，会给我们带来多大盈利空间', '2016-06-28 21:08:17', '2016-06-28 21:08:17', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2424', '10000001', 'text', '根据以往数据统计，数据公布后，油价波动带来的平均收益空间达到128%的', '10000232', '老师，这次美国GDP公布，会给我们带来多大盈利空间', '2016-06-28 21:09:38', '2016-06-28 21:09:38', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2425', '10000001', 'text', '根据以往数据统计，数据公布后，油价波动带来的平均收益空间达到128%的', '10000232', '老师，这次美国GDP公布，会给我们带来多大盈利空间', '2016-06-28 21:09:56', '2016-06-28 21:09:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2426', '10000232', 'text', '那今晚会否出现多次操盘机会？', null, null, '2016-06-28 21:11:59', '2016-06-28 21:11:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2427', '10000001', 'text', '目前API数据未公布之前，走势还是倾向于下跌的', '10000232', '那今晚会否出现多次操盘机会？', '2016-06-28 21:13:37', '2016-06-28 21:13:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2428', '10000001', 'text', '今晚公布的数据，美国一季度GDP增速上修至1.1%，主要是因为软件方面的投资和出口部分抵消了消费支出的疲软', null, null, '2016-06-28 21:17:13', '2016-06-28 21:17:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2429', '10000001', 'text', '表明尽管美国经济在一季度出现放缓，但并没有此前预期严重', null, null, '2016-06-28 21:17:32', '2016-06-28 21:17:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2430', '10000001', 'text', '另外从4、5月的零售销售和房屋销售数据来看，美国经济似乎正在重获动能', null, null, '2016-06-28 21:23:48', '2016-06-28 21:23:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2431', '10000170', 'text', '老师，目前止盈位置还是在310吗？', null, null, '2016-06-28 21:26:14', '2016-06-28 21:26:14', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2432', '10000001', 'text', '目前是312附近', '10000170', '老师，目前止盈位置还是在310吗？', '2016-06-28 21:29:18', '2016-06-28 21:29:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2433', '10000170', 'text', '#9老师美国标普500开盘平开，对原油冲高有支撑么', null, null, '2016-06-28 21:32:38', '2016-06-28 21:32:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2434', '10000001', 'text', '没有，油价的支撑不是股市引起的，是油价影响股市', '10000170', '#9老师美国标普500开盘平开，对原油冲高有支撑么', '2016-06-28 21:36:03', '2016-06-28 21:36:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2435', '10000170', 'text', '#5哦', null, null, '2016-06-28 21:37:00', '2016-06-28 21:37:00', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2436', '10000001', 'text', '目前油价空头较弱啊，一直在压力位不下落', null, null, '2016-06-28 21:47:56', '2016-06-28 21:47:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2437', '10000170', 'text', '看来再等API', null, null, '2016-06-28 21:56:59', '2016-06-28 21:56:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2438', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57728285c4744.png', null, null, '2016-06-28 21:58:29', '2016-06-28 21:58:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2439', '10000001', 'text', '目前油价未冲破压力线，空单还是有机会的', null, null, '2016-06-28 21:58:56', '2016-06-28 21:58:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2440', '10000001', 'text', '如果冲破317位，空单可以先锁仓一半，目标看321', null, null, '2016-06-28 22:05:23', '2016-06-28 22:05:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2441', '10000170', 'text', '老师，空单还可以持有吗？', null, null, '2016-06-28 22:16:13', '2016-06-28 22:16:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2442', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/5772874026dc6.png', null, null, '2016-06-28 22:18:40', '2016-06-28 22:18:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2443', '10000001', 'text', '油价继续压力位震荡', null, null, '2016-06-28 22:19:51', '2016-06-28 22:19:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2444', '10000001', 'text', '没突破317空单还是可以持有的', '10000170', '老师，空单还可以持有吗？', '2016-06-28 22:20:24', '2016-06-28 22:20:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2445', '10000001', 'text', '英国财政大臣奥斯本：在英国和欧盟新关系模式明朗化之前，不应该触发第50条', null, null, '2016-06-28 22:31:08', '2016-06-28 22:31:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2446', '10000187', 'text', '老师，什么是第50条？', null, null, '2016-06-28 22:32:05', '2016-06-28 22:32:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2447', '10000001', 'text', '规定了成员国退出欧盟的过程#9', '10000187', '老师，什么是第50条？', '2016-06-28 22:34:10', '2016-06-28 22:34:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2448', '10000187', 'text', '这些条款哪里可以看呢？', null, null, '2016-06-28 22:38:12', '2016-06-28 22:38:12', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2449', '10000194', 'text', '老师止盈大概多少呀#3', null, null, '2016-06-28 22:39:06', '2016-06-28 22:39:06', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2450', '10000001', 'text', '今晚空单止盈位个人建议312-313附近', '10000194', '老师止盈大概多少呀#3', '2016-06-28 22:40:25', '2016-06-28 22:40:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2451', '10000194', 'text', '#9#9#9嗯', null, null, '2016-06-28 22:41:14', '2016-06-28 22:41:14', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2452', '10000001', 'text', '网上，不过是英文的#17', '10000187', '这些条款哪里可以看呢？', '2016-06-28 22:41:30', '2016-06-28 22:41:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2453', '10000194', 'text', '老师我平咯，收摊睡美容觉了#18', null, null, '2016-06-28 22:44:46', '2016-06-28 22:44:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2454', '10000187', 'text', '#19好吧，给我也看不懂#17', null, null, '2016-06-28 22:45:29', '2016-06-28 22:45:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2455', '10000194', 'text', '老师辛苦了#9', null, null, '2016-06-28 22:45:42', '2016-06-28 22:45:42', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2456', '10000001', 'text', '沙特阿美CEO：出口至欧洲的石油不会受到英国脱欧的影响', null, null, '2016-06-28 22:49:01', '2016-06-28 22:49:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2457', '10000001', 'text', '#14#17', '10000194', '老师我平咯，收摊睡美容觉了#18', '2016-06-28 22:49:47', '2016-06-28 22:49:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2458', '10000194', 'text', '#6#6一会半夜起床在找你', null, null, '2016-06-28 22:51:10', '2016-06-28 22:51:10', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2459', '10000001', 'text', '好，我们直播室会为大家直播行情到凌晨1:30的喔#9', '10000194', '#6#6一会半夜起床在找你', '2016-06-28 22:57:38', '2016-06-28 22:57:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2460', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/577291e1117f4.png', null, null, '2016-06-28 23:04:01', '2016-06-28 23:04:01', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2461', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57729216432b4.png', null, null, '2016-06-28 23:04:54', '2016-06-28 23:04:54', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2462', '10000001', 'text', '油价在压力位316附近震荡后，目前继续回落', null, null, '2016-06-28 23:06:30', '2016-06-28 23:06:30', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2463', '10000001', 'text', '油价在压力位316附近震荡后，目前回落到312附近', null, null, '2016-06-28 23:07:46', '2016-06-28 23:07:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2464', '10000001', 'text', '惠誉：英国脱欧有可能促使美元走强，从而影响经济增长，进而推迟美联储加息时间', null, null, '2016-06-28 23:17:42', '2016-06-28 23:17:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2465', '10000001', 'text', '据彭博：英国议会小组推迟就退欧投票请愿做出决定', null, null, '2016-06-28 23:25:13', '2016-06-28 23:25:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2466', '10000001', 'text', '据报道：中国炼厂已经向本已过剩的亚洲市场出售创纪录规模的汽油和柴油燃料', null, null, '2016-06-28 23:35:08', '2016-06-28 23:35:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2467', '10000001', 'text', '所以中国炼厂可能减产并限制原油订单，将有可能打压油价', null, null, '2016-06-28 23:40:46', '2016-06-28 23:40:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2468', '10000001', 'text', '高盛：英国脱欧或导致欧洲银行利润减少350亿美元', null, null, '2016-06-28 23:50:59', '2016-06-28 23:50:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2469', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160628/57729d51672fb.png', null, null, '2016-06-28 23:52:49', '2016-06-28 23:52:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2470', '10000001', 'text', '如果凌晨4:30公布的API数据利多油价，那么油价将会冲破316压力位拉升至320附近后震荡', null, null, '2016-06-28 23:55:38', '2016-06-28 23:55:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2471', '10000232', 'text', '老师，现在是否平仓？', null, null, '2016-06-28 23:56:46', '2016-06-28 23:56:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2472', '10000001', 'text', '上述讲过，今晚的止盈位在312附近', '10000232', '老师，现在是否平仓？', '2016-06-29 00:00:44', '2016-06-29 00:00:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2473', '10000232', 'text', '啊，那我过头了啊，怎么办', null, null, '2016-06-29 00:01:05', '2016-06-29 00:01:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2474', '10000001', 'text', '可以持有观察API数据情况，个人建议如果有利润可以先平仓', '10000232', '啊，那我过头了啊，怎么办', '2016-06-29 00:03:11', '2016-06-29 00:03:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2475', '10000232', 'text', '亏损#19', null, null, '2016-06-29 00:03:43', '2016-06-29 00:03:43', '-1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2476', '10000232', 'text', '不是说有50%以上的利润吗？', null, null, '2016-06-29 00:04:58', '2016-06-29 00:04:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2477', '10000001', 'text', '根据以往的历史走势统计，数据公布后引起油价波幅有50%以上的利润，行情千变万化，没有100%的#9', '10000232', '不是说有50%以上的利润吗？', '2016-06-29 00:08:17', '2016-06-29 00:08:17', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2478', '10000001', 'text', '目前才凌晨，04:30还有个重磅数据API未公布，今晚的行情还没走完的', '10000232', '不是说有50%以上的利润吗？', '2016-06-29 00:10:03', '2016-06-29 00:10:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2479', '10000232', 'text', '平了，今晚还有操盘吗？', null, null, '2016-06-29 00:12:50', '2016-06-29 00:12:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2480', '10000001', 'text', '行情是有的，基本上每周API数据公布后都会瞬间引起油价的波动#9', '10000232', '平了，今晚还有操盘吗？', '2016-06-29 00:18:13', '2016-06-29 00:18:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2481', '10000232', 'text', 'API公布后也是明天的事了啊，现在老师还指点如何建仓吗？', null, null, '2016-06-29 00:24:02', '2016-06-29 00:24:02', '-1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2482', '10000001', 'text', '欧洲央行管委：目前市场已经逐渐安静和平衡', null, null, '2016-06-29 00:26:48', '2016-06-29 00:26:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2483', '10000001', 'text', '   各位亲爱的投资朋友们，直播室是一个学习投资方法、传播投资理念的地方，客户的互动栏是提供投资者朋友交流的地方，切勿传播广告、脏话、等不良情绪，请为保持良好的投资心态，与老师一起在市场斩获收益！', null, null, '2016-06-29 00:31:44', '2016-06-29 00:31:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2484', '10000232', 'text', '老师，现在是否建仓睡觉？空？多？还是空仓好？', null, null, '2016-06-29 00:39:05', '2016-06-29 00:39:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2485', '10000001', 'text', '亚洲炼油厂即将进入维修季，油价必然会有所下降', null, null, '2016-06-29 00:40:59', '2016-06-29 00:40:59', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2486', '10000001', 'text', '亚洲炼油厂即将进入维修季，油价必然会有所下降', null, null, '2016-06-29 00:41:19', '2016-06-29 00:41:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2487', '10000232', 'text', '那就是说建空？老师明天几点在？', null, null, '2016-06-29 00:42:59', '2016-06-29 00:42:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2488', '10000001', 'text', '个人建议：不建议建仓睡觉，数据没法预测，行情什么时候都有不着急一时#9', '10000232', '老师，现在是否建仓睡觉？空？多？还是空仓好？', '2016-06-29 00:43:54', '2016-06-29 00:43:54', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2489', '10000001', 'text', '我们直播室早上9:30为大家直播行情的#9', '10000232', '那就是说建空？老师明天几点在？', '2016-06-29 00:44:41', '2016-06-29 00:44:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2490', '10000232', 'text', '好的，那我先睡觉了，老师你也早点休息#20#9', null, null, '2016-06-29 00:45:32', '2016-06-29 00:45:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2491', '10000001', 'text', '路透数据显示，本月在新加坡海域储油的油轮从5月的40艘降到了30艘，交易商都在维修季之前抛售原油库存', null, null, '2016-06-29 00:54:43', '2016-06-29 00:54:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2492', '10000001', 'text', '欧洲央行行长德拉基：预计英国脱欧将导致欧元区GDP减少不超过0.5%', null, null, '2016-06-29 01:02:27', '2016-06-29 01:02:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2493', '10000001', 'text', '欧洲央行行长德拉基：预计未来3年GDP增速下降', null, null, '2016-06-29 01:13:55', '2016-06-29 01:13:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2494', '10000001', 'text', '惠誉：爱尔兰经济受到英国脱欧的巨大影响。', null, null, '2016-06-29 01:29:46', '2016-06-29 01:29:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2495', '10000001', 'text', '目前油价回落后继续上探至316-315压力位附近', null, null, '2016-06-29 01:32:48', '2016-06-29 01:32:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2496', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5772b50f2b9e3.png', null, null, '2016-06-29 01:34:07', '2016-06-29 01:34:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2497', '10000001', 'text', '预测后半夜将会在316-315压力位区间震荡，等待API数据', null, null, '2016-06-29 01:35:24', '2016-06-29 01:35:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2498', '10000001', 'text', '好了，今日的直播就到这里了，感谢大家的收看#9', null, null, '2016-06-29 01:36:00', '2016-06-29 01:36:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2499', '10000001', 'text', '大家晚安咯#9', null, null, '2016-06-29 01:36:13', '2016-06-29 01:36:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2500', '8', 'text', '朋友们早上好！#4', null, null, '2016-06-29 09:29:13', '2016-06-29 09:29:13', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2501', '8', 'text', '人生是坎坷的，人生是崎岖的。我坚信：在人生中只有曲线前进的快乐，没有直线上升的成功。只有珍惜今天，才会有美好的明天；只有把握住今天，才会有更辉煌的明天！人生啊，朋友啊！还等什么？奋斗吧！#9\n\n', null, null, '2016-06-29 09:31:22', '2016-06-29 09:31:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2502', '10000000', 'text', '大家早上好！欢迎来到轩湛投资观察室！', null, null, '2016-06-29 09:31:23', '2016-06-29 09:31:23', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2503', '10000000', 'text', '伍老师早上好！#17', null, null, '2016-06-29 09:31:36', '2016-06-29 09:31:36', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2504', '10000000', 'text', '昨天受到西欧最大产油国挪威和南美阿根廷的石油工人罢工影响，原油震荡走强。', null, null, '2016-06-29 09:33:24', '2016-06-29 09:33:24', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2505', '10000000', 'text', '而凌晨的API原油库存数据也低于预期，利好原油，带动原油继续大幅高开。', null, null, '2016-06-29 09:34:14', '2016-06-29 09:34:14', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2506', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160629/577325c7e9358.jpg', null, null, '2016-06-29 09:35:04', '2016-06-29 09:35:04', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2507', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160629/577325f88742a.jpg', null, null, '2016-06-29 09:35:52', '2016-06-29 09:35:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2508', '10000000', 'text', '而昨晚，库欣地区的原油连续两周出现下滑，这是本年以来的首次。', null, null, '2016-06-29 09:36:19', '2016-06-29 09:36:19', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2509', '10000000', 'text', '数据导致了原油今天的高开', null, null, '2016-06-29 09:36:30', '2016-06-29 09:36:30', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2510', '10000000', 'text', '回顾完行情以后，下面我们来进入股票点对点栏目#4', null, null, '2016-06-29 09:36:55', '2016-06-29 09:36:55', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2511', '10000000', 'text', '本周以来，A股走势较为强势，并没有受到上周英国退欧的影响。', null, null, '2016-06-29 09:39:04', '2016-06-29 09:39:04', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2512', '10000000', 'text', '技术上，大盘连续下探后，目前还是较为抗跌的。但需要站稳长期均线上方才有可能进一步反弹', null, null, '2016-06-29 09:41:53', '2016-06-29 09:41:53', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2513', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160629/577327a22645c.jpg', null, null, '2016-06-29 09:42:58', '2016-06-29 09:42:58', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2514', '10000000', 'text', '而短周期上，短期反弹的格局还是相当明显的。', null, null, '2016-06-29 09:43:39', '2016-06-29 09:43:39', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2515', '10000232', 'text', '老师，300367怎么看', null, null, '2016-06-29 09:47:18', '2016-06-29 09:47:18', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2516', '10000102', 'text', '做多', null, null, '2016-06-29 09:47:32', '2016-06-29 09:47:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2517', '10000102', 'text', '可以吗？', null, null, '2016-06-29 09:47:36', '2016-06-29 09:47:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2518', '10000000', 'text', '结构上看300367走势还是不错的，唯一的问题是看看成交量能否放大跟上', '10000232', '老师，300367怎么看', '2016-06-29 09:52:50', '2016-06-29 09:52:50', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2519', '10000000', 'text', '你指原油还是股票呢', '10000102', '可以吗？', '2016-06-29 09:53:01', '2016-06-29 09:53:01', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2520', '10000000', 'text', '板块方面，船舶，军工，航天等板块涨幅居前。后市重点关注除次新股和中报高送转外，蓝筹股以业绩为主，更多的热点机会仍然在新兴产业。数据显示，新兴产业的代表创业板和中小板周二成交量飙升到3672亿元，是巨无霸云集的沪市成交量的1.74倍，这意味着，中小盘股的活跃度远远高于大盘蓝筹股。', null, null, '2016-06-29 09:56:55', '2016-06-29 09:56:55', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2521', '10000102', 'text', '原油，今早已经高开了，还会继续向上吗？', null, null, '2016-06-29 09:58:13', '2016-06-29 09:58:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2522', '10000000', 'text', '现在还在走昨晚API的预期行情', '10000102', '原油，今早已经高开了，还会继续向上吗？', '2016-06-29 10:01:39', '2016-06-29 10:01:39', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2523', '10000000', 'text', '今天A股的走势还是相当强势的，但是由于成交量的不足，建议大家见好就收，前期被套的朋友，抓住反弹机会减仓离场', null, null, '2016-06-29 10:03:19', '2016-06-29 10:03:19', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2524', '10000000', 'text', '受到股市走强的影响，大宗商品也跟着全线大涨', null, null, '2016-06-29 10:06:37', '2016-06-29 10:06:37', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2525', '10000232', 'text', '也就是说，原油也会往上？', null, null, '2016-06-29 10:10:12', '2016-06-29 10:10:12', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2526', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160629/57732e235500b.jpg', null, null, '2016-06-29 10:10:43', '2016-06-29 10:10:43', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2527', '10000000', 'text', '大盘如果能站稳2950，走出箱体震荡，那么后市就有机会测试3050上方', null, null, '2016-06-29 10:11:14', '2016-06-29 10:11:14', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2528', '10000000', 'text', '是的，有一定的影响', '10000232', '也就是说，原油也会往上？', '2016-06-29 10:11:23', '2016-06-29 10:11:23', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2529', '10000000', 'text', '由于有紧急会议要开，下面把时间先交给伍老师继续做行情解读', null, null, '2016-06-29 10:11:52', '2016-06-29 10:11:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2530', '8', 'text', '好的，郑老师要先去开会，由我继续给大家讲解。#17', null, null, '2016-06-29 10:15:11', '2016-06-29 10:15:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2531', '8', 'text', '还是接着股票方面。', null, null, '2016-06-29 10:18:36', '2016-06-29 10:18:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2532', '8', 'text', '外围市场反弹，沪指两连阳，现在是选择向上突破最好的一个时间节点，就看市场选择怎么走了！', null, null, '2016-06-29 10:19:52', '2016-06-29 10:19:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2533', '8', 'text', '在谨慎乐观的同时，也需要保持警惕，毕竟2900之上是重压区，未必一蹴而就，可能还会有反复。', null, null, '2016-06-29 10:21:51', '2016-06-29 10:21:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2534', '8', 'text', '若没量能配合，冲高继续高抛为主，落袋为安！', null, null, '2016-06-29 10:22:38', '2016-06-29 10:22:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2535', '8', 'text', '在昨日的达沃斯论坛上，李克强总理表示：“中国将进一步放宽对外国投资者的准入。中国不会允许资本市场出现井喷式和断崖式的变化。中国将按市场规则和国际惯例对个别违约行为依法处理。英国脱欧后中国尽努力维护自身金融和资本市场稳定。中国仍是世界上最有潜力的投资的市场。”', null, null, '2016-06-29 10:24:53', '2016-06-29 10:24:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2536', '8', 'text', '这是李总理几个月来首次强调维护资本市场稳定，无疑是英国脱欧后政府高层对股市的最新支持性变态，对市场做多有激励作用。', null, null, '2016-06-29 10:27:04', '2016-06-29 10:27:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2537', '10000103', 'text', '#6大家早上好#6', null, null, '2016-06-29 10:28:27', '2016-06-29 10:28:27', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2538', '10000232', 'text', '早上好', null, null, '2016-06-29 10:28:44', '2016-06-29 10:28:44', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2539', '10000103', 'text', '我从英国回来啦#4', null, null, '2016-06-29 10:29:25', '2016-06-29 10:29:25', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2540', '10000103', 'text', '这几天直播室也是硕果累累！昨晚我们伍老师也给出一个短线的操作，顺利止盈！', null, null, '2016-06-29 10:30:46', '2016-06-29 10:30:46', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2541', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160629/577332dd5dda4.jpg', null, null, '2016-06-29 10:30:53', '2016-06-29 10:30:53', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2542', '8', 'text', '早上好#20', '10000232', '早上好', '2016-06-29 10:31:59', '2016-06-29 10:31:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2543', '8', 'text', '今天我们小罗老师回来了，大家欢迎啊！#4', null, null, '2016-06-29 10:33:53', '2016-06-29 10:33:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2544', '10000232', 'text', '伍老师给我们带来高利润，小秘书美女你也从英国带回丰厚的利润？', null, null, '2016-06-29 10:34:21', '2016-06-29 10:34:21', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2545', '10000103', 'text', '当然啦，除了利润，还给大家带来了丰富的英伦文化', '10000232', '伍老师给我们带来高利润，小秘书美女你也从英国带回丰厚的利润？', '2016-06-29 10:38:57', '2016-06-29 10:38:57', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2546', '10000103', 'text', '#5#5', null, null, '2016-06-29 10:39:23', '2016-06-29 10:39:23', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2547', '5', 'text', '今晚EIA利多比较多么？', null, null, '2016-06-29 10:43:51', '2016-06-29 10:43:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2548', '8', 'text', '预测值是利多的，今天凌晨的API数据库存大幅降低，按往期规律来看晚上的EIA利多机会比较大。', '5', '今晚EIA利多比较多么？', '2016-06-29 10:49:32', '2016-06-29 10:49:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2549', '10000232', 'text', '那现在就建仓利多了？？', null, null, '2016-06-29 10:50:11', '2016-06-29 10:50:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2550', '8', 'text', '目前多头上攻有所减缓，可能有回调需要，激进的话，现在建仓是可以的，顺势而为，但是止盈止损也记得带好。', '10000232', '那现在就建仓利多了？？', '2016-06-29 11:03:23', '2016-06-29 11:03:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2551', '8', 'text', '两市继续震荡回落创业板翻绿，沪指涨0.36%创业板跌0.31%。', null, null, '2016-06-29 11:05:25', '2016-06-29 11:05:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2552', '8', 'text', '前期走强的稀土永磁板块今日再度走强', null, null, '2016-06-29 11:12:06', '2016-06-29 11:12:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2553', '10000102', 'text', '原油跌了', null, null, '2016-06-29 11:23:22', '2016-06-29 11:23:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2554', '8', 'text', '高盛表示目前钻井数显示美国石油产量将继续下降，预计今明两年美国产量同比平均下降66万桶/日和38万桶/日，此外预计若钻井数维持在目前水平。', null, null, '2016-06-29 11:26:05', '2016-06-29 11:26:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2555', '8', 'text', '昨天涨幅逾3%，短线是有回调需要的', '10000102', '原油跌了', '2016-06-29 11:27:55', '2016-06-29 11:27:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2556', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773423780d89.png', null, null, '2016-06-29 11:36:23', '2016-06-29 11:36:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2557', '8', 'text', '日线图受中轨压制，上升走缓，预计有回调需要，短线做空操作。#17', null, null, '2016-06-29 11:37:47', '2016-06-29 11:37:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2558', '8', 'text', '大家根据自身做单风格，可适当作参考。#9', null, null, '2016-06-29 11:39:05', '2016-06-29 11:39:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2559', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/577343ff056df.png', null, null, '2016-06-29 11:43:59', '2016-06-29 11:43:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2560', '10000232', 'text', '建仓后能指价平仓的吗？', null, null, '2016-06-29 11:44:06', '2016-06-29 11:44:06', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2561', '10000002', 'text', '大家好 ', null, null, '2016-06-29 11:44:15', '2016-06-29 11:44:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2562', '10000002', 'text', '我是新人 望各位大神 今后多多指教 ', null, null, '2016-06-29 11:45:16', '2016-06-29 11:45:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2563', '8', 'text', '可以的，设置止盈止损就是在指定的价格平仓。', '10000232', '建仓后能指价平仓的吗？', '2016-06-29 11:45:19', '2016-06-29 11:45:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2564', '8', 'text', '你好！', '10000002', '大家好 ', '2016-06-29 11:45:37', '2016-06-29 11:45:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2565', '10000002', 'text', '这里面好多人啊 ', null, null, '2016-06-29 11:46:09', '2016-06-29 11:46:09', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2566', '8', 'text', '欢迎你加入轩湛直播观察室。', '10000002', '我是新人 望各位大神 今后多多指教 ', '2016-06-29 11:46:26', '2016-06-29 11:46:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2567', '10000002', 'text', '轩湛 #20', null, null, '2016-06-29 11:47:43', '2016-06-29 11:47:43', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2568', '10000103', 'text', '欢迎欢饮#14', '10000002', '这里面好多人啊 ', '2016-06-29 11:48:20', '2016-06-29 11:48:20', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2569', '8', 'text', '普京大帝！#14大家欢迎', '10000002', '这里面好多人啊 ', '2016-06-29 11:49:03', '2016-06-29 11:49:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2570', '10000002', 'text', '就两个人欢迎我  唉', null, null, '2016-06-29 11:49:13', '2016-06-29 11:49:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2571', '8', 'text', '大家都被你的霸气震住了#4', '10000002', '就两个人欢迎我  唉', '2016-06-29 11:50:28', '2016-06-29 11:50:28', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2572', '8', 'text', '股市早盘，大盘维持震荡格局，沪指半日小幅上涨；热点方面，早盘次新股、稀土永磁等涨幅居前。', null, null, '2016-06-29 11:51:44', '2016-06-29 11:51:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2573', '8', 'text', '截至午间收盘，沪指涨0.48%，报2926.46点；深证成指微跌0.01%，报10462.58点；创业板指跌0.22%，报2211.89点。', null, null, '2016-06-29 11:52:08', '2016-06-29 11:52:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2574', '10000102', 'text', '', '10000002', '就两个人欢迎我  唉', '2016-06-29 11:53:30', '2016-06-29 11:53:30', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2575', '10000102', 'text', '你的名字太霸气', null, null, '2016-06-29 11:53:40', '2016-06-29 11:53:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2576', '8', 'text', '盘面上看，次新股板块涨近3%，稀土永磁、船舶、海外工程、保险等板块均涨逾2%，中字头、建筑、钛金属、有色等也涨幅居前；半导体、宽带提速、4G概念、量子通信、维生素等跌幅居前。', null, null, '2016-06-29 11:54:11', '2016-06-29 11:54:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2577', '8', 'text', '#20', '10000102', '你的名字太霸气', '2016-06-29 11:54:36', '2016-06-29 11:54:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2578', '10000002', 'text', '', '10000102', '你的名字太霸气', '2016-06-29 11:57:29', '2016-06-29 11:57:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2579', '10000002', 'text', '今后大家来俄罗斯玩，就直接跟我讲！', null, null, '2016-06-29 11:58:19', '2016-06-29 11:58:19', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2580', '8', 'text', '从具体形态上看，震荡的区间越来越小了，因此，这样一个收敛的形态，正在接近一个尾声。', null, null, '2016-06-29 12:00:03', '2016-06-29 12:00:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2581', '8', 'text', '又到了吃午饭的时间，大家美餐一顿，下午继续再战！#4', null, null, '2016-06-29 12:01:11', '2016-06-29 12:01:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2582', '8', 'text', '哈哈，大家不会跟你客气的。', '10000002', '今后大家来俄罗斯玩，就直接跟我讲！', '2016-06-29 12:02:30', '2016-06-29 12:02:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2583', '10000102', 'text', '可以，玩儿俄罗斯方块可以找你吗？哈哈哈哈', null, null, '2016-06-29 12:21:34', '2016-06-29 12:21:34', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2584', '8', 'text', '#5', '10000102', '可以，玩儿俄罗斯方块可以找你吗？哈哈哈哈', '2016-06-29 13:00:24', '2016-06-29 13:00:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2585', '8', 'text', '朋友们下午好！#9', null, null, '2016-06-29 13:00:54', '2016-06-29 13:00:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2586', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773566450a17.png', null, null, '2016-06-29 13:02:28', '2016-06-29 13:02:28', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2587', '8', 'text', '目前，行情在320附近震荡整理。', null, null, '2016-06-29 13:03:41', '2016-06-29 13:03:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2588', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/57735815ec355.png', null, null, '2016-06-29 13:09:42', '2016-06-29 13:09:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2589', '8', 'text', '昨天美元指数的回落，也为这一波强势反弹提供了支撑。', null, null, '2016-06-29 13:10:55', '2016-06-29 13:10:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2590', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/577359cc3fd9b.png', null, null, '2016-06-29 13:17:00', '2016-06-29 13:17:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2591', '8', 'text', '从盘面上我们可以看到，目前价格在322位置受到黄金分割线61.8%压制，上升势头暂缓。', null, null, '2016-06-29 13:21:42', '2016-06-29 13:21:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2592', '10000000', 'text', '原油这么便宜，大帝来买点油#4', '10000002', '今后大家来俄罗斯玩，就直接跟我讲！', '2016-06-29 13:25:21', '2016-06-29 13:25:21', '-1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2593', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/57735c21b682f.png', null, null, '2016-06-29 13:26:57', '2016-06-29 13:26:57', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2594', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/57735cd14828c.png', null, null, '2016-06-29 13:29:53', '2016-06-29 13:29:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2595', '8', 'text', '晚间10.30有美国当周EIA原油库存数据，大家都要记得关注。', null, null, '2016-06-29 13:31:55', '2016-06-29 13:31:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2596', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/57735dc7d9c20.png', null, null, '2016-06-29 13:33:59', '2016-06-29 13:33:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2597', '8', 'text', '上周EIA公布的截至6月17日当周原油库存录得减少91.7万桶。', null, null, '2016-06-29 13:34:54', '2016-06-29 13:34:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2598', '8', 'text', '预期值为减少167.1万桶，前值为减少93.3万桶，连续五周录得下滑。', null, null, '2016-06-29 13:35:44', '2016-06-29 13:35:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2599', '8', 'text', '但是其中汽油和精炼油库存却双双录得增加。', null, null, '2016-06-29 13:39:03', '2016-06-29 13:39:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2600', '8', 'text', '虽然上周美国EIA原油库存连续5周下降，但不及预期，且EIA汽油库存却意外增加，致使原油价格短线迅速跳水，抹掉日内涨幅之后并大幅下挫。', null, null, '2016-06-29 13:40:34', '2016-06-29 13:40:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2601', '8', 'text', '美国石油协会(API)今天凌晨4:30公布的数据显示，美国API原油库存大减386万桶，预计减少237.5万桶，降至5.272亿桶。', null, null, '2016-06-29 13:47:42', '2016-06-29 13:47:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2602', '8', 'text', '数据公布后，原油价格短线跳涨，突破320元/桶关口。', null, null, '2016-06-29 13:48:25', '2016-06-29 13:48:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2603', '8', 'text', '同时美国API库欣地区原油库存减少减少120.7万桶，再次录得下降。', null, null, '2016-06-29 13:51:06', '2016-06-29 13:51:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2604', '8', 'text', '此前截至6月17日当周，美国API原油库存大减522.4万桶，录得半年来最大降幅，之后，美国API原油库存再次超预期大幅削减，无疑表明市场原油需求较为强劲。在英国脱欧忧虑暂缓情况下，美国原油库存的削减无疑将会为国际原油价格提供短线强劲支撑。', null, null, '2016-06-29 13:53:41', '2016-06-29 13:53:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2605', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773636ae6257.jpg', null, null, '2016-06-29 13:58:02', '2016-06-29 13:58:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2606', '8', 'text', '每周一次，每月四次，EIA原油库存数据再度来袭，偏爱EIA的投资者今夜又将迎来一场盛宴。', null, null, '2016-06-29 13:58:33', '2016-06-29 13:58:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2607', '8', 'text', '做过原油投资的人都知道，EIA往往伴随着大行情。#4', null, null, '2016-06-29 14:01:40', '2016-06-29 14:01:40', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2608', '8', 'text', '做过原油投资的人都知道，EIA往往伴随着大行情。#4', null, null, '2016-06-29 14:02:11', '2016-06-29 14:02:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2609', '8', 'text', '为何每次公布EIA库存总是会有大行情?', null, null, '2016-06-29 14:03:36', '2016-06-29 14:03:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2610', '8', 'text', '美国EIA原油库存数据由美国能源信息署(EIA)定期发布，该数据测量了每周美国公司的商业原油库存的变化，而库存的数目影响了可以对通货膨胀和其他经济影响力造成影响的成品油的价格。', null, null, '2016-06-29 14:04:38', '2016-06-29 14:04:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2611', '8', 'text', '若库存水平低于预期，利多原油;若库存水平高于预期，利空原油。', null, null, '2016-06-29 14:05:06', '2016-06-29 14:05:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2612', '8', 'text', '其具体公式如下：公布数据结果大于预期 = 利空原油价格 (利空，也就是看跌)；公布数据结果小于预期 = 利多原油价格 (利多，也就是看涨)。', null, null, '2016-06-29 14:10:10', '2016-06-29 14:10:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2613', '8', 'text', 'EIA库存数据报告公布原油的历史日均波动率为2.77%，最大日波动率高达8.37%，是每周投资原油的绝佳良机。', null, null, '2016-06-29 14:12:25', '2016-06-29 14:12:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2614', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/57736723b5633.png', null, null, '2016-06-29 14:13:55', '2016-06-29 14:13:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2615', '8', 'text', 'EIA库存数据一般分三波走!', null, null, '2016-06-29 14:19:55', '2016-06-29 14:19:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2616', '8', 'text', '第一波：EIA前，预期行情。', null, null, '2016-06-29 14:20:28', '2016-06-29 14:20:28', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2617', '8', 'text', '在EIA前30分钟，我们着重分析EIA数据先行指标，如果以上数据利好，市场普遍预期EIA数据将表现出色，那么我们可以得出一下预测结论，油价或受到提振上扬。', null, null, '2016-06-29 14:20:33', '2016-06-29 14:20:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2618', '8', 'text', '其参考值周一公布的外媒调查以及之前公布的API库存数据，当然，还有历史EIA库存数据，重点是上周的库存数据。', null, null, '2016-06-29 14:21:47', '2016-06-29 14:21:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2619', '8', 'text', '第二波：EIA公布', null, null, '2016-06-29 14:22:20', '2016-06-29 14:22:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2620', '8', 'text', '在10点30分EIA公布以后，一直到11点。这段时间数据会转瞬间反映在市场上，这段原油行情快速而剧烈，也是最大获利最快的行情，一般投资者较难把握。对于这段时间的行情，也有许多不同的方法。', null, null, '2016-06-29 14:22:49', '2016-06-29 14:22:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2621', '8', 'text', '1、双向挂单，即在EIA公布前3分钟，挂一个3点左右的高位挂多单和一个3点左右的低位挂空单，挂上去之前设好5点的获利。一单成交后取消另一单。缺点就是行情波动过快，双向扫损，或者行情达不到，则需要手动平仓。此类交易手法要求动手迅速，而且坚决果断不可恋战，对投资者执行力，风险承担能力等要求高。', null, null, '2016-06-29 14:24:05', '2016-06-29 14:24:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2622', '8', 'text', '2、在消息公布前约3分钟按消息预期做单。这种则比较简单，按消息预期同向做一单，设置止盈与止损，一般盈亏比至少在2：1，以小博大，这种做三次EIA成功一次一般能保本。', null, null, '2016-06-29 14:25:15', '2016-06-29 14:25:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2623', '8', 'text', '3、数据公布后，迅速顺势做单。故名思议，就是在EIA数据公布以后，迅速分析市行情，顺势而为操作，注意及时止损止盈。缺点是行情宽幅震荡下进场点位防止追涨杀跌，对交易平台的敏感度要求较高，对投资者研判能力也较高。', null, null, '2016-06-29 14:26:50', '2016-06-29 14:26:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2624', '8', 'text', '第三波：EIA公布后', null, null, '2016-06-29 14:27:15', '2016-06-29 14:27:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2625', '8', 'text', 'EIA数据公布后30分钟后，原油行情相对趋于稳定。投资者可以根据消息的实际情况，利好或者利空，结合K线形态和均线系统，再做回调或者趁反弹顺势而为。', null, null, '2016-06-29 14:27:43', '2016-06-29 14:27:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2626', '8', 'text', '做单理念：投资想赚大钱一定是做趋势，一个趋势20-50个点的利润，实盘操作手法以做趋势为主，抓住趋势，两三波就能实现可观盈利。', null, null, '2016-06-29 14:30:55', '2016-06-29 14:30:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2627', '8', 'text', '好了，EIA数据就先解析到这里。下面我们开始今天的基础课程讲解。#15', null, null, '2016-06-29 14:32:39', '2016-06-29 14:32:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2628', '10000002', 'text', ' 当然可以啊 ', '10000102', '可以，玩儿俄罗斯方块可以找你吗？哈哈哈哈', '2016-06-29 14:33:31', '2016-06-29 14:33:31', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2629', '10000102', 'text', '#5', null, null, '2016-06-29 14:34:59', '2016-06-29 14:34:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2630', '10000102', 'text', '我也要上一些基础的理论，什么都不会分析#16', null, null, '2016-06-29 14:35:38', '2016-06-29 14:35:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2631', '8', 'text', '投资领域里，宏观经济分析法和技术分析法都各有优势。宏观经济方面，需要大家日常阅读与积累，所以，咱们的课程讲解就是以技术分析为主。', null, null, '2016-06-29 14:39:29', '2016-06-29 14:39:29', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2632', '8', 'text', '为了系统讲解，咱们今天就先从最基础的K线讲起。', null, null, '2016-06-29 14:41:27', '2016-06-29 14:41:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2633', '8', 'text', '跟着直播室节奏，慢慢你也将成为投资达人#17', '10000102', '我也要上一些基础的理论，什么都不会分析#16', '2016-06-29 14:42:21', '2016-06-29 14:42:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2634', '10000002', 'text', '伍老师哪里人士？', null, null, '2016-06-29 14:44:53', '2016-06-29 14:44:53', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2635', '10000002', 'text', '讲可讲的不错 #20', null, null, '2016-06-29 14:45:13', '2016-06-29 14:45:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2636', '8', 'text', '在下湖南人，普京兄多多关照。', '10000002', '伍老师哪里人士？', '2016-06-29 14:46:05', '2016-06-29 14:46:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2637', '8', 'text', '#9', '10000002', '讲可讲的不错 #20', '2016-06-29 14:46:19', '2016-06-29 14:46:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2638', '10000002', 'text', '听投资顾问跟我讲 还有一个郑老师  怎么没看到啊 ', null, null, '2016-06-29 14:47:22', '2016-06-29 14:47:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2639', '8', 'text', 'K线最初是由日本人发明的，也叫蜡烛线，大家每日看行情的图表就是有不同的K线组合而成。', null, null, '2016-06-29 14:49:19', '2016-06-29 14:49:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2640', '10000102', 'text', '郑老师都看到你啦#19刚才还和你说话呢', null, null, '2016-06-29 14:50:18', '2016-06-29 14:50:18', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2641', '8', 'text', 'K线分两种，一种阳线，一种阴线。通常，按照中国人的习惯，红色的表示阳线，绿色的表示阴线。欧美那边则和我们的相反。', null, null, '2016-06-29 14:51:34', '2016-06-29 14:51:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2642', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/577370190a2c6.png', null, null, '2016-06-29 14:52:09', '2016-06-29 14:52:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2643', '10000102', 'text', '日本和韩国的和我们一样吗？', null, null, '2016-06-29 14:52:13', '2016-06-29 14:52:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2644', '8', 'text', '郑老师之前特意过来和普京打过招呼的#4', '10000002', '听投资顾问跟我讲 还有一个郑老师  怎么没看到啊 ', '2016-06-29 14:53:31', '2016-06-29 14:53:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2645', '8', 'text', '亚洲人一般以红色为喜庆颜色，所以亚洲大部分国家都是和我们的一样。', '10000102', '日本和韩国的和我们一样吗？', '2016-06-29 14:54:43', '2016-06-29 14:54:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2646', '10000102', 'text', '哦哦', null, null, '2016-06-29 14:55:19', '2016-06-29 14:55:19', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2647', '8', 'text', 'K线它也有不同形态，下面的图大家可以看到。', null, null, '2016-06-29 14:56:52', '2016-06-29 14:56:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2648', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773714ec9875.png', null, null, '2016-06-29 14:57:18', '2016-06-29 14:57:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2649', '8', 'text', '是的。', '10000102', '哦哦', '2016-06-29 14:57:44', '2016-06-29 14:57:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2650', '8', 'text', 'K线的不同的形态，都表示不同的含义。', null, null, '2016-06-29 14:59:00', '2016-06-29 14:59:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2651', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/577371bbdeff6.png', null, null, '2016-06-29 14:59:07', '2016-06-29 14:59:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2652', '8', 'text', '这种上下影线非常短的K线表示上涨或下跌的力度很大，后市出现同向走势的可能性是非常大的。', null, null, '2016-06-29 15:04:25', '2016-06-29 15:04:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2653', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773732f3ccb3.png', null, null, '2016-06-29 15:05:19', '2016-06-29 15:05:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2654', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/577374f3cab84.png', null, null, '2016-06-29 15:12:51', '2016-06-29 15:12:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2655', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/577375296f987.png', null, null, '2016-06-29 15:13:45', '2016-06-29 15:13:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2656', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/577375cf31fb9.png', null, null, '2016-06-29 15:16:31', '2016-06-29 15:16:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2657', '8', 'text', '大家有什么问题也可以随时提出来，我会一一为大家解答。#15', null, null, '2016-06-29 15:20:51', '2016-06-29 15:20:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2658', '8', 'text', '下面，我们继续。', null, null, '2016-06-29 15:21:33', '2016-06-29 15:21:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2659', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/57737705ac575.png', null, null, '2016-06-29 15:21:41', '2016-06-29 15:21:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2660', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/577377b6853d7.png', null, null, '2016-06-29 15:24:38', '2016-06-29 15:24:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2661', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/577378fb75725.png', null, null, '2016-06-29 15:30:03', '2016-06-29 15:30:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2662', '10000092', 'text', '#20', null, null, '2016-06-29 15:34:10', '2016-06-29 15:34:10', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2663', '8', 'text', '上面是一些常用的单独一根K线的形态及用法。下面我们讲讲多跟K线的组合用法。', null, null, '2016-06-29 15:35:09', '2016-06-29 15:35:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2664', '8', 'text', '#9', '10000092', '#20', '2016-06-29 15:35:15', '2016-06-29 15:35:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2665', '8', 'text', '由于时间问题，我们这里只讲一些常用比较重要的组合，大家想详细了解的话，给大家推荐一本讲K线的经典之作《日本蜡烛图技术》，大家有时间可以看看。好了，下面我们接着讲。', null, null, '2016-06-29 15:38:33', '2016-06-29 15:38:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2666', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/57737b17c4987.png', null, null, '2016-06-29 15:39:03', '2016-06-29 15:39:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2667', '8', 'text', '1.在上涨行情中出现:早晨之星', null, null, '2016-06-29 15:40:27', '2016-06-29 15:40:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2668', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/57737b835b18f.png', null, null, '2016-06-29 15:40:51', '2016-06-29 15:40:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2669', '8', 'text', '注意：K线组合只有在行情顶部或底部出现时，才可能出现有效反转，在其他地方不作为参考依据。', null, null, '2016-06-29 15:47:10', '2016-06-29 15:47:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2670', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/57737db972e68.png', null, null, '2016-06-29 15:50:17', '2016-06-29 15:50:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2671', '8', 'text', '2.黄昏之星', null, null, '2016-06-29 15:59:17', '2016-06-29 15:59:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2672', '8', 'text', '黄昏之星顾名思义，它和早晨之星是相反的形态。一般出现在一段行情的顶部。', null, null, '2016-06-29 16:01:22', '2016-06-29 16:01:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2673', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773806c0ad93.png', null, null, '2016-06-29 16:01:48', '2016-06-29 16:01:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2674', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773809509150.png', null, null, '2016-06-29 16:02:29', '2016-06-29 16:02:29', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2675', '8', 'text', '黄昏之星出现在行情顶部，将构成看跌信号。', null, null, '2016-06-29 16:04:29', '2016-06-29 16:04:29', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2676', '8', 'text', '好了，今天的基础课程先讲到这里，明天继续。', null, null, '2016-06-29 16:05:40', '2016-06-29 16:05:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2677', '8', 'text', '下面我们回到盘面解析。', null, null, '2016-06-29 16:05:55', '2016-06-29 16:05:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2678', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/57738202de56a.png', null, null, '2016-06-29 16:08:34', '2016-06-29 16:08:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2679', '8', 'text', '目前，上涨势头有所延续，继续震荡上行。', null, null, '2016-06-29 16:09:43', '2016-06-29 16:09:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2680', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/577384ad80ddb.png', null, null, '2016-06-29 16:19:57', '2016-06-29 16:19:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2681', '8', 'text', '美元指数持续回落，原油受提振，', null, null, '2016-06-29 16:21:52', '2016-06-29 16:21:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2682', '10000185', 'text', '老师；上涨这么强势，空单还能持有吗', null, null, '2016-06-29 16:25:02', '2016-06-29 16:25:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2683', '8', 'text', '耐心持有，单子做进去以后就要严格按计划执行，上方324-325止损设好，不要轻易动摇。', '10000185', '老师；上涨这么强势，空单还能持有吗', '2016-06-29 16:28:14', '2016-06-29 16:28:14', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2684', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/577386be4df69.png', null, null, '2016-06-29 16:28:46', '2016-06-29 16:28:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2685', '8', 'text', '手上有空单的朋友，注意关注上方压力位324-325，若出现回调，可在318-319部分减仓或先出；若强势突破，按计划止损就好，后续再找入市机会。', null, null, '2016-06-29 16:32:16', '2016-06-29 16:32:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2686', '8', 'text', '苏格兰首席部长斯特金：苏格兰坚决留在欧盟。（在布鲁塞尔讲话）\n', null, null, '2016-06-29 16:37:58', '2016-06-29 16:37:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2687', '8', 'text', '在周二欧洲议会紧急会议上，针对英国脱欧问题出现了非常戏剧化的“热闹”场面。', null, null, '2016-06-29 16:41:26', '2016-06-29 16:41:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2688', '8', 'text', '来自英国的议员、脱欧派代表人物之一、英国独立党领袖法拉奇以一己之力舌战各国议员。', null, null, '2016-06-29 16:42:50', '2016-06-29 16:42:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2689', '8', 'text', '首先挖苦他的是欧盟委员会主席容克，他先用法语发言说尊重英国的民主制度，以及公投结果。此时会场响起了零星的掌声。', null, null, '2016-06-29 16:44:49', '2016-06-29 16:44:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2690', '8', 'text', '容克对着法拉奇用英语说道：“这是你最后一次在这里鼓掌了。以及，你们一直在鼓吹脱离欧盟，英国人民也做出了决定，我不是很明白，你究竟为什么还坐在这里？！”', null, null, '2016-06-29 16:47:21', '2016-06-29 16:47:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2691', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160629/57738ba7bdc4e.jpg', null, null, '2016-06-29 16:49:43', '2016-06-29 16:49:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2692', '8', 'text', '欧洲人何苦为难欧洲人#4', null, null, '2016-06-29 16:52:08', '2016-06-29 16:52:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2693', '10000103', 'text', '#5#5', '10000002', '听投资顾问跟我讲 还有一个郑老师  怎么没看到啊 ', '2016-06-29 16:52:29', '2016-06-29 16:52:29', '-1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2694', '10000103', 'text', '#5#5这表情很逗噢', null, null, '2016-06-29 16:54:02', '2016-06-29 16:54:02', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2695', '8', 'text', '我们小罗老师已经迫不及待要跟大家见面了。#14', null, null, '2016-06-29 16:56:48', '2016-06-29 16:56:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2696', '10000103', 'text', '#6#6大家下午好', null, null, '2016-06-29 16:57:32', '2016-06-29 16:57:32', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2697', '10000103', 'text', '辛苦伍老师讲课了#9#9我也是有看的噢#1', null, null, '2016-06-29 16:59:56', '2016-06-29 16:59:56', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2698', '10000103', 'text', '大家要是有不明白的可以继续烦伍老师哒#5', null, null, '2016-06-29 17:01:20', '2016-06-29 17:01:20', '-1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2699', '10000103', 'text', '当然，也可以咨询我的#17', null, null, '2016-06-29 17:01:58', '2016-06-29 17:01:58', '-1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2700', '10000103', 'text', '日子过得好快，转眼间夏至又过去一个星期了，天气闷热的同时偶尔也伴有暴雨天气，大家出门注意随身带把雨伞喔！', null, null, '2016-06-29 17:08:04', '2016-06-29 17:08:04', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2701', '10000232', 'text', '二位老师辛苦了#9', null, null, '2016-06-29 17:09:15', '2016-06-29 17:09:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2702', '10000103', 'text', '室内外温差大，小伙伴们也要注意多喝水呢#6', null, null, '2016-06-29 17:10:37', '2016-06-29 17:10:37', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2703', '10000232', 'text', '现在应该是看晚上的行情操盘吧，老师', null, null, '2016-06-29 17:11:44', '2016-06-29 17:11:44', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2704', '10000103', 'text', '#18#18', '10000232', '二位老师辛苦了#9', '2016-06-29 17:12:29', '2016-06-29 17:12:29', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2705', '8', 'text', '现在走震荡行情，可以关注晚上的EIA数据操作。', '10000232', '现在应该是看晚上的行情操盘吧，老师', '2016-06-29 17:14:52', '2016-06-29 17:14:52', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2706', '8', 'text', '现在走震荡行情，可以关注晚上的EIA数据操作。', '10000232', '现在应该是看晚上的行情操盘吧，老师', '2016-06-29 17:15:49', '2016-06-29 17:15:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2707', '10000103', 'text', '突然想起苏轼的一首诗，相信大家肯定都知道的吧#4', null, null, '2016-06-29 17:16:14', '2016-06-29 17:16:14', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2708', '10000103', 'text', '水光潋滟晴方好, 山色空蒙雨亦奇。 ', null, null, '2016-06-29 17:16:33', '2016-06-29 17:16:33', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2709', '10000103', 'text', '#5后面那两句谁来接呢', null, null, '2016-06-29 17:17:46', '2016-06-29 17:17:46', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2710', '10000232', 'text', '欲把西湖比西子，淡妆浓抹总相宜#1', null, null, '2016-06-29 17:18:57', '2016-06-29 17:18:57', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2711', '10000103', 'text', '腻害#20#20', '10000232', '欲把西湖比西子，淡妆浓抹总相宜#1', '2016-06-29 17:19:56', '2016-06-29 17:19:56', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2712', '10000103', 'text', '   这首《饮湖上初晴后雨》是不是和现在的这个季节这个天气很配呢#17', null, null, '2016-06-29 17:23:45', '2016-06-29 17:23:45', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2713', '10000232', 'text', '苏轼看到的是杭州西湖的美，我面对的是工作与电脑，幸而二位讲的让人入醉入迷#14，该下班啦，晚上见。', null, null, '2016-06-29 17:24:28', '2016-06-29 17:24:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2714', '10000103', 'text', ' 小心防暑别太忙，注意身体保健康#9#9', '10000232', '苏轼看到的是杭州西湖的美，我面对的是工作与电脑，幸而二位讲的让人入醉入迷#14，该下班啦，晚上见。', '2016-06-29 17:26:22', '2016-06-29 17:26:22', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2715', '10000103', 'text', '#1那么大家知道西子是谁吗？原名是什么？', null, null, '2016-06-29 17:27:44', '2016-06-29 17:27:44', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2716', '10000103', 'text', '答案当然是西施，其本名是施夷光，古代越国美女，一般称其为西施，后人尊称其“西子', null, null, '2016-06-29 17:36:27', '2016-06-29 17:36:27', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2717', '10000103', 'text', '一说到工作，就想到我们大多数人都处于亚健康状态吧。', null, null, '2016-06-29 17:43:39', '2016-06-29 17:43:39', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2718', '10000103', 'text', '1、天天对着电脑；2、一坐坐一天；3、中午趴桌睡；4、晚上拖延症；5、平时少锻炼；6、周末宅家里；7、偶尔不吃饭；8、换季常感冒。你中了几枪呢？', null, null, '2016-06-29 17:44:07', '2016-06-29 17:44:07', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2719', '10000103', 'text', '我是躺着都中枪了#2所以保持健康生活不容忽视，要从现在做起！从现在做起！从现在做起！重要的事情说三遍#4', null, null, '2016-06-29 17:48:08', '2016-06-29 17:48:08', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2720', '10000103', 'text', '首先我们要精力充沛！下面有8个办法：1、享受阳光。 2、出门走走。缺少阳光，长时间在室内待着容易让人感觉疲劳。 3、充满活力地去做事。4、听听你喜欢的振奋人心的歌曲。 5、找个活力十足的朋友聊天。6、处理待办事项清单上的一件事情。7、喝杯咖啡。8、整理书桌。让人精力充沛的8个办法，总有一个适合你 #1', null, null, '2016-06-29 17:50:22', '2016-06-29 17:50:22', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2721', '10000001', 'text', '大家下午好！#9', null, null, '2016-06-29 17:50:50', '2016-06-29 17:50:50', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2722', '10000001', 'text', '大家下午好！#9', null, null, '2016-06-29 17:51:20', '2016-06-29 17:51:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2723', '10000103', 'text', '吴老师下午好呀#6', null, null, '2016-06-29 17:52:13', '2016-06-29 17:52:13', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2724', '10000001', 'text', '小罗老师好#9', null, null, '2016-06-29 17:55:07', '2016-06-29 17:55:07', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2725', '10000001', 'text', '小罗老师#9', null, null, '2016-06-29 17:55:43', '2016-06-29 17:55:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2726', '10000103', 'text', '吴老师是太久没见到罗老师了吗那么想念#5#5', null, null, '2016-06-29 18:00:52', '2016-06-29 18:00:52', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2727', '10000103', 'text', '好啦，晚间直播就辛苦吴老师了！', null, null, '2016-06-29 18:02:15', '2016-06-29 18:02:15', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2728', '10000103', 'text', '#9#9', null, null, '2016-06-29 18:02:23', '2016-06-29 18:02:23', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('2729', '10000001', 'text', '好，谢谢，我们的小罗老师，辛苦了#9', null, null, '2016-06-29 18:04:24', '2016-06-29 18:04:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2730', '10000001', 'text', '当有机会获利时，千万不要畏缩不前。当你对一笔交易有把握时，给对方致命一击', null, null, '2016-06-29 18:05:25', '2016-06-29 18:05:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2731', '10000001', 'text', '我是轩湛投资观察室的吴勇坚，接下来我来为大家做直播，大家有什么疑问或者疑惑，可以在互动区跟我交流喔。', null, null, '2016-06-29 18:05:39', '2016-06-29 18:05:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2732', '10000001', 'text', '好了，接下来看看原油的走势', null, null, '2016-06-29 18:07:37', '2016-06-29 18:07:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2733', '10000001', 'text', '昨晚美国至6月24日当周API原油库存大减386万桶，远超预期的减少237.5万桶', null, null, '2016-06-29 18:09:18', '2016-06-29 18:09:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2734', '10000001', 'text', '推升油价突破压力位320，截止今日开盘油价涨逾3%，并且站在了320上方', null, null, '2016-06-29 18:11:00', '2016-06-29 18:11:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2735', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/57739f1d7161d.png', null, null, '2016-06-29 18:12:45', '2016-06-29 18:12:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2736', '10000001', 'text', '今日开盘后，油价一直在320上方震荡盘整，多头形势较为强劲！', null, null, '2016-06-29 18:17:31', '2016-06-29 18:17:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2737', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773a08100a07.png', null, null, '2016-06-29 18:18:41', '2016-06-29 18:18:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2738', '10000194', 'text', '老师，给个建议#9', null, null, '2016-06-29 18:23:18', '2016-06-29 18:23:18', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2739', '10000001', 'text', '在数据EIA数据出来之前，预测今晚油价走势，油价将会回落到321附近后继续震荡上升', null, null, '2016-06-29 18:24:26', '2016-06-29 18:24:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2740', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773a1fc855f4.png', null, null, '2016-06-29 18:25:00', '2016-06-29 18:25:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2741', '10000001', 'text', '个人建议：可以等待油价回落到321附近，建个轻仓多单', '10000194', '老师，给个建议#9', '2016-06-29 18:26:43', '2016-06-29 18:26:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2742', '10000194', 'text', '#6嗯', null, null, '2016-06-29 18:28:36', '2016-06-29 18:28:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2743', '10000001', 'text', '今晚数据方面：', null, null, '2016-06-29 18:31:05', '2016-06-29 18:31:05', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2744', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773a3c585997.png', null, null, '2016-06-29 18:32:37', '2016-06-29 18:32:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2745', '10000001', 'text', '22:30 美国5月核心PCE物价指数年率', null, null, '2016-06-29 18:33:04', '2016-06-29 18:33:04', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2746', '10000001', 'text', '据机构调查，预测美国核心PCE通胀率将继续爬高，今明两年分别平均为1.7%和1.8%', null, null, '2016-06-29 18:34:10', '2016-06-29 18:34:10', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2747', '10000102', 'text', '我猜今晚儿EIA会跌#5', null, null, '2016-06-29 18:37:05', '2016-06-29 18:37:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2748', '10000001', 'text', '20:30 美国5月核心PCE物价指数年率', null, null, '2016-06-29 18:37:28', '2016-06-29 18:37:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2749', '10000102', 'text', '这文采也太好了', '10000232', '苏轼看到的是杭州西湖的美，我面对的是工作与电脑，幸而二位讲的让人入醉入迷#14，该下班啦，晚上见。', '2016-06-29 18:37:32', '2016-06-29 18:37:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2750', '10000001', 'text', '据机构调查，预测美国核心PCE通胀率将继续爬高，今明两年分别平均为1.7%和1.8%', null, null, '2016-06-29 18:37:41', '2016-06-29 18:37:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2751', '10000001', 'text', '今晚的重磅数据：22:30 美国至6月24日当周EIA原油库存 ！！！！！', null, null, '2016-06-29 18:38:58', '2016-06-29 18:38:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2752', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773a58104885.png', null, null, '2016-06-29 18:40:01', '2016-06-29 18:40:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2753', '10000001', 'text', '每周的EIA公布后都会引起油价的大幅波动', null, null, '2016-06-29 18:42:11', '2016-06-29 18:42:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2754', '10000001', 'text', '错过了昨晚的API，还要错过今晚的EIA吗', null, null, '2016-06-29 18:43:04', '2016-06-29 18:43:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2755', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773a641b8765.png', null, null, '2016-06-29 18:43:13', '2016-06-29 18:43:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2756', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773a7b3a7828.png', null, null, '2016-06-29 18:49:23', '2016-06-29 18:49:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2757', '10000001', 'text', '就算错过了今晚EIA，不要紧，还有明天的欧央行利率会议纪要，哈哈哈#9', null, null, '2016-06-29 18:51:19', '2016-06-29 18:51:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2758', '10000001', 'text', '明晚19:30分，欧央行将公布利率会议纪要', null, null, '2016-06-29 18:52:00', '2016-06-29 18:52:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2759', '10000001', 'text', '上一次会议纪要在2016年5月19日', null, null, '2016-06-29 18:53:43', '2016-06-29 18:53:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2760', '10000001', 'text', '当天的会议纪要利好欧元和原油，带动原油从低位反弹3%，收益空间100%！', null, null, '2016-06-29 18:54:03', '2016-06-29 18:54:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2761', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773a92727443.png', null, null, '2016-06-29 18:55:35', '2016-06-29 18:55:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2762', '10000001', 'text', '鉴于恰逢上周英国意外退欧', null, null, '2016-06-29 19:01:44', '2016-06-29 19:01:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2763', '10000001', 'text', '同时今年欧洲经济复苏非常缓慢，加上还有叙利亚难民危机和恐怖袭击威胁，整个欧洲经济可以说是摇摇欲坠', null, null, '2016-06-29 19:02:04', '2016-06-29 19:02:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2764', '10000001', 'text', '所以明天的欧洲利率会议纪要出现意外的官员观点，将会对欧元和原油产生较大的波动！！', null, null, '2016-06-29 19:02:46', '2016-06-29 19:02:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2765', '10000001', 'text', '那么将会给大家带来更大的收益空间喔#14', null, null, '2016-06-29 19:04:35', '2016-06-29 19:04:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2766', '10000001', 'text', '就算再错过了欧洲的央行的利率会议纪要', null, null, '2016-06-29 19:12:43', '2016-06-29 19:12:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2767', '10000001', 'text', '不要紧', null, null, '2016-06-29 19:12:55', '2016-06-29 19:12:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2768', '10000006', 'text', '#20', null, null, '2016-06-29 19:13:56', '2016-06-29 19:13:56', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2769', '10000001', 'text', '因为还有英国退欧后引起经济下行的担忧，引发的连锁效应将会恶化国际金融市场环境也会引起油价的波动', null, null, '2016-06-29 19:15:19', '2016-06-29 19:15:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2770', '10000001', 'text', '好了，我们回到原油走势方面', null, null, '2016-06-29 19:20:31', '2016-06-29 19:20:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2771', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773af1f672ec.png', null, null, '2016-06-29 19:21:03', '2016-06-29 19:21:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2772', '10000001', 'text', '现在油价开始小幅回落了', null, null, '2016-06-29 19:21:25', '2016-06-29 19:21:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2773', '10000010', 'text', '#1', null, null, '2016-06-29 19:24:41', '2016-06-29 19:24:41', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2774', '10000001', 'text', '下探目前的短期支撑位320', null, null, '2016-06-29 19:26:11', '2016-06-29 19:26:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2775', '10000001', 'text', '摩根大通：基本情况预测苏格兰会独立且会有新的苏格兰货币。', null, null, '2016-06-29 19:33:10', '2016-06-29 19:33:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2776', '10000001', 'text', '所以，如果英格兰进行退英公投，又将会引起市场的波动', null, null, '2016-06-29 19:34:42', '2016-06-29 19:34:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2777', '10000194', 'text', '坐等数据出来，在搞一波#4#4#4', null, null, '2016-06-29 19:42:18', '2016-06-29 19:42:18', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2778', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773b42c4b1f5.png', null, null, '2016-06-29 19:42:36', '2016-06-29 19:42:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2779', '10000001', 'text', '支撑位轻仓短多', null, null, '2016-06-29 19:43:48', '2016-06-29 19:43:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2780', '10000194', 'text', '#9#9#9那再来一小波#4', null, null, '2016-06-29 19:46:11', '2016-06-29 19:46:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2781', '10000001', 'text', '挪威国家石油公司：英国脱欧不太可能影响长期油价和需求', null, null, '2016-06-29 19:51:34', '2016-06-29 19:51:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2782', '10000001', 'text', '挪威国家石油公司：但会在短期内对油价产生不确定性', null, null, '2016-06-29 19:59:15', '2016-06-29 19:59:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2783', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773b8ea7e924.png', null, null, '2016-06-29 20:02:50', '2016-06-29 20:02:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2784', '10000009', 'text', '现在也是等数据了', null, null, '2016-06-29 20:03:50', '2016-06-29 20:03:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2785', '10000001', 'text', '上图看出今日油价一直在320-323区间震荡', null, null, '2016-06-29 20:04:34', '2016-06-29 20:04:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2786', '10000001', 'text', '所以，如果今晚的22:30的EIA利多油价，那么油价将会上探都329附近', null, null, '2016-06-29 20:09:37', '2016-06-29 20:09:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2787', '10000194', 'text', '可按老师的先轻轻建一点哦', '10000009', '现在也是等数据了', '2016-06-29 20:11:38', '2016-06-29 20:11:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2788', '10000001', 'text', '如果今晚TIA数据利空，那么油价将有可能下探至310附近', null, null, '2016-06-29 20:15:58', '2016-06-29 20:15:58', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2789', '10000001', 'text', ' 如果今晚EIA数据利空，那么油价将有可能下探至310附近', null, null, '2016-06-29 20:16:22', '2016-06-29 20:16:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2790', '10000001', 'text', '北京时间20:30将公布：美国5月核心PCE物价指数月率及美国5月核心PCE物价指数年率。', null, null, '2016-06-29 20:23:52', '2016-06-29 20:23:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2791', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773bdf666096.png', null, null, '2016-06-29 20:24:22', '2016-06-29 20:24:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2792', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773bf87ca76d.png', null, null, '2016-06-29 20:31:03', '2016-06-29 20:31:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2793', '10000001', 'text', '数据达到预期值', null, null, '2016-06-29 20:31:51', '2016-06-29 20:31:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2794', '10000001', 'text', '对油价影响较小', null, null, '2016-06-29 20:32:28', '2016-06-29 20:32:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2795', '10000004', 'text', '跟了吴老师的单~现在开始小赚了#9', null, null, '2016-06-29 20:34:19', '2016-06-29 20:34:19', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2796', '10000252', 'text', '老师好', null, null, '2016-06-29 20:34:43', '2016-06-29 20:34:43', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2797', '10000001', 'text', '#14', '10000004', '跟了吴老师的单~现在开始小赚了#9', '2016-06-29 20:35:47', '2016-06-29 20:35:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2798', '10000252', 'text', '', null, null, '2016-06-29 20:37:42', '2016-06-29 20:37:42', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2799', '10000001', 'text', '#9', '10000252', '老师好', '2016-06-29 20:38:27', '2016-06-29 20:38:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2800', '10000252', 'text', '刚才可以做单吗？', null, null, '2016-06-29 20:39:32', '2016-06-29 20:39:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2801', '10000001', 'text', '据路透援引消息人士：如果市场保持平静，欧洲央行在英国脱欧公投后不急于放宽政策。', null, null, '2016-06-29 20:39:54', '2016-06-29 20:39:54', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2802', '10000001', 'text', '是可以轻仓短多的等待EIA数据公布，目前还是多头形势', '10000252', '刚才可以做单吗？', '2016-06-29 20:41:53', '2016-06-29 20:41:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2803', '10000252', 'text', '谢谢！', null, null, '2016-06-29 20:43:16', '2016-06-29 20:43:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2804', '10000001', 'text', '据路透援引消息人士：欧洲央行要等到出现英国脱欧公投后经济放缓的确凿证据，才会采取行动', null, null, '2016-06-29 20:43:16', '2016-06-29 20:43:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2805', '10000001', 'text', '据路透：若英国脱欧造成金融动荡，或导致企业延迟或减小资本支出，令商业投资遭受下行压力；预计未来6个季度英国脱欧将令拉低美国GDP增速0.2%', null, null, '2016-06-29 20:49:17', '2016-06-29 20:49:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2806', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773c59309e7f.png', null, null, '2016-06-29 20:56:51', '2016-06-29 20:56:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2807', '10000001', 'text', '看来原油在EIA数据出来之前，行情还是比较清淡啊', null, null, '2016-06-29 20:57:38', '2016-06-29 20:57:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2808', '10000001', 'text', '瑞典银行经济学家：目前欧洲央行是欧元区经济唯一的支撑，预计欧洲央行会将QE期限延长', null, null, '2016-06-29 21:04:43', '2016-06-29 21:04:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2809', '10000002', 'text', '各位朋友大家晚上好  老师晚上好  我来了 ', null, null, '2016-06-29 21:08:32', '2016-06-29 21:08:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2810', '10000001', 'text', '所以未来将为油价提供支撑', null, null, '2016-06-29 21:09:14', '2016-06-29 21:09:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2811', '10000002', 'text', '英国退欧了也好   能自食其力 说明人家有能 ', null, null, '2016-06-29 21:14:47', '2016-06-29 21:14:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2812', '10000002', 'text', '今晚EIA个人觉得 都可以提前布局了 ', null, null, '2016-06-29 21:16:46', '2016-06-29 21:16:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2813', '10000001', 'text', '油价目前还在震荡等待EIA数据', null, null, '2016-06-29 21:17:38', '2016-06-29 21:17:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2814', '10000001', 'text', '最好等待数据出来再布局，因为有时候EIA原油库存利空，汽油库存利多，会造成油价来回波动的', '10000002', '今晚EIA个人觉得 都可以提前布局了 ', '2016-06-29 21:19:41', '2016-06-29 21:19:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2815', '10000102', 'text', '怎么布局？', '10000002', '今晚EIA个人觉得 都可以提前布局了 ', '2016-06-29 21:20:35', '2016-06-29 21:20:35', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2816', '10000197', 'text', ' 在一块布上写个局#4', '10000102', '怎么布局？', '2016-06-29 21:23:42', '2016-06-29 21:23:42', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2817', '10000002', 'text', '#20#20#20', '10000197', ' 在一块布上写个局#4', '2016-06-29 21:24:20', '2016-06-29 21:24:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2818', '10000001', 'text', '若今晚EIA原油库存录得下滑，将是连续6周下滑', null, null, '2016-06-29 21:25:55', '2016-06-29 21:25:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2819', '10000001', 'text', '这将利好供应过剩仍旧存在的原油市场', null, null, '2016-06-29 21:29:42', '2016-06-29 21:29:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2820', '10000001', 'text', '挪威石油工人罢工愈演愈烈', null, null, '2016-06-29 21:35:43', '2016-06-29 21:35:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2821', '10000001', 'text', '挪威官方表示，如果此次演变成一场旷日持久的罢工，罢工的工人人数将超过7400人', null, null, '2016-06-29 21:35:52', '2016-06-29 21:35:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2822', '10000001', 'text', '挪威石油工人表示如果周五截止日期前未达成薪资协议，那么将会进行罢工', null, null, '2016-06-29 21:42:15', '2016-06-29 21:42:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2823', '10000001', 'text', '那么将会继续提振油价', null, null, '2016-06-29 21:42:45', '2016-06-29 21:42:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2824', '10000001', 'text', '沙特目前的国内日产量已经跟不上出口和国内消耗的量', null, null, '2016-06-29 21:47:07', '2016-06-29 21:47:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2825', '10000194', 'text', '#4这价格震得头晕', null, null, '2016-06-29 21:47:13', '2016-06-29 21:47:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2826', '10000001', 'text', '胜利的道路是迂回曲折的#17', '10000194', '#4这价格震得头晕', '2016-06-29 21:49:25', '2016-06-29 21:49:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2827', '10000194', 'text', '老师一会我先出单，等十点半数据在来OK吗？', null, null, '2016-06-29 21:51:27', '2016-06-29 21:51:27', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2828', '10000001', 'text', '可以啊#9', '10000194', '老师一会我先出单，等十点半数据在来OK吗？', '2016-06-29 21:52:01', '2016-06-29 21:52:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2829', '10000194', 'text', '#6够买个糖超吃了#17', null, null, '2016-06-29 21:53:10', '2016-06-29 21:53:10', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2830', '10000001', 'text', '#17', '10000194', '#6够买个糖超吃了#17', '2016-06-29 21:54:03', '2016-06-29 21:54:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2831', '10000001', 'text', '根据长期监测沙特原油数据的机构的最新数据显示：第一次出现沙特原油库存连续6个月下降', null, null, '2016-06-29 21:54:25', '2016-06-29 21:54:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2832', '10000000', 'text', '大家晚上好！吴老师晚上好！', null, null, '2016-06-29 21:58:52', '2016-06-29 21:58:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2833', '10000001', 'text', '郑老师好#14', null, null, '2016-06-29 21:59:12', '2016-06-29 21:59:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2834', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773d47409991.jpg', null, null, '2016-06-29 22:00:20', '2016-06-29 22:00:20', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2835', '10000194', 'text', '#14#14#14', null, null, '2016-06-29 22:00:28', '2016-06-29 22:00:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2836', '10000000', 'text', '稍后十点将公布今晚的重头戏：EIA！大家准备好子弹了吗#4', null, null, '2016-06-29 22:00:46', '2016-06-29 22:00:46', '-1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2837', '10000000', 'text', '稍后十点半将公布今晚的重头戏：EIA！大家准备好子弹了吗', null, null, '2016-06-29 22:01:23', '2016-06-29 22:01:23', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2838', '10000000', 'text', '#4', null, null, '2016-06-29 22:01:29', '2016-06-29 22:01:29', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2839', '10000000', 'text', '那么下面我们来分析一下数据', null, null, '2016-06-29 22:01:42', '2016-06-29 22:01:42', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2840', '10000194', 'text', '#4时刻准备着', null, null, '2016-06-29 22:01:43', '2016-06-29 22:01:43', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2841', '10000000', 'text', '昨晚的API是小幅利多，那么昨晚和今早市场已经走出了一个利多的走势', null, null, '2016-06-29 22:02:19', '2016-06-29 22:02:19', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2842', '10000000', 'text', '那么今晚除非EIA是大幅低于预期，否则今晚上涨的空间可能会有限。而如果今晚突发利空，那么原油回落的空间将较大', null, null, '2016-06-29 22:03:26', '2016-06-29 22:03:26', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2843', '10000000', 'text', '所以今晚我们的操作建议是：以-400为分水岭。如果数据结果低于-400（例如-450），那么我们将追多操作；如果介于-236~-400之间，那么我们待小幅拉升后短空操作；如果低于-236，则追空操作', null, null, '2016-06-29 22:05:07', '2016-06-29 22:05:07', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2844', '10000000', 'text', '为什么我们选择-400作为分水岭？因为近两三个月，EIA最大的下降幅度是-400多，所以如果能达到这个峰值以上，才能对今晚的原油继续利多，因为今天白天市场已经消化了不少库存数据利多的影响，因为API和EIA有很大的关联性', null, null, '2016-06-29 22:06:42', '2016-06-29 22:06:42', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2845', '10000000', 'text', '所以大家今晚除了要和预期值比以外，还要跟昨晚的API做对比', null, null, '2016-06-29 22:07:08', '2016-06-29 22:07:08', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2846', '10000000', 'text', '今晚我们的操作建议是：1. 以-400为分水岭。如果数据结果低于-400（例如-450），那么我们将追多操作', null, null, '2016-06-29 22:07:29', '2016-06-29 22:07:29', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2847', '10000000', 'text', '2. 如果介于-236~-400之间，那么我们待小幅拉升后短空操作', null, null, '2016-06-29 22:07:42', '2016-06-29 22:07:42', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2848', '10000000', 'text', '3. 如果低于-236，则追空操作', null, null, '2016-06-29 22:07:54', '2016-06-29 22:07:54', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2849', '5', 'text', '老师，吴老师多单要平掉么？', null, null, '2016-06-29 22:09:13', '2016-06-29 22:09:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2850', '10000000', 'text', '如果你有盈利，可以先在EIA出来之前有盈利先走', '5', '老师，吴老师多单要平掉么？', '2016-06-29 22:11:06', '2016-06-29 22:11:06', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2851', '10000000', 'text', '待数据出来后再做', null, null, '2016-06-29 22:11:15', '2016-06-29 22:11:15', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2852', '10000000', 'text', '这样相对来说稳健一点', null, null, '2016-06-29 22:11:30', '2016-06-29 22:11:30', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2853', '10000002', 'text', '久仰郑老师大名  晚上好 郑老师 ', null, null, '2016-06-29 22:11:51', '2016-06-29 22:11:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2854', '10000000', 'text', '普大帝好#4', '10000002', '久仰郑老师大名  晚上好 郑老师 ', '2016-06-29 22:12:05', '2016-06-29 22:12:05', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2855', '10000187', 'text', '赚了个夜宵钱还不错#14', null, null, '2016-06-29 22:12:12', '2016-06-29 22:12:12', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2856', '10000000', 'text', '看看吴老师的多单能否在EIA出来前顺利止盈', null, null, '2016-06-29 22:12:26', '2016-06-29 22:12:26', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2857', '10000002', 'text', '我在320做了多单  ', null, null, '2016-06-29 22:12:56', '2016-06-29 22:12:56', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2858', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773d771b5a46.png', '5', '老师，吴老师多单要平掉么？', '2016-06-29 22:13:05', '2016-06-29 22:13:05', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2859', '10000037', 'text', '止盈了', null, null, '2016-06-29 22:13:11', '2016-06-29 22:13:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2860', '10000037', 'text', '323.5了', null, null, '2016-06-29 22:13:23', '2016-06-29 22:13:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2861', '10000000', 'text', '吴老师的多单顺利盈利了，大家跟上了没？#14', null, null, '2016-06-29 22:13:32', '2016-06-29 22:13:32', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2862', '10000002', 'text', '我认为今晚利多的概率比较大 ', null, null, '2016-06-29 22:13:38', '2016-06-29 22:13:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2863', '10000002', 'text', '所以9点的时候在320做了多单 ', null, null, '2016-06-29 22:14:10', '2016-06-29 22:14:10', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2864', '10000002', 'text', '吴老师的多单也很给力 #20', null, null, '2016-06-29 22:14:31', '2016-06-29 22:14:31', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2865', '10000052', 'text', '已经平了#9', null, null, '2016-06-29 22:14:59', '2016-06-29 22:14:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2866', '10000102', 'text', '没跟上#16', null, null, '2016-06-29 22:15:28', '2016-06-29 22:15:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2867', '10000000', 'text', '我们直播室开播一个星期，收益率已经翻了近三倍了，大家要紧跟我们的精英分析师团队啊', null, null, '2016-06-29 22:15:38', '2016-06-29 22:15:38', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2868', '10000000', 'text', '数据公布前，原油走势有点强势', null, null, '2016-06-29 22:17:13', '2016-06-29 22:17:13', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2869', '10000102', 'text', '哈哈哈哈', '10000197', ' 在一块布上写个局#4', '2016-06-29 22:18:47', '2016-06-29 22:18:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2870', '10000006', 'text', '#9', null, null, '2016-06-29 22:19:07', '2016-06-29 22:19:07', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2871', '10000002', 'text', '关心+听从=郑  三人行+驷马=杰  郑杰老师果然名不虚传 #20', null, null, '2016-06-29 22:20:09', '2016-06-29 22:20:09', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2872', '10000000', 'text', '轩湛投资观察室很高兴能为大家服务，我们将为大家提供最好的服务#15', '10000002', '关心+听从=郑  三人行+驷马=杰  郑杰老师果然名不虚传 #20', '2016-06-29 22:22:20', '2016-06-29 22:22:20', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2873', '10000000', 'text', '还有五分钟就公布数据', null, null, '2016-06-29 22:24:49', '2016-06-29 22:24:49', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2874', '10000102', 'text', '郑老师，好久不见！#6', null, null, '2016-06-29 22:25:23', '2016-06-29 22:25:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2875', '10000026', 'text', '郑老师好', null, null, '2016-06-29 22:25:46', '2016-06-29 22:25:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2876', '10000000', 'text', '您好', '10000026', '郑老师好', '2016-06-29 22:26:41', '2016-06-29 22:26:41', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2877', '10000026', 'text', '今天涨的机会更大吧?', null, null, '2016-06-29 22:28:20', '2016-06-29 22:28:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2878', '10000000', 'text', '从结构来看，目前偏强', '10000026', '今天涨的机会更大吧?', '2016-06-29 22:29:01', '2016-06-29 22:29:01', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2879', '10000077', 'text', '好紧张#13', null, null, '2016-06-29 22:29:14', '2016-06-29 22:29:14', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2880', '10000000', 'text', '从结构来看，目前偏强。但还是要关注10点半的数据', '10000026', '今天涨的机会更大吧?', '2016-06-29 22:29:22', '2016-06-29 22:29:22', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2881', '10000026', 'text', '好期待！', null, null, '2016-06-29 22:29:27', '2016-06-29 22:29:27', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2882', '10000000', 'text', '从结构来看，目前偏强。但还是要关注10点半的数据', '10000026', '今天涨的机会更大吧?', '2016-06-29 22:29:33', '2016-06-29 22:29:33', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2883', '10000002', 'text', '激动人心的时刻到了 ', null, null, '2016-06-29 22:30:02', '2016-06-29 22:30:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2884', '10000000', 'text', '数据利多。', null, null, '2016-06-29 22:30:09', '2016-06-29 22:30:09', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2885', '10000000', 'text', '但是行情很不稳定', null, null, '2016-06-29 22:30:26', '2016-06-29 22:30:26', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2886', '10000000', 'text', '数据利多，但是行情瞬间下坡', null, null, '2016-06-29 22:30:52', '2016-06-29 22:30:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2887', '10000009', 'text', '静待老师指令！', null, null, '2016-06-29 22:31:17', '2016-06-29 22:31:17', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2888', '10000000', 'text', '行情砸不下去，我们做多！', null, null, '2016-06-29 22:31:25', '2016-06-29 22:31:25', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2889', '10000000', 'text', '行情砸不下去，我们做多！', null, null, '2016-06-29 22:32:26', '2016-06-29 22:32:26', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2890', '5', 'text', '#10这行情太妖孽了', null, null, '2016-06-29 22:32:31', '2016-06-29 22:32:31', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2891', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773dc152b7b0.jpg', null, null, '2016-06-29 22:32:53', '2016-06-29 22:32:53', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2892', '10000006', 'text', '老师，怎么做', null, null, '2016-06-29 22:33:25', '2016-06-29 22:33:25', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2893', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773dc67d16be.png', null, null, '2016-06-29 22:34:15', '2016-06-29 22:34:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2894', '10000001', 'text', '数据利多油价', null, null, '2016-06-29 22:34:32', '2016-06-29 22:34:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2895', '10000000', 'text', '做多，数据利多', '10000006', '老师，怎么做', '2016-06-29 22:35:02', '2016-06-29 22:35:02', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2896', '10000001', 'text', '大幅低于预期', null, null, '2016-06-29 22:35:10', '2016-06-29 22:35:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2897', '10000003', 'text', '太完美了...又赚了#20', null, null, '2016-06-29 22:35:24', '2016-06-29 22:35:24', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2898', '10000000', 'text', '行情很不稳定，多单有利就走', null, null, '2016-06-29 22:36:03', '2016-06-29 22:36:03', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2899', '10000194', 'text', '错几个亿了，等下来在搞#4', '10000006', '老师，怎么做', '2016-06-29 22:37:04', '2016-06-29 22:37:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2900', '10000026', 'text', '赚了就跑！等下再来！', null, null, '2016-06-29 22:39:02', '2016-06-29 22:39:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2901', '10000002', 'text', '我的多单果然没错  多谢郑老师的建议 ', null, null, '2016-06-29 22:39:23', '2016-06-29 22:39:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2902', '10000004', 'text', '#9#9', null, null, '2016-06-29 22:39:44', '2016-06-29 22:39:44', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2903', '10000001', 'text', '今晚EIA原油库存连续六周录得下滑', null, null, '2016-06-29 22:40:04', '2016-06-29 22:40:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2904', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773dddbd6988.jpg', null, null, '2016-06-29 22:40:27', '2016-06-29 22:40:27', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2905', '10000004', 'text', '老师我321布局的多单什么价位平仓#6', null, null, '2016-06-29 22:40:31', '2016-06-29 22:40:31', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2906', '10000000', 'text', '现在有利可以走', '10000004', '老师我321布局的多单什么价位平仓#6', '2016-06-29 22:40:40', '2016-06-29 22:40:40', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2907', '10000002', 'text', '我也出了  ', null, null, '2016-06-29 22:40:56', '2016-06-29 22:40:56', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2908', '10000002', 'text', '等回调有机会在继续 ', null, null, '2016-06-29 22:41:16', '2016-06-29 22:41:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2909', '10000000', 'text', '很高兴，又一晚带领大家把握住了EIA的疯狂#4', null, null, '2016-06-29 22:41:20', '2016-06-29 22:41:20', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2910', '10000000', 'text', '下面把时间交回给伍老师', null, null, '2016-06-29 22:41:36', '2016-06-29 22:41:36', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2911', '10000001', 'text', '谢谢郑老师，辛苦了#9', null, null, '2016-06-29 22:42:49', '2016-06-29 22:42:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2912', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773de849db1b.jpg', null, null, '2016-06-29 22:43:16', '2016-06-29 22:43:16', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('2913', '10000194', 'text', '没抢到#11', null, null, '2016-06-29 22:43:47', '2016-06-29 22:43:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2914', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773df26e2c6b.png', null, null, '2016-06-29 22:45:58', '2016-06-29 22:45:58', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2915', '10000001', 'text', '美国EIA汽油库存增幅为5月20日当周以来最大', null, null, '2016-06-29 22:46:20', '2016-06-29 22:46:20', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2916', '1', 'text', '伍老师的单子要是不止盈，现在应该能赚的更多啊', null, null, '2016-06-29 22:46:45', '2016-06-29 22:46:45', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2917', '10000001', 'text', '今晚美国EIA汽油库存增幅为5月20日当周以来最大', null, null, '2016-06-29 22:46:53', '2016-06-29 22:46:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2918', '10000001', 'text', '今晚EIA原油库存是很出乎市场的预料的，因为全球各大机构预测够不低于-250', '1', '伍老师的单子要是不止盈，现在应该能赚的更多啊', '2016-06-29 22:48:43', '2016-06-29 22:48:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2919', '10000194', 'text', '数据没出来就跑了，我胆子小#4', '1', '伍老师的单子要是不止盈，现在应该能赚的更多啊', '2016-06-29 22:50:25', '2016-06-29 22:50:25', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2920', '10000001', 'text', '今晚多单没拿到利润的，不着急，我们还可以等待油价到压力位来一单短空获利', null, null, '2016-06-29 22:51:57', '2016-06-29 22:51:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2921', '10000102', 'text', '现在还可以做多吗？', null, null, '2016-06-29 22:55:12', '2016-06-29 22:55:12', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2922', '10000194', 'text', '不敢下手现在#13', '10000102', '现在还可以做多吗？', '2016-06-29 22:56:09', '2016-06-29 22:56:09', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2923', '10000001', 'text', '个人建议：现在价格不适合进场', '10000102', '现在还可以做多吗？', '2016-06-29 22:56:32', '2016-06-29 22:56:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2924', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773e414bf280.png', null, null, '2016-06-29 23:07:00', '2016-06-29 23:07:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2925', '10000001', 'text', '从图看出油价日内涨幅已经达到3%', null, null, '2016-06-29 23:07:19', '2016-06-29 23:07:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2926', '10000001', 'text', '加上昨日，这两天油价已经涨了6%多', null, null, '2016-06-29 23:09:36', '2016-06-29 23:09:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2927', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160629/5773e4cba816c.png', null, null, '2016-06-29 23:10:03', '2016-06-29 23:10:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2928', '10000001', 'text', '所以预测后半夜走势将会冲到329附近后回落', null, null, '2016-06-29 23:11:31', '2016-06-29 23:11:31', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2929', '10000001', 'text', ' 所以预测后半夜走势将会冲到329压力位附近后回落', null, null, '2016-06-29 23:12:13', '2016-06-29 23:12:13', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2930', '10000001', 'text', '所以预测后半夜走势将会冲到329压力位附近后回落', null, null, '2016-06-29 23:12:30', '2016-06-29 23:12:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2931', '10000194', 'text', '#2重要的事说三遍#4', null, null, '2016-06-29 23:13:01', '2016-06-29 23:13:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2932', '10000194', 'text', '老师一会有机会做单喊一下#21', null, null, '2016-06-29 23:20:49', '2016-06-29 23:20:49', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2933', '10000001', 'text', '好的#9', '10000194', '老师一会有机会做单喊一下#21', '2016-06-29 23:21:21', '2016-06-29 23:21:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2934', '10000001', 'text', '美国原油产量环比下降0.63%，为两个月以来最大降幅，同时触及2014年9月以来新低', null, null, '2016-06-29 23:28:02', '2016-06-29 23:28:02', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2935', '10000001', 'text', '英国“脱欧”的担忧情绪有所缓和，今晚英国富时100指数抹去英国脱欧公投后的全部跌幅', null, null, '2016-06-29 23:34:19', '2016-06-29 23:34:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2936', '10000001', 'text', '高盛表示目前钻井数显示美国石油产量将继续下降', null, null, '2016-06-29 23:44:18', '2016-06-29 23:44:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2937', '10000001', 'text', '高盛：预计今明两年美国产量同比平均下降66万桶/日和38万桶/日', null, null, '2016-06-29 23:53:47', '2016-06-29 23:53:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2938', '10000001', 'text', '目前油价震荡上升，测试上方压力位329-330区间', null, null, '2016-06-30 00:03:03', '2016-06-30 00:03:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2939', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5773f330b0acc.png', null, null, '2016-06-30 00:11:28', '2016-06-30 00:11:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2940', '10000170', 'text', '老师直播到几点', null, null, '2016-06-30 00:13:39', '2016-06-30 00:13:39', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2941', '10000001', 'text', '我们直播室为大家直播行情到1:30#9', '10000170', '老师直播到几点', '2016-06-30 00:15:49', '2016-06-30 00:15:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2942', '10000001', 'text', '尼日利亚石油资源部长周一表示与武装分子的停火已经开始，7月底尼日利亚的原油产量最高可达到每日220万桶', null, null, '2016-06-30 00:25:01', '2016-06-30 00:25:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2943', '10000170', 'text', '今晚空单做不了了', null, null, '2016-06-30 00:32:34', '2016-06-30 00:32:34', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2944', '10000170', 'text', '利空原油不', null, null, '2016-06-30 00:33:31', '2016-06-30 00:33:31', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2945', '10000001', 'text', '目前还没上破到压力位，建议先等待', '10000170', '今晚空单做不了了', '2016-06-30 00:34:00', '2016-06-30 00:34:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2946', '10000001', 'text', '目前的消息面是利多油价的', '10000170', '利空原油不', '2016-06-30 00:34:38', '2016-06-30 00:34:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2947', '10000001', 'text', '目前油价继续上探压力区间330-329', null, null, '2016-06-30 00:45:16', '2016-06-30 00:45:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2948', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5773fc6a5cfc4.png', null, null, '2016-06-30 00:50:50', '2016-06-30 00:50:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2949', '10000001', 'text', '油价有所回落盘整', null, null, '2016-06-30 00:51:06', '2016-06-30 00:51:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2950', '10000001', 'text', '鉴于今晚EIA原油库存减少405.3万桶，超出预期的减少236.5万桶', null, null, '2016-06-30 00:59:01', '2016-06-30 00:59:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2951', '10000001', 'text', '如果今晚油价冲高到压力位329附近没有回落，那么明天油价的走势将会是继续震荡上升', null, null, '2016-06-30 01:03:50', '2016-06-30 01:03:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2952', '10000001', 'text', '明天关注的重要事件：欧洲央行公布6月货币政策会议纪要', null, null, '2016-06-30 01:11:59', '2016-06-30 01:11:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2953', '10000001', 'text', '据统计，欧洲央行公布货币政策会议纪要引起油价的波幅平均达到6%', null, null, '2016-06-30 01:16:18', '2016-06-30 01:16:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2954', '10000001', 'text', '所以，错过了今天晚上的EIA不要紧，还有明天的欧洲央行货币政策会议纪要', null, null, '2016-06-30 01:21:48', '2016-06-30 01:21:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2955', '10000001', 'text', '好了，今天晚上的直播到这里了', null, null, '2016-06-30 01:32:30', '2016-06-30 01:32:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2956', '10000001', 'text', '谢谢大家的观看#9', null, null, '2016-06-30 01:32:41', '2016-06-30 01:32:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2957', '10000001', 'text', '大家晚安#9', null, null, '2016-06-30 01:32:55', '2016-06-30 01:32:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('2958', '8', 'text', '朋友们早上好！#15', null, null, '2016-06-30 09:27:44', '2016-06-30 09:27:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2959', '8', 'text', '有的人总认为全天下都应该懂自己，但其实人家真的没必要去懂你 ，所以人要学会看轻自己，少一些自我，多一些换位，才能快乐。所谓心有多大，快乐就有多少；包容越多，得到越多。抱着一颗淡然的心去面对一切，心静才能听到万物的声音，心清才能看到万物的本质。沉淀自己的心，静观事态变迁。与人相处，需要讲究方式方法。有些事，需忍，勿怒；有些人，需让，勿究。生活不是战场，无需一较高下。生活正是道场，历境修心。', null, null, '2016-06-30 09:29:38', '2016-06-30 09:29:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2960', '8', 'text', '按照惯例，咱们首先回顾下昨日晚间的行情数据。', null, null, '2016-06-30 09:32:20', '2016-06-30 09:32:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2961', '10000004', 'text', '#9', null, null, '2016-06-30 09:32:30', '2016-06-30 09:32:30', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2962', '8', 'text', '#4', '10000004', '#9', '2016-06-30 09:32:53', '2016-06-30 09:32:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2963', '8', 'text', '昨晚EIA数据美国上周库存降幅大于预期，为油价反弹提供强劲推力，油价周三急升4%。', null, null, '2016-06-30 09:35:49', '2016-06-30 09:35:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2964', '8', 'text', '美国能源资料协会(EIA)报告，截至6月24日当周，美国原油库存减少410万桶，为连续第六周减少，降幅超过接受路透调查的分析师预期的240万桶。', null, null, '2016-06-30 09:37:59', '2016-06-30 09:37:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2965', '8', 'text', '尽管如此，油市长期前景不宜过度看多，因EIA报告还显示，汽油库存不合时宜地增加140万桶，大幅超过预估为减少5.8万桶。', null, null, '2016-06-30 09:42:02', '2016-06-30 09:42:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2966', '8', 'text', '而英国退欧忧虑缓解，挪威石油工人可能举行罢工，以及委内瑞拉能源行业的危机也助燃油市的涨势。', null, null, '2016-06-30 09:45:02', '2016-06-30 09:45:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2967', '8', 'text', '加上美元回落也为油价提供了一定支撑。', null, null, '2016-06-30 09:47:18', '2016-06-30 09:47:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2968', '8', 'text', '油价连涨两日，自周一结算以来累积攀升近8%，几乎完全收复了英国脱欧公投结果公布后录得的跌幅。', null, null, '2016-06-30 09:54:16', '2016-06-30 09:54:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2969', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/57747c44b160c.png', null, null, '2016-06-30 09:56:20', '2016-06-30 09:56:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2970', '8', 'text', '近期基本面消息对多头比较有利，目前反弹力度依然强劲，并没有明显的空头信号。', '10000170', '利空原油不', '2016-06-30 09:59:37', '2016-06-30 09:59:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2971', '8', 'text', '下面一起看看今天的股市。', null, null, '2016-06-30 10:01:26', '2016-06-30 10:01:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2972', '8', 'text', '隔夜欧美股市继续大幅反弹。', null, null, '2016-06-30 10:02:16', '2016-06-30 10:02:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2973', '8', 'text', '美股：道指涨284.96点，报17,694.68点，涨幅为1.64%；标普500指数涨34.68点，报2,070.77点，涨幅为1.70%；纳指涨87.38点，报4,779.25点，涨幅为1.86%。', null, null, '2016-06-30 10:02:47', '2016-06-30 10:02:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2974', '8', 'text', '欧股：泛欧绩优股FTSEurofirst 300指数上扬3.06%。英股富时100指数劲升3.58%，报6,360.06点，法股CAC 40指数攀升2.6%，收报4,195.32点。德国DAX指数上升1.75%，收报9,612.27点。', null, null, '2016-06-30 10:03:06', '2016-06-30 10:03:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2975', '8', 'text', '美元：美元指数下跌0.40%，报95.6756点。', null, null, '2016-06-30 10:03:31', '2016-06-30 10:03:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2976', '8', 'text', '今日沪深两市涨跌互现。', null, null, '2016-06-30 10:12:30', '2016-06-30 10:12:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2977', '8', 'text', '沪指平开后小幅上行，无奈次新股、稀土永磁板块低迷，拖累大盘，三大股指相继翻绿。', null, null, '2016-06-30 10:13:38', '2016-06-30 10:13:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2978', '8', 'text', '　盘面上，酿酒板块、量子通信领涨两市，次新股跌幅居前。', null, null, '2016-06-30 10:14:04', '2016-06-30 10:14:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2979', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/57748184cac68.png', null, null, '2016-06-30 10:18:44', '2016-06-30 10:18:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2980', '8', 'text', '目前沪指上方压力区在2950 点区域，创业板指数压力区在2240点一线，短线向上突破的可能性还是比较大，不过要消化上方压力，可能需要几个来回，震荡幅度会加大，所以没有放量确认突破前，继续高抛低吸！', null, null, '2016-06-30 10:19:18', '2016-06-30 10:19:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2981', '8', 'text', '昨日沪指高开震荡，全天均在前一收盘点之上，收盘站上2930点，完成了对6月13日30分钟级别跳空缺口的回补，算是一个小突破。', null, null, '2016-06-30 10:26:08', '2016-06-30 10:26:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2982', '8', 'text', '大盘如期向上突破，不过强度并不强，短线还有一定上冲动能，但是期望值还是不宜太高。', null, null, '2016-06-30 10:27:35', '2016-06-30 10:27:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2983', '8', 'text', '目前两市企稳走高双双翻红，沪指涨0.02%创业板涨0.40%。', null, null, '2016-06-30 10:31:21', '2016-06-30 10:31:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2984', '8', 'text', '白酒板块一马当先，沱牌舍得涨9.90%。', null, null, '2016-06-30 10:32:13', '2016-06-30 10:32:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2985', '8', 'text', '　量子通信概念股崛起，浙江东方、永鼎股份涨停。', null, null, '2016-06-30 10:32:39', '2016-06-30 10:32:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2986', '8', 'text', '　在线教育板块表现强势，勤上光电涨停。', null, null, '2016-06-30 10:44:31', '2016-06-30 10:44:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2987', '8', 'text', '板块方面，仪器仪表居前。', null, null, '2016-06-30 10:48:55', '2016-06-30 10:48:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2988', '8', 'text', '汉威电子、智度投资、金卡股份、东华测试涨停。', null, null, '2016-06-30 10:56:16', '2016-06-30 10:56:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2989', '8', 'text', '30日是月线收官的日子，指数基本上不是小跌就是小涨，无论是哪种形态，在经过两个月的缩量震荡之后，都意味着具有很强的止跌企稳的信号。', null, null, '2016-06-30 10:58:55', '2016-06-30 10:58:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2990', '10000088', 'text', '老师300137是需要送股是吗？', null, null, '2016-06-30 11:07:46', '2016-06-30 11:07:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2991', '8', 'text', '之前只是派现金，并没有送股。', '10000088', '老师300137是需要送股是吗？', '2016-06-30 11:12:28', '2016-06-30 11:12:28', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2992', '8', 'text', '两市冲高回落企稳震荡，沪指涨0.01%创业板涨0.84%。', null, null, '2016-06-30 11:13:01', '2016-06-30 11:13:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2993', '8', 'text', '沪指早盘平开，深指小幅高开，但随后都陷入震荡走势，成交量较昨日相比有所缩量，个股涨跌互半。', null, null, '2016-06-30 11:13:49', '2016-06-30 11:13:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2994', '8', 'text', '昨晚监管层对前期热门次新股施压自查，今日早盘次新股集体下挫。', null, null, '2016-06-30 11:15:37', '2016-06-30 11:15:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2995', '8', 'text', '从技术上看，股指同时面临获利盘和上方套牢盘的压力，技术上有调整需求。', null, null, '2016-06-30 11:21:12', '2016-06-30 11:21:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2996', '8', 'text', '从市场整体结构，仍可判断为在2800-2900点之间的箱体震荡。同时市场连续大幅普涨，而成交量虽有放大，但仍无法突破3000亿级别的情况下，恐难以维系，后市个股大概率会出现分化。今日两市资金净流出接近38亿元，较上一个交易日，资金流出有所减少。', null, null, '2016-06-30 11:22:04', '2016-06-30 11:22:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('2997', '10000026', 'text', '	\n郑老师！有客户想查询三元锂电池是否是个骗局？', null, null, '2016-06-30 11:26:04', '2016-06-30 11:26:04', '-1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('2998', '8', 'text', '截止午盘，沪指报2924.47点，跌7.12点，跌幅0.24%，深成指报10447.21点，跌13.76点，跌幅0.13%，创业板报2215.1点，涨5.31点，涨幅0.24%。', null, null, '2016-06-30 11:34:52', '2016-06-30 11:34:52', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('2999', '8', 'text', '截止午盘，沪指报2924.47点，跌7.12点，跌幅0.24%，深成指报10447.21点，跌13.76点，跌幅0.13%，创业板报2215.1点，涨5.31点，涨幅0.24%。', null, null, '2016-06-30 11:35:08', '2016-06-30 11:35:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3000', '8', 'text', '为什么这么说呢？', '10000026', '	\n郑老师！有客户想查询三元锂电池是否是个骗局？', '2016-06-30 11:36:15', '2016-06-30 11:36:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3001', '8', 'text', '　板块方面，量子通信、券商、酿酒、文化传媒涨幅靠前，次新股、船舶制造、水利建设、稀土永磁跌幅靠前。', null, null, '2016-06-30 11:36:47', '2016-06-30 11:36:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3002', '8', 'text', '外围市场连续反扑下，股指上行乏力，迟迟不能突破。目前仍是存量资金的博弈，月底资金面稍有趋紧，此出不宜盲目追高，减仓观望还是主基调。', null, null, '2016-06-30 11:37:54', '2016-06-30 11:37:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3003', '8', 'text', '短期股指冲高后，可适当减仓观望，逢低继续关注中报预期良好的标的。', null, null, '2016-06-30 11:40:32', '2016-06-30 11:40:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3004', '8', 'text', '下面我们转回到原油。', null, null, '2016-06-30 11:44:39', '2016-06-30 11:44:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3005', '10000006', 'text', '老师，今天的股票怎么看啊', null, null, '2016-06-30 11:44:55', '2016-06-30 11:44:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3006', '10000002', 'text', '各位朋友大家好  老师好', null, null, '2016-06-30 11:48:54', '2016-06-30 11:48:54', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3007', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774972cb9523.png', null, null, '2016-06-30 11:51:08', '2016-06-30 11:51:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3008', '10000037', 'text', '#9', null, null, '2016-06-30 11:51:22', '2016-06-30 11:51:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3009', '8', 'text', '目前，原油价格处高位震荡，上涨动能放缓。', null, null, '2016-06-30 11:52:25', '2016-06-30 11:52:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3010', '8', 'text', '外围市场连续反扑下，股指上行乏力，迟迟不能突破。目前仍是存量资金的博弈，月底资金面稍有趋紧，此出不宜盲目追高，减仓观望还是主基调。', '10000006', '老师，今天的股票怎么看啊', '2016-06-30 11:53:01', '2016-06-30 11:53:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3011', '8', 'text', '普京好！#9', '10000002', '各位朋友大家好  老师好', '2016-06-30 11:53:32', '2016-06-30 11:53:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3012', '8', 'text', '#20', '10000037', '#9', '2016-06-30 11:53:41', '2016-06-30 11:53:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3013', '10000102', 'text', '普京，你在直播实里很火啊#4', null, null, '2016-06-30 12:03:41', '2016-06-30 12:03:41', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3014', '8', 'text', '昨晚EIA大战，大家收获很多，今天行情清淡的时候多休息，静待机会，入市再战！#4', null, null, '2016-06-30 12:04:28', '2016-06-30 12:04:28', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3015', '8', 'text', '到了中午休息时间，咱们下午见。#9', null, null, '2016-06-30 12:05:52', '2016-06-30 12:05:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3016', '8', 'text', '名字霸气无双。', '10000102', '普京，你在直播实里很火啊#4', '2016-06-30 12:08:46', '2016-06-30 12:08:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3017', '10000102', 'text', '吃完了，下午讲什么？我觉得昨晚儿讲的K线很好用的#9', null, null, '2016-06-30 12:44:20', '2016-06-30 12:44:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3018', '10000002', 'text', '#4  ', null, null, '2016-06-30 12:48:21', '2016-06-30 12:48:21', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3019', '10000002', 'text', '我一直都叫普京 ', null, null, '2016-06-30 12:49:23', '2016-06-30 12:49:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3020', '10000092', 'text', '#20', null, null, '2016-06-30 12:54:39', '2016-06-30 12:54:39', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3021', '10000006', 'text', '我还奥巴马呢', null, null, '2016-06-30 12:55:53', '2016-06-30 12:55:53', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3022', '10000002', 'text', '你叫布什都可以 ', null, null, '2016-06-30 12:57:18', '2016-06-30 12:57:18', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3023', '8', 'text', '大家下午好！#15', null, null, '2016-06-30 13:00:40', '2016-06-30 13:00:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3024', '10000000', 'text', '大家中午好！伍老师中午好！#4', null, null, '2016-06-30 13:02:35', '2016-06-30 13:02:35', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3025', '10000000', 'text', '昨晚我们的直播室又带领大家在EIA王者之夜大丰收，大家都把握住机会了吗？#9', null, null, '2016-06-30 13:03:28', '2016-06-30 13:03:28', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3026', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774a845ed504.jpg', null, null, '2016-06-30 13:04:06', '2016-06-30 13:04:06', '-1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3027', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774a860a3558.jpg', null, null, '2016-06-30 13:04:32', '2016-06-30 13:04:32', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3028', '10000000', 'text', '一晚上，两单建议合计获利72.4%！等于七个涨停板的收益！同时，纽约的原油期货交易所的原油交易在22:30分一分钟之内成交了超过6亿美元，这样的市场，大家还需要犹豫吗？？赶紧联系你的投顾参与我们的捞金之旅吧#4', null, null, '2016-06-30 13:06:46', '2016-06-30 13:06:46', '-1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3029', '10000000', 'text', '好了，下面来看看今天有什么重要数据需要我们投资者关注。', null, null, '2016-06-30 13:07:31', '2016-06-30 13:07:31', '-1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3030', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774a95f84765.jpg', null, null, '2016-06-30 13:08:47', '2016-06-30 13:08:47', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3031', '10000000', 'text', '一晚上，两单建议合计获利72.4%！等于七个涨停板的收益！同时，纽约的原油期货交易所的原油交易在22:30分一分钟之内成交了超过6亿美元，这样的市场，大家还需要犹豫吗？？赶紧联系你的投顾参与我们的捞金之旅吧', null, null, '2016-06-30 13:09:00', '2016-06-30 13:09:00', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3032', '10000000', 'text', '好了，下面来看看今天有什么重要数据需要我们投资者关注。#15', null, null, '2016-06-30 13:09:11', '2016-06-30 13:09:11', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3033', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774aa45990a7.jpg', null, null, '2016-06-30 13:12:37', '2016-06-30 13:12:37', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3034', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774aa4f7a14f.jpg', null, null, '2016-06-30 13:12:47', '2016-06-30 13:12:47', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3035', '10000000', 'text', '今天晚上7点半，要关注一下欧央行的货币会议纪要。', null, null, '2016-06-30 13:13:08', '2016-06-30 13:13:08', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3036', '10000000', 'text', '鉴于恰逢上周英国意外退欧，同时今年欧洲经济复苏非常缓慢，加上还有叙利亚难民危机和恐怖袭击威胁，整个欧洲经济可以说是摇摇欲坠。而一旦会议纪要出现意外的官员观点，将会对欧元和原油产生较大的波动。', null, null, '2016-06-30 13:15:56', '2016-06-30 13:15:56', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3037', '10000000', 'text', '今年以来的利率会议纪要：', null, null, '2016-06-30 13:16:11', '2016-06-30 13:16:11', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3038', '10000000', 'text', ' 2016年1月15日：欧洲央行公布的12月货币政策会议纪要显示，欧洲央行12月下调存款利率10个基点为进一步下调留下了空间。 会议纪要指出，欧元区经济复苏较为温和且脆弱，且欧洲央行通胀预期面临下行风险，且该下行趋势可能难以逆转；另外，由于新兴市场放缓，外部环境风险偏向下行。会议纪要预期欧央行将进一步宽松，打压欧元和原油，令原油当日大跌6%，加上33.3倍杠杆共199.8%的收益空间。', null, null, '2016-06-30 13:16:25', '2016-06-30 13:16:25', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3039', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774ab5c85aeb.jpg', null, null, '2016-06-30 13:17:16', '2016-06-30 13:17:16', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3040', '10000000', 'text', ' 2016年2月16日：欧洲央行公布的1月货币政策会议纪要显示，虽然欧元区经济正在温和复苏，但风险正在加大，且有迹象表明，低油价效应可能传导至其它商品和服务的价格。 会议纪要指出，欧元区经济前景处于下行风险，并且风险在去年12月以来有所增加，因而1月会议认为，欧洲央行最好提前采取行动来应对下行风险。 会议纪要大幅利空欧元，打压原油从最高位大跌10%！加上杠杆收益空间高达330%！', null, null, '2016-06-30 13:21:40', '2016-06-30 13:21:40', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3041', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774ac8180a41.jpg', null, null, '2016-06-30 13:22:09', '2016-06-30 13:22:09', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3042', '10000000', 'text', ' 2016年4月7日：欧洲央行公布的3月货币政策会议纪要显示，欧洲央行成员认为，3月降息幅度应该“更大”，以使得利率接近最低界限；同时，调整利率仍是管委会工具之一，不会排除未来进一步降息的可能性。 会议纪要依然偏向于利空欧元和原油，打压原油下跌2.5%，收益空间 83.3%！。 ', null, null, '2016-06-30 13:24:31', '2016-06-30 13:24:31', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3043', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774ad2838671.jpg', null, null, '2016-06-30 13:24:56', '2016-06-30 13:24:56', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3044', '10000000', 'text', '2016年5月19日：欧洲央行公布的4月货币政策会议纪要称，将把重点放在实施最新措施上。此外，必须确保低油价的间接影响不会使低通胀变得根深蒂固。 纪要显示，4月利率决策会议上认为经济增长面临的风险已减缓，但仍偏向下行；此外，市场反映的通胀预期并未随油价反弹“令人忧心”；该央行致力于将通胀推升至目标水平2.0%。会议纪要利好欧元和原油，带动原油从低位反弹3%，收益空间100%！', null, null, '2016-06-30 13:25:18', '2016-06-30 13:25:18', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3045', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774ad5093f40.jpg', null, null, '2016-06-30 13:25:36', '2016-06-30 13:25:36', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3046', '10000000', 'text', '所以大家可以看到，今年以来，每次欧央行利率会议造成的收益空间，都至少达到近一倍以上！所以，错过昨晚EIA的朋友，今晚就绝对不能再错过了', null, null, '2016-06-30 13:33:01', '2016-06-30 13:33:01', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3047', '10000000', 'text', '那么今晚的行情解读及投资建议是', null, null, '2016-06-30 13:34:16', '2016-06-30 13:34:16', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3048', '10000000', 'text', '上周，英国公投意外退欧令整个西方世界惊恐不已。虽然有部分英国民众建议举行第二次公投以挽留英国在欧盟的地位，但从欧盟和英国已经启动了退欧谈判协议来看，英国留在欧洲的可能性已经微乎其微。', null, null, '2016-06-30 13:34:26', '2016-06-30 13:34:26', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3049', '10000000', 'text', '由于伦敦作为世界重要的金融中心，为欧盟很多国家的企业提供大量的金融和贸易服务，同时欧盟国家例如德法等国在英国的企业也为英国带来相当多的就业机会。英国退欧意味着德法等企业将逐渐撤出英伦三岛，而英国也损失了一大部分的金融产值。所以，退欧事件公布后，世界几大评级机构已经纷纷下调了未来两年英国和欧洲的经济预期，高盛甚至预期英国将陷入衰退。所以，会议纪要很有可能会谈及延长甚至扩大QE和宽松来延缓退欧对欧洲经', null, null, '2016-06-30 13:38:14', '2016-06-30 13:38:14', '-1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3050', '10000000', 'text', '由于伦敦作为世界重要的金融中心，为欧盟很多国家的企业提供大量的金融和贸易服务，同时欧盟国家例如德法等国在英国的企业也为英国带来相当多的就业机会。英国退欧意味着德法等企业将逐渐撤出英伦三岛，而英国也损失了一大部分的金融产值', null, null, '2016-06-30 13:38:24', '2016-06-30 13:38:24', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3051', '10000000', 'text', '所以，退欧事件公布后，世界几大评级机构已经纷纷下调了未来两年英国和欧洲的经济预期，高盛甚至预期英国将陷入衰退。所以，会议纪要很有可能会谈及延长甚至扩大QE和宽松来延缓退欧对欧洲经济的短期冲击，从而利空欧元和原油。鉴于这次会议纪要面临了近20年来最大的政治事件，所以行情波动将会是今年所有的会议纪要中最大的一晚！', null, null, '2016-06-30 13:38:45', '2016-06-30 13:38:45', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3052', '10000000', 'text', '另一个数据就是今晚8点半公布的初请失业人数。', null, null, '2016-06-30 13:46:23', '2016-06-30 13:46:23', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3053', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774b250089bb.jpg', null, null, '2016-06-30 13:46:56', '2016-06-30 13:46:56', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3054', '10000037', 'text', '期待今晚的大行情', null, null, '2016-06-30 13:48:32', '2016-06-30 13:48:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3055', '10000000', 'text', '初请人数近一周以来有出现慢慢上升的迹象，说明美国就业市场并非想象中那么强大，加上个月非农数据仅有3.8万左右，所以这次的初请数据很有可能会利空美元利多原油。同时由于下周五就是全世界关注的非农数据，本月最后一次初请数据也是非常受关注。', null, null, '2016-06-30 13:49:10', '2016-06-30 13:49:10', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3056', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774b4e7b5aa7.jpg', null, null, '2016-06-30 13:57:59', '2016-06-30 13:57:59', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3057', '10000002', 'text', '你好 郑老师 我想问一下 我昨天再330建的空单，目前什么价位出比较好？还是我继续持有', null, null, '2016-06-30 14:02:31', '2016-06-30 14:02:31', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3058', '10000109', 'text', '今天是6月30号整整半年了，老师今天晚上行情是不是会很大吗?#6#9', null, null, '2016-06-30 14:02:51', '2016-06-30 14:02:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3059', '10000000', 'text', '目前行情方向还不是很明了，所以单子都是以短线为主，有利润可以先走', '10000002', '你好 郑老师 我想问一下 我昨天再330建的空单，目前什么价位出比较好？还是我继续持有', '2016-06-30 14:04:34', '2016-06-30 14:04:34', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3060', '10000000', 'text', '是的，今晚不仅有风险事件和数据，还有就是今天是收日线的日子，行情波动会较大', '10000109', '今天是6月30号整整半年了，老师今天晚上行情是不是会很大吗?#6#9', '2016-06-30 14:05:06', '2016-06-30 14:05:06', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3061', '10000000', 'text', '下面时间交回给伍老师，看看他给大家带来什么课程#4', null, null, '2016-06-30 14:05:34', '2016-06-30 14:05:34', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3062', '10000002', 'text', '我看均线现在处于弱势，空单的问题不大吧？', null, null, '2016-06-30 14:06:08', '2016-06-30 14:06:08', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3063', '8', 'text', '大家好，郑老师辛苦了，下面咱们接着昨日的课程讲解。#9', '10000002', '你叫布什都可以 ', '2016-06-30 14:06:26', '2016-06-30 14:06:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3064', '10000002', 'text', '伍老师好 ', null, null, '2016-06-30 14:07:59', '2016-06-30 14:07:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3065', '8', 'text', '目前处多头回调走势，空单也建议以短线为主，有利润可以先出。', '10000002', '我看均线现在处于弱势，空单的问题不大吧？', '2016-06-30 14:09:01', '2016-06-30 14:09:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3066', '8', 'text', '大帝好#9', '10000002', '伍老师好 ', '2016-06-30 14:09:39', '2016-06-30 14:09:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3067', '8', 'text', '昨天的课程讲了常见的一些K线形态以及K线组合运用。', null, null, '2016-06-30 14:11:45', '2016-06-30 14:11:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3068', '8', 'text', '大家不要小看这些基础的知识，很多分析方法都是从这些最基础的东西演变过来的。', null, null, '2016-06-30 14:13:56', '2016-06-30 14:13:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3069', '10000002', 'text', '  今天听投顾给我讲了一个低开和高开，这个具体怎么看？', null, null, '2016-06-30 14:18:38', '2016-06-30 14:18:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3070', '8', 'text', '昨天K线组合讲到早晨之星和黄昏之星', null, null, '2016-06-30 14:18:55', '2016-06-30 14:18:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3071', '8', 'text', '高开的意思是早上以跳空走高开盘，低开与之相反，中间留下缺口。', '10000002', '  今天听投顾给我讲了一个低开和高开，这个具体怎么看？', '2016-06-30 14:22:46', '2016-06-30 14:22:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3072', '8', 'text', '关于缺口理论，咱们以后有机会也会讲到。今天先继续昨天的课程。', '10000002', '  今天听投顾给我讲了一个低开和高开，这个具体怎么看？', '2016-06-30 14:24:22', '2016-06-30 14:24:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3073', '8', 'text', 'K线组合：穿头破脚1', null, null, '2016-06-30 14:26:00', '2016-06-30 14:26:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3074', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774bb93e8d5a.png', null, null, '2016-06-30 14:26:28', '2016-06-30 14:26:28', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3075', '8', 'text', '穿头破脚1，它是用法如下：', null, null, '2016-06-30 14:27:54', '2016-06-30 14:27:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3076', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774bc035b8e6.png', null, null, '2016-06-30 14:28:19', '2016-06-30 14:28:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3077', '8', 'text', '它也可以叫做阳包阴，意思就是后面一根阳线完全包覆前面的阴线，它一般出现在行情底部，有反转趋势的作用。', null, null, '2016-06-30 14:31:53', '2016-06-30 14:31:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3078', '8', 'text', '下面是穿头破脚2', null, null, '2016-06-30 14:32:46', '2016-06-30 14:32:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3079', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774bd23d675d.png', null, null, '2016-06-30 14:33:07', '2016-06-30 14:33:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3080', '8', 'text', '它也叫做阴包阳，与前面的阳包阴刚好相反。', null, null, '2016-06-30 14:34:14', '2016-06-30 14:34:14', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3081', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774bd87c4fff.png', null, null, '2016-06-30 14:34:47', '2016-06-30 14:34:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3082', '8', 'text', '它一般出现的行情顶部，出现这种K线组合，则后续走势看空。', null, null, '2016-06-30 14:36:18', '2016-06-30 14:36:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3083', '8', 'text', '上面的这些K线组合是准确率非常高的几种，还有一些组合这里就不一一列举了，大家有时间可以自己去找相关的一些书籍。', null, null, '2016-06-30 14:42:21', '2016-06-30 14:42:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3084', '8', 'text', 'K线讲完以后，下面就开始讲技术指标', null, null, '2016-06-30 14:44:47', '2016-06-30 14:44:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3085', '8', 'text', '技术指标它是对价格的波动进行量化后，得出的较为抽象的价格走势分析工具。', null, null, '2016-06-30 14:45:37', '2016-06-30 14:45:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3086', '8', 'text', '技术指标作为辅助工具对研究价格的走势具有很重要的作用。', null, null, '2016-06-30 14:47:51', '2016-06-30 14:47:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3087', '8', 'text', '现在咱们就常用的几种讲解一下，先从均线开始吧。', null, null, '2016-06-30 14:50:57', '2016-06-30 14:50:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3088', '8', 'text', '移动平均线(MA)是将一段时期内的价格平均值连成曲线，用来显示价格的历史波动情况，进而反映价格未来发展趋势的技术分析方法。', null, null, '2016-06-30 14:51:36', '2016-06-30 14:51:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3089', '8', 'text', '有时候直播室里老师的分析中出现的5MA、10MA等指的就是移动平均线，这里简称均线。', null, null, '2016-06-30 14:53:46', '2016-06-30 14:53:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3090', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774c243779c9.png', null, null, '2016-06-30 14:54:59', '2016-06-30 14:54:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3091', '8', 'text', '移动平均线依计算周期可分为：短期(如5日、10日)移动平均线、中期(如30日)移动平均线、长期(如60日、120日)移动平均线 。', null, null, '2016-06-30 14:56:04', '2016-06-30 14:56:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3092', '8', 'text', '短期的移动平均线比较敏感，适合运用于行情的调整阶段和反转阶段。', null, null, '2016-06-30 14:56:53', '2016-06-30 14:56:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3093', '8', 'text', '长期的移动平均线比较稳定，适合运用于趋势明显的市况。', null, null, '2016-06-30 14:57:20', '2016-06-30 14:57:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3094', '8', 'text', '下面是移动平均线的运用：\n', null, null, '2016-06-30 15:01:15', '2016-06-30 15:01:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3095', '8', 'text', '1.平均线从下降逐渐走平，而价格从平均线的下方突破平均线时，是买进信号。', null, null, '2016-06-30 15:02:05', '2016-06-30 15:02:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3096', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774c4e84e4c0.png', null, null, '2016-06-30 15:06:16', '2016-06-30 15:06:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3097', '8', 'text', '价格虽跌入平均线下，而均价线仍在上扬，不久又回复到平均线上时，为买进信号。', null, null, '2016-06-30 15:11:47', '2016-06-30 15:11:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3098', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774c63e4fb32.png', null, null, '2016-06-30 15:11:58', '2016-06-30 15:11:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3099', '8', 'text', '由于时间的关系，今天课程先讲到这里，下面我看看今天的股市的收盘。', null, null, '2016-06-30 15:13:16', '2016-06-30 15:13:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3100', '8', 'text', '隔夜外围市场的再度反弹带动早盘A股开盘后小幅上行，随后由于市场抛压加重指数震荡回落，个股呈现跌多涨少格局。随着乐视网、东方财富等创业板权重股的大幅反弹，创业板指再度反弹，深强沪弱的格局明显。截至收盘，上证指数以2929.61点报收，成交1875亿元。', null, null, '2016-06-30 15:14:59', '2016-06-30 15:14:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3101', '10000102', 'text', '原油可以进空单吗？现在', null, null, '2016-06-30 15:15:12', '2016-06-30 15:15:12', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3102', '10000000', 'text', '行情大幅回落，我们尝试短线做空，止损止盈带好。个人建议仅供参考。', null, null, '2016-06-30 15:16:51', '2016-06-30 15:16:51', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3103', '10000000', 'text', '可以短空操作，建议带好止损止盈', '10000102', '原油可以进空单吗？现在', '2016-06-30 15:17:29', '2016-06-30 15:17:29', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3104', '8', 'text', '昨晚监管层对次新股发出警告，这造成今天早盘次新股集体跳水，也拖累了整个市场的表现。', null, null, '2016-06-30 15:17:56', '2016-06-30 15:17:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3105', '8', 'text', '不过从盘中来看券商股，尤其是国泰君安有崛起的动向。一旦券商股崛起，市场有可能形成重新反弹的动力。', null, null, '2016-06-30 15:18:38', '2016-06-30 15:18:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3106', '8', 'text', '而且从整个市场的动态来说，今天指数虽然受到次新股的影响，但是跌幅也不深，这说明市场还是比较强的，在强势之下市场每次回调都是低吸的机会。', null, null, '2016-06-30 15:19:38', '2016-06-30 15:19:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3107', '8', 'text', '随着利空因素的消化，投资者信心逐渐恢复，入市操作意愿增强，两市融资余额迎来三连升，截止29日，两市融资余额8498.38亿，逼近8500亿，而融资融券余额则突破8500亿至8525.18亿，这是在5月9日大跌以来的新高，因此显示市场逐将回暖，对下半年的行情走势比较乐观。', null, null, '2016-06-30 15:20:43', '2016-06-30 15:20:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3108', '8', 'text', '【后市展望】在经历一年多的调整之后，A股估值已逐步恢复到成长性与估值相匹配的程度。在震荡行情中，周期性和成长性更具有上涨的逻辑，“重视择股、淡化指数”仍是以不变应万变的较好应对方法。', null, null, '2016-06-30 15:22:30', '2016-06-30 15:22:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3109', '8', 'text', '【后市展望】目前可重点关注两类品种：一是企业盈利拐点出现、受益于涨价的行业，如黄金和养殖业；二是受益于企业自身内生增长、估值与企业成长性匹配的稳定增长类股票，如新能源汽车、虚拟现实等行业。', null, null, '2016-06-30 15:23:19', '2016-06-30 15:23:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3110', '8', 'text', '随着风险逐渐被释放殆尽，市场迎来了新的契机，投资者应把握机会、采用多元化的投资方式来提高收益，分散风险。譬如原油等大宗商品，T+O制度随买随卖及时锁定利润，双向做单机制既能做多也能做空顺势而为，让风险最小化，收益最大化。', null, null, '2016-06-30 15:25:24', '2016-06-30 15:25:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3111', '8', 'text', '请大家稍等一下，我去开个例会，马上回来，精彩继续！#9', null, null, '2016-06-30 15:27:34', '2016-06-30 15:27:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3112', '10000013', 'text', '老师，今晚的欧央行利率会议是几点啊？行情波动会很大吗？', null, null, '2016-06-30 15:37:33', '2016-06-30 15:37:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3113', '10000102', 'text', '老师，啥情况？咋涨上去了？', null, null, '2016-06-30 15:42:04', '2016-06-30 15:42:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3114', '8', 'text', '大家好，我回来了，刚刚去开了个例会，让大家久等了。', null, null, '2016-06-30 15:47:57', '2016-06-30 15:47:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3115', '8', 'text', '今天晚上7点半，欧央行的货币会议纪要。今年以来，每次欧央行利率会议造成的收益空间，都至少达到近一倍以上！鉴于这次会议纪要面临了近20年来最大的政治事件，所以预计行情波动将会是今年所有的会议纪要中最大的一晚！', '10000013', '老师，今晚的欧央行利率会议是几点啊？行情波动会很大吗？', '2016-06-30 15:50:40', '2016-06-30 15:50:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3116', '8', 'text', '刚才消息公布，据路透援引消息人士：伊朗7月石油出口将从6月的4年半高位231万桶/日下滑至214万桶/日，对原油有提振作用。', '10000102', '老师，啥情况？咋涨上去了？', '2016-06-30 15:53:07', '2016-06-30 15:53:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3117', '8', 'text', '行情震荡，空单暂时先持有，不建议加仓。', null, null, '2016-06-30 15:54:33', '2016-06-30 15:54:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3118', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774d15dcd463.png', null, null, '2016-06-30 15:59:25', '2016-06-30 15:59:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3119', '8', 'text', '目前，行情又走到了330这个非常关键的压力位置附近，若后续能持续站稳330上方，多头力量将进一步释放。', null, null, '2016-06-30 16:02:46', '2016-06-30 16:02:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3120', '10000102', 'text', '329.5压力', null, null, '2016-06-30 16:04:50', '2016-06-30 16:04:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3121', '8', 'text', '？？', '10000102', '329.5压力', '2016-06-30 16:07:16', '2016-06-30 16:07:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3122', '10000102', 'text', '涨不上去，是不是？#16我的空单', null, null, '2016-06-30 16:12:55', '2016-06-30 16:12:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3123', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774d48776881.png', null, null, '2016-06-30 16:12:55', '2016-06-30 16:12:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3124', '8', 'text', '大家有没有发现这两段行情比较相似', null, null, '2016-06-30 16:14:00', '2016-06-30 16:14:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3125', '10000000', 'text', '行情走势比较诡异，空单先适当减仓。待行情走出明显方向再操作', null, null, '2016-06-30 16:16:00', '2016-06-30 16:16:00', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3126', '8', 'text', '目前反弹比较强势，稳健的话，空单可以适当减仓操作。', '10000102', '涨不上去，是不是？#16我的空单', '2016-06-30 16:19:25', '2016-06-30 16:19:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3127', '10000008', 'text', '看来真是验证了历史总是惊人的相似，这个说法是有道理的', null, null, '2016-06-30 16:20:36', '2016-06-30 16:20:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3128', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774d6a84d3da.png', null, null, '2016-06-30 16:22:00', '2016-06-30 16:22:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3129', '8', 'text', '美元指数持续回落，给油价提供了支撑。', null, null, '2016-06-30 16:23:10', '2016-06-30 16:23:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3130', '8', 'text', '是的，历史不会重演，但总会惊人的相似，这也是技术分析的依据所在。#20', '10000008', '看来真是验证了历史总是惊人的相似，这个说法是有道理的', '2016-06-30 16:24:45', '2016-06-30 16:24:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3131', '8', 'text', '美元指数出现触底反弹。', null, null, '2016-06-30 16:32:24', '2016-06-30 16:32:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3132', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774d93b34003.png', null, null, '2016-06-30 16:32:59', '2016-06-30 16:32:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3133', '10000000', 'text', '这个原油。。。走的太令人纠结了#13', null, null, '2016-06-30 16:35:12', '2016-06-30 16:35:12', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3134', '10000103', 'text', '纠结君#5#5', null, null, '2016-06-30 16:42:23', '2016-06-30 16:42:23', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3135', '10000102', 'text', '#20', null, null, '2016-06-30 16:43:14', '2016-06-30 16:43:14', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3136', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774dbdca5cd2.png', null, null, '2016-06-30 16:44:12', '2016-06-30 16:44:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3137', '8', 'text', '上方收330关键压力压制，上涨受阻，尝试短线空单，设好止盈止损。个人建议，仅供参考。', null, null, '2016-06-30 16:44:35', '2016-06-30 16:44:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3138', '8', 'text', '原油短线冲高回落，说明上方压力依然很强，美元指数下挫创内日新低，之后快速反弹。综合来看，预计原油有短线回调需要。', null, null, '2016-06-30 16:49:00', '2016-06-30 16:49:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3139', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774de0c75591.png', null, null, '2016-06-30 16:53:32', '2016-06-30 16:53:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3140', '8', 'text', '五点钟有欧元区6月CPI年率初值数据，大家可以适当关注一下。', null, null, '2016-06-30 16:54:43', '2016-06-30 16:54:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3141', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774deb66437c.png', null, null, '2016-06-30 16:56:22', '2016-06-30 16:56:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3142', '8', 'text', '若公布值大于预期值是利好欧元', null, null, '2016-06-30 16:58:17', '2016-06-30 16:58:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3143', '8', 'text', '同时利多原油', null, null, '2016-06-30 16:59:07', '2016-06-30 16:59:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3144', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774dfbdd60f6.png', null, null, '2016-06-30 17:00:45', '2016-06-30 17:00:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3145', '10000103', 'text', '下午好啊亲们#9#9', null, null, '2016-06-30 17:01:01', '2016-06-30 17:01:01', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3146', '10000103', 'text', '今天郑老师、伍老师辛苦啦#14大家先放松一下吧', null, null, '2016-06-30 17:04:25', '2016-06-30 17:04:25', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3147', '10000103', 'text', '昨天说健康从现在做起，其中的8个让人精力充沛的办法大家有没有做呢#6#6', null, null, '2016-06-30 17:13:37', '2016-06-30 17:13:37', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3148', '10000103', 'text', '特别是长时间在室内待着的朋友们，有没觉得很容易让人感觉疲劳，', null, null, '2016-06-30 17:15:19', '2016-06-30 17:15:19', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3149', '10000103', 'text', '这个时候应该站起来伸伸腰，扭扭脖子，活动下筋骨，喝杯咖啡望望窗外，放松一下#4', null, null, '2016-06-30 17:18:14', '2016-06-30 17:18:14', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3150', '10000103', 'text', '刚看到一段关于健康的顺口溜，觉得特接近平时的饮食，给大家念念呗~~', null, null, '2016-06-30 17:27:53', '2016-06-30 17:27:53', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3151', '10000103', 'text', '【顺口溜】若要皮肤好,粥里放红枣。若要不失眠,粥里添白莲。腰酸肾气虚,煮粥放板栗。心虚气不足,粥加桂圆肉。头昏多汗症,粥里加薏仁。', null, null, '2016-06-30 17:31:38', '2016-06-30 17:31:38', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3152', '10000103', 'text', '润肺又止咳,粥里加百合。消暑解热毒，常饮绿豆粥。乌发又补肾,粥加核桃仁。若要降血压,煮粥加荷叶。滋阴润肺好,煮粥加银耳。', null, null, '2016-06-30 17:32:07', '2016-06-30 17:32:07', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3153', '10000103', 'text', '看完这段顺口溜，大家有没有在平时饮食里也加加料呢#17反正我是暗自窃喜了#5#5', null, null, '2016-06-30 17:37:49', '2016-06-30 17:37:49', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3154', '10000103', 'text', '好啦，临近月底，又是总结汇报的时候啦#4', null, null, '2016-06-30 17:51:55', '2016-06-30 17:51:55', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3155', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774ebcb6b2ab.png', null, null, '2016-06-30 17:52:11', '2016-06-30 17:52:11', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3156', '10000103', 'text', '自直播室22号开播以来，老师们的战绩可谓是捷报连连~可圈可点！，统计了一下到今天为止，总的收益达到343.9%！！总共14条操作建议准确率为78.6%！', null, null, '2016-06-30 17:52:39', '2016-06-30 17:52:39', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3157', '10000103', 'text', '#14#14#14', null, null, '2016-06-30 17:52:50', '2016-06-30 17:52:50', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3158', '10000001', 'text', '大家下午好！#9', null, null, '2016-06-30 17:52:56', '2016-06-30 17:52:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3159', '10000001', 'text', '小榆老师好#9', null, null, '2016-06-30 17:54:38', '2016-06-30 17:54:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3160', '10000103', 'text', '之前没有跟上我们的朋友，现在就不要犹豫咯！！赶紧加入把握机会加入我们的捞金之旅吧！！！', null, null, '2016-06-30 17:55:23', '2016-06-30 17:55:23', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3161', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774ecb6b84de.jpeg', null, null, '2016-06-30 17:56:06', '2016-06-30 17:56:06', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3162', '10000103', 'text', '今晚8点30分公布的初请失业人数，记得关注噢', null, null, '2016-06-30 17:57:19', '2016-06-30 17:57:19', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3163', '10000103', 'text', '#4#4吴老师好！', null, null, '2016-06-30 17:57:47', '2016-06-30 17:57:47', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3164', '10000006', 'text', '#20', null, null, '2016-06-30 17:57:50', '2016-06-30 17:57:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3165', '10000103', 'text', '今晚就辛苦吴老师给大家分析讲解啦#9#9#9', null, null, '2016-06-30 17:58:49', '2016-06-30 17:58:49', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3166', '10000103', 'text', '下面把直播室的时间交给吴老师', null, null, '2016-06-30 17:59:10', '2016-06-30 17:59:10', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3167', '10000001', 'text', '好，谢谢小榆老师#9', null, null, '2016-06-30 18:00:48', '2016-06-30 18:00:48', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3168', '10000001', 'text', '好，谢谢小榆老师 #9#14', null, null, '2016-06-30 18:01:27', '2016-06-30 18:01:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3169', '10000001', 'text', '在资本市场获利，回报率只是一个符号，而构成这个符号的不只是智慧和实力，更重要的是勇气和信心', null, null, '2016-06-30 18:03:06', '2016-06-30 18:03:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3170', '10000001', 'text', '我是轩湛投资观察室的吴勇坚，接下来我来为大家做直播，大家有什么疑问或者疑惑，可以在互动区跟我交流喔', null, null, '2016-06-30 18:03:24', '2016-06-30 18:03:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3171', '10000001', 'text', '好，接下来看一下目前原油的走势', null, null, '2016-06-30 18:06:48', '2016-06-30 18:06:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3172', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774efaea2133.png', null, null, '2016-06-30 18:08:46', '2016-06-30 18:08:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3173', '10000001', 'text', '目前油价继续在压力区间震荡', null, null, '2016-06-30 18:09:43', '2016-06-30 18:09:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3174', '10000001', 'text', '由于今晚19:30是欧央行将公布利率会议纪要', null, null, '2016-06-30 18:11:10', '2016-06-30 18:11:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3175', '10000001', 'text', '而且欧洲央行行长曾称认同经济学家做出的英国脱欧将导致未来三年欧元区GDP增长下降0.3%-0.5%的预期', null, null, '2016-06-30 18:12:42', '2016-06-30 18:12:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3176', '10000001', 'text', '那么今晚的欧央行将公布利率会议纪要很大可能会下调通胀目标', null, null, '2016-06-30 18:14:33', '2016-06-30 18:14:33', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3177', '10000001', 'text', '所以今晚的欧洲央行货币政策会议纪要很大可能下调通胀目标', null, null, '2016-06-30 18:17:38', '2016-06-30 18:17:38', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3178', '10000001', 'text', '所以今晚的欧洲央行货币政策会议纪要很大可能下调通胀目标', null, null, '2016-06-30 18:17:57', '2016-06-30 18:17:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3179', '10000001', 'text', '从而利空油价', null, null, '2016-06-30 18:18:17', '2016-06-30 18:18:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3180', '10000001', 'text', '所以，个人建议：大家可以在目前的压力位329-330附近预先布局轻仓的空单', null, null, '2016-06-30 18:20:57', '2016-06-30 18:20:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3181', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774f48e24019.png', null, null, '2016-06-30 18:29:34', '2016-06-30 18:29:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3182', '10000102', 'text', '还能跌吗？', null, null, '2016-06-30 18:31:46', '2016-06-30 18:31:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3183', '10000001', 'text', '上一次欧洲央行的会议纪要：提高通胀目标', null, null, '2016-06-30 18:32:47', '2016-06-30 18:32:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3184', '10000001', 'text', '使油价反弹了3%', null, null, '2016-06-30 18:33:12', '2016-06-30 18:33:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3185', '10000001', 'text', '目前压力位盘整，加上今晚的欧洲央行利率会议纪要预测利空油价，所以今晚很大可能使大涨两天的油价回落', '10000102', '还能跌吗？', '2016-06-30 18:35:55', '2016-06-30 18:35:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3186', '10000102', 'text', '太棒了！#4', null, null, '2016-06-30 18:42:51', '2016-06-30 18:42:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3187', '10000001', 'text', '今晚除了欧洲央行利率会议纪要之外，还有20:30美国至6月25日当周初请失业金人数', null, null, '2016-06-30 18:45:04', '2016-06-30 18:45:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3188', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774f889bbabc.png', null, null, '2016-06-30 18:46:33', '2016-06-30 18:46:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3189', '10000001', 'text', '截至上周，美国当周初请失业金人数为连续68周处于30万人门槛下方，几乎是自1973年来最长周期', null, null, '2016-06-30 18:51:17', '2016-06-30 18:51:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3190', '10000001', 'text', '耶伦曾表示：对就业市场依然乐观', null, null, '2016-06-30 19:00:33', '2016-06-30 19:00:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3191', '10000001', 'text', '加上上周初请失业金人数录得5月的低位，所以预测今晚的初请失业金人数低于预期值', null, null, '2016-06-30 19:02:27', '2016-06-30 19:02:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3192', '10000001', 'text', '从而利空油价，使油价回落', null, null, '2016-06-30 19:02:51', '2016-06-30 19:02:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3193', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5774fdb536e43.png', null, null, '2016-06-30 19:08:37', '2016-06-30 19:08:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3194', '10000001', 'text', '压力位短线空单。个人建议，仅供参考', null, null, '2016-06-30 19:09:12', '2016-06-30 19:09:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3195', '10000001', 'text', '目前行情较为清淡', null, null, '2016-06-30 19:16:34', '2016-06-30 19:16:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3196', '10000001', 'text', '那我就讲讲做单常犯的一些心里错误', null, null, '2016-06-30 19:19:18', '2016-06-30 19:19:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3197', '10000001', 'text', '绝大多数人交易的时候都有一种好赌的心理，喜欢重仓位杀进杀出，想一次赚个够', null, null, '2016-06-30 19:20:44', '2016-06-30 19:20:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3198', '10000001', 'text', '这些人想必一次也亏个够了', null, null, '2016-06-30 19:21:28', '2016-06-30 19:21:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3199', '10000102', 'text', '是的，我就是#16', null, null, '2016-06-30 19:24:26', '2016-06-30 19:24:26', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3200', '10000001', 'text', '在这个市场，没有百分百是对的，我们做的是一个概率', null, null, '2016-06-30 19:25:04', '2016-06-30 19:25:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3201', '10000001', 'text', '现在80%概率倾向做空，那么就做空', null, null, '2016-06-30 19:26:06', '2016-06-30 19:26:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3202', '10000194', 'text', '饿死胆小的就是我了#4', null, null, '2016-06-30 19:29:58', '2016-06-30 19:29:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3203', '10000001', 'text', '就像赌大小那样，已经连续开50次的大，那下一盘按概率算开小的概率比较大，那就押小', null, null, '2016-06-30 19:31:09', '2016-06-30 19:31:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3204', '10000001', 'text', '但下一盘也不一定开小的啊', null, null, '2016-06-30 19:31:36', '2016-06-30 19:31:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3205', '10000170', 'text', '反映平淡', null, null, '2016-06-30 19:32:38', '2016-06-30 19:32:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3206', '10000001', 'text', '控制好仓位很重要', '10000102', '是的，我就是#16', '2016-06-30 19:32:58', '2016-06-30 19:32:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3207', '10000001', 'text', '稳定的盈利，是这个市场能长远生存下去的关键#9', '10000194', '饿死胆小的就是我了#4', '2016-06-30 19:34:15', '2016-06-30 19:34:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3208', '10000001', 'text', '好了，我们回到原油的走势', null, null, '2016-06-30 19:36:21', '2016-06-30 19:36:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3209', '10000170', 'text', '开始了', null, null, '2016-06-30 19:37:27', '2016-06-30 19:37:27', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3210', '10000001', 'text', '有空再聊聊交易的心里，货币政策会议纪要开始了，油价开始走动了', null, null, '2016-06-30 19:37:34', '2016-06-30 19:37:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3211', '10000170', 'text', '会议纪要像老师之前预测的么', null, null, '2016-06-30 19:39:29', '2016-06-30 19:39:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3212', '10000001', 'text', '欧盟委员会副主席讲话的影响，油价开始小幅下落了', null, null, '2016-06-30 19:39:55', '2016-06-30 19:39:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3213', '10000001', 'text', '欧盟委员会副主席表示委员会准备好合理措施来应对未来几周可能出现的震荡', null, null, '2016-06-30 19:40:25', '2016-06-30 19:40:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3214', '10000037', 'text', '跌跌', null, null, '2016-06-30 19:40:29', '2016-06-30 19:40:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3215', '10000001', 'text', '可能性比较大', '10000170', '会议纪要像老师之前预测的么', '2016-06-30 19:40:46', '2016-06-30 19:40:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3216', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160630/57750565864af.png', null, null, '2016-06-30 19:41:25', '2016-06-30 19:41:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3217', '10000037', 'text', '路透财经日历最新显示，欧洲央行6月货币政策会议纪要调整至下周四公布。', null, null, '2016-06-30 19:42:59', '2016-06-30 19:42:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3218', '10000037', 'text', '？？？', null, null, '2016-06-30 19:43:04', '2016-06-30 19:43:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3219', '10000170', 'text', '有安抚的感觉，不过从另一面理解，有预测市场剧烈震荡', null, null, '2016-06-30 19:43:46', '2016-06-30 19:43:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3220', '10000001', 'text', '那就是今晚的会议的决定将在下周四才公布#2', '10000037', '路透财经日历最新显示，欧洲央行6月货币政策会议纪要调整至下周四公布。', '2016-06-30 19:45:09', '2016-06-30 19:45:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3221', '10000001', 'text', '#20', '10000170', '有安抚的感觉，不过从另一面理解，有预测市场剧烈震荡', '2016-06-30 19:45:15', '2016-06-30 19:45:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3222', '10000037', 'text', '那还要持有吗？', null, null, '2016-06-30 19:46:08', '2016-06-30 19:46:08', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3223', '10000037', 'text', '跌', null, null, '2016-06-30 19:46:16', '2016-06-30 19:46:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3224', '10000001', 'text', '可以先持有观察，毕竟今晚市场都预测会议会扩大QE和宽松来延缓退欧对欧洲经济的短期冲击', '10000037', '那还要持有吗？', '2016-06-30 19:48:25', '2016-06-30 19:48:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3225', '10000037', 'text', '好', null, null, '2016-06-30 19:49:17', '2016-06-30 19:49:17', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3226', '10000001', 'text', '路透财经日历最新显示，欧洲央行6月货币政策会议纪要调整至下周四公布', null, null, '2016-06-30 19:54:11', '2016-06-30 19:54:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3227', '10000001', 'text', '虽然是这样，但大家的空单还是可以持有的', null, null, '2016-06-30 19:54:38', '2016-06-30 19:54:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3228', '10000001', 'text', '毕竟还是有个市场预期会议会扩大QE和宽松，是油价下落的', null, null, '2016-06-30 19:55:38', '2016-06-30 19:55:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3229', '10000037', 'text', '326.9了', null, null, '2016-06-30 19:57:22', '2016-06-30 19:57:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3230', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160630/577509234c10c.png', null, null, '2016-06-30 19:57:23', '2016-06-30 19:57:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3231', '10000001', 'text', '下方的短期支撑322附近', null, null, '2016-06-30 19:58:01', '2016-06-30 19:58:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3232', '10000001', 'text', '今晚的空单，大家要有耐心', null, null, '2016-06-30 20:03:58', '2016-06-30 20:03:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3233', '10000001', 'text', '路透调查：今年美原油均价为43.9美元/桶，5月调查为42美元/桶', null, null, '2016-06-30 20:06:06', '2016-06-30 20:06:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3234', '10000194', 'text', '#7现在还可以建单吗', null, null, '2016-06-30 20:09:57', '2016-06-30 20:09:57', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3235', '10000001', 'text', '6点多的时候我已经发了操作建议了，现在最好就建个轻仓，带好止盈止损', '10000194', '#7现在还可以建单吗', '2016-06-30 20:14:34', '2016-06-30 20:14:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3236', '10000194', 'text', '#2#2不开心了。坐等你们赚钱', null, null, '2016-06-30 20:16:50', '2016-06-30 20:16:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3237', '10000001', 'text', '不着急，冲动是魔鬼#9', '10000194', '#2#2不开心了。坐等你们赚钱', '2016-06-30 20:19:50', '2016-06-30 20:19:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3238', '10000001', 'text', '20:30将公布：美国至6月25日当周初请失业金人数', null, null, '2016-06-30 20:20:27', '2016-06-30 20:20:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3239', '10000194', 'text', '#4#4我等八点半', null, null, '2016-06-30 20:20:32', '2016-06-30 20:20:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3240', '10000001', 'text', '英国央行行长在年度报告中表示：英国脱欧的影响是广泛的', null, null, '2016-06-30 20:24:50', '2016-06-30 20:24:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3241', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160630/577510e58ed87.png', null, null, '2016-06-30 20:30:29', '2016-06-30 20:30:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3242', '10000001', 'text', '数据利空美元，利多油价', null, null, '2016-06-30 20:30:57', '2016-06-30 20:30:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3243', '10000194', 'text', '涨吗#8', null, null, '2016-06-30 20:31:48', '2016-06-30 20:31:48', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3244', '10000001', 'text', '但截至今周，美国当周初请失业金人数为连续69周处于30万人门槛下方', null, null, '2016-06-30 20:32:00', '2016-06-30 20:32:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3245', '10000170', 'text', '初请利多，老师，空单继续持有吗？', null, null, '2016-06-30 20:32:32', '2016-06-30 20:32:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3246', '10000001', 'text', '明就业市场增长仍处在扩张趋势上', null, null, '2016-06-30 20:32:42', '2016-06-30 20:32:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3247', '10000170', 'text', '利多不大吧，老师', null, null, '2016-06-30 20:32:54', '2016-06-30 20:32:54', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3248', '10000001', 'text', '不会马上影响油价的', '10000194', '涨吗#8', '2016-06-30 20:33:07', '2016-06-30 20:33:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3249', '10000001', 'text', '可以持有', '10000170', '初请利多，老师，空单继续持有吗？', '2016-06-30 20:33:16', '2016-06-30 20:33:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3250', '10000194', 'text', '那我在等等吧#6', null, null, '2016-06-30 20:33:36', '2016-06-30 20:33:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3251', '10000001', 'text', '与预期相差不大，所以数据影响油价不会很大', '10000170', '利多不大吧，老师', '2016-06-30 20:34:52', '2016-06-30 20:34:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3252', '10000009', 'text', '今天多加了资金，老师晚上有点位可以操作，麻烦及时通知#9', null, null, '2016-06-30 20:35:51', '2016-06-30 20:35:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3253', '10000001', 'text', '好，我会在喊单栏贴出的#9', '10000009', '今天多加了资金，老师晚上有点位可以操作，麻烦及时通知#9', '2016-06-30 20:37:19', '2016-06-30 20:37:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3254', '10000006', 'text', '行情没什么反应啊', null, null, '2016-06-30 20:40:31', '2016-06-30 20:40:31', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3255', '10000006', 'text', '不是利多吗', null, null, '2016-06-30 20:40:36', '2016-06-30 20:40:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3256', '10000001', 'text', '数据与预期相差不大，只相差1000人，所以油价的影响不会很大', '10000006', '不是利多吗', '2016-06-30 20:42:20', '2016-06-30 20:42:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3257', '10000001', 'text', '尽管今晚美国上周初请人数小幅增加，但与预期相差不超多1万人，所以今晚的数据对油价影响不会太大', null, null, '2016-06-30 20:49:47', '2016-06-30 20:49:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3258', '10000001', 'text', '且四周均值并没有上升', null, null, '2016-06-30 20:50:05', '2016-06-30 20:50:05', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3259', '10000001', 'text', '今晚数据与美国强劲的就业市场保持一致', null, null, '2016-06-30 20:50:51', '2016-06-30 20:50:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3260', '10000001', 'text', '所以今晚的空单还是可以持有的', null, null, '2016-06-30 20:52:36', '2016-06-30 20:52:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3261', '10000001', 'text', '要有耐心哦', null, null, '2016-06-30 20:55:39', '2016-06-30 20:55:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3262', '10000001', 'text', '目前油价下落得比较缓慢', null, null, '2016-06-30 21:04:34', '2016-06-30 21:04:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3263', '10000170', 'text', '市场有事件因素影响么', null, null, '2016-06-30 21:06:53', '2016-06-30 21:06:53', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3264', '10000001', 'text', '据路透，今晚欧洲央行6月货币政策会议纪要调整至下周四公布，所以今晚油价的波幅可能不会太大', '10000170', '市场有事件因素影响么', '2016-06-30 21:08:53', '2016-06-30 21:08:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3265', '10000001', 'text', '趁着目前油价走动比较慢，我们接下来讲讲，你们经常会问的问题：错过的我们老师给的建仓点位，可不可以追？', null, null, '2016-06-30 21:12:27', '2016-06-30 21:12:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3266', '10000001', 'text', '我的个人建议是：在没有什么大事件的驱动下，如果现在油价跟老师给的建仓点位没有超过20个点的，是可以追', null, null, '2016-06-30 21:15:40', '2016-06-30 21:15:40', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3267', '10000170', 'text', '最好不要追，心态差了会造成追涨杀跌', null, null, '2016-06-30 21:16:12', '2016-06-30 21:16:12', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3268', '10000001', 'text', '我的个人建议是：如果现在油价跟老师给的建仓点位没有超过20个点的，是可以比老师少一层仓去追', null, null, '2016-06-30 21:17:29', '2016-06-30 21:17:29', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3269', '10000001', 'text', '我的个人建议是：如果现在油价跟老师给的建仓点位没有超过20个点的，是可以比老师少一层仓去追', null, null, '2016-06-30 21:17:44', '2016-06-30 21:17:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3270', '10000001', 'text', '心态不好，最好不要操作喔#9', '10000170', '最好不要追，心态差了会造成追涨杀跌', '2016-06-30 21:18:20', '2016-06-30 21:18:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3271', '10000001', 'text', '路透调查：伊拉克和委内瑞拉原油产出下降，限制了欧佩克产量继续上升的势头', null, null, '2016-06-30 21:19:08', '2016-06-30 21:19:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3272', '10000194', 'text', '20个点是多少钱。', null, null, '2016-06-30 21:19:36', '2016-06-30 21:19:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3273', '10000170', 'text', '那利多了', null, null, '2016-06-30 21:19:43', '2016-06-30 21:19:43', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3274', '10000001', 'text', '是的', '10000170', '那利多了', '2016-06-30 21:20:17', '2016-06-30 21:20:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3275', '10000170', 'text', '', '10000194', '20个点是多少钱。', '2016-06-30 21:20:46', '2016-06-30 21:20:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3276', '10000001', 'text', '2块', '10000170', 'false', '2016-06-30 21:21:07', '2016-06-30 21:21:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3277', '10000170', 'text', '老师这会影响今晚的空单走势吗、', null, null, '2016-06-30 21:21:27', '2016-06-30 21:21:27', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3278', '10000001', 'text', '今晚空单获利可以先出来先', null, null, '2016-06-30 21:21:27', '2016-06-30 21:21:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3279', '10000170', 'text', '好，出', null, null, '2016-06-30 21:23:03', '2016-06-30 21:23:03', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3280', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160630/57751d68b2402.jpeg', null, null, '2016-06-30 21:23:52', '2016-06-30 21:23:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3281', '10000170', 'text', '到老师的价格了，走喽', null, null, '2016-06-30 21:25:33', '2016-06-30 21:25:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3282', '10000008', 'text', '太牛了#9', null, null, '2016-06-30 21:25:52', '2016-06-30 21:25:52', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3283', '10000001', 'text', '会，但要看那个消息面先走，可能下落了再反弹回去，也可能反弹回去再下落，所以避免风险，获利先出比较好', '10000170', '老师这会影响今晚的空单走势吗、', '2016-06-30 21:27:58', '2016-06-30 21:27:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3284', '10000001', 'text', '路透调查：预计欧佩克6月原油产出环比上升25万桶/日至3282万桶/日，为近期最高，但伊拉克和委内瑞拉原油产出下降，限制了欧佩克产量继续上升的势头', null, null, '2016-06-30 21:38:17', '2016-06-30 21:38:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3285', '10000001', 'text', '这两个消息面一个利空油价，一个利多油价，所以如果今晚油价下来了，可以在支撑位做个反弹多单', null, null, '2016-06-30 21:39:26', '2016-06-30 21:39:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3286', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5775219e9a4fa.png', null, null, '2016-06-30 21:41:50', '2016-06-30 21:41:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3287', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160630/5775234960133.png', null, null, '2016-06-30 21:48:57', '2016-06-30 21:48:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3288', '10000001', 'text', '欧洲央行利率会议纪要推迟公布，但今晚23:00，英国央行行长的讲话，有可能会引起油价的波动哦', null, null, '2016-06-30 21:50:31', '2016-06-30 21:50:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3289', '10000001', 'text', '看来今晚的支撑位反弹多单，要看英国央行行长讲话的情况了', null, null, '2016-06-30 21:57:01', '2016-06-30 21:57:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3290', '10000001', 'text', '目前油价还在324震荡，今晚的油价#12', null, null, '2016-06-30 22:03:30', '2016-06-30 22:03:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3291', '10000001', 'text', '据彭博：沙特阿美下调了8月销往亚洲的所有原油官方售价', null, null, '2016-06-30 22:10:04', '2016-06-30 22:10:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3292', '10000252', 'text', '可以多了吗', null, null, '2016-06-30 22:17:03', '2016-06-30 22:17:03', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3293', '10000001', 'text', '可以轻仓短多，带好止损止盈', '10000252', '可以多了吗', '2016-06-30 22:18:20', '2016-06-30 22:18:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3294', '10000187', 'text', '我的空单还在#7', null, null, '2016-06-30 22:19:00', '2016-06-30 22:19:00', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3295', '10000001', 'text', '空单可以先获利先出，因为23:00有英国央行行长的讲话，可能会引起油价的上涨', '10000187', '我的空单还在#7', '2016-06-30 22:20:28', '2016-06-30 22:20:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3296', '10000187', 'text', '还没获利#2', null, null, '2016-06-30 22:21:34', '2016-06-30 22:21:34', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3297', '10000001', 'text', '什么点位追进去的？', '10000187', '还没获利#2', '2016-06-30 22:21:56', '2016-06-30 22:21:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3298', '10000187', 'text', '325.1建仓，助理来电较晚#19', null, null, '2016-06-30 22:22:48', '2016-06-30 22:22:48', '-1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3299', '10000001', 'text', '个人建议：可以持有观察一下', '10000187', '325.1建仓，助理来电较晚#19', '2016-06-30 22:28:00', '2016-06-30 22:28:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3300', '10000194', 'text', '我的也是325.1', null, null, '2016-06-30 22:28:38', '2016-06-30 22:28:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3301', '10000187', 'text', '好的，谢谢老师#6', null, null, '2016-06-30 22:28:55', '2016-06-30 22:28:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3302', '10000001', 'text', '你追空了？', '10000194', '我的也是325.1', '2016-06-30 22:33:25', '2016-06-30 22:33:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3303', '10000194', 'text', '蒽？助理电话来的晚', null, null, '2016-06-30 22:36:34', '2016-06-30 22:36:34', '-1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3304', '10000001', 'text', '据彭博：沙特阿美下调了8月销往美国的原油官方售价', null, null, '2016-06-30 22:36:45', '2016-06-30 22:36:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3305', '10000187', 'text', '然后会利空吗？', '10000194', '我的也是325.1', '2016-06-30 22:38:51', '2016-06-30 22:38:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3306', '10000001', 'text', '23:00 英国央行行长面对媒体与金融从业人员作重要发言', '10000194', '蒽？助理电话来的晚', '2016-06-30 22:45:33', '2016-06-30 22:45:33', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3307', '10000194', 'text', '#2#2#2我追他跑', null, null, '2016-06-30 22:45:38', '2016-06-30 22:45:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3308', '10000001', 'text', '23:00 英国央行行长面对媒体与金融从业人员作重要发言', null, null, '2016-06-30 22:46:12', '2016-06-30 22:46:12', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3309', '10000001', 'text', '23:00 英国央行行长面对媒体与金融从业人员作重要发言', null, null, '2016-06-30 22:46:40', '2016-06-30 22:46:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3310', '10000194', 'text', '耐心等待，无眠夜#9', '10000187', '然后会利空吗？', '2016-06-30 22:47:08', '2016-06-30 22:47:08', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3311', '10000001', 'text', '要看今晚英国央行行长的讲话', '10000187', '然后会利空吗？', '2016-06-30 22:49:10', '2016-06-30 22:49:10', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3312', '10000001', 'text', ' 要看今晚英国央行行长的讲话', '10000187', '然后会利空吗？', '2016-06-30 22:49:34', '2016-06-30 22:49:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3313', '10000001', 'text', '冲动是魔鬼啊#9', '10000194', '#2#2#2我追他跑', '2016-06-30 22:49:55', '2016-06-30 22:49:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3314', '10000194', 'text', '#11#11#11后悔了，以后超老师两块钱都不追了，宁愿空着', null, null, '2016-06-30 22:50:25', '2016-06-30 22:50:25', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3315', '10000001', 'text', '有大事件大行情的时候是可以追的，主要是今晚的欧洲央行利率会议纪要延迟公布了，为油价提供支撑', '10000194', '#11#11#11后悔了，以后超老师两块钱都不追了，宁愿空着', '2016-06-30 22:52:51', '2016-06-30 22:52:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3316', '10000194', 'text', '没追好不好，我也是听话乖乖做单了。只是有点小倒霉#7', null, null, '2016-06-30 22:54:27', '2016-06-30 22:54:27', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3317', '10000001', 'text', '没事，行情天天有，不用在乎一时的得失#9', '10000194', '没追好不好，我也是听话乖乖做单了。只是有点小倒霉#7', '2016-06-30 22:56:29', '2016-06-30 22:56:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3318', '10000194', 'text', '#21#21#21把失去的赚回来，目前为止还是亏的#16', null, null, '2016-06-30 22:58:37', '2016-06-30 22:58:37', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3319', '10000001', 'text', '油价开始走伊拉克和委内瑞拉原油产出下降这个利多消息面了', null, null, '2016-06-30 23:00:02', '2016-06-30 23:00:02', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3320', '10000001', 'text', '油价目前有所反弹', null, null, '2016-06-30 23:00:27', '2016-06-30 23:00:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3321', '10000001', 'text', '持有空单的还是不用着急，目前没有上探到压力位', null, null, '2016-06-30 23:02:16', '2016-06-30 23:02:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3322', '10000001', 'text', '英国央行行长卡尼：英国央行很可能将不得不在夏天放松政策', null, null, '2016-06-30 23:02:50', '2016-06-30 23:02:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3323', '10000001', 'text', '言语偏利空油价', null, null, '2016-06-30 23:03:26', '2016-06-30 23:03:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3324', '10000001', 'text', '油价闻声开始小幅下降', null, null, '2016-06-30 23:04:05', '2016-06-30 23:04:05', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3325', '10000194', 'text', '#4#4叫行长大点声说话', null, null, '2016-06-30 23:05:05', '2016-06-30 23:05:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3326', '10000001', 'text', '英国央行行长卡尼：英国脱欧或带来金融环境收紧的风险', null, null, '2016-06-30 23:08:04', '2016-06-30 23:08:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3327', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160630/57753634ab6cf.png', null, null, '2016-06-30 23:09:40', '2016-06-30 23:09:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3328', '10000001', 'text', '反弹都位，短空', null, null, '2016-06-30 23:09:57', '2016-06-30 23:09:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3329', '10000194', 'text', '这回被我逮到了，行动#9', null, null, '2016-06-30 23:10:26', '2016-06-30 23:10:26', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3330', '10000001', 'text', '英国央行行长卡尼：令人不安的事实是，英国央行无法完全化解脱欧给经济造成的巨大冲击', null, null, '2016-06-30 23:11:00', '2016-06-30 23:11:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3331', '10000001', 'text', '还没有给出支撑英国经济的言语', null, null, '2016-06-30 23:12:04', '2016-06-30 23:12:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3332', '10000194', 'text', '小河同学你不在了吗？赶紧补一点#21', null, null, '2016-06-30 23:13:09', '2016-06-30 23:13:09', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3333', '10000001', 'text', '英国央行行长卡尼：不确定性可能为经济活动带来持续拖累', null, null, '2016-06-30 23:19:59', '2016-06-30 23:19:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3334', '10000001', 'text', '目前油下落得比较慢啊，大家今晚的空单有利润就要跑了', null, null, '2016-06-30 23:22:52', '2016-06-30 23:22:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3335', '10000194', 'text', '回本就跑#2', null, null, '2016-06-30 23:23:37', '2016-06-30 23:23:37', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3336', '10000194', 'text', '#4夜宵钱也不赚了', null, null, '2016-06-30 23:24:48', '2016-06-30 23:24:48', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3337', '10000001', 'text', '英镑短线下跌都150余点，油价才下来这么一点', null, null, '2016-06-30 23:26:21', '2016-06-30 23:26:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3338', '10000194', 'text', '324还是可以到的吧老师#2', null, null, '2016-06-30 23:28:44', '2016-06-30 23:28:44', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3339', '10000001', 'text', '看来目前市场对英国的风险情绪不太感冒了', null, null, '2016-06-30 23:28:58', '2016-06-30 23:28:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3340', '10000001', 'text', '要有耐心#9', '10000194', '324还是可以到的吧老师#2', '2016-06-30 23:30:14', '2016-06-30 23:30:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3341', '10000194', 'text', '静静等待', null, null, '2016-06-30 23:31:31', '2016-06-30 23:31:31', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3342', '10000001', 'text', '英国央行行长卡尼：英镑走软导致实质收入减少，对贸易的提振可能因不确定性而受限', null, null, '2016-06-30 23:35:56', '2016-06-30 23:35:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3343', '10000001', 'text', '目前英国行长的言语还没有提到提振英国经济', null, null, '2016-06-30 23:43:27', '2016-06-30 23:43:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3344', '10000194', 'text', '提振会干嘛', null, null, '2016-06-30 23:46:05', '2016-06-30 23:46:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3345', '10000001', 'text', '英国央行将在8月份做出一个完整的预期，再决定是否采用货币宽松政策', null, null, '2016-06-30 23:46:13', '2016-06-30 23:46:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3346', '10000001', 'text', '采取货币宽松政策', '10000194', '提振会干嘛', '2016-06-30 23:46:48', '2016-06-30 23:46:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3347', '10000001', 'text', '英国央行行长卡尼：英国央行正在应对对经济的“不确定性冲击”', null, null, '2016-06-30 23:47:47', '2016-06-30 23:47:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3348', '10000194', 'text', '几毛钱的煎熬#7', null, null, '2016-06-30 23:47:54', '2016-06-30 23:47:54', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3349', '10000001', 'text', '英国央行行长卡尼：尽快采取政策行动是合理的', null, null, '2016-06-30 23:48:25', '2016-06-30 23:48:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3350', '10000001', 'text', '英国央行行长卡尼在伦敦的讲话结束', null, null, '2016-06-30 23:48:32', '2016-06-30 23:48:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3351', '10000001', 'text', '整体英国央行行长的讲话都是偏利空油价的', null, null, '2016-06-30 23:49:20', '2016-06-30 23:49:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3352', '10000001', 'text', '#17', '10000194', '几毛钱的煎熬#7', '2016-06-30 23:49:29', '2016-06-30 23:49:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3353', '10000194', 'text', '#13#13#13', null, null, '2016-06-30 23:51:05', '2016-06-30 23:51:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3354', '10000001', 'text', '英国央行行长的讲话对油价的影响不太给力啊', null, null, '2016-06-30 23:52:59', '2016-06-30 23:52:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3355', '10000001', 'text', '讲了差不多一小时，油价下来了3块都不到啊', null, null, '2016-06-30 23:53:44', '2016-06-30 23:53:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3356', '10000194', 'text', '从讲#7', null, null, '2016-06-30 23:55:01', '2016-06-30 23:55:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3357', '10000001', 'text', '个人建议：大家今晚的空单有利润就跑了，避免风险，因为今天的油价下落得太慢了', null, null, '2016-06-30 23:56:30', '2016-06-30 23:56:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3358', '10000194', 'text', '终于等到你，还好没放弃。跑路#9', null, null, '2016-06-30 23:59:20', '2016-06-30 23:59:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3359', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160630/577541eb8fe85.png', null, null, '2016-06-30 23:59:39', '2016-06-30 23:59:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3360', '10000001', 'text', '终于开始动了', null, null, '2016-06-30 23:59:52', '2016-06-30 23:59:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3361', '10000001', 'text', '#17', '10000194', '终于等到你，还好没放弃。跑路#9', '2016-07-01 00:00:00', '2016-07-01 00:00:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3362', '10000194', 'text', '#13#13#13老是不对机会卖了就跌，不卖就耗着', null, null, '2016-07-01 00:00:28', '2016-07-01 00:00:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3363', '10000194', 'text', '没爱了', null, null, '2016-07-01 00:00:50', '2016-07-01 00:00:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3364', '10000001', 'text', '没事，睡个美容觉，养足精神，明天再战#9', '10000194', '#13#13#13老是不对机会卖了就跌，不卖就耗着', '2016-07-01 00:01:46', '2016-07-01 00:01:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3365', '10000001', 'text', '留得青山在，不怕没行情#9', '10000194', '没爱了', '2016-07-01 00:02:22', '2016-07-01 00:02:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3366', '10000194', 'text', '#9#9#9好的,老师晚安。么么哒#6', null, null, '2016-07-01 00:03:04', '2016-07-01 00:03:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3367', '10000001', 'text', '晚安#9', '10000194', '#9#9#9好的,老师晚安。么么哒#6', '2016-07-01 00:03:22', '2016-07-01 00:03:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3368', '10000001', 'text', '据彭博：欧洲央行据称考虑放宽QE规则，英国退欧令资产池规模下降', null, null, '2016-07-01 00:09:37', '2016-07-01 00:09:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3369', '10000001', 'text', '英国央行行长讲话的结束，风险情绪减缓，油价喀什有小幅反弹了', null, null, '2016-07-01 00:13:02', '2016-07-01 00:13:02', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3370', '10000001', 'text', '这油价#12', null, null, '2016-07-01 00:13:17', '2016-07-01 00:13:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3371', '10000001', 'text', '英国央行行长讲话的结束，风险情绪减缓，油价小幅反弹了', null, null, '2016-07-01 00:14:21', '2016-07-01 00:14:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3372', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/577546eb0c584.png', null, null, '2016-07-01 00:20:59', '2016-07-01 00:20:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3373', '10000001', 'text', '油价开始小幅反弹，继续走伊拉克和委内瑞拉原油产出下降这个利多消息面了', null, null, '2016-07-01 00:21:59', '2016-07-01 00:21:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3374', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57754793b9b67.jpeg', null, null, '2016-07-01 00:23:47', '2016-07-01 00:23:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3375', '10000001', 'text', '空单获利先出', null, null, '2016-07-01 00:24:04', '2016-07-01 00:24:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3376', '10000001', 'text', '目前油价反弹趋势', null, null, '2016-07-01 00:24:40', '2016-07-01 00:24:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3377', '10000001', 'text', '预测后半夜油价将会反弹到328附近震荡', null, null, '2016-07-01 00:36:10', '2016-07-01 00:36:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3378', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57754aeff2032.png', null, null, '2016-07-01 00:38:08', '2016-07-01 00:38:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3379', '10000001', 'text', '还有人没睡的吗#9', null, null, '2016-07-01 00:39:41', '2016-07-01 00:39:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3380', '10000185', 'text', '有#9老师', null, null, '2016-07-01 00:40:19', '2016-07-01 00:40:19', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3381', '10000001', 'text', '这是看欧洲杯的节奏吗#17', '10000185', '有#9老师', '2016-07-01 00:41:54', '2016-07-01 00:41:54', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3382', '10000176', 'text', '那最好', null, null, '2016-07-01 00:42:25', '2016-07-01 00:42:25', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3383', '10000176', 'text', '#9', null, null, '2016-07-01 00:42:49', '2016-07-01 00:42:49', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3384', '10000001', 'text', '今晚支持那队啊#17', '10000176', '那最好', '2016-07-01 00:43:56', '2016-07-01 00:43:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3385', '10000185', 'text', '老师；我看您点评到直播结束，敬业吧#4', null, null, '2016-07-01 00:45:46', '2016-07-01 00:45:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3386', '10000001', 'text', '太感动了#9', '10000185', '老师；我看您点评到直播结束，敬业吧#4', '2016-07-01 00:47:05', '2016-07-01 00:47:05', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3387', '10000001', 'text', '今晚油价反弹几率还是蛮大的', null, null, '2016-07-01 00:48:28', '2016-07-01 00:48:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3388', '10000185', 'text', '老师是不是今晚还可以做多呀', null, null, '2016-07-01 00:49:36', '2016-07-01 00:49:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3389', '10000001', 'text', '个人建议：现价还是可以短线多单的，带好止损止盈', '10000185', '老师是不是今晚还可以做多呀', '2016-07-01 00:49:57', '2016-07-01 00:49:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3390', '10000185', 'text', '老师止盈止损位置怎么设啊', null, null, '2016-07-01 00:52:00', '2016-07-01 00:52:00', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3391', '10000001', 'text', '轻仓短多，止损321，止盈329.（个人建议，仅供参考）#9', '10000185', '老师止盈止损位置怎么设啊', '2016-07-01 00:53:33', '2016-07-01 00:53:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3392', '10000185', 'text', '#9', null, null, '2016-07-01 00:54:28', '2016-07-01 00:54:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3393', '10000185', 'text', '老师麻烦看一下纸条提问', null, null, '2016-07-01 01:00:24', '2016-07-01 01:00:24', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3394', '10000001', 'text', '[|@梅 老师麻烦看一下纸条提问|好的', '10000185', '老师麻烦看一下纸条提问', '2016-07-01 01:04:05', '2016-07-01 01:04:05', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3395', '10000001', 'text', 'PIMCO：美联储有可能在今年加息一次', null, null, '2016-07-01 01:16:10', '2016-07-01 01:16:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3396', '10000001', 'text', '目前市场普遍预期，美联储今年只加息一次', null, null, '2016-07-01 01:17:20', '2016-07-01 01:17:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3397', '10000001', 'text', '下周五美国6月季调后非农就业人口，届时又会引起油价的波动', null, null, '2016-07-01 01:21:59', '2016-07-01 01:21:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3398', '10000001', 'text', '今晚的油价真的走得太#12', null, null, '2016-07-01 01:27:57', '2016-07-01 01:27:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3399', '10000001', 'text', '明天油价的走势很有可能走一波反弹', null, null, '2016-07-01 01:29:48', '2016-07-01 01:29:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3400', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5775575169fff.png', null, null, '2016-07-01 01:30:57', '2016-07-01 01:30:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3401', '10000001', 'text', '好了，今晚的直播就到这里了，谢谢大家的收看#9', null, null, '2016-07-01 01:32:40', '2016-07-01 01:32:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3402', '10000001', 'text', '晚安#9', null, null, '2016-07-01 01:34:13', '2016-07-01 01:34:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3403', '10000001', 'text', '今晚C罗万岁！！！！#17', null, null, '2016-07-01 01:34:45', '2016-07-01 01:34:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3404', '10000185', 'text', '老师晚安。', null, null, '2016-07-01 01:42:46', '2016-07-01 01:42:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3405', '8', 'text', '大家早上好！#9', null, null, '2016-07-01 09:28:25', '2016-07-01 09:28:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3406', '10000102', 'text', '老师早！', null, null, '2016-07-01 09:29:04', '2016-07-01 09:29:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3407', '8', 'text', '每个人的身上，都依附着两个自己：好的自己和坏的自己。让积极打败消极，让高尚打败鄙陋，让真诚打败虚伪，让宽容打败计较，让快乐打败忧郁，让勤奋打败懒惰，让坚强打败脆弱。只要你愿意，你就能做最好的自己。新的一天，早安！', null, null, '2016-07-01 09:30:10', '2016-07-01 09:30:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3408', '8', 'text', 'Queenie早#9', '10000102', '老师早！', '2016-07-01 09:30:45', '2016-07-01 09:30:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3409', '10000102', 'text', '\n#6老师，今天大概什么方向？', null, null, '2016-07-01 09:31:14', '2016-07-01 09:31:14', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3410', '10000103', 'text', '大家早上好！#9', null, null, '2016-07-01 09:31:49', '2016-07-01 09:31:49', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3411', '10000103', 'text', '先回顾下昨天的行情吧', null, null, '2016-07-01 09:32:41', '2016-07-01 09:32:41', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3412', '8', 'text', '别着急喔，待会我会在直播室给出策略的。#17', '10000102', '\n#6老师，今天大概什么方向？', '2016-07-01 09:32:47', '2016-07-01 09:32:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3413', '10000103', 'text', '原油价格周四(6月30日)收盘下跌7.3，跌幅2.97%，报322.6元/桶', null, null, '2016-07-01 09:33:58', '2016-07-01 09:33:58', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3414', '10000103', 'text', '油价重挫受尼日利亚及加拿大恢复原油供应拖累，今日凌晨英国被正式下调评级更致使油价短线急跌，且交易商在第二季末获利了结。', null, null, '2016-07-01 09:38:17', '2016-07-01 09:38:17', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3415', '10000102', 'text', '\n好的，谢谢老师！#6', null, null, '2016-07-01 09:39:24', '2016-07-01 09:39:24', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3416', '10000103', 'text', '不过，油价本季却录得七年最大升幅。', null, null, '2016-07-01 09:39:47', '2016-07-01 09:39:47', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3417', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5775ca6322e66.jpeg', null, null, '2016-07-01 09:41:55', '2016-07-01 09:41:55', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3418', '10000185', 'text', '#9老师早上好！', null, null, '2016-07-01 09:43:16', '2016-07-01 09:43:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3419', '10000103', 'text', '早上好#6#6', '10000185', '#9老师早上好！', '2016-07-01 09:44:05', '2016-07-01 09:44:05', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3420', '10000109', 'text', '早安#9', null, null, '2016-07-01 09:52:34', '2016-07-01 09:52:34', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3421', '10000103', 'text', ' 昨晚行情这么大，来看看我们的直播室给大家带来的利润！', null, null, '2016-07-01 09:54:12', '2016-07-01 09:54:12', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3422', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5775cd55354cc.png', null, null, '2016-07-01 09:54:29', '2016-07-01 09:54:29', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3423', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5775cd5f665e8.png', null, null, '2016-07-01 09:54:39', '2016-07-01 09:54:39', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3424', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5775cd6b1c204.png', null, null, '2016-07-01 09:54:51', '2016-07-01 09:54:51', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3425', '10000103', 'text', '三单共计获利23.3%+39.4%+25.5%=88.2%！！', null, null, '2016-07-01 09:57:07', '2016-07-01 09:57:07', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3426', '10000103', 'text', '#14#14#14', null, null, '2016-07-01 09:57:52', '2016-07-01 09:57:52', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3427', '10000103', 'text', '老师们的操作建议好给力！！大家把握机会了吗', null, null, '2016-07-01 09:59:49', '2016-07-01 09:59:49', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3428', '10000103', 'text', '这么大的行情，还需要犹豫吗？？赶紧加入我们吧', null, null, '2016-07-01 10:00:46', '2016-07-01 10:00:46', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3429', '10000103', 'text', '#4#4', null, null, '2016-07-01 10:00:58', '2016-07-01 10:00:58', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3430', '10000103', 'text', '早上好亲#1', '10000109', '早安#9', '2016-07-01 10:01:19', '2016-07-01 10:01:19', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3431', '10000103', 'text', '回顾完昨天的行情，把直播室交给伍老师，为大家进行股票的解读吧~', null, null, '2016-07-01 10:03:16', '2016-07-01 10:03:16', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3432', '8', 'text', '好的，谢谢小榆老师！#9', '10000109', '早安#9', '2016-07-01 10:04:04', '2016-07-01 10:04:04', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3433', '8', 'text', '好的，谢谢小榆老师！', null, null, '2016-07-01 10:04:40', '2016-07-01 10:04:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3434', '8', 'text', '下面我们一起来看看今天的股市情况。', null, null, '2016-07-01 10:05:15', '2016-07-01 10:05:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3435', '8', 'text', '昨日欧美股市再次大涨，纷纷收复英国脱欧造成的下跌。A股也在近期完成了中期底部的构筑。', null, null, '2016-07-01 10:07:40', '2016-07-01 10:07:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3436', '8', 'text', '6月交易结束，A股所有指数顶住了诸多的重大利空(未能纳入MSCI指数、联储加息的不确定，英国脱欧公投，加之传统意义上的半年底资金荒)', null, null, '2016-07-01 10:11:55', '2016-07-01 10:11:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3437', '8', 'text', '6月份以小阳线报收，收复了5月移动平均线，月线的KDJ指标出现了右侧金叉，显示中期底部形成。', null, null, '2016-07-01 10:16:33', '2016-07-01 10:16:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3438', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5775d2f25c7c0.png', null, null, '2016-07-01 10:18:26', '2016-07-01 10:18:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3439', '8', 'text', '沪指已经冲破2900点压制，后面将对2800~2950点箱体顶部发动攻势。', null, null, '2016-07-01 10:21:00', '2016-07-01 10:21:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3440', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5775d4d17b3a4.png', null, null, '2016-07-01 10:26:25', '2016-07-01 10:26:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3441', '8', 'text', '7月中报行情将展开，随着超跌蓝筹股的估值修复行情和中小创等高成长股的高送转预期再次升温，市场将迎来吃饭行情。#17', null, null, '2016-07-01 10:31:59', '2016-07-01 10:31:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3442', '8', 'text', '【消息面】昨日，证监会新闻发言人张晓军表示，证监会已同意由中国证券投资基金业协会发布问题解答，明确外商独资和合资私募证券基金管理机构有关资质和登记备案事宜，同时，外资机构在境内开展私募证券基金管理业务，需在境内设立机构，在境内非公开募集资金，投资境内资本市场，为境内合格投资者提供资产管理服务，不涉及跨境资本流动。', null, null, '2016-07-01 10:35:19', '2016-07-01 10:35:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3443', '8', 'text', '登多的境外基金进入中国市场将为中国A股市场注入大股活水', null, null, '2016-07-01 10:42:29', '2016-07-01 10:42:29', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3444', '10000176', 'text', '老师今天多单进场点位在哪里？', null, null, '2016-07-01 10:43:23', '2016-07-01 10:43:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3445', '8', 'text', '昨天离岸人民币盘间一度跌穿6.7关口至6.7078，触及1月份以来最低水平，连带影响在岸人民币走贬。', null, null, '2016-07-01 10:43:35', '2016-07-01 10:43:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3446', '8', 'text', '目前价格在走震荡反弹，先耐心观望吧。', '10000176', '老师今天多单进场点位在哪里？', '2016-07-01 10:47:16', '2016-07-01 10:47:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3447', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5775da5649029.png', null, null, '2016-07-01 10:49:58', '2016-07-01 10:49:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3448', '8', 'text', '昨日美元/人民币急跌后快速反弹，今日继续走高。', null, null, '2016-07-01 10:52:34', '2016-07-01 10:52:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3449', '8', 'text', '央行也随即发布声明，谴责少数媒体在关键时点误导舆论，扰乱外汇市场正常秩序。', null, null, '2016-07-01 10:53:47', '2016-07-01 10:53:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3450', '8', 'text', '今日，次新股表现强势，涨停潮再现。', null, null, '2016-07-01 11:01:27', '2016-07-01 11:01:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3451', '8', 'text', '上海沪工等10余股涨停。', null, null, '2016-07-01 11:01:59', '2016-07-01 11:01:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3452', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5775dd9d1abb2.png', null, null, '2016-07-01 11:03:57', '2016-07-01 11:03:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3453', '8', 'text', '盘面上看，车联网、量子通信、无人驾驶、物流电商等板块涨幅居前，白酒、电子竞技、苹果概念等板块跌幅居前。', null, null, '2016-07-01 11:06:39', '2016-07-01 11:06:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3454', '8', 'text', '　汽车电子概念股表现强劲，昨天次新股集体走弱对市场人气影响较大，在无新的领涨热点形成，同时老题材又不温不火的时期，短期股指调整压力有所增大。', null, null, '2016-07-01 11:09:16', '2016-07-01 11:09:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3455', '8', 'text', '今日两市资金净流入接近7亿元，较上一个交易日，资金结束连续流出转为流入。', null, null, '2016-07-01 11:12:40', '2016-07-01 11:12:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3456', '8', 'text', '因为今天7月1日，是香港特别行政区成立纪念日，沪股通及港股通今日关闭交易。', null, null, '2016-07-01 11:13:48', '2016-07-01 11:13:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3457', '8', 'text', '回到盘面', null, null, '2016-07-01 11:22:10', '2016-07-01 11:22:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3458', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5775e1e9a53d4.png', null, null, '2016-07-01 11:22:17', '2016-07-01 11:22:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3459', '8', 'text', '今天的支撑位置关注五日均线的2922点，压力位置先看能否站稳前期的2950高点。', null, null, '2016-07-01 11:26:59', '2016-07-01 11:26:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3460', '8', 'text', '目前来看，周四市场是一个变盘时间点，监管层通过多次行政干预来压制市场炒作次新股和妖股的意图，投资者应当注意不要过分追涨涨幅过大的品种。', null, null, '2016-07-01 11:32:04', '2016-07-01 11:32:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3461', '10000013', 'text', '老师，今早怎么操作？', null, null, '2016-07-01 11:45:13', '2016-07-01 11:45:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3462', '8', 'text', '好的，下面就讲到了。', '10000176', '老师今天多单进场点位在哪里？', '2016-07-01 11:48:45', '2016-07-01 11:48:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3463', '8', 'text', '下面进入讲解。', '10000013', '老师，今早怎么操作？', '2016-07-01 11:49:03', '2016-07-01 11:49:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3464', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5775e8376ea5e.png', null, null, '2016-07-01 11:49:11', '2016-07-01 11:49:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3465', '8', 'text', '看来朋友们都等不及了', null, null, '2016-07-01 11:50:24', '2016-07-01 11:50:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3466', '8', 'text', '昨日的大跌，短期看已有见顶的趋势，今天凌晨的急跌，价格出现震荡反弹，上方关键的压力位在326.8附近一线，操作上，若反弹到326.8附近受阻可短线空单尝试，注意控制仓位，带好止盈止损。', null, null, '2016-07-01 11:58:59', '2016-07-01 11:58:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3467', '8', 'text', '好了，大家做单时，也不要忘了吃饭，身体才是革命的本钱，赚钱了就好好款待自己，咱们下午见。#4', null, null, '2016-07-01 12:05:43', '2016-07-01 12:05:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3468', '8', 'text', '大家下午好#9', null, null, '2016-07-01 12:58:59', '2016-07-01 12:58:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3469', '8', 'text', '大家中午都休息好了吗', null, null, '2016-07-01 13:04:25', '2016-07-01 13:04:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3470', '8', 'text', '心理学家指出，人在放松和愉快的环境下，往往能创造高效或奇迹。#20', null, null, '2016-07-01 13:07:17', '2016-07-01 13:07:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3471', '8', 'text', '同样的道理，一个身心状况不好的投资者在交易时是十分危险的，只有保持精、气、神、脑均处于一个良好的状态，才能使投资者的的判断更为准确，情绪更为稳定。', null, null, '2016-07-01 13:08:05', '2016-07-01 13:08:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3472', '8', 'text', '大家都提起精神，咱们接下来讲讲今天的数据。', null, null, '2016-07-01 13:11:51', '2016-07-01 13:11:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3473', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5775fc1fd8430.png', null, null, '2016-07-01 13:14:07', '2016-07-01 13:14:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3474', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5775fc5346ce7.png', null, null, '2016-07-01 13:14:59', '2016-07-01 13:14:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3475', '8', 'text', '今天重点关注以上两个数据。', null, null, '2016-07-01 13:16:58', '2016-07-01 13:16:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3476', '8', 'text', '第一个是晚上9点45分美国6月Markit制造业PMI终值数据。预测值和前值是一样的。', null, null, '2016-07-01 13:20:09', '2016-07-01 13:20:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3477', '8', 'text', '这项数据是市场调查公司Markit对约600家制造类企业的经理人进行调查，内容包括受访者对就业、产出、新订单、价格、库存等方面的评估。', null, null, '2016-07-01 13:21:46', '2016-07-01 13:21:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3478', '8', 'text', '它是反映经济状况的领先指标，因企业能对市场环境迅速做出反应。外汇投资者通过观察这类数据的变化，对美国经济前景作出判断，进而影响到美元走势。\n', null, null, '2016-07-01 13:25:37', '2016-07-01 13:25:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3479', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57760010a048e.png', null, null, '2016-07-01 13:30:56', '2016-07-01 13:30:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3480', '8', 'text', '从上图的统计数据我们可以看到，美国Markit制造业PMI终值数据呈现逐月下降的趋势，反应出美国制造业的情况不容乐观。', null, null, '2016-07-01 13:34:50', '2016-07-01 13:34:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3481', '8', 'text', '所以，若晚间公布的数值小于预测值则是利空美元，利多原油。', null, null, '2016-07-01 13:37:53', '2016-07-01 13:37:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3482', '8', 'text', '下面我们再来分析下本周最后一个重磅数据——次日凌晨1:00的美国至7月1日当周石油钻井总数。', null, null, '2016-07-01 13:43:20', '2016-07-01 13:43:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3483', '8', 'text', '这是一个目前影响力和EIA库存相仿的数据。#20', null, null, '2016-07-01 13:47:13', '2016-07-01 13:47:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3484', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/577604a233576.png', null, null, '2016-07-01 13:50:26', '2016-07-01 13:50:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3485', '8', 'text', '2014年以来，由于油价大跌，原油行业收入骤减，与油价在100美元/桶之上时相比较，美国活跃钻机数减少了近30%。', null, null, '2016-07-01 13:54:11', '2016-07-01 13:54:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3486', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/577605b457b42.png', null, null, '2016-07-01 13:55:00', '2016-07-01 13:55:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3487', '8', 'text', '鉴于今年2月份以来，国际油价反弹已经达到一倍，并已经长时间维持在50美元/桶附近，部分美国生产商开足马力恢复生产，有机构甚至预计，未来低成本产油区将出现爆发性增长，钻井数量将增加40%。', null, null, '2016-07-01 14:05:24', '2016-07-01 14:05:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3488', '10000102', 'text', '跌了，现在可以干空吗？', null, null, '2016-07-01 14:06:18', '2016-07-01 14:06:18', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3489', '8', 'text', '但由于美国已经走出宽松周期进入了加息的紧缩周期，原油企业融资压力大，生产成本高，利润不断下降，多数生产商的动作依然缓慢。', null, null, '2016-07-01 14:12:33', '2016-07-01 14:12:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3490', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57760a09bf17c.png', null, null, '2016-07-01 14:13:29', '2016-07-01 14:13:29', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3491', '8', 'text', '可以，上方压力326.8，下方支撑321，带好止盈止损。', '10000102', '跌了，现在可以干空吗？', '2016-07-01 14:14:53', '2016-07-01 14:14:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3492', '10000102', 'text', '止盈止损设在哪里？压力支撑之外吗？', null, null, '2016-07-01 14:16:40', '2016-07-01 14:16:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3493', '8', 'text', '空单，止盈一般设支撑上方，止损设压力上方，多单则相反。', '10000102', '止盈止损设在哪里？压力支撑之外吗？', '2016-07-01 14:18:20', '2016-07-01 14:18:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3494', '10000102', 'text', '明白', null, null, '2016-07-01 14:23:55', '2016-07-01 14:23:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3495', '8', 'text', '#20', '10000102', '明白', '2016-07-01 14:24:13', '2016-07-01 14:24:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3496', '8', 'text', '继续回到数据解读。', null, null, '2016-07-01 14:24:47', '2016-07-01 14:24:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3497', '8', 'text', '美国油企在这种对增不增产拿捏不定的时候，加上美国原油库存近期已经呈现下跌格局的情况下，原油钻井数据就会成为下半年原油走势的关键数据！', null, null, '2016-07-01 14:24:58', '2016-07-01 14:24:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3498', '8', 'text', '下面我们看看往期的数据公布，原油的走势。', null, null, '2016-07-01 14:26:43', '2016-07-01 14:26:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3499', '8', 'text', '由于原油钻井数据是在周六凌晨1点公布，所以数据主要影响的是当晚凌晨和周一的早盘。', null, null, '2016-07-01 14:29:22', '2016-07-01 14:29:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3500', '8', 'text', '1.  6月25日凌晨，钻井小幅下降，小幅利多原油。', null, null, '2016-07-01 14:30:05', '2016-07-01 14:30:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3501', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57760e11370f9.png', null, null, '2016-07-01 14:30:41', '2016-07-01 14:30:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3502', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57760e399635f.png', null, null, '2016-07-01 14:31:21', '2016-07-01 14:31:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3503', '8', 'text', '2.  6月18日凌晨，钻井上升，小幅利空原油。', null, null, '2016-07-01 14:32:31', '2016-07-01 14:32:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3504', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57760e9c637a9.png', null, null, '2016-07-01 14:33:00', '2016-07-01 14:33:00', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3505', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57760edee1f31.png', null, null, '2016-07-01 14:34:07', '2016-07-01 14:34:07', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3506', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57760f6045da2.png', null, null, '2016-07-01 14:36:16', '2016-07-01 14:36:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3507', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57760f7f4efd4.png', null, null, '2016-07-01 14:36:47', '2016-07-01 14:36:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3508', '8', 'text', '3.  6月11日凌晨，钻井上升，小幅利空。', null, null, '2016-07-01 14:37:12', '2016-07-01 14:37:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3509', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57760fad8d9fc.png', null, null, '2016-07-01 14:37:33', '2016-07-01 14:37:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3510', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57760fccd88c2.png', null, null, '2016-07-01 14:38:04', '2016-07-01 14:38:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3511', '8', 'text', '4.  6月4日凌晨，钻井上升，利空原油。', null, null, '2016-07-01 14:38:31', '2016-07-01 14:38:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3512', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57760ffe4c1d4.png', null, null, '2016-07-01 14:38:54', '2016-07-01 14:38:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3513', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5776101cab556.png', null, null, '2016-07-01 14:39:24', '2016-07-01 14:39:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3514', '8', 'text', '作为每一周最后的一个原油数据，钻井数据经常是被国内投资者忽视的数据。', null, null, '2016-07-01 14:40:22', '2016-07-01 14:40:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3515', '8', 'text', '但从上面的历史数据看出，这个被忽视的，往往却是一个巨大的宝藏！#20', null, null, '2016-07-01 14:40:50', '2016-07-01 14:40:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3516', '8', 'text', '若是错过了周三的EIA，今晚的钻井数据将是本周最后的盛宴，是绝对的不容错过。#4', null, null, '2016-07-01 14:45:22', '2016-07-01 14:45:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3517', '10000000', 'text', '大家下午好！伍老师下午好！', null, null, '2016-07-01 14:50:24', '2016-07-01 14:50:24', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3518', '10000000', 'text', '今天大宗商品都在上涨，只有原油没有怎么动。上方面临较强的压力，我们现价进场博取短线空单，止损止盈带好。个人建议仅供参考', null, null, '2016-07-01 14:51:20', '2016-07-01 14:51:20', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3519', '8', 'text', '郑老师刚刚给出了一个短线的操作策略，大家可以根据自己的风格，做出适当参考。#9', null, null, '2016-07-01 14:54:16', '2016-07-01 14:54:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3520', '8', 'text', '今天的数据刚才已经分析的差不多了，接下来，咱们还是继续昨天的基础课程讲解。多了解是有好处的。', null, null, '2016-07-01 14:58:40', '2016-07-01 14:58:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3521', '8', 'text', '昨天我们将到移动平均线的用法，大家还记得多少呢？平时看盘的时候也可以自己多代入试试独立分析，看看实时效果如何。', null, null, '2016-07-01 15:00:54', '2016-07-01 15:00:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3522', '8', 'text', '好了，废话不多说，下面进入正题。', null, null, '2016-07-01 15:01:46', '2016-07-01 15:01:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3523', '8', 'text', '买入信号：价格趋势线低过平均线，突然暴跌，远离平均线之时，极可能再趋向平均线，是买进时机。', null, null, '2016-07-01 15:04:08', '2016-07-01 15:04:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3524', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57761787029ef.png', null, null, '2016-07-01 15:11:03', '2016-07-01 15:11:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3525', '8', 'text', '买入信号：价格趋势线走在平均线之上，价格突然下跌，但未跌破平均线，价格又上升时，可以加码买进机会。', null, null, '2016-07-01 15:13:12', '2016-07-01 15:13:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3526', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5776195026bb8.png', null, null, '2016-07-01 15:18:40', '2016-07-01 15:18:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3527', '8', 'text', '今天股市已经收盘，我们也先来看看。', null, null, '2016-07-01 15:20:48', '2016-07-01 15:20:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3528', '10000102', 'text', '原油还要涨吗？', null, null, '2016-07-01 15:21:02', '2016-07-01 15:21:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3529', '8', 'text', '三大股指受欧美股市三连涨的影响小幅高开，盘中军工股迅速走强，带动沪指小幅冲高，随后沪指在高位维持震荡整理态势。午后开盘，盘面热点逐渐降温，沪指震荡下挫翻绿，临近13：40，黄金概念崛起力挺大盘，沪指企稳回升吹响反攻号角，临近尾盘，沪指持续窄幅震荡。', null, null, '2016-07-01 15:21:59', '2016-07-01 15:21:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3530', '8', 'text', '目前价格仍处于窄幅震荡之中，但是从4小时看，下方被布林中轨支撑，价格价格是否还要上涨，有待进一步信号确认。', '10000102', '原油还要涨吗？', '2016-07-01 15:26:30', '2016-07-01 15:26:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3531', '8', 'text', '股市截至收盘，沪指报2932.82点，涨幅0.1%，深成指报10458.43点，跌幅0.3%，创业板报2211.18点，跌幅0.75%。', null, null, '2016-07-01 15:27:02', '2016-07-01 15:27:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3532', '8', 'text', '成交量方面，沪市成交1730亿元，深市成交3473亿元，两市今日共成交5203亿元。', null, null, '2016-07-01 15:27:41', '2016-07-01 15:27:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3533', '8', 'text', '盘面上，次新股、军工股、黄金概念涨幅居前，采掘服务、足球概念领跌。', null, null, '2016-07-01 15:28:22', '2016-07-01 15:28:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3534', '8', 'text', '目前沪指均线系统呈现出多头向上的格局，但上方筹码较为密集，日线KDJ过高，有调整需求，60分钟的KDJ发散向下，显示调整仍将延续，股指继续保持震荡盘整蓄势的状态。', null, null, '2016-07-01 15:30:21', '2016-07-01 15:30:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3535', '8', 'text', '综合看，外部利空因素出尽，加之近几个月的经济数据表明货币政策收紧的可能性不存在，对未来我们可以保持乐观。', null, null, '2016-07-01 15:31:37', '2016-07-01 15:31:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3536', '8', 'text', '今天股市的情况就到这里，下面继续课程讲解。', null, null, '2016-07-01 15:40:52', '2016-07-01 15:40:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3537', '8', 'text', '下面是讲解如何使用均线看卖出信号。', null, null, '2016-07-01 15:43:06', '2016-07-01 15:43:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3538', '8', 'text', '卖出信号：1.平均线走势从上升逐渐走平，而价格从平均线的上方往下跌破平均线时，是卖出的机会。', null, null, '2016-07-01 15:43:49', '2016-07-01 15:43:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3539', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57761fd986483.png', null, null, '2016-07-01 15:46:33', '2016-07-01 15:46:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3540', '8', 'text', '卖出信号：2.价格虽上升突破平均线，但又立刻回复到平均线之下，而且平均线仍然继续下跌时，是卖出时机。', null, null, '2016-07-01 15:50:32', '2016-07-01 15:50:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3541', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5776220b5a03f.png', null, null, '2016-07-01 15:55:55', '2016-07-01 15:55:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3542', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57762268c4992.png', null, null, '2016-07-01 15:57:28', '2016-07-01 15:57:28', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3543', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5776226e85600.png', null, null, '2016-07-01 15:57:34', '2016-07-01 15:57:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3544', '8', 'text', '数据利多，但是这两项数据对原油影响有限。', null, null, '2016-07-01 15:58:35', '2016-07-01 15:58:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3545', '8', 'text', '今天的课程先讲到这里，下面我们回到原油。#4', null, null, '2016-07-01 16:02:24', '2016-07-01 16:02:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3546', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/577623da84bba.png', null, null, '2016-07-01 16:03:38', '2016-07-01 16:03:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3547', '8', 'text', '今天又到本周的“黑色星期五”了，白天的行情目前一直处于窄幅震荡走势。可能为了晚上的行情爆发蓄力。#20', null, null, '2016-07-01 16:10:37', '2016-07-01 16:10:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3548', '8', 'text', '英国脱欧令全球各大央行新推宽松预期上升，美元预期将走软，投资者做多原油等大宗商品价格。', null, null, '2016-07-01 16:19:57', '2016-07-01 16:19:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3549', '8', 'text', '价格出现下挫', null, null, '2016-07-01 16:31:35', '2016-07-01 16:31:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3550', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57762a6dc8311.png', null, null, '2016-07-01 16:31:41', '2016-07-01 16:31:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3551', '8', 'text', '关注下方4小时布林中轨支撑319-320', null, null, '2016-07-01 16:32:26', '2016-07-01 16:32:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3552', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57762ca1725ac.png', null, null, '2016-07-01 16:41:05', '2016-07-01 16:41:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3553', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57762ee4e1d61.png', null, null, '2016-07-01 16:50:45', '2016-07-01 16:50:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3554', '8', 'text', '上面是美元指数，我们从图上可以看到，短线下跌受阻反弹，这对原油多头不利。', null, null, '2016-07-01 16:53:30', '2016-07-01 16:53:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3555', '8', 'text', '空单的朋友耐心持有，注意关注支撑位319-320.', null, null, '2016-07-01 16:56:04', '2016-07-01 16:56:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3556', '5', 'text', '#1', null, null, '2016-07-01 16:58:43', '2016-07-01 16:58:43', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3557', '8', 'text', '影帝#20', '5', '#1', '2016-07-01 17:01:37', '2016-07-01 17:01:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3558', '8', 'text', '下面是我们郑老师的时间，大家来点掌声。#14', null, null, '2016-07-01 17:03:01', '2016-07-01 17:03:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3559', '10000000', 'text', '\n大家下午好！伍老师辛苦了！', null, null, '2016-07-01 17:03:53', '2016-07-01 17:03:53', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3560', '10000000', 'text', '下午323.5的空单已经开始有利润，仓位超过2成的朋友可以先减仓获利，留下一点仓位看看能否继续往下', null, null, '2016-07-01 17:04:54', '2016-07-01 17:04:54', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3561', '10000000', 'text', '因为近来行情还没有打破305~330大区间，行情反复无常，我们要学会保护自己的利润。', null, null, '2016-07-01 17:05:55', '2016-07-01 17:05:55', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3562', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160701/577632ced57cb.jpg', null, null, '2016-07-01 17:07:26', '2016-07-01 17:07:26', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3563', '10000000', 'text', '目前行情已经震荡偏弱，今晚我们还要看看数据能否助推一把', null, null, '2016-07-01 17:17:56', '2016-07-01 17:17:56', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3564', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5776357265955.jpg', null, null, '2016-07-01 17:18:42', '2016-07-01 17:18:42', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3565', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160701/577635c79e1eb.jpg', null, null, '2016-07-01 17:20:07', '2016-07-01 17:20:07', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3566', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160701/577635fe2c0af.jpg', null, null, '2016-07-01 17:21:02', '2016-07-01 17:21:02', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3567', '10000000', 'text', '现在我们就是要等看看行情能否回落到短期支撑320下方', null, null, '2016-07-01 17:25:44', '2016-07-01 17:25:44', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3568', '10000000', 'text', '而美元指数方面，近期的强势也是打压原油的一大因素', null, null, '2016-07-01 17:32:50', '2016-07-01 17:32:50', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3569', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160701/577638d090051.jpg', null, null, '2016-07-01 17:33:04', '2016-07-01 17:33:04', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3570', '10000000', 'text', '由于下周一是美国独立日，所以下周的API和EIA数据推迟一天公布，大家留意哦', null, null, '2016-07-01 17:42:19', '2016-07-01 17:42:19', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3571', '10000000', 'text', '下午给的空单是一个中线空单，大家可以控制好仓位中线持有', null, null, '2016-07-01 17:44:49', '2016-07-01 17:44:49', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3572', '10000001', 'text', '索罗斯投资秘诀之一：以更广阔的视野，辩证的思维去看待市场，时刻保持清醒的头脑，处惊不乱，从容应对，最终获得成功', null, null, '2016-07-01 18:02:16', '2016-07-01 18:02:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3573', '10000001', 'text', '我是轩湛投资观察室的吴勇坚，接下来我来为大家做直播，大家有什么疑问或者疑惑，可以在互动区跟我交流喔', null, null, '2016-07-01 18:02:40', '2016-07-01 18:02:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3574', '10000001', 'text', '好了，接下里我们一起看一下目前油价的走势', null, null, '2016-07-01 18:08:39', '2016-07-01 18:08:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3575', '10000001', 'text', '油价受到昨晚英国央行行长讲话英国退欧后带来的后续风险情绪', null, null, '2016-07-01 18:11:51', '2016-07-01 18:11:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3576', '10000001', 'text', '和伊朗沙特增产的影响，使今日油价是有所回落的', null, null, '2016-07-01 18:12:08', '2016-07-01 18:12:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3577', '10000001', 'text', '目前油价是下落到支撑位附近了', null, null, '2016-07-01 18:14:38', '2016-07-01 18:14:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3578', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/577642959473b.png', null, null, '2016-07-01 18:14:45', '2016-07-01 18:14:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3579', '10000001', 'text', '所以，在支撑位附近是可以尝试轻仓的短线多单（个人建议，仅供参考）', null, null, '2016-07-01 18:16:46', '2016-07-01 18:16:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3580', '10000194', 'text', '涨吗#9', null, null, '2016-07-01 18:18:53', '2016-07-01 18:18:53', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3581', '10000001', 'text', '支撑位，短线反弹轻仓多单#9（个人建议，仅供参考）', '10000194', '涨吗#9', '2016-07-01 18:20:58', '2016-07-01 18:20:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3582', '10000194', 'text', '嗯', null, null, '2016-07-01 18:22:00', '2016-07-01 18:22:00', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3583', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5776451ba76ae.jpeg', null, null, '2016-07-01 18:25:31', '2016-07-01 18:25:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3584', '10000001', 'text', '支撑位，短线轻仓多单（个人建议，仅供参考）', null, null, '2016-07-01 18:26:32', '2016-07-01 18:26:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3585', '10000194', 'text', '#21坐等收菜了', null, null, '2016-07-01 18:27:00', '2016-07-01 18:27:00', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3586', '10000001', 'text', '#9', '10000194', '#21坐等收菜了', '2016-07-01 18:29:18', '2016-07-01 18:29:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3587', '10000001', 'text', '好了，现在行情还是比较清淡的', null, null, '2016-07-01 18:33:03', '2016-07-01 18:33:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3588', '10000001', 'text', '如果听我在这一直在讲行情，想必也是无聊', null, null, '2016-07-01 18:34:04', '2016-07-01 18:34:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3589', '10000001', 'text', '那我就讲讲，怎么操作可以在减少风险的情况下，获得一个较为稳健的利润', null, null, '2016-07-01 18:37:48', '2016-07-01 18:37:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3590', '10000194', 'text', '#14#14#14', null, null, '2016-07-01 18:38:09', '2016-07-01 18:38:09', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3591', '10000170', 'text', '#14', null, null, '2016-07-01 18:39:00', '2016-07-01 18:39:00', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3592', '10000001', 'text', '想必很多人一操作就是四层仓以上的', null, null, '2016-07-01 18:39:39', '2016-07-01 18:39:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3593', '10000001', 'text', '没错，这么操作赚得很快', null, null, '2016-07-01 18:41:27', '2016-07-01 18:41:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3594', '10000194', 'text', '我两到三层', null, null, '2016-07-01 18:41:51', '2016-07-01 18:41:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3595', '10000001', 'text', '但又没想过行情是没有百分百的', null, null, '2016-07-01 18:42:21', '2016-07-01 18:42:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3596', '10000001', 'text', '如果你是以一种赌博的心态来投资这个原油，那你还不如去赌场玩个德州扑克来得瘾', null, null, '2016-07-01 18:44:00', '2016-07-01 18:44:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3597', '10000001', 'text', '这种操作仓位很好#9', '10000194', '我两到三层', '2016-07-01 18:44:39', '2016-07-01 18:44:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3598', '10000194', 'text', '#2老看别人赚钱#4', null, null, '2016-07-01 18:45:32', '2016-07-01 18:45:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3599', '10000001', 'text', '你投资原油，出发点就是理财，稳健的收益才是最重要的', '10000194', '#2老看别人赚钱#4', '2016-07-01 18:47:06', '2016-07-01 18:47:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3600', '10000194', 'text', '也是，细水长流#21', null, null, '2016-07-01 18:48:16', '2016-07-01 18:48:16', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3601', '10000194', 'text', '每次止盈都会设比老师们的少一块钱，#19胆子太小不行#11', null, null, '2016-07-01 18:51:03', '2016-07-01 18:51:03', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3602', '10000001', 'text', '举个例子，如果你现在是看空的，现在油价走到了压力位附近，你想建四层仓，我的个人建议是：可以先两层仓空单，等着油价下来两块了，方向好像对了，那么加一层仓，油价又下来了一块，那再加一层仓，刚好四层仓', null, null, '2016-07-01 18:53:15', '2016-07-01 18:53:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3603', '10000001', 'text', '虽然收益是少了，但你的风险降低了，两层仓就算方向错了，我亏损也没多少啊', null, null, '2016-07-01 18:55:14', '2016-07-01 18:55:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3604', '10000170', 'text', '#4能做原油的胆都不小', null, null, '2016-07-01 18:57:36', '2016-07-01 18:57:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3605', '10000194', 'text', '有道理#20', null, null, '2016-07-01 18:57:59', '2016-07-01 18:57:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3606', '10000194', 'text', '饿死胆小的，#4', '10000170', '#4能做原油的胆都不小', '2016-07-01 18:58:28', '2016-07-01 18:58:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3607', '10000001', 'text', '有获利就好，不用取得那么准的，谁能确定到了点位就反向了#9', '10000194', '每次止盈都会设比老师们的少一块钱，#19胆子太小不行#11', '2016-07-01 18:58:55', '2016-07-01 18:58:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3608', '10000194', 'text', '我的观点是两块钱就跑路#4', null, null, '2016-07-01 19:00:40', '2016-07-01 19:00:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3609', '10000001', 'text', '#17', '10000194', '我的观点是两块钱就跑路#4', '2016-07-01 19:01:54', '2016-07-01 19:01:54', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3610', '10000170', 'text', '', '10000194', '我的观点是两块钱就跑路#4', '2016-07-01 19:07:48', '2016-07-01 19:07:48', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3611', '10000001', 'text', '咦，目前欧洲央行执委对英国退欧后续影响的讲话，让油价开始有点蠢蠢欲动ing', null, null, '2016-07-01 19:09:24', '2016-07-01 19:09:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3612', '10000170', 'text', '跌么', null, null, '2016-07-01 19:13:37', '2016-07-01 19:13:37', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3613', '10000194', 'text', '#19本来就是', '10000170', 'false', '2016-07-01 19:15:07', '2016-07-01 19:15:07', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3614', '10000001', 'text', '多单不用着急的，油价还在支撑位徘徊', '10000170', '跌么', '2016-07-01 19:15:14', '2016-07-01 19:15:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3615', '10000001', 'text', '欧洲央行执委的讲话还没看到引起油价波动的言语，轻仓短线反弹的多单还是可以持有的', null, null, '2016-07-01 19:19:31', '2016-07-01 19:19:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3616', '10000001', 'text', '今晚可以适当关注：21:45 美国6月Markit制造业PMI终值，届时看能不能引起油价的波动', null, null, '2016-07-01 19:25:34', '2016-07-01 19:25:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3617', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5776534a6c55e.png', null, null, '2016-07-01 19:26:02', '2016-07-01 19:26:02', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3618', '10000001', 'text', '英国司法大臣：预计年内不会启动第50条款', null, null, '2016-07-01 19:36:03', '2016-07-01 19:36:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3619', '10000001', 'text', '那么英国退欧的后续影响可能有所减缓', null, null, '2016-07-01 19:37:32', '2016-07-01 19:37:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3620', '10000001', 'text', '目前油价还是没有太大的走动，较为清淡啊', null, null, '2016-07-01 19:40:22', '2016-07-01 19:40:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3621', '10000001', 'text', '看得我都变无聊了#17', null, null, '2016-07-01 19:40:51', '2016-07-01 19:40:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3622', '10000001', 'text', '据路透援引消息人士：北海EKOFISK油田8月计划装载量为11船，7月时为14船，6月3船', null, null, '2016-07-01 19:45:36', '2016-07-01 19:45:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3623', '10000001', 'text', '所以今晚的反弹短线多单要小心点。有获利就出', null, null, '2016-07-01 19:46:25', '2016-07-01 19:46:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3624', '10000194', 'text', '陪聊陪聊没人有意见吧#9', null, null, '2016-07-01 19:51:42', '2016-07-01 19:51:42', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3625', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5776598f6bbe5.png', null, null, '2016-07-01 19:52:47', '2016-07-01 19:52:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3626', '10000252', 'text', '多单现在平？', null, null, '2016-07-01 19:53:25', '2016-07-01 19:53:25', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3627', '10000001', 'text', '不急，短期压力位在324附近，还没到', '10000252', '多单现在平？', '2016-07-01 19:54:12', '2016-07-01 19:54:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3628', '10000001', 'text', '#17', '10000194', '陪聊陪聊没人有意见吧#9', '2016-07-01 19:54:37', '2016-07-01 19:54:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3629', '10000128', 'text', '短多完  我们空吗？', null, null, '2016-07-01 19:55:34', '2016-07-01 19:55:34', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3630', '10000001', 'text', '我知道我的直播风格很无聊，我会努力改进的#17', '10000194', '陪聊陪聊没人有意见吧#9', '2016-07-01 19:55:38', '2016-07-01 19:55:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3631', '10000252', 'text', '三克油   老师', null, null, '2016-07-01 19:55:52', '2016-07-01 19:55:52', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3632', '10000194', 'text', '聊到325在平#9', null, null, '2016-07-01 19:56:07', '2016-07-01 19:56:07', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3633', '10000194', 'text', '挺好的#14', null, null, '2016-07-01 19:56:22', '2016-07-01 19:56:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3634', '10000001', 'text', '可以轻仓带好止损止盈#9', '10000128', '短多完  我们空吗？', '2016-07-01 19:58:50', '2016-07-01 19:58:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3635', '10000001', 'text', '#9', '10000252', '三克油   老师', '2016-07-01 20:02:05', '2016-07-01 20:02:05', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3636', '10000001', 'text', '哈哈，不是两块吗', '10000194', '聊到325在平#9', '2016-07-01 20:04:29', '2016-07-01 20:04:29', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3637', '10000001', 'text', ' 哈哈，不是两块吗', '10000194', '聊到325在平#9', '2016-07-01 20:04:46', '2016-07-01 20:04:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3638', '10000194', 'text', '我要有野心比你多#5#5#5', null, null, '2016-07-01 20:05:21', '2016-07-01 20:05:21', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3639', '10000128', 'text', '好的', null, null, '2016-07-01 20:07:04', '2016-07-01 20:07:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3640', '10000001', 'text', '我的野心是获利就跑，因为我做的短线反弹#9', '10000194', '我要有野心比你多#5#5#5', '2016-07-01 20:09:30', '2016-07-01 20:09:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3641', '10000001', 'text', '据路透援引消息人士：8月北海奥塞贝格、伊科菲斯克、福尔蒂斯和布伦特原油四种原油供给将从89万桶/日降至83.2万桶/日', null, null, '2016-07-01 20:10:52', '2016-07-01 20:10:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3642', '10000194', 'text', '恩啦，知道了。两块钱，#9', null, null, '2016-07-01 20:11:09', '2016-07-01 20:11:09', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3643', '10000001', 'text', '那么对接下来的油价将可能提供支撑', null, null, '2016-07-01 20:11:33', '2016-07-01 20:11:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3644', '10000001', 'text', '据路透：英国北海Forties输油管道8月将装载20批原油；7月计划是21批', null, null, '2016-07-01 20:20:41', '2016-07-01 20:20:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3645', '10000001', 'text', '好像非OPEC，8月都在减产什么的', null, null, '2016-07-01 20:23:57', '2016-07-01 20:23:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3646', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5776618df32ab.png', null, null, '2016-07-01 20:26:54', '2016-07-01 20:26:54', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3647', '10000001', 'text', '哎呀，油价还是下落了', null, null, '2016-07-01 20:26:59', '2016-07-01 20:26:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3648', '10000001', 'text', '没破320支撑位之前，轻仓短多还是可以持有的', null, null, '2016-07-01 20:28:11', '2016-07-01 20:28:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3649', '10000001', 'text', '还是需要耐心等待啊', null, null, '2016-07-01 20:29:36', '2016-07-01 20:29:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3650', '10000001', 'text', '【温馨提示】因下周一美国独立日休市，原定于下周三公布的API、ADP、EIA数据将分别推迟至下周四04:30、20:15、23:00公布，而6月非农报告将于下周五20:30公布', null, null, '2016-07-01 20:34:36', '2016-07-01 20:34:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3651', '10000001', 'text', '推迟一天', null, null, '2016-07-01 20:34:46', '2016-07-01 20:34:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3652', '10000170', 'text', '今晚可能没行情了吗', null, null, '2016-07-01 20:35:37', '2016-07-01 20:35:37', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3653', '10000001', 'text', '到现在油价还在盘整，这情况油价后半夜会有蛮大的波动的', '10000170', '今晚可能没行情了吗', '2016-07-01 20:37:00', '2016-07-01 20:37:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3654', '10000170', 'text', '哦等', null, null, '2016-07-01 20:39:59', '2016-07-01 20:39:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3655', '10000001', 'text', '好了，竟然行情上去一点又下来一点，这种行情可以讲讲交易心态的问题', null, null, '2016-07-01 20:41:32', '2016-07-01 20:41:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3656', '10000001', 'text', '这种行情，很容易引起心情的浮躁，到底涨不涨？跌不跌？', null, null, '2016-07-01 20:43:26', '2016-07-01 20:43:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3657', '10000001', 'text', '现在的行情又没有什么消息面利空油价，又没有跌破支撑位，你问我油价会不会跌下去，短线来讲我也不知道', null, null, '2016-07-01 20:46:31', '2016-07-01 20:46:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3658', '10000001', 'text', '一个平静的心态是很重要的', null, null, '2016-07-01 20:48:35', '2016-07-01 20:48:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3659', '10000001', 'text', '如果你的心情是浮躁，很有可能会引起是不是要涨了？空单平了，哎怎么又跌多单又平，折腾了一晚上', null, null, '2016-07-01 20:52:01', '2016-07-01 20:52:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3660', '10000001', 'text', '所以根据我以往的经验，给大家一个个人建议：如果你今晚对这个油价的心情是浮躁的，那最好不要做单了。因为行情天天有，不着急一时，长远稳健的收益才是最重要的', null, null, '2016-07-01 20:54:53', '2016-07-01 20:54:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3661', '10000001', 'text', '好吧，我又冷场了#17', null, null, '2016-07-01 20:58:47', '2016-07-01 20:58:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3662', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57766969c02eb.png', null, null, '2016-07-01 21:00:25', '2016-07-01 21:00:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3663', '8', 'text', '中期有见顶趋势，反弹无力，中线空单操作，止损止盈带好。个人建议，仅供参考。#9', null, null, '2016-07-01 21:00:39', '2016-07-01 21:00:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3664', '10000001', 'text', '伍老师#14', null, null, '2016-07-01 21:01:59', '2016-07-01 21:01:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3665', '10000001', 'text', '伍老师今晚陪我们看欧洲杯吗#17', null, null, '2016-07-01 21:03:57', '2016-07-01 21:03:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3666', '10000194', 'text', '我教你压球#17', null, null, '2016-07-01 21:05:08', '2016-07-01 21:05:08', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3667', '10000128', 'text', '321.5可以空了是吗？', null, null, '2016-07-01 21:07:33', '2016-07-01 21:07:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3668', '10000001', 'text', '我教你怎样带球进自己龙门#17', '10000194', '我教你压球#17', '2016-07-01 21:08:11', '2016-07-01 21:08:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3669', '10000194', 'text', '#11#11#11我还可以带球跑了', null, null, '2016-07-01 21:09:17', '2016-07-01 21:09:17', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3670', '10000001', 'text', '伍老师的是中线空单，可以获利短多，进行布局的中线空单喔（个人建议仅供参考）', '10000128', '321.5可以空了是吗？', '2016-07-01 21:10:03', '2016-07-01 21:10:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3671', '10000001', 'text', '息怒#9', '10000194', '#11#11#11我还可以带球跑了', '2016-07-01 21:10:55', '2016-07-01 21:10:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3672', '10000128', 'text', '那现在我们可以轻仓进空 了？', null, null, '2016-07-01 21:11:10', '2016-07-01 21:11:10', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3673', '10000194', 'text', '在录歌直播#17消磨时间#4', null, null, '2016-07-01 21:13:24', '2016-07-01 21:13:24', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3674', '10000001', 'text', '可以啊，因为今晚总体是看空的，我布局的是短线反弹，目前反弹还是无力的（个人建议仅供参考）', '10000128', '那现在我们可以轻仓进空 了？', '2016-07-01 21:13:28', '2016-07-01 21:13:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3675', '10000194', 'text', '我还是多单#10', null, null, '2016-07-01 21:15:28', '2016-07-01 21:15:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3676', '10000001', 'text', '哈哈哈，要去参加我想和你唱的节奏#17', '10000194', '在录歌直播#17消磨时间#4', '2016-07-01 21:15:40', '2016-07-01 21:15:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3677', '10000194', 'text', '老师升级可以语音#17', null, null, '2016-07-01 21:16:04', '2016-07-01 21:16:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3678', '10000001', 'text', '目前反弹还是无力的，可以获利先出，按照伍老师布局中线空单（个人建议，仅供参考）', '10000194', '我还是多单#10', '2016-07-01 21:17:12', '2016-07-01 21:17:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3679', '10000194', 'text', '都还不够交手续费#7', null, null, '2016-07-01 21:17:53', '2016-07-01 21:17:53', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3680', '10000001', 'text', '我320.5布的短线，目前价格够了吧#9', '10000194', '都还不够交手续费#7', '2016-07-01 21:19:30', '2016-07-01 21:19:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3681', '10000001', 'text', '也是可以的#17', '10000194', '老师升级可以语音#17', '2016-07-01 21:20:15', '2016-07-01 21:20:15', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3682', '10000001', 'text', '也是可以的#4', '10000194', '老师升级可以语音#17', '2016-07-01 21:20:36', '2016-07-01 21:20:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3683', '10000194', 'text', '#11我321.4   五点你没出来我就建单了', null, null, '2016-07-01 21:20:58', '2016-07-01 21:20:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3684', '10000194', 'text', '比利时  独赢  老师把你家产压上#4', null, null, '2016-07-01 21:22:00', '2016-07-01 21:22:00', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3685', '10000001', 'text', '标普：将美国2016年GDP增速预期从2.3%下调至2%', null, null, '2016-07-01 21:23:06', '2016-07-01 21:23:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3686', '10000001', 'text', '看来英国退欧给美国带来不少的负面影响啊', null, null, '2016-07-01 21:23:41', '2016-07-01 21:23:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3687', '10000001', 'text', '冲动是魔鬼啊#9', '10000194', '#11我321.4   五点你没出来我就建单了', '2016-07-01 21:25:01', '2016-07-01 21:25:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3688', '10000194', 'text', '#19我们助理的建议', null, null, '2016-07-01 21:25:56', '2016-07-01 21:25:56', '-1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3689', '10000194', 'text', '卖了#17', null, null, '2016-07-01 21:26:17', '2016-07-01 21:26:17', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3690', '10000001', 'text', '压#17', '10000194', '比利时  独赢  老师把你家产压上#4', '2016-07-01 21:27:42', '2016-07-01 21:27:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3691', '10000194', 'text', '认真的，内幕消息#5', null, null, '2016-07-01 21:28:18', '2016-07-01 21:28:18', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3692', '10000170', 'text', '#17仙，魔都是一种境界啊', null, null, '2016-07-01 21:28:22', '2016-07-01 21:28:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3693', '10000001', 'text', '#17', '10000170', '#17仙，魔都是一种境界啊', '2016-07-01 21:29:22', '2016-07-01 21:29:22', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3694', '10000001', 'text', '我身家剩下最后一百块了，吃粥还是吃粥都靠你了#17', '10000194', '认真的，内幕消息#5', '2016-07-01 21:30:12', '2016-07-01 21:30:12', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3695', '10000170', 'text', '#17今晚都看欧洲杯了，你们也是挂羊头卖狗肉啊！咋会有行情#18', null, null, '2016-07-01 21:31:07', '2016-07-01 21:31:07', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3696', '10000194', 'text', '#5#5浪费我一块钱买消息', null, null, '2016-07-01 21:31:30', '2016-07-01 21:31:30', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3697', '10000001', 'text', '标普：预计美联储将在2017年加息3次', '10000194', '认真的，内幕消息#5', '2016-07-01 21:32:02', '2016-07-01 21:32:02', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3698', '10000170', 'text', '投资，你懂得', '10000194', '#5#5浪费我一块钱买消息', '2016-07-01 21:32:03', '2016-07-01 21:32:03', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3699', '10000001', 'text', '标普：预计美联储将在2017年加息3次', null, null, '2016-07-01 21:32:19', '2016-07-01 21:32:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3700', '10000194', 'text', '现在又搞空单吗', null, null, '2016-07-01 21:32:43', '2016-07-01 21:32:43', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3701', '10000194', 'text', '不压，帮老师问的', '10000170', '投资，你懂得', '2016-07-01 21:33:08', '2016-07-01 21:33:08', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3702', '10000170', 'text', '#20不讲足球，行情就来了耶', null, null, '2016-07-01 21:34:04', '2016-07-01 21:34:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3703', '10000194', 'text', '#11这行情真想爆粗，卖了就涨。什么都没捞到', null, null, '2016-07-01 21:34:42', '2016-07-01 21:34:42', '-1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3704', '10000001', 'text', '咦，看油价竟然要走一小波反弹才会下落', '10000194', '现在又搞空单吗', '2016-07-01 21:34:53', '2016-07-01 21:34:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3705', '10000194', 'text', '#13#13#13', null, null, '2016-07-01 21:35:07', '2016-07-01 21:35:07', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3706', '10000170', 'text', '要有颗平常心', null, null, '2016-07-01 21:35:46', '2016-07-01 21:35:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3707', '10000194', 'text', '#2#2#2想哭', null, null, '2016-07-01 21:35:53', '2016-07-01 21:35:53', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3708', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/577671cbe4930.png', null, null, '2016-07-01 21:36:12', '2016-07-01 21:36:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3709', '10000170', 'text', '不是不空，时辰未到', null, null, '2016-07-01 21:36:40', '2016-07-01 21:36:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3710', '10000128', 'text', '什么消息 一下子拉上去', null, null, '2016-07-01 21:36:40', '2016-07-01 21:36:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3711', '10000001', 'text', '还是维持上述建议，今晚短多，有获利就先出', null, null, '2016-07-01 21:36:46', '2016-07-01 21:36:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3712', '10000194', 'text', '#16每次都一样，心塞', '10000170', '要有颗平常心', '2016-07-01 21:38:23', '2016-07-01 21:38:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3713', '10000001', 'text', '标普下调美国2016年的GDP增速，使美元有所下降，提振欧元和油价', '10000128', '什么消息 一下子拉上去', '2016-07-01 21:38:26', '2016-07-01 21:38:26', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3714', '10000001', 'text', '标普将美国2016年GDP增速预期从2.3%下调至2%，使得美元短线下降，提振欧元和油价', null, null, '2016-07-01 21:40:02', '2016-07-01 21:40:02', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3715', '10000001', 'text', '标普将美国2016年GDP增速预期从2.3%下调至2%，利空美元，提振油价', '10000128', '什么消息 一下子拉上去', '2016-07-01 21:41:52', '2016-07-01 21:41:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3716', '10000001', 'text', '没事，走到短期压力位就下来，目前市场消息面总体还是利空油价的#9', '10000194', '#16每次都一样，心塞', '2016-07-01 21:43:09', '2016-07-01 21:43:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3717', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/5776749b0a924.png', null, null, '2016-07-01 21:48:11', '2016-07-01 21:48:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3718', '10000170', 'text', '压力位多少', null, null, '2016-07-01 21:48:59', '2016-07-01 21:48:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3719', '10000194', 'text', '利多#2', null, null, '2016-07-01 21:49:06', '2016-07-01 21:49:06', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3720', '10000001', 'text', '虽然美国6月Markit制造业PMI终值没有达到预期值，但还是比上月提高了0.6的', null, null, '2016-07-01 21:49:23', '2016-07-01 21:49:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3721', '10000001', 'text', '影响不大', '10000194', '利多#2', '2016-07-01 21:49:47', '2016-07-01 21:49:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3722', '10000001', 'text', '今晚美国6月Markit制造业PMI终值对油价是影响不大的', null, null, '2016-07-01 21:50:21', '2016-07-01 21:50:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3723', '10000194', 'text', '老是跟不上脚步#19', null, null, '2016-07-01 21:50:25', '2016-07-01 21:50:25', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3724', '10000001', 'text', '短期压力位324附近，上方330', '10000170', '压力位多少', '2016-07-01 21:50:46', '2016-07-01 21:50:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3725', '10000170', 'text', '好似昨日的初请失业金', null, null, '2016-07-01 21:50:52', '2016-07-01 21:50:52', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3726', '10000170', 'text', '324能部空么', null, null, '2016-07-01 21:51:58', '2016-07-01 21:51:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3727', '10000001', 'text', '没错，公布值跟预期相差不大，不会太大的影响美元，从而对油价影响也较小', '10000170', '好似昨日的初请失业金', '2016-07-01 21:51:59', '2016-07-01 21:51:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3728', '10000001', 'text', '可以轻仓布空单，带好止盈止损（个人建议仅供参考）', '10000170', '324能部空么', '2016-07-01 21:52:35', '2016-07-01 21:52:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3729', '10000170', 'text', '噢卡', null, null, '2016-07-01 21:53:05', '2016-07-01 21:53:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3730', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57767634d9e06.png', null, null, '2016-07-01 21:55:01', '2016-07-01 21:55:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3731', '10000001', 'text', '反弹到位，获利先出', null, null, '2016-07-01 21:55:20', '2016-07-01 21:55:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3732', '10000001', 'text', 'Markit首席经济学家：尽管因目前经济信心较为脆弱雇佣不理想，但就业指数上升暗示企业预期需求疲软只是暂时的', null, null, '2016-07-01 21:59:38', '2016-07-01 21:59:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3733', '10000001', 'text', '暗示美国就业市场依然强劲', null, null, '2016-07-01 22:00:25', '2016-07-01 22:00:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3734', '10000001', 'text', '下周的非农数据可能会有较好的表现', null, null, '2016-07-01 22:01:26', '2016-07-01 22:01:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3735', '10000001', 'text', '美联储副主席费希尔：需要等待以及观察英国脱欧对美国的影响', null, null, '2016-07-01 22:08:08', '2016-07-01 22:08:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3736', '10000001', 'text', '美联储副主席费希尔：美国经济表现良好', null, null, '2016-07-01 22:08:58', '2016-07-01 22:08:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3737', '10000001', 'text', '美国经济的强劲，对未来的油价将构成打压', null, null, '2016-07-01 22:09:34', '2016-07-01 22:09:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3738', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/577679cbaa801.png', null, null, '2016-07-01 22:10:19', '2016-07-01 22:10:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3739', '10000194', 'text', '我静静的看你说#2', null, null, '2016-07-01 22:10:49', '2016-07-01 22:10:49', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3740', '10000001', 'text', '目前油价，可以布局一个轻仓的短线空单（个人建议仅供参考）', null, null, '2016-07-01 22:11:16', '2016-07-01 22:11:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3741', '10000001', 'text', '你也可以说的#4', '10000194', '我静静的看你说#2', '2016-07-01 22:11:36', '2016-07-01 22:11:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3742', '10000194', 'text', '#16没爱了，连续四天这样，看你说#4', null, null, '2016-07-01 22:12:31', '2016-07-01 22:12:31', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3743', '10000194', 'text', '倒霉，老师来来来我们算命一下#4', null, null, '2016-07-01 22:14:28', '2016-07-01 22:14:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3744', '10000001', 'text', '美联储副主席：美联储并无计划实施负利率', '10000194', '#16没爱了，连续四天这样，看你说#4', '2016-07-01 22:15:50', '2016-07-01 22:15:50', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3745', '10000001', 'text', '美联储副主席：美联储并无计划实施负利率', null, null, '2016-07-01 22:16:06', '2016-07-01 22:16:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3746', '10000001', 'text', '暗示英国退欧对美国经济没有造成太大的的负面影响', null, null, '2016-07-01 22:16:58', '2016-07-01 22:16:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3747', '10000128', 'text', '空要止损追多吗', null, null, '2016-07-01 22:17:57', '2016-07-01 22:17:57', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3748', '10000001', 'text', '美联储副主席费希尔：预计将保持在我们一直所处的非常缓慢渐进的加息路径上', null, null, '2016-07-01 22:20:12', '2016-07-01 22:20:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3749', '10000001', 'text', '可以先持有，不要盲目追单（个人建议，仅供参考）', '10000128', '空要止损追多吗', '2016-07-01 22:22:16', '2016-07-01 22:22:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3750', '10000128', 'text', '哦啦#21', null, null, '2016-07-01 22:23:22', '2016-07-01 22:23:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3751', '10000001', 'text', '美联储副主席的讲话，使得今年美联储加息预期的有所下降，使得美元有所走低，但对油价影响有限', null, null, '2016-07-01 22:29:36', '2016-07-01 22:29:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3752', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57767f49a6af2.png', null, null, '2016-07-01 22:33:45', '2016-07-01 22:33:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3753', '10000001', 'text', '反弹到位，轻仓短线空单（个人建议，仅供参考）', null, null, '2016-07-01 22:34:16', '2016-07-01 22:34:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3754', '10000170', 'text', '老师，美元下跌的，可以布局空单？', null, null, '2016-07-01 22:39:57', '2016-07-01 22:39:57', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3755', '10000001', 'text', '美元现在有小幅反弹了，目前没有利多的消息面使油价继续上涨，接下来都是欧洲那边的领导人的风险讲话，预测会有所利空油价', '10000170', '老师，美元下跌的，可以布局空单？', '2016-07-01 22:43:27', '2016-07-01 22:43:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3756', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/577681be9b386.png', null, null, '2016-07-01 22:44:14', '2016-07-01 22:44:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3757', '10000001', 'text', '美元指数受到美联储副主席讲话的结束和欧盟委员会主席风险讲话，美元开始是有所反弹的', null, null, '2016-07-01 22:46:35', '2016-07-01 22:46:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3758', '10000001', 'text', '目前是可以短线轻仓空单的，在凌晨1点美国至7月1日当周石油钻井总数数据出来之前（个人建议，仅供参考）', '10000170', '老师，美元下跌的，可以布局空单？', '2016-07-01 22:48:31', '2016-07-01 22:48:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3759', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57768353f33d7.png', null, null, '2016-07-01 22:51:00', '2016-07-01 22:51:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3760', '10000001', 'text', '凌晨1：00 美国至7月1日当周石油钻井总数', null, null, '2016-07-01 22:51:25', '2016-07-01 22:51:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3761', '10000001', 'text', '届时将会引起油价的波动', null, null, '2016-07-01 22:52:57', '2016-07-01 22:52:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3762', '10000001', 'text', '命中缺财#17', '10000194', '倒霉，老师来来来我们算命一下#4', '2016-07-01 22:56:39', '2016-07-01 22:56:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3763', '10000001', 'text', 'FOMC票委：英国脱欧或通过持续影响美元的方式影响美国', null, null, '2016-07-01 23:06:36', '2016-07-01 23:06:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3764', '10000001', 'text', '现在市场上都是英国退欧后续对经济影响的风险讲话', null, null, '2016-07-01 23:08:13', '2016-07-01 23:08:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3765', '10000194', 'text', '缺财，改名旺财了', null, null, '2016-07-01 23:10:32', '2016-07-01 23:10:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3766', '10000001', 'text', '哈哈哈#17', '10000194', '缺财，改名旺财了', '2016-07-01 23:13:15', '2016-07-01 23:13:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3767', '10000001', 'text', '油价终于下来了', null, null, '2016-07-01 23:13:25', '2016-07-01 23:13:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3768', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/577688c888f65.png', null, null, '2016-07-01 23:14:16', '2016-07-01 23:14:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3769', '10000001', 'text', 'FOMC票委：预计就业和通胀取得持续进展', null, null, '2016-07-01 23:15:47', '2016-07-01 23:15:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3770', '10000001', 'text', 'FOMC票委票委的讲话总体偏利好美元，打压油价的', null, null, '2016-07-01 23:20:52', '2016-07-01 23:20:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3771', '10000001', 'text', '想必大家都觉得今晚油价坐了个小小的过山车那样', null, null, '2016-07-01 23:27:18', '2016-07-01 23:27:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3772', '10000001', 'text', '上去了又下来了', null, null, '2016-07-01 23:29:30', '2016-07-01 23:29:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3773', '10000001', 'text', '像今晚那种反弹，在没有很大利多油价的消息面上反弹，千万不要去盲目的去追单', null, null, '2016-07-01 23:31:20', '2016-07-01 23:31:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3774', '10000001', 'text', '这样的行情，风险是很大的', null, null, '2016-07-01 23:33:29', '2016-07-01 23:33:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3775', '10000001', 'text', '亚特兰大联储GDPNOW模型：将美国第二季度GDP增速预期从2.7%下调至2.6%', null, null, '2016-07-01 23:44:36', '2016-07-01 23:44:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3776', '10000001', 'text', '今晚的消息面是都将美国的GDP增速下调', null, null, '2016-07-01 23:46:26', '2016-07-01 23:46:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3777', '10000001', 'text', '看来会对油价的下落可能会提供一定的支撑啊', null, null, '2016-07-01 23:47:36', '2016-07-01 23:47:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3778', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160701/57769105d8661.png', null, null, '2016-07-01 23:49:25', '2016-07-01 23:49:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3779', '10000001', 'text', '下方支撑位：320', null, null, '2016-07-01 23:50:43', '2016-07-01 23:50:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3780', '10000194', 'text', '#9等', null, null, '2016-07-01 23:51:11', '2016-07-01 23:51:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3781', '10000001', 'text', '今晚的空单需要平常心和耐心喔#9', null, null, '2016-07-01 23:51:37', '2016-07-01 23:51:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3782', '10000001', 'text', '#9', '10000194', '#9等', '2016-07-01 23:52:00', '2016-07-01 23:52:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3783', '10000001', 'text', '快12点了，想必大家还没睡吧#17', null, null, '2016-07-01 23:55:06', '2016-07-01 23:55:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3784', '10000001', 'text', '没有什么数据，今晚后半夜的行情走动是有点慢的，不过凌晨1:00的美国至7月1日当周石油钻井总数会引起油价一定的波动', null, null, '2016-07-01 23:58:01', '2016-07-01 23:58:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3785', '10000001', 'text', '索罗斯的一句名言：如果操作过量，即使对市场决定正确，仍会一败涂地', null, null, '2016-07-01 23:59:21', '2016-07-01 23:59:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3786', '10000001', 'text', '所以我的个人建议：大家在操盘的时候，一天的操作最好在3次之内', null, null, '2016-07-02 00:03:19', '2016-07-02 00:03:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3787', '10000001', 'text', '还是那句话：在这个市场长久的生存下去才是最重要的，因为你是来理财的，不是来赌博的', null, null, '2016-07-02 00:07:42', '2016-07-02 00:07:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3788', '10000001', 'text', '哎，油价还在323盘旋，还没下落#12', null, null, '2016-07-02 00:18:26', '2016-07-02 00:18:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3789', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160702/577698134a804.png', null, null, '2016-07-02 00:19:31', '2016-07-02 00:19:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3790', '10000001', 'text', '来个有趣的新闻#17', null, null, '2016-07-02 00:19:51', '2016-07-02 00:19:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3791', '10000001', 'text', '哈哈，油价开始动了', null, null, '2016-07-02 00:24:52', '2016-07-02 00:24:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3792', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160702/5776995b5c800.png', null, null, '2016-07-02 00:24:59', '2016-07-02 00:24:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3793', '10000001', 'text', 'FOMC票委：油价及中国因素依然为经济预期带来风险', null, null, '2016-07-02 00:34:06', '2016-07-02 00:34:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3794', '10000001', 'text', '而且美联储副主席对中国经济增长幅度的总体预估为6%或6.5%', null, null, '2016-07-02 00:34:49', '2016-07-02 00:34:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3795', '10000001', 'text', '想必大家到了凌晨也是无聊了，有人在可以出来聊聊天', null, null, '2016-07-02 00:44:23', '2016-07-02 00:44:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3796', '10000001', 'text', '明天周末了，想必今晚的欧洲看得尽兴，哈哈', null, null, '2016-07-02 00:46:04', '2016-07-02 00:46:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3797', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160702/57769e70b603d.png', null, null, '2016-07-02 00:46:40', '2016-07-02 00:46:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3798', '10000194', 'text', '怎么上去了', null, null, '2016-07-02 00:48:13', '2016-07-02 00:48:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3799', '10000194', 'text', '老师看一下问题回答我#2', null, null, '2016-07-02 00:48:58', '2016-07-02 00:48:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3800', '10000001', 'text', '正常的回调啊，现在又没有什么消息面驱使，总不能直接来条大阴线吧', '10000194', '怎么上去了', '2016-07-02 00:50:01', '2016-07-02 00:50:01', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3801', '10000001', 'text', '正常的回调，没有什么消息面的驱使，会下得缓慢点的#9', '10000194', '怎么上去了', '2016-07-02 00:51:17', '2016-07-02 00:51:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3802', '10000194', 'text', '#13#13#13不是这个', null, null, '2016-07-02 00:51:32', '2016-07-02 00:51:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3803', '10000001', 'text', '要有耐心喔#9', '10000194', '老师看一下问题回答我#2', '2016-07-02 00:51:52', '2016-07-02 00:51:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3804', '10000001', 'text', '北京时间01:00将公布：美国至7月1日当周石油钻井总数', null, null, '2016-07-02 00:52:53', '2016-07-02 00:52:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3805', '10000128', 'text', '直播到几点哈 老师', null, null, '2016-07-02 00:56:48', '2016-07-02 00:56:48', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3806', '10000001', 'text', '我们直播室为大家直播行情到凌晨1:30的#9', '10000128', '直播到几点哈 老师', '2016-07-02 00:58:16', '2016-07-02 00:58:16', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3807', '10000001', 'text', '我们直播室为大家直播行情到凌晨1:30的#9', '10000128', '直播到几点哈 老师', '2016-07-02 00:58:37', '2016-07-02 00:58:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3808', '10000001', 'text', '该数据一般不会准时公布，将延迟数秒至数分钟不等', null, null, '2016-07-02 00:58:55', '2016-07-02 00:58:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3809', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160702/5776a1e22df03.png', null, null, '2016-07-02 01:01:22', '2016-07-02 01:01:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3810', '10000128', 'text', '利空  算大利空吗', null, null, '2016-07-02 01:01:33', '2016-07-02 01:01:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3811', '10000001', 'text', '数据是利空油价的', null, null, '2016-07-02 01:01:35', '2016-07-02 01:01:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3812', '10000128', 'text', '增加了10', null, null, '2016-07-02 01:01:39', '2016-07-02 01:01:39', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3813', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160702/5776a21442957.png', null, null, '2016-07-02 01:02:12', '2016-07-02 01:02:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3814', '10000001', 'text', '一定程度利空油价#9', '10000128', '利空  算大利空吗', '2016-07-02 01:03:59', '2016-07-02 01:03:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3815', '10000001', 'text', '石油钻井总数增加11座', '10000128', '增加了10', '2016-07-02 01:04:53', '2016-07-02 01:04:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3816', '10000001', 'text', '美国至7月1日当周石油钻井总数比上周增加11座', null, null, '2016-07-02 01:05:38', '2016-07-02 01:05:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3817', '10000001', 'text', '所以会使油价有一定程度的回落', null, null, '2016-07-02 01:06:33', '2016-07-02 01:06:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3818', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160702/5776a43ada5b1.png', null, null, '2016-07-02 01:11:22', '2016-07-02 01:11:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3819', '10000001', 'text', '今晚当周石油钻井总数录得近两个月的高位', null, null, '2016-07-02 01:12:28', '2016-07-02 01:12:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3820', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160702/5776a5bec8913.png', null, null, '2016-07-02 01:17:50', '2016-07-02 01:17:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3821', '10000001', 'text', '预测油价收盘前的走势会回落到320支撑位附近的', null, null, '2016-07-02 01:18:35', '2016-07-02 01:18:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3822', '10000001', 'text', '今晚大家的空单，到了支撑位附近可先获利减仓先，轻仓观察能否跌破支撑位', null, null, '2016-07-02 01:28:08', '2016-07-02 01:28:08', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3823', '10000001', 'text', '今晚大家的空单，到了支撑位320附近可先获利减仓先，轻仓观察能否跌破支撑位', null, null, '2016-07-02 01:28:40', '2016-07-02 01:28:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3824', '10000001', 'text', '好了，今晚我们直播室为大家直播行情就到这里了#9', null, null, '2016-07-02 01:31:44', '2016-07-02 01:31:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3825', '10000001', 'text', '祝大家有个美好的周末！#9', null, null, '2016-07-02 01:32:03', '2016-07-02 01:32:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3826', '10000001', 'text', '我们下周一见#9', null, null, '2016-07-02 01:32:14', '2016-07-02 01:32:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3827', '10000001', 'text', '大家晚安#9', null, null, '2016-07-02 01:32:21', '2016-07-02 01:32:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3828', '10000001', 'text', '大家早上好！#9', null, null, '2016-07-04 09:28:24', '2016-07-04 09:28:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3829', '10000001', 'text', '新的一周，我们轩湛投资观察室继续为大家共创财富#9', null, null, '2016-07-04 09:30:10', '2016-07-04 09:30:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3830', '10000001', 'text', '生机总在绝望时出现，当你忍无可忍时再忍五分钟;当你想放弃时，再试一次吧!', null, null, '2016-07-04 09:30:34', '2016-07-04 09:30:34', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3831', '10000001', 'text', '我是轩湛投资观察室的吴勇坚，接下来我来为大家做直播，大家有什么疑问或者疑惑，可以在互动区跟我交流喔。', null, null, '2016-07-04 09:31:05', '2016-07-04 09:31:05', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3832', '10000001', 'text', '人生的幸福，不在于富足，而在于满足', null, null, '2016-07-04 09:37:11', '2016-07-04 09:37:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3833', '10000103', 'text', '早上好！亲们#9#9#9', null, null, '2016-07-04 09:37:38', '2016-07-04 09:37:38', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3834', '10000001', 'text', '早上好，小榆老师#9#9', null, null, '2016-07-04 09:38:57', '2016-07-04 09:38:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3835', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/5779be78bc2e9.jpeg', null, null, '2016-07-04 09:40:08', '2016-07-04 09:40:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3836', '10000001', 'text', '哈哈，上一周，我们的短线多单，已经顺利止盈了哦#14', null, null, '2016-07-04 09:40:28', '2016-07-04 09:40:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3837', '10000102', 'text', '今天什么操作思路？', null, null, '2016-07-04 09:40:48', '2016-07-04 09:40:48', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3838', '10000001', 'text', '大家快跟进我们老师的脚步！，哈哈哈#9', null, null, '2016-07-04 09:41:32', '2016-07-04 09:41:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3839', '10000001', 'text', '压力位330附近短空#9', '10000102', '今天什么操作思路？', '2016-07-04 09:42:19', '2016-07-04 09:42:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3840', '10000103', 'text', '恭喜吴老师哟！', null, null, '2016-07-04 09:46:00', '2016-07-04 09:46:00', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3841', '10000103', 'text', '#14#14', null, null, '2016-07-04 09:46:07', '2016-07-04 09:46:07', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('3842', '10000001', 'text', '短线空单，日内维持获利先出原则', '10000102', '今天什么操作思路？', '2016-07-04 09:46:14', '2016-07-04 09:46:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3843', '10000194', 'text', '上周空单还有', null, null, '2016-07-04 09:46:40', '2016-07-04 09:46:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3844', '10000001', 'text', '如果超过三层仓位的先适当减减仓，目前空头比较弱，不过上周五上涨原因主要是因为尼日利亚武装组织宣称袭击了多处输油管引起，但该消息还没得到官方的证实', '10000194', '上周空单还有', '2016-07-04 09:52:43', '2016-07-04 09:52:43', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3845', '10000001', 'text', '如果超过三层仓位的先适当减减仓，目前上涨行情比较强，不过上周五上涨原因主要是因为尼日利亚武装组织宣称袭击了多处输油管引起，但该消息还没得到官方的证实', '10000194', '上周空单还有', '2016-07-04 09:55:33', '2016-07-04 09:55:33', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3846', '10000001', 'text', '如果超过三层仓位的先适当减减仓，目前上涨行情比较强，不过上周五上涨原因主要是因为尼日利亚武装组织宣称袭击了多处输油管引起，但该消息还没得到官方的证实', '10000194', '上周空单还有', '2016-07-04 09:55:48', '2016-07-04 09:55:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3847', '10000001', 'text', '好了，接下来回顾一下上周五的行情', null, null, '2016-07-04 09:57:45', '2016-07-04 09:57:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3848', '10000001', 'text', '上周五，因周四晚的欧洲央行的利率会议纪要延迟到下周才公布，市场的对英国脱欧后的欧洲经济的担忧有所减缓', null, null, '2016-07-04 09:58:46', '2016-07-04 09:58:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3849', '10000001', 'text', '同时伊拉克可和委内瑞拉的减产', null, null, '2016-07-04 09:59:08', '2016-07-04 09:59:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3850', '10000001', 'text', '使得油价支撑位320低反弹', null, null, '2016-07-04 09:59:48', '2016-07-04 09:59:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3851', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/5779c35a564cd.png', null, null, '2016-07-04 10:00:58', '2016-07-04 10:00:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3852', '10000001', 'text', '周五晚，尼日利亚武装组织宣称袭击了多处输油管引起，提振油价走到330压力位附近', null, null, '2016-07-04 10:03:09', '2016-07-04 10:03:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3853', '10000001', 'text', '但该消息未得到官方的证实', null, null, '2016-07-04 10:03:33', '2016-07-04 10:03:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3854', '10000001', 'text', '所以，目前上涨行情较强，持有325下方的空单可以先适当减仓，325上方的空单如果超过三层仓也适当减仓持有观察', null, null, '2016-07-04 10:05:14', '2016-07-04 10:05:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3855', '10000001', 'text', '好了，接下来看一下股市大盘', null, null, '2016-07-04 10:05:48', '2016-07-04 10:05:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3856', '10000001', 'text', '今日两市低开高走震荡翻红，目前上证指数报2950.67点，上涨26点', null, null, '2016-07-04 10:08:47', '2016-07-04 10:08:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3857', '10000001', 'text', '今日可以适当黄金，锂电池，稀缺资源等板块', null, null, '2016-07-04 10:12:41', '2016-07-04 10:12:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3858', '10000001', 'text', '由于今周二英国央行发布半年度金融稳定报告，周三美联储FOMC公布6月货币政策会议纪要，周四欧洲央行公布6月货币政策会议纪要，周五非农报告', null, null, '2016-07-04 10:20:52', '2016-07-04 10:20:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3859', '10000001', 'text', '所以今周市场风险事件较多，大家今周可以持续关注黄金板块的走势', null, null, '2016-07-04 10:23:12', '2016-07-04 10:23:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3860', '10000001', 'text', '还有可以持续关注一下有色治炼加工板块', null, null, '2016-07-04 10:28:43', '2016-07-04 10:28:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3861', '10000001', 'text', '目前有色治炼加工，稀缺资源，黄金，小金属板块涨势居前', null, null, '2016-07-04 10:36:48', '2016-07-04 10:36:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3862', '10000001', 'text', '因为英国脱欧引发的避险需求和对全球央行将维持利率在低位的预期，基金经理疯狂涌入黄金和白银市场，白银今日开盘到目前已经大涨7%', null, null, '2016-07-04 10:36:55', '2016-07-04 10:36:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3863', '10000102', 'text', '黄金，白银开盘大涨，而后突然下跌什么原因？', null, null, '2016-07-04 10:41:48', '2016-07-04 10:41:48', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3864', '10000001', 'text', '涨势太猛，走到了上方压力位，使得黄金和白银有所回落', '10000102', '黄金，白银开盘大涨，而后突然下跌什么原因？', '2016-07-04 10:47:39', '2016-07-04 10:47:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3865', '10000102', 'text', '刚刚美元下跌，黄金，白银都涨的很猛，但是原有没什么反应呢？', null, null, '2016-07-04 10:51:13', '2016-07-04 10:51:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3866', '10000102', 'text', '原油', null, null, '2016-07-04 10:51:20', '2016-07-04 10:51:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3867', '10000001', 'text', '原油上方有较强的压力位330.白天油价预测将会震荡为主', '10000102', '原油', '2016-07-04 10:55:08', '2016-07-04 10:55:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3868', '10000102', 'text', '震荡区间呢？', null, null, '2016-07-04 10:58:09', '2016-07-04 10:58:09', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3869', '10000001', 'text', '预测将会压力位330-320区间震荡#9', '10000102', '震荡区间呢？', '2016-07-04 11:01:50', '2016-07-04 11:01:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3870', '10000102', 'text', '谢谢老师', null, null, '2016-07-04 11:02:49', '2016-07-04 11:02:49', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3871', '10000102', 'text', '那现在空单进场，可以吗？', null, null, '2016-07-04 11:03:02', '2016-07-04 11:03:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3872', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/5779d23d8155a.png', null, null, '2016-07-04 11:04:29', '2016-07-04 11:04:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3873', '10000001', 'text', '证券板块突然拉升领涨', null, null, '2016-07-04 11:05:29', '2016-07-04 11:05:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3874', '10000001', 'text', '可以，短线轻仓为主，获利就跑（个人建议，仅供参考）', '10000102', '那现在空单进场，可以吗？', '2016-07-04 11:07:23', '2016-07-04 11:07:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3875', '10000001', 'text', '所以，今日大盘有望上探3000附近', null, null, '2016-07-04 11:09:21', '2016-07-04 11:09:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3876', '10000001', 'text', '所以，股票被套的朋友们，可以趁着现在大盘反弹到3000点附近，可以适当减仓解套', null, null, '2016-07-04 11:13:27', '2016-07-04 11:13:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3877', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/5779d5e27dc7e.png', null, null, '2016-07-04 11:20:02', '2016-07-04 11:20:02', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3878', '10000001', 'text', '根据以往的历史走势，证券板块急速拉升后，大盘会有所回落，上方压力位3000', null, null, '2016-07-04 11:21:58', '2016-07-04 11:21:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3879', '10000001', 'text', '截止收盘，证券板块拉升4.18%，华泰证券涨逾9%', null, null, '2016-07-04 11:32:36', '2016-07-04 11:32:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3880', '10000001', 'text', '棉花概念股走强，新农开发(11.180, 1.02, 10.04%)涨停', null, null, '2016-07-04 11:34:52', '2016-07-04 11:34:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3881', '10000001', 'text', '上证指数早盘收2987.47，上涨60余点，涨幅1.77%', null, null, '2016-07-04 11:37:49', '2016-07-04 11:37:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3882', '10000001', 'text', '由于券商集体拉升，涨势居前，预测午盘开盘后震荡上探到3000点附近后小幅下落', null, null, '2016-07-04 11:42:18', '2016-07-04 11:42:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3883', '10000001', 'text', '好了，我们回到原油方面', null, null, '2016-07-04 11:44:20', '2016-07-04 11:44:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3884', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/5779dbc1d8151.png', null, null, '2016-07-04 11:45:05', '2016-07-04 11:45:05', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3885', '10000102', 'text', '#14', null, null, '2016-07-04 11:45:28', '2016-07-04 11:45:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3886', '10000001', 'text', '油价今日开盘后一直在压力位330下方震荡', null, null, '2016-07-04 11:47:58', '2016-07-04 11:47:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3887', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/5779dd343c88a.png', null, null, '2016-07-04 11:51:16', '2016-07-04 11:51:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3888', '10000001', 'text', '上图看出，油价在近一个月走在下降趋势线上', null, null, '2016-07-04 11:53:25', '2016-07-04 11:53:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3889', '10000001', 'text', '所以，后市油价在没有突破330压力位情况下，技术面上还是看空为主的', null, null, '2016-07-04 11:55:19', '2016-07-04 11:55:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3890', '10000001', 'text', '好了，目前行情较为清淡，我们稍作休息一下#9', null, null, '2016-07-04 12:01:26', '2016-07-04 12:01:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3891', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/5779f2298221a.png', null, null, '2016-07-04 13:20:41', '2016-07-04 13:20:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3892', '10000001', 'text', '今日下午可以适当关注欧元区5月PPI月率', null, null, '2016-07-04 13:21:07', '2016-07-04 13:21:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3893', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/5779f29993e27.png', null, null, '2016-07-04 13:22:33', '2016-07-04 13:22:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3894', '10000001', 'text', '图中可以看出，3月份PPI录得今年的正值后，4月是有所下降', null, null, '2016-07-04 13:24:33', '2016-07-04 13:24:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3895', '10000001', 'text', '由于英国脱欧，接下来每月的PPI数据预期将会有所下调的，并且从最近公布的欧元区的数据看出，欧元区经济是有所放缓的', null, null, '2016-07-04 13:27:01', '2016-07-04 13:27:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3896', '10000001', 'text', '公布值>预测值=利好欧元，利好油价', null, null, '2016-07-04 13:27:22', '2016-07-04 13:27:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3897', '10000001', 'text', '由于今日是美国的独立日，美国纽约证券交易所休市一日', null, null, '2016-07-04 13:31:47', '2016-07-04 13:31:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3898', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/5779f4f333d50.png', null, null, '2016-07-04 13:32:35', '2016-07-04 13:32:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3899', '10000001', 'text', '所以今晚的油价的波幅可能会比较小', null, null, '2016-07-04 13:34:39', '2016-07-04 13:34:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3900', '10000001', 'text', '回到油价上', null, null, '2016-07-04 13:40:38', '2016-07-04 13:40:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3901', '10000102', 'text', '原油，暴涨了', null, null, '2016-07-04 13:41:05', '2016-07-04 13:41:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3902', '10000185', 'text', '快速上涨，什么情况？', null, null, '2016-07-04 13:41:21', '2016-07-04 13:41:21', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3903', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/5779f74edfc58.png', null, null, '2016-07-04 13:42:38', '2016-07-04 13:42:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3904', '10000006', 'text', '老师今天的股市怎么看呢', null, null, '2016-07-04 13:45:24', '2016-07-04 13:45:24', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3905', '10000001', 'text', '目前油价有所上涨是因为今早白银等大宗商品开盘大涨，带动油价是有所上升的', null, null, '2016-07-04 13:48:09', '2016-07-04 13:48:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3906', '10000001', 'text', '白银等大宗商品大涨，带动油价是有所上升的', '10000185', '快速上涨，什么情况？', '2016-07-04 13:48:59', '2016-07-04 13:48:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3907', '10000001', 'text', '证券板块拉升，今日大盘有望冲到3000点附近', '10000006', '老师今天的股市怎么看呢', '2016-07-04 13:50:12', '2016-07-04 13:50:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3908', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/5779f9a4b6835.png', null, null, '2016-07-04 13:52:36', '2016-07-04 13:52:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3909', '10000001', 'text', '油价日线级别的压力位是330附近的', null, null, '2016-07-04 13:53:08', '2016-07-04 13:53:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3910', '10000001', 'text', '所以，持有空单的朋友，如果油价冲破330，可以在331-312区间先锁仓一半', null, null, '2016-07-04 13:54:17', '2016-07-04 13:54:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3911', '10000001', 'text', '大家可以关注3点欧盘开盘后的油价的走势如何，再进行多空的布局', null, null, '2016-07-04 13:59:28', '2016-07-04 13:59:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3912', '10000185', 'text', '老师;什么是锁仓,怎么操作?您说的是上周的空单吗?', null, null, '2016-07-04 14:02:14', '2016-07-04 14:02:14', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3913', '10000001', 'text', '上周的空单冲破330压力位，可在331-332区间锁仓一半，锁仓就是：你有2手空单，然后建1手多单，就是锁仓一半', '10000185', '老师;什么是锁仓,怎么操作?您说的是上周的空单吗?', '2016-07-04 14:05:47', '2016-07-04 14:05:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3914', '10000185', 'text', '哦，谢谢老师！', null, null, '2016-07-04 14:07:00', '2016-07-04 14:07:00', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3915', '10000001', 'text', '目前，还没有重大的利好油价的消息面出现，所以今日白银等大宗商品引领油价上涨也是有限的', null, null, '2016-07-04 14:16:13', '2016-07-04 14:16:13', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3916', '10000001', 'text', '目前，还没有重大的利好油价的消息面出现，所以今日白银等大宗商品引领油价上涨也是有限的', null, null, '2016-07-04 14:16:29', '2016-07-04 14:16:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3917', '10000001', 'text', '今周周三的加22:30的美国至7月1日当周EIA原油库存，因美国独立日，将延时到周四23:00 才公布', null, null, '2016-07-04 14:23:06', '2016-07-04 14:23:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3918', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a01611f592.png', null, null, '2016-07-04 14:25:37', '2016-07-04 14:25:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3919', '10000001', 'text', '而且今周周五20:30，将公布美国6月失业率和美国6月季调后非农就业人口', null, null, '2016-07-04 14:26:44', '2016-07-04 14:26:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3920', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a01b3324b5.png', null, null, '2016-07-04 14:26:59', '2016-07-04 14:26:59', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3921', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a042f85c46.png', null, null, '2016-07-04 14:37:35', '2016-07-04 14:37:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3922', '10000001', 'text', '届时将会引起油价较大的波动', null, null, '2016-07-04 14:38:00', '2016-07-04 14:38:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3923', '10000013', 'text', '老师我的空单还在可以加仓吗？', null, null, '2016-07-04 14:39:25', '2016-07-04 14:39:25', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3924', '10000001', 'text', '个人看法：最好不要加仓，目前行情不太明朗，行情天天有，不要在乎一时的利益（个人建议，仅供参考）', '10000013', '老师我的空单还在可以加仓吗？', '2016-07-04 14:43:03', '2016-07-04 14:43:03', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3925', '10000001', 'text', '你现在仓位是多少？', '10000013', '老师我的空单还在可以加仓吗？', '2016-07-04 14:46:11', '2016-07-04 14:46:11', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3926', '10000001', 'text', '你现在仓位是多少？', '10000013', '老师我的空单还在可以加仓吗？', '2016-07-04 14:46:35', '2016-07-04 14:46:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3927', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a091846053.png', null, null, '2016-07-04 14:58:32', '2016-07-04 14:58:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3928', '10000001', 'text', '欧盘开盘后，油价似乎有所回落了', null, null, '2016-07-04 15:00:44', '2016-07-04 15:00:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3929', '10000000', 'text', '大家下午好！', null, null, '2016-07-04 15:03:20', '2016-07-04 15:03:20', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3930', '10000000', 'text', '由于今天是美国的独立日，美国股市休市。所以原油暂时行情还比较淡。', null, null, '2016-07-04 15:03:54', '2016-07-04 15:03:54', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3931', '10000000', 'text', '我们关注一下目前欧洲开市的情况', null, null, '2016-07-04 15:04:05', '2016-07-04 15:04:05', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3932', '10000000', 'text', '原油短线还是比较强势，空单要注意控制好仓位', null, null, '2016-07-04 15:10:51', '2016-07-04 15:10:51', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3933', '10000000', 'text', '由于上方330是较强的压力，持有多单仓位较重的朋友，稳健的可以先获利离场，等突破以后再操作', null, null, '2016-07-04 15:11:30', '2016-07-04 15:11:30', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3934', '10000000', 'text', '这几天大宗商品，贵金属等全线大涨，原油并没有跟上脚步，所以相对而言，原油有点滞涨', null, null, '2016-07-04 15:14:06', '2016-07-04 15:14:06', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3935', '10000000', 'text', '而今天的股市则走出了令人欣喜的上涨行情。', null, null, '2016-07-04 15:19:06', '2016-07-04 15:19:06', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3936', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a0e023a000.jpg', null, null, '2016-07-04 15:19:30', '2016-07-04 15:19:30', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3937', '10000000', 'text', '技术上 ，上证突破重要关键压力后，已经直逼3000整数关口', null, null, '2016-07-04 15:21:07', '2016-07-04 15:21:07', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3938', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a0eed5d05e.jpg', null, null, '2016-07-04 15:23:25', '2016-07-04 15:23:25', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3939', '10000000', 'text', '从盘面上看，站稳了2960支撑后，只要后市站稳3000，就有机会站上3100的中期目标。', null, null, '2016-07-04 15:24:18', '2016-07-04 15:24:18', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3940', '10000000', 'text', '但这次的反弹是鉴于市场对下半年中国可能降息的预期，', null, null, '2016-07-04 15:25:07', '2016-07-04 15:25:07', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3941', '10000000', 'text', '而经济长期下行的局面下，中国股市是难以有大级别的反弹行情出现的。所以前期被套的朋友，一定要抓住反弹机会解套离场', null, null, '2016-07-04 15:25:59', '2016-07-04 15:25:59', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3942', '10000000', 'text', '而今天股市最令人瞩目的也是停牌了大半年的万科也复牌了', null, null, '2016-07-04 15:27:29', '2016-07-04 15:27:29', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3943', '10000000', 'text', '受到股权之争的影响，万科今天7万手卖单封盘跌停也是意料之中。而市场传言，可能要跌三到四个跌停才会开盘', null, null, '2016-07-04 15:28:30', '2016-07-04 15:28:30', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('3944', '10000001', 'text', '好了，我们回到原油方面', null, null, '2016-07-04 15:37:09', '2016-07-04 15:37:09', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3945', '10000001', 'text', '好，我们回到原油方面', null, null, '2016-07-04 15:37:32', '2016-07-04 15:37:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3946', '10000001', 'text', '已被遗忘的产油猛国利比亚要重磅回归了', null, null, '2016-07-04 15:38:47', '2016-07-04 15:38:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3947', '10000001', 'text', '长期对抗，互相搞得对方没法好好卖油的利比亚两个政府的石油公司突然宣布达成协议', null, null, '2016-07-04 15:40:09', '2016-07-04 15:40:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3948', '10000001', 'text', '目前利比亚原油供应约在20-30万桶/日', null, null, '2016-07-04 15:48:09', '2016-07-04 15:48:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3949', '10000001', 'text', '利比亚石油工业领导人们此前对外宣称一旦局势稳定', null, null, '2016-07-04 15:51:20', '2016-07-04 15:51:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3950', '10000001', 'text', '利比亚的原油出口将能够很快恢复到70万桶/日', null, null, '2016-07-04 15:51:31', '2016-07-04 15:51:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3951', '10000001', 'text', '所以，现在摆在尼日利亚叛军面前的只有两条路：', null, null, '2016-07-04 15:54:54', '2016-07-04 15:54:54', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3952', '10000001', 'text', '要么继续炸掉40-50万桶/日的原油供应以维持短期油价', null, null, '2016-07-04 15:55:06', '2016-07-04 15:55:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3953', '10000001', 'text', '要么掉头做空，然后宣布和尼日利亚政府和解，放出那百万桶中断的原油供应，大捞一把', null, null, '2016-07-04 15:55:29', '2016-07-04 15:55:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3954', '10000001', 'text', '还有目前中国战略储油快要到顶了', null, null, '2016-07-04 16:08:18', '2016-07-04 16:08:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3955', '10000001', 'text', '而且接下来亚洲的炼油厂即将进入维修季，所以将大大的打压油价', null, null, '2016-07-04 16:12:08', '2016-07-04 16:12:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3956', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a1cb32025d.png', null, null, '2016-07-04 16:22:11', '2016-07-04 16:22:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3957', '10000001', 'text', '这油价还是327-330区间震荡啊，看来是要到晚上才会回落了', null, null, '2016-07-04 16:25:43', '2016-07-04 16:25:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3958', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a1f2650645.png', null, null, '2016-07-04 16:32:38', '2016-07-04 16:32:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3959', '10000001', 'text', '看来英国脱欧后，投资者和分析师都不太看好欧元区的经济了', null, null, '2016-07-04 16:33:57', '2016-07-04 16:33:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3960', '10000001', 'text', '而且跌至2014年11月来最低', null, null, '2016-07-04 16:42:05', '2016-07-04 16:42:05', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3961', '10000001', 'text', '表明英国脱欧对全球经济增长预期产生重大影响', null, null, '2016-07-04 16:51:22', '2016-07-04 16:51:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3962', '10000001', 'text', '尤其是对欧元区的影响，欧元区经济增长近乎停滞', null, null, '2016-07-04 16:59:45', '2016-07-04 16:59:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3963', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a26038b40a.png', null, null, '2016-07-04 17:01:55', '2016-07-04 17:01:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3964', '10000001', 'text', '欧元区5月PPI月率数据利多欧元，从而利多油价', null, null, '2016-07-04 17:03:23', '2016-07-04 17:03:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3965', '10000001', 'text', '创下2015年2月来最高水平', null, null, '2016-07-04 17:04:09', '2016-07-04 17:04:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3966', '10000001', 'text', '好了，行情较为清淡，我们讲一下K线的基础课程', null, null, '2016-07-04 17:09:14', '2016-07-04 17:09:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3967', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a285086b0c.png', null, null, '2016-07-04 17:11:44', '2016-07-04 17:11:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3968', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a294477b2c.png', null, null, '2016-07-04 17:15:48', '2016-07-04 17:15:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3969', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a2b3d9886f.png', null, null, '2016-07-04 17:24:13', '2016-07-04 17:24:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3970', '10000001', 'text', '锤头线在日线以上的级别非常好用的，后市看涨的准确率非常高的', null, null, '2016-07-04 17:25:58', '2016-07-04 17:25:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3971', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a2c24cdc51.png', null, null, '2016-07-04 17:28:04', '2016-07-04 17:28:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3972', '10000194', 'text', '老师空单还可以拿吗#10', null, null, '2016-07-04 17:30:47', '2016-07-04 17:30:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3973', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a2d3d6dc1e.png', null, null, '2016-07-04 17:32:45', '2016-07-04 17:32:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3974', '10000001', 'text', '没冲破压力位330之前，还是可以拿着的，如果冲破330，可在331-332区间锁仓一半，耐心#9', '10000194', '老师空单还可以拿吗#10', '2016-07-04 17:34:32', '2016-07-04 17:34:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3975', '10000194', 'text', '嗯#9', null, null, '2016-07-04 17:36:51', '2016-07-04 17:36:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3976', '10000001', 'text', '一样道理，吊颈线在日线级别以上，看跌的概率是非常高的', null, null, '2016-07-04 17:37:09', '2016-07-04 17:37:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3977', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a2faedcb78.png', null, null, '2016-07-04 17:43:10', '2016-07-04 17:43:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3978', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a30835949b.png', null, null, '2016-07-04 17:46:43', '2016-07-04 17:46:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3979', '10000001', 'text', '日线级别以上，十字星出现，行情反转信号', null, null, '2016-07-04 17:47:55', '2016-07-04 17:47:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3980', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a30ed1d171.png', null, null, '2016-07-04 17:48:29', '2016-07-04 17:48:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3981', '8', 'text', '#4', null, null, '2016-07-04 17:59:33', '2016-07-04 17:59:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3982', '8', 'text', '下午好啊#9很高兴又和大家见面了！', null, null, '2016-07-04 18:00:13', '2016-07-04 18:00:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3983', '10000102', 'text', '老师好#6', null, null, '2016-07-04 18:00:28', '2016-07-04 18:00:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3984', '10000001', 'text', '伍老师#14', null, null, '2016-07-04 18:00:32', '2016-07-04 18:00:32', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('3985', '10000001', 'text', '伍老师#14', null, null, '2016-07-04 18:00:52', '2016-07-04 18:00:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3986', '8', 'text', 'Queenie好！', '10000102', '老师好#6', '2016-07-04 18:00:59', '2016-07-04 18:00:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3987', '8', 'text', '吴老师辛苦了#20', null, null, '2016-07-04 18:01:17', '2016-07-04 18:01:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3988', '10000001', 'text', '好了，今天的K线教程就先到这里，下面就交给伍老师了#9', null, null, '2016-07-04 18:01:44', '2016-07-04 18:01:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('3989', '10000102', 'text', '现在讲什么？', null, null, '2016-07-04 18:03:18', '2016-07-04 18:03:18', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('3990', '8', 'text', '接下来咱们先一起来看看盘面吧。', '10000102', '现在讲什么？', '2016-07-04 18:05:56', '2016-07-04 18:05:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3991', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a359004608.png', null, null, '2016-07-04 18:08:16', '2016-07-04 18:08:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3992', '8', 'text', '价格再次走到330附近，短期多空方向可能已经真正到了最后的争持阶段，从4小时图看，附图MACD零轴上方二次金叉，KD指标亦是交叉向上扩散，加上布林带向上开口，综合来看，预计后市大概率将走多头方向，操作策略建议以低位做多为主，高空防守为辅的思路。', null, null, '2016-07-04 18:20:49', '2016-07-04 18:20:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3993', '8', 'text', '今天到目前为止仍一直处在高位震荡走势，波动幅度比较小。', null, null, '2016-07-04 18:37:40', '2016-07-04 18:37:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3994', '8', 'text', '这种行情，考验的是耐心呀#4', null, null, '2016-07-04 18:38:43', '2016-07-04 18:38:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3995', '8', 'text', '不过相对来说这也确是一个布局的好机会', null, null, '2016-07-04 18:40:06', '2016-07-04 18:40:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3996', '8', 'text', '先耐心等待机会，有信号及时入市布局。', null, null, '2016-07-04 18:47:12', '2016-07-04 18:47:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3997', '8', 'text', '大家平时除了看原油，也可以适当关注白银和铜等一些贵金属的行情呀', null, null, '2016-07-04 18:58:53', '2016-07-04 18:58:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3998', '8', 'text', '这短时间白银都涨的飞起#20', null, null, '2016-07-04 19:00:07', '2016-07-04 19:00:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('3999', '8', 'text', '目前这波动真是向死海一样平静', null, null, '2016-07-04 19:17:46', '2016-07-04 19:17:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4000', '8', 'text', '一个月的时间，涨幅达25%。', null, null, '2016-07-04 19:25:04', '2016-07-04 19:25:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4001', '8', 'text', '有机构预测，黄金将要涨到12000美元，白银将要涨到360美元。#4', null, null, '2016-07-04 19:27:26', '2016-07-04 19:27:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4002', '10000170', 'text', '#20#17', null, null, '2016-07-04 19:34:29', '2016-07-04 19:34:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4003', '10000170', 'text', '#17没有美盘不好玩哦', null, null, '2016-07-04 19:37:11', '2016-07-04 19:37:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4004', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a4ad35de15.png', null, null, '2016-07-04 19:38:59', '2016-07-04 19:38:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4005', '8', 'text', '美元指数出现短线下挫，油价得到支撑。', null, null, '2016-07-04 19:40:11', '2016-07-04 19:40:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4006', '10000092', 'text', '老师，今晚做空还是做多', null, null, '2016-07-04 19:49:15', '2016-07-04 19:49:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4007', '8', 'text', '油价反弹，短线看多。做单带好止盈止损。个人建议，仅供参考。#9', null, null, '2016-07-04 19:49:51', '2016-07-04 19:49:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4008', '8', 'text', '今天美国休市，波动确实不大。#4', '10000170', '#17没有美盘不好玩哦', '2016-07-04 19:51:03', '2016-07-04 19:51:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4009', '8', 'text', '刚刚给出了操作策略，你可以参考喔#9', '10000092', '老师，今晚做空还是做多', '2016-07-04 19:51:55', '2016-07-04 19:51:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4010', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a4e257ae7a.png', null, null, '2016-07-04 19:53:09', '2016-07-04 19:53:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4011', '8', 'text', '油价反弹，短线看多。做单带好止盈止损。个人建议，仅供参考。#9', null, null, '2016-07-04 19:53:23', '2016-07-04 19:53:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4012', '8', 'text', '美盘不开，今晚持续窄幅震荡的走势可能性比较大。', null, null, '2016-07-04 20:03:07', '2016-07-04 20:03:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4013', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a52934c5aa.png', null, null, '2016-07-04 20:12:03', '2016-07-04 20:12:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4014', '8', 'text', '明天的数据方面重点关注英国央行发布半年度金融稳定报告以及次日凌晨FOMC票委、纽约联储主席杜德利就政策和经济发表讲话。', null, null, '2016-07-04 20:13:39', '2016-07-04 20:13:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4015', '8', 'text', '英国退欧后，带来市场的冲击还在持续，市场一直持续关注英国央行的动态', null, null, '2016-07-04 20:24:22', '2016-07-04 20:24:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4016', '8', 'text', '英镑创下新低，央行是否出台货币政策和是否干预市场', null, null, '2016-07-04 20:24:57', '2016-07-04 20:24:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4017', '8', 'text', '这些问题将会在明天17:30英国央行发布半年度金融稳定报告中得到提示，届时将会引起油价的波动。', null, null, '2016-07-04 20:27:10', '2016-07-04 20:27:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4018', '8', 'text', '受到英国退欧带来的风险情绪，美元走高，但是同时给美国带来的负面影响，使得美联储的加息预期大大下降', null, null, '2016-07-04 20:36:23', '2016-07-04 20:36:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4019', '8', 'text', '虽然就业市场依然强劲，但近期的美国数据显示经济有所放缓，并且各大机构下调美国2016年的GDP增速。', null, null, '2016-07-04 20:38:00', '2016-07-04 20:38:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4020', '8', 'text', '这次非农或将关系着美联储2016年如何面对加息这个问题', null, null, '2016-07-04 20:53:39', '2016-07-04 20:53:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4021', '8', 'text', '对这次的非农投资者朋友们应该给与极度关注', null, null, '2016-07-04 20:56:37', '2016-07-04 20:56:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4022', '8', 'text', '周六(7月2日)据报道，经济学家对6月份新增非农就业的普遍预期为17.3万人。', null, null, '2016-07-04 20:57:36', '2016-07-04 20:57:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4023', '8', 'text', '投资者和美联储大腕预期6月新增就业岗位应控制在17.5万至20万个，这才能达到“刚刚好”的状态，从而令美联储按兵不动。 ', null, null, '2016-07-04 21:02:17', '2016-07-04 21:02:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4024', '8', 'text', '从上述数据可以得出，6月非农市场普遍预期在18万左右，但5月在前，也不得不防诡异数据发生。#15', null, null, '2016-07-04 21:03:52', '2016-07-04 21:03:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4025', '8', 'text', '若6月就业数据意外强劲，预示会提振通胀，但也不足令人预计美联储近期会加息，而若报告再次疲软，则将被解读为美国经济根基不稳的信号，美元或将贬值。', null, null, '2016-07-04 21:06:07', '2016-07-04 21:06:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4026', '8', 'text', '若美元受挫，原油将进一步得到支撑，或为上涨提供动力。', null, null, '2016-07-04 21:13:26', '2016-07-04 21:13:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4027', '8', 'text', '此次参与就业调查企业较少，且3万未被计入的复工人员可能令非农数据产生变数。', null, null, '2016-07-04 21:15:37', '2016-07-04 21:15:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4028', '8', 'text', '2016年初低迷的经济得以复苏，但经济增长不会太强劲', null, null, '2016-07-04 21:22:07', '2016-07-04 21:22:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4029', '8', 'text', '因为自英国退欧以来，外部环境已不断向美联储施压', null, null, '2016-07-04 21:23:06', '2016-07-04 21:23:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4030', '8', 'text', '这意味着美联储下一次的加息将变得遥遥无期。', null, null, '2016-07-04 21:25:26', '2016-07-04 21:25:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4031', '8', 'text', '甚至，如果局面过于糟糕，美联储就需要重拾宽松利器，在政策上180度大转弯，重新进入降息周期。 ', null, null, '2016-07-04 21:26:50', '2016-07-04 21:26:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4032', '8', 'text', '美联储高层官员欣然承认英国“脱欧”投票显然不能被忽略', null, null, '2016-07-04 21:29:40', '2016-07-04 21:29:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4033', '10000170', 'text', '可以空单么', null, null, '2016-07-04 21:41:38', '2016-07-04 21:41:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4034', '8', 'text', '可以，上方有压力，带好止盈止损。#9', '10000170', '可以空单么', '2016-07-04 21:50:26', '2016-07-04 21:50:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4035', '8', 'text', '短线有所回调，激进的朋友可以尝试在高位做个短空，稳健还是等进一步信号确认。', null, null, '2016-07-04 21:58:12', '2016-07-04 21:58:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4036', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a6bc632c61.png', null, null, '2016-07-04 21:59:34', '2016-07-04 21:59:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4037', '8', 'text', '美元指数，波动也不大，仍处于震荡走势。', null, null, '2016-07-04 22:00:20', '2016-07-04 22:00:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4038', '8', 'text', '上图是白银的走势，上周的极限暴涨行情终于有所放缓。', null, null, '2016-07-04 22:07:13', '2016-07-04 22:07:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4039', '8', 'text', '白银从6月初开始新的一轮上涨，到目前，涨幅达将近25%。', null, null, '2016-07-04 22:09:15', '2016-07-04 22:09:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4040', '8', 'text', '、', null, null, '2016-07-04 22:09:56', '2016-07-04 22:09:56', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4041', '8', 'text', '能算出来这利润空间有多大吗#4', null, null, '2016-07-04 22:10:52', '2016-07-04 22:10:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4042', '8', 'text', '如果按照我们33倍的杠杆来算的话，收益率能达825%。', null, null, '2016-07-04 22:12:37', '2016-07-04 22:12:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4043', '8', 'text', '真是有够恐怖的。#20', null, null, '2016-07-04 22:13:08', '2016-07-04 22:13:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4044', '8', 'text', '告诉大家一个好消息的是，咱们平台不久后也将有白银等贵金属产品上线', null, null, '2016-07-04 22:15:41', '2016-07-04 22:15:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4045', '8', 'text', '到时候，可供大家的产品选择又将增多，这意味着投资的机会也将大增啊#14', null, null, '2016-07-04 22:17:35', '2016-07-04 22:17:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4046', '10000170', 'text', '老师，空单下方大概多少可以平仓？', null, null, '2016-07-04 22:21:35', '2016-07-04 22:21:35', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4047', '8', 'text', '大家空单仓位重的建议有利润可以先出一部分仓位#9', null, null, '2016-07-04 22:31:25', '2016-07-04 22:31:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4048', '8', 'text', '美国市场因独立日假日休市的状况，遏制了油市的交投活跃水平。', null, null, '2016-07-04 22:32:42', '2016-07-04 22:32:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4049', '8', 'text', '今晚的波动幅度估计不会很大。', null, null, '2016-07-04 22:33:08', '2016-07-04 22:33:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4050', '10000170', 'text', '今晚的油价应该就在这样了，上不去也下不来', null, null, '2016-07-04 22:33:28', '2016-07-04 22:33:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4051', '8', 'text', '是啊，美国那边没开市，成交量上不去。', '10000170', '今晚的油价应该就在这样了，上不去也下不来', '2016-07-04 22:35:18', '2016-07-04 22:35:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4052', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160704/577a7493222d4.png', null, null, '2016-07-04 22:37:07', '2016-07-04 22:37:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4053', '8', 'text', '继续走三角震荡走势的几率比较大。', null, null, '2016-07-04 22:39:50', '2016-07-04 22:39:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4054', '8', 'text', '2016欧洲杯1/4决赛已经全部结束了', null, null, '2016-07-04 22:46:43', '2016-07-04 22:46:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4055', '8', 'text', '葡萄牙、威尔士、德国和法国杀入半决赛。', null, null, '2016-07-04 22:48:01', '2016-07-04 22:48:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4056', '8', 'text', '但在第33分钟，刚刚加盟拜仁慕尼黑的新星雷纳托-桑切斯为葡萄牙扳平比分。', null, null, '2016-07-04 22:57:07', '2016-07-04 22:57:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4057', '8', 'text', '30分钟级别，价格受布林带下轨支撑。', null, null, '2016-07-04 23:38:43', '2016-07-04 23:38:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4058', '10000194', 'text', '老师下方压力#9多少', null, null, '2016-07-04 23:42:45', '2016-07-04 23:42:45', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4059', '8', 'text', '做单不带止盈止损是对自己的资金不负责任的行为，不管单子的把握有多大。', null, null, '2016-07-05 00:02:13', '2016-07-05 00:02:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4060', '8', 'text', '严格的资金管理是成功交易的先决条件。', null, null, '2016-07-05 00:03:43', '2016-07-05 00:03:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4061', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577a8bf5d7306.png', null, null, '2016-07-05 00:16:53', '2016-07-05 00:16:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4062', '8', 'text', '这种窄幅震荡走势，心态要保持平和，不急不躁。#9', null, null, '2016-07-05 00:19:24', '2016-07-05 00:19:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4063', '8', 'text', '盈利的关键的2点问题', null, null, '2016-07-05 00:26:15', '2016-07-05 00:26:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4064', '8', 'text', '一种是靠大赚小赔获利', null, null, '2016-07-05 00:27:07', '2016-07-05 00:27:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4065', '8', 'text', '前面有说过哦，下方支撑在326.5附近。', '10000194', '老师下方压力#9多少', '2016-07-05 00:28:30', '2016-07-05 00:28:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4066', '8', 'text', '二种是靠成功的概率获利', null, null, '2016-07-05 00:30:41', '2016-07-05 00:30:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4067', '8', 'text', '回到我们的话题，关于交易盈利关键问题的第2点，要一直追求成功率的情况下，就会不断的去修正你的方法，然后不断出现新的错误，再不断的去修正...就这样一直循环下午。但不管怎样修正，就是无法达到你心目中的成功率，进而陷入这样的一个怪圈。', null, null, '2016-07-05 00:43:29', '2016-07-05 00:43:29', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4068', '8', 'text', '所以，我们也没必要去追求，我们无法做到的事情——100%的成功率。达到一定比例就可以了，没必要一直不断的追求下去，到是可以把盈利的关键集中在大赚小赔上。', null, null, '2016-07-05 00:46:47', '2016-07-05 00:46:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4069', '8', 'text', '哈哈，以上只是个人观点。#17', null, null, '2016-07-05 00:48:00', '2016-07-05 00:48:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4070', '8', 'text', '个人始终认为成功率并不是盈利的关键,一段周期的操作,有50%以上的成功率已属中上。', null, null, '2016-07-05 00:49:44', '2016-07-05 00:49:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4071', '8', 'text', '盈利的关键是操作技术上的,资金管理上的,心态控制上的，这应该是我们所要追求的目标所在。', null, null, '2016-07-05 00:51:23', '2016-07-05 00:51:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4072', '8', 'text', ' 一个交易人应该有这样的理念--在所有最危险的境况下，谁也看不到我的身影，只要我出现的时候，都是市场相对最安全的时候。#17', null, null, '2016-07-05 01:04:38', '2016-07-05 01:04:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4073', '8', 'text', '什么是相对危险相对安全那？这个很好检验。', null, null, '2016-07-05 01:05:34', '2016-07-05 01:05:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4074', '8', 'text', '在一个市场行情中，你可以不断预测这个行情的反向波，比如在明显下跌趋势中，预测所有的反弹，或在明显的上涨趋势中，预测所有的回调。\n', null, null, '2016-07-05 01:06:31', '2016-07-05 01:06:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4075', '8', 'text', '有一种说法是：能深通一种交易方法，就能在市场中获胜，我很赞同这说法，确实如此。', null, null, '2016-07-05 01:13:35', '2016-07-05 01:13:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4076', '8', 'text', '但并不是说，知道多的人肯定就要输。呵呵！有充足精力朋友，我还是建议你们多多学习。这个跟专一并不矛盾。#17', null, null, '2016-07-05 01:14:23', '2016-07-05 01:14:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4077', '8', 'text', '我说的就不代表真理，只代表我个人目前的认知。', null, null, '2016-07-05 01:15:52', '2016-07-05 01:15:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4078', '8', 'text', '上方330的压力依然还是很强，白天触碰了一下之后又开始回落。', null, null, '2016-07-05 01:26:50', '2016-07-05 01:26:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4079', '8', 'text', '但是目前来看依然还是多头趋势，可能受330压制短期或有回调需要', null, null, '2016-07-05 01:29:05', '2016-07-05 01:29:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4080', '8', 'text', '后市需要关注的关键支撑位在320-321，关键压力位还是330.操作上，若价格向其中一个方向突破，则可顺势而为即可。', null, null, '2016-07-05 01:31:54', '2016-07-05 01:31:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4081', '8', 'text', '好了。今天的直播到这里就结束了。', null, null, '2016-07-05 01:32:47', '2016-07-05 01:32:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4082', '8', 'text', '晚上讲的一些交易理念，大家也可以适当参考，交易不仅是一门技术，更是一门艺术。#9', null, null, '2016-07-05 01:34:30', '2016-07-05 01:34:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4083', '8', 'text', '大家晚安！我们明天再见！#9', null, null, '2016-07-05 01:34:59', '2016-07-05 01:34:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4084', '10000006', 'text', '老师早#9', null, null, '2016-07-05 09:12:54', '2016-07-05 09:12:54', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4085', '10000001', 'text', '大家早上好！', null, null, '2016-07-05 09:23:54', '2016-07-05 09:23:54', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4086', '10000001', 'text', '早#9', '10000006', '老师早#9', '2016-07-05 09:24:33', '2016-07-05 09:24:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4087', '10000001', 'text', '今天天气阳光明媚啊，就是有点热，哈哈', null, null, '2016-07-05 09:25:18', '2016-07-05 09:25:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4088', '10000001', 'text', '一个杰出的企业可以预计到，将来可能会发生什么，但不一定知道何时会发生。重心需要放在什么上面，而不是何时上。如果对什么的判断是正确的，那么对何时大可不必过虑', null, null, '2016-07-05 09:28:01', '2016-07-05 09:28:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4089', '10000001', 'text', '我是轩湛投资观察室的吴勇坚，接下来我来为大家做直播，大家有什么疑问或者疑惑，可以在互动区跟我交流喔', null, null, '2016-07-05 09:28:17', '2016-07-05 09:28:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4090', '10000001', 'text', '据消息：利比亚产量有所上升，使得今日油价小幅跳水开盘', null, null, '2016-07-05 09:29:53', '2016-07-05 09:29:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4091', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b0d9478ec9.png', null, null, '2016-07-05 09:29:56', '2016-07-05 09:29:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4092', '10000001', 'text', '下方支撑：320', null, null, '2016-07-05 09:30:15', '2016-07-05 09:30:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4093', '10000001', 'text', '如果持有昨晚多单的朋友，超过3层仓，可以适当的减仓，关注下方的支撑位320附近', null, null, '2016-07-05 09:39:56', '2016-07-05 09:39:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4094', '10000001', 'text', '抱歉各位，刚才临时停电，导致直播暂停了一下，现在直播继续#9', null, null, '2016-07-05 09:57:36', '2016-07-05 09:57:36', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('4095', '10000001', 'text', '抱歉各位，刚才临时停电，导致直播暂停了一下，现在直播继续#9', null, null, '2016-07-05 09:57:53', '2016-07-05 09:57:53', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4096', '10000001', 'text', '抱歉各位，刚才临时有事走开了一下，现在直播继续#9', null, null, '2016-07-05 10:05:17', '2016-07-05 10:05:17', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4097', '10000001', 'text', '抱歉各位，刚才临时有事走开了一下，现在直播继续#9', null, null, '2016-07-05 10:05:30', '2016-07-05 10:05:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4098', '10000001', 'text', '好了，接下来回顾以下昨天的行情', null, null, '2016-07-05 10:05:57', '2016-07-05 10:05:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4099', '10000001', 'text', '晚上，美国因为独立日休市一天，使得昨晚油价波动幅度较小', null, null, '2016-07-05 10:07:04', '2016-07-05 10:07:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4100', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b1661a8c8e.png', null, null, '2016-07-05 10:07:29', '2016-07-05 10:07:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4101', '10000001', 'text', '但受到利比亚产量的上升的影响，昨晚油价上探都压力位后震荡小幅回落至326附近', null, null, '2016-07-05 10:08:02', '2016-07-05 10:08:02', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4102', '10000001', 'text', '好了，行情回顾完，我们接下来看一下今天油价走势', null, null, '2016-07-05 10:12:12', '2016-07-05 10:12:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4103', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b17f14429e.png', null, null, '2016-07-05 10:14:09', '2016-07-05 10:14:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4104', '10000001', 'text', '目前油价在短期支撑位322附近上方盘整', null, null, '2016-07-05 10:15:02', '2016-07-05 10:15:02', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4105', '10000001', 'text', '目前油价在短期支撑位322上方附近盘整', null, null, '2016-07-05 10:15:33', '2016-07-05 10:15:33', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('4106', '10000001', 'text', '目前油价在短期支撑位322上方附近盘整', null, null, '2016-07-05 10:15:57', '2016-07-05 10:15:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4107', '10000001', 'text', '好，我们关注一下数据方面', null, null, '2016-07-05 10:16:46', '2016-07-05 10:16:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4108', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b18995a4c3.png', null, null, '2016-07-05 10:16:57', '2016-07-05 10:16:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4109', '10000001', 'text', '中国6月财新服务业PMI录得高于预期', null, null, '2016-07-05 10:18:11', '2016-07-05 10:18:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4110', '10000001', 'text', '数据表明：6月服务业和制造业PMI的走势明显相反，显现出经济结构转型的迹象', null, null, '2016-07-05 10:19:18', '2016-07-05 10:19:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4111', '10000001', 'text', '服务业的转好也支撑了整体的产出水平', null, null, '2016-07-05 10:19:53', '2016-07-05 10:19:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4112', '10000001', 'text', '对股市有所利好', null, null, '2016-07-05 10:22:39', '2016-07-05 10:22:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4113', '10000001', 'text', '那么，现在我们就去关注一下大盘，现在的情况', null, null, '2016-07-05 10:23:30', '2016-07-05 10:23:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4114', '10000001', 'text', '今日大盘，高开高走啊', null, null, '2016-07-05 10:24:22', '2016-07-05 10:24:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4115', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b1a943f982.png', null, null, '2016-07-05 10:25:24', '2016-07-05 10:25:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4116', '10000001', 'text', '目前大盘已经上破到3000点啦', null, null, '2016-07-05 10:25:57', '2016-07-05 10:25:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4117', '10000001', 'text', '今年5月6日以来首次！！！！', null, null, '2016-07-05 10:26:32', '2016-07-05 10:26:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4118', '10000001', 'text', '盘面上，军工、次新股、大飞机概念股等板块涨幅居前', null, null, '2016-07-05 10:33:15', '2016-07-05 10:33:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4119', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b1c6ecbff7.png', null, null, '2016-07-05 10:33:18', '2016-07-05 10:33:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4120', '10000001', 'text', '大盘上方压力位3050点', null, null, '2016-07-05 10:34:40', '2016-07-05 10:34:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4121', '10000001', 'text', '所以，如果持有股票的朋友们，趁着现在大盘反弹，要适当减仓哦', null, null, '2016-07-05 10:37:28', '2016-07-05 10:37:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4122', '10000001', 'text', '今日大飞机概念股崛起！！ 中直股份(48.150, 3.01, 6.67%)涨8.84%。', null, null, '2016-07-05 10:43:09', '2016-07-05 10:43:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4123', '10000001', 'text', '权重股大涨推动沪指突破压力区，为市场起到了稳定军心的作用！', null, null, '2016-07-05 10:50:27', '2016-07-05 10:50:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4124', '10000001', 'text', '如果今日大盘能站在3000点上方，后市大盘上探3050点有望啊', null, null, '2016-07-05 11:06:21', '2016-07-05 11:06:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4125', '10000001', 'text', '今日OLED概念表现较为低迷，万润股份(49.580, -1.27, -2.50%)跌2.34%', null, null, '2016-07-05 11:13:56', '2016-07-05 11:13:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4126', '10000001', 'text', '昨日全国国有企业改革座谈会在京召开，习近平强调做强做优做大国有企业，尽快在国企改革重要领域和关键环节取得新成效', null, null, '2016-07-05 11:18:13', '2016-07-05 11:18:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4127', '10000001', 'text', '李克强指出国有企业应当提高改革的效率', null, null, '2016-07-05 11:18:31', '2016-07-05 11:18:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4128', '10000001', 'text', '习李讲话再次印证前期判断：国企改革正在加速，看涨期权的催化剂逐渐显现，把握资产注入与壳价值两条主线', null, null, '2016-07-05 11:22:50', '2016-07-05 11:22:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4129', '10000001', 'text', '所以这个星期我们可以适当关注国企改革概念股', null, null, '2016-07-05 11:26:40', '2016-07-05 11:26:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4130', '10000102', 'text', '今天原油会上涨吗？', null, null, '2016-07-05 11:28:10', '2016-07-05 11:28:10', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4131', '10000001', 'text', '目前油价较为弱势，反弹幅度可能较小', '10000102', '今天原油会上涨吗？', '2016-07-05 11:31:34', '2016-07-05 11:31:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4132', '10000001', 'text', '大盘早上高开高走后，目前有所回落', null, null, '2016-07-05 11:37:25', '2016-07-05 11:37:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4133', '10000001', 'text', '截至午间收盘，沪指报3000.66点，涨0.40%', null, null, '2016-07-05 11:39:02', '2016-07-05 11:39:02', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4134', '10000001', 'text', '大盘午市展望：预测今日大盘将在3000点附近震荡为主，收一条小阳线', null, null, '2016-07-05 11:45:03', '2016-07-05 11:45:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4135', '10000001', 'text', '好了，我们回到油价方面', null, null, '2016-07-05 11:52:08', '2016-07-05 11:52:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4136', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b2f2490684.png', null, null, '2016-07-05 11:53:08', '2016-07-05 11:53:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4137', '10000001', 'text', '目前油价是有所反弹盘整的', null, null, '2016-07-05 11:54:49', '2016-07-05 11:54:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4138', '10000001', 'text', '据彭博社报道：由于电力传输的改进和更新换代，八年来印度的电力供应有望首次达到过剩', null, null, '2016-07-05 11:55:53', '2016-07-05 11:55:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4139', '10000001', 'text', '这意味着印度对石油的依赖将逐步减小', null, null, '2016-07-05 11:56:59', '2016-07-05 11:56:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4140', '10000001', 'text', '而且印度曾明确表明拒绝担当“油价救世主”', null, null, '2016-07-05 12:00:32', '2016-07-05 12:00:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4141', '10000001', 'text', '并且计划大举开发国内油气资源，目标是在六年内将石油进口依赖度降低10%', null, null, '2016-07-05 12:00:49', '2016-07-05 12:00:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4142', '10000001', 'text', '长此以往，印度“全球第三大石油消费国”的地位也可能不保了', null, null, '2016-07-05 12:01:31', '2016-07-05 12:01:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4143', '10000001', 'text', '那么，将会重重打压未来的油价', null, null, '2016-07-05 12:02:26', '2016-07-05 12:02:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4144', '10000001', 'text', '好了，我们先休息一会#9', null, null, '2016-07-05 12:03:49', '2016-07-05 12:03:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4145', '10000001', 'text', '大家下午好#9', null, null, '2016-07-05 13:00:29', '2016-07-05 13:00:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4146', '10000001', 'text', '下午关注事件：', null, null, '2016-07-05 13:05:21', '2016-07-05 13:05:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4147', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b4027c7e87.png', null, null, '2016-07-05 13:05:43', '2016-07-05 13:05:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4148', '10000001', 'text', '17:30 英国央行发布半年度金融稳定报告', null, null, '2016-07-05 13:09:42', '2016-07-05 13:09:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4149', '10000001', 'text', '同时英国央行行长卡尼也将发表讲话，这是卡尼12天来第三次公开讲话', null, null, '2016-07-05 13:17:33', '2016-07-05 13:17:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4150', '10000001', 'text', '英国投票决定脱离欧盟后，英镑汇率大跌，银行股也一度大幅走低', null, null, '2016-07-05 13:22:57', '2016-07-05 13:22:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4151', '10000001', 'text', '但是，目前来看，衡量金融市场紧张程度的指标显示风险受控', null, null, '2016-07-05 13:27:28', '2016-07-05 13:27:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4152', '10000001', 'text', '本次报告应该会让市场视为正面讯息', null, null, '2016-07-05 13:32:09', '2016-07-05 13:32:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4153', '10000001', 'text', '届时一定程度上将会引起油价的波动', null, null, '2016-07-05 13:33:12', '2016-07-05 13:33:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4154', '10000252', 'text', 'LAOSHI  ', null, null, '2016-07-05 13:36:30', '2016-07-05 13:36:30', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4155', '10000001', 'text', '目前油价走势较为清淡，大家可以适当关注欧盘开盘后油价的走势', null, null, '2016-07-05 13:38:48', '2016-07-05 13:38:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4156', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b47f5782a7.png', null, null, '2016-07-05 13:39:01', '2016-07-05 13:39:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4157', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b4a2de9a4d.png', null, null, '2016-07-05 13:48:30', '2016-07-05 13:48:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4158', '10000001', 'text', '短期支撑，轻仓短线多单，获利就跑（个人建议，仅供参考）', null, null, '2016-07-05 13:49:08', '2016-07-05 13:49:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4159', '10000252', 'text', '老师', null, null, '2016-07-05 13:50:38', '2016-07-05 13:50:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4160', '10000252', 'text', '昨晚的多单继续持有？', null, null, '2016-07-05 13:51:37', '2016-07-05 13:51:37', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4161', '10000001', 'text', '如果多单仓位低于3层仓，可以持有观察今日能否能跌破320支撑位，如果超过3层仓先适当减仓', '10000252', '昨晚的多单继续持有？', '2016-07-05 13:54:45', '2016-07-05 13:54:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4162', '10000252', 'text', '哦ok', null, null, '2016-07-05 13:56:11', '2016-07-05 13:56:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4163', '10000252', 'text', '谢谢', null, null, '2016-07-05 13:56:56', '2016-07-05 13:56:56', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4164', '10000001', 'text', '好，我们关注一下股市午盘情况', null, null, '2016-07-05 14:06:27', '2016-07-05 14:06:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4165', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b4fc2f3d4f.png', null, null, '2016-07-05 14:12:19', '2016-07-05 14:12:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4166', '10000001', 'text', '指数回踩3000点关口后，主力资金依旧维持净流出', null, null, '2016-07-05 14:14:49', '2016-07-05 14:14:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4167', '10000001', 'text', '所以持有股票的朋友，要适当减仓，跟上市场节奏\n', null, null, '2016-07-05 14:21:48', '2016-07-05 14:21:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4168', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b53c9179d6.png', null, null, '2016-07-05 14:29:29', '2016-07-05 14:29:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4169', '10000001', 'text', '计算机板块资金流出较大', null, null, '2016-07-05 14:30:26', '2016-07-05 14:30:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4170', '10000001', 'text', '好了，我们回到原油方面', null, null, '2016-07-05 14:31:17', '2016-07-05 14:31:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4171', '10000001', 'text', '油价开盘下落后，目前油价开始小幅反弹了', null, null, '2016-07-05 14:32:09', '2016-07-05 14:32:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4172', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b573f6f42b.png', null, null, '2016-07-05 14:44:15', '2016-07-05 14:44:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4173', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b58a73b4f8.png', null, null, '2016-07-05 14:50:15', '2016-07-05 14:50:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4174', '10000001', 'text', '可以适当关注一下17:00欧元区5月零售销售月率', null, null, '2016-07-05 14:51:50', '2016-07-05 14:51:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4175', '10000001', 'text', '毕竟英国退欧后，欧洲经济GDP增速是有所下降的', null, null, '2016-07-05 15:00:23', '2016-07-05 15:00:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4176', '10000001', 'text', '好了，我们去关注一下股市收盘情况', null, null, '2016-07-05 15:13:00', '2016-07-05 15:13:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4177', '10000001', 'text', '截至收盘，上证指数以3006.39点报收，涨幅0.60%，成交2704亿元', null, null, '2016-07-05 15:15:28', '2016-07-05 15:15:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4178', '10000001', 'text', '今日次新股板块再次崛起涨近5%。其次，航天军工、北斗导航和核电等板块走势较好', null, null, '2016-07-05 15:15:55', '2016-07-05 15:15:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4179', '10000001', 'text', '后市展望：', null, null, '2016-07-05 15:19:00', '2016-07-05 15:19:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4180', '10000001', 'text', '今日大强势震荡维持于3000点上方，相对昨日而言，市场成交量仍维持相对高位，但盘面上却分化较大', null, null, '2016-07-05 15:19:48', '2016-07-05 15:19:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4181', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b5fbb5fd7e.png', null, null, '2016-07-05 15:20:27', '2016-07-05 15:20:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4182', '10000001', 'text', '如果成交量持续放大，市场进一步上行将是大概率事件', null, null, '2016-07-05 15:20:33', '2016-07-05 15:20:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4183', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b60d7cebd5.jpg', null, null, '2016-07-05 15:25:11', '2016-07-05 15:25:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4184', '10000001', 'text', '好了，目前油价还是较为清淡', null, null, '2016-07-05 15:35:54', '2016-07-05 15:35:54', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4185', '10000001', 'text', '我们继续了解一下K线', null, null, '2016-07-05 15:36:29', '2016-07-05 15:36:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4186', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b63fd9c6ba.png', null, null, '2016-07-05 15:38:37', '2016-07-05 15:38:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4187', '10000001', 'text', '十字星在日线级别以上，是一个很重要的行情反转信号', null, null, '2016-07-05 15:39:31', '2016-07-05 15:39:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4188', '10000001', 'text', '如果下跌行情中出现十字星，证明下方支撑较强，短期反弹的概率是非常高的', null, null, '2016-07-05 15:40:36', '2016-07-05 15:40:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4189', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b65f60f528.png', null, null, '2016-07-05 15:47:02', '2016-07-05 15:47:02', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4190', '10000001', 'text', '如果上涨行情中出现十字星，证明上方压力较强，短期回落的概率是非常高的', null, null, '2016-07-05 15:52:58', '2016-07-05 15:52:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4191', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b6826b61bc.png', null, null, '2016-07-05 15:56:22', '2016-07-05 15:56:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4192', '10000001', 'text', '好了，回到油价走势方面', null, null, '2016-07-05 16:03:14', '2016-07-05 16:03:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4193', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b69f832db9.png', null, null, '2016-07-05 16:04:08', '2016-07-05 16:04:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4194', '10000001', 'text', '欧元区6月服务业PMI终值表现良好', null, null, '2016-07-05 16:05:19', '2016-07-05 16:05:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4195', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b6a7dbe22d.png', null, null, '2016-07-05 16:06:21', '2016-07-05 16:06:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4196', '10000001', 'text', '目前油价走到了支撑位附近', null, null, '2016-07-05 16:06:33', '2016-07-05 16:06:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4197', '10000001', 'text', '如果下破支撑位320，多单先锁仓一半', null, null, '2016-07-05 16:07:01', '2016-07-05 16:07:01', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4198', '10000001', 'text', '空单先获利适当减减仓', null, null, '2016-07-05 16:07:27', '2016-07-05 16:07:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4199', '10000001', 'text', '如果下破320支撑位，325上方的多单先锁仓一半', null, null, '2016-07-05 16:09:08', '2016-07-05 16:09:08', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('4200', '10000001', 'text', '如果下破320支撑位，325上方的多单先锁仓一半', null, null, '2016-07-05 16:09:19', '2016-07-05 16:09:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4201', '10000001', 'text', '17:30将英国央行发布半年度金融稳定报告同时英国央行行长将会发表讲话', null, null, '2016-07-05 16:13:12', '2016-07-05 16:13:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4202', '10000001', 'text', '市场风险情绪先导', null, null, '2016-07-05 16:13:28', '2016-07-05 16:13:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4203', '10000001', 'text', '使得英镑继续下跌创新低，引发欧元和油价有所下落', null, null, '2016-07-05 16:14:58', '2016-07-05 16:14:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4204', '10000102', 'text', '所以现在是预期利空，是吗？', null, null, '2016-07-05 16:21:53', '2016-07-05 16:21:53', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4205', '10000001', 'text', 'Markit首席经济学家：6月份欧元区经济增长缺乏动能，制造业增长较快但服务业增速放缓，导致整体商业活动扩张速度基本维持不变', null, null, '2016-07-05 16:24:10', '2016-07-05 16:24:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4206', '10000001', 'text', '不一定，央行的金融稳定报告和新闻发布会可能会再度强调，英国银行业状况良好，可以安然度过当前的风暴', '10000102', '所以现在是预期利空，是吗？', '2016-07-05 16:29:29', '2016-07-05 16:29:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4207', '10000001', 'text', '同时，美国发出一份报告表明美国已发现之原油蕴藏量首度超过沙特阿拉伯与俄罗斯', null, null, '2016-07-05 16:31:21', '2016-07-05 16:31:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4208', '10000001', 'text', '总量达2640亿桶，超越沙特的2120亿桶及俄罗斯的2560亿桶', null, null, '2016-07-05 16:31:41', '2016-07-05 16:31:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4209', '10000001', 'text', '加速油价短期下落', null, null, '2016-07-05 16:33:28', '2016-07-05 16:33:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4210', '10000102', 'text', '还能做空吗？', null, null, '2016-07-05 16:33:52', '2016-07-05 16:33:52', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4211', '10000001', 'text', '可以，轻仓短空，带好止损止盈，止损设323，止盈看到315附近（个人建议，仅供参考）', '10000102', '还能做空吗？', '2016-07-05 16:39:55', '2016-07-05 16:39:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4212', '10000001', 'text', 'Markit首席经济学家：第二季度的英国服务业活动整体增速创下2013年第一季度以来最低水平；因此预计未来几周英国很可能会出台更多的刺激措施', null, null, '2016-07-05 16:49:35', '2016-07-05 16:49:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4213', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b77bfdb2ae.png', null, null, '2016-07-05 17:02:55', '2016-07-05 17:02:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4214', '10000001', 'text', '数据达到预期', null, null, '2016-07-05 17:05:06', '2016-07-05 17:05:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4215', '10000001', 'text', '对油价影响不大', null, null, '2016-07-05 17:07:27', '2016-07-05 17:07:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4216', '10000103', 'text', '大家下午好#9#9#9', null, null, '2016-07-05 17:08:16', '2016-07-05 17:08:16', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4217', '10000103', 'text', '大家有没感觉，最近天气一直很差下着不断的雨室外又闷热#2#2室外又被空调冻得要披棉被得节奏#16', null, null, '2016-07-05 17:12:09', '2016-07-05 17:12:09', '-1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4218', '10000103', 'text', '大家有没感觉，最近天气一直很差下着不断的雨室外又闷热#2#2室内又被空调冻得要披棉被得节奏#16', null, null, '2016-07-05 17:13:50', '2016-07-05 17:13:50', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4219', '10000103', 'text', '刚上来又要去紧急开个会，离开一会噢~稍后吴老师会给大家解读行情', null, null, '2016-07-05 17:16:23', '2016-07-05 17:16:23', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4220', '10000103', 'text', '#9#9', null, null, '2016-07-05 17:16:34', '2016-07-05 17:16:34', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4221', '8', 'text', '大家下午好！#9', null, null, '2016-07-05 17:54:06', '2016-07-05 17:54:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4222', '8', 'text', '很高兴又和大家见面了#15', null, null, '2016-07-05 17:54:22', '2016-07-05 17:54:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4223', '8', 'text', '有时候，问题很复杂，答案却很简单。跟生活玩，别太认真了，反正最终没谁能活着离开这场游戏。人生就像舞台，不到谢幕，永远不会知道自己有多精彩。', null, null, '2016-07-05 17:59:52', '2016-07-05 17:59:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4224', '8', 'text', '因本周一美国独立日休市，原定于本周三公布的API、ADP、EIA数据将分别推迟至本周四04:30、20:15、23:00公布，而6月非农报告将于本周五20:30公布。', null, null, '2016-07-05 18:06:27', '2016-07-05 18:06:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4225', '8', 'text', '就是周三晚上的数据退出到周四晚上，具体时间没有改变', null, null, '2016-07-05 18:08:12', '2016-07-05 18:08:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4226', '8', 'text', '6月的非农报告还是在周五晚上8点半', null, null, '2016-07-05 18:08:56', '2016-07-05 18:08:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4227', '8', 'text', '白天油价继续下挫', null, null, '2016-07-05 18:12:06', '2016-07-05 18:12:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4228', '8', 'text', '因OPEC6月产油量增加', null, null, '2016-07-05 18:12:51', '2016-07-05 18:12:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4229', '8', 'text', '尼日利亚、沙特增幅居前', null, null, '2016-07-05 18:13:22', '2016-07-05 18:13:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4230', '8', 'text', '再次引发了市场对原油供应过剩的担忧，油价承压下滑。', null, null, '2016-07-05 18:15:47', '2016-07-05 18:15:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4231', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b8a2965b36.png', null, null, '2016-07-05 18:21:29', '2016-07-05 18:21:29', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4232', '8', 'text', '从黄金分割线来看，下方的支撑在318.3，再下方支撑在314.8', null, null, '2016-07-05 18:23:32', '2016-07-05 18:23:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4233', '8', 'text', '空单仓位比较重的朋友可以获利先出一部分', null, null, '2016-07-05 18:26:37', '2016-07-05 18:26:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4234', '8', 'text', '消息面上此前沙特能源部长法力赫称市场在走向平衡，这给了油价一定的支撑', null, null, '2016-07-05 18:32:09', '2016-07-05 18:32:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4235', '8', 'text', '但他的评论给市场带来的影响被亚洲需求放缓、汽油出现零星供应过剩状况及原油产量可能增加的迹象所盖过。', null, null, '2016-07-05 18:34:40', '2016-07-05 18:34:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4236', '8', 'text', '上述这些基本面的消息都是致使今天油价下跌的主要因素', null, null, '2016-07-05 18:42:49', '2016-07-05 18:42:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4237', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b8fcb81371.png', null, null, '2016-07-05 18:45:31', '2016-07-05 18:45:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4238', '8', 'text', '美元指数早间的持续反弹也给原油价格增添了压力', null, null, '2016-07-05 18:47:25', '2016-07-05 18:47:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4239', '10000252', 'text', '刚才能多吗？', null, null, '2016-07-05 18:48:58', '2016-07-05 18:48:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4240', '8', 'text', '可以，目前空头力量减弱，下方在318处受到支撑。', '10000252', '刚才能多吗？', '2016-07-05 18:58:15', '2016-07-05 18:58:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4241', '8', 'text', '周二英镑兑美元汇率跌穿英国脱欧公投所导致的31年低位，兑欧元汇率也刷新2013年以来新低', null, null, '2016-07-05 19:03:12', '2016-07-05 19:03:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4242', '8', 'text', '这表明英国脱欧对投资者信心的影响正在逐渐加大', null, null, '2016-07-05 19:04:02', '2016-07-05 19:04:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4243', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b95d6273b5.png', null, null, '2016-07-05 19:11:18', '2016-07-05 19:11:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4244', '8', 'text', '下跌无力，短线博反弹，带好止盈止损#9（个人建议，仅供参考）', null, null, '2016-07-05 19:11:59', '2016-07-05 19:11:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4245', '10000252', 'text', '谢谢', null, null, '2016-07-05 19:13:38', '2016-07-05 19:13:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4246', '8', 'text', '不客气。#15', '10000252', '谢谢', '2016-07-05 19:14:24', '2016-07-05 19:14:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4247', '8', 'text', '英国央行行长卡尼在发布会上也承认越来越多的迹象显示英国脱欧公投导致投资推迟，市场对英国经济骤然下滑的担忧正在加深。', null, null, '2016-07-05 19:16:23', '2016-07-05 19:16:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4248', '8', 'text', '据华尔街日报：尼日尔三角洲“复仇者”武装分子称已经发起另一次袭击，炸毁了尼日利亚石油设施。', null, null, '2016-07-05 19:18:25', '2016-07-05 19:18:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4249', '8', 'text', '看来这是尼日尔三角洲“复仇者”又卷土重来了！', null, null, '2016-07-05 19:19:44', '2016-07-05 19:19:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4250', '8', 'text', '这对原油是大利多的消息', null, null, '2016-07-05 19:20:56', '2016-07-05 19:20:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4251', '8', 'text', '此前，尼日利亚石油天然气高级雇员联合会表示，尼日利亚全国石油和天然气工人联盟和Pengassan的石油工人将于7月7日开始逐步展开罢工，抗议有关方面裁员和迟迟不通过新石油法案。', null, null, '2016-07-05 19:26:48', '2016-07-05 19:26:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4252', '8', 'text', '随着抛售重启，英镑/美元今天再次刷新31年低点。', null, null, '2016-07-05 19:31:29', '2016-07-05 19:31:29', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4253', '8', 'text', '英国央行公布半年度金融稳定报告，决定放松银行资本要求，将逆周期资本缓冲要求由0.5%降低至0，卡尼发表讲话称将在必要时采取进一步行动。', null, null, '2016-07-05 19:33:08', '2016-07-05 19:33:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4254', '8', 'text', '原油短线继续下挫', null, null, '2016-07-05 19:41:04', '2016-07-05 19:41:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4255', '8', 'text', '目前短线以318做多空分水岭，若继续下破，320以上的多单先锁仓，320以下的多单就止损先出。若318支撑住了，那么多单的第一目标在323.5', null, null, '2016-07-05 19:49:42', '2016-07-05 19:49:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4256', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577b9f5435243.png', null, null, '2016-07-05 19:51:48', '2016-07-05 19:51:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4257', '8', 'text', '下面一起来看看晚间有哪些数据需要关注的', null, null, '2016-07-05 20:02:04', '2016-07-05 20:02:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4258', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577ba226abe24.png', null, null, '2016-07-05 20:03:50', '2016-07-05 20:03:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4259', '8', 'text', '22点有美国的两个数据，可以适当给与关注', null, null, '2016-07-05 20:05:02', '2016-07-05 20:05:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4260', '8', 'text', 'IBD消费者信心指数是投资者商业日报调查得到的消费者信心指数值', null, null, '2016-07-05 20:07:52', '2016-07-05 20:07:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4261', '8', 'text', '消费者信心指数是反映消费者信心强弱的指标，是综合反映并量化消费者对当前经济形势评价和对经济前景、收入水平、收入预期以及消费心理状态的主观感受，是预测经济走势和消费趋向的一个先行指标。', null, null, '2016-07-05 20:14:00', '2016-07-05 20:14:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4262', '8', 'text', '理论上，实际值大于预期值，利好美元，利空原油。', null, null, '2016-07-05 20:14:52', '2016-07-05 20:14:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4263', '8', 'text', '321上方的多单，超过两层仓，可以先锁仓一半。321下方的多单，可以先止损出来。持有空单的看到318附近先获利平仓。', null, null, '2016-07-05 20:16:29', '2016-07-05 20:16:29', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4264', '8', 'text', '目前下跌势头进一步在扩大', null, null, '2016-07-05 20:17:15', '2016-07-05 20:17:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4265', '8', 'text', '工厂订单月率是代表未来一个月内，对不易耗损的物品订购数量，该数据反映了制造业活动情况，就定义而言，订单泛指有意购买、而预期马上交运或在未来交运的商品交易。', null, null, '2016-07-05 20:19:40', '2016-07-05 20:19:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4266', '8', 'text', '总体而言，若该数据增长，则表示制造业情况有所改善，利好该国货币。反之若降低，则表示制造业出现萎缩，对该国货币利空。市场一般最为重视美国耐用品订单指数。', null, null, '2016-07-05 20:22:15', '2016-07-05 20:22:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4267', '8', 'text', '理论上，若实际值大于预期值，利好美元，利空原油。', null, null, '2016-07-05 20:23:19', '2016-07-05 20:23:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4268', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577ba782c98df.png', null, null, '2016-07-05 20:26:42', '2016-07-05 20:26:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4269', '10000102', 'text', '现在还有下跌的空间吗？', null, null, '2016-07-05 20:28:25', '2016-07-05 20:28:25', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4270', '8', 'text', '另一个比较重要是事件是次日凌晨2点半FOMC票委、纽约联储主席杜德利就政策和经济发表讲话', null, null, '2016-07-05 20:28:31', '2016-07-05 20:28:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4271', '8', 'text', '刚刚触碰了318后出现极速反弹，说明这个位置有一定的支撑力，若跌破，更下方的支撑在314.8.', '10000102', '现在还有下跌的空间吗？', '2016-07-05 20:32:16', '2016-07-05 20:32:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4272', '10000252', 'text', '美盘开了  有影响吗?', null, null, '2016-07-05 20:32:58', '2016-07-05 20:32:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4273', '8', 'text', '美盘开了，交投会变得更活跃，会加速价格的走势。', '10000252', '美盘开了  有影响吗?', '2016-07-05 20:34:20', '2016-07-05 20:34:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4274', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577baadc6c46f.png', null, null, '2016-07-05 20:41:00', '2016-07-05 20:41:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4275', '8', 'text', '从盘面来看，中线多空的格局已有所改变，空头力量变强，所以，多单建议以短线为主，有利润就可以先出。', null, null, '2016-07-05 20:46:50', '2016-07-05 20:46:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4276', '8', 'text', '从盘面来看，中线多空的格局已有所改变，空头力量变强，所以，多单建议以短线为主，有利润就可以先出。', null, null, '2016-07-05 20:47:07', '2016-07-05 20:47:07', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4277', '8', 'text', '从盘面来看，中线多空的格局已有所改变，空头力量变强，所以，多单建议以短线为主，有利润就可以先出。', null, null, '2016-07-05 20:47:12', '2016-07-05 20:47:12', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4278', '10000252', 'text', '大方向还是看空么？', null, null, '2016-07-05 20:53:42', '2016-07-05 20:53:42', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4279', '8', 'text', '空头今天真是很强势，反弹好几次都被打压下来了', null, null, '2016-07-05 20:55:02', '2016-07-05 20:55:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4280', '8', 'text', '目前来看，318支撑位还算比较强力，已经阻挡了两拨下跌攻势，就看这第三波能不能挡住了', null, null, '2016-07-05 20:58:25', '2016-07-05 20:58:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4281', '8', 'text', '若第三波挡住了，短线反弹就有希望了。', null, null, '2016-07-05 20:59:25', '2016-07-05 20:59:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4282', '8', 'text', '一鼓作气，再而衰，三而竭', null, null, '2016-07-05 21:00:12', '2016-07-05 21:00:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4283', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577bb14664e0b.png', null, null, '2016-07-05 21:08:22', '2016-07-05 21:08:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4284', '8', 'text', '日内走势图上看，空头力量暂时有所放缓。', null, null, '2016-07-05 21:09:32', '2016-07-05 21:09:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4285', '8', 'text', '市场投机性抛售，空头比较强势，中期方向偏看空。', '10000252', '大方向还是看空么？', '2016-07-05 21:12:36', '2016-07-05 21:12:36', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('4286', '8', 'text', '市场投机性抛售，空头比较强势，中期方向偏看空。', '10000252', '大方向还是看空么？', '2016-07-05 21:13:18', '2016-07-05 21:13:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4287', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577bb34ba32c0.png', null, null, '2016-07-05 21:16:59', '2016-07-05 21:16:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4288', '8', 'text', '美元指数持续震荡上行，在定价层面又给原油增加了压力。', null, null, '2016-07-05 21:18:55', '2016-07-05 21:18:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4289', '8', 'text', '318支撑被突破了，大家按照之前给出的策略，321上方的多单，超过两层仓，可以先锁仓一半。321下方的多单，可以先止损出来。', null, null, '2016-07-05 21:25:01', '2016-07-05 21:25:01', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4290', '8', 'text', '318支撑被突破了，大家按照之前给出的策略，321上方的多单，超过两层仓，可以先锁仓一半。321下方的多单，可以先出来。', null, null, '2016-07-05 21:28:22', '2016-07-05 21:28:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4291', '10000170', 'text', '老师，这边我们老师看的是多头，今晚的油价为什么会下跌呢？', null, null, '2016-07-05 21:31:20', '2016-07-05 21:31:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4292', '10000170', 'text', '尼日利亚又出现暴动，加上工人罢工，应该是大利多啊，怎么油价下跌的这么厉害呢？', null, null, '2016-07-05 21:33:36', '2016-07-05 21:33:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4293', '8', 'text', '因OPEC产量上升加上全球经济的担忧可能拖累原油需求，以致于使得投机性抛售，使得原油出现下跌。', '10000170', '尼日利亚又出现暴动，加上工人罢工，应该是大利多啊，怎么油价下跌的这么厉害呢？', '2016-07-05 21:39:33', '2016-07-05 21:39:33', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4294', '8', 'text', '因OPEC产量上升加上全球经济的担忧可能拖累原油需求，以致于使得投机性抛售，使得原油出现下跌。', '10000170', '尼日利亚又出现暴动，加上工人罢工，应该是大利多啊，怎么油价下跌的这么厉害呢？', '2016-07-05 21:39:46', '2016-07-05 21:39:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4295', '10000013', 'text', '老师什么是OPEC？', null, null, '2016-07-05 21:39:53', '2016-07-05 21:39:53', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4296', '10000170', 'text', '这种下跌势头会持续多长时间？', null, null, '2016-07-05 21:41:58', '2016-07-05 21:41:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4297', '8', 'text', 'OPEC(Organization of Petroleum Exporting Countries) 就是石油输出国组织 ', '10000013', '老师什么是OPEC？', '2016-07-05 21:42:11', '2016-07-05 21:42:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4298', '10000013', 'text', '谢谢老师，老师这么迟还在帮我们盯盘，辛苦了#9', null, null, '2016-07-05 21:42:56', '2016-07-05 21:42:56', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4299', '10000102', 'text', '老师现在进空单可以吗？', null, null, '2016-07-05 21:43:32', '2016-07-05 21:43:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4300', '8', 'text', '短期来看，空头力量占上风，持续时间要市场情绪', '10000170', '这种下跌势头会持续多长时间？', '2016-07-05 21:46:06', '2016-07-05 21:46:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4301', '8', 'text', '#17客气了', '10000013', '谢谢老师，老师这么迟还在帮我们盯盘，辛苦了#9', '2016-07-05 21:46:43', '2016-07-05 21:46:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4302', '8', 'text', '可以的，目前处空头格局，空单是顺势而为，记得带好止盈止损。#9', '10000102', '老师现在进空单可以吗？', '2016-07-05 21:48:21', '2016-07-05 21:48:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4303', '8', 'text', '22:00将公布美国7月IBD消费者信心指数、美国5月工厂订单月率。', null, null, '2016-07-05 21:51:36', '2016-07-05 21:51:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4304', '8', 'text', '虽然可能对原油影响有限，但还是可以给与适当关注', null, null, '2016-07-05 21:52:36', '2016-07-05 21:52:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4305', '10000006', 'text', '#9', null, null, '2016-07-05 21:57:23', '2016-07-05 21:57:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4306', '8', 'text', '#9', '10000006', '#9', '2016-07-05 21:59:26', '2016-07-05 21:59:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4307', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577bbd8d161bf.png', null, null, '2016-07-05 22:00:45', '2016-07-05 22:00:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4308', '10000170', 'text', '老师，这个数据影响不大吧？', null, null, '2016-07-05 22:01:20', '2016-07-05 22:01:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4309', '8', 'text', '数据利多，但是这两个数据一般影响有限。', null, null, '2016-07-05 22:01:53', '2016-07-05 22:01:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4310', '8', 'text', '影响不会很大。', '10000170', '老师，这个数据影响不大吧？', '2016-07-05 22:02:20', '2016-07-05 22:02:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4311', '10000170', 'text', '好吧，继续关注吧，现在还持有多单呢', null, null, '2016-07-05 22:03:44', '2016-07-05 22:03:44', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4312', '8', 'text', '多单要注意控制仓位哦，参考一下之前给的处理方法。#9', '10000170', '好吧，继续关注吧，现在还持有多单呢', '2016-07-05 22:05:24', '2016-07-05 22:05:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4313', '10000170', 'text', '恩恩，谢谢老师提醒，目前多单是2层仓位', null, null, '2016-07-05 22:06:04', '2016-07-05 22:06:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4314', '8', 'text', '#20', '10000170', '恩恩，谢谢老师提醒，目前多单是2层仓位', '2016-07-05 22:06:53', '2016-07-05 22:06:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4315', '8', 'text', '凌晨02:30的时候有FOMC票委、纽约联储主席杜德利就政策和经济发表讲话，对原油会有一定影响。', null, null, '2016-07-05 22:16:11', '2016-07-05 22:16:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4316', '8', 'text', '目前，原油还在持续下挫', null, null, '2016-07-05 22:16:52', '2016-07-05 22:16:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4317', '8', 'text', '空头强势，321上方的多单，超过两层仓，可以先锁仓一半。321下方的多单，可以先出来。', null, null, '2016-07-05 22:21:54', '2016-07-05 22:21:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4318', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577bc2c04430a.png', null, null, '2016-07-05 22:22:56', '2016-07-05 22:22:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4319', '8', 'text', '用黄金分割线看，下方短线支撑是黄金分割线61.8%，也就是314附近。', null, null, '2016-07-05 22:27:23', '2016-07-05 22:27:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4320', '8', 'text', '据路透：交易商称GENSCAPE数据显示库欣原油库存增加。', null, null, '2016-07-05 22:29:36', '2016-07-05 22:29:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4321', '8', 'text', 'GENSCAPE预计本周库欣地区原油库存将增加23万桶。', null, null, '2016-07-05 22:33:32', '2016-07-05 22:33:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4322', '8', 'text', ' 随着油价逐渐复苏，“打不死”的美国页岩油产商加大了对油价下跌的对冲力度', null, null, '2016-07-05 22:42:05', '2016-07-05 22:42:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4323', '8', 'text', '美盘原油继续扩大跌幅', null, null, '2016-07-05 22:48:07', '2016-07-05 22:48:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4324', '8', 'text', '美元指数创日内新高', null, null, '2016-07-05 22:54:26', '2016-07-05 22:54:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4325', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577bca3420f4f.png', null, null, '2016-07-05 22:54:44', '2016-07-05 22:54:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4326', '8', 'text', '大家还记不记得上周讲的均线的用法？', null, null, '2016-07-05 22:56:48', '2016-07-05 22:56:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4327', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577bcb1d24f5b.jpg', null, null, '2016-07-05 22:58:37', '2016-07-05 22:58:37', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4328', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577bcb2c562bc.jpg', null, null, '2016-07-05 22:58:52', '2016-07-05 22:58:52', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4329', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577bcb40c1381.jpg', null, null, '2016-07-05 22:59:12', '2016-07-05 22:59:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4330', '8', 'text', '上图就是著名的葛南维八大买卖法则', null, null, '2016-07-05 23:01:14', '2016-07-05 23:01:14', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4331', '8', 'text', '其实就是均线的用法', null, null, '2016-07-05 23:02:46', '2016-07-05 23:02:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4332', '8', 'text', '大家也可以学习参考下它的这种用法，非常的有价值。#9', null, null, '2016-07-05 23:06:22', '2016-07-05 23:06:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4333', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160705/577bcf17083fe.png', null, null, '2016-07-05 23:15:35', '2016-07-05 23:15:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4334', '8', 'text', '314都破了，这是要奔着前低303去的节奏啊', null, null, '2016-07-05 23:16:42', '2016-07-05 23:16:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4335', '8', 'text', '哥伦比亚国家石油公司：哥伦比亚第二长输油管线—Cano Limon输油管线遇袭后停止运转。', null, null, '2016-07-05 23:29:09', '2016-07-05 23:29:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4336', '10000170', 'text', '老师，这个消息是利多吧？', null, null, '2016-07-05 23:30:02', '2016-07-05 23:30:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4337', '8', 'text', '这是一个利多的消息', null, null, '2016-07-05 23:30:06', '2016-07-05 23:30:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4338', '8', 'text', '对的。', '10000170', '老师，这个消息是利多吧？', '2016-07-05 23:30:15', '2016-07-05 23:30:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4339', '8', 'text', '最近市场关注的重心还是放在了对需求的担忧上，这条供应方面的消息也不知道对担忧情绪是否能够有所提振。', null, null, '2016-07-05 23:36:56', '2016-07-05 23:36:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4340', '8', 'text', '目前，原油空头在313附近得到支撑，出现小幅反弹。', null, null, '2016-07-05 23:49:59', '2016-07-05 23:49:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4341', '8', 'text', '到目前为止，原油今日的跌幅已经达到了3.68%', null, null, '2016-07-05 23:51:48', '2016-07-05 23:51:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4342', '8', 'text', '02:30 纽约联储主席杜德利就政策和经济发表讲话', null, null, '2016-07-06 00:06:46', '2016-07-06 00:06:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4343', '8', 'text', '精神好的朋友可以关注一下', null, null, '2016-07-06 00:07:54', '2016-07-06 00:07:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4344', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577bdd84754e1.png', null, null, '2016-07-06 00:17:08', '2016-07-06 00:17:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4345', '8', 'text', '下半年撼动市场的大事件还要看耶伦', null, null, '2016-07-06 00:18:25', '2016-07-06 00:18:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4346', '8', 'text', '美联储近日公布了2017年FOMC会议日程', null, null, '2016-07-06 00:19:10', '2016-07-06 00:19:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4347', '8', 'text', '受英国脱欧的事件综合影响，市场预计美联储在2017年2月之前加息概率低于10%，甚至不及降息概率。', null, null, '2016-07-06 00:25:52', '2016-07-06 00:25:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4348', '8', 'text', '最新美国联邦基金利率期货显示，交易员预计美联储2016年7月加息概率为0%，降息概率为4%；9月加息概率为0%，降息概率为13.6%；12月加息概率为8.6%，降息概率为12.2%，2017年6月加息概率为21.6%，降息概率为10.3%，12月加息概率为39.8%，降息概率为7.5%。', null, null, '2016-07-06 00:37:30', '2016-07-06 00:37:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4349', '8', 'text', '关于美联储是否有加息可能', null, null, '2016-07-06 00:43:46', '2016-07-06 00:43:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4350', '8', 'text', '目前来看，退欧风险并未完全释放，后续不确定性依然较高。', null, null, '2016-07-06 00:44:32', '2016-07-06 00:44:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4351', '8', 'text', '目前只是公投胜利，实质性脱欧尚未开始。', null, null, '2016-07-06 00:53:27', '2016-07-06 00:53:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4352', '8', 'text', '二次公投的可能性、政府是否官方承认公投结果、里斯本条约50条的具体触发时点、后续谈判节奏等都存在较大变数', null, null, '2016-07-06 00:54:24', '2016-07-06 00:54:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4353', '8', 'text', '因此风险还未释放完全。目前美联储还在实时监测当中。', null, null, '2016-07-06 01:02:46', '2016-07-06 01:02:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4354', '8', 'text', '大家有问题都可以在互动区提出来，我看到后都会及时予以回复', null, null, '2016-07-06 01:05:35', '2016-07-06 01:05:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4355', '10000170', 'text', '老师，请问下目前下跌的空间大吗？还可以锁仓吗？ ', null, null, '2016-07-06 01:07:47', '2016-07-06 01:07:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4356', '8', 'text', '你的多单还在持有？建议以后还是要果断一点呀，鉴于目前下跌的势头依然强劲，还是锁仓为宜。#9', '10000170', '老师，请问下目前下跌的空间大吗？还可以锁仓吗？ ', '2016-07-06 01:11:08', '2016-07-06 01:11:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4357', '10000170', 'text', '是啊，因为这边老师没有给出任何锁仓的建议，所以我也不知道怎么办', null, null, '2016-07-06 01:11:46', '2016-07-06 01:11:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4358', '8', 'text', '以后要多关注下直播室哦，很早的时候我就在直播室给出了处理的方法。', '10000170', '是啊，因为这边老师没有给出任何锁仓的建议，所以我也不知道怎么办', '2016-07-06 01:13:42', '2016-07-06 01:13:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4359', '10000170', 'text', '您下方看到多少呢、', null, null, '2016-07-06 01:16:21', '2016-07-06 01:16:21', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4360', '10000170', 'text', '这边老师没通知锁仓，您方便给个下方的点位，到时候我好平空单，感谢', null, null, '2016-07-06 01:18:51', '2016-07-06 01:18:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4361', '8', 'text', '[|@荷 您下方看到多少呢、|目前来看，短期的空头格局已经形成，前方低点在303附近，这是空头第一的目标。', '10000170', '您下方看到多少呢、', '2016-07-06 01:20:24', '2016-07-06 01:20:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4362', '10000170', 'text', '好的，老师，十分感谢呢#9', null, null, '2016-07-06 01:21:14', '2016-07-06 01:21:14', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4363', '8', 'text', '多单的话，建议先锁仓。', '10000170', '这边老师没通知锁仓，您方便给个下方的点位，到时候我好平空单，感谢', '2016-07-06 01:22:16', '2016-07-06 01:22:16', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4364', '8', 'text', '不客气#17', '10000170', '好的，老师，十分感谢呢#9', '2016-07-06 01:22:29', '2016-07-06 01:22:29', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4365', '8', 'text', '今天的跌幅到目前为止已经差不多达到4.48%，所以也要警惕小幅反弹。', null, null, '2016-07-06 01:28:42', '2016-07-06 01:28:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4366', '8', 'text', '所以想追空的朋友可以适当等反弹后再操作。', null, null, '2016-07-06 01:31:47', '2016-07-06 01:31:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4367', '8', 'text', '今天的直播到这里就结束了，感谢朋友们的观看，咱们明天再见！', null, null, '2016-07-06 01:34:04', '2016-07-06 01:34:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4368', '8', 'text', '大家晚安#9', null, null, '2016-07-06 01:34:17', '2016-07-06 01:34:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4369', '10000001', 'text', '大家早上好！#9', null, null, '2016-07-06 09:14:58', '2016-07-06 09:14:58', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('4370', '10000001', 'text', '大家早上好！#9', null, null, '2016-07-06 09:15:21', '2016-07-06 09:15:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4371', '10000001', 'text', '如果你已经准备好去找寻你的人生目标，那就去追随那个声音吧。回到那个你最能清晰听到它的地方。对某些人来说，这个声音会在清晨，在繁忙的一天开始之前。而对另外一些人来说，它会在听音乐、看电影或读书时显现。也可能在他们漫步于林间或山岗时显现', null, null, '2016-07-06 09:19:02', '2016-07-06 09:19:02', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4372', '10000001', 'text', '我是轩湛投资观察室的吴勇坚，接下来我来为大家做直播，大家有什么疑问或者疑惑，可以在互动区跟我交流喔。', null, null, '2016-07-06 09:19:26', '2016-07-06 09:19:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4373', '10000103', 'text', '#9#9#9早上好吴老师', null, null, '2016-07-06 09:20:34', '2016-07-06 09:20:34', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('4374', '10000103', 'text', '早上好大家#6#6', null, null, '2016-07-06 09:21:06', '2016-07-06 09:21:06', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4375', '10000252', 'text', '老师早上好', null, null, '2016-07-06 09:22:28', '2016-07-06 09:22:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4376', '10000001', 'text', '早上好！小榆老师#9', null, null, '2016-07-06 09:22:52', '2016-07-06 09:22:52', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('4377', '10000103', 'text', '#9#9#9早上好吴老师', null, null, '2016-07-06 09:22:57', '2016-07-06 09:22:57', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4378', '10000001', 'text', '早上好！小榆老师#9', null, null, '2016-07-06 09:23:19', '2016-07-06 09:23:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4379', '10000001', 'text', '看到小榆老师，整个人都开心了，哈哈哈#9', null, null, '2016-07-06 09:24:51', '2016-07-06 09:24:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4380', '10000252', 'text', '昨天的多单       ', null, null, '2016-07-06 09:25:51', '2016-07-06 09:25:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4381', '10000252', 'text', '\n', null, null, '2016-07-06 09:25:59', '2016-07-06 09:25:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4382', '10000103', 'text', '#17#17#17', null, null, '2016-07-06 09:26:24', '2016-07-06 09:26:24', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('4383', '10000001', 'text', '好了，接下来我们回顾一下昨天的行情', null, null, '2016-07-06 09:27:14', '2016-07-06 09:27:14', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4384', '10000252', 'text', '多单还在手上', null, null, '2016-07-06 09:28:21', '2016-07-06 09:28:21', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4385', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577c5f24a3602.jpeg', null, null, '2016-07-06 09:30:12', '2016-07-06 09:30:12', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4386', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577c5f312663d.jpeg', null, null, '2016-07-06 09:30:25', '2016-07-06 09:30:25', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4387', '10000103', 'text', '昨晚伍老师在直播室已经给大家提示了风险，建议锁仓，希望大家参考！', null, null, '2016-07-06 09:31:30', '2016-07-06 09:31:30', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4388', '10000001', 'text', '昨晚我们伍老师已经给出跌破支撑位进行锁仓的建议#9', '10000252', '多单还在手上', '2016-07-06 09:32:27', '2016-07-06 09:32:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4389', '10000001', 'text', '好了，接下来我们回顾一下昨天的行情', null, null, '2016-07-06 09:32:56', '2016-07-06 09:32:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4390', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577c6052735ef.png', null, null, '2016-07-06 09:35:14', '2016-07-06 09:35:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4391', '10000001', 'text', '昨晚，受到下午英国央行行长的讲话风险和英国半年度金融报告没有明确给出英国退欧后的应对政策，只是表示一旦《里斯本条约》第50条被启动，英国央行有能力应对', null, null, '2016-07-06 09:37:59', '2016-07-06 09:37:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4392', '10000001', 'text', '使得投资者对英国的担忧加大，引发风险情绪，使得英镑继续创下新低', null, null, '2016-07-06 09:38:16', '2016-07-06 09:38:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4393', '10000001', 'text', '引发欧元和油价的下落', null, null, '2016-07-06 09:38:27', '2016-07-06 09:38:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4394', '10000001', 'text', '同时，据机构报告称截止7月1日当周库欣地区原油库存增加', null, null, '2016-07-06 09:40:29', '2016-07-06 09:40:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4395', '10000001', 'text', '此外调查显示尼日利亚已恢复部分产量，加之利比亚局势缓解，导致市场对欧佩克增产的预期升温', null, null, '2016-07-06 09:41:09', '2016-07-06 09:41:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4396', '10000001', 'text', '使得油价加速下跌', null, null, '2016-07-06 09:42:19', '2016-07-06 09:42:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4397', '10000001', 'text', '昨日油价跌幅逾4%', null, null, '2016-07-06 09:43:35', '2016-07-06 09:43:35', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4398', '10000001', 'text', '油价跌幅逾4%', null, null, '2016-07-06 09:44:01', '2016-07-06 09:44:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4399', '10000252', 'text', '今天继续跌吗？', null, null, '2016-07-06 09:45:37', '2016-07-06 09:45:37', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4400', '10000001', 'text', '同时，美元的大幅走强，更是使得油价继续承压', null, null, '2016-07-06 09:46:17', '2016-07-06 09:46:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4401', '10000001', 'text', '目前油价较为弱势，观察能否跌破支撑位310，如果跌破，油价将会下探到303附近', '10000252', '今天继续跌吗？', '2016-07-06 09:48:30', '2016-07-06 09:48:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4402', '10000001', 'text', '好了，我们回到今日油价的走势', null, null, '2016-07-06 09:49:53', '2016-07-06 09:49:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4403', '10000252', 'text', '谢谢', null, null, '2016-07-06 09:50:30', '2016-07-06 09:50:30', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4404', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577c64a9a2e39.png', null, null, '2016-07-06 09:53:45', '2016-07-06 09:53:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4405', '10000001', 'text', '日线级别的支撑位在300', null, null, '2016-07-06 09:54:38', '2016-07-06 09:54:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4406', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577c670a277f9.png', null, null, '2016-07-06 10:03:54', '2016-07-06 10:03:54', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4407', '10000001', 'text', '从图上看，我们近两周油价的走势较为相识的', null, null, '2016-07-06 10:05:15', '2016-07-06 10:05:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4408', '10000001', 'text', '引发油价下跌的先导因素都是英国退欧后市场的风险情绪', null, null, '2016-07-06 10:07:07', '2016-07-06 10:07:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4409', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577c692651914.png', null, null, '2016-07-06 10:12:54', '2016-07-06 10:12:54', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4410', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577c69b637183.png', null, null, '2016-07-06 10:15:18', '2016-07-06 10:15:18', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('4411', '10000001', 'text', '310支撑位还没跌破，反弹的可能性还是比较大的', null, null, '2016-07-06 10:17:12', '2016-07-06 10:17:12', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('4412', '10000001', 'text', '310支撑位目前未能跌破，反弹的可能性还是比较大的', null, null, '2016-07-06 10:17:58', '2016-07-06 10:17:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4413', '10000001', 'text', '同时，虽然利比亚东西两方政府分别控制的国有石油公司突然宣布合并，被认为是利比亚即将增产的前奏', null, null, '2016-07-06 10:18:43', '2016-07-06 10:18:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4414', '10000001', 'text', '但是，在这个派系林立、冲突不断的国家，谁也不会真的放弃对石油的控制，增产仍是一件虚无缥缈的事', null, null, '2016-07-06 10:18:55', '2016-07-06 10:18:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4415', '10000001', 'text', '好了，我们去关注一下股市的情况', null, null, '2016-07-06 10:23:40', '2016-07-06 10:23:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4416', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577c6c4ad9697.png', null, null, '2016-07-06 10:26:18', '2016-07-06 10:26:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4417', '10000001', 'text', '今日大盘是低开低走的', null, null, '2016-07-06 10:26:56', '2016-07-06 10:26:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4418', '10000001', 'text', '不过昨日大盘反弹到3000附近，主力资金出逃，今日大盘开盘有所回落也是正常的', null, null, '2016-07-06 10:29:02', '2016-07-06 10:29:02', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4419', '10000001', 'text', '万科周一复盘后，500万手的卖单，使得万科连续两天跌停', null, null, '2016-07-06 10:33:50', '2016-07-06 10:33:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4420', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577c6e573603e.png', null, null, '2016-07-06 10:35:03', '2016-07-06 10:35:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4421', '10000001', 'text', '但昨日临近收盘，大笔资金在抄底万科A，其成交金额急剧放大', null, null, '2016-07-06 10:36:11', '2016-07-06 10:36:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4422', '10000001', 'text', '宝能系大笔增持，机构敢死队联手扫货', null, null, '2016-07-06 10:36:32', '2016-07-06 10:36:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4423', '10000001', 'text', '使得今日万科开盘后低开高走', null, null, '2016-07-06 10:38:54', '2016-07-06 10:38:54', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4424', '10000001', 'text', '盘面上，黄金概念、白酒板块领涨，次新股、钢铁板块跌幅居前', null, null, '2016-07-06 10:41:13', '2016-07-06 10:41:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4425', '10000102', 'text', '309支撑强吗？', null, null, '2016-07-06 10:49:15', '2016-07-06 10:49:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4426', '10000001', 'text', '受到昨日英国央行行长风险讲话，引发是市场的风险情绪，英镑继续创下新低，使得美元和黄金等避险产品继续走强', null, null, '2016-07-06 10:49:17', '2016-07-06 10:49:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4427', '10000001', 'text', '310-309区间是一个较强的支撑位，反弹的可能性是蛮高的，加上有消息宣称利比亚增产这是个虚火，更是为油价提供支撑', '10000102', '309支撑强吗？', '2016-07-06 10:51:13', '2016-07-06 10:51:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4428', '10000102', 'text', '好的，老师', null, null, '2016-07-06 10:53:11', '2016-07-06 10:53:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4429', '10000001', 'text', '所以我们今周可以持续关注黄金板块的走势', null, null, '2016-07-06 10:53:57', '2016-07-06 10:53:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4430', '10000001', 'text', '昨日全球最大黄金ETF基金更是大增28.81吨黄金，当前持仓量为982.72吨', null, null, '2016-07-06 10:55:51', '2016-07-06 10:55:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4431', '10000001', 'text', '目前大盘继续上探到3000点压力位的', null, null, '2016-07-06 11:03:59', '2016-07-06 11:03:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4432', '10000001', 'text', '今日证券板块还没有什么异动，预计今日大盘不会有太大的涨幅', null, null, '2016-07-06 11:06:27', '2016-07-06 11:06:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4433', '10000001', 'text', '而且会有所下落盘整', null, null, '2016-07-06 11:18:52', '2016-07-06 11:18:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4434', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577c7a00933df.png', null, null, '2016-07-06 11:24:48', '2016-07-06 11:24:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4435', '10000001', 'text', '好了，预测今日盘也是震荡盘整，行情会较为清淡', null, null, '2016-07-06 11:27:11', '2016-07-06 11:27:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4436', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577c7b4a5bf40.png', null, null, '2016-07-06 11:30:18', '2016-07-06 11:30:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4437', '10000001', 'text', '而且今日主力资金继续流出', null, null, '2016-07-06 11:31:41', '2016-07-06 11:31:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4438', '10000006', 'text', '老师，油现在怎么看呢#2', null, null, '2016-07-06 11:46:08', '2016-07-06 11:46:08', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4439', '10000001', 'text', '好了，我们去看看，早盘收盘情况', null, null, '2016-07-06 11:48:45', '2016-07-06 11:48:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4440', '10000001', 'text', '目前油价下方支撑308较强，下破可能性较低；如果下破支撑，将会出现单边行情，我们可以反手做空，把多单的亏损赚回来#9', '10000006', '老师，油现在怎么看呢#2', '2016-07-06 11:55:07', '2016-07-06 11:55:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4441', '10000001', 'text', '截至午间收盘，报3001.07点，跌5.32点，跌幅0.18%，成交1310亿元', null, null, '2016-07-06 11:55:46', '2016-07-06 11:55:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4442', '10000001', 'text', '盘面上，黄金、白酒、杭州亚运会、饮料制造、有色冶炼加工等概念股涨幅居前', null, null, '2016-07-06 11:56:00', '2016-07-06 11:56:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4443', '10000001', 'text', '集成电路、采掘服务、水利、钢铁、机场航运、券商等概念股跌幅居前', null, null, '2016-07-06 11:56:33', '2016-07-06 11:56:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4444', '10000102', 'text', '如果反手做空，下方的空间也不大了吧', null, null, '2016-07-06 11:57:22', '2016-07-06 11:57:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4445', '10000001', 'text', '下方支撑位300,80多个点喔#9', '10000102', '如果反手做空，下方的空间也不大了吧', '2016-07-06 11:59:50', '2016-07-06 11:59:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4446', '10000001', 'text', '同时一旦下破这个区间，那么中期看空的趋势就形成了#9', '10000102', '如果反手做空，下方的空间也不大了吧', '2016-07-06 12:01:11', '2016-07-06 12:01:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4447', '10000001', 'text', '好了，我们先休息一下#9', null, null, '2016-07-06 12:02:42', '2016-07-06 12:02:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4448', '10000170', 'text', '#15002256彩虹精化后市能不能创新高。老师', null, null, '2016-07-06 12:45:56', '2016-07-06 12:45:56', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4449', '10000001', 'text', '大家下午好！#9', null, null, '2016-07-06 13:02:20', '2016-07-06 13:02:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4450', '10000001', 'text', '虽然早上该股主力资金在流入，但午盘看到主力开始流出，想要创新高还是有点乏力的，同时今日大盘主力资金是继续流出，所以持有该股，可以先减仓观望', '10000170', '#15002256彩虹精化后市能不能创新高。老师', '2016-07-06 13:10:07', '2016-07-06 13:10:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4451', '10000001', 'text', '目前油价的而走势开始小幅反弹', null, null, '2016-07-06 13:14:45', '2016-07-06 13:14:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4452', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577c93c93a235.png', null, null, '2016-07-06 13:14:49', '2016-07-06 13:14:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4453', '10000185', 'text', '老师，现在能做多吗，压力位是多少？', null, null, '2016-07-06 13:18:28', '2016-07-06 13:18:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4454', '10000001', 'text', '可以轻仓短多，获利就跑，带好止损止盈，上方短期压力位317（个人建议仅供参考）', '10000185', '老师，现在能做多吗，压力位是多少？', '2016-07-06 13:20:50', '2016-07-06 13:20:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4455', '10000185', 'text', '谢谢老师#9', null, null, '2016-07-06 13:21:32', '2016-07-06 13:21:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4456', '10000001', 'text', '今日关注数据方面：', null, null, '2016-07-06 13:30:33', '2016-07-06 13:30:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4457', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577c97ff42063.png', null, null, '2016-07-06 13:32:47', '2016-07-06 13:32:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4458', '10000001', 'text', '次日凌晨04:30 美国至7月1日当周API原油库存', null, null, '2016-07-06 13:34:23', '2016-07-06 13:34:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4459', '10000001', 'text', '这个数据的影响力跟EIA库存相仿，届时会引起油价较大的波动', null, null, '2016-07-06 13:45:48', '2016-07-06 13:45:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4460', '10000001', 'text', '截至6月17日当周，美国API原油库存大减522.4万桶，录得半年来最大降幅', null, null, '2016-07-06 13:55:27', '2016-07-06 13:55:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4461', '10000001', 'text', '上周美国API原油库存再次超预期大幅削减，无疑表明市场原油需求较为强劲', null, null, '2016-07-06 13:59:18', '2016-07-06 13:59:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4462', '10000001', 'text', '美国原油库存的削减无疑将会为国际原油价格提供短线强劲支撑', null, null, '2016-07-06 14:04:37', '2016-07-06 14:04:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4463', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577ca1bf227b1.png', null, null, '2016-07-06 14:14:23', '2016-07-06 14:14:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4464', '10000001', 'text', '6月29日凌晨04:30，美国至6月24日当周API原油库存(万桶)录得-386小幅低于预期，预期值-237.5。利多油价，算上杠杠带来的收益空间达到157%', null, null, '2016-07-06 14:16:40', '2016-07-06 14:16:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4465', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577ca4e844f58.png', null, null, '2016-07-06 14:27:52', '2016-07-06 14:27:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4466', '10000001', 'text', '6月22日凌晨04:30，美国至6月17日当周API原油库存(万桶)录得-522.4大幅低于预期，预期值-190。利多油价，算上杠杠收益空间达到71.5%', null, null, '2016-07-06 14:29:07', '2016-07-06 14:29:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4467', '10000001', 'text', ' 6月15日凌晨04:30，美国至6月10日当周API原油库存(万桶)录得115.8，大幅高于预期，预期值-226.8。利空油价', null, null, '2016-07-06 14:41:44', '2016-07-06 14:41:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4468', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577ca8afec33c.png', null, null, '2016-07-06 14:44:00', '2016-07-06 14:44:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4469', '10000001', 'text', ' 6月8日凌晨04:30，美国至6月3日当周API原油库存(万桶)录得-356.9，小幅低于预期，预期值-350。利多油价', null, null, '2016-07-06 14:54:06', '2016-07-06 14:54:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4470', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577cab11cdc81.png', null, null, '2016-07-06 14:54:09', '2016-07-06 14:54:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4471', '10000001', 'text', '统计分析，API公布后带来的收益空间平均达到96.75％', null, null, '2016-07-06 14:58:25', '2016-07-06 14:58:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4472', '10000001', 'text', '而且API公布后不仅影响当晚的行情，还会影响公布后当天的行情', null, null, '2016-07-06 14:59:18', '2016-07-06 14:59:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4473', '10000001', 'text', '同时据统计API公布的数据与当天晚上EIA数据相仿', null, null, '2016-07-06 15:03:25', '2016-07-06 15:03:25', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4474', '10000001', 'text', '相似度达到73.3%', null, null, '2016-07-06 15:03:41', '2016-07-06 15:03:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4475', '10000001', 'text', '所以当日API库存录得是利空油价，当天晚上的EIA库存利空油价的可能性非常高', null, null, '2016-07-06 15:04:00', '2016-07-06 15:04:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4476', '10000252', 'text', '老师好', null, null, '2016-07-06 15:05:47', '2016-07-06 15:05:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4477', '10000001', 'text', '你好#9', '10000252', '老师好', '2016-07-06 15:06:28', '2016-07-06 15:06:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4478', '10000252', 'text', '多单继续持有吗?', null, null, '2016-07-06 15:08:17', '2016-07-06 15:08:17', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4479', '10000252', 'text', '#9', null, null, '2016-07-06 15:08:46', '2016-07-06 15:08:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4480', '10000001', 'text', '是什么点位的多单？多少层仓位？', '10000252', '多单继续持有吗?', '2016-07-06 15:10:10', '2016-07-06 15:10:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4481', '10000252', 'text', '314', null, null, '2016-07-06 15:11:02', '2016-07-06 15:11:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4482', '10000252', 'text', '两成', null, null, '2016-07-06 15:11:37', '2016-07-06 15:11:37', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4483', '10000001', 'text', '可以先持有观察，目前还处于反弹趋势，上方短期压力位317', '10000252', '314', '2016-07-06 15:12:26', '2016-07-06 15:12:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4484', '10000252', 'text', 'ok#9', null, null, '2016-07-06 15:13:52', '2016-07-06 15:13:52', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4485', '10000001', 'text', '好了，股市收盘了，我们去看看股市的情况', null, null, '2016-07-06 15:15:50', '2016-07-06 15:15:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4486', '10000102', 'text', '现在多单可以吗？', null, null, '2016-07-06 15:16:06', '2016-07-06 15:16:06', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4487', '10000001', 'text', '不建议现在去追反弹多单（个人建议，仅供参考）', '10000102', '现在多单可以吗？', '2016-07-06 15:18:56', '2016-07-06 15:18:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4488', '10000001', 'text', '截至收盘，上证指数以3017.29点报收，涨幅0.36%，成交2465亿元', null, null, '2016-07-06 15:19:12', '2016-07-06 15:19:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4489', '10000001', 'text', '盘面上，白酒、黄金和有色金属等板块走势较好', null, null, '2016-07-06 15:20:23', '2016-07-06 15:20:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4490', '10000001', 'text', '遵义市白酒企业成功签订了1.6亿元的产销订单', null, null, '2016-07-06 15:20:57', '2016-07-06 15:20:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4491', '10000001', 'text', '在该消息影响下，今日白酒板块相对走强', null, null, '2016-07-06 15:21:09', '2016-07-06 15:21:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4492', '10000001', 'text', '后市展望：', null, null, '2016-07-06 15:22:57', '2016-07-06 15:22:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4493', '10000001', 'text', '目前大盘处于3000点关口附近，总体呈现窄幅震荡的态势，多空博弈有所加剧，部分热点板块有短线获利了结迹象', null, null, '2016-07-06 15:23:39', '2016-07-06 15:23:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4494', '10000001', 'text', '所以建议投资者宜做好仓位管理，适当减仓', null, null, '2016-07-06 15:25:08', '2016-07-06 15:25:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4495', '10000001', 'text', '好了回到原油方面，目前油价继续有所反弹', null, null, '2016-07-06 15:32:57', '2016-07-06 15:32:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4496', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577cb52f59a2f.png', null, null, '2016-07-06 15:37:19', '2016-07-06 15:37:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4497', '10000001', 'text', '今晚关注事件：', null, null, '2016-07-06 15:42:16', '2016-07-06 15:42:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4498', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577cb6e26c7d3.png', null, null, '2016-07-06 15:44:34', '2016-07-06 15:44:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4499', '10000001', 'text', '次日凌晨2:00 美联储FOMC公布6月货币政策会议纪要', null, null, '2016-07-06 15:45:01', '2016-07-06 15:45:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4500', '10000001', 'text', '此次会议需关注两大要点：', null, null, '2016-07-06 15:53:10', '2016-07-06 15:53:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4501', '10000001', 'text', '美联储官员们如何评估近期美国经济数据的下滑，特别是就业市场', null, null, '2016-07-06 15:53:31', '2016-07-06 15:53:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4502', '10000001', 'text', '美联储如何应对英国脱欧引发的金融市场动荡', null, null, '2016-07-06 16:01:51', '2016-07-06 16:01:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4503', '10000006', 'text', '老师今晚有数据吗', null, null, '2016-07-06 16:04:22', '2016-07-06 16:04:22', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4504', '10000001', 'text', '晚上没有重大数据，不过凌晨04:30API原油库存和凌晨2:00 FOMC公布6月货币政策会议纪要', '10000006', '老师今晚有数据吗', '2016-07-06 16:11:00', '2016-07-06 16:11:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4505', '10000001', 'text', '英国央行行长卡尼上周暗示，今夏可能会降息，并采取其他宽松措施，所以美联储可能会推迟下一次加息时间。但市场预计，美国不会因英国脱欧而降息或推出更多的宽松政策', null, null, '2016-07-06 16:16:19', '2016-07-06 16:16:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4506', '10000001', 'text', '所以今晚FOMC公布6月货币政策会议纪要将维持鸽派', null, null, '2016-07-06 16:24:02', '2016-07-06 16:24:02', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4507', '10000001', 'text', '打压美元，为油价提供支撑', null, null, '2016-07-06 16:24:34', '2016-07-06 16:24:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4508', '10000001', 'text', '欧洲央行管委：英国脱欧将对欧元区经济构成影响，但相较于对英国本身的影响较为温和', null, null, '2016-07-06 16:34:49', '2016-07-06 16:34:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4509', '10000001', 'text', '同时法国央行维持2016年法国经济增长预期在1.4%', null, null, '2016-07-06 16:35:06', '2016-07-06 16:35:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4510', '10000001', 'text', '表明目前英国退欧对欧元区经济还没有造成较大的影响', null, null, '2016-07-06 16:35:51', '2016-07-06 16:35:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4511', '10000001', 'text', '无疑为油价提供支撑', null, null, '2016-07-06 16:41:32', '2016-07-06 16:41:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4512', '10000001', 'text', '印度原油从1月至4月，该国原油需求约为494万桶/日创纪录的高位，较去年同期的451万桶/日增长了9.6%', null, null, '2016-07-06 16:54:32', '2016-07-06 16:54:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4513', '10000103', 'text', 'Hi~下午好亲们！#9#9', null, null, '2016-07-06 16:57:14', '2016-07-06 16:57:14', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4514', '10000001', 'text', '印度的原油需求无疑为未来油价提供支撑', null, null, '2016-07-06 16:58:49', '2016-07-06 16:58:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4515', '10000103', 'text', '下午郑老师再次超神完胜，短短两个小时，成功斩获24点，收益率达19.2%，近2个涨停板！\n', null, null, '2016-07-06 16:59:13', '2016-07-06 16:59:13', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4516', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577cc87ba88ad.jpg', null, null, '2016-07-06 16:59:39', '2016-07-06 16:59:39', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4517', '10000103', 'text', '这就是轩湛•高德直播室的实力，这就是我们老师的实力#14', null, null, '2016-07-06 17:00:51', '2016-07-06 17:00:51', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4518', '10000103', 'text', '再观望您错过的就不仅仅是2个涨停板了！！！\n', null, null, '2016-07-06 17:04:02', '2016-07-06 17:04:02', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4519', '10000103', 'text', '为优化投资者的资产配置，降低投资者的投资体验成本，感受轩湛投资带来的全新服务——轩湛高德直播室，当下打开了一次史无前例的活动“最红EIA”！', null, null, '2016-07-06 17:07:34', '2016-07-06 17:07:34', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4520', '10000103', 'text', '这次活动作为回馈新老客户的一个大福利！在保证不亏损的情况下，跟随直播室的老师进行布局EIA捡钱行情！！', null, null, '2016-07-06 17:11:13', '2016-07-06 17:11:13', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4521', '10000103', 'text', '大家是不是很激动#3#4#4', null, null, '2016-07-06 17:12:27', '2016-07-06 17:12:27', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4522', '10000103', 'text', '下面来介绍下活动形式吧', null, null, '2016-07-06 17:13:31', '2016-07-06 17:13:31', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4523', '10000103', 'text', '本次活动分为两个超级特权：一是“赴EIA行情会 跟名师炒原油”，二是“零风险体验（盈利你拿走，亏损我买单）”', null, null, '2016-07-06 17:17:00', '2016-07-06 17:17:00', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4524', '10000103', 'text', '在7月13日即下周三晚上9点至11点，将在我们直播室由郑杰老师携手轩湛几大分析师坐镇，召开EIA行情解读盛会，提供行情解读，点拨趋势，把握布局时机，分享机会，解答客户疑问。', null, null, '2016-07-06 17:19:52', '2016-07-06 17:19:52', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4525', '10000103', 'text', '这是一个只要跟上老师节奏就能坐收渔翁之利的绝佳机会，相信跟上郑神节奏的家人红的不仅仅是EIA还有家人们的账户收益噢！!！', null, null, '2016-07-06 17:22:12', '2016-07-06 17:22:12', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4526', '10000103', 'text', '在7月5日14:00至7月13日04:00期间，会相继进行第二个主题:首笔交易亏损补偿，0风险体验原油投资。', null, null, '2016-07-06 17:28:13', '2016-07-06 17:28:13', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4527', '10000103', 'text', '即：凡在活动期间激活的客户，活动期间的首笔交易（限最高3手大庆原油指数），若发生亏损不论亏损多少，公司将承担所有亏损费用包括手续费、延期费及点差。', null, null, '2016-07-06 17:33:15', '2016-07-06 17:33:15', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4528', '10000001', 'text', '欧元区市场长期通胀预期指标跌至纪录低点1.265%', null, null, '2016-07-06 17:45:05', '2016-07-06 17:45:05', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4529', '10000001', 'text', '引发市场的风险情绪，使得反弹后的欧元和油价小幅下落', null, null, '2016-07-06 17:45:34', '2016-07-06 17:45:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4530', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577cd39d84c44.jpg', null, null, '2016-07-06 17:47:09', '2016-07-06 17:47:09', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4531', '10000103', 'text', '不懂行情？不会操作？没关系，我们还有投资神器——“轩湛•高德直播室”，名师坐镇实时跟踪行情并及时布局，把握这次机会吧#4#4', null, null, '2016-07-06 17:49:57', '2016-07-06 17:49:57', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4532', '8', 'text', '大家下午好！#9', null, null, '2016-07-06 17:52:51', '2016-07-06 17:52:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4533', '10000103', 'text', '下午好伍老师#6', null, null, '2016-07-06 17:56:22', '2016-07-06 17:56:22', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4534', '8', 'text', '小榆老师好#17', null, null, '2016-07-06 17:57:19', '2016-07-06 17:57:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4535', '10000103', 'text', ' 接下来就把直播室交给伍老师#6辛苦了#9#9#9', null, null, '2016-07-06 17:57:29', '2016-07-06 17:57:29', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4536', '8', 'text', '好的谢谢小榆老师', null, null, '2016-07-06 17:58:52', '2016-07-06 17:58:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4537', '8', 'text', '接下来的时间将由我给大家进行行情直播', null, null, '2016-07-06 18:00:22', '2016-07-06 18:00:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4538', '8', 'text', '今晚的有一个重要数据将要公布，大家还记得吧', null, null, '2016-07-06 18:07:12', '2016-07-06 18:07:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4539', '8', 'text', '原本是每周三凌晨4点30公布，这次因为美国的独立日假期延迟了一天', null, null, '2016-07-06 18:09:25', '2016-07-06 18:09:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4540', '8', 'text', '具体公布时间是在周四04:30', null, null, '2016-07-06 18:11:01', '2016-07-06 18:11:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4541', '8', 'text', '这个数据就是美国至7月1日当周API原油库存数据，这次的预测值比前值要大', null, null, '2016-07-06 18:19:10', '2016-07-06 18:19:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4542', '8', 'text', '说明市场对库存数据并不太看好，因此预测值是利空的', null, null, '2016-07-06 18:22:17', '2016-07-06 18:22:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4543', '8', 'text', '下面一起看看盘面的情况', null, null, '2016-07-06 18:28:26', '2016-07-06 18:28:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4544', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577cdde06bc4d.png', null, null, '2016-07-06 18:30:56', '2016-07-06 18:30:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4545', '8', 'text', '开盘延续昨天的跌势，创新低后，反弹回升，之后并没有延续上涨势头，冲高回落，目前差不多已收回日内涨幅。', null, null, '2016-07-06 18:37:09', '2016-07-06 18:37:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4546', '8', 'text', '盘面依然处弱势格局', null, null, '2016-07-06 18:39:17', '2016-07-06 18:39:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4547', '8', 'text', '稳健的操作思路是 以高空为主，顺势而为', null, null, '2016-07-06 18:40:44', '2016-07-06 18:40:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4548', '8', 'text', '目前原油继续下挫，创出新低', null, null, '2016-07-06 18:45:12', '2016-07-06 18:45:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4549', '8', 'text', '空头力量很是强劲，大家暂时不要轻易去抢反弹', null, null, '2016-07-06 18:55:57', '2016-07-06 18:55:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4550', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577ce50be67ec.png', null, null, '2016-07-06 19:01:32', '2016-07-06 19:01:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4551', '8', 'text', '还是昨天的图，突破了黄金分割线61.8%，那么大概率是会奔着前低去的。', null, null, '2016-07-06 19:03:07', '2016-07-06 19:03:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4552', '8', 'text', '白天的反弹也是受分割线61.8%的压制，冲高回落', null, null, '2016-07-06 19:05:02', '2016-07-06 19:05:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4553', '8', 'text', '下方的支撑在303附近', null, null, '2016-07-06 19:05:44', '2016-07-06 19:05:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4554', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577ce8614aa4d.png', null, null, '2016-07-06 19:15:45', '2016-07-06 19:15:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4555', '8', 'text', '早前美元指数突破了近期的下跌趋势线，目前回调到趋势线附近，若是站稳继续上行，原油将进一步承压。', null, null, '2016-07-06 19:20:27', '2016-07-06 19:20:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4556', '10000155', 'text', '#9', null, null, '2016-07-06 19:28:59', '2016-07-06 19:28:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4557', '8', 'text', '鉴于目前空头力量的强势，手上有多单的朋友，要注意控制好仓位了', null, null, '2016-07-06 19:31:25', '2016-07-06 19:31:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4558', '10000102', 'text', '又创新低了', null, null, '2016-07-06 19:37:28', '2016-07-06 19:37:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4559', '8', 'text', '318-319可作为目前短期的支撑，若跌破，312上方多单超两层仓位的朋友可先锁仓一半，312下方的可先出。（个人建议，仅供参考）', null, null, '2016-07-06 19:40:50', '2016-07-06 19:40:50', '-1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4560', '8', 'text', '308-309可作为目前短期的支撑，若跌破，312上方多单超两层仓位的朋友可先锁仓一半，312下方的可先出。#9（个人建议，仅供参考）', null, null, '2016-07-06 19:41:49', '2016-07-06 19:41:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4561', '8', 'text', '目前在319支撑处有小幅反弹', null, null, '2016-07-06 19:54:00', '2016-07-06 19:54:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4562', '10000102', 'text', '老师，什么是黄金分割', null, null, '2016-07-06 19:54:15', '2016-07-06 19:54:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4563', '8', 'text', '创新低后，又反弹上去，那后市有可能出现一段时间的震荡行情。', null, null, '2016-07-06 19:59:44', '2016-07-06 19:59:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4564', '8', 'text', '#9', '10000155', '#9', '2016-07-06 20:00:22', '2016-07-06 20:00:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4565', '8', 'text', '你可以在划线工具中找到，就是斐波那契线，黄金比例。', '10000102', '老师，什么是黄金分割', '2016-07-06 20:03:37', '2016-07-06 20:03:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4566', '8', 'text', '看来309支撑位置还是比较强力的#17', null, null, '2016-07-06 20:11:21', '2016-07-06 20:11:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4567', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577cf5d4bc2bf.png', null, null, '2016-07-06 20:13:08', '2016-07-06 20:13:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4568', '8', 'text', '这个支撑位区域之前被多次验证一直未有效突破', null, null, '2016-07-06 20:14:34', '2016-07-06 20:14:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4569', '8', 'text', '空单若是求稳，其实在这个位置也要多注意。', null, null, '2016-07-06 20:18:02', '2016-07-06 20:18:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4570', '10000013', 'text', '老师今晚美联储纪要是几点？', null, null, '2016-07-06 20:26:40', '2016-07-06 20:26:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4571', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577cfa0e4e108.png', null, null, '2016-07-06 20:31:10', '2016-07-06 20:31:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4572', '8', 'text', '美国5月贸易账数据利多原油', null, null, '2016-07-06 20:32:31', '2016-07-06 20:32:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4573', '8', 'text', '市场面临抉择', null, null, '2016-07-06 20:39:32', '2016-07-06 20:39:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4574', '8', 'text', '在次日凌晨2:00', '10000013', '老师今晚美联储纪要是几点？', '2016-07-06 20:44:52', '2016-07-06 20:44:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4575', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577cfdf079628.png', null, null, '2016-07-06 20:47:44', '2016-07-06 20:47:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4576', '8', 'text', '接下来，美国那边还有两个数据，大家可以适当关注一下。', null, null, '2016-07-06 20:49:06', '2016-07-06 20:49:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4577', '8', 'text', '今天的重点还是以下两个数据', null, null, '2016-07-06 20:50:30', '2016-07-06 20:50:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4578', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577cfe9e47621.png', null, null, '2016-07-06 20:50:38', '2016-07-06 20:50:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4579', '8', 'text', ' 美联储将于周四凌晨2点发布6月议息会议纪要', null, null, '2016-07-06 20:52:28', '2016-07-06 20:52:28', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4580', '8', 'text', '其对于英国脱欧公投风险的观点，以及对就业增长放缓和通胀预期下滑的看法值得留意。', null, null, '2016-07-06 20:53:57', '2016-07-06 20:53:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4581', '8', 'text', '英国脱欧公投导致全球金融不确定性，令市场预计美国最早要等到2017年很晚才会加息', null, null, '2016-07-06 21:01:17', '2016-07-06 21:01:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4582', '8', 'text', '高盛也表示，因英国公投结果为令人震惊的退出欧盟，市场对美联储今年将加息的预期大大降低。', null, null, '2016-07-06 21:06:52', '2016-07-06 21:06:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4583', '8', 'text', '花旗银行一份报告指出，英国脱欧公投问题使美联储加息面临更复杂的局面，强化了美国2016年不会再次加息的预期。', null, null, '2016-07-06 21:15:06', '2016-07-06 21:15:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4584', '8', 'text', '即使6月份的非农就业数据超过预期且美国经济数据延续强劲表现，对全球状况不稳定的担忧以及挥之不去的不确定性仍将使美联储按兵不动。', null, null, '2016-07-06 21:20:05', '2016-07-06 21:20:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4585', '8', 'text', '因此，美联储不加息的预期，将利空美元利多原油。', null, null, '2016-07-06 21:28:04', '2016-07-06 21:28:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4586', '8', 'text', '下面我们来看看今年美联储会议纪要原油的价格走势', null, null, '2016-07-06 21:29:14', '2016-07-06 21:29:14', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4587', '8', 'text', '1.  4月FOMC纪要：全球风险有所消退，保留6月加息可能。加息预期导致原油当晚大跌。做空收益空间54.4%！', null, null, '2016-07-06 21:34:44', '2016-07-06 21:34:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4588', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577d09393b72a.png', null, null, '2016-07-06 21:35:53', '2016-07-06 21:35:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4589', '8', 'text', '2.  3月FOMC会议纪要较会议声明强硬，同时暗示4月加息不妥。鸽派预期令油价大幅拉升。收益空间37.5%！ ', null, null, '2016-07-06 21:39:05', '2016-07-06 21:39:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4590', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577d0a181f632.png', null, null, '2016-07-06 21:39:36', '2016-07-06 21:39:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4591', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577d0bac78d3e.png', null, null, '2016-07-06 21:46:20', '2016-07-06 21:46:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4592', '8', 'text', '美国6月Markit服务业PMI终值数据利多原油', null, null, '2016-07-06 21:46:46', '2016-07-06 21:46:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4593', '8', 'text', ' 3.  1月美联储会议纪要：经济下行风险增加，数据决定加息时机。会议令油价继续大幅下行，当晚收益63.5%！', null, null, '2016-07-06 21:50:00', '2016-07-06 21:50:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4594', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577d0cbe641d9.png', null, null, '2016-07-06 21:50:54', '2016-07-06 21:50:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4595', '8', 'text', '由历史数据统计得知，美联储会议纪要后油价的波动不会小。', null, null, '2016-07-06 21:53:56', '2016-07-06 21:53:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4596', '8', 'text', '大家有时间都关注一下，抓住机会，顺势而为。', null, null, '2016-07-06 21:55:25', '2016-07-06 21:55:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4597', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577d0f7399f74.png', null, null, '2016-07-06 22:02:27', '2016-07-06 22:02:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4598', '8', 'text', '恭喜郑老师的多单获利21%平仓#14', null, null, '2016-07-06 22:05:25', '2016-07-06 22:05:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4599', '8', 'text', '行情反弹上来了，多单仓位重的可以抓住机会适当减减仓#9', null, null, '2016-07-06 22:07:44', '2016-07-06 22:07:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4600', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577d122cece5d.png', null, null, '2016-07-06 22:14:05', '2016-07-06 22:14:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4601', '8', 'text', '这项数据对原油利空', null, null, '2016-07-06 22:14:33', '2016-07-06 22:14:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4602', '8', 'text', '原油再次冲高回落，说明上方314压力很强。', null, null, '2016-07-06 22:20:12', '2016-07-06 22:20:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4603', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577d13eeaf420.png', null, null, '2016-07-06 22:21:34', '2016-07-06 22:21:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4604', '8', 'text', '今天的走势目前大致上还是在309-314的区间震荡', null, null, '2016-07-06 22:23:06', '2016-07-06 22:23:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4605', '8', 'text', '反弹上去，又被打下来了。', null, null, '2016-07-06 22:30:48', '2016-07-06 22:30:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4606', '8', 'text', '巴克莱：将美国第二季度GDP增速预期从2.7%下调至2.4%。', null, null, '2016-07-06 22:38:07', '2016-07-06 22:38:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4607', '8', 'text', '波兰央行行长：与欧洲央行行长德拉基进行了讨论；德拉基因英国退欧有所动摇，但美联储主席耶伦没有。', null, null, '2016-07-06 22:49:59', '2016-07-06 22:49:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4608', '10000170', 'text', '巴克莱：将美国第二季度GDP增速预期从2.7%下调至2.4%，老师，这个消息是利空原油的吧？', '10000092', '老师，今晚做空还是做多', '2016-07-06 23:00:30', '2016-07-06 23:00:30', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4609', '8', 'text', '利空美元，利多原油。', '10000170', '巴克莱：将美国第二季度GDP增速预期从2.7%下调至2.4%，老师，这个消息是利空原油的吧？', '2016-07-06 23:03:08', '2016-07-06 23:03:08', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4610', '8', 'text', '由于后市有美联储会议纪要和API数据公布，目前市场多空交锋比较激烈。', null, null, '2016-07-06 23:05:10', '2016-07-06 23:05:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4611', '8', 'text', '稳健操作，还是等会议纪要后，顺势而为。', null, null, '2016-07-06 23:07:37', '2016-07-06 23:07:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4612', '8', 'text', '这行情#20', null, null, '2016-07-06 23:24:00', '2016-07-06 23:24:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4613', '8', 'text', '震荡行情，建议不要轻易下单。', null, null, '2016-07-06 23:25:22', '2016-07-06 23:25:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4614', '8', 'text', '这样低位震荡，对空头不太有利。', null, null, '2016-07-06 23:27:39', '2016-07-06 23:27:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4615', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577d25ffe84e0.png', null, null, '2016-07-06 23:38:40', '2016-07-06 23:38:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4616', '8', 'text', '低点依次抬高，有继续上升的趋势。', null, null, '2016-07-06 23:39:47', '2016-07-06 23:39:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4617', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160706/577d289ad5b07.png', null, null, '2016-07-06 23:49:46', '2016-07-06 23:49:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4618', '10000155', 'text', '#20', null, null, '2016-07-06 23:50:03', '2016-07-06 23:50:03', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4619', '8', 'text', '短周期看，多头底部似乎已经形成。', null, null, '2016-07-06 23:50:59', '2016-07-06 23:50:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4620', '8', 'text', '#9', '10000155', '#20', '2016-07-06 23:51:49', '2016-07-06 23:51:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4621', '10000155', 'text', '利好消息一出，就能上涨。', null, null, '2016-07-06 23:52:10', '2016-07-06 23:52:10', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4622', '10000155', 'text', '回调可再做多#4', null, null, '2016-07-06 23:54:03', '2016-07-06 23:54:03', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4623', '8', 'text', '哈哈，是的，耐心等待就好。', '10000155', '利好消息一出，就能上涨。', '2016-07-06 23:54:12', '2016-07-06 23:54:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4624', '8', 'text', '也要注意控制好仓位', '10000155', '回调可再做多#4', '2016-07-06 23:54:50', '2016-07-06 23:54:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4625', '10000155', 'text', '#9', null, null, '2016-07-06 23:55:36', '2016-07-06 23:55:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4626', '8', 'text', '亚特兰大联储GDPNOW模型：将美国第二季度GDP增速预期从2.6%下调至2.4%。', null, null, '2016-07-06 23:56:16', '2016-07-06 23:56:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4627', '10000155', 'text', '利好消息开始不断了', null, null, '2016-07-06 23:57:53', '2016-07-06 23:57:53', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4628', '10000170', 'text', '希望今晚的美联储纪要也是利好原有的啊', null, null, '2016-07-06 23:58:47', '2016-07-06 23:58:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4629', '8', 'text', '接下来就看这周的的几个重磅数据了', '10000155', '利好消息开始不断了', '2016-07-07 00:01:41', '2016-07-07 00:01:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4630', '8', 'text', '市场预期会议上可能会表现倾向鸽派，利好原油。#4', '10000170', '希望今晚的美联储纪要也是利好原有的啊', '2016-07-07 00:03:43', '2016-07-07 00:03:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4631', '10000170', 'text', '是啊，然后让我的多单赶紧出来啊', null, null, '2016-07-07 00:05:33', '2016-07-07 00:05:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4632', '10000155', 'text', '开涨了#18', null, null, '2016-07-07 00:06:58', '2016-07-07 00:06:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4633', '8', 'text', '恩，抓住机会，行情已经开始启动了。', '10000170', '是啊，然后让我的多单赶紧出来啊', '2016-07-07 00:07:11', '2016-07-07 00:07:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4634', '8', 'text', '#20', '10000155', '开涨了#18', '2016-07-07 00:07:54', '2016-07-07 00:07:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4635', '8', 'text', '创新高了', null, null, '2016-07-07 00:10:13', '2016-07-07 00:10:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4636', '10000155', 'text', '先看3220吗，老师', null, null, '2016-07-07 00:10:33', '2016-07-07 00:10:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4637', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577d2fea1392e.png', null, null, '2016-07-07 00:20:58', '2016-07-07 00:20:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4638', '8', 'text', '从黄金分割线来看，目前到了第一反弹目标，上方目标在320、322.  多单仓位重的朋友可先适当减部分仓位。', null, null, '2016-07-07 00:24:31', '2016-07-07 00:24:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4639', '8', 'text', '第二目标在320，可以根据自身仓位带上合适的移动止损保护收益。', '10000155', '先看3220吗，老师', '2016-07-07 00:29:35', '2016-07-07 00:29:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4640', '8', 'text', '今天4点30 还有一个重要数据，美国API原油库存  ', null, null, '2016-07-07 00:33:07', '2016-07-07 00:33:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4641', '8', 'text', '下面我们也来讲讲', null, null, '2016-07-07 00:33:25', '2016-07-07 00:33:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4642', '8', 'text', '这个数据的影响力跟EIA库存相仿，届时会引起油价较大的波动。', null, null, '2016-07-07 00:33:57', '2016-07-07 00:33:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4643', '8', 'text', '上周截至6月17日当周，美国API原油库存大减522.4万桶，录得半年来最大降幅，美国API原油库存再次超预期大幅削减，无疑表明市场原油需求较为强劲。', null, null, '2016-07-07 00:37:09', '2016-07-07 00:37:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4644', '8', 'text', '在英国脱欧忧虑暂缓情况下，美国原油库存的削减无疑将会为国际原油价格提供短线强劲支撑。', null, null, '2016-07-07 00:38:02', '2016-07-07 00:38:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4645', '8', 'text', '美国今年的汽油消费量料达到纪录高位，超过2007年创下的前次峰值。', null, null, '2016-07-07 00:40:36', '2016-07-07 00:40:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4646', '8', 'text', '据EIA估计，美国今年前四个月石油产品消耗量增加24万桶/日。', null, null, '2016-07-07 00:41:11', '2016-07-07 00:41:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4647', '10000155', 'text', '下不来，没机会。#12', null, null, '2016-07-07 00:43:28', '2016-07-07 00:43:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4648', '8', 'text', '据路透上月报道，美国对石油需求增长幅度最高，同时EIA公布的周度数据，过去三个月来美国原油总需求已经增加了2000万桶。使得今年的API库存是一直在稳步下降。', null, null, '2016-07-07 00:51:00', '2016-07-07 00:51:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4649', '8', 'text', '你手上是什么单？', '10000155', '下不来，没机会。#12', '2016-07-07 00:51:26', '2016-07-07 00:51:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4650', '10000155', 'text', '空仓', null, null, '2016-07-07 00:52:04', '2016-07-07 00:52:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4651', '8', 'text', '精神好可以等美联储会议纪要后再操作#17', '10000155', '空仓', '2016-07-07 00:53:47', '2016-07-07 00:53:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4652', '8', 'text', '随着夏季驾驶季节的来临，截至 6 月 17 日当周美国汽油需求达到了创纪录的 980 万桶。', null, null, '2016-07-07 00:54:01', '2016-07-07 00:54:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4653', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577d3849507a2.png', null, null, '2016-07-07 00:56:41', '2016-07-07 00:56:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4654', '8', 'text', '下面是往期API原油库存数据及行情走势', null, null, '2016-07-07 01:06:36', '2016-07-07 01:06:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4655', '10000155', 'text', '#2', null, null, '2016-07-07 01:06:49', '2016-07-07 01:06:49', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4656', '10000155', 'text', '暴涨啊', null, null, '2016-07-07 01:07:32', '2016-07-07 01:07:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4657', '8', 'text', '6月29日凌晨04:30，美国至6月24日当周API原油库存(万桶)录得-386小幅低于预期，预期值-237.5。利多油价。', null, null, '2016-07-07 01:09:29', '2016-07-07 01:09:29', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4658', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577d3b85566d1.png', null, null, '2016-07-07 01:10:29', '2016-07-07 01:10:29', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4659', '8', 'text', '这种短线快速上涨一般是不能持久的', '10000155', '暴涨啊', '2016-07-07 01:12:58', '2016-07-07 01:12:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4660', '8', 'text', '大家要注意喔，这种反弹只是可能只是短暂的，一旦重新跌回去就直接是空头趋势了。', null, null, '2016-07-07 01:14:18', '2016-07-07 01:14:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4661', '10000155', 'text', '不会吧#8', null, null, '2016-07-07 01:15:20', '2016-07-07 01:15:20', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4662', '8', 'text', '现在有个反弹机会可以让你们弥补之前仓位过重，要抓住机会啊。', null, null, '2016-07-07 01:15:34', '2016-07-07 01:15:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4663', '8', 'text', '日线图看空头已是兵临城下了', '10000155', '不会吧#8', '2016-07-07 01:18:04', '2016-07-07 01:18:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4664', '10000155', 'text', '现价做空', null, null, '2016-07-07 01:19:12', '2016-07-07 01:19:12', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4665', '10000194', 'text', '什么时候可以开始做空了', null, null, '2016-07-07 01:19:50', '2016-07-07 01:19:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4666', '8', 'text', '现在做也是可以的，不过有点激进，一定要做的话，控制好仓位，带上止盈止损。', '10000155', '现价做空', '2016-07-07 01:22:05', '2016-07-07 01:22:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4667', '8', 'text', '稳健的话，等后市进一步出现信号。', '10000194', '什么时候可以开始做空了', '2016-07-07 01:22:54', '2016-07-07 01:22:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4668', '10000194', 'text', '好的、那就等老师通知了', null, null, '2016-07-07 01:23:34', '2016-07-07 01:23:34', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4669', '8', 'text', '恩 你可以多关注下直播室左上方的策略栏', '10000194', '好的、那就等老师通知了', '2016-07-07 01:26:54', '2016-07-07 01:26:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4670', '8', 'text', '目前，价格在320附近承压', null, null, '2016-07-07 01:28:27', '2016-07-07 01:28:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4671', '8', 'text', '好，下面继续来看往期的API走势，遇上重要数据，就要抓住机会。', null, null, '2016-07-07 01:32:19', '2016-07-07 01:32:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4672', '8', 'text', '6月22日凌晨04:30，美国至6月17日当周API原油库存(万桶)录得-522.4大幅低于预期，预期值-190。利多油价。', null, null, '2016-07-07 01:32:41', '2016-07-07 01:32:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4673', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577d41017f2f0.png', null, null, '2016-07-07 01:33:53', '2016-07-07 01:33:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4674', '8', 'text', '6月15日凌晨04:30，美国至6月10日当周API原油库存(万桶)录得115.8，大幅高于预期，预期值-226.8。利空油价。', null, null, '2016-07-07 01:35:26', '2016-07-07 01:35:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4675', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577d41844689a.png', null, null, '2016-07-07 01:36:04', '2016-07-07 01:36:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4676', '8', 'text', '统计分析，API公布后带来的收益空间平均达到96.75％,而且API公布后不仅影响当晚的行情，还会公布后当天的行情。', null, null, '2016-07-07 01:36:29', '2016-07-07 01:36:29', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4677', '8', 'text', '同时据统计API公布的数据与当天晚上EIA数据相仿，相似度达到73.3%。', null, null, '2016-07-07 01:37:02', '2016-07-07 01:37:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4678', '8', 'text', '所以若是今天API库存录得是利空油价，当天晚上的EIA库存利空油价的可能性非常高。', null, null, '2016-07-07 01:38:17', '2016-07-07 01:38:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4679', '10000170', 'text', '预计API是利空油价的码？', null, null, '2016-07-07 01:39:44', '2016-07-07 01:39:44', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4680', '8', 'text', '预测值是利空的，关键看公布值', '10000170', '预计API是利空油价的码？', '2016-07-07 01:40:45', '2016-07-07 01:40:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4681', '10000170', 'text', '那多单最好在API公布之前出来', null, null, '2016-07-07 01:41:05', '2016-07-07 01:41:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4682', '8', 'text', '好了，今天的直播就到这里结束了。后市两个重要数据，上面也给大家作了分析，数据出来后，顺势而为即可。', null, null, '2016-07-07 01:42:17', '2016-07-07 01:42:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4683', '8', 'text', '对的，这样更稳健。', '10000170', '那多单最好在API公布之前出来', '2016-07-07 01:43:04', '2016-07-07 01:43:04', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4684', '10000170', 'text', 'O(∩_∩)O谢谢老师', null, null, '2016-07-07 01:43:29', '2016-07-07 01:43:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4685', '8', 'text', '#9', '10000170', 'O(∩_∩)O谢谢老师', '2016-07-07 01:43:37', '2016-07-07 01:43:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4686', '8', 'text', '朋友们晚安！我们明天再见！#9', null, null, '2016-07-07 01:44:16', '2016-07-07 01:44:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4687', '10000001', 'text', '大家早上好！#9', null, null, '2016-07-07 09:26:17', '2016-07-07 09:26:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4688', '10000001', 'text', '在现实世界，从假定完美的认识中可以获得的任何理论，都是有缺陷的。在现实世界，买或卖的决定的依据——不是古典经济学家的理想——而是人们的期望。并且，在现实世界中，人们对任何事物的认识都是不完全的', null, null, '2016-07-07 09:27:52', '2016-07-07 09:27:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4689', '10000001', 'text', '我是轩湛投资观察室的吴勇坚，接下来我来为大家做直播，大家有什么疑问或者疑惑，可以在互动区跟我交流喔', null, null, '2016-07-07 09:29:29', '2016-07-07 09:29:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4690', '5', 'text', '老师早上好，原油太疯狂了，又是10元钱的', null, null, '2016-07-07 09:31:36', '2016-07-07 09:31:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4691', '10000001', 'text', '后半夜美联储6月会议纪要鸽派，而且美联储官员在加息上产生分歧，使得美元走弱，同时API库存大幅低于预期和有消息称哥伦比亚第二大输油管遭袭，众多利多因素下，助推油价反弹', '5', '老师早上好，原油太疯狂了，又是10元钱的', '2016-07-07 09:36:58', '2016-07-07 09:36:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4692', '10000274', 'text', '老师，早上好！今天EIA 原油有布局吗', null, null, '2016-07-07 09:39:50', '2016-07-07 09:39:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4693', '10000001', 'text', '有的，到晚上我们老师会给出布局的#9', '10000274', '老师，早上好！今天EIA 原油有布局吗', '2016-07-07 09:41:11', '2016-07-07 09:41:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4694', '10000274', 'text', '谢谢老师', null, null, '2016-07-07 09:41:34', '2016-07-07 09:41:34', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4695', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577db45b2f9ae.png', null, null, '2016-07-07 09:46:03', '2016-07-07 09:46:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4696', '10000001', 'text', '昨日我们郑老师的短线多单太给力了#14', null, null, '2016-07-07 09:46:45', '2016-07-07 09:46:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4697', '10000001', 'text', '哈哈，大家跟紧我们老师的脚步哦#14', null, null, '2016-07-07 09:48:50', '2016-07-07 09:48:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4698', '10000001', 'text', '好了，接下来我们回顾一下昨天的行情', null, null, '2016-07-07 09:49:09', '2016-07-07 09:49:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4699', '10000001', 'text', '昨日，欧元区市场长期通胀预期指标跌至纪录低点1.265%，引发的风险情绪', null, null, '2016-07-07 09:49:51', '2016-07-07 09:49:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4700', '10000001', 'text', '使得油价反弹到315附近后下落到支撑位310 附近', null, null, '2016-07-07 09:50:11', '2016-07-07 09:50:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4701', '10000001', 'text', '但到晚上有消息称美国5月原油出口量升至创纪录的约66.2万桶/日', null, null, '2016-07-07 09:54:56', '2016-07-07 09:54:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4702', '10000001', 'text', '使得油价支撑位反弹至短期压力位325附近后快速下落', null, null, '2016-07-07 09:55:28', '2016-07-07 09:55:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4703', '10000001', 'text', '同时有消息称哥伦比亚第二大输油管遭袭，使得油价从支撑位310继续反弹，后半夜美联储6月会议纪要鸽派，而且美联储官员在加息上产生分歧，使得美元走弱，助推油价上涨', null, null, '2016-07-07 09:56:33', '2016-07-07 09:56:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4704', '10000001', 'text', '凌晨4:30的API原油库存更是录得大幅低于预期', null, null, '2016-07-07 09:56:46', '2016-07-07 09:56:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4705', '10000001', 'text', '更是创13个月以来最大降幅', null, null, '2016-07-07 09:58:16', '2016-07-07 09:58:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4706', '10000001', 'text', '表明美国夏季对原油的需求非常强劲', null, null, '2016-07-07 09:58:48', '2016-07-07 09:58:48', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4707', '10000001', 'text', '助推油价冲破压力位320', null, null, '2016-07-07 09:59:03', '2016-07-07 09:59:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4708', '10000102', 'text', '现在是上方均线压力，已经高开那么多了，还会继续上冲吗？', null, null, '2016-07-07 10:01:52', '2016-07-07 10:01:52', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4709', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577db984ee117.png', null, null, '2016-07-07 10:08:05', '2016-07-07 10:08:05', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4710', '10000001', 'text', '目前上方短期压力位324-325区间，日线级别压力位330，目前油价较为强势，小幅上升的机率较大#9', '10000102', '现在是上方均线压力，已经高开那么多了，还会继续上冲吗？', '2016-07-07 10:10:26', '2016-07-07 10:10:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4711', '10000001', 'text', '下方支撑位320附近', '10000102', '现在是上方均线压力，已经高开那么多了，还会继续上冲吗？', '2016-07-07 10:11:33', '2016-07-07 10:11:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4712', '10000001', 'text', '好了，我们回到油价的走势上', null, null, '2016-07-07 10:18:29', '2016-07-07 10:18:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4713', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577dbc86dd74c.png', null, null, '2016-07-07 10:20:54', '2016-07-07 10:20:54', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4714', '10000001', 'text', '今日油价开盘后站在短期支撑位320附近盘旋', null, null, '2016-07-07 10:21:36', '2016-07-07 10:21:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4715', '10000102', 'text', '好的，谢谢#14', null, null, '2016-07-07 10:23:10', '2016-07-07 10:23:10', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4716', '10000001', 'text', '由于昨晚油价上涨2.3%多', null, null, '2016-07-07 10:26:14', '2016-07-07 10:26:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4717', '10000001', 'text', '上午油价的走势预测将是在压力位附近盘整', null, null, '2016-07-07 10:26:55', '2016-07-07 10:26:55', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4718', '10000001', 'text', '预测上午油价的走势将是在支撑位320附近盘整的', null, null, '2016-07-07 10:29:53', '2016-07-07 10:29:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4719', '10000001', 'text', '所以持有重仓多单的朋友可以在该位置适当减减仓的', null, null, '2016-07-07 10:33:22', '2016-07-07 10:33:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4720', '10000001', 'text', '因为今日晚上19:30欧洲央行公布6月货币政策会议纪要', null, null, '2016-07-07 10:33:55', '2016-07-07 10:33:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4721', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577dbfa976396.png', null, null, '2016-07-07 10:34:17', '2016-07-07 10:34:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4722', '10000001', 'text', '预测风险情绪蛮高的', null, null, '2016-07-07 10:34:37', '2016-07-07 10:34:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4723', '10000001', 'text', '因为目前市场都是英国退欧带来的金融动荡和美联储是否加息的风险情绪', null, null, '2016-07-07 10:46:29', '2016-07-07 10:46:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4724', '10000001', 'text', '好了，我们去看看股市的情况', null, null, '2016-07-07 10:46:58', '2016-07-07 10:46:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4725', '10000001', 'text', '今日大盘低开后，依然是在3000点上方高位震荡的', null, null, '2016-07-07 10:52:08', '2016-07-07 10:52:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4726', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577dc441436bb.png', null, null, '2016-07-07 10:53:53', '2016-07-07 10:53:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4727', '10000001', 'text', '今日主力资金继续是流出的', null, null, '2016-07-07 10:54:13', '2016-07-07 10:54:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4728', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577dc543745d7.png', null, null, '2016-07-07 10:58:11', '2016-07-07 10:58:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4729', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577dc5f68fab5.png', null, null, '2016-07-07 11:01:10', '2016-07-07 11:01:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4730', '10000001', 'text', '昨晚美联储6月会议纪要偏鸽派，最近数据显示美国就业市场是有所放缓，使得避险情绪蛮高的，加上英国退欧对欧元区的影响，使得部分欧元区成员国的债权收益率跌至负数了', null, null, '2016-07-07 11:04:42', '2016-07-07 11:04:42', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4731', '10000001', 'text', '推动黄金等有色金属板块领涨在前的', null, null, '2016-07-07 11:05:32', '2016-07-07 11:05:32', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4732', '10000001', 'text', '昨晚美联储6月会议纪要偏鸽派，最近数据显示美国就业市场是有所放缓，使得避险情绪蛮高的，加上英国退欧对欧元区的影响，使得部分欧元区成员国的债券收益率跌至负数了', null, null, '2016-07-07 11:07:21', '2016-07-07 11:07:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4733', '10000001', 'text', '推动今日的黄金等有色金属板块领涨居前的', null, null, '2016-07-07 11:08:08', '2016-07-07 11:08:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4734', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577dca492ae6b.png', null, null, '2016-07-07 11:19:37', '2016-07-07 11:19:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4735', '10000001', 'text', '目前大盘是有所回落', null, null, '2016-07-07 11:32:31', '2016-07-07 11:32:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4736', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577dcee9a01e2.png', null, null, '2016-07-07 11:39:21', '2016-07-07 11:39:21', '1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('4737', '10000001', 'text', '支撑位320附近，轻仓短线多单（个人建议，仅供参考）', null, null, '2016-07-07 11:40:10', '2016-07-07 11:40:10', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('4738', '10000001', 'text', '支撑位320附近，轻仓短线多单（个人建议，仅供参考）', null, null, '2016-07-07 11:40:23', '2016-07-07 11:40:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4739', '10000001', 'text', '获利就跑', null, null, '2016-07-07 11:42:13', '2016-07-07 11:42:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4740', '10000001', 'text', '好了，我们回到股市收盘情况', null, null, '2016-07-07 11:45:16', '2016-07-07 11:45:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4741', '10000001', 'text', '截至收盘，上证指数报3002.51点，下跌14.78点，跌0.49%，成交额1668.38亿', null, null, '2016-07-07 11:48:44', '2016-07-07 11:48:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4742', '10000001', 'text', '盘面上以黄金、稀土有色为代表的资源品板块成市场主攻品种，同时白酒等防御性板块延续昨日强势走势较为坚挺', null, null, '2016-07-07 11:55:57', '2016-07-07 11:55:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4743', '10000102', 'text', '咋突然下跌了？', null, null, '2016-07-07 12:02:49', '2016-07-07 12:02:49', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4744', '10000001', 'text', '好了，我们先休息一会#9', null, null, '2016-07-07 12:07:56', '2016-07-07 12:07:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4745', '10000001', 'text', '昨晚的美联储货币会议纪要，美联储官员们在经济前景以及加息问题上产生分歧，有部分 官员认为要尽快加息，加上今晚19:30欧洲央行将公布6月货币政策会议纪要，今日市场风险情绪较大', '10000102', '咋突然下跌了？', '2016-07-07 12:15:44', '2016-07-07 12:15:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4746', '10000001', 'text', '大家下午好！#9', null, null, '2016-07-07 13:01:39', '2016-07-07 13:01:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4747', '10000001', 'text', '目前油价继续支撑位附近震荡', null, null, '2016-07-07 13:06:20', '2016-07-07 13:06:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4748', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577de52378e51.png', null, null, '2016-07-07 13:14:11', '2016-07-07 13:14:11', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4749', '10000001', 'text', '如果油价在3点欧盘开盘后还没有下破支撑位320，那么油价再走一波小幅的反弹的机率还是蛮大', null, null, '2016-07-07 13:17:35', '2016-07-07 13:17:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4750', '10000001', 'text', '昨日，因公投后对于脱欧的恐慌令零售投资者中产生了“**雪球效应”，暂停交易的英国地产基金数量增加了逾一倍，使得超过180亿英镑（230亿美元）资产冻结。', null, null, '2016-07-07 13:24:04', '2016-07-07 13:24:04', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4751', '10000001', 'text', '成为2008年金融危机以来投资基金运作失灵规模最大的一次', null, null, '2016-07-07 13:24:18', '2016-07-07 13:24:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4752', '10000001', 'text', '今日关注数据方面：', null, null, '2016-07-07 13:34:23', '2016-07-07 13:34:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4753', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577dea6ba9028.png', null, null, '2016-07-07 13:36:43', '2016-07-07 13:36:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4754', '10000001', 'text', '19:30 美国6月挑战者企业裁员人数', null, null, '2016-07-07 13:42:36', '2016-07-07 13:42:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4755', '10000001', 'text', '该数据反映了企业用工情况的好坏', null, null, '2016-07-07 13:44:10', '2016-07-07 13:44:10', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4756', '10000001', 'text', '若裁员人数增加，显示就业市场恶化，对美元不利，利多油价', null, null, '2016-07-07 13:44:39', '2016-07-07 13:44:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4757', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577dec8c5eebf.png', null, null, '2016-07-07 13:45:48', '2016-07-07 13:45:48', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4758', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577deca183633.png', null, null, '2016-07-07 13:46:09', '2016-07-07 13:46:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4759', '10000001', 'text', '今晚19:30 还有重磅事件：欧洲央行公布6月货币政策会议纪要', null, null, '2016-07-07 13:46:54', '2016-07-07 13:46:54', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4760', '10000001', 'text', '昨日，欧元区市场长期通胀预期指标跌至纪录低点1.265%', null, null, '2016-07-07 13:51:26', '2016-07-07 13:51:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4761', '10000001', 'text', '所以，会议纪要很有可能会谈及延长甚至扩大QE和宽松来延缓退欧对欧洲经济的短期冲击', null, null, '2016-07-07 13:58:36', '2016-07-07 13:58:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4762', '10000001', 'text', '从而利空欧元和原油', null, null, '2016-07-07 14:00:15', '2016-07-07 14:00:15', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4763', '10000001', 'text', '上一次会议央行致力于将通胀推升至目标水平2.0%', null, null, '2016-07-07 14:06:06', '2016-07-07 14:06:06', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4764', '10000001', 'text', '议纪要利好欧元和原油，带动原油从低位反弹3%，收益空间100%！', null, null, '2016-07-07 14:06:12', '2016-07-07 14:06:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4765', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577df1a9822f1.png', null, null, '2016-07-07 14:07:37', '2016-07-07 14:07:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4766', '10000001', 'text', '欧洲央行公布的3月货币政策会议纪要显示，欧洲央行成员认为，3月降息幅度应该“更大”。会议纪要依然偏向于利空欧元和原油，打压原油下跌2.5%，收益空间 83.3%！', null, null, '2016-07-07 14:10:57', '2016-07-07 14:10:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4767', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577df2820d2bf.png', null, null, '2016-07-07 14:11:14', '2016-07-07 14:11:14', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4768', '10000001', 'text', '欧洲央行公布的1月货币政策会议纪要指示：欧元区经济前景处于下行风险。 会议纪要大幅利空欧元，打压原油从最高位大跌10%！加上杠杆收益空间高达330%！', null, null, '2016-07-07 14:15:07', '2016-07-07 14:15:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4769', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577df3903854c.png', null, null, '2016-07-07 14:15:44', '2016-07-07 14:15:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4770', '10000001', 'text', '欧洲央行公布的12月货币政策会议纪要指出：欧元区经济复苏较为温和且脆弱，打压欧元和原油，令原油当日大跌6%，加上33.3倍杠杆共199.8%的收益空间', null, null, '2016-07-07 14:25:45', '2016-07-07 14:25:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4771', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577df70aeb4b2.png', null, null, '2016-07-07 14:30:35', '2016-07-07 14:30:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4772', '10000001', 'text', '今晚的数据还是比较多的', null, null, '2016-07-07 14:40:41', '2016-07-07 14:40:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4773', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577dfa22b4ad6.png', null, null, '2016-07-07 14:43:46', '2016-07-07 14:43:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4774', '10000001', 'text', '20:15 美国6月ADP就业人数', null, null, '2016-07-07 14:44:24', '2016-07-07 14:44:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4775', '10000006', 'text', '是啊', null, null, '2016-07-07 14:45:50', '2016-07-07 14:45:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4776', '10000006', 'text', '什么情况', null, null, '2016-07-07 14:45:55', '2016-07-07 14:45:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4777', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577dfaa5d2337.jpg', null, null, '2016-07-07 14:45:57', '2016-07-07 14:45:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4778', '8', 'text', '日线级别看空，尝试布局中线空单，带好止盈止损。#9（个人建议，仅供参考）', null, null, '2016-07-07 14:46:12', '2016-07-07 14:46:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4779', '10000001', 'text', '伍老师布局的是中线空单，目前反弹还是比较无力的', '10000006', '什么情况', '2016-07-07 14:50:26', '2016-07-07 14:50:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4780', '10000001', 'text', '德国公布德国5月季调后工业产出月率录得-1.3%，表明今年德国第二季度工业状况将会略微疲软，引发欧元和油价的下跌', null, null, '2016-07-07 14:56:44', '2016-07-07 14:56:44', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4781', '10000001', 'text', '受到昨晚API库存下降至13个月以来的最低，表明美国夏季原油的需求非常强劲，使得欧洲开盘后油价小幅拉升', null, null, '2016-07-07 15:10:31', '2016-07-07 15:10:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4782', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e00d82a88d.png', null, null, '2016-07-07 15:12:24', '2016-07-07 15:12:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4783', '10000001', 'text', '好了，回到今晚数据方面，今晚数据比较多啊', null, null, '2016-07-07 15:17:39', '2016-07-07 15:17:39', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4784', '10000001', 'text', '预测今晚油价的波动将会很大，大家一定要跟进脚步啊', null, null, '2016-07-07 15:18:22', '2016-07-07 15:18:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4785', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e0292d4182.png', null, null, '2016-07-07 15:19:46', '2016-07-07 15:19:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4786', '10000001', 'text', '8：30 美国至7月2日当周初请失业金人数', null, null, '2016-07-07 15:20:15', '2016-07-07 15:20:15', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4787', '10000001', 'text', '20:30 美国至7月2日当周初请失业金人数', null, null, '2016-07-07 15:20:34', '2016-07-07 15:20:34', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4788', '10000001', 'text', '好了，目前股市收盘了，去关注一下股市的收盘情况', null, null, '2016-07-07 15:21:43', '2016-07-07 15:21:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4789', '10000001', 'text', '今日A股再一次低开震荡，盘中虽有回来，但上攻的趋势仍然没变；到了午后各大题材带动着大盘震荡回升，再度收出一根小阳线', null, null, '2016-07-07 15:22:53', '2016-07-07 15:22:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4790', '10000001', 'text', '截至收盘，上证指数以3016.85点报收，成交2702亿元', null, null, '2016-07-07 15:23:00', '2016-07-07 15:23:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4791', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e03997dd4e.jpg', null, null, '2016-07-07 15:24:09', '2016-07-07 15:24:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4792', '10000001', 'text', '盘面上看，有色金属、黄金和稀土永磁等板块走势较好', null, null, '2016-07-07 15:29:37', '2016-07-07 15:29:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4793', '10000001', 'text', '有消息称，6月下旬起，中国黄河以南部分省份遭遇强降雨，如湖北、安徽等地，而这些省份多是一些金属的主要产销集散地', null, null, '2016-07-07 15:30:05', '2016-07-07 15:30:05', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4794', '10000001', 'text', '在该消息影响下，有色金属板块相对走强', null, null, '2016-07-07 15:31:27', '2016-07-07 15:31:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4795', '10000001', 'text', '股方面，中色股份(000758)和建新矿业(000688)等走势较好', null, null, '2016-07-07 15:31:41', '2016-07-07 15:31:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4796', '10000001', 'text', '后市展望：', null, null, '2016-07-07 15:32:02', '2016-07-07 15:32:02', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4797', '10000001', 'text', '盘面分化严重，存量博弈下的结构性行情特征明显。预计大盘后市将围绕3000点构筑新的平台', null, null, '2016-07-07 15:34:38', '2016-07-07 15:34:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4798', '10000001', 'text', '建议可以挖掘尚未出现大幅反弹的品种，但目前不要轻易追涨', null, null, '2016-07-07 15:37:17', '2016-07-07 15:37:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4799', '10000001', 'text', '好了，我们回到今晚的关注的重磅数据：', null, null, '2016-07-07 15:47:13', '2016-07-07 15:47:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4800', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e091e91000.png', null, null, '2016-07-07 15:47:42', '2016-07-07 15:47:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4801', '10000001', 'text', '23:00 美国至7月1日当周EIA原油库存', null, null, '2016-07-07 15:48:26', '2016-07-07 15:48:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4802', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e0b4a6325b.png', null, null, '2016-07-07 15:56:58', '2016-07-07 15:56:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4803', '10000001', 'text', '从图上看出，近两个月EIA原油库存是成下降趋势的', null, null, '2016-07-07 15:57:57', '2016-07-07 15:57:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4804', '10000001', 'text', '上周的EIA原油库存更是减少405.3万桶大幅低于预期值236.5万桶，同时库存连续6周下滑', null, null, '2016-07-07 16:03:57', '2016-07-07 16:03:57', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('4805', '10000001', 'text', '上周的EIA原油库存更是减少405.3万桶大幅低于预期值236.5万桶，同时库存连续6周下滑', null, null, '2016-07-07 16:04:22', '2016-07-07 16:04:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4806', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e0dcb10a6e.png', null, null, '2016-07-07 16:07:39', '2016-07-07 16:07:39', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4807', '10000001', 'text', '短期压力位，获利先出', null, null, '2016-07-07 16:08:05', '2016-07-07 16:08:05', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4808', '10000001', 'text', '短线多单，有获利就先出喔#9', null, null, '2016-07-07 16:09:30', '2016-07-07 16:09:30', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4809', '10000001', 'text', '同时昨晚的API大幅低于预期', null, null, '2016-07-07 16:18:55', '2016-07-07 16:18:55', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('4810', '10000001', 'text', '同时昨晚的API大幅低于预期', null, null, '2016-07-07 16:19:18', '2016-07-07 16:19:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4811', '10000001', 'text', '录得近13个月的最低', null, null, '2016-07-07 16:20:09', '2016-07-07 16:20:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4812', '10000001', 'text', '短线多单，有获利先出逃', null, null, '2016-07-07 16:23:47', '2016-07-07 16:23:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4813', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e13a185a94.png', null, null, '2016-07-07 16:32:33', '2016-07-07 16:32:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4814', '10000001', 'text', '短期压力位，获利先出#9', null, null, '2016-07-07 16:33:01', '2016-07-07 16:33:01', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4815', '10000001', 'text', '获利先出#9', null, null, '2016-07-07 16:34:09', '2016-07-07 16:34:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4816', '10000001', 'text', '统计分析API公布的数据与当天晚上EIA数据相仿', null, null, '2016-07-07 16:45:36', '2016-07-07 16:45:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4817', '10000001', 'text', '相似度达到73.3%', null, null, '2016-07-07 16:51:00', '2016-07-07 16:51:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4818', '10000001', 'text', '所以当日API库存录得是利空油价，当天晚上的EIA库存利空油价的可能性非常高', null, null, '2016-07-07 16:58:40', '2016-07-07 16:58:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4819', '10000103', 'text', '#6下午好#6', null, null, '2016-07-07 17:08:00', '2016-07-07 17:08:00', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4820', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e1c5715794.jpg', null, null, '2016-07-07 17:09:43', '2016-07-07 17:09:43', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4821', '10000103', 'text', '昨天我们《轩湛•高德直播室》又再次大获全胜，超神多单完美止盈，获利78点，收益率高达62.5%', null, null, '2016-07-07 17:10:14', '2016-07-07 17:10:14', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4822', '10000103', 'text', '#14#14', null, null, '2016-07-07 17:10:22', '2016-07-07 17:10:22', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4823', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e1d17c9fe8.png', null, null, '2016-07-07 17:12:55', '2016-07-07 17:12:55', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4824', '10000103', 'text', '跟上我们老师脚步的家人都已经盈利，恭喜谢先生，一天的收益达到了24万#3#3加入我们的老师团队，盈利就是如此的简单#20#20', null, null, '2016-07-07 17:14:16', '2016-07-07 17:14:16', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4825', '10000103', 'text', '今晚还有EIA，更有我们轩湛“最红EIA”，让资金**动起来，别再犹豫，因为犹豫永远都只是看到别人赚钱！！！#21#21\n', null, null, '2016-07-07 17:16:23', '2016-07-07 17:16:23', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4826', '10000103', 'text', '还有好消息噢#4', null, null, '2016-07-07 17:18:20', '2016-07-07 17:18:20', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4827', '10000273', 'text', '大家好，我刚来，请多关照', '10000006', '是啊', '2016-07-07 17:26:46', '2016-07-07 17:26:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4828', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e21fd49ded.png', null, null, '2016-07-07 17:33:49', '2016-07-07 17:33:49', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4829', '10000103', 'text', '恭喜家人李大哥昨天盈利近10万，超级行情前的波动就有不小的收益，今晚EIA翻倍机遇，收益更难想象！“最红EIA”账户翻红果然是名不虚传\n#20#20', null, null, '2016-07-07 17:34:10', '2016-07-07 17:34:10', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4830', '10000103', 'text', '你好#9#9#9有什么问题，可以问我们直播室的老师噢#6', '10000273', '大家好，我刚来，请多关照', '2016-07-07 17:36:34', '2016-07-07 17:36:34', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4831', '10000103', 'text', '最红EIA正在进行时，看到机会的家人已经蜂拥申请了，”盈利你拿走，亏损我买单”，名额有限，时不待人！', null, null, '2016-07-07 17:37:26', '2016-07-07 17:37:26', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4832', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e22e883a04.jpg', null, null, '2016-07-07 17:37:44', '2016-07-07 17:37:44', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4833', '10000000', 'text', '欢迎！', '10000273', '大家好，我刚来，请多关照', '2016-07-07 17:38:06', '2016-07-07 17:38:06', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('4834', '10000102', 'text', '郑老师，好久不见', null, null, '2016-07-07 17:38:30', '2016-07-07 17:38:30', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4835', '10000194', 'text', '老师好#9', null, null, '2016-07-07 17:39:40', '2016-07-07 17:39:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4836', '10000000', 'text', '好久不见！欢迎参加今晚轩湛投资直播室的EIA狂欢之夜！#4', '10000102', '郑老师，好久不见', '2016-07-07 17:39:52', '2016-07-07 17:39:52', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('4837', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e237c582da.jpg', null, null, '2016-07-07 17:40:12', '2016-07-07 17:40:12', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4838', '10000103', 'text', '高达84.81%的胜算率伴随0风险的投资盛宴，要想改变口袋，先要改变赚钱思路，最红EIA，郑神亲自坐镇轩湛•高德直播室指点江山，你来吗？\n', null, null, '2016-07-07 17:40:33', '2016-07-07 17:40:33', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4839', '10000102', 'text', '好的，今晚的行情很大，但是有点害怕#7', null, null, '2016-07-07 17:40:47', '2016-07-07 17:40:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4840', '10000103', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e23b6590f6.jpg', null, null, '2016-07-07 17:41:10', '2016-07-07 17:41:10', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4841', '10000103', 'text', '距离EIA公布只剩下6个小时不到了，内部家人做好数钱数到手软的准备，还没有账户的家人，现在联系开户还来得及噢#15', null, null, '2016-07-07 17:41:56', '2016-07-07 17:41:56', '1', '#0f7dde', '20');
INSERT INTO `chat_chat` VALUES ('4842', '10000273', 'text', '老师好，今天刚进来，现在啥行情', null, null, '2016-07-07 17:51:19', '2016-07-07 17:51:19', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4843', '10000001', 'text', '目前油价走到短期压力区间324-325附近，同时19:30将公布欧洲央行利率会议纪要，届时预测将引起较大的风险情绪', '10000273', '老师好，今天刚进来，现在啥行情', '2016-07-07 17:53:54', '2016-07-07 17:53:54', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4844', '8', 'text', '大家好，各位老师好！#9', null, null, '2016-07-07 17:55:01', '2016-07-07 17:55:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4845', '10000194', 'text', '要等七点半数据出来才做单吗', null, null, '2016-07-07 17:56:55', '2016-07-07 17:56:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4846', '10000001', 'text', '接下来，由我们的伍老师为大家直播行情#9#14', null, null, '2016-07-07 17:56:55', '2016-07-07 17:56:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4847', '10000013', 'text', '老师现在可以做空吗？', null, null, '2016-07-07 17:58:33', '2016-07-07 17:58:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4848', '10000001', 'text', '我们的郑老师已经在喊单栏给出建议了#9', '10000194', '要等七点半数据出来才做单吗', '2016-07-07 17:59:52', '2016-07-07 17:59:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4849', '10000001', 'text', '在喊单栏，我们的郑老师已经给出建议了#9', '10000013', '老师现在可以做空吗？', '2016-07-07 18:00:41', '2016-07-07 18:00:41', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('4850', '10000194', 'text', '#9嗯', null, null, '2016-07-07 18:01:02', '2016-07-07 18:01:02', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4851', '8', 'text', '今晚又是EIA狂欢之夜，直播室好热闹！#4', null, null, '2016-07-07 18:03:27', '2016-07-07 18:03:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4852', '8', 'text', '新进来的朋友多关注下直播室左上方的喊单栏，可以做为适当参考。', null, null, '2016-07-07 18:07:24', '2016-07-07 18:07:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4853', '10000194', 'text', '#14#14#14', null, null, '2016-07-07 18:08:30', '2016-07-07 18:08:30', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4854', '8', 'text', '#20', '10000194', '#14#14#14', '2016-07-07 18:09:47', '2016-07-07 18:09:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4855', '10000194', 'text', '躺着等收钱#4#4#4', null, null, '2016-07-07 18:10:55', '2016-07-07 18:10:55', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4856', '8', 'text', '赚钱可不能躺着，手要够快才行#4', '10000194', '躺着等收钱#4#4#4', '2016-07-07 18:15:19', '2016-07-07 18:15:19', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4857', '8', 'text', '昨晚大家有没有看4点30的API数据的？', null, null, '2016-07-07 18:16:26', '2016-07-07 18:16:26', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4858', '10000194', 'text', '#2没有', null, null, '2016-07-07 18:18:45', '2016-07-07 18:18:45', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4859', '10000187', 'text', '老师，数据是利多的#4', null, null, '2016-07-07 18:19:07', '2016-07-07 18:19:07', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4860', '8', 'text', '库存大幅下降，远远超过预测值。', null, null, '2016-07-07 18:19:23', '2016-07-07 18:19:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4861', '10000187', 'text', 'EIA和API有关系吧？', null, null, '2016-07-07 18:20:18', '2016-07-07 18:20:18', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4862', '8', 'text', '所以，消息往往出人意料。', null, null, '2016-07-07 18:20:21', '2016-07-07 18:20:21', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4863', '8', 'text', '#7', '10000194', '#2没有', '2016-07-07 18:20:40', '2016-07-07 18:20:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4864', '8', 'text', '是啊，所以，我昨晚都是建议数据出来后操作，这样顺势而为才够稳健嘛#4', '10000187', '老师，数据是利多的#4', '2016-07-07 18:21:47', '2016-07-07 18:21:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4865', '8', 'text', '是有一定关联的，根据历史数据的统计，同期EIA和API同向的概率达80%以上。但也不能忽略意外情况。', '10000187', 'EIA和API有关系吧？', '2016-07-07 18:23:57', '2016-07-07 18:23:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4866', '10000187', 'text', '希望今晚给我大大的惊喜#4我讨厌惊吓', null, null, '2016-07-07 18:24:29', '2016-07-07 18:24:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4867', '8', 'text', '周三公布数据显示，美国7月1日当周API原油库存大幅减少673.6万桶，汽油库存减少360.3万桶，精炼油库存减少230.5万桶。', null, null, '2016-07-07 18:25:37', '2016-07-07 18:25:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4868', '8', 'text', '祝你好运#9', '10000187', '希望今晚给我大大的惊喜#4我讨厌惊吓', '2016-07-07 18:26:45', '2016-07-07 18:26:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4869', '8', 'text', '不过，库欣地区原油库存却增加了80万桶，值得注意。', null, null, '2016-07-07 18:28:15', '2016-07-07 18:28:15', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4870', '10000187', 'text', '借你吉言#9', null, null, '2016-07-07 18:28:46', '2016-07-07 18:28:46', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4871', '8', 'text', '#17', '10000187', '借你吉言#9', '2016-07-07 18:31:13', '2016-07-07 18:31:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4872', '8', 'text', 'API原油库存骤降支撑油价，隔夜原油均收涨逾2%。', null, null, '2016-07-07 18:32:14', '2016-07-07 18:32:14', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4873', '8', 'text', '而API原油库存骤降，市场预期晚间公布的美国EIA原油及成品油库存也将大幅减少。', null, null, '2016-07-07 18:34:41', '2016-07-07 18:34:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4874', '10000102', 'text', '今晚儿还会大涨吗？', null, null, '2016-07-07 18:34:59', '2016-07-07 18:34:59', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4875', '8', 'text', '看公布数据是否给力，如果再次录得大幅下降，不大涨都难。', '10000102', '今晚儿还会大涨吗？', '2016-07-07 18:36:25', '2016-07-07 18:36:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4876', '10000102', 'text', '突破330有可能吧', null, null, '2016-07-07 18:37:04', '2016-07-07 18:37:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4877', '8', 'text', '如果公布值在-600以下，破330不会很难。', '10000102', '突破330有可能吧', '2016-07-07 18:39:12', '2016-07-07 18:39:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4878', '8', 'text', '但花旗期货分析师也称，美国EIA原油库存大降并非不可能，但这种情况并不常见。', null, null, '2016-07-07 18:41:14', '2016-07-07 18:41:14', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4879', '8', 'text', '所以，意外情况大家也要多加考虑，毕竟人生充满意外#4', null, null, '2016-07-07 18:44:35', '2016-07-07 18:44:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4880', '8', 'text', '今晚的重要事件和数据还真比较多，相信行情波幅不会让大家失望。', null, null, '2016-07-07 18:47:55', '2016-07-07 18:47:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4881', '8', 'text', '19:30欧洲央行公布6月货币政策会议纪要。', null, null, '2016-07-07 18:54:17', '2016-07-07 18:54:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4882', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e354922f5b.jpg', null, null, '2016-07-07 18:56:09', '2016-07-07 18:56:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4883', '8', 'text', '面对英国公投的沉重压力，欧洲央行或将推出更多宽松措施，以减轻英国脱欧带来的打击。', null, null, '2016-07-07 18:59:56', '2016-07-07 18:59:56', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4884', '8', 'text', '美银美林分析师预计，欧洲央行将提出刺激行动建议，并预计决策者们会在7月21的货币政策会议上强烈暗示将在9月采取行动。', null, null, '2016-07-07 19:02:28', '2016-07-07 19:02:28', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4885', '10000013', 'text', '老师现在是不是跌不下去了', null, null, '2016-07-07 19:08:39', '2016-07-07 19:08:39', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4886', '10000102', 'text', '老师，这个不是上周公布吗？为什么要推迟到这周？', null, null, '2016-07-07 19:10:58', '2016-07-07 19:10:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4887', '8', 'text', '包括花旗、摩根大通和德意志银行——认为，受英国脱欧的影响，欧洲央行在7月的货币政策会议上很可能会表现更加鸽派。', null, null, '2016-07-07 19:14:40', '2016-07-07 19:14:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4888', '8', 'text', '目前来看，下跌比较无力。', '10000013', '老师现在是不是跌不下去了', '2016-07-07 19:15:54', '2016-07-07 19:15:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4889', '8', 'text', '你说的是哪个？', '10000102', '老师，这个不是上周公布吗？为什么要推迟到这周？', '2016-07-07 19:16:28', '2016-07-07 19:16:28', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4890', '10000102', 'text', '欧央行货币政策会议纪要', null, null, '2016-07-07 19:17:09', '2016-07-07 19:17:09', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4891', '8', 'text', '没说为什么，咱们就按实际时间为准。', '10000102', '欧央行货币政策会议纪要', '2016-07-07 19:24:51', '2016-07-07 19:24:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4892', '8', 'text', '已经快19:30了，大家有时间都关注一下。', null, null, '2016-07-07 19:25:40', '2016-07-07 19:25:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4893', '8', 'text', '美国6月挑战者企业裁员人数也是在19:30', null, null, '2016-07-07 19:28:47', '2016-07-07 19:28:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4894', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e3d56922f0.jpg', null, null, '2016-07-07 19:30:30', '2016-07-07 19:30:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4895', '8', 'text', '利多', null, null, '2016-07-07 19:30:40', '2016-07-07 19:30:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4896', '8', 'text', '先看会议纪要', null, null, '2016-07-07 19:31:01', '2016-07-07 19:31:01', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4897', '8', 'text', '欧洲央行会议纪要：负利率溢出效应在英国退欧后蔓延至欧元区。', null, null, '2016-07-07 19:31:48', '2016-07-07 19:31:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4898', '8', 'text', '欧洲央行会议纪要：认为在英国公投退欧前欧元区经济复苏稳定、温和。', null, null, '2016-07-07 19:39:06', '2016-07-07 19:39:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4899', '8', 'text', '欧洲央行会议纪要：尽管欧元区经济风险较此前已更为均衡，但仍然偏向下档。', null, null, '2016-07-07 19:39:27', '2016-07-07 19:39:27', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4900', '8', 'text', '原油短线小幅上行', null, null, '2016-07-07 19:44:34', '2016-07-07 19:44:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4901', '8', 'text', '前行情不明朗，建议稳健操作#9', null, null, '2016-07-07 19:52:24', '2016-07-07 19:52:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4902', '8', 'text', '整体欧央行货币政策会议纪要表现更鸽派，理论上利空欧元和原油。', null, null, '2016-07-07 19:54:43', '2016-07-07 19:54:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4903', '10000199', 'text', '#9', null, null, '2016-07-07 19:57:24', '2016-07-07 19:57:24', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4904', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e442cbf7a5.jpg', null, null, '2016-07-07 19:59:40', '2016-07-07 19:59:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4905', '8', 'text', '高位震荡，对多头不太有利', null, null, '2016-07-07 20:01:14', '2016-07-07 20:01:14', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4906', '8', 'text', '#17', '10000199', '#9', '2016-07-07 20:01:42', '2016-07-07 20:01:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4907', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e44f696255.jpg', null, null, '2016-07-07 20:03:02', '2016-07-07 20:03:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4908', '8', 'text', '20:15公布美国6月ADP就业人数', null, null, '2016-07-07 20:03:33', '2016-07-07 20:03:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4909', '8', 'text', '就是号称小非农的这个数据', null, null, '2016-07-07 20:04:36', '2016-07-07 20:04:36', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4910', '8', 'text', '就业数据是消费者支出的领先指标，占总体经济活动中的大部分。', null, null, '2016-07-07 20:06:35', '2016-07-07 20:06:35', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4911', '8', 'text', '它比劳工部公布的官方非农就业数据提前两天公布，被看作是非农数据的先行指标。', null, null, '2016-07-07 20:08:10', '2016-07-07 20:08:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4912', '8', 'text', '公布值若是大于预期值，则利好美国经济及美元，利空原油。', null, null, '2016-07-07 20:10:39', '2016-07-07 20:10:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4913', '10000084', 'text', '#9', null, null, '2016-07-07 20:11:48', '2016-07-07 20:11:48', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4914', '8', 'text', '#9', '10000084', '#9', '2016-07-07 20:12:49', '2016-07-07 20:12:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4915', '10000170', 'text', '整体欧央行货币政策会议纪要表现更鸽派，理论上利空欧元和原油，老师，欧央行和美联储是反的吗？美联储偏鸽派不是利多原油吗？', null, null, '2016-07-07 20:12:58', '2016-07-07 20:12:58', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4916', '8', 'text', '是的，美联储偏鸽派则会利空美元，利多原油。', '10000170', '整体欧央行货币政策会议纪要表现更鸽派，理论上利空欧元和原油，老师，欧央行和美联储是反的吗？美联储偏鸽派不是利多原油吗？', '2016-07-07 20:14:10', '2016-07-07 20:14:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4917', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e47e9ee9ee.jpg', null, null, '2016-07-07 20:15:38', '2016-07-07 20:15:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4918', '8', 'text', '美国6月ADP就业人数数据利空原油', null, null, '2016-07-07 20:16:00', '2016-07-07 20:16:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4919', '10000170', 'text', '老师，数值差距不是太大，影响会大吗？', null, null, '2016-07-07 20:17:27', '2016-07-07 20:17:27', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4920', '8', 'text', '价格没怎么动#12', null, null, '2016-07-07 20:17:55', '2016-07-07 20:17:55', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4921', '10000194', 'text', '利空原油', null, null, '2016-07-07 20:18:01', '2016-07-07 20:18:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4922', '8', 'text', '不太', '10000170', '老师，数值差距不是太大，影响会大吗？', '2016-07-07 20:18:16', '2016-07-07 20:18:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4923', '8', 'text', '#20', '10000194', '利空原油', '2016-07-07 20:18:24', '2016-07-07 20:18:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4924', '10000170', 'text', '等下就是初请了，不过个人觉得还是得看今晚的EIA', null, null, '2016-07-07 20:20:01', '2016-07-07 20:20:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4925', '8', 'text', '这个位置比较关键，多空胶着。', null, null, '2016-07-07 20:21:25', '2016-07-07 20:21:25', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4926', '8', 'text', '是的，初请对原油的影响不如EIA来得直接。#20', '10000170', '等下就是初请了，不过个人觉得还是得看今晚的EIA', '2016-07-07 20:22:39', '2016-07-07 20:22:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4927', '10000194', 'text', 'EIA说明一切', null, null, '2016-07-07 20:24:15', '2016-07-07 20:24:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4928', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e4a0947f0b.jpg', null, null, '2016-07-07 20:24:41', '2016-07-07 20:24:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4929', '8', 'text', '接下来的数据是初请了，不过若是相差不太大，预计影响也不大。', null, null, '2016-07-07 20:25:51', '2016-07-07 20:25:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4930', '8', 'text', '#20', '10000194', 'EIA说明一切', '2016-07-07 20:25:58', '2016-07-07 20:25:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4931', '8', 'text', '初请预测值和前值就相差2000人', null, null, '2016-07-07 20:28:38', '2016-07-07 20:28:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4932', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e4b6e22a1b.jpg', null, null, '2016-07-07 20:30:38', '2016-07-07 20:30:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4933', '8', 'text', '依然是利空', null, null, '2016-07-07 20:30:50', '2016-07-07 20:30:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4934', '8', 'text', '美国一周初请失业保险人数下降至4月中以来最低。', null, null, '2016-07-07 20:31:37', '2016-07-07 20:31:37', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4935', '10000170', 'text', '今晚两个数据差距都不大，接下来坐等EIA#4', null, null, '2016-07-07 20:37:03', '2016-07-07 20:37:03', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4936', '8', 'text', '初请也没有驱动行情走势，看来市场对晚间的EIA期待值很强。', null, null, '2016-07-07 20:38:11', '2016-07-07 20:38:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4937', '8', 'text', '期待', '10000170', '今晚两个数据差距都不大，接下来坐等EIA#4', '2016-07-07 20:39:14', '2016-07-07 20:39:14', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4938', '10000194', 'text', '现在多单的压力位在那里', null, null, '2016-07-07 20:39:52', '2016-07-07 20:39:52', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4939', '10000194', 'text', '有多单怎么办', null, null, '2016-07-07 20:40:37', '2016-07-07 20:40:37', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4940', '8', 'text', '上方强压在330', '10000194', '现在多单的压力位在那里', '2016-07-07 20:43:00', '2016-07-07 20:43:00', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4941', '8', 'text', '你什么位置的多单', '10000194', '有多单怎么办', '2016-07-07 20:43:18', '2016-07-07 20:43:18', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4942', '8', 'text', '【路透评美国当周初请失业金人数】本次美国当周初请人数意外下滑，为美国就业市场仍然保持稳健提供进一步证据；初请失业金人数仍然维持在43年低点附近，且连续第70周低于30万人大关，持续时间为1973年以来最长；此外，美国劳工部分析师还表示本轮数据并未受到特别因素影响。', null, null, '2016-07-07 20:44:44', '2016-07-07 20:44:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4943', '10000273', 'text', '老师，我刚来，空仓，有做单吗', null, null, '2016-07-07 20:45:17', '2016-07-07 20:45:17', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4944', '10000194', 'text', '323.7多单', null, null, '2016-07-07 20:45:29', '2016-07-07 20:45:29', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4945', '10000013', 'text', '现在是做什么单啊老师', null, null, '2016-07-07 20:46:15', '2016-07-07 20:46:15', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4946', '10000006', 'text', '老师 今晚EIA几点公布呢', null, null, '2016-07-07 20:46:47', '2016-07-07 20:46:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4947', '8', 'text', '目前多空胶着，建议观望等待进一步信号。', '10000273', '老师，我刚来，空仓，有做单吗', '2016-07-07 20:46:52', '2016-07-07 20:46:52', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4948', '8', 'text', '目前多空胶着，建议观望等待进一步信号。', '10000013', '现在是做什么单啊老师', '2016-07-07 20:47:12', '2016-07-07 20:47:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4949', '8', 'text', '现在行情还不明朗，可以先拿着，若是跌破320则先出或减仓为宜。', '10000194', '323.7多单', '2016-07-07 20:48:46', '2016-07-07 20:48:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4950', '8', 'text', 'EIA是今晚23:00点公布', '10000006', '老师 今晚EIA几点公布呢', '2016-07-07 20:49:39', '2016-07-07 20:49:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4951', '10000194', 'text', '好的', null, null, '2016-07-07 20:51:11', '2016-07-07 20:51:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4952', '8', 'text', '美国6月ADP超预期增加17.2万人，或预示周五非农“惊艳”，美联储加不加息？明日非农将带来较为清晰的预判。', null, null, '2016-07-07 20:51:44', '2016-07-07 20:51:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4953', '8', 'text', '美国联邦基金期货利率显示交易员预期美联储7月保持利率不变的概率为98%。', null, null, '2016-07-07 20:53:39', '2016-07-07 20:53:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4954', '8', 'text', '#9', '10000194', '好的', '2016-07-07 20:53:46', '2016-07-07 20:53:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4955', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e5234378f3.jpg', null, null, '2016-07-07 20:59:32', '2016-07-07 20:59:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4956', '8', 'text', '关注上方324压力位', null, null, '2016-07-07 21:00:09', '2016-07-07 21:00:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4957', '8', 'text', '动了，又开始涨起', null, null, '2016-07-07 21:09:58', '2016-07-07 21:09:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4958', '8', 'text', '今晨公布的美国API原油及汽油库存均录得大幅下滑，提振美原油价格从数月低点反弹，汽油价格也应声大涨2%', null, null, '2016-07-07 21:16:06', '2016-07-07 21:16:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4959', '8', 'text', '鉴于本次API数据的重大利好，预计晚间EIA报告也将为油价提供些许支撑。', null, null, '2016-07-07 21:16:59', '2016-07-07 21:16:59', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4960', '8', 'text', '不过，尽管油价上涨，但经济放缓及成品油供应过剩，令油市承压。', null, null, '2016-07-07 21:24:12', '2016-07-07 21:24:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4961', '10000013', 'text', '老师我做了空单，现在行情为什么涨起来了？', null, null, '2016-07-07 21:25:28', '2016-07-07 21:25:28', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4962', '10000102', 'text', '可以进空单吗', null, null, '2016-07-07 21:27:56', '2016-07-07 21:27:56', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4963', '10000102', 'text', '今天上涨趋势貌似不太明显', null, null, '2016-07-07 21:28:10', '2016-07-07 21:28:10', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4964', '8', 'text', '早上的API数据重大利好，令市场对晚间的EIA数据继续利好的期待所致', '10000013', '老师我做了空单，现在行情为什么涨起来了？', '2016-07-07 21:28:53', '2016-07-07 21:28:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4965', '8', 'text', '可以，带好止盈止损#9', '10000102', '可以进空单吗', '2016-07-07 21:30:23', '2016-07-07 21:30:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4966', '8', 'text', '关键看EIA', '10000102', '今天上涨趋势貌似不太明显', '2016-07-07 21:32:07', '2016-07-07 21:32:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4967', '8', 'text', '目前，行情并未有效突破324', null, null, '2016-07-07 21:34:07', '2016-07-07 21:34:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4968', '8', 'text', 'EIA数据之前估计是以震荡为主', null, null, '2016-07-07 21:42:13', '2016-07-07 21:42:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4969', '10000013', 'text', '那初请失业金和小非农不是利空的吗', null, null, '2016-07-07 21:46:17', '2016-07-07 21:46:17', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4970', '8', 'text', '又开始上涨了，看来市场继续上行的欲望很强烈。', null, null, '2016-07-07 21:49:58', '2016-07-07 21:49:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4971', '8', 'text', '由于昨晚的API数据大利多，导致了目前原油市场的看多预期。', null, null, '2016-07-07 21:51:22', '2016-07-07 21:51:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4972', '8', 'text', '昨晚的API数据大利多，掩盖了美元的影响。', '10000013', '那初请失业金和小非农不是利空的吗', '2016-07-07 21:53:07', '2016-07-07 21:53:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4973', '8', 'text', '短线出现冲高回落', null, null, '2016-07-07 21:58:28', '2016-07-07 21:58:28', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4974', '8', 'text', '震荡行情，短线指标都会暂时失灵，慎重操作。#9', null, null, '2016-07-07 22:04:54', '2016-07-07 22:04:54', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4975', '8', 'text', '震荡行情，短线指标都会暂时失灵，慎重操作。#9', null, null, '2016-07-07 22:05:05', '2016-07-07 22:05:05', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4976', '8', 'text', '手上有单的就拿着，严格执行自己的交易计划。', null, null, '2016-07-07 22:06:33', '2016-07-07 22:06:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4977', '8', 'text', '没有单子的朋友不要着急，EIA数据怕没有机会吗', null, null, '2016-07-07 22:07:07', '2016-07-07 22:07:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4978', '8', 'text', 'EIA之夜可能关乎近期油价命运', null, null, '2016-07-07 22:19:02', '2016-07-07 22:19:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4979', '8', 'text', '今天国际油价延续回升势头，但有一个隐患不容忽视', null, null, '2016-07-07 22:19:51', '2016-07-07 22:19:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4980', '8', 'text', '美国汽油需求虽创纪录，但汽油库存仍居高不下', null, null, '2016-07-07 22:20:09', '2016-07-07 22:20:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4981', '10000197', 'text', '老师：预判一下涨跌', null, null, '2016-07-07 22:26:57', '2016-07-07 22:26:57', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4982', '8', 'text', '汽油期货价格走势反映出，一旦9月时汽油需求高峰结束，预期到时供给过剩的情况更为严峻。', null, null, '2016-07-07 22:29:13', '2016-07-07 22:29:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4983', '8', 'text', '老师控制不了EIA数据#15', '10000197', '老师：预判一下涨跌', '2016-07-07 22:30:38', '2016-07-07 22:30:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4984', '10000000', 'text', '大家晚上好！伍老师晚上好！', null, null, '2016-07-07 22:31:01', '2016-07-07 22:31:01', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('4985', '10000000', 'text', '还有半个小时，就要公布今晚的EIA数据了', null, null, '2016-07-07 22:31:23', '2016-07-07 22:31:23', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('4986', '10000000', 'text', '\n今晨公布的美国API原油及汽油库存均录得大幅下滑，提振美原油价格从数月低点反弹，汽油价格也应声大涨2%；', null, null, '2016-07-07 22:31:45', '2016-07-07 22:31:45', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('4987', '8', 'text', '郑老师好！#9', '10000197', '老师：预判一下涨跌', '2016-07-07 22:31:48', '2016-07-07 22:31:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4988', '10000000', 'text', '要知道上周数据曾显示正值夏季美国用油高峰期，汽油库存却不降反升，导致汽油价格大幅下挫；而鉴于本次API数据的重大利好，预计晚间EIA报告也将为油价提供些许支撑。', null, null, '2016-07-07 22:32:01', '2016-07-07 22:32:01', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('4989', '10000000', 'text', '由于昨晚的API数据大利多，创出今年最低，导致了目前原油市场的看多预期。所以今晚的EIA我们就今年最低以-400作为分水岭。', null, null, '2016-07-07 22:32:54', '2016-07-07 22:32:54', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('4990', '10000000', 'text', '数据低于-400的话（例如-500），那么就短线追多，抓两三块钱就走；如果介于-225~-400之间，那么就等小幅拉升后逼近328的压力做空；如果高于-225，那么就直接追空操作', null, null, '2016-07-07 22:33:19', '2016-07-07 22:33:19', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('4991', '10000000', 'text', '目前原油依然短期在小区间320~330震荡。大区间是307~340', null, null, '2016-07-07 22:34:04', '2016-07-07 22:34:04', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('4992', '10000000', 'text', '下面我们先来等待11点的数据', null, null, '2016-07-07 22:44:21', '2016-07-07 22:44:21', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('4993', '10000064', 'text', '郑老师，本次EIA预期值多少', null, null, '2016-07-07 22:47:27', '2016-07-07 22:47:27', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('4994', '10000000', 'text', '-225', '10000064', '郑老师，本次EIA预期值多少', '2016-07-07 22:50:13', '2016-07-07 22:50:13', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('4995', '10000000', 'text', '还有五分钟数据就要公布了', null, null, '2016-07-07 22:56:10', '2016-07-07 22:56:10', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('4996', '10000000', 'text', '朋友们准备好你们的子弹！#4', null, null, '2016-07-07 22:56:30', '2016-07-07 22:56:30', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('4997', '8', 'text', 'EIA倒计时', null, null, '2016-07-07 22:58:50', '2016-07-07 22:58:50', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4998', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e6e8f3cd54.jpg', null, null, '2016-07-07 23:00:31', '2016-07-07 23:00:31', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('4999', '8', 'text', '数据利空', null, null, '2016-07-07 23:00:41', '2016-07-07 23:00:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5000', '8', 'text', '做空', null, null, '2016-07-07 23:00:51', '2016-07-07 23:00:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5001', '8', 'text', '极速下跌', null, null, '2016-07-07 23:02:20', '2016-07-07 23:02:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5002', '8', 'text', '美国国内原油产量连续四周下滑，同时也是第13周维持在900万桶/日关口下方。', null, null, '2016-07-07 23:03:10', '2016-07-07 23:03:10', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5003', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e6f78d58d9.jpg', null, null, '2016-07-07 23:04:24', '2016-07-07 23:04:24', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5004', '8', 'text', '壮观！#20', null, null, '2016-07-07 23:04:40', '2016-07-07 23:04:40', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5005', '10000194', 'text', '一泻千里', null, null, '2016-07-07 23:04:51', '2016-07-07 23:04:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5006', '10000178', 'text', '现在还能买空单吗?', null, null, '2016-07-07 23:05:18', '2016-07-07 23:05:18', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5007', '8', 'text', '#14', '10000194', '一泻千里', '2016-07-07 23:06:16', '2016-07-07 23:06:16', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5008', '8', 'text', '可以，轻仓，带好止盈止损。', '10000178', '现在还能买空单吗?', '2016-07-07 23:06:48', '2016-07-07 23:06:48', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5009', '10000000', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e7025b5280.jpg', null, null, '2016-07-07 23:07:17', '2016-07-07 23:07:17', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('5010', '10000000', 'text', '今天已经提前告诉大家了，行情只要在-225下方，可以直接追空', null, null, '2016-07-07 23:09:07', '2016-07-07 23:09:07', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('5011', '10000170', 'text', '今天又是反逾期啊', null, null, '2016-07-07 23:11:23', '2016-07-07 23:11:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5012', '10000170', 'text', '老师，下方看到多少？', null, null, '2016-07-07 23:12:13', '2016-07-07 23:12:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5013', '8', 'text', '是啊 ，意外总是存在。', '10000170', '今天又是反逾期啊', '2016-07-07 23:15:53', '2016-07-07 23:15:53', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5014', '8', 'text', '下方310附近有支撑', '10000170', '老师，下方看到多少？', '2016-07-07 23:16:23', '2016-07-07 23:16:23', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5015', '10000273', 'text', '现在咋做啊', null, null, '2016-07-07 23:18:40', '2016-07-07 23:18:40', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5016', '8', 'text', '虽然美原油产量降幅创2013年9月以来最高，但因此前公布的API原油库存创下13周最大跌幅，导致市场对EIA数据大跌的预期升温，数据公布后降幅不仅远低于API数据，更是低于预期。', null, null, '2016-07-07 23:20:03', '2016-07-07 23:20:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5017', '8', 'text', '稳健可等回调后再布局空单', '10000273', '现在咋做啊', '2016-07-07 23:20:57', '2016-07-07 23:20:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5018', '8', 'text', '此外汽油库存也仅下降12.2万桶，受此影响，油价在数据公布后由涨转跌，跌幅扩大至约1%。', null, null, '2016-07-07 23:22:42', '2016-07-07 23:22:42', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5019', '8', 'text', '目前，跌幅已扩大至1.67%', null, null, '2016-07-07 23:30:22', '2016-07-07 23:30:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5020', '8', 'text', '持续下跌中#20', null, null, '2016-07-07 23:40:03', '2016-07-07 23:40:03', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5021', '10000194', 'text', '已经跑路了#9', '10000273', '现在咋做啊', '2016-07-07 23:44:21', '2016-07-07 23:44:21', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5022', '10000170', 'text', '老师，310的支撑强吗？', null, null, '2016-07-07 23:49:13', '2016-07-07 23:49:13', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5023', '8', 'text', '跌幅还在继续扩大，追单的朋友要慎重了，小心反弹。', null, null, '2016-07-07 23:51:07', '2016-07-07 23:51:07', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5024', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160707/577e7a8796523.jpeg', null, null, '2016-07-07 23:51:35', '2016-07-07 23:51:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5025', '10000273', 'text', '老师现在进多吗', null, null, '2016-07-07 23:52:23', '2016-07-07 23:52:23', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5026', '10000001', 'text', '支撑位310附近，轻仓短线多单（个人建议，仅供参考）', null, null, '2016-07-07 23:52:36', '2016-07-07 23:52:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5027', '8', 'text', '要看下跌力度', '10000170', '老师，310的支撑强吗？', '2016-07-07 23:53:02', '2016-07-07 23:53:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5028', '8', 'text', '直播室左上方有给出参考策略，适当参考。#9', '10000273', '老师现在进多吗', '2016-07-07 23:59:33', '2016-07-07 23:59:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5029', '8', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577e7c93f2c1b.jpg', null, null, '2016-07-08 00:00:20', '2016-07-08 00:00:20', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5030', '8', 'text', '短线博反弹，指引止损带好，个人建议，仅供参考。#9', null, null, '2016-07-08 00:00:33', '2016-07-08 00:00:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5031', '8', 'text', '310不知道能否支撑的住啊', null, null, '2016-07-08 00:07:49', '2016-07-08 00:07:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5032', '8', 'text', '真的势不可挡', null, null, '2016-07-08 00:10:12', '2016-07-08 00:10:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5033', '8', 'text', '308了', null, null, '2016-07-08 00:10:46', '2016-07-08 00:10:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5034', '10000037', 'text', '现在做多吗', null, null, '2016-07-08 00:11:33', '2016-07-08 00:11:33', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5035', '10000170', 'text', '老师，破了310看多少？', null, null, '2016-07-08 00:11:42', '2016-07-08 00:11:42', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5036', '10000170', 'text', '307差不多吧', null, null, '2016-07-08 00:11:50', '2016-07-08 00:11:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5037', '8', 'text', '下方支撑在305附近', '10000170', '307差不多吧', '2016-07-08 00:12:47', '2016-07-08 00:12:47', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5038', '10000037', 'text', '现在还要做多吗？老师', null, null, '2016-07-08 00:13:41', '2016-07-08 00:13:41', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5039', '10000170', 'text', '拿明天继续低开。不要到300了', null, null, '2016-07-08 00:14:11', '2016-07-08 00:14:11', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5040', '8', 'text', '公布值和市场预期相差太大，所以跌的也越猛', null, null, '2016-07-08 00:14:49', '2016-07-08 00:14:49', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5041', '8', 'text', '建议还是缓缓吧，空头力量太强了', '10000037', '现在还要做多吗？老师', '2016-07-08 00:15:34', '2016-07-08 00:15:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5042', '8', 'text', '按这种走法，真有这种可能', '10000170', '拿明天继续低开。不要到300了', '2016-07-08 00:15:58', '2016-07-08 00:15:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5043', '8', 'text', '没发现你的名字和普京有的一比了#4', '10000037', '现在还要做多吗？老师', '2016-07-08 00:16:38', '2016-07-08 00:16:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5044', '10000037', 'text', '哈哈', null, null, '2016-07-08 00:16:50', '2016-07-08 00:16:50', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5045', '10000037', 'text', '酋长明天让油价涨上去', null, null, '2016-07-08 00:17:32', '2016-07-08 00:17:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5046', '8', 'text', '你说要减产，明天马上涨#4', '10000037', '酋长明天让油价涨上去', '2016-07-08 00:21:13', '2016-07-08 00:21:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5047', '10000037', 'text', '还在往下走', null, null, '2016-07-08 00:25:49', '2016-07-08 00:25:49', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5048', '10000037', 'text', '今晚行情真夸张', null, null, '2016-07-08 00:26:01', '2016-07-08 00:26:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5049', '8', 'text', '跌幅3.23%了', '10000037', '今晚行情真夸张', '2016-07-08 00:27:06', '2016-07-08 00:27:06', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5050', '10000170', 'text', '空头没散', null, null, '2016-07-08 00:27:47', '2016-07-08 00:27:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5051', '10000170', 'text', '这样跌下去有可能下半夜要涨一点吧，已经跌了有15块钱了', null, null, '2016-07-08 00:28:51', '2016-07-08 00:28:51', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5052', '10000170', 'text', '老师，现在可以多单吗？', null, null, '2016-07-08 00:29:36', '2016-07-08 00:29:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5053', '8', 'text', '目前，跌势稍稍有所放缓', '10000170', '这样跌下去有可能下半夜要涨一点吧，已经跌了有15块钱了', '2016-07-08 00:29:43', '2016-07-08 00:29:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5054', '8', 'text', '可以，建议轻仓，有利润就跑。', '10000170', '老师，现在可以多单吗？', '2016-07-08 00:30:43', '2016-07-08 00:30:43', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5055', '8', 'text', '空头太强劲，就算反弹估计也不会很大。', null, null, '2016-07-08 00:31:30', '2016-07-08 00:31:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5056', '8', 'text', '大家手上要是没有单子的，建议还是做顺势单比较稳健。', null, null, '2016-07-08 00:36:29', '2016-07-08 00:36:29', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5057', '10000170', 'text', '顺势单？空单？我就是没有单子了，看能否搏反弹，进了多单，我觉得跌幅差不多了吧', null, null, '2016-07-08 00:38:25', '2016-07-08 00:38:25', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5058', '8', 'text', '你刚刚进了多单吗？', '10000170', '顺势单？空单？我就是没有单子了，看能否搏反弹，进了多单，我觉得跌幅差不多了吧', '2016-07-08 00:41:17', '2016-07-08 00:41:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5059', '10000170', 'text', '是的', null, null, '2016-07-08 00:43:10', '2016-07-08 00:43:10', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5060', '8', 'text', '反弹是肯定会有的，问题是不知道它能反弹上来多少，所以一般做反弹就是不要贪，有利润要懂得及时出手。', '10000170', '是的', '2016-07-08 00:45:51', '2016-07-08 00:45:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5061', '10000170', 'text', '了解！', null, null, '2016-07-08 00:46:21', '2016-07-08 00:46:21', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5062', '8', 'text', '#20', '10000170', '了解！', '2016-07-08 00:46:39', '2016-07-08 00:46:39', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5063', '8', 'text', '现在盘面整体都是偏弱势了。', null, null, '2016-07-08 00:53:33', '2016-07-08 00:53:33', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5064', '8', 'text', '又下跌到了低点附近，反弹无力。', null, null, '2016-07-08 01:01:51', '2016-07-08 01:01:51', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5065', '8', 'text', '短线持续下挫', null, null, '2016-07-08 01:05:38', '2016-07-08 01:05:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5066', '8', 'text', '最低去到304', null, null, '2016-07-08 01:06:17', '2016-07-08 01:06:17', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5067', '10000273', 'text', '多单310的咋办', null, null, '2016-07-08 01:08:41', '2016-07-08 01:08:41', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5068', '10000273', 'text', '要止损吗。下方看到300吗', null, null, '2016-07-08 01:09:26', '2016-07-08 01:09:26', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5069', '8', 'text', '锁仓一半', '10000273', '要止损吗。下方看到300吗', '2016-07-08 01:11:02', '2016-07-08 01:11:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5070', '8', 'text', '你之前没设止损，现在锁仓吧。', '10000273', '多单310的咋办', '2016-07-08 01:11:45', '2016-07-08 01:11:45', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5071', '8', 'text', '这行情实在太猛了', null, null, '2016-07-08 01:13:13', '2016-07-08 01:13:13', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5072', '8', 'text', '最强EIA啊', null, null, '2016-07-08 01:13:22', '2016-07-08 01:13:22', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5073', '8', 'text', '多单的朋友都先锁仓', null, null, '2016-07-08 01:15:02', '2016-07-08 01:15:02', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5074', '10000273', 'text', '下方支撑在多少', null, null, '2016-07-08 01:16:42', '2016-07-08 01:16:42', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5075', '8', 'text', '300是目前比较强的一个支撑。', '10000273', '下方支撑在多少', '2016-07-08 01:17:38', '2016-07-08 01:17:38', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5076', '8', 'text', '今天都下跌了4.58%了#12', null, null, '2016-07-08 01:20:11', '2016-07-08 01:20:11', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5077', '8', 'text', '这种EIA也真是不多见', null, null, '2016-07-08 01:20:44', '2016-07-08 01:20:44', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5078', '10000273', 'text', '304.3锁仓了', null, null, '2016-07-08 01:25:41', '2016-07-08 01:25:41', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5079', '10000273', 'text', '又反弹了', null, null, '2016-07-08 01:26:04', '2016-07-08 01:26:04', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5080', '8', 'text', '目前来看，300支撑还是很强的，估计也不是那么容易被破的。', null, null, '2016-07-08 01:26:57', '2016-07-08 01:26:57', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5081', '8', 'text', '反弹上去了，你的多单就要适当减仓了', '10000273', '又反弹了', '2016-07-08 01:28:09', '2016-07-08 01:28:09', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5082', '10000170', 'text', '不应该去搏这种反弹的，被套了啊#2', null, null, '2016-07-08 01:31:45', '2016-07-08 01:31:45', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5083', '8', 'text', '你之前也没设止损的吗？', '10000170', '不应该去搏这种反弹的，被套了啊#2', '2016-07-08 01:33:32', '2016-07-08 01:33:32', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5084', '10000170', 'text', '恩啊，我是308的多单', null, null, '2016-07-08 01:33:57', '2016-07-08 01:33:57', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5085', '8', 'text', '308可以暂时拿着，之前触底后，现在是在反弹。不过不要想着一定回本才走，反弹到一定位置就先走吧', '10000170', '恩啊，我是308的多单', '2016-07-08 01:36:14', '2016-07-08 01:36:14', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5086', '10000170', 'text', '行情反弹不上去吗？今晚都跌了20块钱了', null, null, '2016-07-08 01:37:30', '2016-07-08 01:37:30', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5087', '8', 'text', '现在306了，上方短期的压力在308附近，多单的朋友可在这个位置适当减仓或先出。', '10000170', '恩啊，我是308的多单', '2016-07-08 01:38:41', '2016-07-08 01:38:41', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5088', '8', 'text', '现在就在反弹，但是在这样的抛售行情中，不能保证它能反弹很多。', '10000170', '行情反弹不上去吗？今晚都跌了20块钱了', '2016-07-08 01:40:58', '2016-07-08 01:40:58', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5089', '8', 'text', '好了，今晚的直播到这里就结束了。', null, null, '2016-07-08 01:43:30', '2016-07-08 01:43:30', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5090', '8', 'text', '后市的走势，在强支撑304～305被有效突破之前，原油依然是维持304～330的大区间震荡。', null, null, '2016-07-08 01:43:46', '2016-07-08 01:43:46', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5091', '8', 'text', '关键看今天收盘情况。', null, null, '2016-07-08 01:44:12', '2016-07-08 01:44:12', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5092', '8', 'text', '大家晚安！明天见！#9', null, null, '2016-07-08 01:44:34', '2016-07-08 01:44:34', '1', '#000', '20');
INSERT INTO `chat_chat` VALUES ('5093', '5', 'text', '#7昨晚跌了20块，错过了！', null, null, '2016-07-08 09:28:36', '2016-07-08 09:28:36', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5094', '10000001', 'text', '大家早上好！#9', null, null, '2016-07-08 09:31:16', '2016-07-08 09:31:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5095', '5', 'text', ' 老师早1', null, null, '2016-07-08 09:36:01', '2016-07-08 09:36:01', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5096', '10000001', 'text', '没事，今晚还有非农，届时将会引起油价较大的波动#9', '5', '#7昨晚跌了20块，错过了！', '2016-07-08 09:36:21', '2016-07-08 09:36:21', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5097', '10000001', 'text', '热热的天，开心的你，幸福的日子里加一杯冰柠檬，消去夏日的暑气#9', null, null, '2016-07-08 09:43:46', '2016-07-08 09:43:46', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5098', '10000001', 'text', '我是轩湛投资观察室的吴勇坚，接下来我来为大家做直播，大家有什么疑问或者疑惑，可以在互动区跟我交流喔', null, null, '2016-07-08 09:43:53', '2016-07-08 09:43:53', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5099', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f0593742fd.png', null, null, '2016-07-08 09:44:51', '2016-07-08 09:44:51', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5100', '10000001', 'text', '昨晚的EIA数据利空，我们的这个郑老师布局的空单，顺利止盈了#9', null, null, '2016-07-08 09:46:19', '2016-07-08 09:46:19', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5101', '10000001', 'text', '收益空间54.34%#14', null, null, '2016-07-08 09:46:40', '2016-07-08 09:46:40', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5102', '10000001', 'text', '早啊#9', '5', ' 老师早1', '2016-07-08 09:47:20', '2016-07-08 09:47:20', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('5103', '10000001', 'text', '早啊#9', '5', ' 老师早1', '2016-07-08 09:48:27', '2016-07-08 09:48:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5104', '10000001', 'text', ' 昨晚的EIA数据利空，我们郑老师布局的空单，顺利止盈了#9#14', null, null, '2016-07-08 09:48:47', '2016-07-08 09:48:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5105', '10000001', 'text', '收益空间54.34%#14#14', null, null, '2016-07-08 09:49:16', '2016-07-08 09:49:16', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5106', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f06a3cbd99.png', null, null, '2016-07-08 09:49:23', '2016-07-08 09:49:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5107', '10000001', 'text', '昨日我们伍老#20#14师布局的中线空单，也顺利止盈了', null, null, '2016-07-08 09:51:38', '2016-07-08 09:51:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5108', '10000001', 'text', '收益空间29.63%#14', null, null, '2016-07-08 09:51:58', '2016-07-08 09:51:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5109', '10000001', 'text', '大家快跟紧我们老师哦#14#14', null, null, '2016-07-08 09:54:17', '2016-07-08 09:54:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5110', '10000001', 'text', '好了，接下来我们一起回顾一下昨天的行情', null, null, '2016-07-08 09:54:37', '2016-07-08 09:54:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5111', '10000006', 'text', '老师好', null, null, '2016-07-08 09:55:57', '2016-07-08 09:55:57', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5112', '10000001', 'text', '昨晚，欧洲央行6月货币会议纪要指出：欧元区经济风险较此前已更为均衡，但仍然偏向下档', null, null, '2016-07-08 09:59:26', '2016-07-08 09:59:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5113', '10000001', 'text', '使得油价小幅下落', null, null, '2016-07-08 10:00:29', '2016-07-08 10:00:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5114', '10000001', 'text', '但，欧洲央行认为刺激措施带来的影响比6月预期所暗示的更大', null, null, '2016-07-08 10:02:26', '2016-07-08 10:02:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5115', '10000001', 'text', '使得油价小幅下落后继续上探至压力位325附近', null, null, '2016-07-08 10:03:20', '2016-07-08 10:03:20', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5116', '10000001', 'text', '同时美国6月ADP就业人数录得增长高于预期和美国至7月2日当周初请失业金人数意外录得低于预期', null, null, '2016-07-08 10:06:08', '2016-07-08 10:06:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5117', '10000001', 'text', '表明美国就业市场依然强劲', null, null, '2016-07-08 10:06:26', '2016-07-08 10:06:26', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5118', '10000001', 'text', '早！#9', '10000006', '老师好', '2016-07-08 10:06:43', '2016-07-08 10:06:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5119', '10000001', 'text', '使得油价在压力位325附近震荡盘整，等待EIA原油库存数据', null, null, '2016-07-08 10:09:03', '2016-07-08 10:09:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5120', '10000001', 'text', 'EIA原油库存数据公布后', null, null, '2016-07-08 10:09:35', '2016-07-08 10:09:35', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5121', '10000001', 'text', '录得高于预期，利空油价', null, null, '2016-07-08 10:10:22', '2016-07-08 10:10:22', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5122', '10000001', 'text', '而且数据的降幅不仅远低于API数据的降幅，使得市场存在预期差', null, null, '2016-07-08 10:11:50', '2016-07-08 10:11:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5123', '10000001', 'text', '油价响声下跌', null, null, '2016-07-08 10:11:59', '2016-07-08 10:11:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5124', '10000001', 'text', '凌晨油价冲破支撑位310', null, null, '2016-07-08 10:12:56', '2016-07-08 10:12:56', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5125', '10000001', 'text', '加上欧洲央行6月货币会议纪要指出：欧元区经济仍然偏向下档，引起的风险情绪', null, null, '2016-07-08 10:14:59', '2016-07-08 10:14:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5126', '10000001', 'text', '后半夜油价继续下落到300支撑位附近', null, null, '2016-07-08 10:15:30', '2016-07-08 10:15:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5127', '10000001', 'text', '跌幅达5%', null, null, '2016-07-08 10:16:52', '2016-07-08 10:16:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5128', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f0d65790d9.png', null, null, '2016-07-08 10:18:13', '2016-07-08 10:18:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5129', '10000001', 'text', '好了，我们回到油价的走势上', null, null, '2016-07-08 10:19:00', '2016-07-08 10:19:00', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5130', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f0df4c8f3f.png', null, null, '2016-07-08 10:20:36', '2016-07-08 10:20:36', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5131', '10000274', 'text', '吴老师，你好！今天还是高空策略吗', null, null, '2016-07-08 10:21:17', '2016-07-08 10:21:17', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5132', '10000274', 'text', '上方压力310  ，破310看到哪', null, null, '2016-07-08 10:21:45', '2016-07-08 10:21:45', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5133', '10000001', 'text', '昨晚油价大跌5%多，那么预测白天的油价将会是小幅反弹震荡盘整', null, null, '2016-07-08 10:23:57', '2016-07-08 10:23:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5134', '10000001', 'text', '破310#9，上方短期压力位315，上方压力位320', '10000274', '上方压力310  ，破310看到哪', '2016-07-08 10:25:01', '2016-07-08 10:25:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5135', '10000274', 'text', '谢谢老师', null, null, '2016-07-08 10:25:32', '2016-07-08 10:25:32', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5136', '10000274', 'text', '现在可以做个短线回调啊', null, null, '2016-07-08 10:26:21', '2016-07-08 10:26:21', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5137', '10000001', 'text', '目前是保持高空策略为主的，但晚上20:30有非农数据', '10000274', '吴老师，你好！今天还是高空策略吗', '2016-07-08 10:27:45', '2016-07-08 10:27:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5138', '10000001', 'text', '可以，但要带好止损止盈，有利润就出哦（个人建议，仅供参考）', '10000274', '现在可以做个短线回调啊', '2016-07-08 10:29:09', '2016-07-08 10:29:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5139', '10000001', 'text', '好了，我们去关注一下，股市的情况', null, null, '2016-07-08 10:29:40', '2016-07-08 10:29:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5140', '10000001', 'text', '今日大盘开盘低开低走', null, null, '2016-07-08 10:34:19', '2016-07-08 10:34:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5141', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f11707134f.png', null, null, '2016-07-08 10:35:28', '2016-07-08 10:35:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5142', '10000001', 'text', '大盘五连阳之后，今日大盘有所回落盘整的', null, null, '2016-07-08 10:36:33', '2016-07-08 10:36:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5143', '10000274', 'text', '#9', null, null, '2016-07-08 10:39:47', '2016-07-08 10:39:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5144', '10000001', 'text', '近期强势板块主要为资源股和防御性板块，中小盘个股表现低迷', null, null, '2016-07-08 10:40:33', '2016-07-08 10:40:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5145', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f14cbc5579.jpg', null, null, '2016-07-08 10:49:47', '2016-07-08 10:49:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5146', '10000001', 'text', '盘面上，量子通信、次新股等板块涨幅靠前', null, null, '2016-07-08 10:58:23', '2016-07-08 10:58:23', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5147', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f1967bd120.png', null, null, '2016-07-08 11:09:27', '2016-07-08 11:09:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5148', '10000001', 'text', '今日日大盘主力资金还是在流出的', null, null, '2016-07-08 11:11:31', '2016-07-08 11:11:31', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5149', '10000001', 'text', '但是今日，能源互联网概念逆市上涨，市北高新(24.540, 0.73, 3.07%)涨3.86%', null, null, '2016-07-08 11:23:32', '2016-07-08 11:23:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5150', '10000013', 'text', '老师我拿着多单怎么办啊', null, null, '2016-07-08 11:26:27', '2016-07-08 11:26:27', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5151', '10000001', 'text', '什么位置的多单，仓位是多少？', '10000013', '老师我拿着多单怎么办啊', '2016-07-08 11:28:58', '2016-07-08 11:28:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5152', '10000013', 'text', '314的多单，30手1000桶', null, null, '2016-07-08 11:34:44', '2016-07-08 11:34:44', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5153', '10000013', 'text', '怎么办啊老师', null, null, '2016-07-08 11:38:54', '2016-07-08 11:38:54', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5154', '10000001', 'text', '昨晚我们伍老师已经给出建议，下破308适当减仓或锁仓一半#9', '10000013', '314的多单，30手1000桶', '2016-07-08 11:40:49', '2016-07-08 11:40:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5155', '10000252', 'text', '老师   直播室旁边的单子看不懂', null, null, '2016-07-08 11:42:38', '2016-07-08 11:42:38', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5156', '10000013', 'text', '为什么要锁仓啊', null, null, '2016-07-08 11:42:47', '2016-07-08 11:42:47', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5157', '10000001', 'text', '目前来说，油价在反弹盘整，可以持有观察，观察下午欧盘开盘后油价的走势，如果下破305位，先锁仓一半', '10000013', '怎么办啊老师', '2016-07-08 11:42:54', '2016-07-08 11:42:54', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5158', '10000001', 'text', '好了，我们先休息一会#9', '10000013', '为什么要锁仓啊', '2016-07-08 12:01:45', '2016-07-08 12:01:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5159', '10000001', 'text', '锁仓可以锁定**损', '10000013', '为什么要锁仓啊', '2016-07-08 12:01:56', '2016-07-08 12:01:56', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5160', '10000001', 'text', '降低风险', '10000013', '为什么要锁仓啊', '2016-07-08 12:22:03', '2016-07-08 12:22:03', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('5161', '10000001', 'text', '哪里不懂？#9', '10000252', '老师   直播室旁边的单子看不懂', '2016-07-08 12:24:12', '2016-07-08 12:24:12', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('5162', '10000001', 'text', '降低风险#9', '10000013', '为什么要锁仓啊', '2016-07-08 12:24:37', '2016-07-08 12:24:37', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5163', '10000001', 'text', '哪里不懂？#9', '10000252', '老师   直播室旁边的单子看不懂', '2016-07-08 12:24:47', '2016-07-08 12:24:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5164', '10000252', 'text', '305.6是凌晨4点系统平仓吗', null, null, '2016-07-08 12:34:05', '2016-07-08 12:34:05', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5165', '10000001', 'text', '不是，是到了止损位就平了#9', '10000252', '305.6是凌晨4点系统平仓吗', '2016-07-08 12:54:01', '2016-07-08 12:54:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5166', '10000000', 'text', '大家下午好，原油反弹无力，短线继续布局顺势空单，止损止盈带好，个人建议仅供参考。', null, null, '2016-07-08 12:55:30', '2016-07-08 12:55:30', '1', '#e71516', '20');
INSERT INTO `chat_chat` VALUES ('5167', '10000001', 'text', '#14', null, null, '2016-07-08 12:57:09', '2016-07-08 12:57:09', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5168', '10000001', 'text', '好了，我们去看看油价的走势', null, null, '2016-07-08 13:09:33', '2016-07-08 13:09:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5169', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f36d557557.png', null, null, '2016-07-08 13:15:01', '2016-07-08 13:15:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5170', '10000001', 'text', '图上看出，近两个月，油价都是在330-300这个大区间上运行', null, null, '2016-07-08 13:16:38', '2016-07-08 13:16:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5171', '10000001', 'text', '虽然昨晚油价创下了上月的新低，但在油价还没有下破300-330这个区间', null, null, '2016-07-08 13:24:03', '2016-07-08 13:24:03', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5172', '10000001', 'text', '昨晚的EIA原油库存降幅只是小幅低于预期，但EIA原油库存连续七周录得下滑', null, null, '2016-07-08 13:28:19', '2016-07-08 13:28:19', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5173', '10000001', 'text', '昨晚的EIA原油库存降幅虽然小幅低于预期，但EIA原油库存连续七周录得下滑', null, null, '2016-07-08 13:29:38', '2016-07-08 13:29:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5174', '10000001', 'text', '表明美国夏季对原油的需求依然强劲', null, null, '2016-07-08 13:30:08', '2016-07-08 13:30:08', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5175', '10000001', 'text', '所以，目前油价要下破300-300这个区间的可能性极低', null, null, '2016-07-08 13:30:59', '2016-07-08 13:30:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5176', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f3c24a88b3.png', null, null, '2016-07-08 13:37:40', '2016-07-08 13:37:40', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5177', '10000001', 'text', '目前油价震荡盘整，大家可以关注欧盘开盘后油价的走势', null, null, '2016-07-08 13:38:33', '2016-07-08 13:38:33', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5178', '10000001', 'text', '好了，那我们去关注一下下午的股市情况', null, null, '2016-07-08 13:39:43', '2016-07-08 13:39:43', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5179', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f3d9f11d05.png', null, null, '2016-07-08 13:43:59', '2016-07-08 13:43:59', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5180', '10000001', 'text', '股市午市开盘后看到证券板块资金有所流入喔', null, null, '2016-07-08 13:46:47', '2016-07-08 13:46:47', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5181', '10000001', 'text', '盘面上，国防军工、化工新材料、仪器仪表、电气设备领涨居前', null, null, '2016-07-08 13:58:17', '2016-07-08 13:58:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5182', '10000001', 'text', '煤炭开采、汽车整车、有色冶炼加工、钢铁等板块跌幅居前', null, null, '2016-07-08 13:59:12', '2016-07-08 13:59:12', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5183', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f42e84dd05.png', null, null, '2016-07-08 14:06:32', '2016-07-08 14:06:32', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5184', '10000001', 'text', '技术面上，今日大盘已经回踩到5日均线，而且运行在下方', null, null, '2016-07-08 14:07:17', '2016-07-08 14:07:17', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5185', '10000001', 'text', '午盘我们看到证券板块的资金在流入', null, null, '2016-07-08 14:08:13', '2016-07-08 14:08:13', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5186', '10000001', 'text', '那么今日大盘收盘很有可能收在3000点上方', null, null, '2016-07-08 14:08:55', '2016-07-08 14:08:55', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5187', '10000001', 'text', '好，我们回到今日关注数据方面：', null, null, '2016-07-08 14:14:05', '2016-07-08 14:14:05', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5188', '10000001', 'text', '今晚是非农夜！！！', null, null, '2016-07-08 14:16:48', '2016-07-08 14:16:48', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5189', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f4586dc5a5.png', null, null, '2016-07-08 14:17:42', '2016-07-08 14:17:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5190', '10000001', 'text', '20:30 美国6月失业率和美国6月季调后非农就业人口(万人)', null, null, '2016-07-08 14:18:58', '2016-07-08 14:18:58', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5191', '10000001', 'text', '非农夜来袭#14', null, null, '2016-07-08 14:19:27', '2016-07-08 14:19:27', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5192', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f4601781d9.jpg', null, null, '2016-07-08 14:19:45', '2016-07-08 14:19:45', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5193', '10000102', 'text', '行情真的会跌破300吗？#2', null, null, '2016-07-08 14:21:00', '2016-07-08 14:21:00', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5194', '10000001', 'text', '目前这个概率非常低，除非今晚非农数据非常强劲，但从目前公布的就业数据来看，美国的就是市场还是有所放缓的，失业率要达到4.8%是有所牵强的', '10000102', '行情真的会跌破300吗？#2', '2016-07-08 14:25:07', '2016-07-08 14:25:07', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5195', '10000001', 'text', '昨晚公布的美国“小非农”ADP数据显示，美国6月私营部门就业人口增加17.2万人', null, null, '2016-07-08 14:33:01', '2016-07-08 14:33:01', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5196', '10000001', 'text', '增幅略强于预期，前值由17.3万下修至16.8万；ADP数据的良好', null, null, '2016-07-08 14:34:24', '2016-07-08 14:34:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5197', '10000001', 'text', '暗示本周五公布的非农数据可能比上月大幅好转', null, null, '2016-07-08 14:35:18', '2016-07-08 14:35:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5198', '10000001', 'text', '但是6月非农就业系列数据会好转也是市场预期的', null, null, '2016-07-08 14:37:38', '2016-07-08 14:37:38', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5199', '10000037', 'text', '今晚市场预计是利多还是利空', null, null, '2016-07-08 14:38:31', '2016-07-08 14:38:31', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5200', '10000037', 'text', '？', null, null, '2016-07-08 14:38:34', '2016-07-08 14:38:34', '1', '#000000', '14');
INSERT INTO `chat_chat` VALUES ('5201', '10000001', 'text', '因为美国5月的非农就业人数仅仅增长3.8万，主要受到了当月电信业巨头工人罢工的影响', null, null, '2016-07-08 14:40:30', '2016-07-08 14:40:30', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5202', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f4b828ecd7.png', null, null, '2016-07-08 14:43:14', '2016-07-08 14:43:14', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5203', '10000001', 'text', '市场预期6月的非农数据料将良好，但不会过于亮眼#9', '10000037', '今晚市场预计是利多还是利空', '2016-07-08 14:44:02', '2016-07-08 14:44:02', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5204', '10000001', 'text', '目前油价跌破330-300区间的概率非常低，除非今晚的非农报告非常亮眼，不然后市油价将继续在该区间上运行', '10000102', '行情真的会跌破300吗？#2', '2016-07-08 14:51:05', '2016-07-08 14:51:05', '-1', '#000', '14');
INSERT INTO `chat_chat` VALUES ('5205', '10000001', 'text', '目前油价跌破330-300区间的概率非常低，除非今晚的非农报告非常亮眼，不然后市油价将继续在该区间上运行', '10000102', '行情真的会跌破300吗？#2', '2016-07-08 14:51:19', '2016-07-08 14:51:19', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5206', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f4ed4c8b89.png', null, null, '2016-07-08 14:57:24', '2016-07-08 14:57:24', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5207', '10000001', 'text', '每月非农报告带来的波动是非常大的', null, null, '2016-07-08 14:58:49', '2016-07-08 14:58:49', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5208', '10000001', 'text', '同时今晚6月份就业报告对于美联储下一次加息的时机至关重要', null, null, '2016-07-08 14:59:42', '2016-07-08 14:59:42', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5209', '10000001', 'text', '因为美联储6月货币会议纪要有部分官员在会上反对加息等待过久', null, null, '2016-07-08 15:01:50', '2016-07-08 15:01:50', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5210', '10000001', 'text', '并且大部分官员一致认为：要等待更多就业数据出炉', null, null, '2016-07-08 15:04:29', '2016-07-08 15:04:29', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5211', '10000001', 'text', '所以，今晚大家好好把握机会喔', null, null, '2016-07-08 15:05:57', '2016-07-08 15:05:57', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5212', '10000001', 'img', 'http://chat.xz286.com/data/upload/img/20160708/577f50f494dbc.jpg', null, null, '2016-07-08 15:06:28', '2016-07-08 15:06:28', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5213', '10000001', 'text', '各大机构预期非农就业人数（万/人）：高盛 21万，美银 18 摩根大通 16， 花旗 15.5， 德银 15.5 ，渣打 16', null, null, '2016-07-08 15:14:11', '2016-07-08 15:14:11', '-1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5214', '10000001', 'text', '高盛：预测今晚非农就业人口 21（万/人）', null, null, '2016-07-08 15:17:52', '2016-07-08 15:17:52', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5215', '10000001', 'text', '摩根大通：预测今晚非农就业人口 16（万/人）', null, null, '2016-07-08 15:19:07', '2016-07-08 15:19:07', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5216', '10000001', 'text', '花旗：预测今晚非农就业人口 15.5（万/人）', null, null, '2016-07-08 15:19:18', '2016-07-08 15:19:18', '1', '#249311', '20');
INSERT INTO `chat_chat` VALUES ('5217', '10000001', 'text', '好了，我们去关注一下，欧洲开盘后油市的走势', null, null, '2016-07-08 15:21:08', '2016-07-08 15:21:08', '1', '#249311', '20');

-- ----------------------------
-- Table structure for chat_comments
-- ----------------------------
DROP TABLE IF EXISTS `chat_comments`;
CREATE TABLE `chat_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_table` varchar(100) NOT NULL COMMENT '评论内容所在表，不带表前缀',
  `post_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '评论内容 id',
  `url` varchar(255) DEFAULT NULL COMMENT '原文地址',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '发表评论的用户id',
  `to_uid` int(11) NOT NULL DEFAULT '0' COMMENT '被评论的用户id',
  `full_name` varchar(50) DEFAULT NULL COMMENT '评论者昵称',
  `email` varchar(255) DEFAULT NULL COMMENT '评论者邮箱',
  `createtime` datetime NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '评论时间',
  `content` text NOT NULL COMMENT '评论内容',
  `type` smallint(1) NOT NULL DEFAULT '1' COMMENT '评论类型；1实名评论',
  `parentid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '被回复的评论id',
  `path` varchar(500) DEFAULT NULL,
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '状态，1已审核，0未审核',
  PRIMARY KEY (`id`),
  KEY `comment_post_ID` (`post_id`),
  KEY `comment_approved_date_gmt` (`status`),
  KEY `comment_parent` (`parentid`),
  KEY `table_id_status` (`post_table`,`post_id`,`status`),
  KEY `createtime` (`createtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='评论表';

-- ----------------------------
-- Records of chat_comments
-- ----------------------------

-- ----------------------------
-- Table structure for chat_common_action_log
-- ----------------------------
DROP TABLE IF EXISTS `chat_common_action_log`;
CREATE TABLE `chat_common_action_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` bigint(20) DEFAULT '0' COMMENT '用户id',
  `object` varchar(100) DEFAULT NULL COMMENT '访问对象的id,格式：不带前缀的表名+id;如posts1表示xx_posts表里id为1的记录',
  `action` varchar(50) DEFAULT NULL COMMENT '操作名称；格式规定为：应用名+控制器+操作名；也可自己定义格式只要不发生冲突且惟一；',
  `count` int(11) DEFAULT '0' COMMENT '访问次数',
  `last_time` int(11) DEFAULT '0' COMMENT '最后访问的时间戳',
  `ip` varchar(15) DEFAULT NULL COMMENT '访问者最后访问ip',
  PRIMARY KEY (`id`),
  KEY `user_object_action` (`user`,`object`,`action`),
  KEY `user_object_action_ip` (`user`,`object`,`action`,`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='访问记录表';

-- ----------------------------
-- Records of chat_common_action_log
-- ----------------------------

-- ----------------------------
-- Table structure for chat_config
-- ----------------------------
DROP TABLE IF EXISTS `chat_config`;
CREATE TABLE `chat_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(100) DEFAULT NULL,
  `value` varchar(1000) DEFAULT NULL,
  `info` varchar(200) DEFAULT NULL COMMENT '描述',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态：1正常，-1删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `key` (`key`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of chat_config
-- ----------------------------
INSERT INTO `chat_config` VALUES ('1', 'call_deal_variety', '1|大庆原油\n2|美国原油\n3|吉商现货银\n4|吉商现货油\n5|吉商现货铂\n6|吉商现货镍\n7|吉商现货铜', '喊单种类', '1', '2016-05-24 17:50:25');
INSERT INTO `chat_config` VALUES ('2', 'room_status', '1', '直播室状态', '1', '2016-06-15 21:48:10');
INSERT INTO `chat_config` VALUES ('3', 'home_link_photo', 'http://chat.xz286.com/public/static/img/yy.jpg|/', '首页图片广告位', '1', '2016-06-23 23:26:20');

-- ----------------------------
-- Table structure for chat_guestbook
-- ----------------------------
DROP TABLE IF EXISTS `chat_guestbook`;
CREATE TABLE `chat_guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(50) NOT NULL COMMENT '留言者姓名',
  `email` varchar(100) NOT NULL COMMENT '留言者邮箱',
  `title` varchar(255) DEFAULT NULL COMMENT '留言标题',
  `msg` text NOT NULL COMMENT '留言内容',
  `createtime` datetime NOT NULL COMMENT '留言时间',
  `status` smallint(2) NOT NULL DEFAULT '1' COMMENT '留言状态，1：正常，0：删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='留言表';

-- ----------------------------
-- Records of chat_guestbook
-- ----------------------------

-- ----------------------------
-- Table structure for chat_important
-- ----------------------------
DROP TABLE IF EXISTS `chat_important`;
CREATE TABLE `chat_important` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(500) DEFAULT NULL,
  `f_data` varchar(50) DEFAULT '0' COMMENT '前值',
  `y_data` varchar(50) DEFAULT '0' COMMENT '预期值',
  `r_data` varchar(50) DEFAULT '0' COMMENT '公布值',
  `impact` varchar(50) DEFAULT NULL COMMENT '影响：利空还是利多',
  `r_time` timestamp NULL DEFAULT NULL COMMENT '公布时间',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态，1正常，-1删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='重要经济数据表';

-- ----------------------------
-- Records of chat_important
-- ----------------------------
INSERT INTO `chat_important` VALUES ('1', '美国6月17日当周EIA原油库存变动(万桶)', '-93.3', '-93.3', '-93.3', '这是一个美国指标，库存的增减，会影响到原油价格，', '2016-06-22 22:30:00', '2016-05-24 16:09:30', '-1');
INSERT INTO `chat_important` VALUES ('2', '美国贝克休斯总钻井总数(口)', '424', '-', '', '这是一个美国指标，钻井数量的增减，意味着原油产出料将相应增加或减少，导致原油价格的变化', '2016-06-25 01:00:00', '2016-06-24 14:44:05', '-1');
INSERT INTO `chat_important` VALUES ('3', '美国6月Markit服务业PMI初值', '51.3', '51.3', '51.3', '数据好于预期利空，反之则利多', '0000-00-00 00:00:00', '2016-06-27 15:24:27', '-1');
INSERT INTO `chat_important` VALUES ('4', '美国第一季度实际GDP年化季率终值', '0.8%', '0.8%', '0.8%', '实际值&gt;预期值=利好美元', '0000-00-00 00:00:00', '2016-06-28 09:26:01', '-1');
INSERT INTO `chat_important` VALUES ('5', '美国至6月24日当周API原油库存(万桶)', '-522.4', '-237.5', '-386', '数据&lt;预期=利好原油价格', '0000-00-00 00:00:00', '2016-06-29 09:31:00', '-1');
INSERT INTO `chat_important` VALUES ('6', '美国当周EIA原油库存(万桶)', '-91.7', '-237.5', '', '数据&lt;预期=利好原油价格', '2016-06-29 22:30:00', '2016-06-29 09:36:38', '-1');
INSERT INTO `chat_important` VALUES ('7', '美国至6月25日当周初请失业金人数(万人)', '25.9', '26.7', '', '公布值＞预期值，利空美元，利多非美。', '2016-06-29 20:30:00', '2016-06-30 09:30:24', '-1');
INSERT INTO `chat_important` VALUES ('8', '美国6月ISM制造业PMI', '51.3', '51.4', '', '实际值&gt;预期值=利好美元 。 ISM指数ISM指数是由美国供应管理协会公布的重要数据，对反映美', '2016-07-01 22:00:00', '2016-07-01 10:06:32', '-1');
INSERT INTO `chat_important` VALUES ('9', '欧元区5月PPI月率', '-0.3%', '0.3%', '', '公布值&gt;预测值=利好欧元    该数据一般仅产生潜在影响，因占欧元区整体经济约一半的德国和法国', '2016-07-01 17:00:00', '2016-07-04 09:03:51', '-1');
INSERT INTO `chat_important` VALUES ('10', '美国5月工厂订单月率', '1.9%', '-1%', '', '实际值&gt;预期值=利好美元    若该数据增长，则表示制造业情况有所改善，利好该国货币。反之若降', '2016-07-05 22:00:00', '2016-07-05 10:07:35', '-1');
INSERT INTO `chat_important` VALUES ('11', '美国6月ISM非制造业PMI', '52.9', '53.3', '', '实际值&gt;预期值=利好美元\nISM服务业（非制造业指数）是由美国供应管理协会（the Insti', '2016-07-06 22:00:00', '2016-07-06 08:59:40', '-1');
INSERT INTO `chat_important` VALUES ('12', '美国至7月1日当周EIA原油库存(万桶)', '-405.3', '-225', '', '数据&lt;预期=利好原油价格  \n库存的增减，会影响到原油价格，进而对加元产生影响，因加拿大原油出', '2016-07-07 23:00:00', '2016-07-07 14:04:00', '1');

-- ----------------------------
-- Table structure for chat_links
-- ----------------------------
DROP TABLE IF EXISTS `chat_links`;
CREATE TABLE `chat_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) NOT NULL COMMENT '友情链接地址',
  `link_name` varchar(255) NOT NULL COMMENT '友情链接名称',
  `link_image` varchar(255) DEFAULT NULL COMMENT '友情链接图标',
  `link_target` varchar(25) NOT NULL DEFAULT '_blank' COMMENT '友情链接打开方式',
  `link_description` text NOT NULL COMMENT '友情链接描述',
  `link_status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1显示，0不显示',
  `link_rating` int(11) NOT NULL DEFAULT '0' COMMENT '友情链接评级',
  `link_rel` varchar(255) DEFAULT NULL COMMENT '链接与网站的关系',
  `listorder` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='友情链接表';

-- ----------------------------
-- Records of chat_links
-- ----------------------------
INSERT INTO `chat_links` VALUES ('1', 'http://www.thinkcmf.com', 'ThinkCMF', '', '_blank', '', '1', '0', '', '0');

-- ----------------------------
-- Table structure for chat_menu
-- ----------------------------
DROP TABLE IF EXISTS `chat_menu`;
CREATE TABLE `chat_menu` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `app` char(20) NOT NULL COMMENT '应用名称app',
  `model` char(20) NOT NULL COMMENT '控制器',
  `action` char(20) NOT NULL COMMENT '操作名称',
  `data` char(50) NOT NULL COMMENT '额外参数',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '菜单类型  1：权限认证+菜单；0：只作为菜单',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态，1显示，0不显示',
  `name` varchar(50) NOT NULL COMMENT '菜单名称',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `remark` varchar(255) NOT NULL COMMENT '备注',
  `listorder` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '排序ID',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `parentid` (`parentid`),
  KEY `model` (`model`)
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='后台菜单表';

-- ----------------------------
-- Records of chat_menu
-- ----------------------------
INSERT INTO `chat_menu` VALUES ('1', '0', 'Admin', 'Content', 'default', '', '0', '0', '内容管理', 'th', '', '30');
INSERT INTO `chat_menu` VALUES ('2', '1', 'Api', 'Guestbookadmin', 'index', '', '1', '1', '所有留言', '', '', '0');
INSERT INTO `chat_menu` VALUES ('3', '2', 'Api', 'Guestbookadmin', 'delete', '', '1', '0', '删除网站留言', '', '', '0');
INSERT INTO `chat_menu` VALUES ('4', '1', 'Comment', 'Commentadmin', 'index', '', '1', '1', '评论管理', '', '', '0');
INSERT INTO `chat_menu` VALUES ('5', '4', 'Comment', 'Commentadmin', 'delete', '', '1', '0', '删除评论', '', '', '0');
INSERT INTO `chat_menu` VALUES ('6', '4', 'Comment', 'Commentadmin', 'check', '', '1', '0', '评论审核', '', '', '0');
INSERT INTO `chat_menu` VALUES ('7', '1', 'Portal', 'AdminPost', 'index', '', '1', '1', '文章管理', '', '', '1');
INSERT INTO `chat_menu` VALUES ('8', '7', 'Portal', 'AdminPost', 'listorders', '', '1', '0', '文章排序', '', '', '0');
INSERT INTO `chat_menu` VALUES ('9', '7', 'Portal', 'AdminPost', 'top', '', '1', '0', '文章置顶', '', '', '0');
INSERT INTO `chat_menu` VALUES ('10', '7', 'Portal', 'AdminPost', 'recommend', '', '1', '0', '文章推荐', '', '', '0');
INSERT INTO `chat_menu` VALUES ('11', '7', 'Portal', 'AdminPost', 'move', '', '1', '0', '批量移动', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('12', '7', 'Portal', 'AdminPost', 'check', '', '1', '0', '文章审核', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('13', '7', 'Portal', 'AdminPost', 'delete', '', '1', '0', '删除文章', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('14', '7', 'Portal', 'AdminPost', 'edit', '', '1', '0', '编辑文章', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('15', '14', 'Portal', 'AdminPost', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `chat_menu` VALUES ('16', '7', 'Portal', 'AdminPost', 'add', '', '1', '0', '添加文章', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('17', '16', 'Portal', 'AdminPost', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `chat_menu` VALUES ('18', '1', 'Portal', 'AdminTerm', 'index', '', '0', '1', '分类管理', '', '', '2');
INSERT INTO `chat_menu` VALUES ('19', '18', 'Portal', 'AdminTerm', 'listorders', '', '1', '0', '文章分类排序', '', '', '0');
INSERT INTO `chat_menu` VALUES ('20', '18', 'Portal', 'AdminTerm', 'delete', '', '1', '0', '删除分类', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('21', '18', 'Portal', 'AdminTerm', 'edit', '', '1', '0', '编辑分类', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('22', '21', 'Portal', 'AdminTerm', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `chat_menu` VALUES ('23', '18', 'Portal', 'AdminTerm', 'add', '', '1', '0', '添加分类', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('24', '23', 'Portal', 'AdminTerm', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `chat_menu` VALUES ('25', '1', 'Portal', 'AdminPage', 'index', '', '1', '1', '页面管理', '', '', '3');
INSERT INTO `chat_menu` VALUES ('26', '25', 'Portal', 'AdminPage', 'listorders', '', '1', '0', '页面排序', '', '', '0');
INSERT INTO `chat_menu` VALUES ('27', '25', 'Portal', 'AdminPage', 'delete', '', '1', '0', '删除页面', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('28', '25', 'Portal', 'AdminPage', 'edit', '', '1', '0', '编辑页面', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('29', '28', 'Portal', 'AdminPage', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `chat_menu` VALUES ('30', '25', 'Portal', 'AdminPage', 'add', '', '1', '0', '添加页面', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('31', '30', 'Portal', 'AdminPage', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `chat_menu` VALUES ('32', '1', 'Admin', 'Recycle', 'default', '', '1', '1', '回收站', '', '', '4');
INSERT INTO `chat_menu` VALUES ('33', '32', 'Portal', 'AdminPost', 'recyclebin', '', '1', '1', '文章回收', '', '', '0');
INSERT INTO `chat_menu` VALUES ('34', '33', 'Portal', 'AdminPost', 'restore', '', '1', '0', '文章还原', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('35', '33', 'Portal', 'AdminPost', 'clean', '', '1', '0', '彻底删除', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('36', '32', 'Portal', 'AdminPage', 'recyclebin', '', '1', '1', '页面回收', '', '', '1');
INSERT INTO `chat_menu` VALUES ('37', '36', 'Portal', 'AdminPage', 'clean', '', '1', '0', '彻底删除', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('38', '36', 'Portal', 'AdminPage', 'restore', '', '1', '0', '页面还原', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('39', '0', 'Admin', 'Extension', 'default', '', '0', '0', '扩展工具', 'cloud', '', '40');
INSERT INTO `chat_menu` VALUES ('40', '39', 'Admin', 'Backup', 'default', '', '1', '1', '备份管理', '', '', '0');
INSERT INTO `chat_menu` VALUES ('41', '40', 'Admin', 'Backup', 'restore', '', '1', '1', '数据还原', '', '', '0');
INSERT INTO `chat_menu` VALUES ('42', '40', 'Admin', 'Backup', 'index', '', '1', '1', '数据备份', '', '', '0');
INSERT INTO `chat_menu` VALUES ('43', '42', 'Admin', 'Backup', 'index_post', '', '1', '0', '提交数据备份', '', '', '0');
INSERT INTO `chat_menu` VALUES ('44', '40', 'Admin', 'Backup', 'download', '', '1', '0', '下载备份', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('45', '40', 'Admin', 'Backup', 'del_backup', '', '1', '0', '删除备份', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('46', '40', 'Admin', 'Backup', 'import', '', '1', '0', '数据备份导入', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('47', '39', 'Admin', 'Plugin', 'index', '', '1', '1', '插件管理', '', '', '0');
INSERT INTO `chat_menu` VALUES ('48', '47', 'Admin', 'Plugin', 'toggle', '', '1', '0', '插件启用切换', '', '', '0');
INSERT INTO `chat_menu` VALUES ('49', '47', 'Admin', 'Plugin', 'setting', '', '1', '0', '插件设置', '', '', '0');
INSERT INTO `chat_menu` VALUES ('50', '49', 'Admin', 'Plugin', 'setting_post', '', '1', '0', '插件设置提交', '', '', '0');
INSERT INTO `chat_menu` VALUES ('51', '47', 'Admin', 'Plugin', 'install', '', '1', '0', '插件安装', '', '', '0');
INSERT INTO `chat_menu` VALUES ('52', '47', 'Admin', 'Plugin', 'uninstall', '', '1', '0', '插件卸载', '', '', '0');
INSERT INTO `chat_menu` VALUES ('53', '39', 'Admin', 'Slide', 'default', '', '1', '1', '幻灯片', '', '', '1');
INSERT INTO `chat_menu` VALUES ('54', '53', 'Admin', 'Slide', 'index', '', '1', '1', '幻灯片管理', '', '', '0');
INSERT INTO `chat_menu` VALUES ('55', '54', 'Admin', 'Slide', 'listorders', '', '1', '0', '幻灯片排序', '', '', '0');
INSERT INTO `chat_menu` VALUES ('56', '54', 'Admin', 'Slide', 'toggle', '', '1', '0', '幻灯片显示切换', '', '', '0');
INSERT INTO `chat_menu` VALUES ('57', '54', 'Admin', 'Slide', 'delete', '', '1', '0', '删除幻灯片', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('58', '54', 'Admin', 'Slide', 'edit', '', '1', '0', '编辑幻灯片', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('59', '58', 'Admin', 'Slide', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `chat_menu` VALUES ('60', '54', 'Admin', 'Slide', 'add', '', '1', '0', '添加幻灯片', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('61', '60', 'Admin', 'Slide', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `chat_menu` VALUES ('62', '53', 'Admin', 'Slidecat', 'index', '', '1', '1', '幻灯片分类', '', '', '0');
INSERT INTO `chat_menu` VALUES ('63', '62', 'Admin', 'Slidecat', 'delete', '', '1', '0', '删除分类', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('64', '62', 'Admin', 'Slidecat', 'edit', '', '1', '0', '编辑分类', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('65', '64', 'Admin', 'Slidecat', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `chat_menu` VALUES ('66', '62', 'Admin', 'Slidecat', 'add', '', '1', '0', '添加分类', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('67', '66', 'Admin', 'Slidecat', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `chat_menu` VALUES ('68', '39', 'Admin', 'Ad', 'index', '', '1', '1', '网站广告', '', '', '2');
INSERT INTO `chat_menu` VALUES ('69', '68', 'Admin', 'Ad', 'toggle', '', '1', '0', '广告显示切换', '', '', '0');
INSERT INTO `chat_menu` VALUES ('70', '68', 'Admin', 'Ad', 'delete', '', '1', '0', '删除广告', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('71', '68', 'Admin', 'Ad', 'edit', '', '1', '0', '编辑广告', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('72', '71', 'Admin', 'Ad', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `chat_menu` VALUES ('73', '68', 'Admin', 'Ad', 'add', '', '1', '0', '添加广告', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('74', '73', 'Admin', 'Ad', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `chat_menu` VALUES ('75', '39', 'Admin', 'Link', 'index', '', '0', '1', '友情链接', '', '', '3');
INSERT INTO `chat_menu` VALUES ('76', '75', 'Admin', 'Link', 'listorders', '', '1', '0', '友情链接排序', '', '', '0');
INSERT INTO `chat_menu` VALUES ('77', '75', 'Admin', 'Link', 'toggle', '', '1', '0', '友链显示切换', '', '', '0');
INSERT INTO `chat_menu` VALUES ('78', '75', 'Admin', 'Link', 'delete', '', '1', '0', '删除友情链接', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('79', '75', 'Admin', 'Link', 'edit', '', '1', '0', '编辑友情链接', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('80', '79', 'Admin', 'Link', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `chat_menu` VALUES ('81', '75', 'Admin', 'Link', 'add', '', '1', '0', '添加友情链接', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('82', '81', 'Admin', 'Link', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `chat_menu` VALUES ('83', '39', 'Api', 'Oauthadmin', 'setting', '', '1', '1', '第三方登陆', 'leaf', '', '4');
INSERT INTO `chat_menu` VALUES ('84', '83', 'Api', 'Oauthadmin', 'setting_post', '', '1', '0', '提交设置', '', '', '0');
INSERT INTO `chat_menu` VALUES ('85', '0', 'Admin', 'Menu', 'default', '', '1', '1', '菜单管理', 'list', '', '20');
INSERT INTO `chat_menu` VALUES ('86', '85', 'Admin', 'Navcat', 'default1', '', '1', '1', '前台菜单', '', '', '0');
INSERT INTO `chat_menu` VALUES ('87', '86', 'Admin', 'Nav', 'index', '', '1', '1', '菜单管理', '', '', '0');
INSERT INTO `chat_menu` VALUES ('88', '87', 'Admin', 'Nav', 'listorders', '', '1', '0', '前台导航排序', '', '', '0');
INSERT INTO `chat_menu` VALUES ('89', '87', 'Admin', 'Nav', 'delete', '', '1', '0', '删除菜单', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('90', '87', 'Admin', 'Nav', 'edit', '', '1', '0', '编辑菜单', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('91', '90', 'Admin', 'Nav', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `chat_menu` VALUES ('92', '87', 'Admin', 'Nav', 'add', '', '1', '0', '添加菜单', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('93', '92', 'Admin', 'Nav', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `chat_menu` VALUES ('94', '86', 'Admin', 'Navcat', 'index', '', '1', '1', '菜单分类', '', '', '0');
INSERT INTO `chat_menu` VALUES ('95', '94', 'Admin', 'Navcat', 'delete', '', '1', '0', '删除分类', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('96', '94', 'Admin', 'Navcat', 'edit', '', '1', '0', '编辑分类', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('97', '96', 'Admin', 'Navcat', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `chat_menu` VALUES ('98', '94', 'Admin', 'Navcat', 'add', '', '1', '0', '添加分类', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('99', '98', 'Admin', 'Navcat', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `chat_menu` VALUES ('100', '85', 'Admin', 'Menu', 'index', '', '1', '1', '后台菜单', '', '', '0');
INSERT INTO `chat_menu` VALUES ('101', '100', 'Admin', 'Menu', 'add', '', '1', '0', '添加菜单', '', '', '0');
INSERT INTO `chat_menu` VALUES ('102', '101', 'Admin', 'Menu', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `chat_menu` VALUES ('103', '100', 'Admin', 'Menu', 'listorders', '', '1', '0', '后台菜单排序', '', '', '0');
INSERT INTO `chat_menu` VALUES ('104', '100', 'Admin', 'Menu', 'export_menu', '', '1', '0', '菜单备份', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('105', '100', 'Admin', 'Menu', 'edit', '', '1', '0', '编辑菜单', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('106', '105', 'Admin', 'Menu', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `chat_menu` VALUES ('107', '100', 'Admin', 'Menu', 'delete', '', '1', '0', '删除菜单', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('108', '100', 'Admin', 'Menu', 'lists', '', '1', '0', '所有菜单', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('109', '0', 'Admin', 'Setting', 'default', '', '0', '1', '设置', 'cogs', '', '0');
INSERT INTO `chat_menu` VALUES ('110', '109', 'Admin', 'Setting', 'userdefault', '', '0', '1', '个人信息', '', '', '0');
INSERT INTO `chat_menu` VALUES ('111', '110', 'Admin', 'User', 'userinfo', '', '1', '1', '修改信息', '', '', '0');
INSERT INTO `chat_menu` VALUES ('112', '111', 'Admin', 'User', 'userinfo_post', '', '1', '0', '修改信息提交', '', '', '0');
INSERT INTO `chat_menu` VALUES ('113', '110', 'Admin', 'Setting', 'password', '', '1', '1', '修改密码', '', '', '0');
INSERT INTO `chat_menu` VALUES ('114', '113', 'Admin', 'Setting', 'password_post', '', '1', '0', '提交修改', '', '', '0');
INSERT INTO `chat_menu` VALUES ('115', '109', 'Admin', 'Setting', 'site', '', '1', '1', '网站信息', '', '', '0');
INSERT INTO `chat_menu` VALUES ('116', '115', 'Admin', 'Setting', 'site_post', '', '1', '0', '提交修改', '', '', '0');
INSERT INTO `chat_menu` VALUES ('117', '115', 'Admin', 'Route', 'index', '', '1', '0', '路由列表', '', '', '0');
INSERT INTO `chat_menu` VALUES ('118', '115', 'Admin', 'Route', 'add', '', '1', '0', '路由添加', '', '', '0');
INSERT INTO `chat_menu` VALUES ('119', '118', 'Admin', 'Route', 'add_post', '', '1', '0', '路由添加提交', '', '', '0');
INSERT INTO `chat_menu` VALUES ('120', '115', 'Admin', 'Route', 'edit', '', '1', '0', '路由编辑', '', '', '0');
INSERT INTO `chat_menu` VALUES ('121', '120', 'Admin', 'Route', 'edit_post', '', '1', '0', '路由编辑提交', '', '', '0');
INSERT INTO `chat_menu` VALUES ('122', '115', 'Admin', 'Route', 'delete', '', '1', '0', '路由删除', '', '', '0');
INSERT INTO `chat_menu` VALUES ('123', '115', 'Admin', 'Route', 'ban', '', '1', '0', '路由禁止', '', '', '0');
INSERT INTO `chat_menu` VALUES ('124', '115', 'Admin', 'Route', 'open', '', '1', '0', '路由启用', '', '', '0');
INSERT INTO `chat_menu` VALUES ('125', '115', 'Admin', 'Route', 'listorders', '', '1', '0', '路由排序', '', '', '0');
INSERT INTO `chat_menu` VALUES ('126', '109', 'Admin', 'Mailer', 'default', '', '1', '1', '邮箱配置', '', '', '0');
INSERT INTO `chat_menu` VALUES ('127', '126', 'Admin', 'Mailer', 'index', '', '1', '1', 'SMTP配置', '', '', '0');
INSERT INTO `chat_menu` VALUES ('128', '127', 'Admin', 'Mailer', 'index_post', '', '1', '0', '提交配置', '', '', '0');
INSERT INTO `chat_menu` VALUES ('129', '126', 'Admin', 'Mailer', 'active', '', '1', '1', '邮件模板', '', '', '0');
INSERT INTO `chat_menu` VALUES ('130', '129', 'Admin', 'Mailer', 'active_post', '', '1', '0', '提交模板', '', '', '0');
INSERT INTO `chat_menu` VALUES ('131', '109', 'Admin', 'Setting', 'clearcache', '', '1', '1', '清除缓存', '', '', '1');
INSERT INTO `chat_menu` VALUES ('132', '0', 'User', 'Indexadmin', 'default', '', '1', '1', '用户管理', 'group', '', '10');
INSERT INTO `chat_menu` VALUES ('133', '132', 'User', 'Indexadmin', 'default1', '', '1', '1', '用户组', '', '', '0');
INSERT INTO `chat_menu` VALUES ('134', '133', 'User', 'Indexadmin', 'index', '', '1', '1', '本站用户', 'leaf', '', '0');
INSERT INTO `chat_menu` VALUES ('135', '134', 'User', 'Indexadmin', 'ban', '', '1', '0', '拉黑会员', '', '', '0');
INSERT INTO `chat_menu` VALUES ('136', '134', 'User', 'Indexadmin', 'cancelban', '', '1', '0', '启用会员', '', '', '0');
INSERT INTO `chat_menu` VALUES ('137', '133', 'User', 'Oauthadmin', 'index', '', '1', '0', '第三方用户', 'leaf', '', '0');
INSERT INTO `chat_menu` VALUES ('138', '137', 'User', 'Oauthadmin', 'delete', '', '1', '0', '第三方用户解绑', '', '', '0');
INSERT INTO `chat_menu` VALUES ('139', '132', 'User', 'Indexadmin', 'default3', '', '1', '1', '管理组', '', '', '0');
INSERT INTO `chat_menu` VALUES ('140', '139', 'Admin', 'Rbac', 'index', '', '1', '1', '角色管理', '', '', '0');
INSERT INTO `chat_menu` VALUES ('141', '140', 'Admin', 'Rbac', 'member', '', '1', '0', '成员管理', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('142', '140', 'Admin', 'Rbac', 'authorize', '', '1', '0', '权限设置', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('143', '142', 'Admin', 'Rbac', 'authorize_post', '', '1', '0', '提交设置', '', '', '0');
INSERT INTO `chat_menu` VALUES ('144', '140', 'Admin', 'Rbac', 'roleedit', '', '1', '0', '编辑角色', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('145', '144', 'Admin', 'Rbac', 'roleedit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `chat_menu` VALUES ('146', '140', 'Admin', 'Rbac', 'roledelete', '', '1', '1', '删除角色', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('147', '140', 'Admin', 'Rbac', 'roleadd', '', '1', '1', '添加角色', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('148', '147', 'Admin', 'Rbac', 'roleadd_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `chat_menu` VALUES ('149', '139', 'Admin', 'User', 'index', '', '1', '1', '管理员', '', '', '0');
INSERT INTO `chat_menu` VALUES ('150', '149', 'Admin', 'User', 'delete', '', '1', '0', '删除管理员', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('151', '149', 'Admin', 'User', 'edit', '', '1', '0', '管理员编辑', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('152', '151', 'Admin', 'User', 'edit_post', '', '1', '0', '编辑提交', '', '', '0');
INSERT INTO `chat_menu` VALUES ('153', '149', 'Admin', 'User', 'add', '', '1', '0', '管理员添加', '', '', '1000');
INSERT INTO `chat_menu` VALUES ('154', '153', 'Admin', 'User', 'add_post', '', '1', '0', '添加提交', '', '', '0');
INSERT INTO `chat_menu` VALUES ('155', '47', 'Admin', 'Plugin', 'update', '', '1', '0', '插件更新', '', '', '0');
INSERT INTO `chat_menu` VALUES ('156', '39', 'Admin', 'Storage', 'index', '', '1', '1', '文件存储', '', '', '0');
INSERT INTO `chat_menu` VALUES ('157', '156', 'Admin', 'Storage', 'setting_post', '', '1', '0', '文件存储设置提交', '', '', '0');
INSERT INTO `chat_menu` VALUES ('158', '54', 'Admin', 'Slide', 'ban', '', '1', '0', '禁用幻灯片', '', '', '0');
INSERT INTO `chat_menu` VALUES ('159', '54', 'Admin', 'Slide', 'cancelban', '', '1', '0', '启用幻灯片', '', '', '0');
INSERT INTO `chat_menu` VALUES ('160', '149', 'Admin', 'User', 'ban', '', '1', '0', '禁用管理员', '', '', '0');
INSERT INTO `chat_menu` VALUES ('161', '149', 'Admin', 'User', 'cancelban', '', '1', '0', '启用管理员', '', '', '0');
INSERT INTO `chat_menu` VALUES ('162', '0', 'Admin', 'Zbs', 'default', '', '1', '1', '直播室管理', 'bullseye', '', '0');
INSERT INTO `chat_menu` VALUES ('163', '162', 'Admin', 'Important', 'index', '', '1', '1', '重要经济数据', '', '', '0');
INSERT INTO `chat_menu` VALUES ('164', '162', 'Admin', 'Call', 'index', '', '1', '1', '喊单管理', '', '', '0');
INSERT INTO `chat_menu` VALUES ('165', '0', 'Admin', 'Config', 'index', '', '1', '1', '配置管理', 'ils', '', '0');
INSERT INTO `chat_menu` VALUES ('166', '162', 'Admin', 'User', 'teacher', '', '1', '1', '老师管理', '', '', '0');
INSERT INTO `chat_menu` VALUES ('167', '162', 'Admin', 'Live', 'index', '', '1', '1', '聊天', '', '', '0');
INSERT INTO `chat_menu` VALUES ('168', '162', 'Admin', 'Zhanfa', 'index', '', '1', '1', '战法列表', '', '', '0');
INSERT INTO `chat_menu` VALUES ('169', '162', 'Admin', 'Word', 'index', '', '1', '1', '敏感词管理', '', '', '0');
INSERT INTO `chat_menu` VALUES ('170', '162', 'Admin', 'Call', 'add', '', '1', '0', '添加喊单', '', '', '0');
INSERT INTO `chat_menu` VALUES ('171', '162', 'Admin', 'Call', 'ping', '', '1', '0', '喊单平仓', '', '', '0');
INSERT INTO `chat_menu` VALUES ('172', '162', 'Admin', 'Word', 'add', '', '1', '0', '敏感词添加', '', '', '0');
INSERT INTO `chat_menu` VALUES ('173', '162', 'Admin', 'Word', 'edit', '', '1', '0', '敏感词编辑', '', '', '0');
INSERT INTO `chat_menu` VALUES ('174', '162', 'Admin', 'Word', 'del', '', '1', '0', '敏感词隐藏', '', '', '0');
INSERT INTO `chat_menu` VALUES ('175', '162', 'Admin', 'Zhanfa', 'add', '', '1', '0', '添加战法', '', '', '0');
INSERT INTO `chat_menu` VALUES ('176', '162', 'Admin', 'Zhanfa', 'del', '', '1', '0', '隐藏战法', '', '', '0');
INSERT INTO `chat_menu` VALUES ('177', '165', 'Admin', 'Config', 'add', '', '1', '0', '添加配置', '', '', '0');
INSERT INTO `chat_menu` VALUES ('178', '165', 'Admin', 'Config', 'editor', '', '1', '0', '编辑配置', '', '', '0');
INSERT INTO `chat_menu` VALUES ('179', '165', 'Admin', 'Config', 'del', '', '1', '0', '删除配置', '', '', '0');
INSERT INTO `chat_menu` VALUES ('180', '109', 'Admin', 'User', 'avatar', '', '1', '0', '设置头像', '', '', '0');
INSERT INTO `chat_menu` VALUES ('181', '162', 'Admin', 'Chat', 'index', '', '1', '1', '消息管理', '', '', '0');
INSERT INTO `chat_menu` VALUES ('182', '162', 'Admin', 'Important', 'add', '', '1', '0', '重要经济数据添加', '', '', '0');
INSERT INTO `chat_menu` VALUES ('183', '162', 'Admin', 'Important', 'edit', '', '1', '0', '重要经济数据修改', '', '', '0');
INSERT INTO `chat_menu` VALUES ('184', '162', 'Admin', 'Important', 'del', '', '1', '0', '重要经济数据删除', '', '', '0');
INSERT INTO `chat_menu` VALUES ('185', '162', 'Admin', 'Chat', 'del', '', '1', '0', '消息删除', '', '', '0');
INSERT INTO `chat_menu` VALUES ('186', '162', 'Admin', 'call', 'edit', '', '1', '0', '喊单编辑', '', '', '0');
INSERT INTO `chat_menu` VALUES ('187', '162', 'Admin', 'Call', 'del', '', '1', '0', '喊单删除', '', '', '0');

-- ----------------------------
-- Table structure for chat_nav
-- ----------------------------
DROP TABLE IF EXISTS `chat_nav`;
CREATE TABLE `chat_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL COMMENT '导航分类 id',
  `parentid` int(11) NOT NULL COMMENT '导航父 id',
  `label` varchar(255) NOT NULL COMMENT '导航标题',
  `target` varchar(50) DEFAULT NULL COMMENT '打开方式',
  `href` varchar(255) NOT NULL COMMENT '导航链接',
  `icon` varchar(255) NOT NULL COMMENT '导航图标',
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1显示，0不显示',
  `listorder` int(6) DEFAULT '0' COMMENT '排序',
  `path` varchar(255) NOT NULL DEFAULT '0' COMMENT '层级关系',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='前台导航表';

-- ----------------------------
-- Records of chat_nav
-- ----------------------------
INSERT INTO `chat_nav` VALUES ('1', '1', '0', '首页', '', 'home', '', '1', '0', '0-1');
INSERT INTO `chat_nav` VALUES ('2', '1', '0', '列表演示', '', 'a:2:{s:6:\"action\";s:17:\"Portal/List/index\";s:5:\"param\";a:1:{s:2:\"id\";s:1:\"1\";}}', '', '1', '0', '0-2');
INSERT INTO `chat_nav` VALUES ('3', '1', '0', '瀑布流', '', 'a:2:{s:6:\"action\";s:17:\"Portal/List/index\";s:5:\"param\";a:1:{s:2:\"id\";s:1:\"2\";}}', '', '1', '0', '0-3');

-- ----------------------------
-- Table structure for chat_nav_cat
-- ----------------------------
DROP TABLE IF EXISTS `chat_nav_cat`;
CREATE TABLE `chat_nav_cat` (
  `navcid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '导航分类名',
  `active` int(1) NOT NULL DEFAULT '1' COMMENT '是否为主菜单，1是，0不是',
  `remark` text COMMENT '备注',
  PRIMARY KEY (`navcid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='前台导航分类表';

-- ----------------------------
-- Records of chat_nav_cat
-- ----------------------------
INSERT INTO `chat_nav_cat` VALUES ('1', '主导航', '1', '主导航');

-- ----------------------------
-- Table structure for chat_note
-- ----------------------------
DROP TABLE IF EXISTS `chat_note`;
CREATE TABLE `chat_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `to` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT '1' COMMENT '1：文字，2图片，3语音',
  `message` varchar(1000) DEFAULT NULL,
  `note_uid` int(11) DEFAULT NULL,
  `note_message` varchar(1000) DEFAULT NULL,
  `note_time` datetime DEFAULT NULL COMMENT '回复内容创建时间',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1' COMMENT '状态：1，-1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of chat_note
-- ----------------------------
INSERT INTO `chat_note` VALUES ('1', '5', '8', '0', 'ssss', null, '', null, '2016-06-02 22:45:36', '1');
INSERT INTO `chat_note` VALUES ('2', '5', '8', '0', 'fsdfsdfsdfsdfsd', null, '', null, '2016-06-02 22:45:50', '1');
INSERT INTO `chat_note` VALUES ('3', '5', '0', '0', '是撒大声地', null, '', null, '2016-06-02 22:48:50', '1');
INSERT INTO `chat_note` VALUES ('4', '2', '3', '0', 'jjjj4ff', null, '', null, '2016-06-02 22:48:56', '1');
INSERT INTO `chat_note` VALUES ('5', '5', '0', '0', '柔柔弱弱', null, '', null, '2016-06-02 22:49:14', '1');
INSERT INTO `chat_note` VALUES ('6', '2', '8', '0', 'jjjj4ff', null, '', null, '2016-06-02 23:01:09', '1');
INSERT INTO `chat_note` VALUES ('7', '5', '3', '0', 'qqqqq', null, '', null, '2016-06-02 23:01:48', '1');
INSERT INTO `chat_note` VALUES ('8', '2', '8', '0', '等到山顶氵丁三点水的', null, '', null, '2016-06-02 23:04:04', '1');
INSERT INTO `chat_note` VALUES ('9', '2', '3', '0', '等到山顶氵丁三点水的#5快快快', null, '', null, '2016-06-02 23:05:06', '1');
INSERT INTO `chat_note` VALUES ('10', '2', '3', '0', '#21', null, '', null, '2016-06-02 23:05:25', '1');
INSERT INTO `chat_note` VALUES ('11', '5', '3', '0', 'pppp', null, '', null, '2016-06-02 23:05:32', '1');
INSERT INTO `chat_note` VALUES ('12', '2', '3', '0', '#2#17', null, '', null, '2016-06-02 23:05:48', '1');
INSERT INTO `chat_note` VALUES ('13', '2', '0', '0', '#7#6#6', null, '', null, '2016-06-02 23:11:26', '1');
INSERT INTO `chat_note` VALUES ('14', '5', '3', '0', '帅', null, '', null, '2016-06-02 23:16:01', '1');
INSERT INTO `chat_note` VALUES ('15', '2', '0', '0', '#6', null, '', null, '2016-06-02 23:16:05', '1');
INSERT INTO `chat_note` VALUES ('16', '2', '0', '0', '#6#7', null, '', null, '2016-06-02 23:16:57', '1');
INSERT INTO `chat_note` VALUES ('17', '2', '0', '0', '#1', null, '', null, '2016-06-02 23:17:00', '1');
INSERT INTO `chat_note` VALUES ('18', '2', '0', '0', '#15', null, '', null, '2016-06-02 23:17:03', '1');
INSERT INTO `chat_note` VALUES ('19', '2', '3', '0', '#13', null, '', null, '2016-06-02 23:19:10', '1');
INSERT INTO `chat_note` VALUES ('20', '5', '3', '0', '古古怪怪', null, '', null, '2016-06-02 23:20:08', '1');
INSERT INTO `chat_note` VALUES ('21', '2', '3', '0', '让人', null, '', null, '2016-06-02 23:25:00', '1');
INSERT INTO `chat_note` VALUES ('22', '2', '3', '0', '灌灌灌灌', null, '', null, '2016-06-02 23:25:16', '1');
INSERT INTO `chat_note` VALUES ('23', '2', '3', '0', '#9', null, '', null, '2016-06-02 23:25:53', '1');
INSERT INTO `chat_note` VALUES ('24', '5', '3', '0', '啊啊啊啊', null, '', null, '2016-06-02 23:26:46', '1');
INSERT INTO `chat_note` VALUES ('25', '5', '3', '0', '担待点', null, '', null, '2016-06-02 23:27:22', '1');
INSERT INTO `chat_note` VALUES ('26', '5', '10000000', '0', '担待点', null, '', null, '2016-06-02 23:29:22', '1');
INSERT INTO `chat_note` VALUES ('27', '5', '10000000', '0', '凤飞飞', null, '', null, '2016-06-02 23:31:34', '1');
INSERT INTO `chat_note` VALUES ('28', '2', '3', '0', '嘎嘎嘎', null, '', null, '2016-06-02 23:31:56', '1');
INSERT INTO `chat_note` VALUES ('29', '2', '3', '0', '#21', null, '', null, '2016-06-02 23:32:15', '1');
INSERT INTO `chat_note` VALUES ('30', '2', '3', '0', '#7', null, '', null, '2016-06-02 23:37:20', '1');
INSERT INTO `chat_note` VALUES ('31', '5', '10000000', '0', '11111', null, '', null, '2016-06-02 23:38:24', '1');
INSERT INTO `chat_note` VALUES ('32', '5', '10000000', '0', '3333', null, '', null, '2016-06-02 23:39:07', '1');
INSERT INTO `chat_note` VALUES ('33', '2', '3', '0', '份饭', null, '', null, '2016-06-02 23:39:21', '1');
INSERT INTO `chat_note` VALUES ('34', '2', '3', '0', '江湖救急', null, '', null, '2016-06-02 23:42:08', '1');
INSERT INTO `chat_note` VALUES ('35', '2', '3', '0', '的人多', null, '', null, '2016-06-02 23:46:16', '1');
INSERT INTO `chat_note` VALUES ('36', '3', '3', '0', '反反复复', null, '', null, '2016-06-02 23:49:22', '1');
INSERT INTO `chat_note` VALUES ('37', '2', '3', '0', '乐扣乐扣', null, '', null, '2016-06-02 23:52:19', '1');
INSERT INTO `chat_note` VALUES ('38', '2', '3', '0', '方法', null, '', null, '2016-06-05 22:25:07', '1');
INSERT INTO `chat_note` VALUES ('39', '0', '3', '0', '方法纷纷扰扰', null, '', null, '2016-06-05 22:25:44', '1');
INSERT INTO `chat_note` VALUES ('40', '5', '8', '0', '#21 哈哈哈', null, '', null, '2016-06-07 23:01:00', '1');
INSERT INTO `chat_note` VALUES ('41', '5', '10000000', '0', '你好#8#4', null, '', null, '2016-06-07 23:01:20', '1');
INSERT INTO `chat_note` VALUES ('42', '2', '3', '0', '反反复复', null, '', null, '2016-06-09 17:00:28', '1');
INSERT INTO `chat_note` VALUES ('43', '2', '3', '0', '44', null, '', null, '2016-06-09 17:01:30', '1');
INSERT INTO `chat_note` VALUES ('44', '3', null, '0', 'ddd', null, '', null, '2016-06-10 14:10:24', '1');
INSERT INTO `chat_note` VALUES ('45', '3', null, '0', 'fjjfjfjf', null, '', null, '2016-06-10 14:18:58', '1');
INSERT INTO `chat_note` VALUES ('46', '1', '8', '0', '#21#6', null, '', null, '2016-06-10 14:25:19', '1');
INSERT INTO `chat_note` VALUES ('47', '2', '3', '0', 'eee', null, '', null, '2016-06-10 14:42:03', '1');
INSERT INTO `chat_note` VALUES ('48', '3', null, '0', 'ffffff', null, '', null, '2016-06-10 14:42:25', '1');
INSERT INTO `chat_note` VALUES ('49', '3', '2', '0', 'ddddd', '2', 'eee', '2016-06-10 14:42:03', '2016-06-10 14:43:50', '1');
INSERT INTO `chat_note` VALUES ('50', '2', '3', '0', '555555灌灌灌灌', null, '', null, '2016-06-11 15:13:22', '1');
INSERT INTO `chat_note` VALUES ('51', '7', '10000000', '0', 'ni\\', null, '', null, '2016-06-12 10:13:17', '1');
INSERT INTO `chat_note` VALUES ('52', '7', '10000000', '0', '#13', null, '', null, '2016-06-12 10:14:12', '1');
INSERT INTO `chat_note` VALUES ('53', '5', '10000000', '0', '啊啊啊啊啊啊啊', null, '', null, '2016-06-12 10:18:39', '1');
INSERT INTO `chat_note` VALUES ('54', '10000000', '5', '0', '怎么样了?', '5', '啊啊啊啊啊啊啊', '2016-06-12 10:18:39', '2016-06-12 10:19:46', '1');
INSERT INTO `chat_note` VALUES ('55', '10000000', '7', '0', '你好', '7', '#13', '2016-06-12 10:14:12', '2016-06-12 10:24:38', '1');
INSERT INTO `chat_note` VALUES ('56', '5', '10000000', '0', '#8', null, '', null, '2016-06-12 10:39:37', '1');
INSERT INTO `chat_note` VALUES ('57', '7', '10000000', '0', '你好', null, '', null, '2016-06-12 10:52:09', '1');
INSERT INTO `chat_note` VALUES ('58', '10000000', '7', '0', 'hsyudsahd', '7', '你好', '2016-06-12 10:52:09', '2016-06-12 10:52:42', '1');
INSERT INTO `chat_note` VALUES ('59', '5', '8', '0', 'fdsafdsafdsf', null, '', null, '2016-06-12 11:35:01', '1');
INSERT INTO `chat_note` VALUES ('60', '5', '10000000', '0', 'fdsafsafdsafdsf', null, '', null, '2016-06-12 11:35:08', '1');
INSERT INTO `chat_note` VALUES ('61', '3', '3', '0', '嘀嘀嘀', null, '', null, '2016-06-12 13:24:39', '1');
INSERT INTO `chat_note` VALUES ('62', '6', '3', '0', '事实上', null, '', null, '2016-06-12 13:25:12', '1');
INSERT INTO `chat_note` VALUES ('63', '6', '3', '0', '额鹅鹅鹅', null, '', null, '2016-06-12 13:25:40', '1');
INSERT INTO `chat_note` VALUES ('64', '6', '3', '0', '嗡嗡嗡', null, '', null, '2016-06-12 13:26:27', '1');
INSERT INTO `chat_note` VALUES ('65', '6', '3', '0', '发士大夫撒旦法', null, '', null, '2016-06-12 13:26:30', '1');
INSERT INTO `chat_note` VALUES ('66', '3', '6', '0', '方法', '6', '额鹅鹅鹅', '2016-06-12 13:25:40', '2016-06-12 13:26:35', '1');
INSERT INTO `chat_note` VALUES ('67', '3', '3', '0', '9999', '3', '方法', '1970-01-01 08:00:00', '2016-06-12 13:27:03', '1');
INSERT INTO `chat_note` VALUES ('68', '3', '3', '0', '999995方法', '3', '9999', '1970-01-01 08:00:00', '2016-06-12 13:27:30', '1');
INSERT INTO `chat_note` VALUES ('69', '3', '6', '0', '9999凤飞飞', '6', '发士大夫撒旦法', '2016-06-12 13:26:30', '2016-06-12 13:28:05', '1');
INSERT INTO `chat_note` VALUES ('70', '10000002', '10000000', '0', '#9', null, '', null, '2016-06-12 14:14:26', '1');
INSERT INTO `chat_note` VALUES ('71', '10000006', '10000000', '0', '#4', null, '', null, '2016-06-12 16:23:20', '1');
INSERT INTO `chat_note` VALUES ('72', '2', '8', '0', '55555', null, '', null, '2016-06-12 20:47:29', '1');
INSERT INTO `chat_note` VALUES ('73', '2', '3', '0', '3434', null, '', null, '2016-06-12 20:57:52', '1');
INSERT INTO `chat_note` VALUES ('74', '2', '3', '0', 'ddddd', null, '', null, '2016-06-12 21:01:43', '1');
INSERT INTO `chat_note` VALUES ('75', '2', '3', '0', '666', null, '', null, '2016-06-12 21:02:15', '1');
INSERT INTO `chat_note` VALUES ('76', '2', '3', '0', '000', null, '', null, '2016-06-12 21:03:04', '1');
INSERT INTO `chat_note` VALUES ('77', '2', '3', '0', '6666', null, '', null, '2016-06-12 21:04:32', '1');
INSERT INTO `chat_note` VALUES ('78', '2', '3', '0', '7777', null, '', null, '2016-06-12 21:05:23', '1');
INSERT INTO `chat_note` VALUES ('79', '2', '3', '0', 'tttt', null, '', null, '2016-06-12 21:08:15', '1');
INSERT INTO `chat_note` VALUES ('80', '2', '3', '0', 'tttttttttttttt', null, '', null, '2016-06-12 21:08:35', '1');
INSERT INTO `chat_note` VALUES ('81', '2', '3', '0', '6767', null, '', null, '2016-06-12 21:08:49', '1');
INSERT INTO `chat_note` VALUES ('82', '2', '3', '0', 'haha ', null, '', null, '2016-06-12 21:10:40', '1');
INSERT INTO `chat_note` VALUES ('83', '2', '3', '0', 'ddddd', null, '', null, '2016-06-12 21:10:53', '1');
INSERT INTO `chat_note` VALUES ('84', '6', '3', '0', 'hello', null, '', null, '2016-06-12 21:49:10', '1');
INSERT INTO `chat_note` VALUES ('85', '6', '3', '0', 'ok', null, '', null, '2016-06-12 21:50:21', '1');
INSERT INTO `chat_note` VALUES ('86', '3', '3', '0', '111', null, '', null, '2016-06-12 21:52:53', '1');
INSERT INTO `chat_note` VALUES ('87', '2', '3', '0', '反反复复', null, '', null, '2016-06-12 21:53:50', '1');
INSERT INTO `chat_note` VALUES ('88', '3', '3', '0', 'hello', null, '', null, '2016-06-12 21:55:28', '1');
INSERT INTO `chat_note` VALUES ('89', '3', '3', '0', 'ok', null, '', null, '2016-06-12 21:55:51', '1');
INSERT INTO `chat_note` VALUES ('90', '2', '3', '0', '滴答滴答', null, '', null, '2016-06-12 21:56:09', '1');
INSERT INTO `chat_note` VALUES ('91', '7', '3', '0', 'ok', null, '', null, '2016-06-12 21:57:23', '1');
INSERT INTO `chat_note` VALUES ('92', '7', '3', '0', 'hh', null, '', null, '2016-06-12 21:57:52', '1');
INSERT INTO `chat_note` VALUES ('93', '7', '3', '0', 'qqqq', null, '', null, '2016-06-12 21:57:57', '1');
INSERT INTO `chat_note` VALUES ('94', '7', '3', '0', 'qqqq', null, '', null, '2016-06-12 21:58:03', '1');
INSERT INTO `chat_note` VALUES ('95', '7', '3', '0', 'rrrrrr', null, '', null, '2016-06-12 21:58:09', '1');
INSERT INTO `chat_note` VALUES ('96', '7', '3', '0', '1234', null, '', null, '2016-06-12 21:58:32', '1');
INSERT INTO `chat_note` VALUES ('97', '2', '3', '0', '斤斤计较', null, '', null, '2016-06-12 21:59:31', '1');
INSERT INTO `chat_note` VALUES ('98', '7', '10000000', '0', 'hello', null, '', null, '2016-06-12 21:59:47', '1');
INSERT INTO `chat_note` VALUES ('99', '2', '3', '0', '灌灌灌灌', null, '', null, '2016-06-12 21:59:52', '1');
INSERT INTO `chat_note` VALUES ('100', '2', '3', '0', '费大幅度发', null, '', null, '2016-06-12 22:00:38', '1');
INSERT INTO `chat_note` VALUES ('101', '2', '3', '0', '辅导费方法', null, '', null, '2016-06-12 22:01:05', '1');
INSERT INTO `chat_note` VALUES ('102', '2', '3', '0', '他一天一天', null, '', null, '2016-06-12 22:02:16', '1');
INSERT INTO `chat_note` VALUES ('103', '2', '3', '0', '吞吞吐吐', null, '', null, '2016-06-12 22:02:30', '1');
INSERT INTO `chat_note` VALUES ('104', '2', '3', '0', '嘎嘎嘎', null, '', null, '2016-06-12 22:02:45', '1');
INSERT INTO `chat_note` VALUES ('105', '2', '3', '0', '他共同体', null, '', null, '2016-06-12 22:03:03', '1');
INSERT INTO `chat_note` VALUES ('106', '2', '3', '0', '广告', null, '', null, '2016-06-12 22:03:11', '1');
INSERT INTO `chat_note` VALUES ('107', '2', '3', '0', '饭方法', null, '', null, '2016-06-12 22:12:10', '1');
INSERT INTO `chat_note` VALUES ('108', '2', '3', '0', '顶顶顶', null, '', null, '2016-06-12 22:12:20', '1');
INSERT INTO `chat_note` VALUES ('109', '2', '3', '0', '东方大道', null, '', null, '2016-06-12 22:12:32', '1');
INSERT INTO `chat_note` VALUES ('110', '7', '10000000', '0', 'ooo', null, '', null, '2016-06-12 22:13:20', '1');
INSERT INTO `chat_note` VALUES ('111', '10000000', '7', '0', ' hello', '7', 'ooo', '2016-06-12 22:13:20', '2016-06-12 22:13:43', '1');
INSERT INTO `chat_note` VALUES ('112', '0', '10000000', '0', '#9', null, '', null, '2016-06-13 11:38:35', '1');
INSERT INTO `chat_note` VALUES ('113', '6', '8', '0', '老师今天什么盘？', null, '', null, '2016-06-15 13:42:20', '1');
INSERT INTO `chat_note` VALUES ('114', '10000109', '10000000', '0', '老师6009637块3买进！2万股！求老师看看#9#5#6', null, '', null, '2016-06-22 11:18:55', '1');
INSERT INTO `chat_note` VALUES ('115', '10000109', '10000000', '0', '额、、、是600963', null, '', null, '2016-06-22 11:19:16', '1');
INSERT INTO `chat_note` VALUES ('116', '10000088', '10000001', '0', '中分哥 威武\n', null, '', null, '2016-06-22 11:20:26', '1');
INSERT INTO `chat_note` VALUES ('117', '10000000', '10000109', '0', '昨天收大阳，看看能否突破6.4，突破才能继续往上', '10000109', '额、、、是600963', '2016-06-22 11:19:16', '2016-06-22 13:26:14', '1');
INSERT INTO `chat_note` VALUES ('118', '10000087', '10000000', '0', '#20', null, '', null, '2016-06-22 13:49:18', '1');
INSERT INTO `chat_note` VALUES ('119', '10000084', '10000000', '0', '老师，对股市2016年的后势您怎么看？', null, '', null, '2016-06-22 13:51:27', '1');
INSERT INTO `chat_note` VALUES ('120', '10000000', '10000087', '0', '谢谢\n', '10000087', '#20', '2016-06-22 13:49:18', '2016-06-22 14:20:00', '1');
INSERT INTO `chat_note` VALUES ('121', '10000000', '10000084', '0', '经历了这次的猛烈下跌后，市场信心明显缺失。中国经济也经历着L型走势。所以下半年还是以低位徘徊，震荡走弱的格局为主\n', '10000084', '老师，对股市2016年的后势您怎么看？', '2016-06-22 13:51:27', '2016-06-22 14:21:23', '1');
INSERT INTO `chat_note` VALUES ('122', '10000109', '8', '0', '300277,10.5,2万股，满仓！   000788,12.7,2000股，2成仓！\n	\n\n000670空仓观望\n老师帮忙看看', null, '', null, '2016-06-23 20:23:08', '1');
INSERT INTO `chat_note` VALUES ('123', '10000160', '10000001', '0', '转多单了吗', null, '', null, '2016-06-23 21:08:38', '1');
INSERT INTO `chat_note` VALUES ('124', '10000121', '10000000', '0', '老师，现在油怎么操作？看多还是看空？', null, '', null, '2016-06-24 10:17:04', '1');
INSERT INTO `chat_note` VALUES ('125', '10000000', '10000121', '0', '现在建议还是先等结果\n', '10000121', '老师，现在油怎么操作？看多还是看空？', '2016-06-24 10:17:04', '2016-06-24 10:58:33', '1');
INSERT INTO `chat_note` VALUES ('126', '10000160', '10000000', '0', '这里黎明静悄悄啊', null, '', null, '2016-06-24 19:40:00', '1');
INSERT INTO `chat_note` VALUES ('127', '10000194', '8', '0', '老师你好现在可以补一点空单吗我是313的单', null, '', null, '2016-06-24 22:40:22', '1');
INSERT INTO `chat_note` VALUES ('128', '8', '10000194', '0', '趋势是空头，加仓是可以的，但是注意控制仓位。', '10000194', '老师你好现在可以补一点空单吗我是313的单', '2016-06-24 22:40:22', '2016-06-27 13:01:51', '1');
INSERT INTO `chat_note` VALUES ('129', '10000000', '10000160', '0', '#4', '10000160', '这里黎明静悄悄啊', '2016-06-24 19:40:00', '2016-06-27 16:28:41', '1');
INSERT INTO `chat_note` VALUES ('130', '10000001', '10000160', '0', '目前处于空头形势', '10000160', '转多单了吗', '2016-06-23 21:08:38', '2016-06-27 18:34:54', '1');
INSERT INTO `chat_note` VALUES ('131', '10000170', '10000001', '0', '无论如何结果要接受，但不知后面会不会有其他中性的言论', null, '', null, '2016-06-27 22:58:03', '1');
INSERT INTO `chat_note` VALUES ('132', '10000170', '10000001', '0', '标普500继续下挫，行情还有延续吧', null, '', null, '2016-06-27 23:01:47', '1');
INSERT INTO `chat_note` VALUES ('133', '10000170', '10000001', '0', '国家安全那就是大大利空喽！老师', null, '', null, '2016-06-27 23:03:13', '1');
INSERT INTO `chat_note` VALUES ('134', '10000170', '10000001', '0', '降息利好原油么', null, '', null, '2016-06-27 23:11:45', '1');
INSERT INTO `chat_note` VALUES ('135', '10000170', '10000001', '0', '短暂利好', null, '', null, '2016-06-27 23:12:56', '1');
INSERT INTO `chat_note` VALUES ('136', '10000001', '10000170', '0', '这个没法猜测#9', '10000170', '无论如何结果要接受，但不知后面会不会有其他中性的言论', '2016-06-27 22:58:03', '2016-06-27 23:33:32', '1');
INSERT INTO `chat_note` VALUES ('137', '10000001', '10000170', '0', '英国退欧后，对股市的影响还会延续#9', '10000170', '标普500继续下挫，行情还有延续吧', '2016-06-27 23:01:47', '2016-06-27 23:35:48', '1');
INSERT INTO `chat_note` VALUES ('138', '10000001', '10000170', '0', '如果英国真的降息，将会稳定英国退欧后面临的经济下行压力，从而长远上看，利多油价', '10000170', '降息利好原油么', '2016-06-27 23:11:45', '2016-06-27 23:41:30', '1');
INSERT INTO `chat_note` VALUES ('139', '10000001', '10000170', '0', '是的', '10000170', '国家安全那就是大大利空喽！老师', '2016-06-27 23:03:13', '2016-06-27 23:57:39', '1');
INSERT INTO `chat_note` VALUES ('140', '10000170', '8', '0', '老师我有通化东宝，后期走势如何', null, '', null, '2016-06-28 12:24:36', '1');
INSERT INTO `chat_note` VALUES ('141', '10000232', '10000001', '0', '老师，股票300367你怎么看？这星期会往上吗？', null, '', null, '2016-06-28 20:28:57', '1');
INSERT INTO `chat_note` VALUES ('142', '10000001', '10000232', '0', '稍等哦#9', '10000232', '老师，股票300367你怎么看？这星期会往上吗？', '2016-06-28 20:28:57', '2016-06-28 20:40:02', '1');
INSERT INTO `chat_note` VALUES ('143', '10000001', '10000232', '0', '这只股压力位27.1，支撑23 ，今日这只股已经走到了压力位附近，上涨空间不大啊，加上盘一直在2900点压力位附近震荡，加上英国退欧引起全球股市下跌，所以这只股想突破压力位上涨几率很少，短期会下降', '10000232', '老师，股票300367你怎么看？这星期会往上吗？', '2016-06-28 20:28:57', '2016-06-28 21:02:14', '1');
INSERT INTO `chat_note` VALUES ('144', '10000185', '10000001', '0', '老师；空单312没平仓，今晚还能平吗？', null, '', null, '2016-06-28 23:45:07', '1');
INSERT INTO `chat_note` VALUES ('145', '10000001', '10000185', '0', '能的，现在还处于空头形势', '10000185', '老师；空单312没平仓，今晚还能平吗？', '2016-06-28 23:45:07', '2016-06-29 00:20:41', '1');
INSERT INTO `chat_note` VALUES ('146', '10000185', '10000001', '0', '好的。谢谢老师，辛苦了#9', null, '', null, '2016-06-29 00:27:27', '1');
INSERT INTO `chat_note` VALUES ('147', '10000001', '10000185', '0', '#9', '10000185', '好的。谢谢老师，辛苦了#9', '2016-06-29 00:27:27', '2016-06-29 00:30:32', '1');
INSERT INTO `chat_note` VALUES ('148', '10000185', '8', '0', '老师您好:麻烦您帮忙看一下股票，爱尔眼科300015，成本35元，后市怎么操作？还能继续持有吗？还有600879航天电子？', null, '', null, '2016-06-30 10:47:15', '1');
INSERT INTO `chat_note` VALUES ('149', '8', '10000170', '0', '短期的强势行情可能结束，及时短线卖出、离场观望为宜。\n', '10000170', '老师我有通化东宝，后期走势如何', '2016-06-28 12:24:36', '2016-06-30 17:38:10', '1');
INSERT INTO `chat_note` VALUES ('150', '8', '10000185', '0', '300015股价的强势特征已经确立，短线可能回调，建议可先获利减仓或离场。600879市场最近连续上涨，短期上方遇阻，小心回调。中期有加速上涨趋势。\n\n', '10000185', '老师您好:麻烦您帮忙看一下股票，爱尔眼科300015，成本35元，后市怎么操作？还能继续持有吗？还有600879航天电子？', '2016-06-30 10:47:15', '2016-06-30 17:41:56', '1');
INSERT INTO `chat_note` VALUES ('151', '10000185', '8', '0', '#15谢谢伍老师的帮忙分析。#9', null, '', null, '2016-06-30 19:11:44', '1');
INSERT INTO `chat_note` VALUES ('152', '10000037', '10000001', '0', '单子还要持有吗\n', null, '', null, '2016-06-30 19:47:20', '1');
INSERT INTO `chat_note` VALUES ('153', '10000001', '10000037', '0', '最好获利先出#9', '10000037', '单子还要持有吗\n', '2016-06-30 19:47:20', '2016-06-30 21:48:11', '1');
INSERT INTO `chat_note` VALUES ('154', '10000194', '10000001', '0', '需要割了吗，还是能等#2', null, '', null, '2016-06-30 23:01:24', '1');
INSERT INTO `chat_note` VALUES ('155', '10000001', '10000194', '0', '还是那句：耐心#9', '10000194', '需要割了吗，还是能等#2', '2016-06-30 23:01:24', '2016-06-30 23:58:45', '1');
INSERT INTO `chat_note` VALUES ('156', '10000185', '10000001', '0', '老师我手里有2手318.8的空单，我持有了好几天了，没有及时止损，现在不知道该怎么做了，麻烦老师指点一下。万分感谢！', null, '', null, '2016-07-01 00:59:39', '1');
INSERT INTO `chat_note` VALUES ('157', '10000001', null, '0', '原本今晚欧洲央行的利率会议纪要是有机会平的，但突然说延迟到下周四才公布，那么预测是利空油价的，可以先锁仓一半，减少亏损，下周很大几率有机会平掉的（个人建议，仅供参考）', null, null, null, '2016-07-01 01:06:44', '1');
INSERT INTO `chat_note` VALUES ('158', '10000185', '10000001', '0', '看来我的问题没希望了', null, '', null, '2016-07-01 01:34:09', '1');
INSERT INTO `chat_note` VALUES ('159', '10000185', '10000001', '0', '#9', null, '', null, '2016-07-01 01:34:24', '1');
INSERT INTO `chat_note` VALUES ('160', '8', '10000185', '0', '不客气#15', '10000185', '#15谢谢伍老师的帮忙分析。#9', '2016-06-30 19:11:44', '2016-07-01 09:11:36', '1');
INSERT INTO `chat_note` VALUES ('161', '10000194', '10000001', '0', '我321。4的单   我下午五点没下班就建的了', null, '', null, '2016-07-01 20:09:07', '1');
INSERT INTO `chat_note` VALUES ('162', '10000001', '10000194', '0', '#9', '10000194', '我321。4的单   我下午五点没下班就建的了', '2016-07-01 20:09:07', '2016-07-01 22:03:19', '1');
INSERT INTO `chat_note` VALUES ('163', '10000194', '10000001', '0', '#4老师不听话叫你聊天到325了嘛。你不陪聊#4', null, '', null, '2016-07-01 22:19:08', '1');
INSERT INTO `chat_note` VALUES ('164', '10000194', '10000001', '0', '老师问个问题，直播里面不可以说其他无关的话吗？说话太多会被移出来的吗？', null, '', null, '2016-07-01 22:36:55', '1');
INSERT INTO `chat_note` VALUES ('165', '10000194', '10000001', '0', '不敢说话了，人家不喜欢我说话#2', null, '', null, '2016-07-01 23:08:07', '1');
INSERT INTO `chat_note` VALUES ('166', '10000001', '10000194', '0', '#12', '10000194', '#4老师不听话叫你聊天到325了嘛。你不陪聊#4', '2016-07-01 22:19:08', '2016-07-02 00:56:18', '1');
INSERT INTO `chat_note` VALUES ('167', '10000001', '10000194', '0', '可以啊，不要传达负面情绪就行喔#9', '10000194', '老师问个问题，直播里面不可以说其他无关的话吗？说话太多会被移出来的吗？', '2016-07-01 22:36:55', '2016-07-02 00:56:54', '1');
INSERT INTO `chat_note` VALUES ('168', '10000001', '10000194', '0', '没有啦，很喜欢你说话，哈哈#9', '10000194', '不敢说话了，人家不喜欢我说话#2', '2016-07-01 23:08:07', '2016-07-02 00:57:23', '1');
INSERT INTO `chat_note` VALUES ('169', '10000093', '8', '0', '老师您好，今天什么板块比较活跃', null, '', null, '2016-07-07 10:16:14', '1');
INSERT INTO `chat_note` VALUES ('170', '10000274', '8', '0', '伍老师好！原油有布局吗', null, '', null, '2016-07-07 15:30:52', '1');
INSERT INTO `chat_note` VALUES ('171', '10000274', '10000000', '0', '郑老师，早！油今天是高空策略吗？', null, '', null, '2016-07-08 09:52:53', '1');

-- ----------------------------
-- Table structure for chat_oauth_user
-- ----------------------------
DROP TABLE IF EXISTS `chat_oauth_user`;
CREATE TABLE `chat_oauth_user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `from` varchar(20) NOT NULL COMMENT '用户来源key',
  `name` varchar(30) NOT NULL COMMENT '第三方昵称',
  `head_img` varchar(200) NOT NULL COMMENT '头像',
  `uid` int(20) NOT NULL COMMENT '关联的本站用户id',
  `create_time` datetime NOT NULL COMMENT '绑定时间',
  `last_login_time` datetime NOT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(16) NOT NULL COMMENT '最后登录ip',
  `login_times` int(6) NOT NULL COMMENT '登录次数',
  `status` tinyint(2) NOT NULL,
  `access_token` varchar(512) NOT NULL,
  `expires_date` int(11) NOT NULL COMMENT 'access_token过期时间',
  `openid` varchar(40) NOT NULL COMMENT '第三方用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='第三方用户表';

-- ----------------------------
-- Records of chat_oauth_user
-- ----------------------------

-- ----------------------------
-- Table structure for chat_options
-- ----------------------------
DROP TABLE IF EXISTS `chat_options`;
CREATE TABLE `chat_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL COMMENT '配置名',
  `option_value` longtext NOT NULL COMMENT '配置值',
  `autoload` int(2) NOT NULL DEFAULT '1' COMMENT '是否自动加载',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='全站配置表';

-- ----------------------------
-- Records of chat_options
-- ----------------------------
INSERT INTO `chat_options` VALUES ('1', 'member_email_active', '{\"title\":\"ThinkCMF\\u90ae\\u4ef6\\u6fc0\\u6d3b\\u901a\\u77e5.\",\"template\":\"<p>\\u672c\\u90ae\\u4ef6\\u6765\\u81ea<a href=\\\"http:\\/\\/www.thinkcmf.com\\\">ThinkCMF<\\/a><br\\/><br\\/>&nbsp; &nbsp;<strong>---------------<\\/strong><br\\/>&nbsp; &nbsp;<strong>\\u5e10\\u53f7\\u6fc0\\u6d3b\\u8bf4\\u660e<\\/strong><br\\/>&nbsp; &nbsp;<strong>---------------<\\/strong><br\\/><br\\/>&nbsp; &nbsp; \\u5c0a\\u656c\\u7684<span style=\\\"FONT-SIZE: 16px; FONT-FAMILY: Arial; COLOR: rgb(51,51,51); LINE-HEIGHT: 18px; BACKGROUND-COLOR: rgb(255,255,255)\\\">#username#\\uff0c\\u60a8\\u597d\\u3002<\\/span>\\u5982\\u679c\\u60a8\\u662fThinkCMF\\u7684\\u65b0\\u7528\\u6237\\uff0c\\u6216\\u5728\\u4fee\\u6539\\u60a8\\u7684\\u6ce8\\u518cEmail\\u65f6\\u4f7f\\u7528\\u4e86\\u672c\\u5730\\u5740\\uff0c\\u6211\\u4eec\\u9700\\u8981\\u5bf9\\u60a8\\u7684\\u5730\\u5740\\u6709\\u6548\\u6027\\u8fdb\\u884c\\u9a8c\\u8bc1\\u4ee5\\u907f\\u514d\\u5783\\u573e\\u90ae\\u4ef6\\u6216\\u5730\\u5740\\u88ab\\u6ee5\\u7528\\u3002<br\\/>&nbsp; &nbsp; \\u60a8\\u53ea\\u9700\\u70b9\\u51fb\\u4e0b\\u9762\\u7684\\u94fe\\u63a5\\u5373\\u53ef\\u6fc0\\u6d3b\\u60a8\\u7684\\u5e10\\u53f7\\uff1a<br\\/>&nbsp; &nbsp; <a title=\\\"\\\" href=\\\"http:\\/\\/#link#\\\" target=\\\"_self\\\">http:\\/\\/#link#<\\/a><br\\/>&nbsp; &nbsp; (\\u5982\\u679c\\u4e0a\\u9762\\u4e0d\\u662f\\u94fe\\u63a5\\u5f62\\u5f0f\\uff0c\\u8bf7\\u5c06\\u8be5\\u5730\\u5740\\u624b\\u5de5\\u7c98\\u8d34\\u5230\\u6d4f\\u89c8\\u5668\\u5730\\u5740\\u680f\\u518d\\u8bbf\\u95ee)<br\\/>&nbsp; &nbsp; \\u611f\\u8c22\\u60a8\\u7684\\u8bbf\\u95ee\\uff0c\\u795d\\u60a8\\u4f7f\\u7528\\u6109\\u5feb\\uff01<br\\/><br\\/>&nbsp; &nbsp; \\u6b64\\u81f4<br\\/>&nbsp; &nbsp; ThinkCMF \\u7ba1\\u7406\\u56e2\\u961f.<\\/p>\"}', '1');
INSERT INTO `chat_options` VALUES ('2', 'site_options', '{\"site_name\":\"\\u8f69\\u6e5b\\u76f4\\u64ad\\u5ba4\",\"site_host\":\"http:\\/\\/testim.lityou.com\\/\",\"site_admin_url_password\":\"\",\"site_tpl\":\"simplebootx\",\"site_adminstyle\":\"bluesky\",\"site_icp\":\"\",\"site_admin_email\":\"shl3807@qq.com\",\"site_tongji\":\"\",\"site_copyright\":\"\",\"site_seo_title\":\"\\u76f4\\u64ad\\u5ba4\",\"site_seo_keywords\":\"\\u76f4\\u64ad\\u5ba4\",\"site_seo_description\":\"\\u76f4\\u64ad\\u5ba4\",\"urlmode\":\"0\",\"html_suffix\":\"\",\"comment_time_interval\":60}', '1');
INSERT INTO `chat_options` VALUES ('3', 'cmf_settings', '{\"banned_usernames\":\"\"}', '1');

-- ----------------------------
-- Table structure for chat_plugins
-- ----------------------------
DROP TABLE IF EXISTS `chat_plugins`;
CREATE TABLE `chat_plugins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `name` varchar(50) NOT NULL COMMENT '插件名，英文',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '插件名称',
  `description` text COMMENT '插件描述',
  `type` tinyint(2) DEFAULT '0' COMMENT '插件类型, 1:网站；8;微信',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态；1开启；',
  `config` text COMMENT '插件配置',
  `hooks` varchar(255) DEFAULT NULL COMMENT '实现的钩子;以“，”分隔',
  `has_admin` tinyint(2) DEFAULT '0' COMMENT '插件是否有后台管理界面',
  `author` varchar(50) DEFAULT '' COMMENT '插件作者',
  `version` varchar(20) DEFAULT '' COMMENT '插件版本号',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '插件安装时间',
  `listorder` smallint(6) NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='插件表';

-- ----------------------------
-- Records of chat_plugins
-- ----------------------------

-- ----------------------------
-- Table structure for chat_posts
-- ----------------------------
DROP TABLE IF EXISTS `chat_posts`;
CREATE TABLE `chat_posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned DEFAULT '0' COMMENT '发表者id',
  `post_keywords` varchar(150) NOT NULL COMMENT 'seo keywords',
  `post_source` varchar(150) DEFAULT NULL COMMENT '转载文章的来源',
  `post_date` datetime DEFAULT '2000-01-01 00:00:00' COMMENT 'post创建日期，永久不变，一般不显示给用户',
  `post_content` longtext COMMENT 'post内容',
  `post_title` text COMMENT 'post标题',
  `post_excerpt` text COMMENT 'post摘要',
  `post_status` int(2) DEFAULT '1' COMMENT 'post状态，1已审核，0未审核',
  `comment_status` int(2) DEFAULT '1' COMMENT '评论状态，1允许，0不允许',
  `post_modified` datetime DEFAULT '2000-01-01 00:00:00' COMMENT 'post更新时间，可在前台修改，显示给用户',
  `post_content_filtered` longtext,
  `post_parent` bigint(20) unsigned DEFAULT '0' COMMENT 'post的父级post id,表示post层级关系',
  `post_type` int(2) DEFAULT NULL,
  `post_mime_type` varchar(100) DEFAULT '',
  `comment_count` bigint(20) DEFAULT '0',
  `smeta` text COMMENT 'post的扩展字段，保存相关扩展属性，如缩略图；格式为json',
  `post_hits` int(11) DEFAULT '0' COMMENT 'post点击数，查看数',
  `post_like` int(11) DEFAULT '0' COMMENT 'post赞数',
  `istop` tinyint(1) NOT NULL DEFAULT '0' COMMENT '置顶 1置顶； 0不置顶',
  `recommended` tinyint(1) NOT NULL DEFAULT '0' COMMENT '推荐 1推荐 0不推荐',
  PRIMARY KEY (`id`),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`id`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`),
  KEY `post_date` (`post_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Portal文章表';

-- ----------------------------
-- Records of chat_posts
-- ----------------------------

-- ----------------------------
-- Table structure for chat_role
-- ----------------------------
DROP TABLE IF EXISTS `chat_role`;
CREATE TABLE `chat_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '角色名称',
  `pid` smallint(6) DEFAULT NULL COMMENT '父角色ID',
  `status` tinyint(1) unsigned DEFAULT NULL COMMENT '状态',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `listorder` int(3) NOT NULL DEFAULT '0' COMMENT '排序字段',
  PRIMARY KEY (`id`),
  KEY `parentId` (`pid`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色表';

-- ----------------------------
-- Records of chat_role
-- ----------------------------
INSERT INTO `chat_role` VALUES ('1', '超级管理员', '0', '1', '拥有网站最高管理员权限！', '1329633709', '1329633709', '0');
INSERT INTO `chat_role` VALUES ('2', '老师', null, '1', '老师', '1464078052', '0', '0');
INSERT INTO `chat_role` VALUES ('3', '前台普通用户', null, '1', '前台普通用户', '1464511576', '0', '0');

-- ----------------------------
-- Table structure for chat_role_user
-- ----------------------------
DROP TABLE IF EXISTS `chat_role_user`;
CREATE TABLE `chat_role_user` (
  `role_id` int(11) unsigned DEFAULT '0' COMMENT '角色 id',
  `user_id` int(11) DEFAULT '0' COMMENT '用户id',
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户角色对应表';

-- ----------------------------
-- Records of chat_role_user
-- ----------------------------
INSERT INTO `chat_role_user` VALUES ('3', '10000016');
INSERT INTO `chat_role_user` VALUES ('3', '7');
INSERT INTO `chat_role_user` VALUES ('2', '8');
INSERT INTO `chat_role_user` VALUES ('2', '10000001');
INSERT INTO `chat_role_user` VALUES ('3', '10000002');
INSERT INTO `chat_role_user` VALUES ('3', '10000003');
INSERT INTO `chat_role_user` VALUES ('3', '10000004');
INSERT INTO `chat_role_user` VALUES ('3', '10000005');
INSERT INTO `chat_role_user` VALUES ('3', '10000006');
INSERT INTO `chat_role_user` VALUES ('3', '10000007');
INSERT INTO `chat_role_user` VALUES ('3', '10000008');
INSERT INTO `chat_role_user` VALUES ('3', '10000009');
INSERT INTO `chat_role_user` VALUES ('3', '10000010');
INSERT INTO `chat_role_user` VALUES ('3', '10000011');
INSERT INTO `chat_role_user` VALUES ('3', '10000012');
INSERT INTO `chat_role_user` VALUES ('3', '10000013');
INSERT INTO `chat_role_user` VALUES ('3', '10000014');
INSERT INTO `chat_role_user` VALUES ('3', '10000015');
INSERT INTO `chat_role_user` VALUES ('3', '10000017');
INSERT INTO `chat_role_user` VALUES ('3', '10000018');
INSERT INTO `chat_role_user` VALUES ('3', '10000019');
INSERT INTO `chat_role_user` VALUES ('3', '10000020');
INSERT INTO `chat_role_user` VALUES ('3', '10000021');
INSERT INTO `chat_role_user` VALUES ('3', '10000022');
INSERT INTO `chat_role_user` VALUES ('3', '10000023');
INSERT INTO `chat_role_user` VALUES ('3', '10000024');
INSERT INTO `chat_role_user` VALUES ('3', '10000025');
INSERT INTO `chat_role_user` VALUES ('3', '10000026');
INSERT INTO `chat_role_user` VALUES ('3', '10000027');
INSERT INTO `chat_role_user` VALUES ('3', '10000028');
INSERT INTO `chat_role_user` VALUES ('3', '10000029');
INSERT INTO `chat_role_user` VALUES ('3', '10000030');
INSERT INTO `chat_role_user` VALUES ('3', '10000031');
INSERT INTO `chat_role_user` VALUES ('3', '10000032');
INSERT INTO `chat_role_user` VALUES ('3', '10000033');
INSERT INTO `chat_role_user` VALUES ('3', '10000034');
INSERT INTO `chat_role_user` VALUES ('3', '10000035');
INSERT INTO `chat_role_user` VALUES ('3', '10000036');
INSERT INTO `chat_role_user` VALUES ('3', '10000037');
INSERT INTO `chat_role_user` VALUES ('3', '10000038');
INSERT INTO `chat_role_user` VALUES ('3', '10000039');
INSERT INTO `chat_role_user` VALUES ('3', '10000040');
INSERT INTO `chat_role_user` VALUES ('3', '10000041');
INSERT INTO `chat_role_user` VALUES ('3', '10000042');
INSERT INTO `chat_role_user` VALUES ('3', '10000043');
INSERT INTO `chat_role_user` VALUES ('3', '10000044');
INSERT INTO `chat_role_user` VALUES ('3', '10000045');
INSERT INTO `chat_role_user` VALUES ('3', '10000046');
INSERT INTO `chat_role_user` VALUES ('3', '10000047');
INSERT INTO `chat_role_user` VALUES ('3', '10000048');
INSERT INTO `chat_role_user` VALUES ('3', '10000049');
INSERT INTO `chat_role_user` VALUES ('3', '10000050');
INSERT INTO `chat_role_user` VALUES ('3', '10000051');
INSERT INTO `chat_role_user` VALUES ('3', '10000052');
INSERT INTO `chat_role_user` VALUES ('3', '10000053');
INSERT INTO `chat_role_user` VALUES ('3', '10000054');
INSERT INTO `chat_role_user` VALUES ('3', '10000055');
INSERT INTO `chat_role_user` VALUES ('3', '10000056');
INSERT INTO `chat_role_user` VALUES ('3', '10000057');
INSERT INTO `chat_role_user` VALUES ('3', '10000058');
INSERT INTO `chat_role_user` VALUES ('3', '10000059');
INSERT INTO `chat_role_user` VALUES ('3', '10000060');
INSERT INTO `chat_role_user` VALUES ('3', '10000061');
INSERT INTO `chat_role_user` VALUES ('3', '10000062');
INSERT INTO `chat_role_user` VALUES ('3', '10000063');
INSERT INTO `chat_role_user` VALUES ('3', '10000064');
INSERT INTO `chat_role_user` VALUES ('3', '10000065');
INSERT INTO `chat_role_user` VALUES ('3', '10000066');
INSERT INTO `chat_role_user` VALUES ('3', '10000067');
INSERT INTO `chat_role_user` VALUES ('3', '10000068');
INSERT INTO `chat_role_user` VALUES ('3', '10000069');
INSERT INTO `chat_role_user` VALUES ('3', '10000070');
INSERT INTO `chat_role_user` VALUES ('3', '10000071');
INSERT INTO `chat_role_user` VALUES ('3', '10000072');
INSERT INTO `chat_role_user` VALUES ('3', '10000073');
INSERT INTO `chat_role_user` VALUES ('3', '10000074');
INSERT INTO `chat_role_user` VALUES ('3', '10000075');
INSERT INTO `chat_role_user` VALUES ('3', '10000076');
INSERT INTO `chat_role_user` VALUES ('3', '10000077');
INSERT INTO `chat_role_user` VALUES ('3', '10000078');
INSERT INTO `chat_role_user` VALUES ('3', '10000079');
INSERT INTO `chat_role_user` VALUES ('3', '10000080');
INSERT INTO `chat_role_user` VALUES ('3', '10000081');
INSERT INTO `chat_role_user` VALUES ('3', '10000082');
INSERT INTO `chat_role_user` VALUES ('3', '10000083');
INSERT INTO `chat_role_user` VALUES ('3', '10000084');
INSERT INTO `chat_role_user` VALUES ('3', '10000085');
INSERT INTO `chat_role_user` VALUES ('3', '10000086');
INSERT INTO `chat_role_user` VALUES ('3', '10000087');
INSERT INTO `chat_role_user` VALUES ('3', '10000088');
INSERT INTO `chat_role_user` VALUES ('3', '10000089');
INSERT INTO `chat_role_user` VALUES ('3', '10000090');
INSERT INTO `chat_role_user` VALUES ('3', '10000091');
INSERT INTO `chat_role_user` VALUES ('3', '10000092');
INSERT INTO `chat_role_user` VALUES ('3', '10000093');
INSERT INTO `chat_role_user` VALUES ('3', '10000094');
INSERT INTO `chat_role_user` VALUES ('3', '10000095');
INSERT INTO `chat_role_user` VALUES ('3', '10000096');
INSERT INTO `chat_role_user` VALUES ('3', '10000097');
INSERT INTO `chat_role_user` VALUES ('3', '10000098');
INSERT INTO `chat_role_user` VALUES ('3', '10000099');
INSERT INTO `chat_role_user` VALUES ('3', '10000100');
INSERT INTO `chat_role_user` VALUES ('3', '10000101');
INSERT INTO `chat_role_user` VALUES ('3', '10000102');
INSERT INTO `chat_role_user` VALUES ('2', '10000103');
INSERT INTO `chat_role_user` VALUES ('3', '10000104');
INSERT INTO `chat_role_user` VALUES ('3', '10000105');
INSERT INTO `chat_role_user` VALUES ('3', '10000106');
INSERT INTO `chat_role_user` VALUES ('3', '10000107');
INSERT INTO `chat_role_user` VALUES ('3', '10000108');
INSERT INTO `chat_role_user` VALUES ('3', '10000109');
INSERT INTO `chat_role_user` VALUES ('3', '10000110');
INSERT INTO `chat_role_user` VALUES ('3', '10000111');
INSERT INTO `chat_role_user` VALUES ('3', '10000112');
INSERT INTO `chat_role_user` VALUES ('3', '10000113');
INSERT INTO `chat_role_user` VALUES ('3', '10000114');
INSERT INTO `chat_role_user` VALUES ('3', '10000115');
INSERT INTO `chat_role_user` VALUES ('3', '10000116');
INSERT INTO `chat_role_user` VALUES ('3', '10000117');
INSERT INTO `chat_role_user` VALUES ('3', '10000118');
INSERT INTO `chat_role_user` VALUES ('3', '10000119');
INSERT INTO `chat_role_user` VALUES ('3', '10000120');
INSERT INTO `chat_role_user` VALUES ('3', '10000121');
INSERT INTO `chat_role_user` VALUES ('3', '10000122');
INSERT INTO `chat_role_user` VALUES ('3', '10000123');
INSERT INTO `chat_role_user` VALUES ('3', '10000124');
INSERT INTO `chat_role_user` VALUES ('3', '10000125');
INSERT INTO `chat_role_user` VALUES ('3', '10000126');
INSERT INTO `chat_role_user` VALUES ('3', '10000127');
INSERT INTO `chat_role_user` VALUES ('3', '10000128');
INSERT INTO `chat_role_user` VALUES ('3', '10000129');
INSERT INTO `chat_role_user` VALUES ('3', '10000130');
INSERT INTO `chat_role_user` VALUES ('3', '10000131');
INSERT INTO `chat_role_user` VALUES ('3', '10000132');
INSERT INTO `chat_role_user` VALUES ('3', '10000133');
INSERT INTO `chat_role_user` VALUES ('3', '10000134');
INSERT INTO `chat_role_user` VALUES ('3', '10000135');
INSERT INTO `chat_role_user` VALUES ('3', '10000136');
INSERT INTO `chat_role_user` VALUES ('3', '10000137');
INSERT INTO `chat_role_user` VALUES ('3', '10000138');
INSERT INTO `chat_role_user` VALUES ('3', '10000139');
INSERT INTO `chat_role_user` VALUES ('3', '10000140');
INSERT INTO `chat_role_user` VALUES ('3', '10000141');
INSERT INTO `chat_role_user` VALUES ('3', '10000142');
INSERT INTO `chat_role_user` VALUES ('3', '10000143');
INSERT INTO `chat_role_user` VALUES ('3', '10000144');
INSERT INTO `chat_role_user` VALUES ('3', '10000145');
INSERT INTO `chat_role_user` VALUES ('3', '10000146');
INSERT INTO `chat_role_user` VALUES ('3', '10000147');
INSERT INTO `chat_role_user` VALUES ('3', '10000148');
INSERT INTO `chat_role_user` VALUES ('3', '10000149');
INSERT INTO `chat_role_user` VALUES ('3', '10000150');
INSERT INTO `chat_role_user` VALUES ('3', '10000151');
INSERT INTO `chat_role_user` VALUES ('3', '10000152');
INSERT INTO `chat_role_user` VALUES ('3', '10000153');
INSERT INTO `chat_role_user` VALUES ('3', '10000154');
INSERT INTO `chat_role_user` VALUES ('3', '10000155');
INSERT INTO `chat_role_user` VALUES ('3', '10000156');
INSERT INTO `chat_role_user` VALUES ('3', '10000157');
INSERT INTO `chat_role_user` VALUES ('3', '10000158');
INSERT INTO `chat_role_user` VALUES ('3', '10000159');
INSERT INTO `chat_role_user` VALUES ('3', '10000160');
INSERT INTO `chat_role_user` VALUES ('3', '10000161');
INSERT INTO `chat_role_user` VALUES ('3', '10000162');
INSERT INTO `chat_role_user` VALUES ('3', '10000163');
INSERT INTO `chat_role_user` VALUES ('3', '10000164');
INSERT INTO `chat_role_user` VALUES ('3', '10000165');
INSERT INTO `chat_role_user` VALUES ('3', '10000166');
INSERT INTO `chat_role_user` VALUES ('3', '10000167');
INSERT INTO `chat_role_user` VALUES ('3', '10000168');
INSERT INTO `chat_role_user` VALUES ('3', '10000169');
INSERT INTO `chat_role_user` VALUES ('3', '10000170');
INSERT INTO `chat_role_user` VALUES ('3', '10000171');
INSERT INTO `chat_role_user` VALUES ('3', '10000172');
INSERT INTO `chat_role_user` VALUES ('3', '10000173');
INSERT INTO `chat_role_user` VALUES ('3', '10000174');
INSERT INTO `chat_role_user` VALUES ('3', '10000175');
INSERT INTO `chat_role_user` VALUES ('3', '10000176');
INSERT INTO `chat_role_user` VALUES ('3', '10000177');
INSERT INTO `chat_role_user` VALUES ('3', '10000178');
INSERT INTO `chat_role_user` VALUES ('3', '10000179');
INSERT INTO `chat_role_user` VALUES ('3', '10000180');
INSERT INTO `chat_role_user` VALUES ('3', '10000181');
INSERT INTO `chat_role_user` VALUES ('3', '10000182');
INSERT INTO `chat_role_user` VALUES ('3', '10000183');
INSERT INTO `chat_role_user` VALUES ('3', '10000184');
INSERT INTO `chat_role_user` VALUES ('3', '10000185');
INSERT INTO `chat_role_user` VALUES ('3', '10000186');
INSERT INTO `chat_role_user` VALUES ('3', '10000187');
INSERT INTO `chat_role_user` VALUES ('3', '10000188');
INSERT INTO `chat_role_user` VALUES ('3', '10000189');
INSERT INTO `chat_role_user` VALUES ('3', '10000190');
INSERT INTO `chat_role_user` VALUES ('3', '10000191');
INSERT INTO `chat_role_user` VALUES ('3', '10000192');
INSERT INTO `chat_role_user` VALUES ('3', '10000193');
INSERT INTO `chat_role_user` VALUES ('3', '10000194');
INSERT INTO `chat_role_user` VALUES ('3', '10000195');
INSERT INTO `chat_role_user` VALUES ('3', '10000196');
INSERT INTO `chat_role_user` VALUES ('3', '10000197');
INSERT INTO `chat_role_user` VALUES ('3', '10000198');
INSERT INTO `chat_role_user` VALUES ('3', '10000199');
INSERT INTO `chat_role_user` VALUES ('3', '10000200');
INSERT INTO `chat_role_user` VALUES ('3', '10000201');
INSERT INTO `chat_role_user` VALUES ('3', '10000202');
INSERT INTO `chat_role_user` VALUES ('3', '10000203');
INSERT INTO `chat_role_user` VALUES ('3', '10000204');
INSERT INTO `chat_role_user` VALUES ('3', '10000205');
INSERT INTO `chat_role_user` VALUES ('3', '10000206');
INSERT INTO `chat_role_user` VALUES ('3', '10000207');
INSERT INTO `chat_role_user` VALUES ('3', '10000208');
INSERT INTO `chat_role_user` VALUES ('3', '10000209');
INSERT INTO `chat_role_user` VALUES ('3', '10000210');
INSERT INTO `chat_role_user` VALUES ('3', '10000211');
INSERT INTO `chat_role_user` VALUES ('3', '10000212');
INSERT INTO `chat_role_user` VALUES ('3', '10000213');
INSERT INTO `chat_role_user` VALUES ('3', '10000214');
INSERT INTO `chat_role_user` VALUES ('3', '10000215');
INSERT INTO `chat_role_user` VALUES ('3', '10000216');
INSERT INTO `chat_role_user` VALUES ('3', '10000217');
INSERT INTO `chat_role_user` VALUES ('3', '10000218');
INSERT INTO `chat_role_user` VALUES ('3', '10000219');
INSERT INTO `chat_role_user` VALUES ('3', '10000220');
INSERT INTO `chat_role_user` VALUES ('3', '10000221');
INSERT INTO `chat_role_user` VALUES ('3', '10000222');
INSERT INTO `chat_role_user` VALUES ('3', '10000223');
INSERT INTO `chat_role_user` VALUES ('3', '10000224');
INSERT INTO `chat_role_user` VALUES ('3', '10000225');
INSERT INTO `chat_role_user` VALUES ('3', '10000226');
INSERT INTO `chat_role_user` VALUES ('3', '10000227');
INSERT INTO `chat_role_user` VALUES ('3', '10000228');
INSERT INTO `chat_role_user` VALUES ('3', '10000229');
INSERT INTO `chat_role_user` VALUES ('3', '10000230');
INSERT INTO `chat_role_user` VALUES ('3', '10000231');
INSERT INTO `chat_role_user` VALUES ('3', '10000232');
INSERT INTO `chat_role_user` VALUES ('3', '10000233');
INSERT INTO `chat_role_user` VALUES ('2', '10000000');
INSERT INTO `chat_role_user` VALUES ('3', '10000234');
INSERT INTO `chat_role_user` VALUES ('3', '10000235');
INSERT INTO `chat_role_user` VALUES ('3', '10000236');
INSERT INTO `chat_role_user` VALUES ('3', '10000237');
INSERT INTO `chat_role_user` VALUES ('3', '10000238');
INSERT INTO `chat_role_user` VALUES ('3', '10000239');
INSERT INTO `chat_role_user` VALUES ('3', '10000240');
INSERT INTO `chat_role_user` VALUES ('3', '10000241');
INSERT INTO `chat_role_user` VALUES ('3', '10000242');
INSERT INTO `chat_role_user` VALUES ('3', '10000243');
INSERT INTO `chat_role_user` VALUES ('3', '10000244');
INSERT INTO `chat_role_user` VALUES ('3', '10000245');
INSERT INTO `chat_role_user` VALUES ('3', '10000246');
INSERT INTO `chat_role_user` VALUES ('3', '10000247');
INSERT INTO `chat_role_user` VALUES ('3', '10000248');
INSERT INTO `chat_role_user` VALUES ('3', '10000249');
INSERT INTO `chat_role_user` VALUES ('3', '10000250');
INSERT INTO `chat_role_user` VALUES ('3', '10000251');
INSERT INTO `chat_role_user` VALUES ('3', '10000252');
INSERT INTO `chat_role_user` VALUES ('3', '10000253');
INSERT INTO `chat_role_user` VALUES ('3', '10000254');
INSERT INTO `chat_role_user` VALUES ('3', '10000255');
INSERT INTO `chat_role_user` VALUES ('3', '10000256');
INSERT INTO `chat_role_user` VALUES ('3', '10000257');
INSERT INTO `chat_role_user` VALUES ('3', '10000258');
INSERT INTO `chat_role_user` VALUES ('3', '10000259');
INSERT INTO `chat_role_user` VALUES ('3', '10000260');
INSERT INTO `chat_role_user` VALUES ('3', '10000261');
INSERT INTO `chat_role_user` VALUES ('3', '10000262');
INSERT INTO `chat_role_user` VALUES ('3', '10000263');
INSERT INTO `chat_role_user` VALUES ('3', '10000264');
INSERT INTO `chat_role_user` VALUES ('3', '10000265');
INSERT INTO `chat_role_user` VALUES ('3', '10000266');
INSERT INTO `chat_role_user` VALUES ('3', '10000267');
INSERT INTO `chat_role_user` VALUES ('3', '10000268');
INSERT INTO `chat_role_user` VALUES ('3', '10000269');
INSERT INTO `chat_role_user` VALUES ('3', '10000270');
INSERT INTO `chat_role_user` VALUES ('3', '10000271');
INSERT INTO `chat_role_user` VALUES ('3', '10000272');
INSERT INTO `chat_role_user` VALUES ('3', '10000273');
INSERT INTO `chat_role_user` VALUES ('3', '10000274');

-- ----------------------------
-- Table structure for chat_route
-- ----------------------------
DROP TABLE IF EXISTS `chat_route`;
CREATE TABLE `chat_route` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '路由id',
  `full_url` varchar(255) DEFAULT NULL COMMENT '完整url， 如：portal/list/index?id=1',
  `url` varchar(255) DEFAULT NULL COMMENT '实际显示的url',
  `listorder` int(5) DEFAULT '0' COMMENT '排序，优先级，越小优先级越高',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态，1：启用 ;0：不启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='url路由表';

-- ----------------------------
-- Records of chat_route
-- ----------------------------

-- ----------------------------
-- Table structure for chat_slide
-- ----------------------------
DROP TABLE IF EXISTS `chat_slide`;
CREATE TABLE `chat_slide` (
  `slide_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slide_cid` int(11) NOT NULL COMMENT '幻灯片分类 id',
  `slide_name` varchar(255) NOT NULL COMMENT '幻灯片名称',
  `slide_pic` varchar(255) DEFAULT NULL COMMENT '幻灯片图片',
  `slide_url` varchar(255) DEFAULT NULL COMMENT '幻灯片链接',
  `slide_des` varchar(255) DEFAULT NULL COMMENT '幻灯片描述',
  `slide_content` text COMMENT '幻灯片内容',
  `slide_status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1显示，0不显示',
  `listorder` int(10) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`slide_id`),
  KEY `slide_cid` (`slide_cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='幻灯片表';

-- ----------------------------
-- Records of chat_slide
-- ----------------------------

-- ----------------------------
-- Table structure for chat_slide_cat
-- ----------------------------
DROP TABLE IF EXISTS `chat_slide_cat`;
CREATE TABLE `chat_slide_cat` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) NOT NULL COMMENT '幻灯片分类',
  `cat_idname` varchar(255) NOT NULL COMMENT '幻灯片分类标识',
  `cat_remark` text COMMENT '分类备注',
  `cat_status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1显示，0不显示',
  PRIMARY KEY (`cid`),
  KEY `cat_idname` (`cat_idname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='幻灯片分类表';

-- ----------------------------
-- Records of chat_slide_cat
-- ----------------------------

-- ----------------------------
-- Table structure for chat_terms
-- ----------------------------
DROP TABLE IF EXISTS `chat_terms`;
CREATE TABLE `chat_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `name` varchar(200) DEFAULT NULL COMMENT '分类名称',
  `slug` varchar(200) DEFAULT '',
  `taxonomy` varchar(32) DEFAULT NULL COMMENT '分类类型',
  `description` longtext COMMENT '分类描述',
  `parent` bigint(20) unsigned DEFAULT '0' COMMENT '分类父id',
  `count` bigint(20) DEFAULT '0' COMMENT '分类文章数',
  `path` varchar(500) DEFAULT NULL COMMENT '分类层级关系路径',
  `seo_title` varchar(500) DEFAULT NULL,
  `seo_keywords` varchar(500) DEFAULT NULL,
  `seo_description` varchar(500) DEFAULT NULL,
  `list_tpl` varchar(50) DEFAULT NULL COMMENT '分类列表模板',
  `one_tpl` varchar(50) DEFAULT NULL COMMENT '分类文章页模板',
  `listorder` int(5) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1发布，0不发布',
  PRIMARY KEY (`term_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Portal 文章分类表';

-- ----------------------------
-- Records of chat_terms
-- ----------------------------
INSERT INTO `chat_terms` VALUES ('1', '列表演示', '', 'article', '', '0', '0', '0-1', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `chat_terms` VALUES ('2', '瀑布流', '', 'article', '', '0', '0', '0-2', '', '', '', 'list_masonry', 'article', '0', '1');

-- ----------------------------
-- Table structure for chat_term_relationships
-- ----------------------------
DROP TABLE IF EXISTS `chat_term_relationships`;
CREATE TABLE `chat_term_relationships` (
  `tid` bigint(20) NOT NULL AUTO_INCREMENT,
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'posts表里文章id',
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '分类id',
  `listorder` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1发布，0不发布',
  PRIMARY KEY (`tid`),
  KEY `term_taxonomy_id` (`term_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='Portal 文章分类对应表';

-- ----------------------------
-- Records of chat_term_relationships
-- ----------------------------

-- ----------------------------
-- Table structure for chat_users
-- ----------------------------
DROP TABLE IF EXISTS `chat_users`;
CREATE TABLE `chat_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) NOT NULL DEFAULT '' COMMENT '用户名',
  `user_pass` varchar(64) NOT NULL DEFAULT '' COMMENT '登录密码；sp_password加密',
  `user_nicename` varchar(50) NOT NULL DEFAULT '' COMMENT '用户美名',
  `user_email` varchar(100) NOT NULL DEFAULT '' COMMENT '登录邮箱',
  `user_url` varchar(100) NOT NULL DEFAULT '' COMMENT '用户个人网站',
  `avatar` varchar(255) DEFAULT NULL COMMENT '用户头像，相对于upload/avatar目录',
  `sex` smallint(1) DEFAULT '0' COMMENT '性别；0：保密，1：男；2：女',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `signature` varchar(255) DEFAULT NULL COMMENT '个性签名',
  `user_title` varchar(50) DEFAULT '' COMMENT '用户头衔',
  `user_star` int(5) DEFAULT '4' COMMENT '默认为4星分析师',
  `last_login_ip` varchar(16) DEFAULT NULL COMMENT '最后登录ip',
  `last_login_time` datetime NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '最后登录时间',
  `create_time` datetime NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '注册时间',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '' COMMENT '激活码',
  `user_status` int(11) NOT NULL DEFAULT '1' COMMENT '用户状态 0：禁用； 1：正常 ；2：未验证',
  `score` int(11) NOT NULL DEFAULT '0' COMMENT '用户积分',
  `user_type` smallint(3) DEFAULT '3' COMMENT '用户类型，1:admin ;2:老师；3普通用户',
  `coin` int(11) NOT NULL DEFAULT '0' COMMENT '金币',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  PRIMARY KEY (`id`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`)
) ENGINE=InnoDB AUTO_INCREMENT=10000275 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户表';

-- ----------------------------
-- Records of chat_users
-- ----------------------------
INSERT INTO `chat_users` VALUES ('1', 'admin', '###77d686c8a8ca6af6d25c72dd7d6532f1', 'admin', 'shl3807@qq.com', '', '576a091243aea.jpg', '0', null, null, '', '4', '211.161.245.62', '2016-06-20 22:29:30', '2016-05-20 05:47:00', '', '1', '0', '1', '0', '');
INSERT INTO `chat_users` VALUES ('2', '1_qq_com', '###77d686c8a8ca6af6d25c72dd7d6532f1', '小二', '1@qq.com', '', '5768013e9e2df.jpg', '0', null, null, '', '4', '118.132.82.135', '2016-06-20 22:08:31', '2016-05-23 23:27:09', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('4', '333_qq_com', '###77d686c8a8ca6af6d25c72dd7d6532f1', '333_qq_com', '33@qq.com', '', null, '0', null, null, '', '4', '120.236.150.233', '2016-06-30 20:09:33', '2016-05-28 23:11:32', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('5', '44_qq_com', '###77d686c8a8ca6af6d25c72dd7d6532f1', '阿华', '44@qq.com', '', '5773362fab3e7.jpg', '0', null, '', '', '4', '183.63.120.174', '2016-07-08 09:25:43', '2016-05-28 23:11:58', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('6', '55_qq_com', '###357763c46079351071f5f187017be419', '呵呵', '55@qq.com', '', '575d626dd080b.jpg', '0', null, '', '', '4', '180.167.10.194', '2016-06-15 13:41:52', '2016-05-28 23:12:25', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('7', '66', '###77d686c8a8ca6af6d25c72dd7d6532f1', '流流', '66@qq.com', '', '5769f2454ad55.jpg', '0', null, 'heelo', '', '4', '180.167.10.194', '2016-07-08 10:36:09', '2016-05-29 16:48:14', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('8', 'teacher', '###77d686c8a8ca6af6d25c72dd7d6532f1', '伍尤顺老师', '88@qq.com', '', '5774d887d7678.jpg', '0', '1988-06-02', '工作年限：2010年至今（5年有余） 具有多年股市、汇市，期货，原油及贵金属的交易经验。\n    2009年进入国际金融市场。10年入驻国内白银市场。\n    2012年以月收益800%获称“盈利王”的称号。曾创造原油连续11周盈利神话！\n　　\n    对经济数据、K线理论、均线理论、指标理论、形态理论、波浪理论均有独特的研究造诣，\n    不仅具备深厚的技术分析功底，而且擅用逆向思维来把握市场群体心理反应，\n    能够准确的把握市场动向，在变幻莫测、扑朔迷离的交易市场中游刃有余。', '', '4', '211.161.245.62', '2016-06-22 23:03:16', '2016-05-29 17:21:11', '', '1', '0', '2', '0', '');
INSERT INTO `chat_users` VALUES ('10000000', 'teacher@qq.com', '###77d686c8a8ca6af6d25c72dd7d6532f1', '郑杰', 'teacher@qq.com', '', '5769fb291d1b6.jpg', '0', null, '从事金融行业6年，曾担任国内一大型投资机构分析师和培训师。在期货，外汇和大宗商品市场均有较强的实战经验。对均线系统，K线形态，数据建模，宏观分析等均有较深的理解和运用。擅长中线行情分析布局和宏观分析，交易秉承“耐心等待，大胆出击”的风格，坚持顺应了趋势就大胆加仓，错误了就坚决离场的交易理念。曾经在进入金融行业前，利用自身所学理论成功预测金银2013年将在近十年春节首次出现下跌。', '首席分析师', '5', '119.131.77.206', '2016-06-30 20:42:53', '2016-06-02 23:24:44', '', '1', '0', '2', '0', '');
INSERT INTO `chat_users` VALUES ('10000001', '12345@qq.com', '###77d686c8a8ca6af6d25c72dd7d6532f1', '吴勇坚老师', '61727175@qq.com', '', '5774e6605110e.jpg', '0', null, '从2010年进入国际金融市场，至今（5年有余） ，先后曾在多家企业担任职业操盘，在股市、贵金属，原油及贵金属市场拥有丰富的实战操作经验和分析能力\n2013-2015年间，平均月盈率达到40%。\n擅长江恩理论，波浪理论、趋势线和通道以及各种技术指标应用进行短期投资。\n其交易理念为，从基本面需求大方向，从技术面寻求入场水平，控制仓位耐心等待机会顺势而为', '', '4', '211.161.245.62', '2016-06-20 22:47:10', '2016-06-12 11:13:56', '', '1', '0', '2', '0', '');
INSERT INTO `chat_users` VALUES ('10000002', '40462', '###4a1ae5d93defb1a39091b9f41d26b307', '普京', 'caiaimen@qq.com', '', '577343e7166a4.jpg', '0', '1989-06-13', '想生财，先大气！', '', '4', '183.63.120.174', '2016-07-08 09:44:05', '2016-06-12 13:39:21', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000003', '40458', '###a1de5e2f4d76345a48078e91abf56a3e', 'y运姐', '654@qq.com', '', '576a248386b79.jpg', '0', null, '赚钱要像流水', '', '4', '183.63.120.174', '2016-07-05 20:54:38', '2016-06-12 13:40:41', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000004', '40419', '###ce624b8d048ae57e25ad112e1ce481ab', '结点', '6321@qq.com', '', '576b638be352d.jpg', '0', '1976-07-20', '', '', '4', '183.63.120.174', '2016-07-07 23:16:48', '2016-06-12 13:41:19', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000005', '61286', '###a1de5e2f4d76345a48078e91abf56a3e', '油海行舟', '654321@qq.com', '', '576a0a46d045c.jpg', '0', '1983-06-01', '', '', '4', '120.236.150.233', '2016-06-30 15:44:38', '2016-06-12 13:42:58', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000006', '40463', '###bacce76d03e73b258332d69ef56b2d56', '回首往日..', '63581@qq.com', '', '576a050a6b1cc.jpg', '0', '1985-06-07', '赚赚赚、不要停、、', '', '4', '120.236.150.239', '2016-07-08 14:20:42', '2016-06-12 13:43:23', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000007', '61355', '###4a1ae5d93defb1a39091b9f41d26b307', '细妹', '6994@qq.com', '', '', '0', null, '', '', '4', '112.96.164.106', '2016-07-07 20:31:24', '2016-06-12 13:44:03', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000008', '40457', '###4a1ae5d93defb1a39091b9f41d26b307', '当先', '123@qq.com', '', '', '0', null, '', '', '4', '183.63.120.174', '2016-07-07 23:12:39', '2016-06-12 13:44:29', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000009', '40460', '###a1de5e2f4d76345a48078e91abf56a3e', '', '95@qq.com', '', '', '0', null, '', '', '4', '183.63.120.174', '2016-07-05 22:19:54', '2016-06-12 13:44:50', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000010', '40454', '###4a1ae5d93defb1a39091b9f41d26b307', '', '40454@qq.com', '', '', '0', null, '', '', '4', '120.236.150.233', '2016-07-08 10:58:48', '2016-06-12 13:58:38', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000011', '40456', '###77d686c8a8ca6af6d25c72dd7d6532f1', '', '40456@qq.com', '', '', '0', null, '', '', '4', '120.236.150.233', '2016-06-30 10:36:19', '2016-06-12 14:04:24', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000012', '40455', '###4a1ae5d93defb1a39091b9f41d26b307', '', '40455@qq.com', '', '', '0', null, '', '', '4', '120.236.170.83', '2016-06-24 14:43:04', '2016-06-12 14:04:43', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000013', '61197', '###a1de5e2f4d76345a48078e91abf56a3e', '楠', '61197@qq.com', '', '577f1f6abe83f.jpg', '0', null, '', '', '4', '120.236.150.239', '2016-07-08 11:09:21', '2016-06-13 09:54:33', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000014', '61282', '###a1de5e2f4d76345a48078e91abf56a3e', '雯', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-06-29 13:25:47', '2016-06-22 10:00:15', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000015', '61279', '###a1de5e2f4d76345a48078e91abf56a3e', '芬', '', '', null, '0', null, '', '', '4', '120.236.170.83', '2016-06-22 10:50:29', '2016-06-22 10:03:34', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000016', '61299', '###a1de5e2f4d76345a48078e91abf56a3e', '霞', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-07 22:56:43', '2016-06-22 10:06:17', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000017', '61320', '###a1de5e2f4d76345a48078e91abf56a3e', '杰', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-07 22:50:10', '2016-06-22 10:06:42', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000018', '61499', '###a1de5e2f4d76345a48078e91abf56a3e', '~.~', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-07 22:56:43', '2016-06-22 10:07:02', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000019', '61476', '###a1de5e2f4d76345a48078e91abf56a3e', '龙', '', '', null, '0', null, '', '', '4', '120.236.150.239', '2016-07-08 09:58:44', '2016-06-22 10:07:14', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000020', '61502', '###a1de5e2f4d76345a48078e91abf56a3e', '静', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-06-29 13:28:18', '2016-06-22 10:07:31', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000021', '61571', '###a1de5e2f4d76345a48078e91abf56a3e', '敏', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-07-08 13:53:45', '2016-06-22 10:07:45', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000022', '61588', '###a1de5e2f4d76345a48078e91abf56a3e', '毅', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-07 23:00:26', '2016-06-22 10:08:01', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000023', '61591', '###a1de5e2f4d76345a48078e91abf56a3e', '俊', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-07 22:45:10', '2016-06-22 10:08:17', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000024', '61300', '###a1de5e2f4d76345a48078e91abf56a3e', '建', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-07 23:03:18', '2016-06-22 10:08:30', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000025', '61482', '###a1de5e2f4d76345a48078e91abf56a3e', '媚', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-06-29 22:11:54', '2016-06-22 10:08:46', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000026', '61478', '###a1de5e2f4d76345a48078e91abf56a3e', '沙特', '', '', null, '0', null, '', '', '4', '120.236.150.239', '2016-07-08 14:43:16', '2016-06-22 10:08:59', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000027', '61497', '###a1de5e2f4d76345a48078e91abf56a3e', '披着狼皮的羊', '', '', '576a02e63d7d1.jpg', '0', '1900-01-01', '路漫漫其修远兮，吾将上下而求索。', '', '4', '120.236.170.82', '2016-06-24 10:53:05', '2016-06-22 10:09:18', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000028', '61237', '###a1de5e2f4d76345a48078e91abf56a3e', '生', '', '', null, '0', null, '', '', '4', '120.236.170.82', '2016-06-22 11:30:35', '2016-06-22 10:09:40', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000029', '61529', '###a1de5e2f4d76345a48078e91abf56a3e', '清', '', '', null, '0', null, '', '', '4', '120.236.170.82', '2016-06-24 09:17:48', '2016-06-22 10:09:52', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000030', '61585', '###a1de5e2f4d76345a48078e91abf56a3e', '缱绻旧时光', '', '', null, '0', null, '', '', '4', '120.236.170.84', '2016-06-23 09:36:03', '2016-06-22 10:10:05', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000031', '61392', '###a1de5e2f4d76345a48078e91abf56a3e', '吴耀映', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-05 12:54:07', '2016-06-22 10:10:19', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000032', '61493', '###a1de5e2f4d76345a48078e91abf56a3e', '金融风暴来了', '', '', null, '0', null, '我只想穷的只剩下钱', '', '4', '183.63.120.174', '2016-07-08 09:50:03', '2016-06-22 10:10:34', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000033', '61480', '###a1de5e2f4d76345a48078e91abf56a3e', '森', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-06-30 14:13:28', '2016-06-22 10:10:46', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000034', '61513', '###a1de5e2f4d76345a48078e91abf56a3e', '明', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-06-27 09:58:40', '2016-06-22 10:10:57', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000035', '61511', '###a1de5e2f4d76345a48078e91abf56a3e', '朝', '', '', null, '0', null, '', '', '4', '120.236.170.82', '2016-06-22 22:13:29', '2016-06-22 10:11:12', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000036', '61512', '###a1de5e2f4d76345a48078e91abf56a3e', '广', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-05 18:58:19', '2016-06-22 10:11:25', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000037', '61528', '###a1de5e2f4d76345a48078e91abf56a3e', '阿拉伯酋长', '', '', '5774970a458e3.jpg', '0', '1989-07-25', '', '', '4', '120.236.150.233', '2016-07-08 10:35:24', '2016-06-22 10:11:44', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000038', '61596', '###a1de5e2f4d76345a48078e91abf56a3e', '建', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 10:11:59', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000039', '61602', '###a1de5e2f4d76345a48078e91abf56a3e', '玲', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 10:12:15', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000040', '61375', '###a1de5e2f4d76345a48078e91abf56a3e', '州', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-06-27 14:02:04', '2016-06-22 10:12:28', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000041', '61407', '###a1de5e2f4d76345a48078e91abf56a3e', '炽', '', '', null, '0', null, '', '', '4', '120.236.170.84', '2016-06-22 13:40:29', '2016-06-22 10:12:41', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000042', '61268', '###a1de5e2f4d76345a48078e91abf56a3e', '术', '', '', null, '0', null, '', '', '4', '120.236.170.83', '2016-06-22 17:47:20', '2016-06-22 10:12:54', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000043', '61378', '###a1de5e2f4d76345a48078e91abf56a3e', '韩', '', '', null, '0', null, '', '', '4', '120.236.150.239', '2016-07-08 11:14:40', '2016-06-22 10:13:14', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000044', '61524', '###a1de5e2f4d76345a48078e91abf56a3e', '东', '', '', null, '0', null, '', '', '4', '120.236.170.82', '2016-06-22 13:40:27', '2016-06-22 10:13:28', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000045', '61514', '###a1de5e2f4d76345a48078e91abf56a3e', '凡', '', '', null, '0', null, '', '', '4', '120.236.170.82', '2016-06-24 09:33:52', '2016-06-22 10:13:48', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000046', '61535', '###a1de5e2f4d76345a48078e91abf56a3e', '鹏', '', '', null, '0', null, '', '', '4', '120.236.170.84', '2016-06-22 13:40:41', '2016-06-22 10:13:59', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000047', '61572', '###a1de5e2f4d76345a48078e91abf56a3e', '诗', '', '', null, '0', null, '', '', '4', '120.236.170.82', '2016-06-22 13:47:45', '2016-06-22 10:14:12', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000048', '61590', '###a1de5e2f4d76345a48078e91abf56a3e', 'Rock', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-06-28 18:11:25', '2016-06-22 10:14:26', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000049', '61597', '###a1de5e2f4d76345a48078e91abf56a3e', '思', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 10:14:39', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000050', '61306', '###9787c474ec6af9a0d1cee75f5c839dab', '酆', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-07 23:08:54', '2016-06-22 10:14:50', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000051', '61308', '###a1de5e2f4d76345a48078e91abf56a3e', '三滴水', '', '', '576a25d95e31c.jpg', '0', '2016-05-11', '想做成功者，先学会做人', '', '4', '120.236.150.233', '2016-06-29 22:21:21', '2016-06-22 10:15:08', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000052', '61206', '###a1de5e2f4d76345a48078e91abf56a3e', '宋金莱', '', '', '576a2516cab45.jpg', '0', null, '实现', '', '4', '120.236.150.233', '2016-06-29 22:07:31', '2016-06-22 10:15:27', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000053', '61468', '###a1de5e2f4d76345a48078e91abf56a3e', '请叫我女王大人', '', '', '576a250f579c6.jpg', '0', '1990-06-15', '抵得住寂寞，守得住繁华', '', '4', '120.236.170.82', '2016-06-24 11:36:28', '2016-06-22 10:15:37', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000054', '61475', '###a1de5e2f4d76345a48078e91abf56a3e', '￥￥￥', '', '', null, '0', null, '￥￥￥', '', '4', '183.63.120.174', '2016-07-07 22:45:27', '2016-06-22 10:15:49', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000055', '61516', '###a1de5e2f4d76345a48078e91abf56a3e', '熙', '', '', null, '0', '1987-07-20', '珍惜才能拥有', '', '4', '120.236.170.82', '2016-06-24 09:55:39', '2016-06-22 10:16:07', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000056', '61515', '###a1de5e2f4d76345a48078e91abf56a3e', '叮咚', '', '', null, '0', '2016-02-10', '', '', '4', '120.236.170.83', '2016-06-24 14:01:36', '2016-06-22 10:16:18', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000057', '61517', '###dc0afd584747ca200e181fba092bdb37', '张姐姐', '', '', null, '0', '1989-12-02', '终于等到你开播', '', '4', '120.236.150.233', '2016-06-28 10:23:04', '2016-06-22 10:16:29', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000058', '61598', '###a1de5e2f4d76345a48078e91abf56a3e', '小郑', '', '', '576a233feb877.jpg', '0', '1981-06-19', '生活就是挑战', '', '4', '120.236.170.82', '2016-06-24 12:48:50', '2016-06-22 10:16:47', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000059', '61293', '###3f60c4a7a13af6e7deddb2623ac67393', '莲', '', '', null, '0', null, '越努力越幸运', '', '4', '120.236.150.233', '2016-06-30 16:23:00', '2016-06-22 10:17:10', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000060', '61333', '###a1de5e2f4d76345a48078e91abf56a3e', '明', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-06-30 14:52:40', '2016-06-22 10:17:23', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000061', '61343', '###bcee34b300e35fe999917db31e76a124', '访客', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-05 16:57:15', '2016-06-22 10:17:36', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000062', '61484', '###a1de5e2f4d76345a48078e91abf56a3e', '豪豪', '', '', '576a1bb95f102.jpg', '0', '1995-04-24', '带你装逼带你飞', '', '4', '120.236.170.82', '2016-06-24 20:34:11', '2016-06-22 10:17:48', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000063', '61345', '###a1de5e2f4d76345a48078e91abf56a3e', '夏末', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-08 09:41:21', '2016-06-22 10:17:57', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000064', '61505', '###a1de5e2f4d76345a48078e91abf56a3e', '雄', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-07-08 10:08:45', '2016-06-22 10:18:30', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000065', '61599', '###a1de5e2f4d76345a48078e91abf56a3e', '澎', '', '', null, '0', null, '', '', '4', '120.236.170.83', '2016-06-23 20:59:44', '2016-06-22 10:18:46', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000066', '61313', '###a1de5e2f4d76345a48078e91abf56a3e', '霞', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 10:18:56', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000067', '61370', '###a1de5e2f4d76345a48078e91abf56a3e', '艳', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 10:19:08', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000068', '61520', '###a1de5e2f4d76345a48078e91abf56a3e', '芬', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 10:19:18', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000069', '61553', '###a1de5e2f4d76345a48078e91abf56a3e', '秋', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 10:19:29', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000070', '61574', '###a1de5e2f4d76345a48078e91abf56a3e', '成', '', '', null, '0', null, '', '', '4', '120.236.170.83', '2016-06-23 20:05:45', '2016-06-22 10:19:42', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000071', '61284', '###a1de5e2f4d76345a48078e91abf56a3e', '玲', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-07 13:37:00', '2016-06-22 10:19:53', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000072', '61231', '###a1de5e2f4d76345a48078e91abf56a3e', '浮生', '', '', null, '0', null, '', '', '4', '61.140.140.169', '2016-07-03 22:47:30', '2016-06-22 10:20:20', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000073', '61489', '###a1de5e2f4d76345a48078e91abf56a3e', '妃', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-06-29 22:21:10', '2016-06-22 10:20:34', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000074', '61491', '###a1de5e2f4d76345a48078e91abf56a3e', '敏', '', '', null, '0', null, '', '', '4', '61.140.222.191', '2016-07-08 10:26:22', '2016-06-22 10:20:44', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000075', '61600', '###a1de5e2f4d76345a48078e91abf56a3e', '敏', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 10:20:53', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000076', '61601', '###a1de5e2f4d76345a48078e91abf56a3e', '静', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-07 13:48:25', '2016-06-22 10:22:49', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000077', '61442', '###a1de5e2f4d76345a48078e91abf56a3e', '送终鸡', '', '', null, '0', '2016-04-22', '因为个性.所以没签名', '', '4', '120.236.150.233', '2016-06-29 22:03:45', '2016-06-22 10:23:14', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000078', '61525', '###a1de5e2f4d76345a48078e91abf56a3e', '佐', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-06-30 09:46:23', '2016-06-22 10:23:28', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000079', '61526', '###a1de5e2f4d76345a48078e91abf56a3e', '峰', '', '', '576a0def749f6.jpg', '0', null, '', '', '4', '120.236.150.239', '2016-07-08 12:56:54', '2016-06-22 10:23:41', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000080', '61533', '###191fef37ec79ae2bff8874fea205844c', '小飞飞1111', '', '', null, '0', '2016-05-29', '命是失败者的借口，运是成功者的谦词，命运掌握在自己手中！！！', '', '4', '120.236.150.233', '2016-06-28 18:29:17', '2016-06-22 10:23:55', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000081', '61592', '###a1de5e2f4d76345a48078e91abf56a3e', '杰', '', '', null, '0', null, '', '', '4', '120.236.170.82', '2016-06-23 09:35:29', '2016-06-22 10:24:09', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000082', '61595', '###a1de5e2f4d76345a48078e91abf56a3e', '秋', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-06-28 19:16:26', '2016-06-22 10:24:20', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000083', '61594', '###a1de5e2f4d76345a48078e91abf56a3e', '珍', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 10:24:33', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000084', '61389', '###a1de5e2f4d76345a48078e91abf56a3e', '张小宾', '', '', null, '0', '1981-01-01', '感谢高德老师帮助我实现梦想！', '', '4', '183.63.120.174', '2016-07-07 22:19:57', '2016-06-22 10:24:49', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000085', '61303', '###a1de5e2f4d76345a48078e91abf56a3e', '不一样的烟火！', '', '', '576a170fec95e.jpg', '0', null, '', '', '4', '120.236.150.233', '2016-06-29 22:07:10', '2016-06-22 10:24:59', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000086', '60668', '###a1de5e2f4d76345a48078e91abf56a3e', '小糊涂', '', '', null, '0', '1890-06-03', '', '', '4', '120.236.170.84', '2016-06-22 13:54:24', '2016-06-22 10:25:16', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000087', '61486', '###a1de5e2f4d76345a48078e91abf56a3e', 'ya', '', '', null, '0', '1989-04-14', '', '', '4', '120.236.170.82', '2016-06-23 14:07:37', '2016-06-22 10:25:30', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000088', '61577', '###a1de5e2f4d76345a48078e91abf56a3e', '渣哥', '', '', null, '0', '1988-06-07', '赚钱才是王道', '', '4', '120.236.150.233', '2016-07-08 13:43:55', '2016-06-22 10:25:43', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000089', '61589', '###a1de5e2f4d76345a48078e91abf56a3e', '.....', '', '', '576a17e9cd820.jpg', '0', null, '', '', '4', '120.236.170.84', '2016-06-24 11:51:01', '2016-06-22 10:26:01', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000090', '61265', '###a1de5e2f4d76345a48078e91abf56a3e', '情义两心知', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-07-08 13:01:48', '2016-06-22 10:26:13', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000091', '61269', '###a1de5e2f4d76345a48078e91abf56a3e', '海阔天空', '', '', '576a247f32eb2.jpg', '0', null, '', '', '4', '120.236.170.83', '2016-06-24 14:26:06', '2016-06-22 10:26:31', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000092', '61270', '###a1de5e2f4d76345a48078e91abf56a3e', '心态', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-07 16:58:21', '2016-06-22 10:26:44', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000093', '61523', '###4a1ae5d93defb1a39091b9f41d26b307', '从头再来', '', '', null, '0', '1980-06-01', '在西藏再努力也烧不开一壶水，说明环境很重要。骑自行车再努力也追不上宝马，说明平台很重要。一个人再有能力也干不过一人，说明团队很重要！', '', '4', '183.63.120.174', '2016-07-07 22:52:42', '2016-06-22 10:26:58', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000094', '61569', '###a1de5e2f4d76345a48078e91abf56a3e', '蓝色的天空', '', '', null, '0', '1988-06-01', '', '', '4', '120.236.170.84', '2016-06-22 13:32:52', '2016-06-22 10:27:14', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000095', '61580', '###ac43c84e4584348014c0f5d1faca4496', 'msxiqi', '', '', null, '0', null, '专注要好一件事情', '', '4', '120.236.170.83', '2016-06-24 09:16:13', '2016-06-22 10:27:26', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000096', '61581', '###a1de5e2f4d76345a48078e91abf56a3e', '小兆兆', '', '', null, '0', null, '我就是我，不一样的烟火', '', '4', '183.63.120.174', '2016-07-07 14:02:05', '2016-06-22 10:27:41', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000097', '61582', '###a1de5e2f4d76345a48078e91abf56a3e', '环', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 10:27:55', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000098', '61586', '###a1de5e2f4d76345a48078e91abf56a3e', '老大姐', '', '', null, '0', null, '', '', '4', '120.236.170.84', '2016-06-22 13:29:46', '2016-06-22 10:28:07', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000099', '61040', '###a1de5e2f4d76345a48078e91abf56a3e', '梧桐', '', '', '576a0eb37c8c2.jpg', '0', null, '呵呵', '', '4', '120.236.170.84', '2016-06-22 14:35:13', '2016-06-22 10:28:52', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000100', '61366', '###a1de5e2f4d76345a48078e91abf56a3e', '文贤', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-07-08 11:03:45', '2016-06-22 10:30:12', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000101', '61285', '###a1de5e2f4d76345a48078e91abf56a3e', '彪', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-07-08 13:21:28', '2016-06-22 10:30:22', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000102', '61298', '###77d686c8a8ca6af6d25c72dd7d6532f1', 'Queenie ', '', '', '576b751541a27.jpg', '0', null, '', '', '4', '120.236.150.239', '2016-07-08 10:23:45', '2016-06-22 10:30:32', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000103', 'shuyu.luo', '###e29f03ba88056680571481f5bc93d398', '小榆老师', 'shuyu.luo@xz286.com', '', '577214e1f41b3.jpg', '2', null, '本科金融专业毕业，2014年进入金融市场，两年现货市场的从业和讲师经验，擅长道氏理论，拥有丰富的客户理财经验。', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 10:38:33', '', '1', '0', '2', '0', '');
INSERT INTO `chat_users` VALUES ('10000104', '40461', '###a1de5e2f4d76345a48078e91abf56a3e', '阿萨德撒', '', '', null, '0', null, '', '', '4', '120.236.170.84', '2016-06-23 13:18:10', '2016-06-22 10:55:44', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000105', '286000000000100', '###a1de5e2f4d76345a48078e91abf56a3e', '芳', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:00:19', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000106', '286000000000229', '###a1de5e2f4d76345a48078e91abf56a3e', '坤', '', '', null, '0', null, '', '', '4', '113.94.115.47', '2016-07-04 21:55:42', '2016-06-22 11:02:15', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000107', '286000000000135', '###a1de5e2f4d76345a48078e91abf56a3e', '灿', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:02:49', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000108', '286000000000151', '###a1de5e2f4d76345a48078e91abf56a3e', '高', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:03:17', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000109', '61469', '###a1de5e2f4d76345a48078e91abf56a3e', '天空是蓝的', '', '', '576b4a1dc5ad2.jpg', '0', null, '', '', '4', '60.223.139.134', '2016-07-07 22:38:44', '2016-06-22 11:12:30', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000110', '286000000000237', '###a1de5e2f4d76345a48078e91abf56a3e', '华', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:17:06', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000111', '286000000000008', '###a1de5e2f4d76345a48078e91abf56a3e', '峰', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:17:40', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000112', '286000000000017', '###a1de5e2f4d76345a48078e91abf56a3e', '集', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:18:12', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000113', '286000000000010', '###a1de5e2f4d76345a48078e91abf56a3e', '标', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:18:35', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000114', '286000000000018', '###a1de5e2f4d76345a48078e91abf56a3e', '波', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:19:12', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000115', '286000000000023', '###a1de5e2f4d76345a48078e91abf56a3e', '强', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:20:25', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000116', '86000000000038', '###a1de5e2f4d76345a48078e91abf56a3e', '君', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:20:56', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000117', '286000000000011', '###a1de5e2f4d76345a48078e91abf56a3e', '霞', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:21:20', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000118', '286000000000053', '###a1de5e2f4d76345a48078e91abf56a3e', '文', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:21:39', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000119', '86000000000029', '###a1de5e2f4d76345a48078e91abf56a3e', '开', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:22:05', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000120', '286000000000056', '###a1de5e2f4d76345a48078e91abf56a3e', '菊', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:22:26', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000121', '10215', '###a1de5e2f4d76345a48078e91abf56a3e', '虎哥', '', '', null, '0', '1986-03-06', '计划我的交易，交易我的计划！！', '', '4', '14.151.9.149', '2016-06-28 16:17:03', '2016-06-22 11:23:16', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000122', '286000000000061', '###a1de5e2f4d76345a48078e91abf56a3e', '庆', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:23:41', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000123', '286000000000022', '###a1de5e2f4d76345a48078e91abf56a3e', '荣', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:24:01', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000124', '286000000000062', '###a1de5e2f4d76345a48078e91abf56a3e', '瑞', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:24:43', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000125', '286000000000037', '###a1de5e2f4d76345a48078e91abf56a3e', '宗', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:25:19', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000126', '286000000000070', '###a1de5e2f4d76345a48078e91abf56a3e', '波', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:25:49', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000127', '286000000000052', '###a1de5e2f4d76345a48078e91abf56a3e', '勇', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:26:13', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000128', '286000000000086', '###a1de5e2f4d76345a48078e91abf56a3e', '老狼', '', '', null, '0', null, '', '', '4', '112.5.236.207', '2016-07-01 18:47:43', '2016-06-22 11:26:46', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000129', '286000000000079', '###a1de5e2f4d76345a48078e91abf56a3e', '革', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:57:12', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000130', '286000000000072', '###a1de5e2f4d76345a48078e91abf56a3e', '臻', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:57:36', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000131', '286000000000103', '###a1de5e2f4d76345a48078e91abf56a3e', '超', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:58:00', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000132', '286000000000096', '###a1de5e2f4d76345a48078e91abf56a3e', '龙', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 11:58:25', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000133', '286000000000032', '###a1de5e2f4d76345a48078e91abf56a3e', '东', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 12:01:30', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000134', '286000000000036', '###a1de5e2f4d76345a48078e91abf56a3e', '云', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 12:01:53', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000135', '286000000000081', '###a1de5e2f4d76345a48078e91abf56a3e', '奎', '', '', null, '0', null, '', '', '4', '112.14.205.147', '2016-07-08 12:37:54', '2016-06-22 12:02:18', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000136', '286000000000093', '###a1de5e2f4d76345a48078e91abf56a3e', '亮', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 12:02:52', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000137', '286000000000121', '###a1de5e2f4d76345a48078e91abf56a3e', '贤', '', '', null, '0', null, '', '', '4', '222.211.213.171', '2016-06-23 18:26:17', '2016-06-22 12:03:15', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000138', '286000000000218', '###a1de5e2f4d76345a48078e91abf56a3e', '武', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 12:03:38', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000139', '286000000000129', '###a1de5e2f4d76345a48078e91abf56a3e', '腾', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 12:04:04', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000140', '286000000000139', '###a1de5e2f4d76345a48078e91abf56a3e', '如', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 12:04:24', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000141', '286000000000105', '###a1de5e2f4d76345a48078e91abf56a3e', '军', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 12:05:33', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000142', '50559', '###a1de5e2f4d76345a48078e91abf56a3e', '玉', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-08 11:46:10', '2016-06-22 12:36:47', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000143', '286000000000152', '###a1de5e2f4d76345a48078e91abf56a3e', '金', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:18:54', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000144', '286000000000110', '###a1de5e2f4d76345a48078e91abf56a3e', '荣', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:19:09', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000145', '286000000000137', '###a1de5e2f4d76345a48078e91abf56a3e', '洁', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:19:21', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000146', '286000000000153', '###a1de5e2f4d76345a48078e91abf56a3e', '莲', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:19:34', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000147', '286000000000169', '###a1de5e2f4d76345a48078e91abf56a3e', '贵', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:19:48', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000148', '286000000000177', '###a1de5e2f4d76345a48078e91abf56a3e', '娟', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:20:03', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000149', '286000000000127', '###a1de5e2f4d76345a48078e91abf56a3e', '军', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:20:18', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000150', '286000000000005', '###a1de5e2f4d76345a48078e91abf56a3e', '慰', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:20:46', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000151', '286000000000138', '###a1de5e2f4d76345a48078e91abf56a3e', '华', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:20:59', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000152', '286000000000171', '###a1de5e2f4d76345a48078e91abf56a3e', '军', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:21:12', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000153', '286000000000182', '###a1de5e2f4d76345a48078e91abf56a3e', '富', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:21:37', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000154', '286000000000158', '###a1de5e2f4d76345a48078e91abf56a3e', '珍', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:21:48', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000155', '286000000000180', '###a1de5e2f4d76345a48078e91abf56a3e', '军', '', '', null, '0', null, '', '', '4', '114.231.74.62', '2016-07-07 10:28:00', '2016-06-22 13:22:02', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000156', '286000000000168', '###a1de5e2f4d76345a48078e91abf56a3e', '芳', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:22:16', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000157', '286000000000201', '###a1de5e2f4d76345a48078e91abf56a3e', '发', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:22:28', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000158', '286000000000188', '###a1de5e2f4d76345a48078e91abf56a3e', '轩', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:22:42', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000159', '286000000000199', '###a1de5e2f4d76345a48078e91abf56a3e', '哲', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:23:04', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000160', '286000000000208', '###a1de5e2f4d76345a48078e91abf56a3e', '艳', '', '', null, '0', null, '', '', '4', '36.63.182.235', '2016-06-24 18:18:46', '2016-06-22 13:23:22', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000161', '286000000000197', '###a1de5e2f4d76345a48078e91abf56a3e', '东', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:23:37', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000162', '286000000000211', '###cc609272f93a4e0d9766969c1dab28d1', '余', '', '', null, '0', null, '', '', '4', '117.86.188.234', '2016-07-07 18:56:45', '2016-06-22 13:23:55', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000163', '286000000000186', '###a1de5e2f4d76345a48078e91abf56a3e', '辉', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:24:06', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000164', '286000000000192', '###a1de5e2f4d76345a48078e91abf56a3e', '敏', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:24:17', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000165', '286000000000205', '###a1de5e2f4d76345a48078e91abf56a3e', '刚', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:24:41', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000166', '286000000000191', '###a1de5e2f4d76345a48078e91abf56a3e', '磊', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:24:54', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000167', '286000000000220', '###a1de5e2f4d76345a48078e91abf56a3e', '群', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:25:08', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000168', '286000000000212', '###a1de5e2f4d76345a48078e91abf56a3e', '凡', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:25:20', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000169', '286000000000231', '###a1de5e2f4d76345a48078e91abf56a3e', '娣', '', '', null, '0', null, '', '', '4', '223.167.196.129', '2016-07-04 15:17:50', '2016-06-22 13:25:33', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000170', '286000000000206', '###a1de5e2f4d76345a48078e91abf56a3e', '荷', '', '', null, '0', null, '', '', '4', '60.169.117.20', '2016-07-08 01:30:04', '2016-06-22 13:25:49', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000171', '286000000000216', '###a1de5e2f4d76345a48078e91abf56a3e', '远', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:26:02', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000172', '286000000000227', '###a1de5e2f4d76345a48078e91abf56a3e', '勇', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:26:13', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000173', '286000000000232', '###a1de5e2f4d76345a48078e91abf56a3e', '举', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:26:27', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000174', '286000000000256', '###a1de5e2f4d76345a48078e91abf56a3e', '燕', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:26:37', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000175', '286000000000257', '###a1de5e2f4d76345a48078e91abf56a3e', '芝', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:26:52', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000176', '286000000000101', '###a1de5e2f4d76345a48078e91abf56a3e', '军', '', '', null, '0', null, '', '', '4', '42.95.148.75', '2016-07-05 18:50:18', '2016-06-22 13:27:03', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000177', '286000000000255', '###a1de5e2f4d76345a48078e91abf56a3e', '丽', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:27:14', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000178', '286000000000262', '###a1de5e2f4d76345a48078e91abf56a3e', '小肥羊', '', '', null, '0', null, '', '', '4', '36.63.184.8', '2016-07-08 14:03:26', '2016-06-22 13:27:25', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000179', '286000000000265', '###a1de5e2f4d76345a48078e91abf56a3e', '辉', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:27:38', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000180', '286000000000258', '###a1de5e2f4d76345a48078e91abf56a3e', '军', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:27:49', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000181', '286000000000259', '###a1de5e2f4d76345a48078e91abf56a3e', '彬', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:28:04', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000182', '286000000000236', '###a1de5e2f4d76345a48078e91abf56a3e', '章', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:28:16', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000183', '286000000000263', '###a1de5e2f4d76345a48078e91abf56a3e', '宁', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:28:37', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000184', '286000000000275', '###a1de5e2f4d76345a48078e91abf56a3e', '立', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:29:35', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000185', '286000000000277', '###a1de5e2f4d76345a48078e91abf56a3e', '梅', '', '', '5775dec34aa05.jpg', '0', null, '', '', '4', '61.51.86.6', '2016-07-08 13:44:40', '2016-06-22 13:29:48', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000186', '286000000000280', '###a1de5e2f4d76345a48078e91abf56a3e', '婷', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:30:01', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000187', '286000000000279', '###84fdf4ec9e6af367fef7874b8b7eb010', '小河', '', '', '576d0a17888cd.jpg', '0', '2016-06-02', '双赢', '', '4', '202.100.245.105', '2016-07-08 11:22:15', '2016-06-22 13:30:17', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000188', '286000000000090', '###a1de5e2f4d76345a48078e91abf56a3e', '治', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:30:32', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000189', '286000000000278', '###a1de5e2f4d76345a48078e91abf56a3e', '洲', '', '', null, '0', null, '', '', '4', '113.107.244.71', '2016-07-08 13:34:38', '2016-06-22 13:30:49', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000190', '286000000000268', '###a1de5e2f4d76345a48078e91abf56a3e', '新', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:30:59', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000191', '286000000000312', '###a1de5e2f4d76345a48078e91abf56a3e', '驰', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:31:18', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000192', '286000000000306', '###a1de5e2f4d76345a48078e91abf56a3e', '伟', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:31:31', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000193', '286000000000287', '###a1de5e2f4d76345a48078e91abf56a3e', '东', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:31:42', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000194', '286000000000320', '###3cda13efe5d18b34a9826c27ac94dfc3', '小雨', '', '', '57713f3628b31.jpg', '0', null, '', '', '4', '223.198.139.110', '2016-07-07 17:35:21', '2016-06-22 13:31:57', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000195', '286000000000308', '###a1de5e2f4d76345a48078e91abf56a3e', '生', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:32:09', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000196', '286000000000331', '###a1de5e2f4d76345a48078e91abf56a3e', '洲', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:32:21', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000197', '286000000000333', '###a1de5e2f4d76345a48078e91abf56a3e', '凯', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-07 19:28:56', '2016-06-22 13:32:31', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000198', '286000000000276', '###a1de5e2f4d76345a48078e91abf56a3e', '辉', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:32:44', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000199', '286000000000302', '###a1de5e2f4d76345a48078e91abf56a3e', '峰', '', '', null, '0', null, '', '', '4', '110.178.83.61', '2016-07-08 14:00:52', '2016-06-22 13:32:55', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000200', '286000000000369', '###a1de5e2f4d76345a48078e91abf56a3e', '平', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:33:06', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000201', '286000000000260', '###a1de5e2f4d76345a48078e91abf56a3e', '生', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:33:23', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000202', '286000000000329', '###a1de5e2f4d76345a48078e91abf56a3e', '兵', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-22 13:34:27', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000203', '28600000000325', '###a1de5e2f4d76345a48078e91abf56a3e', '宝林', '', '', null, '0', null, '', '', '4', '27.218.19.153', '2016-06-22 21:43:41', '2016-06-22 20:57:16', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000204', '286000000000385', '###a1de5e2f4d76345a48078e91abf56a3e', '磊', '', '', null, '0', null, '', '', '4', '123.7.17.45', '2016-07-08 15:11:21', '2016-06-23 16:27:57', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000205', '286000000000386', '###a1de5e2f4d76345a48078e91abf56a3e', '凌', '', '', null, '0', null, '', '', '4', '139.205.146.249', '2016-06-23 16:48:46', '2016-06-23 16:28:29', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000206', '286000000000379', '###a1de5e2f4d76345a48078e91abf56a3e', '剑彬', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-23 16:32:41', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000207', '286000000000368', '###a1de5e2f4d76345a48078e91abf56a3e', '文明', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-23 16:33:03', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000208', '286000000000388', '###a1de5e2f4d76345a48078e91abf56a3e', '建华', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-23 17:17:34', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000209', '286000000000067', '###a1de5e2f4d76345a48078e91abf56a3e', '生萌', '', '', null, '0', null, '', '', '4', '58.40.124.157', '2016-06-24 10:57:15', '2016-06-24 09:34:52', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000210', '286000000000310', '###a1de5e2f4d76345a48078e91abf56a3e', '拥军', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 09:51:29', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000211', '28600000000219', '###a1de5e2f4d76345a48078e91abf56a3e', '文刚', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 09:52:38', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000212', '286000000000185', '###a1de5e2f4d76345a48078e91abf56a3e', '国军', '', '', null, '0', null, '', '', '4', '223.211.93.229', '2016-06-24 12:29:42', '2016-06-24 10:00:23', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000213', '286000000000356', '###a1de5e2f4d76345a48078e91abf56a3e', '海霖', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 10:02:14', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000214', '286000000000377', '###a1de5e2f4d76345a48078e91abf56a3e', '建太', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 10:03:08', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000215', '286000000000396', '###a1de5e2f4d76345a48078e91abf56a3e', '加平', '', '', null, '0', null, '', '', '4', '119.188.101.228', '2016-06-24 10:44:22', '2016-06-24 10:05:37', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000216', '286000000000390', '###a1de5e2f4d76345a48078e91abf56a3e', '新喜', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 10:46:53', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000217', '286000000000071', '###a1de5e2f4d76345a48078e91abf56a3e', '武强', '', '', null, '0', null, '', '', '4', '183.43.211.228', '2016-06-24 11:37:12', '2016-06-24 11:10:09', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000218', '286000000000365', '###a1de5e2f4d76345a48078e91abf56a3e', '妍', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 11:10:29', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000219', '286000000000286', '###a1de5e2f4d76345a48078e91abf56a3e', '虎', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 11:10:48', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000220', '286000000000325', '###a1de5e2f4d76345a48078e91abf56a3e', '宝林', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 11:11:23', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000221', '286000000000332', '###a1de5e2f4d76345a48078e91abf56a3e', '晓斌', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 11:11:42', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000222', '286000000000291', '###a1de5e2f4d76345a48078e91abf56a3e', '银玲', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 11:12:01', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000223', '286000000000338', '###a1de5e2f4d76345a48078e91abf56a3e', '红杰', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 11:12:24', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000224', '286000000000380', '###a1de5e2f4d76345a48078e91abf56a3e', '永濮', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 11:12:39', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000225', '286000000000119', '###a1de5e2f4d76345a48078e91abf56a3e', '平', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 11:12:54', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000226', '286000000000391', '###a1de5e2f4d76345a48078e91abf56a3e', '敏', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 11:30:43', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000227', '286000000000399', '###a1de5e2f4d76345a48078e91abf56a3e', '利槐', '', '', null, '0', null, '', '', '4', '14.117.121.113', '2016-06-30 00:16:27', '2016-06-24 11:31:16', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000228', '286000000000398', '###a1de5e2f4d76345a48078e91abf56a3e', '莲娥', '', '', null, '0', null, '', '', '4', '113.134.172.60', '2016-07-08 13:46:35', '2016-06-24 11:40:44', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000229', '286000000000293', '###a1de5e2f4d76345a48078e91abf56a3e', '军', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 12:36:10', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000230', '286000000000179', '###a1de5e2f4d76345a48078e91abf56a3e', '金辉', '', '', null, '0', null, '', '', '4', '101.22.218.106', '2016-06-24 13:52:09', '2016-06-24 13:44:41', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000231', '286000000000315', '###a1de5e2f4d76345a48078e91abf56a3e', '永超', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 13:46:17', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000232', '286000000000381', '###a1de5e2f4d76345a48078e91abf56a3e', '卫想天开', '', '', null, '0', null, '', '', '4', '111.148.52.140', '2016-06-29 08:37:20', '2016-06-24 14:20:17', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000233', '286000000000373', '###a1de5e2f4d76345a48078e91abf56a3e', '娥', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-24 14:20:40', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000234', '286000000000507', '###a1de5e2f4d76345a48078e91abf56a3e', '文慧', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-28 10:44:21', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000235', '286000000000506', '###a1de5e2f4d76345a48078e91abf56a3e', '敏', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-28 10:45:13', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000236', '286000000000213', '###a1de5e2f4d76345a48078e91abf56a3e', '记秋', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-29 10:18:43', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000237', '286000000000513', '###a1de5e2f4d76345a48078e91abf56a3e', '以月', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-29 10:19:23', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000238', '61625', '###a1de5e2f4d76345a48078e91abf56a3e', '妙连', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-29 11:10:44', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000239', '61624', '###a1de5e2f4d76345a48078e91abf56a3e', '国梅', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-29 11:11:10', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000240', '61623', '###a1de5e2f4d76345a48078e91abf56a3e', '宏霖', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-29 11:11:27', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000241', '61622', '###a1de5e2f4d76345a48078e91abf56a3e', '钟诚 ', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-29 11:11:53', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000242', '61621', '###a1de5e2f4d76345a48078e91abf56a3e', '会琴', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-29 11:12:11', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000243', '61620', '###a1de5e2f4d76345a48078e91abf56a3e', '琪', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-06-29 15:15:10', '2016-06-29 11:12:31', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000244', '61619', '###a1de5e2f4d76345a48078e91abf56a3e', '梅秀', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-29 11:12:47', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000245', '61618', '###a1de5e2f4d76345a48078e91abf56a3e', ' 会玲 ', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-07 21:57:06', '2016-06-29 11:13:01', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000246', '61617', '###a1de5e2f4d76345a48078e91abf56a3e', '志鹏', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-07-08 10:22:35', '2016-06-29 11:13:15', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000247', '61626', '###a1de5e2f4d76345a48078e91abf56a3e', '浪', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-29 11:13:29', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000248', '61603', '###a1de5e2f4d76345a48078e91abf56a3e', '世金', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-29 11:13:49', '', '0', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000249', '286000000000392', '###a1de5e2f4d76345a48078e91abf56a3e', '军考', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-29 15:49:01', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000250', '286000000000363', '###a1de5e2f4d76345a48078e91abf56a3e', '金钰', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-29 16:39:46', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000251', '286000000000502', '###a1de5e2f4d76345a48078e91abf56a3e', '瑾', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-29 17:02:48', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000252', '286000000000295', '###a1de5e2f4d76345a48078e91abf56a3e', '898', '', '', null, '0', null, '', '', '4', '27.24.254.49', '2016-07-08 09:23:14', '2016-06-29 20:25:37', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000253', '286000000000089', '###a1de5e2f4d76345a48078e91abf56a3e', '全刚', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-30 11:14:32', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000254', '286000000000517', '###a1de5e2f4d76345a48078e91abf56a3e', '春花', '', '', null, '0', null, '', '', '4', '113.99.6.35', '2016-07-07 09:15:32', '2016-06-30 12:00:16', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000255', '286000000000508', '###a1de5e2f4d76345a48078e91abf56a3e', '晓琴', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-30 14:03:57', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000256', '286000000000393', '###a1de5e2f4d76345a48078e91abf56a3e', '来兴', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-06-30 14:15:42', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000257', '286000000000509', '###a1de5e2f4d76345a48078e91abf56a3e', '广飞', '', '', null, '0', null, '', '', '4', '120.236.150.233', '2016-06-30 15:07:47', '2016-06-30 15:06:41', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000258', '286000000000511', '###a1de5e2f4d76345a48078e91abf56a3e', '卫中', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-07-04 10:52:08', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000259', '286000000000520', '###a1de5e2f4d76345a48078e91abf56a3e', '虹', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-07-04 19:55:09', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000260', '286000000000522', '###a1de5e2f4d76345a48078e91abf56a3e', '贵桃', '', '', null, '0', null, '', '', '4', '122.195.114.93', '2016-07-05 18:20:33', '2016-07-04 19:55:37', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000261', '286000000000525', '###a1de5e2f4d76345a48078e91abf56a3e', '带娣', '', '', null, '0', null, '', '', '4', '113.224.232.170', '2016-07-08 08:45:19', '2016-07-05 18:47:10', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000262', '61611', '###a1de5e2f4d76345a48078e91abf56a3e', '雄', '', '', null, '0', null, '', '', '4', '183.63.120.174', '2016-07-08 09:29:43', '2016-07-06 10:52:47', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000263', '61610', '###a1de5e2f4d76345a48078e91abf56a3e', '娜', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-07-06 10:53:07', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000264', '61627', '###a1de5e2f4d76345a48078e91abf56a3e', '顺成', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-07-06 10:53:23', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000265', '61628', '###a1de5e2f4d76345a48078e91abf56a3e', '朝熙', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-07-06 10:53:39', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000266', '61609', '###a1de5e2f4d76345a48078e91abf56a3e', '秋影', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-07-06 10:53:56', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000267', '61638', '###a1de5e2f4d76345a48078e91abf56a3e', '兴', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-07-06 10:57:14', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000268', '61639', '###a1de5e2f4d76345a48078e91abf56a3e', '秋园', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-07-06 10:57:36', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000269', '61634', '###a1de5e2f4d76345a48078e91abf56a3e', '志明', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-07-06 11:07:46', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000270', '61636', '###a1de5e2f4d76345a48078e91abf56a3e', '大芳', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-07-06 11:08:01', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000271', '61637', '###a1de5e2f4d76345a48078e91abf56a3e', '双珠', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-07-06 11:08:35', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000272', '286000000000518', '###a1de5e2f4d76345a48078e91abf56a3e', '贵池', '', '', null, '0', null, '', '', '4', null, '2000-01-01 00:00:00', '2016-07-06 14:42:21', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000273', '286000000000252', '###a1de5e2f4d76345a48078e91abf56a3e', '俊', '', '', null, '0', null, '', '', '4', '171.211.53.10', '2016-07-07 22:20:46', '2016-07-07 08:59:29', '', '1', '0', '3', '0', '');
INSERT INTO `chat_users` VALUES ('10000274', '286000000000537', '###a1de5e2f4d76345a48078e91abf56a3e', '金小川', '', '', null, '0', '1970-06-11', '', '', '4', '60.188.124.128', '2016-07-08 09:47:23', '2016-07-07 09:26:37', '', '1', '0', '3', '0', '');

-- ----------------------------
-- Table structure for chat_user_favorites
-- ----------------------------
DROP TABLE IF EXISTS `chat_user_favorites`;
CREATE TABLE `chat_user_favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户 id',
  `title` varchar(255) DEFAULT NULL COMMENT '收藏内容的标题',
  `url` varchar(255) DEFAULT NULL COMMENT '收藏内容的原文地址，不带域名',
  `description` varchar(500) DEFAULT NULL COMMENT '收藏内容的描述',
  `table` varchar(50) DEFAULT NULL COMMENT '收藏实体以前所在表，不带前缀',
  `object_id` int(11) DEFAULT NULL COMMENT '收藏内容原来的主键id',
  `createtime` int(11) DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户收藏表';

-- ----------------------------
-- Records of chat_user_favorites
-- ----------------------------

-- ----------------------------
-- Table structure for chat_word
-- ----------------------------
DROP TABLE IF EXISTS `chat_word`;
CREATE TABLE `chat_word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '敏感词',
  `status` tinyint(1) DEFAULT '1' COMMENT '1正常，-1删除',
  `creator` varchar(50) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of chat_word
-- ----------------------------
INSERT INTO `chat_word` VALUES ('1', 'sb', '1', '1', '2016-06-09 15:41:16');
INSERT INTO `chat_word` VALUES ('2', '滚', '1', '1', '2016-06-09 15:41:16');
INSERT INTO `chat_word` VALUES ('3', '垃圾', '1', '1', '2016-06-09 15:42:34');
INSERT INTO `chat_word` VALUES ('4', '骗子', '1', '1', '2016-06-09 15:42:34');
INSERT INTO `chat_word` VALUES ('5', '去你妈', '1', '1', '2016-06-09 15:42:34');
INSERT INTO `chat_word` VALUES ('6', '亏', '-1', '1', '2016-06-09 15:54:36');
INSERT INTO `chat_word` VALUES ('7', '你妈', '1', '1', '2016-06-09 15:54:36');

-- ----------------------------
-- Table structure for chat_zhanfa
-- ----------------------------
DROP TABLE IF EXISTS `chat_zhanfa`;
CREATE TABLE `chat_zhanfa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `content` varchar(500) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '状态：1正常，-1删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of chat_zhanfa
-- ----------------------------
INSERT INTO `chat_zhanfa` VALUES ('1', '私募股票', '连续六根阳线代表大丰收', 'http://baidu.com', '1', '2016-06-09 14:18:03');
SET FOREIGN_KEY_CHECKS=1;
