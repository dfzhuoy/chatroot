#!/bin/bash 
####服务端重启####
if [ $(ps -ef |grep "webim_server.php"|grep -v "grep"|wc -l) -gt 0 ];then
	allpids=$(ps -ef|grep 'webim_server.php'|grep -v "grep"|awk '{print $2}')
	for pid in $allpids
	do
		$(kill -9 $pid)
		echo "kill -9 $pid"
	done
	echo "/application/php54/bin/php /application/nginx/html/chatroom/shell/webim_server.php"
	$(/application/php54/bin/php /application/nginx/html/chatroom/shell/webim_server.php >> /application/nginx/logs/webim.log &)
	
else
	echo '没有进程需要kill，直接启动'
	$(/application/php54/bin/php /application/nginx/html/chatroom/shell/webim_server.php >> /application/nginx/logs/webim.log  &)
fi 

