<?php
namespace WebIM\Store;

use Common\Model\ChatModel;
use Common\Model\UsersModel;

class Mysql {

	function __construct() {
		// 引入ThinkPHP入口文件
        require_once 'ThinkPHP/ThinkPHP.php';
	}

	/**
	 * @param $uid
	 * @param $msg
	 * @return mixed
	 * 聊天数据入库
	 */
    public function addChat($uid,$msg){
    	$map['uid'] = $uid ? $uid : $msg['user']['uid'];
		$map['message'] = $msg['data'];
		if($msg['type']){
			$map['type'] = $msg['type'];
		}
		if($msg['color']){
			$map['color'] = $msg['color'];
		}
		if($msg['size']){
			$map['size'] = $msg['size'];
		}
		$map['reply_uid'] = $msg['reply_uid'];
		$map['reply_message'] = $msg['reply_message'];
		$map['reply_time'] = date('Y-m-d H:i:s');

		$rs = M('chat_chat')->add($map);
		file_put_contents('/alidata/log/web/chat.log',M('')->_sql()."\n",FILE_APPEND);

		return $rs;
    }

	/**
	 * @param $uid
	 * @param $msg
	 * @return mixed
	 * 纸条数据入库
	 */
	public function addNote($uid,$msg){
		$map['uid'] = $uid ;
		$map['to'] = $msg['to'];
		$map['type'] = $msg['type'];
		$map['message'] = $msg['data'];
		$map['note_uid'] = $msg['note_uid'];
		$map['note_message'] = $msg['note_message'];
		if($msg['note_time']){
			$map['note_time'] = date('Y-m-d H:i:s',strtotime($msg['note_time']));
		}
		$rs = M('chat_note')->add($map);
		file_put_contents('/alidata/log/web/note.log',M('')->_sql()."\n",FILE_APPEND);

		return $rs;
	}

	/**
	 * @param $msg
	 * 直播室状态
	 */
	public function setLive($msg){
		if($msg['status']){
			$map['key'] = 'room_status';
			$row['value'] = $msg['status'];
			M('chat_config')->where($map)->save($row);
			file_put_contents('/alidata/log/web/status.log',M('')->_sql()."\n",FILE_APPEND);
		}else{
			file_put_contents('/alidata/log/web/status.log','数据状态异常:'.json_encode($msg)."\n",FILE_APPEND);
		}
	}

	/**
	 * @param $uid
	 * @param $msg
	 * @return mixed
	 * 喊单数据入库
	 */
	public function addCall($uid,$msg){
		//如果是平仓
		if($msg['id'] && $msg['if_close']){
			$where['id'] = $msg['id'];

			$params['if_close'] = 1 ;
			$params['close_time'] = Date('Y-m-d H:i:s');
			$params['close_price_category'] = $msg['close_price_category'];
			$params['close_price'] = $msg['close_price'];
			$params['close_remark'] = $msg['close_remark'];
			$params['close_teacher_id'] = $uid;

			$rs = M('chat_call')->where($where)->save($params);
			file_put_contents('/alidata/log/web/call.log',json_encode($params).'--'.M('')->_sql()."\n",FILE_APPEND);

			return $rs;
		}else{
			//开仓
			$params['teacher_id'] = $uid ;
			$params['category'] = $msg['category'];
			$params['price_category'] = $msg['price_category'];
			$params['price'] = $msg['price'];
			$params['position'] = $msg['position'];
			$params['stop_profit_price'] = $msg['stop_profit_price'];
			$params['stop_loss_price'] = $msg['stop_loss_price'];
			$params['deal_variety'] = $msg['deal_variety'];
			$params['remark'] = $msg['remark'];
			$rs = M('chat_call')->add($params);
			file_put_contents('/alidata/log/web/call.log',json_encode($params).'--'.M('')->_sql()."\n",FILE_APPEND);
			return $rs;
		}

	}

	public function getCalldetail($callid){
		if(empty($callid)){
			file_put_contents('/alidata/log/web/call.log','喊单数据为空\n',FILE_APPEND);
			return false;
		}
		$result = M('chat_call')->field('id,price,teacher_id,remark,create_time,category,deal_variety,position,price_category')->where(['id'=>$callid])->find();
		$category = $this->getCategory();
		$deal_variety = $this->getDealVariety();
		$position = $this->getPostion();
		$price_category = $this->getPriceCategory();

		$result['category_name'] = $category[$result['category']]['name'];
		$result['deal_variety_name'] = $deal_variety[$result['deal_variety']]['name'];
		$result['position_name'] = $position[$result['position']]['name'];
		$result['price_category_name'] = $price_category[$result['price_category']]['name'];

		return $result;
	}

	public function getCategory(){
		return $arr = [
			'1'=>['name'=>'买入建仓', 'value'=>'1'],
			'2'=>['name'=>'卖出建仓', 'value'=>'2'],
		];
	}

	public function getPriceCategory(){
		return $arr = [
			'1'=>['name'=>'市价', 'value'=>'1'],
			'2'=>['name'=>'指价', 'value'=>'2'],
		];
	}

	public function getPostion(){
		return $arr = [
			'100'=>['name'=>'满仓', 'value'=>'100'],
			'90'=>['name'=>'九层仓', 'value'=>'90'],
			'80'=>['name'=>'八层仓', 'value'=>'80'],
			'70'=>['name'=>'七层仓', 'value'=>'70'],
			'60'=>['name'=>'六层仓', 'value'=>'60'],
			'50'=>['name'=>'五层仓', 'value'=>'50'],
			'40'=>['name'=>'四层仓', 'value'=>'40'],
			'30'=>['name'=>'三层仓', 'value'=>'30'],
			'20'=>['name'=>'二层仓', 'value'=>'20'],
			'10'=>['name'=>'一层仓', 'value'=>'10'],
		];
	}

	public function getDealVariety(){
		$arr = [];
		$map['status'] = 1;
		$map['key'] = 'call_deal_variety';
		$value_str = M('chat_config')->where($map)->getField('value');
		$row = explode("\n",$value_str);
		if(!empty($row)){
			foreach ($row as $item) {
				list($value,$name) = explode('|',$item);
				$arr[$value] = ['name'=>$name,'value'=>$value];
			}
		}
		return $arr;
	}
	/**
	 * @param $uid
	 * 判断是否是老师
	 */
	public function isTeacher($uid){
		if(empty($uid)){
			return false;
		}
		$map['user_id'] = $uid;
		$role_id = M('chat_role_user')->cache(true,600)->where($map)->getField('role_id');
		//2默认是老师
		if($role_id == 2){
			return true;
		}else{
			return false;
		}
	}

}
