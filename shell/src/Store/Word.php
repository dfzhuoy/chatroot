<?php
namespace WebIM\Store;

class Word {

	function __construct() {
		// 引入ThinkPHP入口文件
        require_once 'ThinkPHP/ThinkPHP.php';
        require_once 'SimpleDict.php';
	}

    /**
     * @param $message
     * @return string
     * 过滤敏感词
     */
    public function filterword($message){
        if(empty($message)){
            $this->error = '空消息';
            return false;
        }
        
        //利用SimpleDict将关键词创建成字典
        $out_file = '/tmp/1_one_level_dict_'.date('H-i').'file';
        if(!file_exists($out_file)){
            //选出全部一级的监控词
            $map['status'] = 1 ; //全部启用的关键词
            $data = M('chat_word')->field('id,name')->where($map)->select();
            $one_data = $this->_formatWord($data);
            (new SimpleDict)->arr_make($one_data ,$out_file);
        }
        $dict = new SimpleDict($out_file);
        $res_data = $dict->search($message);
        if($res_data){
            //第一级有敏感词，但是可以正常发送
            $do_message = $dict->replace($message,'**');
            return $do_message;
        }else{
            return $message;
        }
    }
    /**
     * @param $data
     * 格式化监控词的数据
     */
    protected function _formatWord($data){
        if(empty($data)){
            return false;
        }
        foreach($data as $k=> $v){
            $result[$v['id']] = $v['name'];
        }
        return $result;
    }
	


	
}
